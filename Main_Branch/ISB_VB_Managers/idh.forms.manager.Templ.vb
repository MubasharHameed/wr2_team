Imports System.IO
Imports System.Collections
Imports System

Imports IDHAddOns.idh.controls
Imports com.idh.bridge.reports
Imports com.idh.utils.Conversions
Imports com.idh.bridge
Imports com.idh.bridge.data
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User
Imports com.idh.bridge.action
Imports com.idh.controls
Imports com.idh.controls.strct
Imports com.idh.dbObjects.numbers
Imports com.idh.bridge.resources
Imports com.isb.core.forms

Namespace idh.forms.manager
    Public MustInherit Class Templ
        Inherits IDHAddOns.idh.forms.Base

        'Shared moTotalFields() As String = { _
        '    "49", "45", "44", "46", "47", "48", _
        '    "IDH_CstWgt", "IDH_SubAD", "IDH_Total", _
        '    "IDH_DisAmt", "IDH_AddEx", "IDH_TaxAmt" _
        '    }

        Shared moTotalFields() As String = {
            "IDH_CstWgt", "IDH_SubAD", "IDH_SbADCt", "IDH_Total",
            "IDH_DisAmt", "IDH_AddEx", "IDH_TaxAmt",
            "IDH_RdWgt",
            "IDH_TAdCst", "IDH_TAdChg", "IDH_TxAmCo",
            "IDH_TotChg", "IDH_ChgDed"
        }
        Shared moTotalFields_Charge() As String = {
           "IDH_SubAD", "IDH_TAdChg", "IDH_ChgDed", "IDH_TaxAmt", "IDH_Total", "IDH_DisAmt"
       }
        Shared moTotalFields_Cost() As String = {
           "IDH_SbADCt", "IDH_TAdCst", "IDH_AddEx", "IDH_TxAmCo", "IDH_TotChg"
       }

        Dim AxBrowser As SHDocVw.InternetExplorer
        Dim bModal As Boolean = False
        Private ghStatusChanged As New ArrayList

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sID As String, ByVal sParMenu As String, ByVal sSRF As String, ByVal iMenuPosition As Integer, ByVal sTitle As String)
            MyBase.New(oParent, sID, (If(sParMenu Is Nothing, "IDHPS", sParMenu)), iMenuPosition, sSRF, True, True, False, sTitle, load_Types.idh_LOAD_NORMAL)
        End Sub

        Protected Overridable Sub doTheGridLayout(ByVal oGridN As UpdateGrid)
            If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                Dim sFormTypeId As String = oGridN.getSBOForm().TypeEx
                Dim oFormSettings As IDH_FORMSET = getWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId)
                If oFormSettings Is Nothing Then
                    oFormSettings = New IDH_FORMSET()
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, doGetCapacityQuery())
                    setWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId, oFormSettings)

                    oFormSettings.doAddFieldToGrid(oGridN)

                    doSetListFields(oGridN)
                    doAddAdditionalRequiredListFields(oGridN)

                    'If oFormSettings.SkipFormSettings = False Then
                    '    oFormSettings.doSetGrid(oGridN)
                    'End If
                    oFormSettings.doSyncDB(oGridN)
                Else
                    If oFormSettings.SkipFormSettings Then
                        doSetListFields(oGridN)
                        doAddAdditionalRequiredListFields(oGridN)
                    Else
                        oFormSettings.doAddFieldToGrid(oGridN)
                        'oFormSettings.doSetGrid(oGridN)
                    End If
                End If
            Else
                doSetListFields(oGridN)
                doAddAdditionalRequiredListFields(oGridN)
            End If
        End Sub

        Protected Function doGetProgressStatusQuery() As String

            Try
                Dim sProgressStatusQuery As String = ""

                Dim oRecords As com.idh.bridge.DataRecords = Nothing
                oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doGetProgressQuerries", "Select U_ProgSQL,U_ProgImgSQL from [@IDH_PRGSQL] with(nolock)")
                If (oRecords IsNot Nothing AndAlso oRecords.RecordCount > 0) Then
                    sProgressStatusQuery = oRecords.getValue("U_ProgSQL").ToString()
                    Return sProgressStatusQuery
                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {"doGetProgressStatusQuery"})
            End Try
            If Config.ParameterAsBool("USENWSTS", False) Then
                Dim varname1 As New System.Text.StringBuilder
                varname1.Append("( CASE When  e.U_Status = 9 AND e.Code = r.U_JobNr AND r.U_Driver != 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') Then " _
                                & " '" & com.idh.bridge.lookups.FixedValues.getProgressClosed(False) & "'" _
                                & " When e.U_Status = 9 AND e.Code = r.U_JobNr AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') " _
                                & " AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') Then '" & com.idh.bridge.lookups.FixedValues.getProgressCancelled(False) & "' " _
                                & " When e.U_Status = 9 AND e.Code = r.U_JobNr AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') " _
                                & " AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) Then '" & com.idh.bridge.lookups.FixedValues.getProgressDFT(False) & "' " _
                                & " when e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' + " _
                                & " STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 AND U_ASDate IS NULL " _
                                & " AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') " _
                                & " Then '" & com.idh.bridge.lookups.FixedValues.getProgressLate(False) & "' " _
                                & " When e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' " _
                                & " + STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 AND U_ASDate IS NULL " _
                                & " AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') Then '" & com.idh.bridge.lookups.FixedValues.getProgressCancelled(False) & "' " _
                                & " When e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' " _
                                & " + STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 " _
                                & " AND U_ASDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) " _
                                & " Then '" & com.idh.bridge.lookups.FixedValues.getProgressDFT(False) & "' " _
                                & " When e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') " _
                                & " AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') Then '" & com.idh.bridge.lookups.FixedValues.getProgressNoRequest(False) & "' " _
                                & " when  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') " _
                                & " Then '" & com.idh.bridge.lookups.FixedValues.getProgressCancelled(False) & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) " _
                                & " Then '" & com.idh.bridge.lookups.FixedValues.getProgressDFT(False) & "' " _
                                & " when e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL " _
                                & " AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') " _
                                & " Then '" & com.idh.bridge.lookups.FixedValues.getProgressAssigned(False) & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL AND r.U_Driver = 'Y' " _
                                & " AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') Then '" & com.idh.bridge.lookups.FixedValues.getProgressCancelled(False) & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL AND r.U_Driver = 'Y' " _
                                & " AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) Then '" & com.idh.bridge.lookups.FixedValues.getProgressCancelled(False) & "' " _
                                & " when  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL " _
                                & " AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) " _
                                & " Then '" & com.idh.bridge.lookups.FixedValues.getProgressDFT(False) & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL " _
                                & " AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') " _
                                & " Then '" & com.idh.bridge.lookups.FixedValues.getProgressDone(False) & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL AND r.U_Driver = 'Y' " _
                                & " AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') then '" & com.idh.bridge.lookups.FixedValues.getProgressCancelled(False) & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' + " _
                                & " STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60 " _
                                & " AND U_ASDate IS NULL AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') " _
                                & " Then '" & com.idh.bridge.lookups.FixedValues.getProgressWaiting(False) & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' + " _
                                & " STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60 AND U_ASDate IS NULL " _
                                & " AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') Then '" & com.idh.bridge.lookups.FixedValues.getProgressCancelled(False) & "' " _
                                & " when  (e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' + " _
                                & " STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60 " _
                                & " AND U_ASDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " _
                                & " Then '" & com.idh.bridge.lookups.FixedValues.getProgressDFT(False) & "' End )")
                Return varname1.ToString
            Else
                Return "(" _
                          & " CASE When e.U_Status = 9 Then '" & com.idh.bridge.lookups.FixedValues.getProgressClosed(False) & "'  " _
                          & " When e.U_Status != 9 AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffLateCheck3() & " " _
                          & " AND r.U_ASDate IS NULL Then '" & com.idh.bridge.lookups.FixedValues.getProgressLate(False) & "' " _
                          & " WHEN e.U_Status != 9 " _
                          & " AND U_RDate IS NULL Then '" & com.idh.bridge.lookups.FixedValues.getProgressNoRequest(False) & "' " _
                          & " WHEN e.U_Status != 9 " _
                          & " AND r.U_ASDate IS NOT NULL " _
                          & " AND r.U_AEDate IS NULL Then '" & com.idh.bridge.lookups.FixedValues.getProgressBusy(False) & "' " _
                          & " WHEN e.U_Status != 9 " _
                          & " AND U_ASDate IS NOT NULL " _
                          & " AND U_AEDate IS NOT NULL THEN '" & com.idh.bridge.lookups.FixedValues.getProgressDone(False) & "' " _
                          & " WHEN e.U_Status != 9 " _
                          & " AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffWaitCheck3() & "  " _
                          & " AND U_ASDate IS NULL THEN '" & com.idh.bridge.lookups.FixedValues.getProgressWaiting(False) & "' " _
                          & " End " _
                          & " )"

            End If
        End Function

        Protected Function doGetProgressImageQuery() As String
            Try
                Dim sProgressImageQuery As String = ""
                Dim oRecords As com.idh.bridge.DataRecords = Nothing
                oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doGetProgressQuerries", "Select U_ProgSQL,U_ProgImgSQL from [@IDH_PRGSQL] with(nolock)")
                If (oRecords IsNot Nothing AndAlso oRecords.RecordCount > 0) Then
                    sProgressImageQuery = oRecords.getValue("U_ProgImgSQL").ToString()
                    Dim sImgPath As String = Config.ParameterWithDefault("IMGPATH", "").Trim
                    If sImgPath <> "" Then
                        If Not sImgPath.EndsWith("\") Then
                            sImgPath &= "\"
                        End If
                        sProgressImageQuery = sProgressImageQuery.Replace("\IMGPATH\", sImgPath)
                    Else
                        Dim sPath As String = IDHAddOns.idh.addon.Base.doFixImagePath("Late.png").Replace("Late.png", "")
                        sProgressImageQuery = sProgressImageQuery.Replace("\IMGPATH\", sPath)
                    End If
                    Return sProgressImageQuery
                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {"doGetProgressStatusQuery"})
            End Try

            If Config.ParameterAsBool("USENWSTS", False) Then
                Dim varname1 As New System.Text.StringBuilder

                Dim sLate As String
                Dim sClosed As String
                Dim sCancelled As String
                Dim sDN As String
                Dim sNoRequest As String
                Dim sStarted As String
                Dim sComplete As String
                Dim sWaiting As String
                If Config.ParameterWithDefault("IMGPATH", "").Trim = "" Then
                    sLate = IDHAddOns.idh.addon.Base.doFixImagePath("Late.png")
                    sClosed = IDHAddOns.idh.addon.Base.doFixImagePath("Closed.png")
                    sCancelled = IDHAddOns.idh.addon.Base.doFixImagePath("Cancelled.png")
                    sDN = IDHAddOns.idh.addon.Base.doFixImagePath("DN.png")
                    sNoRequest = IDHAddOns.idh.addon.Base.doFixImagePath("No Request.png")
                    sStarted = IDHAddOns.idh.addon.Base.doFixImagePath("Started.png")
                    sComplete = IDHAddOns.idh.addon.Base.doFixImagePath("Complete.png")
                    sWaiting = IDHAddOns.idh.addon.Base.doFixImagePath("Waiting.png")
                Else
                    Dim sImgPath As String = Config.ParameterWithDefault("IMGPATH", "").Trim
                    If Not sImgPath.EndsWith("\") Then
                        sImgPath &= "\"
                    End If
                    sLate = sImgPath & ("Late.png")
                    sClosed = sImgPath & ("Closed.png")
                    sCancelled = sImgPath & ("Cancelled.png")
                    sDN = sImgPath & ("DN.png")
                    sNoRequest = sImgPath & ("No Request.png")
                    sStarted = sImgPath & ("Started.png")
                    sComplete = sImgPath & ("Complete.png")
                    sWaiting = sImgPath & ("Waiting.png")
                End If
                varname1.Append("( CASE When  e.U_Status = 9 AND e.Code = r.U_JobNr AND r.U_Driver != 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') Then " _
                                & " '" & sClosed & "'" _
                                & " When e.U_Status = 9 AND e.Code = r.U_JobNr AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') " _
                                & " AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') Then '" & sCancelled & "' " _
                                & " When e.U_Status = 9 AND e.Code = r.U_JobNr AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') " _
                                & " AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) Then '" & sDN & "' " _
                                & " when e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' + " _
                                & " STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 AND U_ASDate IS NULL " _
                                & " AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') " _
                                & " Then '" & sLate & "' " _
                                & " When e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' " _
                                & " + STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 AND U_ASDate IS NULL " _
                                & " AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') Then '" & sCancelled & "' " _
                                & " When e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' " _
                                & " + STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 " _
                                & " AND U_ASDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) " _
                                & " Then '" & sDN & "' " _
                                & " When e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') " _
                                & " AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') Then '" & sNoRequest & "' " _
                                & " when  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') " _
                                & " Then '" & sCancelled & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) " _
                                & " Then '" & sDN & "' " _
                                & " when e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL " _
                                & " AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') " _
                                & " Then '" & sStarted & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL AND r.U_Driver = 'Y' " _
                                & " AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') Then '" & sCancelled & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL AND r.U_Driver = 'Y' " _
                                & " AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) Then '" & sCancelled & "' " _
                                & " when  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL " _
                                & " AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) " _
                                & " Then '" & sDN & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL " _
                                & " AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') " _
                                & " Then '" & sComplete & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL AND r.U_Driver = 'Y' " _
                                & " AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') then '" & sCancelled & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' + " _
                                & " STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60 " _
                                & " AND U_ASDate IS NULL AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') " _
                                & " Then '" & sWaiting & "' " _
                                & " When  e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' + " _
                                & " STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60 AND U_ASDate IS NULL " _
                                & " AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') Then '" & sCancelled & "' " _
                                & " when  (e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' + " _
                                & " STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60 " _
                                & " AND U_ASDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " _
                                & " Then '" & sDN & "' End )")
                Return varname1.ToString
            Else
                Dim sLate As String
                Dim sClosed As String

                Dim sNoRequest As String
                Dim sStarted As String
                Dim sComplete As String
                Dim sWaiting As String
                If Config.ParameterWithDefault("IMGPATH", "").Trim = "" Then
                    sLate = IDHAddOns.idh.addon.Base.doFixImagePath("Late.png")
                    sClosed = IDHAddOns.idh.addon.Base.doFixImagePath("Closed.png")
                    sNoRequest = IDHAddOns.idh.addon.Base.doFixImagePath("No Request.png")
                    sStarted = IDHAddOns.idh.addon.Base.doFixImagePath("Started.png")
                    sComplete = IDHAddOns.idh.addon.Base.doFixImagePath("Complete.png")
                    sWaiting = IDHAddOns.idh.addon.Base.doFixImagePath("Waiting.png")
                Else
                    Dim sImgPath As String = Config.ParameterWithDefault("IMGPATH", "").Trim
                    If Not sImgPath.EndsWith("\") Then
                        sImgPath &= "\"
                    End If
                    sLate = sImgPath & ("Late.png")
                    sClosed = sImgPath & ("Closed.png")
                    sNoRequest = sImgPath & ("No Request.png")
                    sStarted = sImgPath & ("Started.png")
                    sComplete = sImgPath & ("Complete.png")
                    sWaiting = sImgPath & ("Waiting.png")
                End If
                Return "(" _
                 & " CASE When e.U_Status = 9 Then '" & sClosed & "'  " _
                 & " When e.U_Status != 9 AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffLateCheck3() & " " _
                 & " AND r.U_ASDate IS NULL Then '" & sLate & "' " _
                 & " WHEN e.U_Status != 9 " _
                 & " AND U_RDate IS NULL Then '" & sNoRequest & "' " _
                 & " WHEN e.U_Status != 9 " _
                 & " AND r.U_ASDate IS NOT NULL " _
                 & " AND r.U_AEDate IS NULL Then '" & sStarted & "' " _
                 & " WHEN e.U_Status != 9 " _
                 & " AND U_ASDate IS NOT NULL " _
                 & " AND U_AEDate IS NOT NULL THEN '" & sComplete & "' " _
                 & " WHEN e.U_Status != 9 " _
                 & " AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffWaitCheck3() & "  " _
                 & " AND U_ASDate IS NULL THEN '" & sWaiting & "' " _
                 & " End " _
                 & " )"
            End If
        End Function

        '*** This is where the list fields are set
        Protected MustOverride Sub doSetListFields(ByVal oGrid As UpdateGrid)

        '*** This is where the search fields are s
        Protected Overridable Sub doSetFilterLayout(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        'Public Function doSetGrid(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid, Optional ByVal sGridName As String = "") As Boolean
        '    'IDH_FORMSET.doSetGrid(oGridN, sGridName, doGetCapacityQuery())

        '    'Dim sSql As String = "Select * from [@IDH_FORMSET] Where U_FormID='" & oGridN.getSBOForm.TypeEx & "' And U_UserName='" & PARENT.gsUserName & "' And IsNull(U_GridName,'')='" & sGridName & "' Order by U_Index Asc, Cast(code as Int)"
        '    'Dim oRecSet As SAPbobsCOM.Recordset = PARENT.goDB.doSelectQuery(sSql)
        '    'If oRecSet IsNot Nothing AndAlso oRecSet.RecordCount = 0 Then
        '    '    sSql = "Select * from [@IDH_FORMSET] Where U_FormID='" & oGridN.getSBOForm.TypeEx & "' And U_UserName='All' And U_DeptCode=(Select Department from OUSR Where OUSR.USER_CODE='" & PARENT.gsUserName & "')  And IsNull(U_GridName,'')='" & sGridName & "' Order by U_Index Asc, Cast(code as Int) Asc"
        '    '    oRecSet = PARENT.goDB.doSelectQuery(sSql)
        '    '    If Not (oRecSet IsNot Nothing AndAlso oRecSet.RecordCount > 0) Then
        '    '        sSql = "Select * from [@IDH_FORMSET] Where U_FormID='" & oGridN.getSBOForm.TypeEx & "' And U_UserName='template'  And IsNull(U_GridName,'')='" & sGridName & "' Order by U_Index Asc, Cast(code as Int) Asc"
        '    '        oRecSet = PARENT.goDB.doSelectQuery(sSql)
        '    '    End If
        '    'End If

        '    'If oRecSet IsNot Nothing AndAlso oRecSet.RecordCount > 0 Then
        '    '    Dim iWidth, sFieldId, sTitle, sType, _
        '    '    iLinkObject, sWidthIsMax, sPinned, iIndex, sUID As String
        '    '    Dim iBackColor, iSumType, iDisplayType As Int32
        '    '    Dim bEditable As Boolean
        '    '    Dim sTableNameinFiled() As String '= ""
        '    '    Dim sFieldOrderBy As String = "", sCompleteOrderBy As String = ""
        '    '    While Not oRecSet.EoF
        '    '        'sName = oRecSet.Fields.Item("Name").Value
        '    '        iWidth = oRecSet.Fields.Item("U_Width").Value
        '    '        sFieldId = oRecSet.Fields.Item("U_FieldId").Value
        '    '        sTitle = oRecSet.Fields.Item("U_Title").Value
        '    '        bEditable = IIf(oRecSet.Fields.Item("U_Editable").Value = "Y", True, False)
        '    '        sType = IIf(oRecSet.Fields.Item("U_sType").Value = "", Nothing, oRecSet.Fields.Item("U_sType").Value)
        '    '        sUID = IIf(oRecSet.Fields.Item("U_sUID").Value = "", Nothing, oRecSet.Fields.Item("U_sUID").Value)
        '    '        iBackColor = IIf(oRecSet.Fields.Item("U_iBackColor").Value <= 0, -1, oRecSet.Fields.Item("U_iBackColor").Value)
        '    '        iLinkObject = IIf(oRecSet.Fields.Item("U_oLinkObject").Value <= 0, -1, oRecSet.Fields.Item("U_oLinkObject").Value)
        '    '        sWidthIsMax = oRecSet.Fields.Item("U_bWidthIsMax").Value
        '    '        sPinned = oRecSet.Fields.Item("U_bPinned").Value
        '    '        iSumType = oRecSet.Fields.Item("U_oSumType").Value
        '    '        iDisplayType = oRecSet.Fields.Item("U_oDisplayType").Value
        '    '        sFieldOrderBy = oRecSet.Fields.Item("U_OrderBy").Value

        '    '        If goParent.goLookup.getParameterAsBool("OSM4DCOV", False) = False AndAlso sFieldId.StartsWith("f.") _
        '    '            AndAlso ((oGridN.getSBOForm.TypeEx = "IDHJOBR4" OrElse oGridN.getSBOForm.TypeEx = "IDHJOBR5")) Then
        '    '            'If goParent.goLookup.getParameterAsBool("OSM4DCOV", False) = False AndAlso sFieldId.StartsWith("f.") Then
        '    '            oRecSet.MoveNext()
        '    '            Continue While
        '    '            'End If
        '    '        ElseIf oGridN.getSBOForm.TypeEx = "IDHVECR3" AndAlso sGridName = "VEHGRID" AndAlso sFieldId = "doGetCapacityQuery()" Then
        '    '            sFieldId = doGetCapacityQuery()
        '    '        End If

        '    '        Dim oField As ListField = oGridN.doAddFormSettingListField(sFieldId, sTitle, bEditable, iWidth, sType, sUID, iBackColor, iLinkObject, _
        '    '                                         IIf(Not sWidthIsMax = "" AndAlso sWidthIsMax = "Y", True, False), _
        '    '                                         IIf(sPinned = "" OrElse sPinned = "Y", True, False), _
        '    '                                         IIf(iSumType = -1, Nothing, iSumType), _
        '    '                                         IIf(iDisplayType = -1, Nothing, iDisplayType))

        '    '        'If iBackColor = -1 AndAlso iLinkObject = -1 AndAlso sWidthIsMax = "" AndAlso sPinned = "" AndAlso iSumType = -1 AndAlso iDisplayType = -1 Then
        '    '        '    oGridN.doAddListField(sFieldId, sTitle, bEditable, iWidth, sType, sUID)
        '    '        'ElseIf iBackColor > 0 And iLinkObject = -1 AndAlso sWidthIsMax = "" AndAlso sPinned = "" AndAlso iSumType = -1 AndAlso iDisplayType = -1 Then
        '    '        '    oGridN.doAddListField(sFieldId, sTitle, bEditable, iWidth, sType, sUID, iBackColor)
        '    '        'ElseIf iLinkObject > 0 AndAlso sWidthIsMax = "" AndAlso sPinned = "" AndAlso iSumType = -1 AndAlso iDisplayType = -1 Then
        '    '        '    oGridN.doAddListField(sFieldId, sTitle, bEditable, iWidth, sType, sUID, iBackColor, iLinkObject)
        '    '        'ElseIf sWidthIsMax <> "" AndAlso sPinned <> "" AndAlso iSumType = -1 AndAlso iDisplayType = -1 Then
        '    '        '    Dim bWidthIsMax As Boolean = IIf(sWidthIsMax = "Y", True, False)
        '    '        '    Dim bPinned As Boolean = IIf(sPinned = "Y", True, False)

        '    '        '    'Dim bWidthIsMax As Boolean = False
        '    '        '    'If sWidthIsMax = "Y" Then
        '    '        '    '    bWidthIsMax = True
        '    '        '    'Else
        '    '        '    '    bWidthIsMax = False
        '    '        '    'End If
        '    '        '    'Dim bPinned As Boolean = False
        '    '        '    'If sPinned = "Y" Then
        '    '        '    '    bPinned = True
        '    '        '    'Else
        '    '        '    '    bPinned = False
        '    '        '    'End If
        '    '        '    oGridN.doAddListField(sFieldId, sTitle, bEditable, iWidth, sType, sUID, iBackColor, iLinkObject, bWidthIsMax, bPinned)
        '    '        'ElseIf sWidthIsMax <> "" AndAlso sPinned <> "" AndAlso iSumType <> -1 AndAlso iDisplayType <> -1 Then
        '    '        '    Dim bWidthIsMax As Boolean = False
        '    '        '    If sWidthIsMax = "Y" Then
        '    '        '        bWidthIsMax = True
        '    '        '    Else
        '    '        '        bWidthIsMax = False
        '    '        '    End If
        '    '        '    Dim bPinned As Boolean = False
        '    '        '    If sPinned = "Y" Then
        '    '        '        bPinned = True
        '    '        '    Else
        '    '        '        bPinned = False
        '    '        '    End If
        '    '        '    oGridN.doAddListField(sFieldId, sTitle, bEditable, iWidth, sType, sUID, iBackColor, iLinkObject, bWidthIsMax, bPinned, iSumType, iDisplayType)
        '    '        'End If

        '    '        If sFieldOrderBy <> "" Then
        '    '            sCompleteOrderBy &= ", " & sFieldId & " " & sFieldOrderBy
        '    '        End If

        '    '        oRecSet.MoveNext()
        '    '    End While

        '    '    If sCompleteOrderBy.Trim <> "" Then
        '    '        sCompleteOrderBy = sCompleteOrderBy.Trim.TrimStart(",")
        '    '        oGridN.setOrderValue(sCompleteOrderBy)
        '    '    End If
        '    '    Return True
        '    'Else
        '    '    Return False
        '    'End If
        'End Function

        Protected Overridable Function doGetCapacityQuery() As String
            Dim sPerc As String = Config.Parameter("VSFULLW")
            If sPerc Is Nothing OrElse sPerc.Length = 0 Then
                sPerc = "50"
            End If
            Dim sFull, sNearly, sAvailable As String
            If Config.ParameterWithDefault("IMGPATH", "").Trim = "" Then
                sFull = IDHAddOns.idh.addon.Base.doFixImagePath("full.png")
                sNearly = IDHAddOns.idh.addon.Base.doFixImagePath("nearly.png")
                sAvailable = IDHAddOns.idh.addon.Base.doFixImagePath("available.png")

            Else
                Dim sImgPath As String = Config.ParameterWithDefault("IMGPATH", "").Trim
                If Not sImgPath.EndsWith("\") Then
                    sImgPath &= "\"
                End If
                sFull = sImgPath & ("full.png")
                sNearly = sImgPath & ("nearly.png")
                sAvailable = sImgPath & ("available.png")
            End If
            Return "CASE WHEN Total >= DayCap THEN '" & sFull & "' ELSE CASE WHEN Total > (DayCap * " & sPerc & "/100 ) THEN '" & sNearly & "' ELSE '" & sAvailable & "' END End"
        End Function

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '*** Read input params
        ' Protected Overridable Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            'Dim sParentID As String = goParent.doGetParentID(oForm.UniqueID)
            If getHasSharedData(oForm) Then
                Dim oGridN As FilterGrid
                oGridN = FilterGrid.getInstance(oForm, "LINESGRID")

                Dim iIndex As Integer
                For iIndex = 0 To oGridN.getGridControl().getFilterFields().Count - 1
                    Dim oField As com.idh.controls.strct.FilterField = oGridN.getGridControl().getFilterFields().Item(iIndex)
                    Dim sFieldName As String = oField.msFieldName
                    Dim sFieldValue As String = getParentSharedData(oForm, sFieldName)
                    Try
                        setUFValue(oForm, sFieldName, sFieldValue)
                    Catch ex As Exception
                    End Try
                Next
            End If
        End Sub

        Public Sub doAddTotalFields(ByVal oForm As SAPbouiCOM.Form)
            doAddUF(oForm, "IDH_CstWgt", SAPbouiCOM.BoDataType.dt_QUANTITY, 20, False, False)
            doAddUF(oForm, "IDH_SubAD", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_SbADCt", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_Total", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_DisAmt", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_AddEx", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_TaxAmt", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)

            doAddUF(oForm, "IDH_RdWgt", SAPbouiCOM.BoDataType.dt_QUANTITY, 20, False, False)
            doAddUF(oForm, "IDH_TAdCst", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_TAdChg", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_TxAmCo", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_TotChg", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_ChgDed", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                oForm.Title = gsTitle
                Dim iWidth As Integer = oForm.Width

                Dim oGridN As OrderRowGrid = New OrderRowGrid(Me, oForm, "LINESGRID", 7, 113, iWidth - 20, IIf(gsType = "IDHDELNK", 255, 235), "r")
                doSetFilterLayout(oForm)

                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False

                oForm.EnableMenu(Config.NAV_ADD, False)
                oForm.EnableMenu(Config.NAV_FIND, False)
                oForm.EnableMenu(Config.NAV_FIRST, False)
                oForm.EnableMenu(Config.NAV_NEXT, False)
                oForm.EnableMenu(Config.NAV_PREV, False)
                oForm.EnableMenu(Config.NAV_LAST, False)

                'Moved below block away from USAREL check. The caption can be changed by any client. 
                'Set Documents button caption on Job Managers
                Dim oItm As SAPbouiCOM.Item
                Dim oBtn As SAPbouiCOM.Button
                Try
                    oItm = oForm.Items.Item("IDH_DOCS")
                    If oItm IsNot Nothing Then
                        oBtn = oItm.Specific
                        oBtn.Caption = Config.ParameterWithDefault("BTDOCCAP", "Documents")
                    End If
                Catch ex As Exception
                End Try


                oForm.SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_All
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDLF", {Nothing})
                BubbleEvent = False
            End Try
        End Sub

        ''Need this for the auto fields linked to the search grid
        Protected Overridable Sub doSetGridFilters(ByVal oGridN As FilterGrid)
            oGridN.doAddFilterField("IDH_STATUS", "r.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PSTAT", "r.U_PStat", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            If Not (oGridN.getSBOForm.TypeEx.IndexOf("IDHJOBR") > -1 OrElse
              oGridN.getSBOForm.TypeEx.IndexOf("IDHWOBI") > -1) Then
                oGridN.doAddFilterField("IDH_PROG", "v.State", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            End If

            oGridN.doAddFilterField("IDH_REQSTF", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_REQSTT", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_ACTSTF", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_ACTSTT", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_CUST", "e.U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_NAME", "e.U_CardNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PROD", "r.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ITMGRP", "r.U_ItmGrp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_JOBTYP", "r.U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            'oGridN.doAddFilterField("IDH_ORDNO", "e.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ROWNO", "r.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ORDNO", "r.U_JobNr", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_VEHREG", "r.U_Lorry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DRIVER", "r.U_Driver", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ADDR", "e.U_Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_POSTCD", "e.U_ZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CUSPHO", "e.U_Phone1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SITETL", "e.U_SiteTl", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_REF", "r.U_CustRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WSCD", "r.U_WasCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WSDC", "r.U_WasDsc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            'Fix to avoid user ids with similar user code like davidM and davidP
            'oGridN.doAddFilterField("IDH_USER", "r.U_User", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_USER", "r.U_User", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)
            oGridN.doAddFilterField("IDH_BRANCH", "r.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)

            ''## MA Start 29-08-2014 Issue#421
            oGridN.doAddFilterField("IDH_BRANC2", "r.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            ''## MA End 29-08-2014 Issue#421

            oGridN.doAddFilterField("IDH_STEID", "e.U_SteId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_STREET", "e.U_Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_CARCD", "r.U_CarrCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CARNM", "r.U_CarrNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_STATUS", "r.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PSTAT", "r.U_PStat", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_PROG", "v.State", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'KA -- START -- 20121022
            oGridN.doAddFilterField("IDH_BPSTAT", "bp.ValidFor", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 30)
            'KA -- END -- 20121022

            'New filters on Reference numbers and Maximo No on WOR 
            oGridN.doAddFilterField("IDH_PROREF", "r.U_ProRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DSPREF", "r.U_SiteRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SUPREF", "r.U_SupRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_MAXIMO", "r.U_MaximoNum", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            '#MA START 21-04-2017 issue#394
            oGridN.doAddFilterField("IDH_CITY", "e.U_City", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            '#MA END 21-04-2017

        End Sub

        Public Overridable Sub doSetTables(ByVal oGridN As FilterGrid)
            '        	'Return "[@IDH_JOBSHD] r, [@IDH_JOBENTR] e, IDH_VPROGRESS v "
            '        	oGridN.doAddGridTable( New GridTable("@IDH_JOBSHD", "r", "Code", True), True )
            '        	oGridN.doAddGridTable( New GridTable("@IDH_JOBENTR", "e", "Code", True) )
            '        	oGridN.doAddGridTable( New GridTable("IDH_VPROGRESS", "v") )
        End Sub

        Public Overridable Function getListRequiredStr(OForm As SAPbouiCOM.Form) As String
            'Return "r.U_JobNr = e.Code And v.Code = r.Code And e.U_Status in ('1','2','3','4','5')"
            'Return "r.U_JobNr = e.Code And v.Code = r.Code "
            'KA -- START -- 20121022
            Dim sReqStr As String = ""
            If Not (OForm.TypeEx.IndexOf("IDHJOBR") > -1 OrElse
             OForm.TypeEx.IndexOf("IDHWOBI") > -1) Then
                sReqStr = "r.U_JobNr = e.Code And v.Code = r.Code AND (r." + IDH_JOBSHD._CustCd + " = bp.CardCode Or (r." + IDH_JOBSHD._CustCd + " = '' AND r." + IDH_JOBSHD._ProCd + " = bp.CardCode))"
            Else
                sReqStr = "r.U_JobNr = e.Code AND (r." + IDH_JOBSHD._CustCd + " = bp.CardCode Or (r." + IDH_JOBSHD._CustCd + " = '' AND r." + IDH_JOBSHD._ProCd + " = bp.CardCode))"
                'sReqStr = sReqStr & " AND (ad.CardCode = bp.CardCode AND ad.Address = e." & IDH_JOBENTR._Address & ")"
            End If
            'KA -- END -- 20121022

            Return sReqStr
        End Function

        Public Sub doAddAdditionalRequiredListFields(ByVal oGridN As OrderRowGrid)
            oGridN.doAddListFieldNotPinned("e." & IDH_JOBENTR._Status, "WO Status", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._Status, "Sales Status", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._PStat, "Purchase Status", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._SAINV, "Sales Invoice", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._SLPO, "Skip licence PO", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._SAORD, "Sales Order", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._GRIn, "Goods Receipt Incomming", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._TIPPO, "Tipping PO", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._JOBPO, "Haulage PO", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._ProPO, "Producer PO", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._CustGR, "Customer Goods Receipt", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._ProGRPO, "Producer Goods Receipt PO", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._SODlvNot, "Do Stock Movement", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._Jrnl, "Journal Entry", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._TPCN, "Disposal Purchase Credit Note", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._TCCN, "Customer Credit Note", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._RowSta, "Row Status", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._LnkPBI, "Linked PBI", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._WROrd, "Linked DO", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._WRRow, "Linked DO Row", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("e." & IDH_JOBENTR._Address, "Order Address", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("e." & IDH_JOBENTR._PAddress, "Producer Address", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("e." & IDH_JOBENTR._PZpCd, "Producer ZipCode", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("e." & IDH_JOBENTR._PPhone1, "Producer Phone", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("e." & IDH_JOBENTR._FirstBP, "First BP", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._ReqArch, "Request Archiving", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._DFirstBP, "First BP", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._JCost, "Total Order Cost", False, 0, Nothing, "TOrdCost")
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._VtCostAmt, "Purchase Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._RowSta, "Row Status", False, -1, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._AddCost, "Additional Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._AddCharge, "Additional Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._ValDed, "Charge Deduction", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("e." & IDH_JOBENTR._PCardCd, "Producer", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("e." & IDH_JOBENTR._SCardCd, "Disposal Site", False, 0, Nothing, Nothing)
            oGridN.doFinalize()
        End Sub

        Public Overridable Sub doHideTotals(ByVal oForm As SAPbouiCOM.Form)
            ''MA Start 03-11-2014 Issue#417 Issue#418
            If (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) = False) OrElse (Config.INSTANCE.getParameterAsBool("HDTOTALS", False) = True) Then
                Dim oItem As SAPbouiCOM.Item
                Dim iBlankColor As Integer = 14930874
                For Each sItem As String In moTotalFields_Cost
                    oItem = oForm.Items.Item(sItem)
                    oItem.ForeColor = iBlankColor
                    oItem.BackColor = iBlankColor
                Next
            End If
            If (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) = False) OrElse (Config.INSTANCE.getParameterAsBool("HDTOTALS", False) = True) Then
                Dim oItem As SAPbouiCOM.Item
                Dim iBlankColor As Integer = 14930874
                For Each sItem As String In moTotalFields_Charge
                    oItem = oForm.Items.Item(sItem)
                    oItem.ForeColor = iBlankColor
                    oItem.BackColor = iBlankColor
                Next
            End If
            ''MA End 03-11-2014 Issue#417 Issue#418

            'If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) Then
            '    doSetVisible(oForm, True, moTotalFields)
            'Else
            '    doSetVisible(oForm, False, moTotalFields)
            'End If
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            doHideTotals(oForm)
            doAddWF(oForm, "SUPPLIERS", Nothing)
            doAddWF(oForm, "LASTJOBMENU", Nothing)
            '            doAddWF(oForm, "CLOSEDWOH", Nothing)

            oForm.Top = goParent.goApplication.Desktop.Height() - (oForm.Height + 130)
            oForm.Left = 200

            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New OrderRowGrid(Me, oForm, "LINESGRID", "r")
            End If

            'Required tables
            oGridN.doAddGridTable(New GridTable("@IDH_JOBSHD", "r", "Code", True, True), True)
            If Not (oGridN.getSBOForm.TypeEx.IndexOf("IDHJOBR") > -1 OrElse
               oGridN.getSBOForm.TypeEx.IndexOf("IDHWOBI") > -1) Then
                oGridN.doAddGridTable(New GridTable("IDH_VPROGRESS", "v"))
            End If
            'KA -- START -- 20121022
            oGridN.doAddGridTable(New GridTable("OCRD", "bp", Nothing, False, True))
            'KA -- END -- 20121022

            oGridN.doAddGridTable(New GridTable("@IDH_JOBENTR", "e", "Code", True, True))
            'oGridN.doAddGridJoin(New GridJoin("CRD1", com.idh.bridge.data.GridJoin.TYPE_JOIN.LEFT, "ad", "CardCode", "e", "U_CardCd", True))
            '#MA Start 17-05-2017 Testing point402
            ' oGridN.doAddGridJoin(New GridJoin("CRD1", com.idh.bridge.data.GridJoin.TYPE_JOIN.LEFT, "ad", "CardCode", "e.U_Address = ad.Address AND e.U_CardCd = ad.CardCode And ad.AdresType='S'", True))
            '#MA End 17-05-2017
            doSetGridFilters(oGridN)
            'Required Filters
            'If gsFormSRF.Equals("Job Manager.srf") Then
            oGridN.doAddFilterField("IDH_ROWSTA", "r.U_RowSta", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_HDSTA", "e.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 30)

            '##MA Start 11-04-2017 issue# 377  Add New filters to the OSMs and Billing managers for the waste material item group
            oGridN.doAddFilterField("IDH_ITMGP", "r.U_WasItmGrp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30)
            '##MA End 11-04-2017

            'End If

            doSetTables(oGridN)
            'oGridN.setKeyField("r.Code")

            'oGridN.setOrderValue("r.U_JobTp, v.State, r.U_RDate, r.U_RTime, CAST(r.Code As NUMERIC)")
            oGridN.setRequiredFilter(getListRequiredStr(oForm))
            oGridN.doSetDoCount(True)
            oGridN.doSetBlankLine(False)

            'Set the required List Fields
            doTheGridLayout(oGridN)
            'doSetListFields(oGridN)
            'doAddAdditionalRequiredListFields(oGridN)
            ''MA Start 03-11-2014

            'Now Hide the fields as per authorization 
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) = False OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse
                 Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Cost_PRICES) = False OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) = False Then
                Dim oListField As com.idh.controls.strct.ListField
                For Each sField As String In IDH_JOBSHD.PriceFields_Cost
                    oListField = oGridN.getListfields().getListField("r." & sField)
                    If Not oListField Is Nothing Then
                        oListField.miWidth = 0
                    End If
                Next
                'None DB Field
                'oListField = oGridN.getListfields().getListField("SubAftDisc")
                'If Not oListField Is Nothing Then
                '    oListField.miWidth = 0
                'End If
            End If
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) = False OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse
                 Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Charge_PRICES) = False OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) = False Then
                Dim oListField As com.idh.controls.strct.ListField
                For Each sField As String In IDH_JOBSHD.PriceFields_Charge
                    oListField = oGridN.getListfields().getListField("r." & sField)
                    If Not oListField Is Nothing Then
                        oListField.miWidth = 0
                    End If
                Next
                'None DB Field
                oListField = oGridN.getListfields().getListField("SubAftDisc")
                If Not oListField Is Nothing Then
                    oListField.miWidth = 0
                End If
            End If
            ''MA End 03-11-2014

            doBranchCombo(oForm)
            'KA -- START -- 20121022
            doBPStatusCombo(oForm)
            'KA -- END -- 20121022
            '##MA Start 11-04-2017
            doItemGroupCombo(oForm)
            '##MA END 11-04-2017
            'If gsFormSRF.Equals("Job Manager.srf") Then
            doGetStatus(oForm)
            doRowStatusCombo(oForm)
            'End If
            'MA Start 20-11-2014

            Dim bHasParams As Boolean = False
            'MA End 20-11-2014
            If getHasSharedData(oForm) Then
                doReadInputParams(oForm)
                'MA Start 20-11-2014
                bHasParams = True
                'MA End 20-11-2014

                Dim sSuppliers As String = getParentSharedData(oForm, "SUPPLIERS")
                If Not sSuppliers Is Nothing AndAlso sSuppliers.Length > 0 Then
                    Dim sExtraReqFilter As String = oGridN.getRequiredFilter()

                    If sExtraReqFilter.Length > 0 Then
                        sExtraReqFilter = sExtraReqFilter & " AND "
                    End If

                    '20151026: Below block is using = operator causing wrong results returned 
                    'Also have included the U_CustCd in the where clause to get similar results when open OSM directly as compared to 
                    'when open the OSM from BP form 
                    'sExtraReqFilter = sExtraReqFilter & " (e.U_CCardCd = '" & sSuppliers & "' OR e.U_PCardCd = '" & sSuppliers & "' Or e.U_SCardCd = '" & sSuppliers & "') "

                    sExtraReqFilter = sExtraReqFilter & " (r.U_CustCd LIKE '" & sSuppliers & "%' OR e.U_CCardCd LIKE '" & sSuppliers & "%' OR e.U_PCardCd LIKE '" & sSuppliers & "%' Or e.U_SCardCd LIKE '" & sSuppliers & "%') "

                    oGridN.setRequiredFilter(sExtraReqFilter)
                End If
            Else
                setUFValue(oForm, "IDH_STATUS", Config.Parameter("OSMSOS"))
                setUFValue(oForm, "IDH_PSTAT", Config.Parameter("OSMPOS"))
                setUFValue(oForm, "IDH_PROG", Config.Parameter("OSMPRO"))
            End If

            Dim sDefStatus As String = Config.Parameter("OSMDFSTA")
            If Not sDefStatus Is Nothing AndAlso sDefStatus.Length > 0 Then
                setUFValue(oForm, "IDH_HDSTA", sDefStatus)
            Else
                setUFValue(oForm, "IDH_HDSTA", "1,2,3,4,5")
            End If
            'If gsFormSRF.Equals("Job Manager.srf") Then
            setUFValue(oForm, "IDH_ROWSTA", com.idh.bridge.lookups.FixedValues.getStatusOpen())
            'End If
            'MA Start 20-11-2014
            If bHasParams = False Then
                oGridN.setInitialFilterValue("r.U_JobNr = '-100'")
            End If
            'MA End 20-11-2014

            Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_ROWSTA")
            '## Start 29-07-2013
            If oItem.Visible = True AndAlso oForm.TypeEx <> "IDHWOBIL5" Then
                '## End
                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_DELETE) Then
                    oItem.Left = 585

                    oItem = oForm.Items.Item("IDH_HDSTA")
                    oItem.Width = 39
                Else
                    oItem.Left = -100

                    oItem = oForm.Items.Item("IDH_HDSTA")
                    oItem.Width = 80

                End If
            End If
            'oGridN.doFinalize()

            'Set User and Branch field values and status according to WR Config keys and current User logged in 
            doSetUserAndBranchFields(oForm)

        End Sub

        '*
        ' Before using this ensure that the required Form Fields exists
        ' Using the form to get the grid with an ID of 'LINESGRID'
        '*
        Protected Sub doGridTotals(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoZero As Boolean = False)
            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            doGridTotals(oGridN, bDoZero)
        End Sub
        '*
        ' Before using this ensure that the required Form Fields exists
        '*
        Protected Sub doGridTotals(ByVal oGridN As OrderRowGrid, Optional ByVal bDoZero As Boolean = False)
            ''MA 05-01-2015 We will not calculate totals
            If oGridN.getSBOForm.TypeEx = "IDHCONMWO" OrElse oGridN.getSBOForm.TypeEx = "IDHCONMDO" OrElse (oGridN.getSBOForm.TypeEx.IndexOf("IDHJOBR") > -1 AndAlso com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("TOTALOSM", "TRUE").Trim.ToUpper <> "TRUE") OrElse
                (oGridN.getSBOForm.TypeEx.IndexOf("IDHWOBI") > -1 AndAlso com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("TOTALBM", "TRUE").Trim.ToUpper <> "TRUE") Then
                Exit Sub
            End If
            'MA 05-01-2015 end change

            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
                Dim oTotals As RowGridTotals

                ''New
                ''## MA test 27-08-2014 
                'Dim sTime As DateTime = Now
                If com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("TOTALTYP", "OLD").Trim.ToUpper = "OLD" Then
                    oTotals = oGridN.doTotals(bDoZero)
                Else
                    oTotals = oGridN.doTotalsNew(bDoZero)
                End If
                'Dim eTime As DateTime = Now
                'PARENT.APPLICATION.StatusBar.SetText("Milliseconds to load Totals by method (" & com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("TOTALTYP", "OLD") & "): " & eTime.Subtract(sTime).TotalMilliseconds.ToString, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                ''## MA test 27-08-2014
                ''Current
                'Dim sTime2 As DateTime = Now
                'oTotals = oGridN.doTotals(False)
                'Dim eTime2 As DateTime = Now
                'PARENT.APPLICATION.StatusBar.SetText("By Iteration of grid(current):" & eTime2.Subtract(sTime2).TotalMilliseconds.ToString, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                ''MA Start 30-10-2014 Issue#417 Issue#418
                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) Then
                    'Do the Totals
                    setUFValue(oGridN.getSBOForm(), "IDH_SbADCt", oTotals.CostTotal)
                    setUFValue(oGridN.getSBOForm(), "IDH_TAdCst", oTotals.AdditionalCostTotal)
                    setUFValue(oGridN.getSBOForm(), "IDH_TxAmCo", oTotals.TxAmtCostTotal)
                    setUFValue(oGridN.getSBOForm(), "IDH_TotChg", oTotals.JCostTotal)

                    setUFValue(oGridN.getSBOForm(), "IDH_AddEx", oTotals.AddEx)

                End If
                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
                    setUFValue(oGridN.getSBOForm(), "IDH_SubAD", oTotals.AftDisc)
                    setUFValue(oGridN.getSBOForm(), "IDH_TAdChg", oTotals.AdditionalChargeTotal)
                    setUFValue(oGridN.getSBOForm(), "IDH_ChgDed", oTotals.ChargeDed)
                    setUFValue(oGridN.getSBOForm(), "IDH_TaxAmt", oTotals.TxAmt)
                    setUFValue(oGridN.getSBOForm(), "IDH_Total", oTotals.Total)

                    setUFValue(oGridN.getSBOForm(), "IDH_DisAmt", oTotals.DisAmt)

                End If
                setUFValue(oGridN.getSBOForm(), "IDH_CstWgt", oTotals.CstWgt)
                setUFValue(oGridN.getSBOForm(), "IDH_RdWgt", oTotals.RdWgt)
                ''MA End 30-10-2014 Issue#417 Issue#418
            End If
        End Sub

        Private Sub doBranchCombo(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks", Nothing, Nothing, True)
        End Sub

        Protected Overridable Sub doItemGroupCombo(ByRef oForm As SAPbouiCOM.Form)
            Dim oRecordSet As DataRecords
            Dim oItmGrp As SAPbouiCOM.ComboBox
            'oItmGrp = oForm.Items.Item("IDH_ITMGP").Specific
            'oItmGrp.Item.DisplayDesc = True
            Dim iCount As Integer
            Dim oVal1 As String
            Dim oVal2 As String
            Try
                oItmGrp = oForm.Items.Item("IDH_ITMGP").Specific
                oItmGrp.Item.DisplayDesc = True
                'Dim sQry As String = "SELECT ItmsGrpCod,ItmsGrpNam FROM OITB WITH(NOLOCK) WHERE ItmsGrpCod IN (SELECT DISTINCT U_WasItmGrp from [@IDH_JOBSHD] WITH (NOLOCK)) ORDER BY (ItmsGrpNam) ASC"
                Dim sQry As String = "SELECT ItmsGrpCod,ItmsGrpNam FROM OITB WITH(NOLOCK)  ORDER BY (ItmsGrpNam) ASC"
                oRecordSet = DataHandler.INSTANCE.doBufferedSelectQuery("WasteItemGrop", sQry)
                If oRecordSet.RecordCount > 0 Then
                    doClearValidValues(oItmGrp.ValidValues)
                    oItmGrp.ValidValues.Add("", "Any")
                    For iCount = 0 To oRecordSet.RecordCount - 1
                        oRecordSet.gotoRow(iCount)
                        oVal1 = oRecordSet.getValueAsString(0)
                        oVal2 = oRecordSet.getValueAsString(1)
                        Try
                            oItmGrp.ValidValues.Add(oVal1, oVal2)
                        Catch ex As Exception
                        End Try
                    Next
                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {"doItemGroupCombo"})
            End Try
        End Sub

        Private Sub doRowStatusCombo(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oRowStaCombo As SAPbouiCOM.ComboBox
                oRowStaCombo = CType(oForm.Items.Item("IDH_ROWSTA").Specific, SAPbouiCOM.ComboBox)
                oRowStaCombo.ValidValues.Add("", getTranslatedWord("All"))
                oRowStaCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getStatusOpen(), com.idh.bridge.lookups.FixedValues.getStatusOpen())
                oRowStaCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getStatusDeleted(), com.idh.bridge.lookups.FixedValues.getStatusDeleted())
            Catch ex As Exception
            End Try
        End Sub
        'KA -- START -- 20121022
        Private Sub doBPStatusCombo(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oBPStaCombo As SAPbouiCOM.ComboBox
                oBPStaCombo = CType(oForm.Items.Item("IDH_BPSTAT").Specific, SAPbouiCOM.ComboBox)
                oBPStaCombo.ValidValues.Add("", getTranslatedWord("All"))
                oBPStaCombo.ValidValues.Add("Y", getTranslatedWord("Active"))
                oBPStaCombo.ValidValues.Add("N", getTranslatedWord("Inactive"))
            Catch ex As Exception
            End Try
        End Sub
        'KA -- END -- 20121022


        Public Overridable Sub doGetStatus(ByVal oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_DELETE) Then
                Try
                    Dim oCombo As SAPbouiCOM.ComboBox
                    oCombo = CType(oForm.Items.Item("IDH_HDSTA").Specific, SAPbouiCOM.ComboBox)

                    doFillCombo(oForm, "IDH_HDSTA", "[@IDH_WRSTATUS]", "Code", "Name", Nothing, "CAST(Code As Numeric)", getTranslatedWord("Default"), "1,2,3,4,5")
                Catch ex As Exception
                End Try
            Else
                Try
                    Dim oItem As SAPbouiCOM.Item
                    oItem = oForm.Items.Item("IDH_HDSTA")
                    Dim oCombo As SAPbouiCOM.ComboBox
                    oCombo = CType(oItem.Specific(), SAPbouiCOM.ComboBox)

                    Dim oValidValues As SAPbouiCOM.ValidValues
                    oValidValues = oCombo.ValidValues
                    doClearValidValues(oValidValues)

                    oValidValues.Add("1,2,3,4,5", com.idh.bridge.lookups.FixedValues.getStatusOpen())
                    oValidValues.Add("9", com.idh.bridge.lookups.FixedValues.getStatusClosed())
                Catch ex As Exception
                End Try
            End If
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            doReLoadData(oForm, Not getHasSharedData(oForm))
        End Sub

        Protected Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            Try
                Dim sFormType As String = getParentSharedData(oForm, "FTYPE")
                Dim bIsSearch As Boolean = False
                If Not sFormType Is Nothing AndAlso sFormType.Equals("SEARCH") = True Then
                    bIsSearch = True
                End If

                If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Find")
                    If bIsSearch Then
                        oForm.Items.Item("2").Visible = True
                        oForm.Items.Item("IDH_CHOOSE").Visible = True
                    Else
                        oForm.Items.Item("2").Visible = False
                        oForm.Items.Item("IDH_CHOOSE").Visible = False
                    End If
                ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Update")
                    oForm.Items.Item("2").Visible = True
                    oForm.Items.Item("IDH_CHOOSE").Visible = False
                End If
                'oForm.Mode = iMode
            Catch ex As Exception
            End Try
        End Sub

        '** The Initializer
        Protected Overridable Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean)
            Try
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

                Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                'If bIsFirst = False Then
                '    oGridN.getGridControl().doCreateFilterString()
                '    If oGridN.doCheckForQueryChange() = False Then
                '        Return
                '    End If
                'End If

                ''## MA test 27-08-2014
                ''Dim sTime As DateTime = Now
                Dim sFilter As String = oGridN.getRequiredFilter()
                doSetStatusFilter(oGridN)
                doUpdateStatuses(oGridN)
                doSetFormSpecificFilters(oGridN)
                oGridN.doReloadData()
                oGridN.setRequiredFilter(sFilter)
                ''Dim eTime As DateTime = Now
                ''PARENT.APPLICATION.StatusBar.SetText("Milliseconds to load Grid by Use of IDH_VPROGRESS=" & com.idh.bridge.lookups.Config.INSTANCE.getParameter("USEVIEW") & ": " & eTime.Subtract(sTime).TotalMilliseconds.ToString, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                ''## MA test 27-08-2014

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error reloading the Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", {Nothing})
            End Try
        End Sub
        Protected Overridable Sub doUpdateStatuses(oGridN As OrderRowGrid)
            Try
                If Config.ParameterWithDefault("TRNSTATS", "FALSE").Trim = True Then
                    Dim oFields As ListFields = oGridN.getGridControl().getListFields()
                    Dim oTField As ListField
                    For i As Int32 = 0 To oFields.Count - 1
                        oTField = oFields.Item(i)
                        If oTField.msFieldTitle = Translation.getTranslatedWord("Progress") Then

                            oTField.msFieldId = oTField.msFieldId.Replace("'Closed'", "'" & com.idh.bridge.lookups.FixedValues.getProgressClosed(True) & "'").Replace("'Late'", "'" & com.idh.bridge.lookups.FixedValues.getProgressLate(True) & "'").Replace("'Started'", "'" & com.idh.bridge.lookups.FixedValues.getProgressBusy(True) & "'").Replace("'Complete'", "'" & com.idh.bridge.lookups.FixedValues.getProgressDone(True) & "'").Replace("'Waiting'", "'" & com.idh.bridge.lookups.FixedValues.getProgressWaiting(True) & "'").Replace("'No Request'", "'" & com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True) & "'").Replace("'DN'", "'" & com.idh.bridge.lookups.FixedValues.getProgressDontKnow(True) & "'").Replace("'Cancelled'", "'" & com.idh.bridge.lookups.FixedValues.getProgressCancelled(True) & "'").Replace("'DFT'", "'" & com.idh.bridge.lookups.FixedValues.getProgressDFT(True) & "'").Replace("'Assigned'", "'" & com.idh.bridge.lookups.FixedValues.getProgressAssigned(True) & "'")
                            oTField.msFieldName = oTField.msFieldId.Replace("'Closed'", "'" & com.idh.bridge.lookups.FixedValues.getProgressClosed(True) & "'").Replace("'Late'", "'" & com.idh.bridge.lookups.FixedValues.getProgressLate(True) & "'").Replace("'Started'", "'" & com.idh.bridge.lookups.FixedValues.getProgressBusy(True) & "'").Replace("'Complete'", "'" & com.idh.bridge.lookups.FixedValues.getProgressDone(True) & "'").Replace("'Waiting'", "'" & com.idh.bridge.lookups.FixedValues.getProgressWaiting(True) & "'").Replace("'No Request'", "'" & com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True) & "'").Replace("'DN'", "'" & com.idh.bridge.lookups.FixedValues.getProgressDontKnow(True) & "'").Replace("'Cancelled'", "'" & com.idh.bridge.lookups.FixedValues.getProgressCancelled(True) & "'").Replace("'DFT'", "'" & com.idh.bridge.lookups.FixedValues.getProgressDFT(True) & "'").Replace("'Assigned'", "'" & com.idh.bridge.lookups.FixedValues.getProgressAssigned(True) & "'")
                            oGridN.getGridControl().ListFlds.Item(i) = oTField
                            Exit For
                        End If
                    Next
                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", {Nothing})
            End Try
        End Sub
        Protected Overridable Sub doSetStatusFilter(oGridN As OrderRowGrid)
            Dim oForm As SAPbouiCOM.Form = oGridN.getSBOForm
            If (oGridN.getSBOForm.TypeEx.IndexOf("IDHJOBR") > -1 OrElse
                               oGridN.getSBOForm.TypeEx.IndexOf("IDHWOBI") > -1) Then
                If getItemValue(oForm, "IDH_PROG").ToString.Trim <> "" Then
                    Dim sStatusQry As String = doGetStatusFilterQueryDynamic(getItemValue(oForm, "IDH_PROG").ToString.Trim.ToUpper)
                    If sStatusQry <> "" Then
                        If oGridN.getRequiredFilter().Trim <> "" Then
                            oGridN.setRequiredFilter(oGridN.getRequiredFilter() & " And " & sStatusQry)
                        Else
                            oGridN.setRequiredFilter(sStatusQry)
                        End If
                        'setSharedData(oForm, "RequredFilter", oGridN.getRequiredFilter())

                        'oGridN.doAddFilterField("IDH_PROG", sStatusQry, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)
                        'oGridN.doFilterAsYouType("IDH_PROG")

                        Exit Sub
                    End If
                End If
                'oGridN.doFilterAsYouType()
                'oGridN.doReloadSetExt()
                'oGridN.setRequiredFilter(getListRequiredStr(oForm))
            End If
        End Sub
        Private Function getQueryForClosedStatus(ByVal UseNew As Boolean) As String
            If UseNew Then
                Dim varname1 As New System.Text.StringBuilder
                varname1.Append("(e.U_Status = 9 AND e.Code = r.U_JobNr AND r.U_Driver != 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
                Return varname1.ToString
            Else
                Return "(e.U_Status = 9 )"
            End If
        End Function
        Private Function getQueryForDFTStatus() As String
            Dim varname1 As New System.Text.StringBuilder
            varname1.Append("(((e.U_Status = 9 AND e.Code = r.U_JobNr AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='')  AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            varname1.Append("		OR " & vbCrLf)
            varname1.Append("	( (e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60  AND U_ASDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            varname1.Append("	)) " & vbCrLf)
            varname1.Append("	OR (e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' '  + STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60  AND U_ASDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            varname1.Append("	or (e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            varname1.Append("	or (e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)))")
            Return varname1.ToString
        End Function
        Private Function getQueryForLateStatus(ByVal UseNew As Boolean) As String
            If UseNew Then
                Dim varname1 As New System.Text.StringBuilder
                varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 AND U_ASDate IS NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
                Return varname1.ToString
            Else
                Return "(e.U_Status != 9 AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffLateCheck3() & " " _
                        & " 	AND r.U_ASDate IS NULL )"
            End If
        End Function
        Private Function getQueryForNoRequestStatus(UseNew As Boolean) As String
            If UseNew Then
                Dim varname1 As New System.Text.StringBuilder
                varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='')  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
                Return varname1.ToString
            Else
                Return "(e.U_Status != 9 " _
                         & " AND U_RDate IS NULL )"
            End If
        End Function

        Private Function getQueryForAssignedStatus() As String
            Dim varname1 As New System.Text.StringBuilder
            varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')  )")
            Return varname1.ToString
        End Function
        Private Function getQueryForDoneStatus(ByVal UseNew As Boolean) As String
            If UseNew Then
                Dim varname1 As New System.Text.StringBuilder
                varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')  )")
                Return varname1.ToString
            Else
                Return "(e.U_Status != 9 " _
                       & " AND U_ASDate IS NOT NULL " _
                       & " AND U_AEDate IS NOT NULL )"
            End If

        End Function
        Private Function getQueryForCancelled() As String
            Dim varname1 As New System.Text.StringBuilder
            varname1.Append("((e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL AND r.U_Driver = 'Y'  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            varname1.Append("		or " & vbCrLf)
            varname1.Append("	(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60 AND U_ASDate IS NULL  AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            varname1.Append("		or " & vbCrLf)
            varname1.Append("		(e.U_Status = 9 AND e.Code = r.U_JobNr AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='')  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            varname1.Append("		or " & vbCrLf)
            varname1.Append("		(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' '  + STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 AND U_ASDate IS NULL  AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            varname1.Append("		or " & vbCrLf)
            varname1.Append("		(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))		or (e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL AND r.U_Driver = 'Y'  AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) ) " & vbCrLf)
            varname1.Append("		or " & vbCrLf)
            varname1.Append("		(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL AND r.U_Driver = 'Y'  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') ))")
            Return varname1.ToString

        End Function
        Private Function getQueryForWaiting(ByVal UseNew As Boolean) As String
            If UseNew Then
                Dim varname1 As New System.Text.StringBuilder
                varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60  AND U_ASDate IS NULL AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
                Return varname1.ToString
            Else
                Return "(e.U_Status != 9 " _
                      & " AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffWaitCheck3() & "  " _
                      & " AND U_ASDate IS NULL )"
            End If

        End Function
        Private Function getQueryForBusyStatus(ByVal UseNew As Boolean) As String
            If UseNew Then

            Else
                Return "(e.U_Status != 9 " _
                        & " AND r.U_ASDate IS NOT NULL " _
                        & " AND r.U_AEDate IS NULL )"
            End If
            Return ""
        End Function

        Protected Function doGetStatusFilterQuery(ByVal sStatus As String) As String
            If Config.ParameterAsBool("USENWSTS", False) Then ''status filter for IIS
                If sStatus.IndexOf("*") > 0 Then
                    sStatus = sStatus.Substring(0, sStatus.IndexOf("*"))
                    Dim bAddNot As Boolean = False
                    If sStatus.StartsWith("!") Then
                        bAddNot = True
                        sStatus = sStatus.Replace("!", "")
                    End If
                    Dim sQuery As String = ""
                    If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForClosedStatus(True)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressDFT(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForDFTStatus()
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForLateStatus(True)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForNoRequestStatus(True)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressAssigned(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForAssignedStatus()
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForDoneStatus(True)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressCancelled(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForCancelled()
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForWaiting(True)
                    Else
                        Return " 1=0 "
                    End If
                    If bAddNot Then
                        Return "Not (" & sQuery & ")"
                    Else
                        Return sQuery
                    End If
                ElseIf sStatus.IndexOf("*") = 0 Then
                    sStatus = sStatus.Replace("*", "")
                    If sStatus.Trim = "" Then
                        Return ""
                    End If
                    Dim bAddNot As Boolean = False
                    If sStatus.StartsWith("!") Then
                        bAddNot = True
                        sStatus = sStatus.Replace("!", "")
                    End If
                    Dim sQuery As String = ""
                    If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForClosedStatus(True)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressDFT(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForDFTStatus()
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForLateStatus(True)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForNoRequestStatus(True)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressAssigned(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForAssignedStatus()
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForDoneStatus(True)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressCancelled(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForCancelled()
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForWaiting(True)
                    Else
                        Return " 1=0 "
                    End If
                    If bAddNot Then
                        Return "Not (" & sQuery & ")"
                    Else
                        Return sQuery
                    End If
                Else
                    Dim bAddNot As Boolean = False
                    Dim sQuery As String = ""
                    If sStatus.StartsWith("!") Then
                        bAddNot = True
                        sStatus = sStatus.Replace("!", "")
                    End If
                    If sStatus = "" Then
                        Return ""
                    End If
                    Dim aStatus As String() = sStatus.Split(","c)
                    For i As Int16 = 0 To aStatus.Length - 1
                        If aStatus(i).Trim <> "" Then
                            If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForClosedStatus(True)
                            ElseIf com.idh.bridge.lookups.FixedValues.getProgressDFT(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForDFTStatus()
                            ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForLateStatus(True)
                            ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForNoRequestStatus(True)
                            ElseIf com.idh.bridge.lookups.FixedValues.getProgressAssigned(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForAssignedStatus()
                            ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForDoneStatus(True)
                            ElseIf com.idh.bridge.lookups.FixedValues.getProgressCancelled(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForCancelled()
                            ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForWaiting(True)
                            Else
                                Return " 1=0 "
                            End If
                            aStatus(i) = sQuery
                        End If
                    Next
                    sQuery = ""
                    For i As Int16 = 0 To aStatus.Length - 2
                        If aStatus(i).Trim <> "" Then
                            sQuery &= " (" & aStatus(i).Trim & ") OR"
                        End If
                    Next
                    If aStatus(aStatus.Length - 1).Trim <> "" Then
                        sQuery &= " (" & aStatus(aStatus.Length - 1).Trim & ")"
                    End If
                    sQuery = sQuery.Trim '.TrimEnd("OR")
                    If sQuery.EndsWith("OR") Then
                        sQuery = sQuery.Substring(0, sQuery.Length - 2)
                    End If
                    If sQuery = "" Then
                        Return " 1=0 "
                    End If
                    If bAddNot Then
                        Return "Not (" & sQuery & ")"
                    Else
                        Return sQuery
                    End If
                End If
            Else ''default status filter
                If sStatus.IndexOf("*") > 0 Then
                    sStatus = sStatus.Substring(0, sStatus.IndexOf("*"))
                    Dim bAddNot As Boolean = False
                    If sStatus.StartsWith("!") Then
                        bAddNot = True
                        sStatus = sStatus.Replace("!", "")
                    End If
                    Dim sQuery As String = ""
                    If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForClosedStatus(False)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForLateStatus(False)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForNoRequestStatus(False)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressBusy(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForBusyStatus(False)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForDoneStatus(False)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.StartsWith(sStatus) Then
                        sQuery = getQueryForWaiting(False)
                    Else
                        Return " 1=0 "
                    End If
                    If bAddNot Then
                        Return "Not (" & sQuery & ")"
                    Else
                        Return sQuery
                    End If
                ElseIf sStatus.IndexOf("*") = 0 Then
                    sStatus = sStatus.Replace("*", "")
                    If sStatus.Trim = "" Then
                        Return ""
                    End If
                    Dim bAddNot As Boolean = False
                    If sStatus.StartsWith("!") Then
                        bAddNot = True
                        sStatus = sStatus.Replace("!", "")
                    End If
                    Dim sQuery As String = ""

                    If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForClosedStatus(False)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForLateStatus(False)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForNoRequestStatus(False)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressBusy(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForBusyStatus(False)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForDoneStatus(False)
                    ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = getQueryForWaiting(False)
                    Else
                        Return " 1=0 "
                    End If

                    If bAddNot Then
                        Return "Not (" & sQuery & ")"
                    Else
                        Return sQuery
                    End If
                Else
                    Dim bAddNot As Boolean = False
                    Dim sQuery As String = ""
                    If sStatus.StartsWith("!") Then
                        bAddNot = True
                        sStatus = sStatus.Replace("!", "")
                    End If
                    If sStatus = "" Then
                        Return ""
                    End If

                    Dim aStatus As String() = sStatus.Split(","c)
                    For i As Int16 = 0 To aStatus.Length - 1
                        If aStatus(i).Trim <> "" Then
                            If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForClosedStatus(False)
                            ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForLateStatus(False)
                            ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForNoRequestStatus(False)
                            ElseIf com.idh.bridge.lookups.FixedValues.getProgressBusy(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForBusyStatus(False)
                            ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForDoneStatus(False)
                            ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = getQueryForWaiting(False)
                            Else
                                Return " 1=0 "
                            End If
                            aStatus(i) = sQuery
                        End If
                    Next
                    sQuery = ""
                    For i As Int16 = 0 To aStatus.Length - 2
                        If aStatus(i).Trim <> "" Then
                            sQuery &= " (" & aStatus(i).Trim & ") OR"
                        End If
                    Next
                    If aStatus(aStatus.Length - 1).Trim <> "" Then
                        sQuery &= " (" & aStatus(aStatus.Length - 1).Trim & ")"
                    End If
                    sQuery = sQuery.Trim '.TrimEnd("OR")
                    If sQuery.EndsWith("OR") Then
                        sQuery = sQuery.Substring(0, sQuery.Length - 2)
                    End If
                    If sQuery = "" Then
                        Return " 1=0 "
                    End If
                    If bAddNot Then
                        Return "Not (" & sQuery & ")"
                    Else
                        Return sQuery
                    End If
                End If
            End If
            Return ""

            'If Config.ParameterAsBool("USENWSTS", False) Then ''status filter for IIS
            '    Dim varname1 As System.Text.StringBuilder
            '    If sStatus.IndexOf("*") > 0 Then
            '        sStatus = sStatus.Substring(0, sStatus.IndexOf("*"))
            '        If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.StartsWith(sStatus) Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(e.U_Status = 9 AND e.Code = r.U_JobNr AND r.U_Driver != 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
            '            Return varname1.ToString()

            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressDFT(True).ToUpper.StartsWith(sStatus) Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(((e.U_Status = 9 AND e.Code = r.U_JobNr AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='')  AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            '            varname1.Append("		OR " & vbCrLf)
            '            varname1.Append("	( (e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60  AND U_ASDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            '            varname1.Append("	)) " & vbCrLf)
            '            varname1.Append("	OR (e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' '  + STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60  AND U_ASDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            '            varname1.Append("	or (e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            '            varname1.Append("	or (e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)))")
            '            Return varname1.ToString()

            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.StartsWith(sStatus) Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 AND U_ASDate IS NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
            '            Return varname1.ToString()

            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.StartsWith(sStatus) Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='')  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
            '            Return varname1.ToString()

            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressAssigned(True).ToUpper.StartsWith(sStatus) Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')  )")
            '            Return varname1.ToString()


            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.StartsWith(sStatus) Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')  )")
            '            Return varname1.ToString()

            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressCancelled(True).ToUpper.StartsWith(sStatus) Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("((e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL AND r.U_Driver = 'Y'  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            '            varname1.Append("		or " & vbCrLf)
            '            varname1.Append("	(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60 AND U_ASDate IS NULL  AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            '            varname1.Append("		or " & vbCrLf)
            '            varname1.Append("		(e.U_Status = 9 AND e.Code = r.U_JobNr AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='')  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            '            varname1.Append("		or " & vbCrLf)
            '            varname1.Append("		(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' '  + STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 AND U_ASDate IS NULL  AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            '            varname1.Append("		or " & vbCrLf)
            '            varname1.Append("		(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))		or (e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL AND r.U_Driver = 'Y'  AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) ) " & vbCrLf)
            '            varname1.Append("		or " & vbCrLf)
            '            varname1.Append("		(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL AND r.U_Driver = 'Y'  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') ))")
            '            Return varname1.ToString()

            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.StartsWith(sStatus) Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60  AND U_ASDate IS NULL AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
            '            Return varname1.ToString()
            '        Else
            '            Return " 1=0 "
            '        End If
            '    ElseIf sStatus.IndexOf("*") <= 0 Then
            '        sStatus = sStatus.Replace("*", "")
            '        If sStatus.Trim = "" Then
            '            Return ""
            '        End If
            '        If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(e.U_Status = 9 AND e.Code = r.U_JobNr AND r.U_Driver != 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
            '            Return varname1.ToString()

            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressDFT(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(((e.U_Status = 9 AND e.Code = r.U_JobNr AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='')  AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            '            varname1.Append("		OR " & vbCrLf)
            '            varname1.Append("	( (e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60  AND U_ASDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            '            varname1.Append("	)) " & vbCrLf)
            '            varname1.Append("	OR (e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' '  + STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60  AND U_ASDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            '            varname1.Append("	or (e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            '            varname1.Append("	or (e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)))")
            '            Return varname1.ToString()

            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 AND U_ASDate IS NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
            '            Return varname1.ToString()

            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='')  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
            '            Return varname1.ToString()

            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressAssigned(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')  )")
            '            Return varname1.ToString()


            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')  )")
            '            Return varname1.ToString()

            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressCancelled(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("((e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL AND r.U_Driver = 'Y'  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            '            varname1.Append("		or " & vbCrLf)
            '            varname1.Append("	(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60 AND U_ASDate IS NULL  AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            '            varname1.Append("		or " & vbCrLf)
            '            varname1.Append("		(e.U_Status = 9 AND e.Code = r.U_JobNr AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='')  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            '            varname1.Append("		or " & vbCrLf)
            '            varname1.Append("		(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' '  + STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 AND U_ASDate IS NULL  AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            '            varname1.Append("		or " & vbCrLf)
            '            varname1.Append("		(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))		or (e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL AND r.U_Driver = 'Y'  AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) ) " & vbCrLf)
            '            varname1.Append("		or " & vbCrLf)
            '            varname1.Append("		(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL AND r.U_Driver = 'Y'  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') ))")
            '            Return varname1.ToString()

            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            varname1 = New System.Text.StringBuilder
            '            varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60  AND U_ASDate IS NULL AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
            '            Return varname1.ToString()
            '        Else
            '            Return " 1=0 "
            '        End If

            '    Else
            '        Select Case sStatus
            '            Case com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper
            '                varname1 = New System.Text.StringBuilder
            '                varname1.Append("(e.U_Status = 9 AND e.Code = r.U_JobNr AND r.U_Driver != 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
            '                Return varname1.ToString()
            '            Case com.idh.bridge.lookups.FixedValues.getProgressDFT(True).ToUpper
            '                varname1 = New System.Text.StringBuilder
            '                varname1.Append("(((e.U_Status = 9 AND e.Code = r.U_JobNr AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='')  AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            '                varname1.Append("		OR " & vbCrLf)
            '                varname1.Append("	( (e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60  AND U_ASDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            '                varname1.Append("	)) " & vbCrLf)
            '                varname1.Append("	OR (e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' '  + STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60  AND U_ASDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            '                varname1.Append("	or (e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)) " & vbCrLf)
            '                varname1.Append("	or (e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0)))")
            '                Return varname1.ToString()
            '            Case com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper
            '                varname1 = New System.Text.StringBuilder
            '                varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 AND U_ASDate IS NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
            '                Return varname1.ToString()
            '            Case com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper
            '                varname1 = New System.Text.StringBuilder
            '                varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='')  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
            '                Return varname1.ToString()
            '            Case com.idh.bridge.lookups.FixedValues.getProgressAssigned(True).ToUpper
            '                varname1 = New System.Text.StringBuilder
            '                varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')  )")
            '                Return varname1.ToString()
            '            Case com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper
            '                varname1 = New System.Text.StringBuilder
            '                varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL  AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')  )")
            '                Return varname1.ToString()
            '            Case com.idh.bridge.lookups.FixedValues.getProgressCancelled(True).ToUpper
            '                varname1 = New System.Text.StringBuilder
            '                varname1.Append("((e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NOT NULL AND r.U_Driver = 'Y'  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            '                varname1.Append("		or " & vbCrLf)
            '                varname1.Append("	(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60 AND U_ASDate IS NULL  AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            '                varname1.Append("		or " & vbCrLf)
            '                varname1.Append("		(e.U_Status = 9 AND e.Code = r.U_JobNr AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='')  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            '                varname1.Append("		or " & vbCrLf)
            '                varname1.Append("		(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' '  + STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) > 1 * - 60 AND U_ASDate IS NULL  AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '')) " & vbCrLf)
            '                varname1.Append("		or " & vbCrLf)
            '                varname1.Append("		(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_RDate IS NULL AND r.U_Driver = 'Y' AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))		or (e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL AND r.U_Driver = 'Y'  AND (r.U_DPRRef IS NOT NULL AND LEN(r.U_DPRRef) >0) ) " & vbCrLf)
            '                varname1.Append("		or " & vbCrLf)
            '                varname1.Append("		(e.U_Status != 9 AND e.Code = r.U_JobNr AND U_ASDate IS NOT NULL AND U_AEDate IS NULL AND r.U_Driver = 'Y'  AND (r.U_DPRRef IS NULL OR r.U_DPRRef = '') ))")
            '                Return varname1.ToString()
            '            Case com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper
            '                varname1 = New System.Text.StringBuilder
            '                varname1.Append("(e.U_Status != 9 AND e.Code = r.U_JobNr AND DATEDIFF(minute, CAST((CONVERT(NVARCHAR, U_RDate, 12) + ' ' +  STUFF(RIGHT('0000' + CAST(U_RTime AS VARCHAR), 4), 3, 0, ':')) AS DATETIME), GETDATE()) <= 1 * - 60  AND U_ASDate IS NULL AND (r.U_Driver != 'Y' OR r.U_Driver IS NULL OR r.U_Driver ='') AND (r.U_DPRRef IS NULL OR r.U_DPRRef = ''))")
            '                Return varname1.ToString()
            '            Case Else
            '                Return " 1=0 "
            '        End Select
            '    End If
            'Else ''default status filter
            '    If sStatus.IndexOf("*") > 0 Then
            '        sStatus = sStatus.Substring(0, sStatus.IndexOf("*"))
            '        If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.StartsWith(sStatus) Then
            '            Return "(e.U_Status = 9 )"
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.StartsWith(sStatus) Then
            '            Return "(e.U_Status != 9 AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffLateCheck3() & " " _
            '                  & " 		AND r.U_ASDate IS NULL )"
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.StartsWith(sStatus) Then
            '            Return "(e.U_Status != 9 " _
            '                   & " AND U_RDate IS NULL )"
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressBusy(True).ToUpper.StartsWith(sStatus) Then
            '            Return "(e.U_Status != 9 " _
            '                 & " AND r.U_ASDate IS NOT NULL " _
            '                 & " AND r.U_AEDate IS NULL )"
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.StartsWith(sStatus) Then
            '            Return "(e.U_Status != 9 " _
            '                 & " AND U_ASDate IS NOT NULL " _
            '                 & " AND U_AEDate IS NOT NULL )"
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.StartsWith(sStatus) Then
            '            Return "(e.U_Status != 9 " _
            '                 & " AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffWaitCheck3() & "  " _
            '                 & " AND U_ASDate IS NULL )"
            '        Else
            '            Return " 1=0 "
            '        End If
            '    ElseIf sStatus.IndexOf("*") <= 0 Then
            '        sStatus = sStatus.Replace("*", "")
            '        If sStatus.Trim = "" Then
            '            Return ""
            '        End If
            '        If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            Return "(e.U_Status = 9 )"
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            Return "(e.U_Status != 9 AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffLateCheck3() & " " _
            '                  & " 		AND r.U_ASDate IS NULL )"
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            Return "(e.U_Status != 9 " _
            '                   & " AND U_RDate IS NULL )"
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressBusy(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            Return "(e.U_Status != 9 " _
            '                 & " AND r.U_ASDate IS NOT NULL " _
            '                 & " AND r.U_AEDate IS NULL )"
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            Return "(e.U_Status != 9 " _
            '                 & " AND U_ASDate IS NOT NULL " _
            '                 & " AND U_AEDate IS NOT NULL )"
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            Return "(e.U_Status != 9 " _
            '                 & " AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffWaitCheck3() & "  " _
            '                 & " AND U_ASDate IS NULL )"
            '        Else
            '            Return " 1=0 "
            '        End If
            '    Else
            '        Select Case sStatus
            '            Case com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper
            '                Return "(e.U_Status = 9 )"
            '            Case com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper
            '                Return "(e.U_Status != 9 AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffLateCheck3() & " " _
            '                  & " 		AND r.U_ASDate IS NULL )"
            '            Case com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper
            '                Return "(e.U_Status != 9 " _
            '                  & " AND U_RDate IS NULL )"
            '            Case com.idh.bridge.lookups.FixedValues.getProgressBusy(True).ToUpper
            '                Return "(e.U_Status != 9 " _
            '                  & " AND r.U_ASDate IS NOT NULL " _
            '                  & " AND r.U_AEDate IS NULL )"
            '            Case com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper
            '                Return "(e.U_Status != 9 " _
            '                  & " AND U_ASDate IS NOT NULL " _
            '                  & " AND U_AEDate IS NOT NULL )"
            '            Case com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper
            '                Return "(e.U_Status != 9 " _
            '                  & " AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffWaitCheck3() & "  " _
            '                  & " AND U_ASDate IS NULL )"
            '            Case Else
            '                Return " 1=0 "
            '        End Select
            '    End If
            'End If
            ''Select Case sStatus
            ''    Case com.idh.bridge.lookups.FixedValues.getProgressClosed().ToUpper
            ''        Return "(Case WHEN e.U_Status = 9 THEN '" & com.idh.bridge.lookups.FixedValues.getProgressClosed() & "' ELSE '' END)"
            ''    Case com.idh.bridge.lookups.FixedValues.getProgressLate().ToUpper
            ''        Return "(Case When e.U_Status != 9 AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffLateCheck3() & " " _
            ''          & " 		AND r.U_ASDate IS NULL THEN '" & com.idh.bridge.lookups.FixedValues.getProgressLate() & "' ELSE '' END)"
            ''    Case com.idh.bridge.lookups.FixedValues.getProgressNoRequest().ToUpper
            ''        Return "(Case When e.U_Status != 9 " _
            ''          & " AND U_RDate IS NULL THEN '" & com.idh.bridge.lookups.FixedValues.getProgressNoRequest() & "' ELSE '' END)"
            ''    Case com.idh.bridge.lookups.FixedValues.getProgressBusy().ToUpper
            ''        Return "(Case When e.U_Status != 9 " _
            ''          & " AND r.U_ASDate IS NOT NULL " _
            ''          & " AND r.U_AEDate IS NULL THEN '" & com.idh.bridge.lookups.FixedValues.getProgressBusy() & "' ELSE '' END)"
            ''    Case com.idh.bridge.lookups.FixedValues.getProgressDone().ToUpper
            ''        Return "(Case When e.U_Status != 9 " _
            ''          & " AND U_ASDate IS NOT NULL " _
            ''          & " AND U_AEDate IS NOT NULL THEN '" & com.idh.bridge.lookups.FixedValues.getProgressDone() & "' ELSE '' END)"
            ''    Case com.idh.bridge.lookups.FixedValues.getProgressWaiting().ToUpper
            ''        Return "(Case When  e.U_Status != 9 " _
            ''          & " AND " & com.idh.bridge.lookups.Config.INSTANCE.getDateDiffWaitCheck3() & "  " _
            ''          & " AND U_ASDate IS NULL THEN '" & com.idh.bridge.lookups.FixedValues.getProgressWaiting() & "' ELSE '' END)"
            ''    Case Else
            ''        Return ""
            ''End Select
        End Function

        Protected Function doGetStatusFilterQueryDynamic(ByVal sStatus As String) As String
            'Try

            Dim oRecords As com.idh.bridge.DataRecords = Nothing
            Dim sSQL As String = ""
            sSQL = "Select U_ProgFilter,U_ProgFltSQL from [@IDH_PRGFLT] with(nolock)"
            oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doGetProgressFilterQuerries", sSQL)
            If (oRecords Is Nothing OrElse oRecords.RecordCount = 0) Then
                'old method, 
                Return doGetStatusFilterQuery(sStatus)
            End If
            'Catch ex As Exception
            '    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {"doGetProgressStatusQuery"})
            'End Try
            'Dim oRecords As com.idh.bridge.DataRecords = Nothing
            oRecords.gotoFirst()

            If sStatus.IndexOf("*") > 0 Then
                sStatus = sStatus.Substring(0, sStatus.IndexOf("*"))
                Dim bAddNot As Boolean = False
                If sStatus.StartsWith("!") Then
                    bAddNot = True
                    sStatus = sStatus.Replace("!", "")
                End If
                ' where U_ProgFilter Like '" & sStatus & "%'"
                Dim sQuery As String = ""

                For i As Int16 = 0 To oRecords.RecordCount - 1
                    oRecords.gotoRow(i)
                    If Translation.getTranslated("PROGRESS", oRecords.getValueAsString("U_ProgFilter"), "", oRecords.getValueAsString("U_ProgFilter")).ToUpper.StartsWith(sStatus) Then
                        'End If
                        'If (Translation.getTranslatedWord(oRecords.getValueAsString("U_ProgFilter")).ToUpper.StartsWith(sStatus)) Then
                        sQuery = oRecords.getValueAsString("U_ProgFltSQL")
                        Exit For
                    End If
                Next
                If sQuery = "" Then
                    Return " 1=0 "
                End If

                'If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.StartsWith(sStatus) Then
                '    sQuery = getQueryForClosedStatus(True)
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressDFT(True).ToUpper.StartsWith(sStatus) Then
                '    sQuery = getQueryForDFTStatus()
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.StartsWith(sStatus) Then
                '    sQuery = getQueryForLateStatus(True)
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.StartsWith(sStatus) Then
                '    sQuery = getQueryForNoRequestStatus(True)
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressAssigned(True).ToUpper.StartsWith(sStatus) Then
                '    sQuery = getQueryForAssignedStatus()
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.StartsWith(sStatus) Then
                '    sQuery = getQueryForDoneStatus(True)
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressCancelled(True).ToUpper.StartsWith(sStatus) Then
                '    sQuery = getQueryForCancelled()
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.StartsWith(sStatus) Then
                '    sQuery = getQueryForWaiting(True)
                'Else
                '    Return " 1=0 "
                'End If
                If bAddNot Then
                    Return "Not (" & sQuery & ")"
                Else
                    Return sQuery
                End If
            ElseIf sStatus.IndexOf("*") = 0 Then
                sStatus = sStatus.Replace("*", "")
                If sStatus.Trim = "" Then
                    Return ""
                End If
                Dim bAddNot As Boolean = False
                If sStatus.StartsWith("!") Then
                    bAddNot = True
                    sStatus = sStatus.Replace("!", "")
                End If
                Dim sQuery As String = ""
                For i As Int16 = 0 To oRecords.RecordCount - 1
                    oRecords.gotoRow(i)
                    If Translation.getTranslated("PROGRESS", oRecords.getValueAsString("U_ProgFilter"), "", oRecords.getValueAsString("U_ProgFilter")).ToUpper.IndexOf(sStatus) > -1 Then
                        sQuery = oRecords.getValueAsString("U_ProgFltSQL")
                        Exit For
                    End If
                Next
                If sQuery = "" Then
                    Return " 1=0 "
                End If
                'If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.IndexOf(sStatus) > -1 Then
                '    sQuery = getQueryForClosedStatus(True)
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressDFT(True).ToUpper.IndexOf(sStatus) > -1 Then
                '    sQuery = getQueryForDFTStatus()
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.IndexOf(sStatus) > -1 Then
                '    sQuery = getQueryForLateStatus(True)
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.IndexOf(sStatus) > -1 Then
                '    sQuery = getQueryForNoRequestStatus(True)
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressAssigned(True).ToUpper.IndexOf(sStatus) > -1 Then
                '    sQuery = getQueryForAssignedStatus()
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.IndexOf(sStatus) > -1 Then
                '    sQuery = getQueryForDoneStatus(True)
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressCancelled(True).ToUpper.IndexOf(sStatus) > -1 Then
                '    sQuery = getQueryForCancelled()
                'ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.IndexOf(sStatus) > -1 Then
                '    sQuery = getQueryForWaiting(True)
                'Else
                '    Return " 1=0 "
                'End If
                If bAddNot Then
                    Return "Not (" & sQuery & ")"
                Else
                    Return sQuery
                End If
            Else
                Dim bAddNot As Boolean = False
                Dim sQuery As String = ""
                If sStatus.StartsWith("!") Then
                    bAddNot = True
                    sStatus = sStatus.Replace("!", "")
                End If
                If sStatus = "" Then
                    Return ""
                End If
                Dim aStatus As String() = sStatus.Split(","c)
                For i As Int16 = 0 To aStatus.Length - 1
                    If aStatus(i).Trim <> "" Then
                        oRecords.gotoFirst()
                        For irec As Int16 = 0 To oRecords.RecordCount - 1
                            oRecords.gotoRow(irec)
                            If Translation.getTranslated("PROGRESS", oRecords.getValueAsString("U_ProgFilter"), "", oRecords.getValueAsString("U_ProgFilter")).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                                sQuery = oRecords.getValueAsString("U_ProgFltSQL")
                                Exit For
                            End If
                        Next
                        If sQuery = "" Then
                            Return " 1=0 "
                        End If
                        aStatus(i) = sQuery
                        'If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                        '    sQuery = getQueryForClosedStatus(True)
                        'ElseIf com.idh.bridge.lookups.FixedValues.getProgressDFT(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                        '    sQuery = getQueryForDFTStatus()
                        'ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                        '    sQuery = getQueryForLateStatus(True)
                        'ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                        '    sQuery = getQueryForNoRequestStatus(True)
                        'ElseIf com.idh.bridge.lookups.FixedValues.getProgressAssigned(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                        '    sQuery = getQueryForAssignedStatus()
                        'ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                        '    sQuery = getQueryForDoneStatus(True)
                        'ElseIf com.idh.bridge.lookups.FixedValues.getProgressCancelled(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                        '    sQuery = getQueryForCancelled()
                        'ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
                        '    sQuery = getQueryForWaiting(True)
                        'Else
                        '    Return " 1=0 "
                        'End If

                    End If
                Next
                sQuery = ""
                For i As Int16 = 0 To aStatus.Length - 2
                    If aStatus(i).Trim <> "" Then
                        sQuery &= " (" & aStatus(i).Trim & ") OR"
                    End If
                Next
                If aStatus(aStatus.Length - 1).Trim <> "" Then
                    sQuery &= " (" & aStatus(aStatus.Length - 1).Trim & ")"
                End If
                sQuery = sQuery.Trim '.TrimEnd("OR")
                If sQuery.EndsWith("OR") Then
                    sQuery = sQuery.Substring(0, sQuery.Length - 2)
                End If
                If sQuery = "" Then
                    Return " 1=0 "
                End If
                If bAddNot Then
                    Return "Not (" & sQuery & ")"
                Else
                    Return sQuery
                End If
            End If
            ''-----------------------------------------------------------------------
            'If Config.ParameterAsBool("USENWSTS", False) Then ''status filter for IIS
            '    If sStatus.IndexOf("*") > 0 Then
            '        sStatus = sStatus.Substring(0, sStatus.IndexOf("*"))
            '        Dim bAddNot As Boolean = False
            '        If sStatus.StartsWith("!") Then
            '            bAddNot = True
            '            sStatus = sStatus.Replace("!", "")
            '        End If
            '        Dim sQuery As String = ""
            '        If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForClosedStatus(True)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressDFT(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForDFTStatus()
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForLateStatus(True)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForNoRequestStatus(True)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressAssigned(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForAssignedStatus()
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForDoneStatus(True)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressCancelled(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForCancelled()
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForWaiting(True)
            '        Else
            '            Return " 1=0 "
            '        End If
            '        If bAddNot Then
            '            Return "Not (" & sQuery & ")"
            '        Else
            '            Return sQuery
            '        End If
            '    ElseIf sStatus.IndexOf("*") = 0 Then
            '        sStatus = sStatus.Replace("*", "")
            '        If sStatus.Trim = "" Then
            '            Return ""
            '        End If
            '        Dim bAddNot As Boolean = False
            '        If sStatus.StartsWith("!") Then
            '            bAddNot = True
            '            sStatus = sStatus.Replace("!", "")
            '        End If
            '        Dim sQuery As String = ""
            '        If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForClosedStatus(True)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressDFT(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForDFTStatus()
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForLateStatus(True)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForNoRequestStatus(True)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressAssigned(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForAssignedStatus()
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForDoneStatus(True)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressCancelled(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForCancelled()
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForWaiting(True)
            '        Else
            '            Return " 1=0 "
            '        End If
            '        If bAddNot Then
            '            Return "Not (" & sQuery & ")"
            '        Else
            '            Return sQuery
            '        End If
            '    Else
            '        Dim bAddNot As Boolean = False
            '        Dim sQuery As String = ""
            '        If sStatus.StartsWith("!") Then
            '            bAddNot = True
            '            sStatus = sStatus.Replace("!", "")
            '        End If
            '        If sStatus = "" Then
            '            Return ""
            '        End If
            '        Dim aStatus As String() = sStatus.Split(","c)
            '        For i As Int16 = 0 To aStatus.Length - 1
            '            If aStatus(i).Trim <> "" Then
            '                If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForClosedStatus(True)
            '                ElseIf com.idh.bridge.lookups.FixedValues.getProgressDFT(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForDFTStatus()
            '                ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForLateStatus(True)
            '                ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForNoRequestStatus(True)
            '                ElseIf com.idh.bridge.lookups.FixedValues.getProgressAssigned(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForAssignedStatus()
            '                ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForDoneStatus(True)
            '                ElseIf com.idh.bridge.lookups.FixedValues.getProgressCancelled(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForCancelled()
            '                ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForWaiting(True)
            '                Else
            '                    Return " 1=0 "
            '                End If
            '                aStatus(i) = sQuery
            '            End If
            '        Next
            '        sQuery = ""
            '        For i As Int16 = 0 To aStatus.Length - 2
            '            If aStatus(i).Trim <> "" Then
            '                sQuery &= " (" & aStatus(i).Trim & ") OR"
            '            End If
            '        Next
            '        If aStatus(aStatus.Length - 1).Trim <> "" Then
            '            sQuery &= " (" & aStatus(aStatus.Length - 1).Trim & ")"
            '        End If
            '        sQuery = sQuery.Trim '.TrimEnd("OR")
            '        If sQuery.EndsWith("OR") Then
            '            sQuery = sQuery.Substring(0, sQuery.Length - 2)
            '        End If
            '        If sQuery = "" Then
            '            Return " 1=0 "
            '        End If
            '        If bAddNot Then
            '            Return "Not (" & sQuery & ")"
            '        Else
            '            Return sQuery
            '        End If
            '    End If
            'Else ''default status filter
            '    If sStatus.IndexOf("*") > 0 Then
            '        sStatus = sStatus.Substring(0, sStatus.IndexOf("*"))
            '        Dim bAddNot As Boolean = False
            '        If sStatus.StartsWith("!") Then
            '            bAddNot = True
            '            sStatus = sStatus.Replace("!", "")
            '        End If
            '        Dim sQuery As String = ""
            '        If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForClosedStatus(False)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForLateStatus(False)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForNoRequestStatus(False)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressBusy(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForBusyStatus(False)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForDoneStatus(False)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.StartsWith(sStatus) Then
            '            sQuery = getQueryForWaiting(False)
            '        Else
            '            Return " 1=0 "
            '        End If
            '        If bAddNot Then
            '            Return "Not (" & sQuery & ")"
            '        Else
            '            Return sQuery
            '        End If
            '    ElseIf sStatus.IndexOf("*") = 0 Then
            '        sStatus = sStatus.Replace("*", "")
            '        If sStatus.Trim = "" Then
            '            Return ""
            '        End If
            '        Dim bAddNot As Boolean = False
            '        If sStatus.StartsWith("!") Then
            '            bAddNot = True
            '            sStatus = sStatus.Replace("!", "")
            '        End If
            '        Dim sQuery As String = ""

            '        If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForClosedStatus(False)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForLateStatus(False)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForNoRequestStatus(False)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressBusy(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForBusyStatus(False)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForDoneStatus(False)
            '        ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.IndexOf(sStatus) > -1 Then
            '            sQuery = getQueryForWaiting(False)
            '        Else
            '            Return " 1=0 "
            '        End If

            '        If bAddNot Then
            '            Return "Not (" & sQuery & ")"
            '        Else
            '            Return sQuery
            '        End If
            '    Else
            '        Dim bAddNot As Boolean = False
            '        Dim sQuery As String = ""
            '        If sStatus.StartsWith("!") Then
            '            bAddNot = True
            '            sStatus = sStatus.Replace("!", "")
            '        End If
            '        If sStatus = "" Then
            '            Return ""
            '        End If

            '        Dim aStatus As String() = sStatus.Split(","c)
            '        For i As Int16 = 0 To aStatus.Length - 1
            '            If aStatus(i).Trim <> "" Then
            '                If com.idh.bridge.lookups.FixedValues.getProgressClosed(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForClosedStatus(False)
            '                ElseIf com.idh.bridge.lookups.FixedValues.getProgressLate(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForLateStatus(False)
            '                ElseIf com.idh.bridge.lookups.FixedValues.getProgressNoRequest(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForNoRequestStatus(False)
            '                ElseIf com.idh.bridge.lookups.FixedValues.getProgressBusy(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForBusyStatus(False)
            '                ElseIf com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForDoneStatus(False)
            '                ElseIf com.idh.bridge.lookups.FixedValues.getProgressWaiting(True).ToUpper.IndexOf(aStatus(i).Trim) > -1 Then
            '                    sQuery = getQueryForWaiting(False)
            '                Else
            '                    Return " 1=0 "
            '                End If
            '                aStatus(i) = sQuery
            '            End If
            '        Next
            '        sQuery = ""
            '        For i As Int16 = 0 To aStatus.Length - 2
            '            If aStatus(i).Trim <> "" Then
            '                sQuery &= " (" & aStatus(i).Trim & ") OR"
            '            End If
            '        Next
            '        If aStatus(aStatus.Length - 1).Trim <> "" Then
            '            sQuery &= " (" & aStatus(aStatus.Length - 1).Trim & ")"
            '        End If
            '        sQuery = sQuery.Trim '.TrimEnd("OR")
            '        If sQuery.EndsWith("OR") Then
            '            sQuery = sQuery.Substring(0, sQuery.Length - 2)
            '        End If
            '        If sQuery = "" Then
            '            Return " 1=0 "
            '        End If
            '        If bAddNot Then
            '            Return "Not (" & sQuery & ")"
            '        Else
            '            Return sQuery
            '        End If
            '    End If
            'End If
            Return ""

        End Function

        '        Protected Overridable Sub doTotals(ByVal oGridN As UpdateGrid, Optional ByVal bDoZero As Boolean = False)
        '        End Sub
        Protected Overridable Sub doSetFormSpecificFilters(oGridN As OrderRowGrid)

        End Sub
        Protected Overridable Sub doWarnInActiveBPnSites(ByVal oForm As SAPbouiCOM.Form, oGridN As IDHGrid)
            Try
                If Config.ParameterAsBool("SHWINACW", False) = False Then
                    Exit Sub
                End If

                Dim aRows As ArrayList
                'If bBySelection Then
                '    aRows = oGridN.getGrid.Rows.SelectedRows
                'Else
                aRows = oGridN.doGetChangedRows()
                'End If
                If Not aRows Is Nothing Then
                    Dim sValue As String = ""



                    Dim sBPCodes As String() '= {""}
                    Dim sAddresses As String() ' = {""}
                    If oForm.TypeEx.IndexOf("IDHJOBR") > -1 Then
                        sBPCodes = {"Customer", "e.U_CardCd", "Carrier", "r.U_CarrCd", "Site Lic Supplier", "r.U_SLicSp", "Producer", "e.U_PCardCd",
                                                  "Disposal Site", "r.U_Tip"}

                        sAddresses = {"Site Address", "e.U_CardCd", "e.U_Address", "Producer Address", "e.U_PCardCd", "e.U_PAddress"}
                        If oForm.TypeEx = "IDHJOBR3" OrElse oForm.TypeEx = "IDHJOBR6" Then
                            sAddresses = com.idh.utils.General.doCombineArrays(sAddresses, {"Disp.Sit.Add", "e.U_SCardCd", "r.U_SAddress"})
                        End If

                    ElseIf oForm.TypeEx.IndexOf("IDHWOBI") > -1 Then
                        sBPCodes = {"Customer", "e.U_CardCd", "Carrier", "r.U_CarrCd", "Disposal Site", "r.U_Tip"}
                        If oForm.TypeEx = "IDHWOBIL4" Then
                            sBPCodes = com.idh.utils.General.doCombineArrays(sBPCodes, {"Supplier Code", "r.U_ProCd"})
                        End If

                        sAddresses = {"Address Name", "e.U_CardCd", "e.U_Address", "Producer Address", "e.U_PCardCd", "e.U_PAddress"}

                    ElseIf oForm.TypeEx.IndexOf("IDHDOM") > -1 Then
                        sBPCodes = {"Customer", "e.U_CardCd", "Carrier", "r.U_CarrCd", "Disposal Site", "r.U_Tip",
                                                                              "Site Lic Supplier", "r.U_SLicSp"}

                        sAddresses = {"Order Address", "e.U_CardCd", "e.U_Address", "Disp.Sit.Add", "e.U_SCardCd", "r.U_SAddress"}

                    ElseIf oForm.TypeEx.IndexOf("IDHROUP") > -1 AndAlso oGridN.GridId = "LINESGRID" Then
                        Exit Sub
                        sBPCodes = {"Customer", "h.U_CardCd", "Carrier", "r.U_CarrCd", "Disposal Site", "r.U_Tip",
                                                                              "Site Lic Supplier", "r.U_SLicSp", "Supplier Code", "r.U_ProCd"}

                        sAddresses = {"Address", "h.U_CardCd", "h.U_Address", "Producer Address", "r.U_ProCd", "h.U_PAddress"}
                    ElseIf oForm.TypeEx.IndexOf("IDHROUP") > -1 AndAlso oGridN.GridId = "JOBGRID" Then
                        sBPCodes = {"Customer", "h.U_CardCd", "Carrier", "r.U_CarrCd", "Disposal Site", "r.U_Tip",
                                                                              "Site Lic Supplier", "r.U_SLicSp", "Supplier Code", "r.U_ProCd"}

                        sAddresses = {"Address", "h.U_CardCd", "h.U_Address", "Producer Address", "r.U_ProCd", "h.U_PAddress"}
                    ElseIf oForm.TypeEx.IndexOf("IDHUPDTR") > -1 Then
                        sBPCodes = {"Customer", "e.U_CardCd", "Carrier", "r.U_CarrCd", "Disposal Site", "r.U_Tip",
                                                      "Site Lic Supplier", "r.U_SLicSp"}

                        sAddresses = {"Order Address", "e.U_CardCd", "e.U_Address", "Producer Address", "e.U_PCardCd", "e.U_PAddress"}

                    ElseIf oForm.TypeEx.IndexOf("IDHDOUPD") > -1 Then
                        sBPCodes = {"Customer", "e.U_CardCd", "Carrier", "r.U_CarrCd", "Disposal Site", "r.U_Tip",
                                                                              "Site Lic Supplier", "r.U_SLicSp"}
                        sAddresses = {"Order Address", "e.U_CardCd", "e.U_Address"}

                    Else
                        Exit Sub
                    End If
                    Dim sLabel, sBPField, sBPCode, sAddressField, sBPColName As String
                    Dim oBPParam() As String = {"", ""}
                    Dim oAddressParam() As String = {"", "", ""}
                    For iIndex As Integer = 0 To aRows.Count - 1
                        oGridN.setCurrentDataRowIndex(aRows(iIndex))
                        For iitem As Int32 = 0 To sBPCodes.Count - 1 Step 2
                            sLabel = sBPCodes(iitem)
                            sBPField = sBPCodes(iitem + 1)
                            sValue = oGridN.doGetFieldValue(sBPField)
                            If sValue IsNot Nothing AndAlso sValue.Trim <> "" AndAlso Not Config.INSTANCE.doCheckBPActive(sValue, DateTime.Now) Then

                                oBPParam(0) = Translation.getTranslatedWord(sLabel)


                                oBPParam(1) = sValue
                                doWarnMess(Messages.getGMessage("WRINACBP", oBPParam), SAPbouiCOM.BoMessageTime.bmt_Short)

                            End If
                        Next
                        For iitem As Int32 = 0 To sAddresses.Count - 1 Step 3
                            sLabel = sAddresses(iitem)
                            sBPField = sAddresses(iitem + 1)
                            sAddressField = sAddresses(iitem + 2)

                            sValue = oGridN.doGetFieldValue(sAddressField)
                            sBPCode = oGridN.doGetFieldValue(sBPField)
                            sBPColName = oGridN.Columns.Item(oGridN.doFieldIndex(sBPField)).TitleObject.Caption
                            If sValue IsNot Nothing AndAlso sValue.Trim <> "" AndAlso Not Config.INSTANCE.doCheckBPAddressActive(sBPCode, sValue) Then
                                oAddressParam(0) = Translation.getTranslatedWord(sLabel)
                                oAddressParam(1) = sValue
                                oAddressParam(2) = sBPColName
                                doWarnMess(Messages.getGMessage("WRINACAD", oAddressParam), SAPbouiCOM.BoMessageTime.bmt_Short)

                            End If
                        Next
                    Next
                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "doWarnInActiveBPnSites", {Nothing})
            End Try
        End Sub

        Protected Overridable Function doSaveOrderRows(ByVal oForm As SAPbouiCOM.Form) As Boolean
            'Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            doWarnInActiveBPnSites(oForm, oGridN)
            Dim aRows As ArrayList = oGridN.doGetChangedRows()

            Dim sField As String = ""
            Dim oValue As Object = ""
            Dim sKey1 As String = ""
            Dim sKey2 As String = ""

            Dim oAuditObj1 As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_JOBSHD")
            Dim oAuditObj2 As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_JOBENTR")
            Try
                '                Dim sSOStatus As String
                '                Dim sPOStatus As String
                Dim iResult As Integer
                Dim sResult As String = Nothing

                Dim aChangedFields As ArrayList
                If Not aRows Is Nothing Then
                    For iIndex As Integer = 0 To aRows.Count - 1
                        oGridN.setCurrentDataRowIndex(aRows(iIndex))
                        sKey1 = oGridN.doGetFieldValue("r.Code")
                        sKey2 = oGridN.doGetFieldValue("r.U_JobNr")

                        If oAuditObj1.setKeyPair(sKey1, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) AndAlso
                           oAuditObj2.setKeyPair(sKey2, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then
                            aChangedFields = oGridN.doGetChangedFields(aRows(iIndex))
                            For iField As Integer = 0 To aChangedFields.Count - 1
                                sField = aChangedFields.Item(iField)
                                oValue = oGridN.doGetFieldValue(sField)

                                If sField.StartsWith("r.") Then
                                    sField = sField.Substring(2)
                                    oAuditObj1.setFieldValue(sField, oValue)
                                ElseIf sField.StartsWith("e.") Then
                                    sField = sField.Substring(2)
                                    oAuditObj2.setFieldValue(sField, oValue)
                                End If

                                ''USA Release 
                                ''Additional Items Surcharge Logic 
                                ''Put on HOLD because the columns required in 
                                ''surcharge row calculations like Manifest Qty, UOM etc 
                                ''are missing in the OSM Grids 
                                ''Need to include all those columns on all OSM 1...6 
                                ''START 
                                'If sField.Equals("U_AEDate") AndAlso _
                                '    (Not oValue Is Nothing) AndAlso _
                                '    Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                                '    Dim sAEDate As String = oValue.ToString()
                                '    If sAEDate.Length > 0 Then
                                '        doAddSurcharges(oForm, oGridN)
                                '    Else
                                '        doRemovesurcharges(oGridN)
                                '    End If
                                'End If
                                ''END 
                            Next

                            iResult = oAuditObj1.Commit()
                            If iResult <> 0 Then
                                goParent.goDICompany.GetLastError(iResult, sResult)
                                'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error saving the Order rows: " + sResult, "Error saving the Order rows - " & sKey1)
                                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error saving the Order rows: " + sResult + " - " + sKey1, "ERSYORDR", {com.idh.bridge.Translation.getTranslatedWord(sResult), sKey1})
                            Else
                                oGridN.doDeletePBIDetails()
                            End If
                            iResult = oAuditObj2.Commit()
                            If iResult <> 0 Then
                                goParent.goDICompany.GetLastError(iResult, sResult)
                                'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error saving the Order rows: " + sResult, "Error saving the Order rows - " & sKey2)
                                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error saving the Order rows: " + sResult + " - " + sKey2, "ERSYORDR", {com.idh.bridge.Translation.getTranslatedWord(sResult), sKey2})
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error saving the Order rows - " & sKey1 & "." & sField & "." & oValue)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBS", {com.idh.bridge.Translation.getTranslatedWord("Order rows"), sKey1, sField, oValue})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj1)
            End Try
            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Update") OrElse
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    'Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                    Dim aRows As ArrayList = oGridN.doGetChangedRows()
                    Dim oCloseList As ArrayList = New ArrayList()
                    If doSaveOrderRows(oForm) = True Then
                        Dim sHead As String
                        Dim sRow As String
                        Dim sSOStatus As String
                        Dim sPOStatus As String
                        Dim sDoHead As String
                        Dim sDORow As String
                        If Not aRows Is Nothing Then
                            Dim bDoInBack As Boolean = Config.INSTANCE.getParameterAsBool("MDCMIBG", False)
                            If bDoInBack = False OrElse oGridN.mbForceRealTimeBilling = True Then
                                For iIndex As Integer = 0 To aRows.Count - 1
                                    oGridN.setCurrentDataRowIndex(aRows(iIndex))
                                    sHead = oGridN.doGetFieldValue("r." & IDH_JOBSHD._JobNr)
                                    sRow = oGridN.doGetFieldValue("r." & IDH_JOBSHD._Code)
                                    sSOStatus = oGridN.doGetFieldValue("r." & IDH_JOBSHD._Status)
                                    sPOStatus = oGridN.doGetFieldValue("r." & IDH_JOBSHD._PStat)
                                    sDoHead = oGridN.doGetFieldValue("r." & IDH_JOBSHD._WROrd)   '#MA point#425
                                    sDORow = oGridN.doGetFieldValue("r." & IDH_JOBSHD._WRRow)     '#MA point#425
                                    If sSOStatus.StartsWith(FixedValues.getDoOrderStatus()) OrElse
                                     sPOStatus.StartsWith(FixedValues.getDoOrderStatus()) Then
                                        MarketingDocs.doOrders("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow)
                                        '#MA Start 13-06-2017 Point#425
                                        If Not String.IsNullOrWhiteSpace(sDoHead) AndAlso Not String.IsNullOrWhiteSpace(sDORow) AndAlso Not String.IsNullOrWhiteSpace(sSOStatus) AndAlso Not String.IsNullOrWhiteSpace(sPOStatus) Then
                                            Dim oGetStatus As IDH_JOBSHD = New IDH_JOBSHD()
                                            If oGetStatus.getByKey(sRow) Then
                                                If Not String.IsNullOrWhiteSpace(oGetStatus.U_Status) AndAlso Not String.IsNullOrWhiteSpace(oGetStatus.U_PStat) Then
                                                    If (oGetStatus.U_Status.Equals(FixedValues.getStatusOrdered()) Or oGetStatus.U_PStat.Equals(FixedValues.getStatusOrdered())) Then
                                                        Dim oDoRow As IDH_DISPROW = New IDH_DISPROW()
                                                        oDoRow.UnLockTable = True
                                                        If oDoRow.getByKey(sDORow) Then
                                                            oDoRow.U_Status = com.idh.bridge.lookups.FixedValues.getFocStatus()
                                                            oDoRow.DoSkipUpdateLinkedWO = True
                                                            oDoRow.doUpdateDataRow()
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                        '#MA 13-06-2017 End 
                                    End If

                                    If sSOStatus.StartsWith(FixedValues.getDoInvoiceStatus()) OrElse
                                     sSOStatus.StartsWith(FixedValues.getDoPInvoiceStatus()) Then
                                        MarketingDocs.doInvoicesAR("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow)
                                    End If

                                    If sSOStatus.StartsWith(FixedValues.getDoRebateStatus()) OrElse
                                     sPOStatus.StartsWith(FixedValues.getDoRebateStatus()) Then
                                        MarketingDocs.doRebates("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow)
                                    End If

                                    If sSOStatus.StartsWith(FixedValues.getDoFocStatus()) Then
                                        MarketingDocs.doFoc("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow)
                                    End If

                                    MarketingDocs.doPayment("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow)

                                    'CR 20150601: The DO Journals functionality build for USA Clients can now be used for all under new WR Config Key "NEWDOJRN" 
                                    ''For USA release 
                                    'If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
                                    If Config.INSTANCE.getParameterAsBool("NEWDOJRN", False) = True Then
                                        Dim sResult As Boolean = MarketingDocs.doJournals("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow)
                                        If sResult Then
                                            doWarnMess("The Journal entry for selected row(s) was posted successfully.")
                                        End If
                                    End If

                                    If oCloseList.Contains(sHead) = False Then
                                        Dim aFields As ArrayList = oGridN.doGetChangedFields(aRows(iIndex))
                                        If aFields.Contains("r.U_AEDate") Then
                                            Dim oEDate As Object = oGridN.doGetFieldValue("r.U_AEDate")
                                            If com.idh.utils.Dates.isValidDate(oEDate) Then
                                                Dim oWOH As IDH_JOBENTR = New IDH_JOBENTR()
                                                If oWOH.doCheckAndCloseWO(sHead, sRow) = True Then
                                                    oCloseList.Add(sHead)
                                                End If
                                            End If
                                        End If
                                    End If
                                Next
                            End If
                        End If
                        doReLoadData(oForm, True)
                    End If
                ElseIf CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Find") Then
                    doReLoadData(oForm, True)
                End If
                BubbleEvent = False
            End If
        End Sub

        Public Overrides Sub doButtonID2(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                doReLoadData(oForm, True)
            End If
        End Sub

        '** Send By Custom Events
        'Return True done
        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = False Then
                        doGridDoubleClick(oForm, pVal)
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                'This is to display the Popup menu
                If pVal.BeforeAction = True Then
                    Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                    If oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) Then
                        Dim oMenuItem As SAPbouiCOM.MenuItem
                        Dim sRMenuId As String = IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU
                        oMenuItem = goParent.goApplication.Menus.Item(sRMenuId)

                        Dim oMenus As SAPbouiCOM.Menus
                        Dim iMenuPos As Integer = 1
                        Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                        oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
                        oCreationPackage.Enabled = True
                        oMenus = oMenuItem.SubMenus
                        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING

                        oCreationPackage.UniqueID = "New"
                        oCreationPackage.String = getTranslatedWord("New &Order")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
                        Dim iSelectionCount As Integer = oSelected.Count

                        If iSelectionCount > 0 Then
                            setWFValue(oForm, "LASTJOBMENU", Nothing)

                            Dim iCurrentDataRow As Integer

                            'Fix for Right Click Menu when Grid is in Group Mode 
                            If oGridN.getSBOGrid().CollapseLevel > 0 Then
                                iCurrentDataRow = oGridN.GetDataTableRowIndex(pVal.Row)
                            Else
                                iCurrentDataRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)
                            End If

                            '*
                            ' Actions that can take place on multiple Rows
                            '*
                            If iSelectionCount > 1 Then

                                'Allow Duplicate Option for multi-row selection as well. for Non-USA clients only 
                                If Config.INSTANCE.getParameterAsBool("USAREL", False) = False Then
                                    oCreationPackage.UniqueID = "DUPLIMULTI"
                                    oCreationPackage.String = getTranslatedWord("Duplicate Multi Rows")
                                    oCreationPackage.Position = iMenuPos
                                    iMenuPos += 1
                                    oMenus.AddEx(oCreationPackage)
                                End If

                                If oGridN.doGetDeleteActive() Then
                                    If Not (pVal.Row = iCurrentDataRow) Then
                                        oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM
                                        oCreationPackage.String = getTranslatedWord("Delete &Selection")
                                        oCreationPackage.Enabled = True
                                        oMenus.AddEx(oCreationPackage)
                                    End If
                                End If

                                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_ARCHIVE) = False OrElse
                                    Config.INSTANCE.doGetIsSupperUser() Then
                                    oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE
                                    oCreationPackage.String = getTranslatedWord("Archive Rows")
                                    oCreationPackage.Enabled = True
                                    oMenus.AddEx(oCreationPackage)
                                End If

                                ''If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_) = False OrElse _
                                ''   Config.INSTANCE.doGetIsSupperUser() Then
                                'oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREOPEN
                                'oCreationPackage.String = getTranslatedWord("ReOpen Rows")
                                'oCreationPackage.Enabled = True
                                'oMenus.AddEx(oCreationPackage)
                                ''End If

                                ''If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_) = False OrElse _
                                ''    Config.INSTANCE.doGetIsSupperUser() Then
                                'oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUCANCELMARKDOCS
                                'oCreationPackage.String = getTranslatedWord("Cancel Marketing Documents")
                                'oCreationPackage.Enabled = True
                                'oMenus.AddEx(oCreationPackage)
                                ''End If
                            ElseIf iSelectionCount = 1 Then
                                Dim sCurrentJobType As String
                                Dim sJob As String
                                Dim sJobCd As String
                                Dim sJobNr As String
                                Dim sRowNr As String
                                Dim oJobTypes As ArrayList = Nothing
                                Dim sStatus As String
                                Dim sWORStatus As String

                                sJobNr = oGridN.doGetFieldValue("r.U_JobNr", iCurrentDataRow)
                                sRowNr = oGridN.doGetFieldValue("r.Code", iCurrentDataRow)
                                sCurrentJobType = oGridN.doGetFieldValue("r.U_JobTp", iCurrentDataRow)
                                sStatus = oGridN.doGetFieldValue("e.U_Status", iCurrentDataRow)
                                sWORStatus = oGridN.doGetFieldValue("r.U_RowSta", iCurrentDataRow)

                                If sStatus.Equals("9") = False Then
                                    oJobTypes = Config.INSTANCE.doGetAvailableWOJobTypes(sJobNr)
                                End If

                                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_DELETE) AndAlso
                                    sWORStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusDeleted()) Then
                                    If iCurrentDataRow >= 0 Then
                                        oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML
                                        oCreationPackage.String = getTranslatedWord("UnDelete Waste Order Row ") & " (" & (iCurrentDataRow + 1) & ")[" & sRowNr & "]"
                                        oCreationPackage.Enabled = True
                                        oMenus.AddEx(oCreationPackage)
                                    End If
                                End If

                                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_ARCHIVE) = False OrElse
                                    Config.INSTANCE.doGetIsSupperUser() Then
                                    If iCurrentDataRow >= 0 Then
                                        oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE
                                        oCreationPackage.String = getTranslatedWord("Archive Row ") & " (" & (iCurrentDataRow + 1) & ")[" & sRowNr & "]"
                                        oCreationPackage.Enabled = True
                                        oMenus.AddEx(oCreationPackage)
                                    End If
                                End If

                                ''If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_) = False OrElse _
                                ''   Config.INSTANCE.doGetIsSupperUser() Then
                                'oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREOPEN
                                'oCreationPackage.String = getTranslatedWord("ReOpen Row")
                                'oCreationPackage.Enabled = True
                                'oMenus.AddEx(oCreationPackage)
                                ''End If

                                ''If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_) = False OrElse _
                                ''    Config.INSTANCE.doGetIsSupperUser() Then
                                'oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUCANCELMARKDOCS
                                'oCreationPackage.String = getTranslatedWord("Cancel Marketing Documents")
                                'oCreationPackage.Enabled = True
                                'oMenus.AddEx(oCreationPackage)
                                ''End If

                                If sWORStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusDeleted()) = False AndAlso
                                            oGridN.doGetDeleteActive() Then
                                    If iCurrentDataRow >= 0 Then
                                        oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML
                                        oCreationPackage.String = getTranslatedWord("Delete Waste Order Row ") & " (" & (iCurrentDataRow + 1) & ")[" & sRowNr & "]"
                                        oCreationPackage.Enabled = True
                                        oMenus.AddEx(oCreationPackage)
                                    End If
                                End If

                                Dim oJobSeq As IDH_JOBTYPE_SEQ = IDH_JOBTYPE_SEQ.getInstance()
                                If Not oJobTypes Is Nothing AndAlso oJobTypes.Count > 0 Then
                                    Dim oMenuList As New ArrayList
                                    Dim iJob As Integer
                                    For iJob = 0 To oJobTypes.Count() - 1
                                        Try
                                            oJobSeq.gotoRow(oJobTypes.Item(iJob))
                                            sJob = oJobSeq.U_JobTp
                                            sJobCd = oJobSeq.Code

                                            Dim oDt(2) As Object
                                            oDt(1) = sJob

                                            ''Filter out the &
                                            Dim iIndex As Integer = sJob.IndexOf("&")
                                            If (iIndex <> -1) Then
                                                sJob = sJob.Substring(0, iIndex) & "And" & sJob.Substring(iIndex + 1)
                                            End If

                                            If sJob.Equals(sCurrentJobType) Then
                                                oCreationPackage.UniqueID = "DUPLICATE"
                                                oCreationPackage.String = getTranslatedWord("Duplicate Waste Order Row ") & " (" & (iCurrentDataRow + 1) & ")[" & sRowNr & "]"
                                                oCreationPackage.Position = iMenuPos
                                                iMenuPos += 1
                                                oMenus.AddEx(oCreationPackage)
                                            End If

                                            Dim sTitle As String = getTranslatedWord("Create a [%1] Order", sJob)
                                            'If sJob.Length > 8 Then
                                            '    sJob = "JB_" & sJob.Substring(0, 6).ToUpper()
                                            '    'sJob = "JB_" & sJob.Substring(sJob.Length - 6, 6).ToUpper()
                                            'Else
                                            '    sJob = "JB_" & sJob.ToUpper()
                                            'End If

                                            'oDt(0) = sJob

                                            If sJobCd.Length > 8 Then
                                                sJobCd = "JB_" & sJobCd.Substring(0, 6).ToUpper()
                                            Else
                                                sJobCd = "JB_" & sJobCd.ToUpper()
                                            End If

                                            oDt(0) = sJobCd
                                            oMenuList.Add(oDt)
                                            If goParent.goApplication.Menus.Exists(sJobCd) = False Then
                                                oCreationPackage.UniqueID = sJobCd
                                                oCreationPackage.String = sTitle
                                                oCreationPackage.Position = iMenuPos
                                                iMenuPos += 1
                                                oMenus.AddEx(oCreationPackage)
                                            End If

                                        Catch
                                        End Try
                                    Next
                                    setWFValue(oForm, "LASTJOBMENU", oMenuList)
                                Else
                                    oCreationPackage.UniqueID = "NEWWO"
                                    oCreationPackage.String = getTranslatedWord("Create a new Waste Order from Current")
                                    oCreationPackage.Position = iMenuPos
                                    iMenuPos += 1
                                    oMenus.AddEx(oCreationPackage)
                                End If
                            End If

                            oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUEDIT
                            oCreationPackage.String = getTranslatedWord("Set &Values for the Selected Items")
                            oCreationPackage.Position = iMenuPos
                            iMenuPos += 1
                            oMenus.AddEx(oCreationPackage)
                        End If
                        Return False
                    End If
                Else
                    'To hide the Popup Menu
                    If goParent.goApplication.Menus.Exists("DUPLICATE") Then
                        goParent.goApplication.Menus.RemoveEx("DUPLICATE")
                    End If
                    If goParent.goApplication.Menus.Exists("New") Then
                        goParent.goApplication.Menus.RemoveEx("New")
                    End If
                    If goParent.goApplication.Menus.Exists("DUPLIMULTI") Then
                        goParent.goApplication.Menus.RemoveEx("DUPLIMULTI")
                    End If
                    Dim oMenuList As ArrayList = getWFValue(oForm, "LASTJOBMENU")
                    Dim sJob As String
                    If Not oMenuList Is Nothing AndAlso oMenuList.Count > 0 Then
                        For iJob As Integer = 0 To oMenuList.Count - 1
                            sJob = oMenuList.Item(iJob)(0)
                            If goParent.goApplication.Menus.Exists(sJob) Then
                                goParent.goApplication.Menus.RemoveEx(sJob)
                            End If
                        Next
                    End If
                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM)
                    End If
                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML)
                    End If

                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUEDIT) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUEDIT)
                    End If
                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML)
                    End If

                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE)
                    End If

                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREOPEN) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREOPEN)
                    End If

                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUCANCELMARKDOCS) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUCANCELMARKDOCS)
                    End If

                    'If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVEM) Then
                    '    goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVEM)
                    'End If

                    If goParent.goApplication.Menus.Exists("NEWWO") Then
                        goParent.goApplication.Menus.RemoveEx("NEWWO")
                    End If
                    setWFValue(oForm, "LASTJOBMENU", Nothing)
                    Return True
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                If pVal.BeforeAction = False Then
                    If pVal.oData.MenuUID.Equals("New") Then
                        doNewOrder(oForm)
                        Return False
                    ElseIf pVal.oData.MenuUID.Equals(IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML) Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                        If oGridN.doReactivateSelection() Then
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        End If
                        Return False
                    ElseIf pVal.oData.MenuUID.Equals(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE) Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                        oGridN.doRequestArchive()
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        Return False
                    ElseIf pVal.oData.MenuUID.Equals(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREOPEN) Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                        oGridN.doRequestReOpen()
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        Return False
                    ElseIf pVal.oData.MenuUID.Equals(IDHAddOns.idh.controls.IDHGrid.GRIDMENUCANCELMARKDOCS) Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                        oGridN.doRequestCancelMarketingDocs()
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        Return False
                    ElseIf pVal.oData.MenuUID.Equals("NEWWO") Then
                        doNewOrderFromCurrent(oForm)
                        Return False
                    ElseIf pVal.oData.MenuUID.Equals("DUPLIMULTI") Then
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows()
                        If Not oSelected Is Nothing AndAlso oSelected.Count > 1 Then
                            Dim iNoOfCopies As Integer = 1
                            Dim bCopyADDI As String = Config.Parameter("WOADDITM", False)
                            Dim bCopyComm As String = Config.Parameter("WOCOMENT", False)
                            Dim bCopyPrice As String = Config.Parameter("WOCOPYPR", False)
                            Dim bCopyQty As String = Config.Parameter("WOCOPYQT", False)
                            Dim bCopyVehReg As String = Config.Parameter("WOCPYVHR", False)
                            Dim bCopyDriver As String = Config.Parameter("WOCPYDRV", False)
                            Dim sNewCode As String = doDuplicate(oForm, bCopyComm, bCopyADDI, bCopyPrice, bCopyQty, bCopyVehReg, bCopyDriver, "")
                            doReLoadData(oForm, True)
                        End If
                        Return False
                    ElseIf pVal.oData.MenuUID.Equals("DUPLICATE") Then
                        'Check if this is a valid request
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows()
                        If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then
                            'OnTime [#Ico000????] USA 
                            'START
                            If Config.INSTANCE.getParameterAsBool("MDDUWA", False) = True Then
                                setSharedData(oForm, "SETDUP", "TRUE")
                                goParent.doOpenModalForm("IDH_DUPLICATE", oForm)
                            Else
                                Dim iNoOfCopies As Integer = com.idh.utils.Conversions.ToInt(Config.ParameterWithDefault("WODUPLNO", "1"))
                                Dim bCopyADDI As String = Config.Parameter("WOADDITM", False)
                                Dim bCopyComm As String = Config.Parameter("WOCOMENT", False)
                                Dim bCopyPrice As String = Config.Parameter("WOCOPYPR", False)
                                Dim bCopyQty As String = Config.Parameter("WOCOPYQT", False)

                                Dim bCopyVehReg As String = Config.Parameter("WOCPYVHR", False)
                                Dim bCopyDriver As String = Config.Parameter("WOCPYDRV", False)

                                If iNoOfCopies < 1 Then
                                    iNoOfCopies = 1
                                End If
                                For cnt As Integer = 1 To iNoOfCopies
                                    Dim sNewCode As String = doDuplicate(oForm, bCopyComm, bCopyADDI, bCopyPrice, bCopyQty, bCopyVehReg, bCopyDriver, "")
                                Next
                                doReLoadData(oForm, True)
                            End If
                        End If
                        Return False
                    ElseIf pVal.oData.MenuUID.StartsWith("JB_") Then
                        Dim oMenuList As ArrayList = getWFValue(oForm, "LASTJOBMENU")
                        Dim sMenuItem As String
                        Dim sJob As String
                        If Not oMenuList Is Nothing AndAlso oMenuList.Count > 0 Then
                            For iJob As Integer = 0 To oMenuList.Count - 1
                                sMenuItem = oMenuList.Item(iJob)(0)
                                If pVal.oData.MenuUID.Equals(sMenuItem) Then
                                    sJob = oMenuList.Item(iJob)(1)

                                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                                    oGridN.setCurrentLineByClick(pVal.Row)
                                    '..
                                    'Dim sCustomer As String = oGridN.doGetFieldValue("e.U_CardCd")
                                    'If Config.INSTANCE.doCheckCredit(goParent, sCustomer, 0, True) = True Then
                                    '## Start 25-09-2013
                                    'Change Copy Comment argument to True-->Bug List Issue#165
                                    Dim sRowCode As String = doDuplicate(oForm, True, False, False, False, False, False, "", sJob)
                                    '## End
                                    If Not sRowCode Is Nothing AndAlso sRowCode.Length > 0 Then
                                        If sRowCode.Length > 0 Then
                                            Try
                                                Dim oOrderRow As com.isb.core.forms.OrderRow = New com.isb.core.forms.OrderRow(oForm.UniqueID, sRowCode)
                                                oOrderRow.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                                                oOrderRow.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                                                oOrderRow.doShowModal()
                                            Catch Ex As Exception
                                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                                            End Try

                                            'Dim oData As New ArrayList
                                            'oData.Add("DoAuto")     ''index: 0
                                            'oData.Add(sRowCode)     ''index: 1
                                            'oData.Add(oGridN.doGetFieldValue("e.U_BDate"))      ''index: 2
                                            'oData.Add(goParent.doTimeStrToStr(oGridN.doGetFieldValue("e.U_BTime")))     ''index: 3
                                            'oData.Add(oGridN.doGetFieldValue("e.U_ZpCd"))       ''index: 4
                                            'oData.Add(oGridN.doGetFieldValue("e.U_Address"))        ''index: 5
                                            'oData.Add("CANNOTDOCOVERAGE")       ''index: 6
                                            'oData.Add(oGridN.doGetFieldValue("e.U_SteId"))      ''index: 7

                                            ''Added to handle the BP Switching
                                            'oData.Add(Nothing)      ''index: 8
                                            'oData.Add(oGridN.doGetFieldValue("e.U_PAddress"))       ''index: 9
                                            'oData.Add(oGridN.doGetFieldValue("e.U_PZpCd"))      ''index: 10
                                            'oData.Add(oGridN.doGetFieldValue("e.U_PPhone1"))        ''index: 11
                                            'oData.Add(oGridN.doGetFieldValue("e.U_FirstBP"))        ''index: 12

                                            ''Adding WO Header status for TFS 
                                            'oData.Add(getWOHStatus(oForm, oGridN.doGetFieldValue("r.U_JobNr")))     ''index: 13

                                            ''Adding Flag to trigger Additional Expenses row addition 
                                            'oData.Add("INSERTADDEXP")       ''index: 14

                                            'goParent.doOpenModalForm("IDHJOBS", oForm, oData)
                                        End If
                                    End If
                                    'End If
                                    Return False
                                End If
                            Next
                        End If
                    ElseIf pVal.oData.MenuUID.Equals("IDHSORTA") AndAlso (oForm.TypeEx.IndexOf("IDHJOBR") > -1 OrElse
                              oForm.TypeEx.IndexOf("IDHWOBI") > -1) Then
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        Dim sFilter As String = oGridN.getRequiredFilter()
                        doSetStatusFilter(oGridN)
                        oGridN.doSort(pVal.ColUID, "ASC")
                        oGridN.setRequiredFilter(sFilter)
                        Return False
                    ElseIf pVal.oData.MenuUID.Equals("IDHSORTD") AndAlso (oForm.TypeEx.IndexOf("IDHJOBR") > -1 OrElse
                              oForm.TypeEx.IndexOf("IDHWOBI") > -1) Then
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        Dim sFilter As String = oGridN.getRequiredFilter()
                        doSetStatusFilter(oGridN)
                        oGridN.doSort(pVal.ColUID, "DESC")
                        oGridN.setRequiredFilter(sFilter)
                        Return False
                    Else
                        If pVal.oData.MenuUID.Equals(IDHAddOns.idh.controls.IDHGrid.GRIDMENUEDIT) Then
                            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                            ''##MA Start 16-09-2015 Issue#831 'do not allow completed job to edit
                            If oGridN.getSBOGrid().Rows.SelectedRows.Count > 0 Then
                                For iRow As Int16 = 0 To oGridN.getSBOGrid().Rows.SelectedRows.Count - 1
                                    Dim sProgress As String = oGridN.doGetFieldValue("Progress", CInt(oGridN.getSBOGrid().Rows.SelectedRows.Item(iRow, SAPbouiCOM.BoOrderType.ot_RowOrder)))
                                    If sProgress IsNot Nothing AndAlso (sProgress.Trim.ToUpper = com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.Trim _
                                        OrElse sProgress.Trim.ToUpper = com.idh.bridge.lookups.FixedValues.getProgressInPlanning(True).ToUpper.Trim) Then
                                        If Config.INSTANCE.getParameterWithDefault("OSMOPNCL", "").Trim <> String.Empty Then
                                            Dim sEditableFields() As String = Config.INSTANCE.getParameterWithDefault("OSMOPNCL", "").Trim.Split(",")
                                            For iColList As Int16 = 0 To sEditableFields.Length - 1
                                                If oGridN.Columns.Item(pVal.ColUID).TitleObject.Caption = sEditableFields(iColList) Then
                                                    'If oGridN.doCheckIsSameCol(pVal.ColUID, sEditableFields(iColList)) Then
                                                    Return False
                                                End If
                                            Next
                                        End If
                                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("You cannot edit a completed job.", "ERJBCOMP", Nothing)
                                        Return False
                                    End If
                                Next
                            End If
                            ''##MA End 16-09-2015 Issue#831 'do not allow completed job to edit
                            If oGridN.getSBOGrid().Rows.SelectedRows.Count = 1 Then
                                oGridN.setCurrentLineByClick(pVal.Row)

                                Dim sVehReg As String = oGridN.doGetFieldValue("r.U_Lorry")
                                setSharedData(oForm, "IDH_VEHREG", sVehReg)

                                Dim sCarrCd As String = oGridN.doGetFieldValue("r.U_CarrCd")
                                Dim sCarrName As String = oGridN.doGetFieldValue("r.U_CarrNm")
                                Dim sDriver As String = oGridN.doGetFieldValue("r.U_Driver")
                                Dim sCarrierRefNo As String = oGridN.doGetFieldValue("r.U_SupRef")
                                ''MA Start 23-02-2015 Issue#256
                                Dim sCustRefNo As String = oGridN.doGetFieldValue("r.U_CustRef")
                                Dim sCustCode As String = oGridN.doGetFieldValue("e.U_CardCd")
                                Dim sCustAddress As String = oGridN.doGetFieldValue("e.U_Address")
                                ''MA End 23-02-2015 Issue#256

                                setSharedData(oForm, "IDH_CARCD", sCarrCd)
                                setSharedData(oForm, "IDH_CARNM", sCarrName)
                                setSharedData(oForm, "IDH_DRIVER", sDriver)
                                setSharedData(oForm, "IDH_CARREF", sCarrierRefNo)

                                setSharedData(oForm, "IDH_OLDVR", sVehReg)
                                setSharedData(oForm, "IDH_OLDCC", sCarrCd)

                                ''MA Start 23-02-2015 Issue#256
                                setSharedData(oForm, "IDH_CUSTCD", sCustCode)
                                setSharedData(oForm, "IDH_CUSREF", sCustRefNo)
                                setSharedData(oForm, "IDH_CUSADD", sCustAddress)
                                ''MA End 23-02-2015 Issue#256
                            End If
                            goParent.doOpenModalForm("IDH_OSMEDIT", oForm)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        End If
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FORMAT_FIELDS Then
                If pVal.BeforeAction = False Then
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
                If pVal.BeforeAction = True Then
                    Dim sField As String
                    sField = pVal.oData

                    Dim sValueB As String
                    Dim sValueA As String
                    If sField.Equals("r.U_Lorry") Then

                        Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, pVal.ItemUID)

                        sValueB = oGridN.doGetFieldValue(sField)
                        If sValueB.Length > 0 Then
                            If sValueB = "*" Then
                                sValueA = ""
                            Else
                                sValueA = com.idh.bridge.utils.General.doFixReg(sValueB)
                            End If
                            ''Dim sCode As String = oGridN.doGetFieldValue("r.Code")
                            Dim sCurrentVehReg As String = oGridN.moLastFieldValue '  oGridN.doSetFieldValue("r.U_Lorry", oGridN.moLastFieldValue)

                            If Config.INSTANCE.getParameterAsBool("OSMVMSUC", True) Then
                                Dim sCustomer As String = oGridN.doGetFieldValue("e.U_CardCd")
                                setSharedData(oForm, "IDH_VEHCUS", sCustomer)
                            Else
                                setSharedData(oForm, "IDH_VEHCUS", "")
                            End If

                            setSharedData(oForm, "IDH_CURVEHREG", sCurrentVehReg)
                            setSharedData(oForm, "IDH_VEHREG", sValueA)
                            'setSharedData(oForm, "IDH_VEHCUS", sCustomer)
                            setSharedData(oForm, "IDH_WR1OT", "^WO")
                            setSharedData(oForm, "SHOWACTIVITY", Config.Parameter("WOSVAC"))
                            setSharedData(oForm, "SILENT", "SHOWMULTI")
                            goParent.doOpenModalForm("IDHLOSCH", oForm)
                        End If
                    ElseIf sField.Equals("r.U_IDHRTCD") Then
                        Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, pVal.ItemUID)
                        Dim sValue As String = oGridN.doGetFieldValue("r.U_IDHRTCD")
                        If sValue.Length() = 0 Then
                            doChooseRoute(oForm, oGridN)
                        End If
                    End If
                    doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = True Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                        'oGridN.doCheckAndDeleteSelection()
                        oGridN.doCommentDeletion()
                        Return False

                        'If oGridN.doCheckCanDeleteSelection() = False Then
                        '    Return False
                        'Else
                        '	oGridN.doMarkSelectionAsDeleted()
                        '	Return False
                        'End If
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL_MULTI Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = True Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                        'oGridN.doCheckAndDeleteSelection()
                        oGridN.doCommentDeletion()
                        Return False

                        'If oGridN.doCheckCanDeleteSelection() = False Then
                        '    Return False
                        'Else
                        '	oGridN.doMarkSelectionAsDeleted()
                        '	Return False
                        'End If
                    End If
                End If

            End If

            Return True
        End Function
        Public Overrides Function doMenuEvent(oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction AndAlso oForm.ActiveItem = "LINESGRID" AndAlso (pVal.MenuUID.Equals("771") OrElse pVal.MenuUID.Equals("773") OrElse pVal.MenuUID.Equals("774")) Then
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                ''##MA Start 16-09-2015 Issue#831 'do not allow completed job to edit
                If oGridN.getSBOGrid().Rows.SelectedRows.Count > 0 Then
                    For iRow As Int16 = 0 To oGridN.getSBOGrid().Rows.SelectedRows.Count - 1
                        Dim sProgress As String = oGridN.doGetFieldValue("Progress", CInt(oGridN.getSBOGrid().Rows.SelectedRows.Item(iRow, SAPbouiCOM.BoOrderType.ot_RowOrder)))
                        If sProgress IsNot Nothing AndAlso sProgress.Trim.ToUpper = com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.Trim Then
                            If Config.INSTANCE.getParameterWithDefault("OSMOPNCL", "").Trim <> String.Empty Then
                                Dim sEditableFields() As String = Config.INSTANCE.getParameterWithDefault("OSMOPNCL", "").Trim.Split(",")
                                For iColList As Int16 = 0 To sEditableFields.Length - 1
                                    If oGridN.Columns.Item(pVal.ColUID).TitleObject.Caption = sEditableFields(iColList) Then
                                        'If oGridN.doCheckIsSameCol(pVal.ColUID, sEditableFields(iColList)) Then
                                        Return False
                                    End If
                                Next
                            End If
                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("You cannot edit a completed job.", "ERJBCOMP", Nothing)
                            BubbleEvent = False
                        End If
                    Next
                End If
                ''##MA End 16-09-2015 Issue#831 'do not allow completed job to edit
            End If

            Return MyBase.doMenuEvent(oForm, pVal, BubbleEvent)
        End Function
        Public Overrides Function doRightClickEvent(oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean) As Boolean
            Return (MyBase.doRightClickEvent(oForm, pVal, BubbleEvent))
        End Function

        '        Protected Overridable Function doCheckAndDeleteRow(ByVal oGridN As IDHGrid) As Boolean
        '            Dim iSRow As Integer
        '            Dim iRow As Integer
        '            Dim bDoContinue As Boolean = False
        '            oGridN.getSBOForm.Freeze(True)
        '            Try
        '                Dim oSelectedRows As SAPbouiCOM.SelectedRows
        '                oSelectedRows = oGridN.getSBOGrid().Rows.SelectedRows()
        '
        '                Dim iIndex As Integer
        '                Dim sList As String = ""
        '                Dim oaRemove As New ArrayList
        '                Dim iSize As Integer = oSelectedRows.Count
        '
        '                If iSize = 0 Then Return True
        '                For iIndex = 0 To iSize - 1
        '                    iSRow = oSelectedRows.Item(iIndex, SAPbouiCOM.BoOrderType.ot_RowOrder)
        '                    iRow = oGridN.getSBOGrid().GetDataTableRowIndex(iSRow)
        '                    'iRow = oGridN.getDataTableRowIndex(oSelectedRows, iIndex)
        '
        '                    Dim sSOStatus As String = oGridN.doGetFieldValue("r.U_Status", iRow)
        '                    Dim sPOStatus As String = oGridN.doGetFieldValue("r.U_PStat", iRow)
        '                    Dim sWROrd As String = oGridN.doGetFieldValue("r.U_WROrd", iRow)
        '                    Dim sWRRow As String = oGridN.doGetFieldValue("r.U_WRRow", iRow)
        '
        '                    If sSOStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusDeleted()) OrElse _
        '                    	sSOStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) OrElse _
        '                        sSOStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusInvoiced()) OrElse _
        '                        sPOStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) OrElse _
        '                        sPOStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusInvoiced()) OrElse _
        '                        (Not sWROrd Is Nothing AndAlso sWROrd.Length > 0) OrElse _
        '                        (Not sWRRow Is Nothing AndAlso sWRRow.Length > 0) Then
        '                        oaRemove.Add(iSRow)
        '                    Else
        '                        bDoContinue = True
        '                    End If
        '                Next
        '
        '                If oaRemove.Count > 0 Then
        '                    For i As Integer = 0 To oaRemove.Count - 1
        '                        If i > 0 Then
        '                            sList = sList & ","
        '                        End If
        '                        sList = sList & (oaRemove.Item(i) + 1)
        '                        oSelectedRows.Remove(oaRemove.Item(i))
        '                    Next
        '                    Dim sRows As String = "row"
        '                    If oaRemove.Count > 0 Then
        '                        sRows = sRows & "s"
        '                    End If
        '                    com.idh.bridge.DataHandler.INSTANCE.doError("The selected " & sRows & " has been processed or has been linked to a DO and can not be deleted: " & sList)
        '                End If
        '            Catch ex As Exception
        '                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error checking for delete - " & iSRow)
        '            Finally
        '                oGridN.getSBOForm.Freeze(False)
        '            End Try
        '
        '            Return bDoContinue
        '        End Function

        'Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        '    If sModalFormType = "IDHJOBS" OrElse sModalFormType = "IDHJOBE" OrElse sModalFormType = "IDH_WASTORD" Then
        '        doReLoadData(oForm, True)
        '    End If
        'End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType.Equals("IDH_COMMFRM") Then
                    Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                    oGridN.doCheckAndDeleteSelection(getSharedData(oForm, "CMMNT"))
                ElseIf sModalFormType = "IDHLOSCH" Then
                    'setWFValue(oForm, "CheckReg", False)
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                    'Dim iRow As Integer = oGridN.getCurrentLine()
                    Dim sNewReg As String = getSharedData(oForm, "REG")
                    Dim sVehDesc As String = getSharedData(oForm, "VEHDESC")
                    Dim sDriver As String = getSharedData(oForm, "DRIVER")
                    Dim sOldCarrierCd As String = oGridN.doGetFieldValue("r.U_CarrCd")
                    Dim sNewCarrierCd As String = getSharedData(oForm, "WCCD")
                    Dim sNewCarrierNm As String = getSharedData(oForm, "WCNAM")
                    Dim sOldVehReg As String = oGridN.moLastFieldValue 'getSharedData(oForm, "IDH_CURVEHREG")
                    If sOldVehReg <> "" Then
                        If (sOldVehReg <> sNewReg) Then
                            If (sOldCarrierCd <> sNewCarrierCd) Then
                                If Config.INSTANCE.getParameterAsBool("SETVEHDT", False) Then
                                    goParent.doMessage("Warning: Changing Vehicle Reg. No., may have changed Carrier details on selected row(s). Make sure you review each WOR and action acordingly. Thank you.")
                                End If
                            End If
                        Else
                            If (sOldCarrierCd <> sNewCarrierCd) Then
                                If Config.INSTANCE.getParameterAsBool("SETVEHDT", False) Then
                                    goParent.doMessage("Warning: Changing Vehicle Reg. No., may have changed Carrier details on selected row(s). Make sure you review each WOR and action acordingly. Thank you.")
                                End If
                            End If
                        End If
                    End If
                    oGridN.doSetFieldValue("r.U_Lorry", sNewReg)
                    oGridN.doSetFieldValue("r.U_VehTyp", sVehDesc)
                    oGridN.doSetFieldValue("r.U_Driver", sDriver)
                    oGridN.doSetFieldValue("r.U_CarrCd", sNewCarrierCd)
                    oGridN.doSetFieldValue("r.U_CarrNm", sNewCarrierNm)

                ElseIf sModalFormType = "IDH_OSMEDIT" Then
                    doSetValues(oForm)
                ElseIf sModalFormType = "IDHROSRC" Then
                    Dim oGridN As UpdateGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                    Dim sRoute As String = getSharedData(oForm, "IDH_RTCODE")
                    Dim iSeq As String = getSharedData(oForm, "IDH_RTSEQ")

                    oGridN.doSetFieldValue("r.U_IDHRTCD", sRoute)
                    oGridN.doSetFieldValue("r.U_IDHSEQ", iSeq)

                    doClearSharedData(oForm)
                ElseIf sModalFormType = "IDHPAYMET" Then
                    oForm.Freeze(True)
                    Try
                        Dim oToSelectPayment As ArrayList = getSharedData(oForm, "ROWS")
                        If Not oToSelectPayment Is Nothing Then
                            Dim sPayMeth As String = getSharedData(oForm, "METHOD")
                            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                            Dim iIndex As Integer
                            Dim dTotal As Double = 0
                            Dim sCustomerCd As String = Nothing
                            Dim sCustomerNM As String = Nothing
                            Dim bDoContinue As Boolean = True

                            'First check the Customers if teh Payment Method is CC
                            If sPayMeth.Equals(Config.INSTANCE.getCreditCardName()) Then
                                For iIndex = 0 To oToSelectPayment.Count - 1
                                    If iIndex = 0 Then
                                        sCustomerCd = oGridN.doGetFieldValue("e.U_CardCd", oToSelectPayment(iIndex))
                                        sCustomerNM = oGridN.doGetFieldValue("e.U_CardNM", oToSelectPayment(iIndex))
                                    Else
                                        If sCustomerCd.Equals(oGridN.doGetFieldValue("e.U_CardCd", oToSelectPayment(iIndex))) = False Then
                                            'com.idh.bridge.DataHandler.INSTANCE.doError("Can not process a CreditCard payment with multiple customers in the selection.")
                                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Can not process a CreditCard payment with multiple customers in the selection.", "ERUSPCP", {Nothing})
                                            bDoContinue = False
                                            Exit For
                                        End If
                                    End If
                                    dTotal = dTotal + oGridN.doGetFieldValue("r.U_Total", oToSelectPayment(iIndex))
                                Next
                            End If

                            If bDoContinue Then
                                For iIndex = 0 To oToSelectPayment.Count - 1
                                    oGridN.doForceFieldValue("r.U_PayStat", oToSelectPayment(iIndex), com.idh.bridge.lookups.FixedValues.getStatusPaid())
                                    oGridN.doForceFieldValue("r.U_PayMeth", oToSelectPayment(iIndex), sPayMeth)
                                    oGridN.doForceFieldValue("r.U_CCNum", oToSelectPayment(iIndex), "")
                                    oGridN.doForceFieldValue("r.U_CCType", oToSelectPayment(iIndex), "0")
                                    oGridN.doForceFieldValue("r.U_CCStat", oToSelectPayment(iIndex), "")
                                    oGridN.doForceFieldValue("r.U_CCIs", oToSelectPayment(iIndex), "")
                                    oGridN.doForceFieldValue("r.U_CCSec", oToSelectPayment(iIndex), "")
                                    oGridN.doForceFieldValue("r.U_CCHNum", oToSelectPayment(iIndex), "")
                                    oGridN.doForceFieldValue("r.U_CCPCd", oToSelectPayment(iIndex), "")
                                Next

                                If sPayMeth.Equals(Config.INSTANCE.getCreditCardName()) Then
                                    setSharedData(oForm, "ROWS", oToSelectPayment)
                                    setSharedData(oForm, "CUSTCD", sCustomerCd)
                                    setSharedData(oForm, "CUSTNM", sCustomerNM)
                                    setSharedData(oForm, "AMOUNT", dTotal)
                                    goParent.doOpenModalForm("IDHCCPAY", oForm)
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal results - " & sModalFormType)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
                    End Try
                    oForm.Freeze(False)
                ElseIf sModalFormType = "IDHCCPAY" Then
                    Dim sCCStatPre As String
                    Dim sPStat As String
                    Dim sCCNum As String
                    Dim sCCType As String
                    Dim sCCStat As String

                    Dim sCCIs As String
                    Dim sCCSec As String
                    Dim sCCHNm As String
                    Dim sCCPCd As String
                    Dim sCCExp As String

                    oForm.Freeze(True)
                    Try
                        sCCNum = getSharedData(oForm, "CCNUM")
                        sPStat = getSharedData(oForm, "PSTAT")
                        sCCType = getSharedData(oForm, "CCTYPE")
                        sCCStat = getSharedData(oForm, "CCSTAT")

                        sCCIs = getSharedData(oForm, "CCIs")
                        sCCSec = getSharedData(oForm, "CCSec")
                        sCCHNm = getSharedData(oForm, "CCHNm")
                        sCCPCd = getSharedData(oForm, "CCPCd")
                        sCCExp = getSharedData(oForm, "CCEXP")

                        If sPStat.Equals(com.idh.bridge.lookups.FixedValues.getStatusPaid()) Then
                            sCCStatPre = "APPROVED: "
                        Else
                            sCCStatPre = "DECLINED: "
                        End If

                        Dim oToSelectPayment As ArrayList = getSharedData(oForm, "ROWS")
                        If Not oToSelectPayment Is Nothing Then
                            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                            Dim iIndex As Integer
                            For iIndex = 0 To oToSelectPayment.Count - 1
                                oGridN.doForceFieldValue("r.U_CCNum", oToSelectPayment(iIndex), sCCNum)
                                oGridN.doForceFieldValue("r.U_CCType", oToSelectPayment(iIndex), sCCType)
                                oGridN.doForceFieldValue("r.U_CCStat", oToSelectPayment(iIndex), sCCStatPre & sCCStat)
                                oGridN.doForceFieldValue("r.U_PayStat", oToSelectPayment(iIndex), sPStat)
                                oGridN.doForceFieldValue("r.U_CCExp", oToSelectPayment(iIndex), sCCExp)
                                oGridN.doForceFieldValue("r.U_CCIs", oToSelectPayment(iIndex), sCCIs)
                                oGridN.doForceFieldValue("r.U_CCSec", oToSelectPayment(iIndex), sCCSec)
                                oGridN.doForceFieldValue("r.U_CCHNum", oToSelectPayment(iIndex), sCCHNm)
                                oGridN.doForceFieldValue("r.U_CCPCd", oToSelectPayment(iIndex), sCCPCd)
                            Next
                        End If
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal results - " & sModalFormType)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})

                    End Try
                    oForm.Freeze(False)
                    'OnTime [#Ico000????] USA 
                    'START
                ElseIf sModalFormType = "IDH_DUPLICATE" Then
                    Dim sSetDup As String = getSharedData(oForm, "SETDUP")
                    If sSetDup.ToUpper() = "TRUE" Then
                        Dim iNoOfCopies As Integer = Convert.ToInt16(getSharedData(oForm, "IDHNOCOPY"))
                        Dim bCopyADDI As Boolean = Convert.ToBoolean(getSharedData(oForm, "IDHCHKADI"))
                        Dim bCopyComm As Boolean = Convert.ToBoolean(getSharedData(oForm, "IDHCHKCOM"))
                        Dim bCopyPrice As Boolean = Convert.ToBoolean(getSharedData(oForm, "IDHCOPYPR"))
                        Dim bCopyQty As Boolean = Convert.ToBoolean(getSharedData(oForm, "IDHCOPYQT"))
                        Dim bCopyVehReg As Boolean = Convert.ToBoolean(getSharedData(oForm, "IDHVEHREG"))
                        Dim bCopyDriver As Boolean = Convert.ToBoolean(getSharedData(oForm, "IDHDRIVER"))
                        Dim sReqDate As String = getSharedData(oForm, "IDHREQDT")

                        If iNoOfCopies > 0 And iNoOfCopies < Convert.ToInt16(Config.Parameter("MXDUPCNT")) Then
                            For cnt As Integer = 1 To iNoOfCopies
                                Dim sNewCode As String = doDuplicate(oForm, bCopyComm, bCopyADDI, bCopyPrice, bCopyQty, bCopyVehReg, bCopyDriver, sReqDate, Nothing, True)
                            Next
                            doReLoadData(oForm, True)
                        Else
                            doWarnMess("No. of copies exceeds 'MXDUPCNT' WR Config Key.", SAPbouiCOM.BoMessageTime.bmt_Short)
                        End If
                    Else
                    End If
                    'END
                    'OnTime [#Ico000????] USA 
                    ''## MA Start 01-09-2014 Issue#421
                ElseIf sModalFormType = "IDHBRANCHSRC" Then
                    Dim sCodes As String = getSharedData(oForm, "IDH_BRANCSELCD")
                    Dim sDesc As String = getSharedData(oForm, "IDH_BRANCSELNM")
                    If sDesc = "" Then
                        Return
                    End If
                    setUFValue(oForm, "IDH_BRANC1", sDesc)
                    setUFValue(oForm, "IDH_BRANC2", sCodes)
                    ''## MA End 01-09-2014 Issue#421
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal results - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        Protected Overrides Sub doReturnCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            Try
                If sModalFormType = "IDHLOSCH" Then
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                    oGridN.doSetFieldValue("r.U_Lorry", oGridN.moLastFieldValue)
                ElseIf sModalFormType = "IDHJOBS" Then
                    doReLoadData(oForm, True)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error handeling the cancel - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCF", {sModalFormType})
            End Try
        End Sub

        Public Function Handler_WOROKReturn(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            doReLoadData(oOForm.SBOParentForm, True)
            Return True
        End Function

        Public Function Handler_WORCancelReturn(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            'doReLoadData(oOForm.SBOParentForm, True)
            Return True
        End Function

        Public Sub doSetValues(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                Dim sVal As String
                'Dim oClosedList As ArrayList = getWFValue(oForm, "CLOSEDWOH")
                Dim oSelected As SAPbouiCOM.SelectedRows
                Dim sWOR As String
                Dim sWOH As String
                Dim oEDate As Date
                oSelected = oGridN.getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim iCurrentDataRow As Integer
                    Dim bFound As Boolean = False
                    For iIndex As Integer = 0 To oSelected.Count - 1
                        'iRow = oSelected.Item(iIndex, SAPbouiCOM.BoOrderType.ot_RowOrder)
                        'iRow = oGridN.getSBOGrid().GetDataTableRowIndex(iRow)
                        iCurrentDataRow = oGridN.getDataTableRowIndex(oSelected, iIndex)

                        sWOH = oGridN.doGetFieldValue("r.U_JobNr", iCurrentDataRow)
                        sWOR = oGridN.doGetFieldValue("r.Code", iCurrentDataRow)

                        sVal = getSharedData(oForm, "RDATE")
                        If sVal.Equals("IGNORE") = False Then
                            oGridN.doSetFieldValue("r.U_RDate", iCurrentDataRow, com.idh.utils.Dates.doStrToDate(sVal))
                        End If

                        sVal = getSharedData(oForm, "RTIMF")
                        If sVal.Equals("IGNORE") = False Then
                            oGridN.doSetFieldValue("r.U_RTime", iCurrentDataRow, sVal)
                        End If

                        sVal = getSharedData(oForm, "RTIMT")
                        If sVal.Equals("IGNORE") = False Then
                            oGridN.doSetFieldValue("r.U_RTimeT", iCurrentDataRow, sVal)
                        End If

                        Dim bDoDate As Boolean = False
                        sVal = getSharedData(oForm, "ASDATE")
                        If sVal.Equals("IGNORE") = False Then
                            bDoDate = True
                            oGridN.doSetFieldValue("r.U_ASDate", iCurrentDataRow, com.idh.utils.Dates.doStrToDate(sVal))
                        End If

                        sVal = getSharedData(oForm, "AEDATE")
                        If sVal.Equals("IGNORE") = False Then
                            bDoDate = True
                            oEDate = com.idh.utils.Dates.doStrToDate(sVal)
                            oGridN.doSetFieldValue("r.U_AEDate", iCurrentDataRow, oEDate)
                        Else
                            oEDate = Date.MinValue
                        End If

                        If bDoDate Then
                            Dim dStartDate As Date = oGridN.doGetFieldValue("r.U_ASDate")
                            Dim dEndDate As Date = oGridN.doGetFieldValue("r.U_AEDate")

                            Config.INSTANCE.doCheckDatesMonths(dStartDate, dEndDate)
                        End If

                        sVal = getSharedData(oForm, "REG")
                        If sVal.Equals("IGNORE") = False Then
                            oGridN.doSetFieldValue("r.U_Lorry", iCurrentDataRow, sVal)
                            '## Start 16-09-2013 It was inside of If Block of following Key; Where as if Vahicle Reg No is changed then its other info should also
                            '' change so move following line out of Key's If Block
                            oGridN.doSetFieldValue("r.U_LorryCd", iCurrentDataRow, getSharedData(oForm, "VEHCODE"))
                            oGridN.doSetFieldValue("r.U_VehTyp", iCurrentDataRow, getSharedData(oForm, "VEHDESC"))
                            ''## End
                            'Set value for Vehicle Registration and Carrier Details etc. as well 
                            If Config.INSTANCE.getParameterAsBool("SETVEHDT", False) Then


                                oGridN.doSetFieldValue("r.U_CarrCd", iCurrentDataRow, getSharedData(oForm, "WCSCD"))
                                oGridN.doSetFieldValue("r.U_CarrNm", iCurrentDataRow, getSharedData(oForm, "WCSNM"))
                                '## Start 12-09-2013
                                oGridN.doSetFieldValue("r.U_SupRef", iCurrentDataRow, getSharedData(oForm, "WCRFNM"))
                                '## End
                            End If
                        End If

                        sVal = getSharedData(oForm, "DRIVER")
                        If sVal.Equals("IGNORE") = False Then
                            oGridN.doSetFieldValue("r.U_Driver", iCurrentDataRow, sVal)
                        End If

                        sVal = getSharedData(oForm, "ROUTECD")
                        If sVal.Equals("IGNORE") = False Then
                            oGridN.doSetFieldValue("r.U_IDHRTCD", iCurrentDataRow, sVal)
                        End If

                        sVal = getSharedData(oForm, "CONNO")
                        If sVal.Equals("IGNORE") = False Then
                            oGridN.doSetFieldValue("r.U_ConNum", iCurrentDataRow, sVal)
                        End If

                        ''MA Start 23-02-2015 Issue#256
                        sVal = getSharedData(oForm, "CUSREF")
                        If sVal.Equals("IGNORE") = False Then
                            oGridN.doSetFieldValue("r.U_CustRef", iCurrentDataRow, sVal)
                        End If
                        ''MA End 23-02-2015 Issue#256
                        '			            If com.idh.utils.dates.isValidDate( oEDate ) AndAlso Not sWOH Is Nothing AndAlso Not sWOR Is Nothing Then
                        '							If oClosedList Is Nothing Then
                        '								oClosedList = New ArrayList()
                        '								setWFValue(oForm, "CLOSEDWOH", oClosedList)
                        '								oClosedList.Add(sWOH)
                        '							ElseIf oClosedList.Contains( sWOH ) = False Then
                        '								oClosedList.Add( sWOH )
                        '							End If
                        '			            End If
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the values.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSV", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Private Function doDuplicate(ByVal oForm As SAPbouiCOM.Form, ByVal bCopyComments As Boolean, ByVal bCopyADDI As Boolean, ByVal bCopyPrice As Boolean, ByVal bCopyQty As Boolean, ByVal bCopyVehReg As Boolean, ByVal bCopyDriver As Boolean, ByVal sReqDate As String, Optional ByVal sJob As String = Nothing, Optional ByVal bFromOptions As Boolean = False) As String
            Dim sNewCode As String = Nothing

            Dim oTableSrc As SAPbobsCOM.UserTable = Nothing
            Try
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                oTableSrc = goParent.goDICompany.UserTables.Item("IDH_JOBSHD")
                oSelected = oGridN.getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim iCurrentDataRow As Integer
                    'Dim bCopyComments As Boolean = False
                    Dim sRowCode As String
                    Dim sWOCode As String
                    Dim sCustomer As String
                    Dim sJobType As String = sJob
                    Dim sItemGrp As String

                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iCurrentDataRow = oGridN.getDataTableRowIndex(oSelected, iIndex)

                        sRowCode = oGridN.doGetFieldValue("r.Code", iCurrentDataRow)
                        sWOCode = oGridN.doGetFieldValue("r.U_JobNr", iCurrentDataRow)
                        sCustomer = oGridN.doGetFieldValue("e.U_CardCd", iCurrentDataRow)
                        sItemGrp = oGridN.doGetFieldValue("r.U_ItmGrp", iCurrentDataRow)
                        If sJobType Is Nothing Then
                            sJobType = oGridN.doGetFieldValue("r.U_JobTp", iCurrentDataRow)
                        End If

                        If Config.INSTANCE.doCheckWOIsJobAvail(sWOCode, sItemGrp, sJobType) = True Then
                            Dim sAdditionalMessage As String = getTranslatedWord("Creating a Job for Customer") &
                               "  - " & sCustomer & " " &
                               getTranslatedWord("from row") & " - " & sRowCode

                            Dim bCChkPass As Boolean = False
                            'CR 20150601: The Credit Check build for USA Clients will now be used for all under new WR Config Key "NEWCRCHK" 
                            'If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                            If Config.INSTANCE.getParameterAsBool("NEWCRCHK", False) Then
                                'OnTime [#Ico000????] USA 
                                'START
                                'USA Release 

                                Dim sHeaderMsg As String = String.Empty
                                Dim oParentForm As SAPbouiCOM.Form = goParent.doGetParentForm(oForm.UniqueID)

                                'Company level Credit Check 
                                If Config.INSTANCE.getParameterAsBool("MDCRDO", False) Then
                                    'Load information from Credit Check View 
                                    Dim oResult As DataRecords
                                    Dim sQry As String = "SELECT bp.validFor, bp.validFrom, bp.validTo, " &
                                        " v.CardCode, v.CreditLine,  v.Balance, v.OrdersBal, v.DNotesBal, " &
                                        " v.TBalance, v.WRBalance, v.RemainBal, v.Deviation, " &
                                        " v.frozenFor, v.FrozenComm, v.IDHICL, v.frozenFrom, " &
                                        " v.frozenTo, bp.GroupNum " &
                                        " FROM IDH_VWR1BAL v, OCRD bp " &
                                        " WHERE v.CardCode = bp.CardCode AND v.CardCode = '" + sCustomer + "'"
                                    oResult = DataHandler.INSTANCE.doBufferedSelectQuery("doCheckCredit", sQry)

                                    Dim sIgnoreCheck As String
                                    Dim sPaymentTermCode As String
                                    Dim dTBalance As Double
                                    Dim dWROrders As Double

                                    Dim sActive As String
                                    Dim sActiveFrom As String
                                    Dim sActiveTo As String

                                    Dim sFrozen As String
                                    Dim sFrozenComm As String
                                    Dim sFrozenFrom As String
                                    Dim sFrozenTo As String

                                    Dim dCreditLimit As Double
                                    Dim dCreditRemain As Double
                                    Dim dDeviationPrc As Double

                                    If oResult.RecordCount > 0 Then
                                        sIgnoreCheck = oResult.getValueAsStringNotNull("IDHICL")
                                        sPaymentTermCode = oResult.getValueAsStringNotNull("GroupNum")
                                        dTBalance = oResult.getValueAsDouble("TBalance")
                                        dWROrders = oResult.getValueAsDouble("WRBalance")

                                        sActive = oResult.getValueAsString("validFor")
                                        sActiveFrom = oResult.getValueAsString("validFrom")
                                        sActiveTo = oResult.getValueAsString("validTo")

                                        sFrozen = oResult.getValueAsString("frozenFor")
                                        sFrozenComm = oResult.getValueAsString("FrozenComm")
                                        sFrozenFrom = oResult.getValueAsString("frozenFrom")
                                        sFrozenTo = oResult.getValueAsString("frozenTo")
                                        dCreditLimit = oResult.getValueAsDouble("CreditLine")
                                        dCreditRemain = oResult.getValueAsDouble("RemainBal")
                                        dDeviationPrc = oResult.getValueAsDouble("Deviation")

                                        'Release oResult object 
                                        oResult = Nothing


                                        'See if the WOH status is not 'Quote' 
                                        'For a Quote WOH there is NO Credit Check 
                                        'Dim oWOHRS As SAPbobsCOM.Recordset
                                        Dim sHeaderStatus As String
                                        sHeaderStatus = oGridN.doGetFieldValue("e.U_Status", iCurrentDataRow)

                                        'BP exempt from credit check 
                                        If sIgnoreCheck <> "Y" AndAlso Not sHeaderStatus.Equals(Config.ParameterWithDefault("MDQUOTCD", "8")) Then
                                            'BP is Cash Customer or Other 
                                            If Config.ParameterWithDefault("MDCPTC", "0") = sPaymentTermCode Then
                                                goParent.doMessage("[Customer Order Duplication]" + Chr(13) + "Customer is CIA")
                                                'cash customer
                                                If Config.INSTANCE.doCheckCreditCashCustomer(sCustomer, dTBalance, dWROrders) = False Then
                                                    'Set Order to DRAFT
                                                    'Update WOH with status = "DRAFT"
                                                    Dim sUpdateQry = "Update [@IDH_JOBENTR] set U_Status = '6' where Code = '" & sWOCode & "'"
                                                    DataHandler.INSTANCE.doUpdateQuery(sUpdateQry)

                                                    'setSharedData(oParentForm, "WOTODRAFT", Config.ParameterWithDefault("MDDRFTCD", "6"))
                                                    'setFormDFValueFromUser(oForm, "U_Status", Config.ParameterWithDefault("MDDRFTCD", "6"), True)
                                                    sHeaderMsg = "Over Credit Limit"
                                                    doSendCreditCheckAlertAndMessage(oForm, sCustomer, sWOCode, sRowCode, sHeaderMsg, "CCCASHAL", "CCCALERT")
                                                End If
                                                'If Config.INSTANCE.doCheckCreditCashCustomer(goParent, sCustomer, dTBalance, dWROrders) = False Then
                                                '    'Alert Users
                                                '    Dim sCCAlertUsers As String = Config.ParameterWithDefault("CCCASHAL", "")
                                                '    If sCCAlertUsers IsNot Nothing AndAlso sCCAlertUsers.Length > 0 Then
                                                '        Dim sAlertMsg As String = Config.ParameterWithDefault("CCCALERT", "CIA Credit check alert!")
                                                '        sAlertMsg = sAlertMsg + Chr(13)
                                                '        sAlertMsg = sAlertMsg + "Work Order: " + sWOCode + Chr(13)
                                                '        sAlertMsg = sAlertMsg + "From User: " + goParent.gsUserName + Chr(13)
                                                '        sAlertMsg = sAlertMsg + "Cc: " + sCCAlertUsers
                                                '        If Not sCCAlertUsers.Contains(goParent.gsUserName) Then
                                                '            sCCAlertUsers = sCCAlertUsers + "," + goParent.gsUserName
                                                '        End If
                                                '        goParent.doSendAlert(sCCAlertUsers.Split(","), "Over Credit Limit: " & sCustomer, sAlertMsg, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner, sCustomer, True)
                                                '    End If
                                                '    'Changing as on 20120831: change the status to Draft
                                                '    setFormDFValueFromUser(oForm, "U_Status", Config.ParameterWithDefault("MDDRFTCD", "6"), True)
                                                '    bCChkPass = False
                                                'Else
                                                '    bCChkPass = True
                                                'End If
                                            Else
                                                'account customer 
                                                Dim bPayTermLimitPass As Boolean = False
                                                Dim bCreditCheckPass As Boolean = False
                                                'Dim sMessage As String
                                                Dim bDoAlert As Boolean = True
                                                Dim bDoDraft As Boolean = False
                                                Dim sMessageType As String = Nothing

                                                Dim oActiveFrom As Date = com.idh.utils.Dates.doStrToDate(sActiveFrom)
                                                Dim oActiveTo As Date = com.idh.utils.Dates.doStrToDate(sActiveTo)
                                                Dim oFrozenFrom As Date = com.idh.utils.Dates.doStrToDate(sFrozenFrom)
                                                Dim oFrozenTo As Date = com.idh.utils.Dates.doStrToDate(sFrozenTo)
                                                Dim oRequestedDate As Date = com.idh.utils.Dates.doStrToDate(sReqDate)

                                                If com.idh.bridge.lookups.Config.INSTANCE.doCheckBPActive(sActive, oActiveFrom, oActiveTo, sFrozen, sFrozenComm, oFrozenFrom, oFrozenTo, oRequestedDate, True) = True Then
                                                    'if BP is not frozen 
                                                    If Config.INSTANCE.getParameterAsBool("DPTLCHK", False) Then 'DoPaymentTermLimitCheck
                                                        Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                                                        Dim sINVQry As String = ""
                                                        sINVQry = sINVQry & "SELECT DocDueDate + " + Config.ParameterWithDefault("PTGRCPRD", "0") + " AS BufferDocDueDate, "
                                                        sINVQry = sINVQry & " DocEntry, "
                                                        sINVQry = sINVQry & " CardCode "
                                                        sINVQry = sINVQry & "FROM   OINV "
                                                        sINVQry = sINVQry & "WHERE  CardCode = '" + sCustomer + "' "
                                                        sINVQry = sINVQry & "       AND DocDueDate + " + Config.ParameterWithDefault("PTGRCPRD", "0") + " <= GetDate() "
                                                        sINVQry = sINVQry & "       AND DocStatus = 'O' "

                                                        oRecordSet = goParent.goDB.doSelectQuery(sINVQry)
                                                        If oRecordSet.RecordCount > 0 Then
                                                            bPayTermLimitPass = False
                                                        Else
                                                            bPayTermLimitPass = True
                                                        End If
                                                        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                                                    Else
                                                        bPayTermLimitPass = True
                                                    End If
                                                    'If Config.INSTANCE.getParameterAsBool("DPTLCHK", False) Then 'DoPaymentTermLimitCheck
                                                    '    'bPayTermLimitPass = Config.INSTANCE.doCheckCreditAdvance( _
                                                    '    '    goParent, sCustomer, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, _
                                                    '    '    0, bDoAlert, False, Nothing, bDoDraft)
                                                    '    bPayTermLimitPass = True
                                                    'Else
                                                    '    bPayTermLimitPass = True
                                                    'End If

                                                    If bPayTermLimitPass Then
                                                        If Config.INSTANCE.getParameterAsBool("DCRLCHK", False) Then 'DoCreditLimitCheck
                                                            Config.INSTANCE.doCheckCreditAdvance(
                                                            sCustomer, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc,
                                                                0, bDoAlert, False, Nothing, bDoDraft, sMessageType)

                                                            If bDoDraft Then
                                                                'Update WOH with status = "DRAFT"
                                                                Dim sUpdateQry = "Update [@IDH_JOBENTR] set U_Status = '6' where Code = '" & sWOCode & "'"
                                                                DataHandler.INSTANCE.doUpdateQuery(sUpdateQry)

                                                                'setSharedData(oParentForm, "WOTODRAFT", Config.ParameterWithDefault("MDDRFTCD", "6"))
                                                                'setFormDFValueFromUser(oForm, "U_Status", Config.ParameterWithDefault("MDDRFTCD", "6"), True)
                                                            Else
                                                                'setSharedData(oParentForm, "WOTODRAFT", "-1")
                                                            End If

                                                            If sMessageType = "NEARING" Then
                                                                sHeaderMsg = "Customer NEARING Credit Limit"
                                                            ElseIf sMessageType = "OVER" Then
                                                                sHeaderMsg = "Customer OVER Credit Limit"
                                                            ElseIf sMessageType = "NONE" Then
                                                                sHeaderMsg = String.Empty
                                                            End If

                                                            If sHeaderMsg.Length > 0 Then
                                                                doSendCreditCheckAlertAndMessage(oForm, sCustomer, sWOCode, sRowCode, sHeaderMsg, "MDCDAL", "CCHKAMSG")
                                                            End If
                                                        End If

                                                    Else
                                                        'Update WOH with status = "DRAFT"
                                                        Dim sUpdateQry = "Update [@IDH_JOBENTR] set U_Status = '6' where Code = '" & sWOCode & "'"
                                                        DataHandler.INSTANCE.doUpdateQuery(sUpdateQry)

                                                        'setSharedData(oParentForm, "WOTODRAFT", Config.ParameterWithDefault("MDDRFTCD", "6"))
                                                        'setFormDFValueFromUser(oForm, "U_Status", Config.ParameterWithDefault("MDDRFTCD", "6"), True)
                                                        sHeaderMsg = "Over Credit Terms"
                                                        doSendCreditCheckAlertAndMessage(oForm, sCustomer, sWOCode, sRowCode, sHeaderMsg, "MDCDAL", "CCHKAMSG")
                                                    End If
                                                    'If Config.INSTANCE.getParameterAsBool("DCRLCHK", False) Then 'DoCreditLimitCheck
                                                    '    bCreditCheckPass = Config.INSTANCE.doCheckCreditAdvance( _
                                                    '        goParent, sCustomer, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, _
                                                    '        0, bDoAlert, False, Nothing, bDoDraft)
                                                    'End If

                                                    ''For both Payment Terms OR Credit Check failure  
                                                    ''If any of the checks fail create the order as draft 
                                                    'If (Not bCreditCheckPass) Or (Not bPayTermLimitPass) Then
                                                    '    'setFormDFValueFromUser(oForm, "U_Status", Config.ParameterWithDefault("MDDRFTCD", "6"), True)

                                                    '    'Do alert the super users 
                                                    '    If bDoAlert Then
                                                    '        Dim sAlertusers As String = Config.ParameterWithDefault("MDCDAL", "")
                                                    '        If sAlertusers IsNot Nothing AndAlso sAlertusers.Length > 0 Then
                                                    '            Dim sAlertMsg As String = Config.ParameterWithDefault("CCHKAMSG", "Credit check alert!")
                                                    '            sAlertMsg = sAlertMsg + Chr(13)
                                                    '            sAlertMsg = sAlertMsg + "Work Order: " + sWOCode + Chr(13)
                                                    '            sAlertMsg = sAlertMsg + "From User: " + goParent.gsUserName + Chr(13)
                                                    '            sAlertMsg = sAlertMsg + "Cc: " + sAlertusers
                                                    '            If Not sAlertusers.Contains(goParent.gsUserName) Then
                                                    '                sAlertusers = sAlertusers + "," + goParent.gsUserName
                                                    '            End If
                                                    '            goParent.doSendAlert(sAlertusers.Split(","), "Credit Limit Alert for: " & sCustomer, sAlertMsg, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner, sCustomer, True)
                                                    '        End If
                                                    '    End If
                                                    'Else
                                                    '    bCChkPass = True
                                                    'End If
                                                Else
                                                    ''As BP is frozen 
                                                    'bCChkPass = False
                                                End If
                                            End If
                                        Else
                                            'bCChkPass = True
                                        End If
                                    End If
                                    'oResult = Nothing
                                End If
                                'bCChkPass = True
                                'If sHeaderMsg.Equals(String.Empty) Then
                                '    sNewCode = idh.forms.OrderRow.doDuplicateRow2(goParent, sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJob)
                                '    If bCopyADDI Then
                                '        idh.forms.OrderRow.doDuplicateAdditionalItems(goParent, sRowCode, sNewCode, sJob, bCopyPrice, bCopyQty)
                                '    End If
                                'End If

                                'sNewCode = idh.forms.OrderRow.doDuplicateRow2(goParent, sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJob, bCopyADDI)

                                'LPV - START - 20150429 - Must use the values from the options popup
                                'sNewCode = IDH_JOBSHD.doDuplicateRow2(sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJob, bCopyADDI)
                                sNewCode = IDH_JOBSHD.doDuplicateRow3(sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJob, bCopyADDI, bCopyVehReg, bCopyDriver, sReqDate, bFromOptions)
                                'LPV - END - 20150429 - Must use the values from the options popup

                                If bCopyADDI Then
                                    'idh.forms.OrderRow.doDuplicateAdditionalItems(goParent, sRowCode, sNewCode, bCopyPrice, bCopyQty)
                                    IDH_WOADDEXP.doDuplicateAdditionalItems(sRowCode, sNewCode, bCopyPrice, bCopyQty)
                                End If

                                'END
                                'OnTime [#Ico000????] USA 
                            Else
                                'Non-USA Release 
                                If Config.INSTANCE.doCheckCredit(sCustomer, 0, True, True, sAdditionalMessage) = True Then
                                    'sNewCode = idh.forms.OrderRow.doDuplicateRow2(goParent, sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJob, bCopyADDI)
                                    'sNewCode = idh.forms.OrderRow.doDuplicateRow2(goParent, sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJob, bCopyADDI, bCopyVehReg, bCopyDriver, sReqDate)
                                    sNewCode = IDH_JOBSHD.doDuplicateRow3(sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJob, bCopyADDI, bCopyVehReg, bCopyDriver, sReqDate, bFromOptions)
                                    If bCopyADDI Then
                                        'idh.forms.OrderRow.doDuplicateAdditionalItems(goParent, sRowCode, sNewCode, bCopyPrice, bCopyQty)
                                        IDH_WOADDEXP.doDuplicateAdditionalItems(sRowCode, sNewCode, bCopyPrice, bCopyQty)
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error duplicating the row.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBD", {Nothing})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oTableSrc)
            End Try
            Return sNewCode
        End Function

        'Public Function doDuplicateAdditionalItems(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParentRowCode As String, ByVal sNewRowCode As String, ByVal sJob As String) As String
        '    Try
        '        Dim oField As SAPbobsCOM.Field
        '        Dim sFieldName As String
        '        Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '        Dim sQry As String
        '        Dim sInsertQry As String

        '        Dim sNowDate As String = oParent.doDateToStr()
        '        Dim sNowTime As String = com.idh.utils.dates.doTimeToNumStr()

        '        sQry = "select [ArchiveDate], [U_JobNr], [U_RowNr], [U_ItemCd], [U_ItemDsc], [U_CustCd], [U_CustNm], " & _
        '        " [U_SuppCd], [U_SuppNm], [U_UOM], [U_Quantity], [U_ItmCost], [U_ItmChrg], [U_ItmCostVat], [U_ItmChrgVat], [U_ItmTCost], " & _
        '        " [U_ItmTChrg], [U_CostVatGrp], [U_ChrgVatGrp], [U_SONr], [U_PONr], [U_SINVNr], [U_PINVNr], [U_FPCost], [U_FPChrg], " & _
        '        " [U_EmpId], [U_EmpNm] from [@IDH_WOADDEXP] where U_RowNr = '" + sParentRowCode + "'"

        '        oRecordSet = goParent.goDB.doSelectQuery(sQry)

        '        If oRecordSet.RecordCount > 0 Then
        '            For cntInsert As Integer = 0 To oRecordSet.RecordCount - 1

        '                Dim sNewSeqCode As Integer = goParent.goDB.doNextNumber("ADDEXPKEY")
        '                Dim oInsertRS As SAPbobsCOM.Recordset = Nothing

        '                sInsertQry = " INSERT INTO [@IDH_WOADDEXP]([Code],[Name], [ArchiveDate], [U_JobNr], [U_RowNr], [U_ItemCd], [U_ItemDsc], [U_CustCd], [U_CustNm], " & _
        '                " [U_SuppCd], [U_SuppNm], [U_UOM], [U_Quantity], [U_ItmCost], [U_ItmChrg], [U_ItmCostVat], [U_ItmChrgVat], [U_ItmTCost], " & _
        '                " [U_ItmTChrg], [U_CostVatGrp], [U_ChrgVatGrp], [U_SONr], [U_PONr], [U_SINVNr], [U_PINVNr], [U_FPCost], [U_FPChrg], " & _
        '                " [U_EmpId], [U_EmpNm])VALUES( '" & _
        '                sNewSeqCode + "', '" & _
        '                sNewSeqCode + "', '" & _
        '                sNowDate + "', '" & _
        '                oRecordSet.Fields.Item(3).Value + "', '" & _
        '                sNewRowCode + "', '" & _
        '                oRecordSet.Fields.Item(5).Value + "', '" & _
        '                oRecordSet.Fields.Item(6).Value + "', '" & _
        '                oRecordSet.Fields.Item(7).Value + "', '" & _
        '                oRecordSet.Fields.Item(8).Value + "', '" & _
        '                oRecordSet.Fields.Item(9).Value + "', '" & _
        '                oRecordSet.Fields.Item(10).Value + "', '" & _
        '                oRecordSet.Fields.Item(11).Value + "', " & _
        '                oRecordSet.Fields.Item(12).Value + ", " & _
        '                oRecordSet.Fields.Item(13).Value + ", " & _
        '                oRecordSet.Fields.Item(14).Value + ", " & _
        '                oRecordSet.Fields.Item(15).Value + ", " & _
        '                oRecordSet.Fields.Item(16).Value + ", " & _
        '                oRecordSet.Fields.Item(17).Value + ", " & _
        '                oRecordSet.Fields.Item(18).Value + ", '" & _
        '                oRecordSet.Fields.Item(19).Value + "', '" & _
        '                oRecordSet.Fields.Item(20).Value + "', '" & _
        '                oRecordSet.Fields.Item(21).Value + "', '" & _
        '                oRecordSet.Fields.Item(22).Value + "', '" & _
        '                oRecordSet.Fields.Item(23).Value + "', '" & _
        '                oRecordSet.Fields.Item(24).Value + "', '" & _
        '                oRecordSet.Fields.Item(25).Value + "', '" & _
        '                oRecordSet.Fields.Item(26).Value + "', '" & _
        '                oRecordSet.Fields.Item(27).Value + "', '" & _
        '                oRecordSet.Fields.Item(28).Value + "' ) "

        '                oInsertRS = goParent.goDB.doSelectQuery(sInsertQry)
        '                oRecordSet.MoveNext()

        '                oParent.goDB.doReleaseObject(oInsertRS)
        '            Next
        '        End If
        '    Catch ex As Exception

        '    End Try
        'End Function

        '        Public Sub doMarkAsToPay(ByVal oForm As SAPbouiCOM.Form)
        '            oForm.Freeze(True)
        '            Try
        '                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
        '
        '                'Step through the selected rows and update
        '                Dim oSelected As SAPbouiCOM.SelectedRows
        '                Dim oToRemove As New ArrayList
        '                Dim oToSelectPayment As New ArrayList
        '                Dim oToCCError As New ArrayList
        '                Dim iIndex As Integer
        '                oSelected = oGridN.getGrid().Rows.SelectedRows()
        '                If Not oSelected Is Nothing Then
        '                    Dim iRow As Integer
        '                    Dim bFound As Boolean = False
        '                    For iIndex = 0 To oSelected.Count - 1
        '                        'iRow = oSelected.Item(iIndex, SAPbouiCOM.BoOrderType.ot_RowOrder)
        '                        'iRow = oGridN.getSBOGrid().GetDataTableRowIndex(iRow)
        '                        iRow = oGridN.getDataTableRowIndex(oSelected, iIndex)
        '
        '                        Dim sStatus As String = oGridN.doGetFieldValue("r.U_Status", iRow)
        '                        Dim sPayMethod As String = oGridN.doGetFieldValue("r.U_PayMeth", iRow)
        '                        Dim sPayStatus As String = oGridN.doGetFieldValue("r.U_PayStat", iRow)
        '                        Dim sPayRcpt As String = oGridN.doGetFieldValue("r.U_PARCPT", iRow)
        '                        If sStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusInvoiced()) = True AndAlso _
        '                           (sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusPaid()) OrElse _
        '                           		sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusErrPaid()) OrElse _
        '                           		sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusUnPaid())) AndAlso _
        '                           (sPayRcpt Is Nothing OrElse sPayRcpt.Length = 0) Then
        '                            If sPayMethod.Equals("Cash") OrElse sPayMethod.Equals("Check") OrElse sPayMethod.Equals("Cheque") Then
        '                                oGridN.doForceFieldValue("r.U_PayStat", iRow, com.idh.bridge.lookups.FixedValues.getStatusPaid())
        '                            ElseIf sPayMethod.Equals(Config.INSTANCE.getCreditCardName(goParent)) Then
        '                                If sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusErrPaid()) Then
        '                                    oToCCError.Add(iRow)
        '                                Else
        '                                    oGridN.doForceFieldValue("r.U_PayStat", iRow, com.idh.bridge.lookups.FixedValues.getStatusPaid())
        '                                End If
        '                            ElseIf sPayMethod.Length = 0 Then
        '                                oToSelectPayment.Add(iRow)
        '                            End If
        '                        Else
        '                            oToRemove.Add(iRow)
        '                        End If
        '                    Next
        '
        '                    Dim sMessageId As String = ""
        '                    Dim sParams As String() = Nothing
        '
        '                    'Remove the rows from the selection
        '                    If oToRemove.Count() > 0 Then
        ''                        Dim sMessage As String = ""
        ''                        If oToRemove.Count() > 1 Then
        ''                            sMessage = "The following " & oToRemove.Count() & " rows (" & doCreateCSL(oToRemove) & ") will not be processed, because they have not been Invoice, marked as cash or have already been paid." & Chr(10)
        ''                        Else
        ''                            sMessage = "The following row (" & doCreateCSL(oToRemove) & ") will not be processed, because it has not been Invoiced, marked as cash transactions or has allready been paid." & Chr(10)
        ''                        End If
        ''                        goParent.goApplication.M_essageBox(sMessage, 1, "Ok")
        '
        '                        If oToRemove.Count() > 1 Then
        '                            sMessageId = "MANAIPRS"
        '                            sParams = New String(){oToRemove.Count().ToString(),doCreateCSL(oToRemove)}
        '                        Else
        '                            sMessageId = "MANAIPRS"
        '                            sParams = New String(){doCreateCSL(oToRemove)}
        '                        End If
        '                        goParent.doResourceMessage(sMessageId, sParams, 1, "Ok")
        '                        For iIndex = 0 To oToRemove.Count - 1
        '                            oSelected.Remove(oToRemove(iIndex))
        '                        Next
        '                    End If
        '
        '                    'Show the Credit card Error option
        '                    Dim bFromCC As Boolean = False
        '                    If oToCCError.Count() > 0 Then
        ''                        Dim sMessage As String = ""
        ''                        If oToSelectPayment.Count() > 1 Then
        ''                            sMessage = "The following CreditCard " & oToCCError.Count() & " rows (" & doCreateCSL(oToCCError) & ") had processing errors." & _
        ''                                Chr(10) & "Do you want to select new Payment Means." & Chr(10)
        ''                        Else
        ''                            sMessage = "The following CreditCard row (" & doCreateCSL(oToCCError) & ") had processing errors." & _
        ''                                Chr(10) & "Do you want to select new Payment Means." & Chr(10)
        ''                        End If
        ''                        If goParent.goApplication.M_essageBox(sMessage, 1, "Yes", "No") = 2 Then
        '                        If oToSelectPayment.Count() > 1 Then
        '                            sMessageId = "MANCCERS"
        '                            sParams = New String(){oToCCError.Count().ToString(),doCreateCSL(oToCCError)}
        '                        Else
        '                            sMessageId = "MANCCER"
        '                            sParams = New String(){doCreateCSL(oToCCError)}
        '                        End If
        '                        If goParent.doResourceMessageYNAsk(sMessageId, sParams, 1) = 2 Then
        '                            For iIndex = 0 To oToCCError.Count - 1
        '                                oGridN.doForceFieldValue("r.U_PayStat", oToCCError(iIndex), com.idh.bridge.lookups.FixedValues.getStatusPaid())
        '                            Next
        '                        Else
        '                            For iIndex = 0 To oToCCError.Count - 1
        '                                oToSelectPayment.Add(oToCCError(iIndex))
        '                            Next
        '                            bFromCC = True
        '                        End If
        '                    End If
        '
        '                    'Show the payment selection option
        '                    If oToSelectPayment.Count() > 0 Then
        ''                        Dim sMessage As String = ""
        ''                        If bFromCC = False Then
        ''                            If oToSelectPayment.Count() > 1 Then
        ''                                sMessage = "You have to select a Payment Means for the following " & oToSelectPayment.Count() & " rows (" & doCreateCSL(oToSelectPayment) & ")." & _
        ''                                    Chr(10) & "Do you want to continue." & Chr(10)
        ''                            Else
        ''                                sMessage = "You have to select a Payment Means for the following row (" & doCreateCSL(oToSelectPayment) & ")." & _
        ''                                    Chr(10) & "Do you want to continue." & Chr(10)
        ''                            End If
        ''                        End If
        ''
        ''                        If bFromCC OrElse goParent.goApplication.M_essageBox(sMessage, 1, "Yes", "No") = 1 Then
        '                        If bFromCC = False Then
        '                            If oToSelectPayment.Count() > 1 Then
        '                                sMessageId = "MANPMRS"
        '                                sParams = New String(){oToSelectPayment.Count().ToString(),doCreateCSL(oToSelectPayment)}
        '							Else
        '                            	sMessageId = "MANPMR"
        '                                sParams = New String(){doCreateCSL(oToSelectPayment)}
        '                            End If
        '                        End If
        '                        If bFromCC OrElse goParent.doResourceMessageYNAsk(sMessageId, sParams, 1) = 1 Then
        '                            'setSharedData(oForm, "PREFIX", Config.INSTANCE.getStatusPreFixDo())
        '                            setSharedData(oForm, "ACTION", "D")
        '                            setSharedData(oForm, "ROWS", oToSelectPayment)
        '                            goParent.doOpenModalForm("IDHPAYMET", oForm)
        '                        End If
        '                    End If
        '                End If
        '            Catch ex As Exception
        '                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error marking to pay.")
        '            Finally
        '                oForm.Freeze(False)
        '            End Try
        '        End Sub

        '        Public Sub doMarkAsToInvoice(ByVal oForm As SAPbouiCOM.Form)
        '            oForm.Freeze(True)
        '            Try
        '                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
        '
        '                'Step through the selected rows and update
        '                Dim oSelected As SAPbouiCOM.SelectedRows
        '
        '                Dim oToRemove As New ArrayList
        '                Dim oToPayCash As New ArrayList
        '                Dim oToPayCheque As New ArrayList
        '                Dim oToPayCC As New ArrayList
        '                Dim oToSelectPayment As New ArrayList
        '                Dim iIndex As Integer
        '                oSelected = oGridN.getGrid().Rows.SelectedRows()
        '                If Not oSelected Is Nothing Then
        '                    Dim iRow As Integer
        '                    Dim bFound As Boolean = False
        '                    For iIndex = 0 To oSelected.Count - 1
        '                        'iRow = oSelected.Item(iIndex, SAPbouiCOM.BoOrderType.ot_RowOrder)
        '                        'iRow = oGridN.getSBOGrid().GetDataTableRowIndex(iRow)
        '                        iRow = oGridN.getDataTableRowIndex(oSelected, iIndex)
        '
        '                        Dim sStatus As String = oGridN.doGetFieldValue("r.U_Status", iRow)
        '                        Dim sPayMethod As String = oGridN.doGetFieldValue("r.U_PayMeth", iRow)
        '                        Dim sPayStatus As String = oGridN.doGetFieldValue("r.U_PayStat", iRow)
        '                        Dim sPayRcpt As String = oGridN.doGetFieldValue("r.U_PARCPT", iRow)
        '                        'If sStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusInvoiced()) = False Then
        '                        If com.idh.bridge.lookups.FixedValues.isOpen(sStatus) OrElse _
        '                        		com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse _
        '                            	com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) OrElse _
        '                            	com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) Then
        '                            sStatus = com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()
        '                            oGridN.doForceFieldValue("r.U_Status", iRow, sStatus)
        '
        '                            'Check the rows to be marked as to be paid
        '                            If sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusUnPaid()) OrElse sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusErrPaid()) AndAlso _
        '                               (sPayRcpt Is Nothing OrElse sPayRcpt.Length = 0) Then
        '                                If sPayMethod.Equals("Cash") Then
        '                                    oToPayCash.Add(iRow)
        '                                ElseIf sPayMethod.Equals("Check") OrElse sPayMethod.Equals("Cheque") Then
        '                                    oToPayCheque.Add(iRow)
        '                                ElseIf sPayMethod.Equals(Config.INSTANCE.getCreditCardName(goParent)) Then
        '                                    oToPayCC.Add(iRow)
        '                                ElseIf sPayMethod.Length = 0 Then
        '                                    oToSelectPayment.Add(iRow)
        '                                End If
        '                            End If
        '                        Else
        '                            oToRemove.Add(iRow)
        '                        End If
        '                    Next
        '
        '                    Dim sParams() As String
        '                    Dim sMessageId As String = ""
        '
        '                    'Remove to rows that need to be skipped
        '                    If oToRemove.Count() > 0 Then
        ''                        Dim sMessage As String = ""
        ''                        If oToRemove.Count() > 1 Then
        ''                            sMessage = "The following " & oToRemove.Count() & " rows (" & doCreateCSL(oToRemove) & ") will not be processed, because they have already been Invoiced." & Chr(10)
        ''                        Else
        ''                            sMessage = "The following row (" & doCreateCSL(oToRemove) & ") will not be processed, because it has allready been Invoiced." & Chr(10)
        ''                        End If
        ''                        goParent.goApplication.M_essageBox(sMessage, 1, "Ok")
        '                        If oToRemove.Count() > 1 Then
        '                            sMessageId = "MANAIRS"
        '                            sParams = New String(){oToRemove.Count().ToString(),doCreateCSL(oToRemove)}
        '                        Else
        '                            sMessageId = "MANAIR"
        '                            sParams = New String(){doCreateCSL(oToRemove)}
        '                        End If
        '                        goParent.doResourceMessage(sMessageId, sParams, 1, "Ok")
        '                        For iIndex = 0 To oToRemove.Count - 1
        '                            oSelected.Remove(oToRemove(iIndex))
        '                        Next
        '                    End If
        '
        '                    'Check the Cash
        '                    If oToPayCash.Count() > 0 Then
        ''                        Dim sMessage As String = ""
        ''                        If oToPayCash.Count() > 1 Then
        ''                            sMessage = "Have you received the Cash for the following " & oToPayCash.Count() & " rows (" & doCreateCSL(oToPayCash) & ")." & Chr(10)
        ''                        Else
        ''                            sMessage = "Have you received the Cash for the following row (" & doCreateCSL(oToPayCash) & ")." & Chr(10)
        ''                        End If
        ''                        If goParent.goApplication.M_essageBox(sMessage, 1, "Yes", "No") = 1 Then
        '                        If oToPayCash.Count() > 1 Then
        '                            sMessageId = "MANRMRS"
        '                            sParams = New String(){oToPayCash.Count().ToString(),doCreateCSL(oToPayCash)}
        '						Else
        '                        	sParams = New String(){doCreateCSL(oToPayCash)}
        '                            sMessageId = "MANRMR"
        '                        End If
        '                        If goParent.doResourceMessageYN(sMessageId, sParams, 1) = 1 Then
        '                            For iIndex = 0 To oToPayCash.Count - 1
        '                                oGridN.doForceFieldValue("r.U_PayStat", oToPayCash(iIndex), com.idh.bridge.lookups.FixedValues.getStatusPaid())
        '                            Next
        '                        End If
        '                    End If
        '
        '                    'Check the Cheque
        '                    If oToPayCheque.Count() > 0 Then
        ''                        Dim sMessage As String = ""
        ''                        If oToPayCheque.Count() > 1 Then
        ''                            sMessage = "Have you received the Cheque for the following " & oToPayCheque.Count() & " rows (" & doCreateCSL(oToPayCheque) & ")." & Chr(10)
        ''                        Else
        ''                            sMessage = "Have you received the Cheque for the following row (" & doCreateCSL(oToPayCheque) & ")." & Chr(10)
        ''                        End If
        ''                        If goParent.goApplication.M_essageBox(sMessage, 1, "Yes", "No") = 1 Then
        '                        If oToPayCheque.Count() > 1 Then
        '                            sMessageId = "MANRCRS"
        '                            sParams = New String(){oToPayCheque.Count().ToString(),doCreateCSL(oToPayCheque)}
        '                        Else
        '                            sMessageId = "MANRCR"
        '                            sParams = New String(){doCreateCSL(oToPayCheque)}
        '                        End If
        '                        If goParent.doResourceMessageYN(sMessageId, sParams, 1) = 1 Then
        '                            For iIndex = 0 To oToPayCheque.Count - 1
        '                                oGridN.doForceFieldValue("r.U_PayStat", oToPayCheque(iIndex), com.idh.bridge.lookups.FixedValues.getStatusPaid())
        '                            Next
        '                        End If
        '                    End If
        '
        '                    'Check the CreditCard
        '                    If oToPayCC.Count() > 0 Then
        ''                        Dim sMessage As String = ""
        ''                        If oToPayCC.Count() > 1 Then
        ''                            sMessage = "Do you want to process the CreditCard transactions for the following " & oToPayCC.Count() & " rows (" & doCreateCSL(oToPayCC) & ")." & Chr(10)
        ''                        Else
        ''                            sMessage = "Do you want to process the CreditCard transactions for the following row (" & doCreateCSL(oToPayCC) & ")." & Chr(10)
        ''                        End If
        ''                        If goParent.goApplication.M_essageBox(sMessage, 1, "Yes", "No") = 1 Then
        '                        If oToPayCC.Count() > 1 Then
        '                            sMessageId = "MANPCRS"
        '                            sParams = New String(){oToPayCC.Count().ToString(),doCreateCSL(oToPayCC)}
        '                        Else
        '                            sMessageId = "MANPCR"
        '                            sParams = New String(){doCreateCSL(oToPayCC)}
        '                        End If
        '                        If goParent.doResourceMessageYN(sMessageId, sParams, 1) = 1 Then
        '                          For iIndex = 0 To oToPayCC.Count - 1
        '                                oGridN.doForceFieldValue("r.U_PayStat", oToPayCC(iIndex), com.idh.bridge.lookups.FixedValues.getStatusPaid())
        '                            Next
        '                        End If
        '                    End If
        '
        '                    'Show the payment selection option
        '                    If oToSelectPayment.Count() > 0 Then
        ''                        Dim sMessage As String = ""
        ''                        If oToSelectPayment.Count() > 1 Then
        ''                            sMessage = "You have to select a Payment Means for the following " & oToSelectPayment.Count() & " rows (" & doCreateCSL(oToSelectPayment) & ")." & _
        ''                             Chr(10) & "Do you want to continue." & Chr(10)
        ''                        Else
        ''                            sMessage = "You have to select a Payment Means for the following row (" & doCreateCSL(oToSelectPayment) & ")." & _
        ''                             Chr(10) & "Do you want to continue." & Chr(10)
        ''                        End If
        '                        'If goParent.goApplication.M_essageBox(sMessage, 1, "Yes", "No") = 1 Then
        '                        If oToSelectPayment.Count() > 1 Then
        '                            sMessageId = "MANPMRS"
        '                            sParams = New String(){oToSelectPayment.Count().ToString(),doCreateCSL(oToSelectPayment)}
        '                        Else
        '                            sMessageId = "MANPMR"
        '                            sParams = New String(){doCreateCSL(oToSelectPayment)}
        '                        End If
        '                        If goParent.doResourceMessageYNAsk(sMessageId, sParams, 1) = 1 Then
        '                            'setSharedData(oForm, "PREFIX", Config.INSTANCE.getStatusPreFixDo())
        '                            setSharedData(oForm, "ACTION", "D")
        '                            setSharedData(oForm, "ROWS", oToSelectPayment)
        '                            goParent.doOpenModalForm("IDHPAYMET", oForm)
        '                        End If
        '                    End If
        '                End If
        '            Catch ex As Exception
        '                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error martking to invoice.")
        '            Finally
        '                oForm.Freeze(False)
        '            End Try
        '        End Sub

        '        Public Sub doMarkAsToOrder(ByVal oForm As SAPbouiCOM.Form, ByVal bDoSO As Boolean, ByVal bDoPO As Boolean)
        '            If bDoSO = False AndAlso bDoPO = False Then
        '                Exit Sub
        '            End If
        '
        '            oForm.Freeze(True)
        '            Try
        '                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
        '
        '                'Step through the selected rows and update
        '                Dim oSelected As SAPbouiCOM.SelectedRows
        '                oSelected = oGridN.getGrid().Rows.SelectedRows()
        '                If Not oSelected Is Nothing Then
        '                    Dim iRow As Integer
        '                    Dim bFound As Boolean = False
        '                    For iIndex As Integer = 0 To oSelected.Count - 1
        '                        'iRow = oSelected.Item(iIndex, SAPbouiCOM.BoOrderType.ot_RowOrder)
        '                        'iRow = oGridN.getSBOGrid().GetDataTableRowIndex(iRow)
        '                        iRow = oGridN.getDataTableRowIndex(oSelected, iIndex)
        '
        '                        Dim sStatus As String
        '                        Dim sTipPO As String = Nothing
        '                        Dim sHaulPO As String = Nothing
        '                        If bDoSO Then
        '                            sStatus = oGridN.doGetFieldValue("r.U_Status", iRow)
        '                            'If sStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) = False Then
        '                            If com.idh.bridge.lookups.FixedValues.isOpen(sStatus) OrElse _
        '                            	com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse _
        '                            	com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) OrElse _
        '                            	com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) Then
        '                                oGridN.doForceFieldValue("r.U_Status", iRow, com.idh.bridge.lookups.FixedValues.getDoOrderStatus())
        '                                oGridN.doForceFieldValue("r.U_PayMeth", iRow, "Accounts")
        '                                oGridN.doForceFieldValue("r.U_PayStat", iRow, com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
        '                                oGridN.doForceFieldValue("r.U_CCNum", iRow, "")
        '                                oGridN.doForceFieldValue("r.U_CCType", iRow, "0")
        '                                oGridN.doForceFieldValue("r.U_CCStat", iRow, "")
        '                            End If
        '                        End If
        '
        '                        If bDoPO Then
        '                            sStatus = oGridN.doGetFieldValue("r.U_PStat", iRow)
        '
        '							If sStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) = True Then
        '								Dim idx As Integer
        '								Dim bCanDoPO As Boolean = False
        '								idx = oGridN.doIndexField("r.U_TIPPO")
        '								If idx > -1 Then
        '									sTipPO = oGridN.doGetFieldValue(idx, iRow)
        '									bCanDoPO = True
        '								End If
        '
        '								idx = oGridN.doIndexField("r.U_JOBPO")
        '								If idx > -1 Then
        '									sHaulPO = oGridN.doGetFieldValue("r.U_JOBPO", iRow)
        '									bCanDoPO = True
        '								End If
        '
        '								If bCanDoPO AndAlso _
        '									( (sTipPO Is Nothing OrElse sTipPO.Length = 0 ) OrElse _
        '								      (sHaulPO Is Nothing OrElse sHaulPO.Length = 0 ) )	Then
        '	                                oGridN.doForceFieldValue("r.U_PStat", iRow, com.idh.bridge.lookups.FixedValues.getDoOrderStatus())
        '	                            End If
        '							Else
        '		                    	'If sStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) = False Then
        '	    	                    If com.idh.bridge.lookups.FixedValues.isOpen(sStatus) OrElse _
        '	    	                    	com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse _
        '	        	                	com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) OrElse _
        '	            	            	com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) Then
        '	                                oGridN.doForceFieldValue("r.U_PStat", iRow, com.idh.bridge.lookups.FixedValues.getDoOrderStatus())
        '	                            End If
        '							End If
        '                        End If
        '
        '                        If bDoPO = False AndAlso bDoSO = False Then
        '                            oSelected.Remove(iIndex)
        '                        End If
        '                    Next
        '                End If
        '            Catch ex As Exception
        '                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error marking to order.")
        '            Finally
        '                oForm.Freeze(False)
        '            End Try
        '        End Sub

        '        Public Sub doMarkAsToRebate(ByVal oForm As SAPbouiCOM.Form, ByVal bDoARRebate As Boolean, ByVal bDoAPRebate As Boolean)
        '            If bDoARRebate = False AndAlso bDoAPRebate = False Then
        '                Exit Sub
        '            End If
        '
        '            oForm.Freeze(True)
        '            Try
        '                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
        '
        '                'Step through the selected rows and update
        '                Dim oSelected As SAPbouiCOM.SelectedRows
        '                oSelected = oGridN.getGrid().Rows.SelectedRows()
        '                If Not oSelected Is Nothing Then
        '                    Dim iRow As Integer
        '                    Dim bFound As Boolean = False
        '                    For iIndex As Integer = 0 To oSelected.Count - 1
        '                        iRow = oGridN.getDataTableRowIndex(oSelected, iIndex)
        '
        '                        Dim sStatus As String
        '                        If bDoARRebate Then
        '                            sStatus = oGridN.doGetFieldValue("r.U_Status", iRow)
        '                            If com.idh.bridge.lookups.FixedValues.isWaitingForRebate(sStatus) Then
        '                                oGridN.doForceFieldValue("r.U_Status", iRow, com.idh.bridge.lookups.FixedValues.getDoRebateStatus())
        '                                oGridN.doForceFieldValue("r.U_PayMeth", iRow, "")
        '                                oGridN.doForceFieldValue("r.U_PayStat", iRow, "")
        '                                oGridN.doForceFieldValue("r.U_CCNum", iRow, "")
        '                                oGridN.doForceFieldValue("r.U_CCType", iRow, "0")
        '                                oGridN.doForceFieldValue("r.U_CCStat", iRow, "")
        '                            End If
        '                        End If
        '
        '                        If bDoAPRebate Then
        '                            sStatus = oGridN.doGetFieldValue("r.U_PStat", iRow)
        '                            If com.idh.bridge.lookups.FixedValues.isWaitingForRebate(sStatus) Then
        '                                oGridN.doForceFieldValue("r.U_PStat", iRow, com.idh.bridge.lookups.FixedValues.getDoRebateStatus())
        '                            End If
        '                        End If
        '
        '                        If bDoARRebate = False AndAlso bDoAPRebate = False Then
        '                            oSelected.Remove(iIndex)
        '                        End If
        '                    Next
        '                End If
        '            Catch ex As Exception
        '                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error marking for rebate.")
        '            Finally
        '                oForm.Freeze(False)
        '            End Try
        '        End Sub

        Public Overridable Sub doNewOrderFromCurrent(ByVal oForm As SAPbouiCOM.Form)
            Dim oTableSrc As SAPbobsCOM.UserTable = Nothing
            Try
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = oGridN.getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then
                    Dim iCurrentDataRow As Integer
                    iCurrentDataRow = oGridN.getDataTableRowIndex(oSelected, 0)

                    Dim sCode As String
                    Dim sRowCode As String
                    Dim sItemGrp As String
                    Dim sNewJobType As String = Nothing
                    sCode = oGridN.doGetFieldValue("r.U_JobNr", iCurrentDataRow)
                    sRowCode = oGridN.doGetFieldValue("r.Code", iCurrentDataRow)
                    sItemGrp = oGridN.doGetFieldValue("r.U_ItmGrp", iCurrentDataRow)

                    Dim iJobIndex As Integer = IDH_JOBTYPE_SEQ.getFirstJob(Config.CAT_WO, sItemGrp)
                    If iJobIndex > -1 Then
                        Dim oJobSEQ As IDH_JOBTYPE_SEQ = IDH_JOBTYPE_SEQ.getInstance()
                        oJobSEQ.gotoRow(iJobIndex)
                        sNewJobType = oJobSEQ.U_JobTp
                    End If

                    'setSharedData(oForm, "ACTION", "Duplicate")
                    'setSharedData(oForm, "USESHARE", True)
                    'setSharedData(oForm, "WOCode", sCode)
                    'setSharedData(oForm, "RowCode", sRowCode)
                    'setSharedData(oForm, "NewJobType", sNewJobType)
                    'goParent.doOpenModalForm("IDH_WASTORD", oForm)

                    Try
                        Dim oOrder As com.isb.core.forms.WasteOrder = New com.isb.core.forms.WasteOrder(oForm.UniqueID, sCode)
                        oOrder.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                        oOrder.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                        oOrder.doShowModal()
                    Catch Ex As Exception
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                    End Try
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error creating the new Waste Order.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCNWO", {Nothing})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oTableSrc)
            End Try
        End Sub

        Public Overridable Sub doNewOrder(ByVal oForm As SAPbouiCOM.Form)
            'Dim oData As New ArrayList
            'setSharedData(oForm, "ACTION", "DoAdd")
            'setSharedData(oForm, "Code", "")
            'goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
            Try
                Dim oOrder As com.isb.core.forms.WasteOrder = New com.isb.core.forms.WasteOrder(oForm.UniqueID, Nothing)
                oOrder.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                oOrder.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                oOrder.doShowModal()
            Catch Ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
            End Try
        End Sub

        '*** Get the selected rows
        Protected Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")

            Dim iCurrentDataRow As Integer
            Dim oSelectedRows As SAPbouiCOM.SelectedRows
            oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
            If oSelectedRows.Count = 0 Then
                Messages.INSTANCE.doResourceMessage("MANRSE", Nothing)
                Exit Sub
            End If

            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                If Messages.INSTANCE.doResourceMessageYNAsk("MANRUC", Nothing) = 2 Then
                    Exit Sub
                End If
            End If

            iCurrentDataRow = oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder))
            If iCurrentDataRow > -1 Then
                oGridN.setCurrentDataRowIndex(iCurrentDataRow)
                Dim iFields As Integer
                Dim oField As com.idh.controls.strct.ListField
                For iFields = 0 To oGridN.getListfields().Count - 1
                    oField = oGridN.getListfields().Item(iFields)
                    setParentSharedData(oForm, oField.msFieldId, oGridN.doGetFieldValue(oField.msFieldName))
                Next
            End If
            doReturnFromModalShared(oForm, True)
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sItemID As String = pVal.ItemUID
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If sItemID = "IDH_QREP" Then
                        doQuickReport(oForm)
                    ElseIf sItemID = "IDH_CHOOSE" Then
                        doPrepareModalData(oForm)
                    ElseIf sItemID = "IDH_CERT" Then
                        doCertBtn(oForm)
                    ElseIf sItemID = "IDH_ONSITE" Then
                        doOnSite(oForm)
                    ElseIf sItemID = "IDH_LOC" Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                        Dim oData As New ArrayList
                        oData.Add("GetMap")

                        'NOTE: Commented for fix of OnTime: 16735 - Maps
                        'Dim aContainer As Hashtable = New Hashtable
                        'Dim aZips As Hashtable
                        ''Dim iCCount As Integer

                        'Dim sItemName As String
                        ''Dim sCardCode As String
                        'Dim sZips As String
                        'With oGridN.getSBOGrid.DataTable
                        '    Dim iRow As Integer
                        '    For iRow = 0 To .Rows.Count - 1
                        '        sItemName = oGridN.doGetFieldValue("r.U_ItemDsc", iRow)

                        '        If aContainer.ContainsKey(sItemName) = False Then
                        '            aZips = New Hashtable
                        '            aContainer.Add(sItemName, aZips)
                        '        Else
                        '            aZips = aContainer.Item(sItemName)
                        '        End If

                        '        sZips = oGridN.doGetFieldValue("e.U_ZpCd", iRow)
                        '        If aZips.ContainsKey(sZips) = False Then
                        '            Dim oAr(2) As Object
                        '            oAr(0) = oGridN.doGetFieldValue("e.U_CardCd", iRow)
                        '            oAr(1) = 1
                        '            aZips.Add(sZips, oAr)
                        '        Else
                        '            Dim oAr() As Object
                        '            oAr = aZips.Item(sZips)
                        '            oAr(1) = oAr(1) + 1
                        '        End If

                        '    Next
                        'End With

                        'Dim oCollection As ICollection = aContainer.Keys()
                        'Dim oEN As IEnumerator = oCollection.GetEnumerator

                        'Dim oCCZips As Hashtable
                        'Dim oCZips As ICollection
                        'Dim oEZips As IEnumerator

                        'Dim sReq As String = "PC="
                        'While oEN.MoveNext
                        '    sItemName = oEN.Current()
                        '    If sItemName.IndexOf("Skip") > -1 Then
                        '        sReq = sReq & "sk"
                        '    ElseIf sItemName.IndexOf("Rol") > -1 Then
                        '        sReq = sReq & "rl"
                        '    ElseIf sItemName.IndexOf("Whee") > -1 Then
                        '        sReq = sReq & "we"
                        '    ElseIf sItemName.IndexOf("Dust") > -1 Then
                        '        sReq = sReq & "du"
                        '    ElseIf sItemName.IndexOf("Comp") > -1 Then
                        '        sReq = sReq & "co"
                        '    Else
                        '        sReq = sReq & "wr"
                        '    End If

                        '    oCCZips = aContainer.Item(sItemName)
                        '    oCZips = oCCZips.Keys()
                        '    oEZips = oCZips.GetEnumerator
                        '    While oEZips.MoveNext
                        '        sZips = oEZips.Current()
                        '        Dim oAr() As Object
                        '        oAr = oCCZips.Item(sZips)
                        '        sReq = sReq & "[" & sZips & "|(" & oAr(1) & ") " & sItemName & " - " & oAr(0) & "]"
                        '    End While
                        '    sReq = sReq & ";"
                        'End While

                        'OnTime Ticket 16735 - Maps
                        'START
                        Dim sReq As String = "PC="
                        Dim U_Add, U_Str, U_Blk, U_Cty, U_ZpCd As String
                        '## MA Start 22-07-2014
                        If oGridN.getGrid.Rows.SelectedRows().Count = 0 Then
                            'com.idh.bridge.DataHandler.INSTANCE.doError("User: For locations, please select a job.", "For locations, please select a job.")
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("For locations, please select a job.", "ERUSLSJB", {Nothing})
                            Return True
                            '## MA End 22-07-2014
                        ElseIf oGridN.getGrid.Rows.SelectedRows().Count > 1 Then
                            'com.idh.bridge.DataHandler.INSTANCE.doError("User: For locations, please select one job at a time.", "For locations, please select one job at a time.")
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("For locations, please select one job at a time.", "ERUSSOJB", {Nothing})
                        Else
                            U_Add = oGridN.doPrepareListFromSelection("e.U_Address")(0).ToString()
                            U_Str = oGridN.doPrepareListFromSelection("e.U_Street")(0).ToString()
                            U_Blk = oGridN.doPrepareListFromSelection("e.U_Block")(0).ToString()
                            U_Cty = oGridN.doPrepareListFromSelection("e.U_City")(0).ToString()
                            U_ZpCd = oGridN.doPrepareListFromSelection("e.U_ZpCd")(0).ToString()

                            sReq = sReq + U_Add
                            sReq = sReq + ", " + U_Str
                            sReq = sReq + ", " + U_Blk
                            sReq = sReq + ", " + U_Cty
                            sReq = sReq + ", " + U_ZpCd
                        End If
                        'If sReq.Length > 0 Then
                        '    sReq += U_Add
                        '    sReq += U_Str
                        '    sReq += U_Blk
                        '    sReq += U_Cty
                        '    sReq += U_ZpCd
                        'End If
                        'END

                        If sReq.Length > 0 Then
                            'sReq = sReq.Replace(" ", "%20")
                            oData.Add(sReq)
                            'goParent.doOpenModalForm("IDHMAP", oForm, oData)
                            sReq = Config.Parameter("MAPURL").ToString() + "?" + sReq
                            setSharedData(oForm, "URL", sReq)

                            'goParent.doOpenModalForm("IDHHTML", oForm, oData)

                            doWR1Browser(goParent, oForm, sReq)

                            ''my code
                            'Dim oBrowser As SAPbouiCOM.ActiveX
                            'oBrowser = owItem.Specific
                            'oBrowser.ClassID = Config.Parameter("AXBRW") '"Shell.Explorer.2"
                            'AxBrowser = oBrowser.Object
                            'sZip = sZip.Trim().Replace(" ", "%20")

                            'Dim sURL As String = Config.Parameter("MAPURL") & "?PC=" & sZip
                            'AxBrowser.Navigate2(sURL)
                            ''end my code

                        End If
                    ElseIf sItemID = "IDH_DOC" Then
                        doDocReport(oForm)
                    ElseIf sItemID = "IDH_DOCS" Then
                        doDocumentsReport(oForm)
                    ElseIf sItemID = "IDH_GENCNU" Then
                        doGenConNumbers(oForm)
                    ElseIf sItemID = "IDH_WS" Then
                        doWorksheetBtn(oForm)
                    ElseIf sItemID = "IDH_CALC" Then
                        doGridTotals(oForm, False)
                        'ElseIf sItemID = "IDH_SUBCON" Then
                        '    Dim sCardCode As String = "" 'getFormDFValue(oForm, "U_IDHCCODE")
                        '    setSharedData(oForm, "IDH_ZPCDTX", "")
                        '    setSharedData(oForm, "IDH_ITEM", "")
                        '    setSharedData(oForm, "IDH_WSCD", "")
                        '    setSharedData(oForm, "IDH_RTNG", "")

                        '    setSharedData(oForm, "IDH_CUSCOD", sCardCode)
                        '    goParent.doOpenModalForm("IDH_SUBBY", oForm)
                        ''########################################################################
                        ''Start OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                        ''other Buttons here
                    ElseIf sItemID = "IDH_BPBI" Then 'Button PBI
                        goParent.doOpenModalForm("IDH_PBI", oForm)
                    ElseIf sItemID = "IDH_BRT" Then 'Button Route
                        goParent.doOpenModalForm("IDHRTADM2", oForm)
                    ElseIf sItemID = "IDH_BRSH" Then 'Button Route Sequence
                        Dim iIndex As Integer
                        Dim iRows As Integer
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        iRows = oGridN.getRowCount() - 1
                        If iRows > 0 Then
                            oForm.Freeze(True)
                            For iIndex = 0 To iRows - 1
                                oGridN.doSetFieldValue("r.U_IDHSEQ", iIndex, iIndex + 1)
                            Next
                            doSetUpdate(oForm)
                            oForm.Items.Item("1").Click()
                            oForm.Freeze(False)
                        End If
                        'ElseIf pVal.ItemUID = "IDH_BRTS" Then 'Button Route Sheets
                        'DO CALL THE PRINT HERE
                        'End OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                        '########################################################################
                        'temp change
                    ElseIf sItemID = "IDH_BRTS" Then
                        '//goParent.doResourceMessage("ROUTEP", Nothing)
                        doRouteSheetReport(oForm, "IDH_BRTS")
                    ElseIf sItemID = "IDH_RS2" Then
                        '//goParent.doResourceMessage("ROUTEP", Nothing)
                        doRouteSheetReport(oForm, "IDH_RS2")
                        '## MA Start 29-08-2014 Issue#421
                    ElseIf sItemID = "IDH_CFLBR" Then
                        goParent.doOpenModalForm("IDHBRANCHSRC", oForm)
                        '## MA End 29-08-2014 Issue#421
                    End If
                End If
                '            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK Then
                '                If pVal.ItemUID = "LINESGRID" Then
                '                    If pVal.BeforeAction = False Then
                '                        doGridDoubleClick(oForm, pVal, BubbleEvent)
                '                    End If
                '                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If sItemID = "LINESGRID" And pVal.ItemChanged Then
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        Dim bDoDate As Boolean = False
                        If oGridN.doCheckIsSameCol(pVal.ColUID, "r.U_ASDate") Then
                            Dim iCurrentDataRow As Integer = oGridN.getGrid().GetDataTableRowIndex(pVal.Row)
                            oGridN.setCurrentDataRowIndex(iCurrentDataRow)
                            Dim sValue As String = oGridN.doGetFieldValue("r.U_ASTime")
                            If sValue.Length() = 0 OrElse sValue.Equals("00:00") = True Then
                                sValue = Config.Parameter("OSMDAST")
                                If Not sValue Is Nothing AndAlso sValue.ToUpper().Equals("TRUE") = True Then
                                    sValue = goParent.doSBOStrToTimeStr(Nothing)
                                    oGridN.doSetFieldValue("r.U_ASTime", sValue)
                                End If
                            End If
                            bDoDate = True
                        ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "r.U_AEDate") Then
                            bDoDate = True
                        End If

                        If bDoDate Then
                            Dim dStartDate As Date = oGridN.doGetFieldValue("r.U_ASDate")
                            Dim dEndDate As Date = oGridN.doGetFieldValue("r.U_AEDate")

                            Config.INSTANCE.doCheckDatesMonths(dStartDate, dEndDate)
                        End If
                    End If
                End If
            End If
            Return False
        End Function

        'Protected Overridable Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Protected Overridable Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base)
            Dim oGridN As UpdateGrid = pVal.oGrid 'UpdateGrid.getInstance(oForm, "LINESGRID")
            'If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
            If pVal.Row >= 0 Then
                'Already set
                'oGridN.setCurrentLineByClick(pVal.Row)
                'oGridN.setCurrentLine(pVal.Row)

                Dim sRowCode As String = oGridN.doGetFieldValue("r.Code")
                Dim sJobEntr As String = oGridN.doGetFieldValue("r.U_JobNr")

                Dim oData As New ArrayList
                If sJobEntr.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("A Job Entry must be selected.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("A Job Entry must be selected.", "ERUSJOBS", {Nothing})
                Else
                    oData.Add("DoUpdate")
                    If oGridN.doCheckIsSameCol(pVal.ColUID, "e.U_ZpCd") Then
                        oData.Add("PC=" & oGridN.doGetFieldValue("e.U_ZpCd"))
                        goParent.doOpenModalForm("IDHMAP", oForm, oData)
                    ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "r.U_JobNr") Then
                        ''oData.Add(sJobEntr)
                        ''goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)

                        Try
                            Dim oOrder As com.isb.core.forms.WasteOrder = New com.isb.core.forms.WasteOrder(oForm.UniqueID, sJobEntr)
                            oOrder.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                            oOrder.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                            oOrder.doShowModal()
                        Catch Ex As Exception
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                        End Try
                    Else
                        If sRowCode.Length > 0 Then
                            Try
                                Dim oOrderRow As com.isb.core.forms.OrderRow = New com.isb.core.forms.OrderRow(oForm.UniqueID, sRowCode)
                                oOrderRow.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                                oOrderRow.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                                oOrderRow.doShowModal()
                            Catch Ex As Exception
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                            End Try

                            'oData.Add(sRowCode)
                            'oData.Add(oGridN.doGetFieldValue("e.U_BDate"))
                            'oData.Add(goParent.doTimeStrToStr(oGridN.doGetFieldValue("e.U_BTime")))
                            'oData.Add(oGridN.doGetFieldValue("e.U_ZpCd"))
                            'oData.Add(oGridN.doGetFieldValue("e.U_Address"))
                            'oData.Add("CANNOTDOCOVERAGE")
                            'oData.Add(oGridN.doGetFieldValue("e.U_SteId"))

                            ''Added to handle the BP Switching
                            'oData.Add(Nothing)
                            'oData.Add(oGridN.doGetFieldValue("e.U_PAddress"))
                            'oData.Add(oGridN.doGetFieldValue("e.U_PZpCd"))
                            'oData.Add(oGridN.doGetFieldValue("e.U_PPhone1"))
                            'oData.Add(oGridN.doGetFieldValue("e.U_FirstBP"))

                            ''Adding WO Header status for TFS 
                            'oData.Add(getWOHStatus(oForm, sJobEntr))

                            'goParent.doOpenModalForm("IDHJOBS", oForm, oData)
                        End If
                    End If
                End If
            End If
        End Sub

        Protected Overridable Sub doWorksheetBtn(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            Dim oSelection As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows()
            If oSelection.Count = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("No Jobs were selected.")
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("No Jobs were selected.", "ERUSJOBS", {Nothing})
            Else
                Dim sReportFile As String
                sReportFile = Config.Parameter("OSMWWO")

                '##MA Start 05-08-2014 CR:Forge Pt#9
                Dim oList As New ArrayList
                oList = oGridN.doPrepareListFromSelection("r.Code")
                Dim oParams As New Hashtable
                oParams.Add("RowNum", oList)
                If Config.INSTANCE.getParameterAsBool("UNEWRPTV", False) Then
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                    IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                Else
                    IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                End If
                '##MA End 04-08-2014
            End If
        End Sub

        Protected Overridable Sub doChooseRoute(ByVal oForm As SAPbouiCOM.Form, ByVal oGridN As UpdateGrid)
            goParent.doOpenModalForm("IDHROSRC", oForm)
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
            UpdateGrid.doRemoveGrid(oForm, "LINESGRID")
        End Sub

        Protected Overridable Sub doOnSite(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim sList As String = oGridN.doPrepareCSLFromSelection("e.U_CardCd")
            If sList.Length > 0 Then
                setSharedData(oForm, "IDH_CUST", sList)
            Else
                setSharedData(oForm, "IDH_CUST", "")
            End If
            goParent.doOpenModalForm("IDHONST", oForm)
        End Sub

        Protected Overridable Sub doCertBtn(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim sReportFile As String
            sReportFile = Config.Parameter("MNCRTRP")

            Dim oParams As New Hashtable
            oParams.Add("WONumber", "")

            Dim oList As New ArrayList
            oList = oGridN.doPrepareListFromSelection("r.Code")
            oParams.Add("RowNum", oList)
            If (Config.INSTANCE.useNewReportViewer()) Then
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)

            Else
                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
            End If
        End Sub

        Public Overrides Sub doClose()
        End Sub

        Protected Overridable Sub doDocReport(ByVal oForm As SAPbouiCOM.Form)
            If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
                If Messages.INSTANCE.doResourceMessageYNAsk("MANRUC", Nothing, 2) = 2 Then
                    Exit Sub
                End If
            End If

            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim oRows As ArrayList = oGridN.doPrepareListFromSelection("r.Code")
            If oRows.Count() = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("Select a waste order row.")
                DataHandler.INSTANCE.doResUserError("Select a waste order row.", "ERUSJOBR", {Nothing})
                Exit Sub
            End If

            'OnTime Ticket 25926 - DOC Report crahses application
            'if report starts with http display PDF report in web browser control
            Dim sReportFile As String
            sReportFile = Config.Parameter("WDOCDOC")

            If sReportFile.StartsWith("http://") Then
                'Start ISB Code
                Dim oParams As New Hashtable
                oParams.Add("RowNum", oRows.Item(0))

                'implementation of doCallReportDefaults
                doCallHTTPRpts(goParent, oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType, Config.INSTANCE.doGetBranch(goParent.gsUserName))
                'End ISB Code
            Else
                Dim oReport As DocCon = New DocCon(Me, oForm, "WO", gsType, True)
                Dim bNewConsignments As Boolean = oReport.doDocConReport(oRows)
                doAfterConsignmentReport(oForm, oReport)
                oReport = Nothing
            End If
        End Sub

        Protected Overridable Sub doDocumentsReport(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim oRows As ArrayList = oGridN.doPrepareListFromSelection("r.Code")
            If oRows.Count() = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("Select a waste order row.")
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Select a waste order row.", "ERUSJOBR", {Nothing})
                Exit Sub
            End If

            Dim sReportFile As String
            sReportFile = Config.Parameter("WDOCUM")


            'Dim oList As New ArrayList
            'oList = oGridN.doPrepareListFromSelection("r.Code")
            'oParams.Add("PARAM2", oRows)
            If (Config.INSTANCE.useNewReportViewer()) Then
                Dim oParams As New Hashtable
                oParams.Add("RowNum", oRows)
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
            Else
                Dim oParams As New Hashtable
                oParams.Add("sRowNums", oRows)

                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
            End If

            'goParent.doMessage("'WDOCUM' configuration key not setup. Ask your system administrator for help.", 1)
            'Exit Sub

            'If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
            '    If goParent.doResourceMessageYNAsk("MANRUC", Nothing, 2) = 2 Then
            '        Exit Sub
            '    End If
            'End If

            'Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            'Dim oRows As ArrayList = oGridN.doPrepareListFromSelection("r.Code")
            'If oRows.Count() = 0 Then
            '    com.idh.bridge.DataHandler.INSTANCE.doError("Select a waste order row.")
            '    Exit Sub
            'End If

            ''OnTime Ticket 25926 - DOC Report crahses application
            ''if report starts with http display PDF report in web browser control
            'Dim sReportFile As String
            'sReportFile = Config.Parameter("WDOCUM")

            'If sReportFile.StartsWith("http://") Then
            '    'Start ISB Code
            '    Dim oParams As New Hashtable
            '    oParams.Add("RowNum", oRows.Item(0))

            '    'implementation of doCallReportDefaults
            '    doCallHTTPRpts(goParent, oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType, Config.INSTANCE.doGetBranch(goParent.gsUserName))
            '    'End ISB Code
            'Else
            '    Dim oReport As DocCon = New DocCon(Me, oForm, "WO", gsType, True)
            '    Dim bNewConsignments As Boolean = oReport.doDocConReport(oRows)
            '    doAfterConsignmentReport(oForm, oReport)
            '    oReport = Nothing
            'End If
        End Sub

        Protected Overridable Sub doQuickReport(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim oRows As ArrayList = oGridN.doPrepareListFromSelection("r.Code")
            If oRows.Count() = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("Select a waste order row.")
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Select a waste order row.", "ERUSJOBR", {Nothing})
                Exit Sub
            End If

            Dim sReportFile As String
            sReportFile = Config.Parameter("OSMQREP")

            Dim oParams As New Hashtable
            oParams.Add("RowNum", oRows)
            oParams.Add("r.Code", oRows)

            IDHAddOns.idh.report.Base.doCallReport(goParent, oForm, sReportFile, "TRUE", "FALSE", "FALSE", 1, oParams, Nothing, 0, 0)
            ''IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
        End Sub

        Protected Overridable Sub doGenConNumbers(ByVal oForm As SAPbouiCOM.Form)
            If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
                If Messages.INSTANCE.doResourceMessageYNAsk("MANRUC", Nothing, 2) = 2 Then
                    Exit Sub
                End If
            End If

            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim oRows As ArrayList = oGridN.doPrepareListFromSelection("r.Code")
            If oRows.Count() = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("Select a disposal order row.")
                DataHandler.INSTANCE.doResUserError("Select a disposal order row.", "ERUSDOR", {Nothing})
                Exit Sub
            End If

            Dim oReport As DocCon = New DocCon(Me, oForm, "WO", gsType, True)
            Dim bNewConsignments As Boolean = oReport.doGenerateConNumbers(oRows)
            doAfterConsignmentReport(oForm, oReport)
            oReport = Nothing
        End Sub

        Public Sub doAfterConsignmentReport(ByVal oForm As SAPbouiCOM.Form, ByVal oReport As DocCon)
            'Prompt for the JJK Number if it is an USA version
            Dim bISUSARelease As Boolean = Config.INSTANCE.getParameterAsBool("USAREL", False)
            If bISUSARelease = True Then
                If oReport.hasConsignmentNotes Then
                    If Not oReport.ConNumbers Is Nothing AndAlso oReport.ConNumbers.Count > 0 Then
                        For Each sConNum As String In oReport.ConNumbers
                            Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.JJKNumber = New WR1_FR2Forms.idh.forms.fr2.JJKNumber(Nothing, oForm, Nothing)
                            oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_ReturnFromJJK)
                            oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_ReturnFromJJK)
                            'oOOForm.RowCodes = oReport.ConRowsToUpdate
                            oOOForm.TempConNum = sConNum 'oReport.LastConsignmentNumber
                            oOOForm.RowTable = "@IDH_JOBSHD"
                            oOOForm.doShowModal()
                        Next
                    End If
                End If
            Else
                If oReport.HasNewConsignmentNote Then
                    doReLoadData(oForm, False)
                End If
            End If
        End Sub

        Public Function Handler_ReturnFromJJK(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oPForm As WR1_FR2Forms.idh.forms.fr2.JJKNumber = oOForm
            doReLoadData(oOForm.SBOParentForm, False)
            Return True
        End Function


        'OnTime Ticket 25926 - DOC Report crahses application
        Public Function doCallHTTPRpts(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sReport As String, ByVal sDoShow As String, ByVal sDoPrint As String, ByVal sCopies As String, ByRef oParams As Hashtable, ByRef sFromForm As String, ByRef sBranch As String) As Boolean
            Dim sDoPrintDialog As String = Config.Parameter("MDSWPD")
            If sDoPrintDialog Is Nothing Then
                sDoPrintDialog = "TRUE"
            Else
                sDoPrintDialog = sDoPrintDialog.ToUpper()
            End If

            Dim sDoAutoPrint As String = Config.Parameter("AUTOPRNT")
            If sDoAutoPrint Is Nothing Then
                sDoAutoPrint = "TRUE"
            Else
                sDoAutoPrint = sDoAutoPrint.ToUpper()
            End If

            'Get the printer from the DB
            'Dim oPrinterSettings As idh.data.PrintSettings = Nothing
            'oPrinterSettings = Config.INSTANCE.getPrinterSetting(oParent, sReport, sFromForm, sBranch)

            Dim oPrinterSettings As com.idh.bridge.PrintSettings = Nothing
            oPrinterSettings = PrintSettings.getPrinterSetting(sReport, sFromForm, sBranch)

            Dim sPrinterName As String = Nothing
            Dim iWidth As Integer = 0
            Dim iHeight As Integer = 0
            If Not oPrinterSettings Is Nothing Then
                sPrinterName = oPrinterSettings.msPrinterName
                iWidth = oPrinterSettings.miWidth
                iHeight = oPrinterSettings.miHeight
            End If
            Return doCallHTTPRptsContainer(oParent, oForm, sReport, sDoShow, sDoAutoPrint, sDoPrintDialog, sCopies, oParams, sPrinterName, iWidth, iHeight)

        End Function

        Public Function doCallHTTPRptsContainer(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sReport As String, ByVal sDoShow As String, ByVal sDoPrint As String, ByVal sDoDialog As String, ByVal sCopies As String, ByRef oParams As Hashtable, ByRef sPrinterName As String, ByRef iWidth As Integer, ByRef iHeight As Integer) As Boolean
            'Get The print option
            Dim bDoPrint As Boolean = False
            If (Not sDoPrint Is Nothing) AndAlso sDoPrint.ToUpper().Equals("TRUE") Then
                bDoPrint = True
            End If

            'Get The show the report
            Dim bDoShow As Boolean = True
            If (Not sDoShow Is Nothing) AndAlso sDoShow.ToUpper().Equals("FALSE") Then
                bDoShow = False
                bDoPrint = True
            End If

            'Get The show printdialog option
            Dim bDoDialog As Boolean = True
            If (Not sDoDialog Is Nothing) AndAlso sDoDialog.ToUpper().Equals("FALSE") Then
                bDoDialog = False
            End If

            'Get The number of copies
            Dim iCopies As Integer = 1
            If Not sCopies Is Nothing Then
                Try
                    iCopies = Val(sCopies)
                Catch ex As Exception
                End Try
            End If

            setSharedData(oForm, "DOPRINT", sDoPrint)
            setSharedData(oForm, "DOSHOWPRNDIALOG", sDoDialog)
            setSharedData(oForm, "DOSHOW", sDoShow)
            setSharedData(oForm, "NUMCOPIES", sCopies)
            setSharedData(oForm, "PARAMS", oParams)
            setSharedData(oForm, "PRINTERNAME", sPrinterName)

            setSharedData(oForm, "WIDTH", iWidth)
            setSharedData(oForm, "HEIGHT", iHeight)

            If sReport.StartsWith("http://") Then
                Dim sURL As String = sReport

                If oParams.Count > 0 Then
                    Dim en As IEnumerator = oParams.Keys.GetEnumerator()
                    Dim sKey As String
                    Dim sValue As String

                    While en.MoveNext
                        sKey = en.Current().ToString()
                        If Not sKey Is Nothing Then
                            sValue = oParams.Item(sKey)
                            sURL = sURL & "&" & sKey & "=" & sValue
                        End If
                    End While
                End If

                If bDoShow = False Then
                    'Dim oHTML As idh.report.HTMLViewer
                    'oHTML = idh.report.HTMLViewer.getInstance(oParent)
                    'oHTML.doPrintSilent(sURL, bDoDialog, iCopies)
                Else
                    setSharedData(oForm, "URL", sURL)
                    oParent.doOpenModalForm("IDHHTML", oForm)
                End If
            Else
                'Dim sReportChk As String = idh.report.CrystalViewer.getReportChk(oParent, sReport)
                Dim sReportChk As String = getReportChk(oParent, sReport)
                If Not sReportChk Is Nothing Then
                    setSharedData(oForm, "CHKREPORT", sReportChk)
                    setSharedData(oForm, "REPORT", sReport)
                    setSharedData(oForm, "ZOOM", "69")
                    setSharedData(oForm, "DSPGROUPTREE", "FALSE")
                    setSharedData(oForm, "DSPTOOLBAR", "TRUE")
                    oParent.doOpenModalForm("IDHREPORT", oForm)
                Else
                    Return False
                End If
            End If
            Return True

        End Function

        Public Sub doWR1Browser(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal oForm As SAPbouiCOM.Form, ByRef sReq As String)
            Dim cp As SAPbouiCOM.FormCreationParams
            Dim oItem As SAPbouiCOM.Item
            'Dim oStatic As SAPbouiCOM.StaticText

            ' Create the form
            cp = oParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)

            cp.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed
            cp.FormType = "Modal"
            cp.UniqueID = "Modal"

            oForm = oParent.goApplication.Forms.AddEx(cp)
            oForm.Title = "WR1 Browser"
            oForm.ClientHeight = 740
            oForm.ClientWidth = 600

            ' Create the form GUI elements
            oForm.AutoManaged = False
            oForm.SupportedModes = 0
            oItem = oForm.Items.Add("1", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.AffectsFormMode = False
            oItem.Top = 665
            oItem.Left = 10

            'oItem = oForm.Items.Add("MyStatic", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            'oStatic = oItem.Specific()
            'oStatic.Caption = getTranslatedWord("I am a modal form")

            Dim oBrowser As SAPbouiCOM.ActiveX
            oItem = oForm.Items.Add("ISBBRW", SAPbouiCOM.BoFormItemTypes.it_ACTIVE_X)
            oItem.Top = 10
            oItem.Left = 10
            oItem.Height = 650
            oItem.Width = 570
            oBrowser = oItem.Specific
            oBrowser.ClassID = Config.Parameter("AXBRW") '"Shell.Explorer.2"
            AxBrowser = oBrowser.Object

            'Dim sURL As String = "http://servicecall.isbglobal.com/ISBMAPS/Default.aspx?PC=CRANLEIGH CRC, ELMBRIDGE ROAD, NANHURST, CRANLEIGH, GU6 8JX"
            AxBrowser.Navigate2(sReq)

            oForm.Visible = True
            bModal = True
        End Sub

        Public Function getReportChk(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sReportIn As String) As String
            Dim sReport As String = Nothing
            Dim sPath As String = Config.Parameter("REPDIR")
            If Not sPath Is Nothing AndAlso sPath.Trim().Length() > 0 Then
                If sPath.EndsWith("\") = False Then
                    sPath = sPath & "\"
                End If
            Else
                'sPath = Directory.GetCurrentDirectory & "\reports\"
                sPath = IDHAddOns.idh.addon.Base.REPORTPATH
            End If

            sReport = sReportIn.Trim()
            If sReport Is Nothing OrElse sReport.Length() = 0 Then
                Return Nothing
            End If
            sReport = sPath & sReport

            'If oParent.gsAdditionalSRFPath Is Nothing Then
            If IDHAddOns.idh.addon.Base.REPORTPATH Is Nothing Then
                If File.Exists(sReport) = False Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Config: The report file could not be found - " & sReportIn, "The report file could not be found - " & sReportIn)
                    com.idh.bridge.DataHandler.INSTANCE.doResConfigError("The report file could not be found - " & sReportIn, "ERCONREP", {sReportIn})
                    Return Nothing
                Else
                    Return sReport
                End If
            Else
                If File.Exists(sReport) = False Then
                    'sPath = oParent.gsAdditionalSRFPath & "\reports\"
                    sPath = IDHAddOns.idh.addon.Base.REPORTPATH & "\"
                    sReport = sPath & sReportIn.Trim()
                    If File.Exists(sReport) = False Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Config: The report file could not be found - " & sReportIn, "The report file could not be found - " & sReportIn)
                        com.idh.bridge.DataHandler.INSTANCE.doResConfigError("The report file could not be found - " & sReportIn, "ERCONREP", {sReportIn})
                        Return Nothing
                    Else
                        Return sReport
                    End If
                Else
                    Return sReport
                End If
            End If
        End Function

        '        Protected Overridable Sub doRouteSheetReport(ByRef oForm As SAPbouiCOM.Form)
        '            If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
        '                If goParent.doResourceMessageYNAsk("MANRUC", Nothing, 2) = 2 Then
        '                    Exit Sub
        '                End If
        '            End If
        '
        '            ''Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
        '            ''Dim oRows As ArrayList = oGridN.doPrepareListFromSelection("r.Code")
        '            ''If oRows.Count() = 0 Then
        '            ''    com.idh.bridge.DataHandler.INSTANCE.doError("Select a disposal order row.")
        '            ''    Exit Sub
        '            ''End If
        '
        '            '            Dim oReport As idh.report.DocCon = New idh.report.DocCon(goParent, oForm, "DO", gsType, True)
        '            '			Dim bNewConsignments As Boolean = oReport.doDocReport(oRows)
        '            '			oReport = Nothing
        '            '            If bNewConsignments Then
        '            '            	doReLoadData(oForm, False)
        '            '            End If
        '
        '            'Dim oReport As DocCon = New DocCon(oForm, "DO", gsType, True)
        '            'Dim bNewConsignments As Boolean = oReport.doDocReport(oRows)
        '            'oReport = Nothing
        '            'If bNewConsignments Then
        '            '   doReLoadData(oForm, False)
        '            'End If
        '
        '            'OnTime Ticket 25926 - DOC Report crahses application
        '            'if report starts with http display PDF report in web browser control
        '            Dim sReportFile As String
        '            sReportFile = Config.Parameter("ROUTESHT")
        '            Dim sRoute As String = getUFValue(oForm, "IDH_FROUTE")
        '            If sReportFile.StartsWith("http://") Then
        '                'Start ISB Code
        '                Dim oParams As New Hashtable
        '                sRoute = getUFValue(oForm, "IDH_FROUTE")
        '                oParams.Add("RowNum", sRoute)
        '
        '                'implementation of doCallReportDefaults
        '                doCallHTTPRpts(goParent, oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType, Config.INSTANCE.doGetBranch(goParent.gsUserName))
        '                'End ISB Code
        '            Else
        '                'to do
        '                'Dim oReport As DocCon = New DocCon(oForm, "DO", gsType, True)
        '                'Dim bNewConsignments As Boolean = oReport.doDocReport(sRoute)
        '                'oReport = Nothing
        '                'If bNewConsignments Then
        '                '    doReLoadData(oForm, False)
        '                'End If
        '            End If
        '        End Sub

        Protected Overridable Sub doRouteSheetReport(ByRef oForm As SAPbouiCOM.Form, ByRef oBtnSource As String)
            If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
                If Messages.INSTANCE.doResourceMessageYNAsk("MANRUC", Nothing, 2) = 2 Then
                    Exit Sub
                End If
            End If

            Dim sRouteCode = getUFValue(oForm, "IDH_FROUTE")
            If sRouteCode = String.Empty Or sRouteCode = "99999999" Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("Select a route to print route sheet.")
                DataHandler.INSTANCE.doResUserError("Select a route to print route sheet.", "ERUSSRT", {Nothing})
                Exit Sub
            End If

            Dim sRouteDate As String = getUFValue(oForm, "IDH_RTDT")
            If sRouteDate Is Nothing OrElse sRouteDate.Length < 8 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("Select a route date to print route sheet.")
                DataHandler.INSTANCE.doResUserError("Select a route date to print route sheet.", "ERUSSRTD", {Nothing})
                Exit Sub
            Else
                'Converting Date into ANSI format YYYY.MM.DD
                sRouteDate = sRouteDate.Substring(0, 4) + "." + sRouteDate.Substring(4, 2) + "." + sRouteDate.Substring(6, 2)
                If sRouteDate = String.Empty Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Select a route date to print route sheet.")
                    DataHandler.INSTANCE.doResUserError("Select a route date to print route sheet.", "ERUSSRTD", {Nothing})
                    Exit Sub
                End If
            End If

            'OnTime Ticket 25926 - DOC Report crahses application 
            'if report starts with http display PDF report in web browser control
            Dim sReportFile As String

            If (oBtnSource = "IDH_BRTS") Then
                sReportFile = Config.Parameter("ROUTESHT")
            Else
                sReportFile = Config.Parameter("ROTESHT2")
            End If

            If sReportFile.StartsWith("http://") Then
                'Start ISB Code 
                Dim oParams As New Hashtable
                sRouteCode = getUFValue(oForm, "IDH_FROUTE")
                oParams.Add("RouteCode", sRouteCode)

                'implementation of doCallReportDefaults 
                doCallHTTPRpts(goParent, oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType, Config.INSTANCE.doGetBranch(goParent.gsUserName))
                'End ISB Code 
            Else
                Dim oParams As New Hashtable
                oParams.Add("RouteCode", sRouteCode)
                oParams.Add("RouteDate", sRouteDate)
                If (Config.INSTANCE.useNewReportViewer()) Then
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                    IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                Else
                    IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                End If
            End If
        End Sub

        Public Sub doSendCreditCheckAlertAndMessage(ByVal oForm As SAPbouiCOM.Form, ByVal sCardCode As String, ByVal sWOCode As String, ByVal sRCode As String, ByVal sHeaderMsg As String, ByVal sAlertUsersKey As String, ByVal sAlertMsgKey As String)
            Dim sAlertusers As String = Config.ParameterWithDefault(sAlertUsersKey, "")
            If sAlertusers IsNot Nothing AndAlso sAlertusers.Length > 0 Then
                Dim sAlertMsg As String = Config.ParameterWithDefault(sAlertMsgKey, "Credit check alert!")
                sAlertMsg = sAlertMsg + Chr(13)
                sAlertMsg = sAlertMsg + sHeaderMsg + Chr(13)
                sAlertMsg = sAlertMsg + "Work Order: " + sWOCode + Chr(13)
                sAlertMsg = sAlertMsg + "From User: " + goParent.gsUserName + Chr(13)
                sAlertMsg = sAlertMsg + "Cc: " + sAlertusers
                If Not sAlertusers.Contains(goParent.gsUserName) Then
                    sAlertusers = sAlertusers + "," + goParent.gsUserName
                End If
                goParent.doMessage(sHeaderMsg)
                goParent.doSendAlert(sAlertusers.Split(","), sHeaderMsg & ": " & sCardCode, sAlertMsg, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner, sCardCode, True)
            End If
        End Sub

        Public Sub doAddSurcharges(ByVal oForm As SAPbouiCOM.Form, ByRef oGridN As OrderRowGrid)
            'now add additional items 
            Dim sCardCode As String = oGridN.doGetFieldValue("e.U_CardCd")
            Dim sCardName As String = oGridN.doGetFieldValue("e.U_CardNM")
            Dim sWOCode As String = oGridN.doGetFieldValue("r.U_JobNr")
            Dim sRowNr As String = oGridN.doGetFieldValue("r.Code")
            Dim oRecordset As SAPbobsCOM.Recordset

            Dim sFuelsurcharge As String = Nothing
            Dim sCountyCharge As String = Nothing
            Dim sPortCharge As String = Nothing
            Dim sFinalMsg As String = Nothing

            oRecordset = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim sQry As String = "select U_FULSCHG, U_COUNTAR, U_PORTARF from OCRD where CardCode = '" + sCardCode + "'"
            oRecordset = goParent.goDB.doSelectQuery(sQry)

            If oRecordset.RecordCount > 0 Then
                sFuelsurcharge = oRecordset.Fields.Item(0).Value
                sCountyCharge = oRecordset.Fields.Item(1).Value
                sPortCharge = oRecordset.Fields.Item(2).Value
            End If
            IDHAddOns.idh.data.Base.doReleaseObject(oRecordset)

            'Fuel Surcharge 
            If sFuelsurcharge IsNot Nothing AndAlso sFuelsurcharge.Length > 0 AndAlso sFuelsurcharge.ToUpper() = "Y" Then
                Dim oFuelRS As SAPbobsCOM.Recordset
                Dim sVehGrp As String = Config.ParameterWithDefault("FSVEHGRP", "-1")
                sQry = " select AddExp.U_ItemCd, AddExp.U_Quantity, AddExp.U_ItmCost, U_ItmChrg, U_UOM, " + _
                        " * from [@IDH_WOADDEXP] AddExp " + _
                        " INNER JOIN oitm on AddExp.U_ItemCd = OITM.ItemCode  " + _
                        " AND OITM.ItmsGrpCod IN (" + sVehGrp + ") " + _
                        " where U_JobNr = '" + sWOCode + "' AND U_RowNr = '" + sRowNr + "'"
                oFuelRS = goParent.goDB.doSelectQuery(sQry)

                Dim sQty As String
                Dim sItmCharge As String
                Dim sIC As String
                Dim sUOM As String
                Dim dItmCharge As Double

                While Not oFuelRS.EoF
                    sQty = oFuelRS.Fields.Item("U_Quantity").Value.ToString()
                    sItmCharge = oFuelRS.Fields.Item("U_ItmChrg").Value.ToString()
                    sIC = oFuelRS.Fields.Item("U_ItemCd").Value.ToString()
                    sUOM = oFuelRS.Fields.Item("U_UOM").Value.ToString()
                    dItmCharge = 0.0

                    If sItmCharge IsNot Nothing AndAlso sItmCharge.Length > 0 AndAlso Convert.ToDouble(sItmCharge) > 0 Then
                        dItmCharge = Convert.ToDouble(sItmCharge)
                    Else
                        'Get Item Charge from CIP 
                        sQry = "SELECT Code, Name, U_CustCd, U_AItmCd, U_UOM, U_AItmPr from [@IDH_CSITPR] " + _
                                " where U_LinkNr != 0 AND U_AItmCd = '" + sIC + "' AND U_UOM = '" + sUOM + "'"
                        Dim oRSTemp As SAPbobsCOM.Recordset
                        oRSTemp = goParent.goDB.doSelectQuery(sQry)
                        If oRSTemp.RecordCount > 0 Then
                            sItmCharge = oRSTemp.Fields.Item("U_AItmPr").Value.ToString()
                            If sItmCharge IsNot Nothing AndAlso sItmCharge.Length > 0 AndAlso Convert.ToDouble(sItmCharge) > 0 Then
                                dItmCharge = Convert.ToDouble(sItmCharge)
                                IDHAddOns.idh.data.Base.doReleaseObject(oRSTemp)
                            End If
                        End If
                    End If
                    If dItmCharge > 0 Then
                        doAddFuelSurcharge(oForm, oGridN, sQty, dItmCharge, sFinalMsg)
                    Else
                        'Update FinalMessage for Item Charge value = ZERO 
                        If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
                            sFinalMsg = sFinalMsg + ", Fuel Surcharge(Item charge value not found!)"
                        Else
                            sFinalMsg = sFinalMsg + "Fuel Surcharge(Item charge value not found!)"
                        End If
                    End If
                    oFuelRS.MoveNext()
                End While
                IDHAddOns.idh.data.Base.doReleaseObject(oFuelRS)
            End If

            'County surcharge
            If sCountyCharge IsNot Nothing AndAlso sCountyCharge.Length > 0 AndAlso sCountyCharge.ToUpper() = "Y" Then
                '*** doAddCountyCharge(oForm, sCardCode, sCardName, sFinalMsg)
            End If

            'Port surcharge 
            If sPortCharge IsNot Nothing AndAlso sPortCharge.Length > 0 AndAlso sPortCharge.ToUpper() = "Y" Then
                'Now check if there is an existing Port Chanrge Row in WO 
                Dim oPortRS As SAPbobsCOM.Recordset
                sQry = "select * from [@IDH_WOADDEXP] where U_ItemCd = '" + Config.ParameterWithDefault("PORTCITM", "") + "' AND U_JobNr = '" + sWOCode + "'"
                oPortRS = goParent.goDB.doSelectQuery(sQry)
                'if there is no Add. Exp. for 'Port Charge' item code add row 
                If Not oPortRS.RecordCount > 0 Then
                    '*** doAddPortCharge(oForm, sFinalMsg)
                End If
                IDHAddOns.idh.data.Base.doReleaseObject(oPortRS)
            End If

            If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
                'oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                doWarnMess("Following Additional Items are added: " + sFinalMsg + ". Please review Additional Tab and update Ordered Quantity.", SAPbouiCOM.BoMessageTime.bmt_Short)
            End If

        End Sub

        Public Sub doRemoveSurcharges(ByRef oGridN As OrderRowGrid)
            'Remove following items 
            Config.Parameter("FUELSITM")
            Config.Parameter("CNTYCITM")
            Config.Parameter("PORTCITM")
        End Sub

        Private Sub doAddFuelSurcharge(ByVal oForm As SAPbouiCOM.Form, ByRef oGridN As OrderRowGrid, ByVal sQty As String, ByVal dItmCharge As Double, ByRef sFinalMsg As String)
            Dim sActualEndDt As String = com.idh.utils.Dates.doStrToDate(getFormDFValue(oForm, "U_AEDate").ToString())
            Dim oRS As SAPbobsCOM.Recordset
            oRS = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim sFSQry As String = " SELECT U_VendorCd, U_VendorNm, U_Rate, (U_CostTrf * U_Rate / 100) as U_CostTrf, (U_ChargeTrf * U_Rate / 100) as U_ChargeTrf FROM [@ISB_FUELSCHG] " & _
                                    " WHERE '" + sActualEndDt + "' BETWEEN convert(nvarchar, U_StartDt, 103) AND convert(nvarchar, U_EndDt, 103)"
            oRS.DoQuery(sFSQry)
            If oRS.RecordCount > 0 Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")

                Dim sItemCd As String = Config.Parameter("FUELSITM")
                If Not sItemCd Is Nothing AndAlso sItemCd.Length > 0 Then
                    Dim sJobNr As String = oGridN.doGetFieldValue("r.U_JobNr")
                    Dim sRowNr As String = oGridN.doGetFieldValue("r.Code")
                    Dim sItemNm As String = sItemCd

                    Dim sCustCd As String = oGridN.doGetFieldValue("e.U_CardCd")
                    Dim sCustNm As String = oGridN.doGetFieldValue("e.U_CardNM")

                    Dim sSuppCd As String = oRS.Fields.Item(0).Value.ToString()
                    Dim sSuppNm As String = oRS.Fields.Item(1).Value.ToString()
                    Dim sEmpId As String = ""
                    Dim sEmpNm As String = ""
                    Dim sRate As String = oRS.Fields.Item("U_Rate").Value.ToString()

                    Dim nItmCost As Double = 0 'oRS.Fields.Item(3).Value
                    Dim nItmChrg As Double = Convert.ToDouble(sQty) * dItmCharge * (Convert.ToDouble(sRate) / 100) 'oRS.Fields.Item(4).Value
                    Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, sItemCd)
                    Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, sItemCd)

                    Dim dChrgVat As Double = Config.INSTANCE.doGetVatRate(sChrgVatGrp, Date.Now)
                    Dim dCostVat As Double = Config.INSTANCE.doGetVatRate(sCostVatGrp, Date.Now)

                    Dim nItmCostVat As Double = nItmCost * (dCostVat / 100)
                    Dim nItmChrgVat As Double = nItmChrg * (dChrgVat / 100)

                    Dim nItmTCost As Double = nItmCost + nItmCostVat
                    Dim nItmTChrg As Double = nItmChrg + nItmChrgVat

                    'oGrid.doAddExpense(sJobNr, sRowNr, _
                    '    sCustCd, sCustNm, _
                    '    sSuppCd, sSuppNm, _
                    '    sEmpId, sEmpNm, _
                    '    sItemCd, sItemNm, _
                    '    "ea", 1, _
                    '    nItmCost, nItmChrg, _
                    '    sCostVatGrp, _
                    '    sChrgVatGrp, _
                    '    True, _
                    '    True)

                    'START FROM HERE
                    'COPY FOLLOWING BLOCK IN COUNTY AND PORT CHARGES 
                    'ALSO VERIFY CALCULATIONS 
                    'AND TESTING 
                    'Dim iNewSeqCode As Integer = DataHandler.doGenerateCode(IDH_WOADDEXP.AUTONUMPREFIX, "ADDEXPKEY")
                    Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_WOADDEXP.AUTONUMPREFIX, "ADDEXPKEY")
                    Dim oInsertRS As SAPbobsCOM.Recordset = Nothing
                    Dim sInsertQry As String = String.Empty

                    sInsertQry = sInsertQry + " INSERT INTO [@IDH_WOADDEXP]([Code],[Name], [U_JobNr], [U_RowNr], "
                    sInsertQry = sInsertQry + " [U_ItemCd], [U_ItemDsc], [U_CustCd], [U_CustNm], "
                    sInsertQry = sInsertQry + " [U_SuppCd], [U_SuppNm], [U_UOM], [U_Quantity], "
                    sInsertQry = sInsertQry + " [U_ItmCost], [U_ItmChrg], [U_ItmCostVat], [U_ItmChrgVat], "
                    sInsertQry = sInsertQry + " [U_ItmTCost], [U_ItmTChrg], [U_CostVatGrp], [U_ChrgVatGrp], "
                    sInsertQry = sInsertQry + " [U_FPCost], [U_FPChrg], [U_EmpId], [U_EmpNm])VALUES( "

                    sInsertQry = sInsertQry + "'" + oNumbers.CodeCode + "', "
                    sInsertQry = sInsertQry + "'" + oNumbers.NameCode + "', "
                    sInsertQry = sInsertQry + "'" + sJobNr + "', "
                    sInsertQry = sInsertQry + "'" + sRowNr + "', "

                    sInsertQry = sInsertQry + "'" + sItemCd + "', "
                    sInsertQry = sInsertQry + "'" + sItemNm + "', "
                    sInsertQry = sInsertQry + "'" + sCustCd + "', "
                    sInsertQry = sInsertQry + "'" + sCustNm + "', "

                    sInsertQry = sInsertQry + "'" + sSuppCd + "', "
                    sInsertQry = sInsertQry + "'" + sSuppNm + "', "
                    sInsertQry = sInsertQry + "'" + "ea" + "', "
                    sInsertQry = sInsertQry + "'" + "1" + "', "

                    sInsertQry = sInsertQry + "" + nItmCost + ", "
                    sInsertQry = sInsertQry + "" + nItmChrg + ", "
                    sInsertQry = sInsertQry + "" + nItmCostVat + ", "
                    sInsertQry = sInsertQry + "" + nItmChrgVat + ", "

                    sInsertQry = sInsertQry + "'" + nItmTCost + "', "
                    sInsertQry = sInsertQry + "'" + nItmTChrg + "', "
                    sInsertQry = sInsertQry + "'" + sCostVatGrp + "', "
                    sInsertQry = sInsertQry + "'" + sChrgVatGrp + "', "

                    sInsertQry = sInsertQry + "'" + "Y" + "', "
                    sInsertQry = sInsertQry + "'" + "Y" + "', "
                    sInsertQry = sInsertQry + "'" + sEmpId + "', "
                    sInsertQry = sInsertQry + "'" + sEmpNm + "' ) "

                    goParent.goDB.doUpdateQuery(sInsertQry)
                    IDHAddOns.idh.data.Base.doReleaseObject(oInsertRS)

                    If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
                        sFinalMsg = sFinalMsg + ", Fuel Surcharge"
                    Else
                        sFinalMsg = sFinalMsg + "Fuel Surcharge"
                    End If
                End If
            End If
            IDHAddOns.idh.data.Base.doReleaseObject(oRS)
            oRS = Nothing
        End Sub

        Private Sub doAddCountyCharge(ByVal oForm As SAPbouiCOM.Form, ByRef oGridN As OrderRowGrid, ByRef sCustCd As String, ByRef sCustNm As String, ByRef sFinalMsg As String)
            Dim sJobNr As String = oGridN.doGetFieldValue("r.U_JobNr")
            Dim sRowNr As String = oGridN.doGetFieldValue("r.Code")
            Dim sWasteCd As String = oGridN.doGetFieldValue("r.U_WasCd")

            'Dim sCustCd As String = getFormDFValue(oForm, "U_CustCd")
            Dim sCustAdr As String = oGridN.doGetFieldValue("e.U_Address")
            Dim sDspSiteCd As String = oGridN.doGetFieldValue("r.U_Tip")
            Dim sDspSiteAdr As String = oGridN.doGetFieldValue("r.U_Tip") 'getFormDFValue(oForm, "U_SAddress")
            Dim dManQty As Double = oGridN.doGetFieldValue("r.U_Tip") 'Convert.ToDouble(getFormDFValue(oForm, "U_CBIManQty"))
            Dim sManUOM As String = oGridN.doGetFieldValue("r.U_Tip") 'getFormDFValue(oForm, "U_CBIManUOM")
            Dim sWstProfileCd As String = oGridN.doGetFieldValue("r.U_Tip") 'getFormDFValue(oForm, "U_WasCd")

            Dim sItemCd As String = Config.Parameter("CNTYCITM")
            Dim sItemNm As String = sItemCd
            Dim sQry As String
            Dim oRSCountyCharge As SAPbobsCOM.Recordset

            sQry = ""
            sQry = sQry & "SELECT Adr.cardcode, "
            sQry = sQry & "       Adr.address, "
            sQry = sQry & "       Adr.u_usacounty AS County, "
            sQry = sQry & "       CTR.u_cntycd, "
            sQry = sQry & "       CTR.u_wstgpcd, "
            sQry = sQry & "       CTR.u_vendorcd, "
            sQry = sQry & "       CTR.u_vendornm, "
            sQry = sQry & "       CTR.u_costtrf, "
            sQry = sQry & "       CTR.u_chargetrf, "
            sQry = sQry & "       CTR.u_uom "
            sQry = sQry & "FROM   dbo.oitm "
            sQry = sQry & "       INNER JOIN dbo.[@idh_wgpcnty] "
            sQry = sQry & "               ON dbo.oitm.itemcode = dbo.[@idh_wgpcnty].u_itemcd "
            sQry = sQry & "       INNER JOIN dbo.[@isb_cntytariff] AS CTR "
            sQry = sQry & "                  INNER JOIN dbo.crd1 AS Adr "
            sQry = sQry & "                          ON CTR.u_cntycd = Adr.u_usacounty "
            sQry = sQry & "                             AND Adr.cardcode = '" + sCustCd + "' "
            sQry = sQry & "                             AND Adr.address = '" + sCustAdr + "' "
            sQry = sQry & "               ON dbo.[@idh_wgpcnty].u_wstgpcd = CTR.u_wstgpcd "
            sQry = sQry & "WHERE  ( dbo.oitm.itemcode = '" + sWstProfileCd + "' ) "
            sQry = sQry & "UNION "
            sQry = sQry & "SELECT Adr.cardcode, "
            sQry = sQry & "       Adr.address, "
            sQry = sQry & "       Adr.u_usacounty AS County, "
            sQry = sQry & "       CTR.u_cntycd, "
            sQry = sQry & "       CTR.u_wstgpcd, "
            sQry = sQry & "       CTR.u_vendorcd, "
            sQry = sQry & "       CTR.u_vendornm, "
            sQry = sQry & "       CTR.u_costtrf, "
            sQry = sQry & "       CTR.u_chargetrf, "
            sQry = sQry & "       CTR.u_uom "
            sQry = sQry & "FROM   dbo.oitm "
            sQry = sQry & "       INNER JOIN dbo.[@idh_wgpcnty] "
            sQry = sQry & "               ON dbo.oitm.itemcode = dbo.[@idh_wgpcnty].u_itemcd "
            sQry = sQry & "       INNER JOIN dbo.[@isb_cntytariff] AS CTR "
            sQry = sQry & "                  INNER JOIN dbo.crd1 AS Adr "
            sQry = sQry & "                          ON CTR.u_cntycd = Adr.u_usacounty "
            sQry = sQry & "                             AND Adr.cardcode = '" + sDspSiteCd + "' "
            sQry = sQry & "                             AND Adr.address = '" + sDspSiteAdr + "' "
            sQry = sQry & "               ON dbo.[@idh_wgpcnty].u_wstgpcd = CTR.u_wstgpcd "
            sQry = sQry & "WHERE  ( dbo.oitm.itemcode = '" + sWstProfileCd + "' ) "

            oRSCountyCharge = goParent.goDB.doSelectQuery(sQry)

            If oRSCountyCharge.RecordCount > 0 Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                If sItemCd IsNot Nothing AndAlso sItemCd.Length > 0 Then
                    For index As Integer = 0 To oRSCountyCharge.RecordCount - 1
                        Dim sSuppCd As String = oRSCountyCharge.Fields.Item("U_VendorCd").Value.ToString()
                        Dim sSuppNm As String = oRSCountyCharge.Fields.Item("U_VendorNm").Value.ToString()
                        Dim sEmpId As String = ""
                        Dim sEmpNm As String = ""

                        Dim sTariffUOM As String = oRSCountyCharge.Fields.Item("U_UOM").Value
                        Dim sTariffCounty As String = oRSCountyCharge.Fields.Item("County").Value
                        Dim nItmCost As Double = oRSCountyCharge.Fields.Item("U_CostTrf").Value
                        Dim nItmChrg As Double = oRSCountyCharge.Fields.Item("U_ChargeTrf").Value

                        Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, sItemCd)
                        Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, sItemCd)
                        Dim bDOAddExp As Boolean = True

                        If Not sTariffUOM.Equals(sManUOM) Then
                            'get conversion factor from Manifest UOM -->> Tariff UOM 
                            Dim oFactorRS As SAPbobsCOM.Recordset
                            Dim sFacQry As String
                            sFacQry = "select U_Factor from [@IDH_CNTYTRFFACTOR] where U_UOMFrom = '" + sManUOM + "' AND U_UOMTo = '" + sTariffUOM + "' AND U_County = '" + sTariffCounty + "'"
                            oFactorRS = goParent.goDB.doSelectQuery(sFacQry)

                            If oFactorRS.RecordCount > 0 Then
                                Dim nFactor As Double = oFactorRS.Fields.Item("U_Factor").Value
                                'nItmChrg = nItmChrg * nFactor * dManQty
                                nItmChrg = nItmChrg * nFactor
                            Else
                                'Message: conversion factor not found 
                                If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
                                    sFinalMsg = sFinalMsg + ", County Tariff(Conversion factor not found for ManifestUOM-" + sManUOM + " to TariffUOM-" + sTariffUOM + ")"
                                Else
                                    sFinalMsg = sFinalMsg + "County Tariff(Conversion factor not found for ManifestUOM-" + sManUOM + " to TariffUOM-" + sTariffUOM + ")"
                                End If
                                bDOAddExp = False
                            End If
                            IDHAddOns.idh.data.Base.doReleaseObject(oFactorRS)
                        Else
                            'nItmChrg = nItmChrg * dManQty
                        End If

                        If bDOAddExp Then
                            'REPLACE THIS BLOCK WITH THE QUERY ABOVE 
                            'oGrid.doAddExpense(sJobNr, sRowNr, _
                            '    sCustCd, sCustNm, _
                            '    sSuppCd, sSuppNm, _
                            '    sEmpId, sEmpNm, _
                            '    sItemCd, sItemNm, _
                            '        sTariffUOM, dManQty, _
                            '    nItmCost, nItmChrg, _
                            '    sCostVatGrp, _
                            '    sChrgVatGrp, _
                            '    True, _
                            '    True)
                            Dim dChrgVat As Double = Config.INSTANCE.doGetVatRate(sChrgVatGrp, Date.Now)
                            Dim dCostVat As Double = Config.INSTANCE.doGetVatRate(sCostVatGrp, Date.Now)

                            Dim nItmCostVat As Double = nItmCost * (dCostVat / 100)
                            Dim nItmChrgVat As Double = nItmChrg * (dChrgVat / 100)

                            Dim nItmTCost As Double = nItmCost + nItmCostVat
                            Dim nItmTChrg As Double = nItmChrg + nItmChrgVat

                            'Dim iNewSeqCode As Integer = DataHandler.doGenerateCode(IDH_WOADDEXP.AUTONUMPREFIX, "ADDEXPKEY")
                            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_WOADDEXP.AUTONUMPREFIX, "ADDEXPKEY")
                            Dim oInsertRS As SAPbobsCOM.Recordset = Nothing
                            Dim sInsertQry As String = String.Empty

                            sInsertQry = sInsertQry + " INSERT INTO [@IDH_WOADDEXP]([Code],[Name], [U_JobNr], [U_RowNr], "
                            sInsertQry = sInsertQry + " [U_ItemCd], [U_ItemDsc], [U_CustCd], [U_CustNm], "
                            sInsertQry = sInsertQry + " [U_SuppCd], [U_SuppNm], [U_UOM], [U_Quantity], "
                            sInsertQry = sInsertQry + " [U_ItmCost], [U_ItmChrg], [U_ItmCostVat], [U_ItmChrgVat], "
                            sInsertQry = sInsertQry + " [U_ItmTCost], [U_ItmTChrg], [U_CostVatGrp], [U_ChrgVatGrp], "
                            sInsertQry = sInsertQry + " [U_FPCost], [U_FPChrg], [U_EmpId], [U_EmpNm])VALUES( "

                            sInsertQry = sInsertQry + "'" + oNumbers.CodeCode + "', "
                            sInsertQry = sInsertQry + "'" + oNumbers.NameCode + "', "
                            sInsertQry = sInsertQry + "'" + sJobNr + "', "
                            sInsertQry = sInsertQry + "'" + sRowNr + "', "

                            sInsertQry = sInsertQry + "'" + sItemCd + "', "
                            sInsertQry = sInsertQry + "'" + sItemNm + "', "
                            sInsertQry = sInsertQry + "'" + sCustCd + "', "
                            sInsertQry = sInsertQry + "'" + sCustNm + "', "

                            sInsertQry = sInsertQry + "'" + sSuppCd + "', "
                            sInsertQry = sInsertQry + "'" + sSuppNm + "', "
                            sInsertQry = sInsertQry + "'" + "ea" + "', "
                            sInsertQry = sInsertQry + "'" + "1" + "', "

                            sInsertQry = sInsertQry + "" + nItmCost + ", "
                            sInsertQry = sInsertQry + "" + nItmChrg + ", "
                            sInsertQry = sInsertQry + "" + nItmCostVat + ", "
                            sInsertQry = sInsertQry + "" + nItmChrgVat + ", "

                            sInsertQry = sInsertQry + "'" + nItmTCost + "', "
                            sInsertQry = sInsertQry + "'" + nItmTChrg + "', "
                            sInsertQry = sInsertQry + "'" + sCostVatGrp + "', "
                            sInsertQry = sInsertQry + "'" + sChrgVatGrp + "', "

                            sInsertQry = sInsertQry + "'" + "Y" + "', "
                            sInsertQry = sInsertQry + "'" + "Y" + "', "
                            sInsertQry = sInsertQry + "'" + sEmpId + "', "
                            sInsertQry = sInsertQry + "'" + sEmpNm + "' ) "

                            goParent.goDB.doUpdateQuery(sInsertQry)
                            IDHAddOns.idh.data.Base.doReleaseObject(oInsertRS)
                        End If

                        oRSCountyCharge.MoveNext()
                    Next
                Else
                    'show warrning message for no County Item in WR Config 
                    If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
                        sFinalMsg = sFinalMsg + ", County Tariff(Item not found in WRConfig: CNTYCITM)"
                    Else
                        sFinalMsg = sFinalMsg + "County Tariff(Item not found in WRConfig: CNTYCITM)"
                    End If
                End If

            End If

            IDHAddOns.idh.data.Base.doReleaseObject(oRSCountyCharge)

        End Sub

        Private Sub doAddPortCharge(ByVal oForm As SAPbouiCOM.Form, ByRef oGridN As OrderRowGrid, ByRef sFinalMsg As String)
            Dim sCustAddress As String = oGridN.doGetFieldValue("r.U_Tip") 'getUFValue(oForm, "STADDR")
            Dim dManQty As Double = oGridN.doGetFieldValue("r.U_Tip") 'Convert.ToDouble(getFormDFValue(oForm, "U_CBIManQty"))

            Dim oRS As SAPbobsCOM.Recordset
            oRS = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim sPCQry As String = " select U_SiteAdd, U_VendorCd, U_VendorNm, U_UOM, U_CostTrf, U_ChargeTrf FROM [@ISB_PORTMASTR] " & _
                                    " WHERE U_SiteAdd = '" + sCustAddress + "' "
            oRS.DoQuery(sPCQry)
            If oRS.RecordCount > 0 Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")

                Dim sItemCd As String = Config.Parameter("PORTCITM")
                If Not sItemCd Is Nothing AndAlso sItemCd.Length > 0 Then
                    Dim sJobNr As String = oGridN.doGetFieldValue("r.U_JobNr")
                    Dim sRowNr As String = oGridN.doGetFieldValue("r.Code")
                    Dim sItemNm As String = sItemCd

                    Dim sCustCd As String = oGridN.doGetFieldValue("e.U_CardCd")
                    Dim sCustNm As String = oGridN.doGetFieldValue("e.U_CardNM")

                    Dim sSuppCd As String = oRS.Fields.Item(1).Value.ToString()
                    Dim sSuppNm As String = oRS.Fields.Item(2).Value.ToString()
                    Dim sUOM As String = oRS.Fields.Item(3).Value.ToString()
                    Dim sEmpId As String = ""
                    Dim sEmpNm As String = ""

                    Dim nItmCost As Double = oRS.Fields.Item(4).Value
                    Dim nItmChrg As Double = oRS.Fields.Item(5).Value
                    Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, sItemCd)
                    Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, sItemCd)

                    'REPLACE THIS BLOCK WITH INSERT QUERY 
                    'oGrid.doAddExpense(sJobNr, sRowNr, _
                    '    sCustCd, sCustNm, _
                    '    sSuppCd, sSuppNm, _
                    '    sEmpId, sEmpNm, _
                    '    sItemCd, sItemNm, _
                    '    sUOM, dManQty, _
                    '    nItmCost, nItmChrg, _
                    '    sCostVatGrp, _
                    '    sChrgVatGrp, _
                    '    True, _
                    '    True)
                    Dim dChrgVat As Double = Config.INSTANCE.doGetVatRate(sChrgVatGrp, Date.Now)
                    Dim dCostVat As Double = Config.INSTANCE.doGetVatRate(sCostVatGrp, Date.Now)

                    Dim nItmCostVat As Double = nItmCost * (dCostVat / 100)
                    Dim nItmChrgVat As Double = nItmChrg * (dChrgVat / 100)

                    Dim nItmTCost As Double = nItmCost + nItmCostVat
                    Dim nItmTChrg As Double = nItmChrg + nItmChrgVat

                    'Dim iNewSeqCode As Integer = DataHandler.doGenerateCode(IDH_WOADDEXP.AUTONUMPREFIX, "ADDEXPKEY")
                    Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_WOADDEXP.AUTONUMPREFIX, "ADDEXPKEY")
                    Dim oInsertRS As SAPbobsCOM.Recordset = Nothing
                    Dim sInsertQry As String = String.Empty

                    sInsertQry = sInsertQry + " INSERT INTO [@IDH_WOADDEXP]([Code],[Name], [U_JobNr], [U_RowNr], "
                    sInsertQry = sInsertQry + " [U_ItemCd], [U_ItemDsc], [U_CustCd], [U_CustNm], "
                    sInsertQry = sInsertQry + " [U_SuppCd], [U_SuppNm], [U_UOM], [U_Quantity], "
                    sInsertQry = sInsertQry + " [U_ItmCost], [U_ItmChrg], [U_ItmCostVat], [U_ItmChrgVat], "
                    sInsertQry = sInsertQry + " [U_ItmTCost], [U_ItmTChrg], [U_CostVatGrp], [U_ChrgVatGrp], "
                    sInsertQry = sInsertQry + " [U_FPCost], [U_FPChrg], [U_EmpId], [U_EmpNm])VALUES( "

                    sInsertQry = sInsertQry + "'" + oNumbers.CodeCode + "', "
                    sInsertQry = sInsertQry + "'" + oNumbers.NameCode + "', "
                    sInsertQry = sInsertQry + "'" + sJobNr + "', "
                    sInsertQry = sInsertQry + "'" + sRowNr + "', "

                    sInsertQry = sInsertQry + "'" + sItemCd + "', "
                    sInsertQry = sInsertQry + "'" + sItemNm + "', "
                    sInsertQry = sInsertQry + "'" + sCustCd + "', "
                    sInsertQry = sInsertQry + "'" + sCustNm + "', "

                    sInsertQry = sInsertQry + "'" + sSuppCd + "', "
                    sInsertQry = sInsertQry + "'" + sSuppNm + "', "
                    sInsertQry = sInsertQry + "'" + "ea" + "', "
                    sInsertQry = sInsertQry + "'" + "1" + "', "

                    sInsertQry = sInsertQry + "" + nItmCost + ", "
                    sInsertQry = sInsertQry + "" + nItmChrg + ", "
                    sInsertQry = sInsertQry + "" + nItmCostVat + ", "
                    sInsertQry = sInsertQry + "" + nItmChrgVat + ", "

                    sInsertQry = sInsertQry + "'" + nItmTCost + "', "
                    sInsertQry = sInsertQry + "'" + nItmTChrg + "', "
                    sInsertQry = sInsertQry + "'" + sCostVatGrp + "', "
                    sInsertQry = sInsertQry + "'" + sChrgVatGrp + "', "

                    sInsertQry = sInsertQry + "'" + "Y" + "', "
                    sInsertQry = sInsertQry + "'" + "Y" + "', "
                    sInsertQry = sInsertQry + "'" + sEmpId + "', "
                    sInsertQry = sInsertQry + "'" + sEmpNm + "' ) "

                    goParent.goDB.doUpdateQuery(sInsertQry)
                    IDHAddOns.idh.data.Base.doReleaseObject(oInsertRS)

                    If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
                        sFinalMsg = sFinalMsg + ", Port Charge"
                    Else
                        sFinalMsg = sFinalMsg + "Port Charge"
                    End If
                End If
            End If
            IDHAddOns.idh.data.Base.doReleaseObject(oRS)
            oRS = Nothing
        End Sub

        Private Sub doSetUserAndBranchFields(ByVal oForm As SAPbouiCOM.Form)
            Dim sDoFilter As String = getParentSharedData(oForm, "FILTERUB")
            Dim bDoFilter As Boolean = True
            If Not sDoFilter Is Nothing AndAlso sDoFilter = "FALSE" Then
                bDoFilter = False
            End If

            Dim oUserName As String = goParent.gsUserName.Trim()
            Dim oUserListBranchName As String = Config.ParameterWithDefault("USRLBRN", "")
            Dim oUserListUserName As String = Config.ParameterWithDefault("USRLUSRN", "")

            Dim oUserEnableList As String = Config.ParameterWithDefault("USRFENBL", "ALL")
            Dim oBranchEnableList As String = Config.ParameterWithDefault("BRNFENBL", "ALL")

            Dim oUsers() As String = oUserListBranchName.Split(",")
            Dim sResult As String = Array.Find(oUsers, Function(m) m = "-" + goParent.gsUserName)


            If Not oBranchEnableList.Equals("ALL") Then
                If Not oBranchEnableList.Contains(oUserName) Then
                    setEnableItem(oForm, False, "IDH_BRANCH")
                End If
            End If

            If oUserListBranchName.Contains(oUserName) Or oUserListBranchName.Equals("ALL") Then
                If bDoFilter Then
                    If Not sResult Is Nothing AndAlso sResult.Length > 0 Then
                        setUFValue(oForm, "IDH_BRANCH", Config.INSTANCE.doGetBranch(""))
                    Else
                        setUFValue(oForm, "IDH_BRANCH", Config.INSTANCE.doGetBranch(goParent.gsUserName))
                    End If
                End If
            End If

            If Not oUserEnableList.Equals("ALL") Then
                If Not oUserEnableList.Contains(oUserName) Then
                    setEnableItem(oForm, False, "IDH_USER")
                End If
            End If
            If oUserListUserName.Contains(oUserName) Or oUserListUserName.Equals("ALL") Then
                If bDoFilter Then
                    setUFValue(oForm, "IDH_USER", oUserName)
                End If
            End If

            'set focus back to the first field 
            doSetFocus(oForm, "IDH_REQSTF")
        End Sub

        Public Sub doSetMultiBranch(oForm As SAPbouiCOM.Form)
            ''## MA Start 29-08-2014 Issue#421

            If isItemEnabled(oForm, "IDH_BRANCH") AndAlso isItemEnabled(oForm, "IDH_BRANC2") AndAlso oForm.Items.Item("IDH_BRANCH").Visible AndAlso Config.ParameterAsBool("MULBRNCH", False) Then
                setVisible(oForm, "IDH_BRANC1", True)
                setVisible(oForm, "IDH_CFLBR", True)
                setEnableItem(oForm, True, "IDH_CFLBR")
                setVisible(oForm, "IDH_BRANCH", False)

                Dim oCOmbo As SAPbouiCOM.ComboBox = oForm.Items.Item("IDH_BRANCH").Specific
                If oCOmbo.Selected IsNot Nothing Then
                    setUFValue(oForm, "IDH_BRANC1", oCOmbo.Selected.Description)
                    setUFValue(oForm, "IDH_BRANC2", oCOmbo.Selected.Value)
                Else
                    setUFValue(oForm, "IDH_BRANC1", "")
                    setUFValue(oForm, "IDH_BRANC2", "")
                End If
                ' If oCOmbo.ValidValues.Count > 0 Then oCOmbo.SelectExclusive("", SAPbouiCOM.BoSearchKey.psk_ByValue)
                setUFValue(oForm, "IDH_BRANCH", Config.INSTANCE.doGetBranch(""))
            Else
                setVisible(oForm, "IDH_BRANC1", False)
                setVisible(oForm, "IDH_CFLBR", False)

                setUFValue(oForm, "IDH_BRANC2", "")
            End If
            ''## MA End 29-08-2014 Issue#421
        End Sub
        Public Function getWOHStatus(ByVal oForm As SAPbouiCOM.Form, ByVal sHeaderCode As String) As String
            Dim oRS As SAPbobsCOM.Recordset
            oRS = goParent.goDB.doSelectQuery("select U_Status from [@IDH_JOBENTR] where Code = '" + sHeaderCode + "'")
            If oRS.RecordCount > 0 Then
                Return oRS.Fields.Item("U_Status").Value
            Else
                Return "-1"
            End If
        End Function
        Public Function doValidateWasteDescription(ByVal oForm As SAPbouiCOM.Form, ByVal sGrid As OrderRowGrid) '#MA issue: 20170501
            Dim oRowList As String = ""
            Dim oSelected As SAPbouiCOM.SelectedRows
            oSelected = sGrid.getGrid().Rows.SelectedRows
            If Not oSelected Is Nothing Then
                Dim iRow As Integer
                Dim iIndex As Integer
                For iIndex = 0 To oSelected.Count - 1
                    iRow = sGrid.GetDataTableRowIndex(oSelected, iIndex)
                    Dim sSOStatus As String = sGrid.doGetFieldValue("r." & IDH_JOBSHD._Status, iRow)
                    Dim sPOStatus As String = sGrid.doGetFieldValue("r." & IDH_JOBSHD._PStat, iRow)
                    If Not sSOStatus.StartsWith(FixedValues.getStatusOrdered()) OrElse
                      Not sPOStatus.StartsWith(FixedValues.getStatusOrdered()) Then
                        Dim sWasteDsc As String = Config.INSTANCE.doGetItemDescription(sGrid.doGetFieldValue("r.U_WasCd", iRow).ToString.Trim())
                        If Not sWasteDsc.Contains(sGrid.doGetFieldValue("r.U_WasDsc", iRow)) Then
                            oRowList = oRowList + "," + sGrid.doGetFieldValue("r.Code", iRow)
                        End If
                    End If
                Next
                If oRowList.Count > 0 Then
                    com.idh.bridge.DataHandler.INSTANCE.doError("Invalid Waste Description for Row :" + oRowList.TrimStart(",") + " Please Deselect!")
                    Return False
                End If
            End If
            Return True
        End Function
    End Class
End Namespace
