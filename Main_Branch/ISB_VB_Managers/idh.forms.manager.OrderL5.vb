Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System.Net
Imports System.Web

Imports com.idh.bridge.data
Imports com.idh.dbObjects.User
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.lookups

Namespace idh.forms.manager
    Public Class OrderL5
        Inherits idh.forms.manager.Order

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, iMenuPosition, "IDHJOBR5", "Job Manager5.srf", "Order Schedule Manager L5")
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            'If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
            '    If doSetGrid(oGridN) Then
            '        Return
            '    End If
            'End If
            '*** New Sequence 
            'oGridN.doAddListField("v.State", "Progress", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("v.IMG", "", False, 10, "PICTURE", Nothing)
            oGridN.doAddListField(doGetProgressStatusQuery, "Progress", False, -1, Nothing, Nothing)
            oGridN.doAddListField(doGetProgressImageQuery, "", False, 10, "PICTURE", Nothing)
            oGridN.doAddListField("r.U_JobNr", "Order No.", False, 30, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Row No.", False, 30, Nothing, Nothing)
            'Column to display Additional Items 
            'OLD Query: (SELECT CASE count(*) WHEN 0 THEN ' ' ELSE 'Y' END FROM [@IDH_WOADDEXP] WHERE U_RowNr = r.Code AND (U_SONr!='' OR U_SINVNr!='' Or U_PONr!='' OR U_PINVNr!=''))
            'oGridN.doAddListField("(SELECT CASE count(*) WHEN 0 THEN ' ' ELSE 'Y' END FROM [@IDH_WOADDEXP] WITH(NOLOCK) WHERE U_RowNr = r.Code )", "Additional Items", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_Address", "Order Address", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_Street", "Street", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_City", "City", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_RDate", "Req. Start", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASDate", "Act. Start", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act. End", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Lorry", "Vehicle Reg.", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_VehTyp", "Vehicle", True, -1, Nothing, Nothing)
            'KA -- START -- 20121025
            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            'KA -- END -- 20121025
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 100, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Waste Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CusQty", "Cont Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_CarrNm", "Carrier Name", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_Phone1", "Main No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_SiteTl", "Site Tel No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Comment", "Comment", False, 150, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_None, True, True)
            oGridN.doAddListField("r.U_Tip", "Disposal Site", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r." + IDH_JOBSHD._ProCd, "Producer Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r." + IDH_JOBSHD._ProNm, "Producer Name", False, 70, Nothing, Nothing)
            '*** Old Sequence 

            'oGridN.doAddListField("'" & IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\' + v.IMG", "", False, 10, "PICTURE", Nothing)

            oGridN.doAddListField("e.U_ForeCS", "Forecast", False, 0, Nothing, Nothing)

            If Config.INSTANCE.getParameterAsBool("OSM4DPBI", False) = True Then
                oGridN.doAddListField("r.U_IntComm", "PBI Instruction", False, 150, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_None, True, True)
            End If
            If Config.INSTANCE.getParameterAsBool("OSM4DCOV", False) = True Then
                oGridN.doAddListField("f.StartDate", "PB-StartDate", False, -1, Nothing, Nothing)
                oGridN.doAddListField("f.EndDate", "PB-EndDate", False, -1, Nothing, Nothing)
                oGridN.doAddListField("f.Pattern", "Pattern", False, -1, Nothing, Nothing)
                oGridN.doAddListField("f.Freq", "Freq", False, -1, Nothing, Nothing)
                oGridN.doAddListField("f.Sun", "Sun", False, -1, Nothing, Nothing)
                oGridN.doAddListField("f.Mon", "Mon", False, -1, Nothing, Nothing)
                oGridN.doAddListField("f.Tue", "Tue", False, -1, Nothing, Nothing)
                oGridN.doAddListField("f.Wed", "Wed", False, -1, Nothing, Nothing)
                oGridN.doAddListField("f.Thu", "Thu", False, -1, Nothing, Nothing)
                oGridN.doAddListField("f.Fri", "Fri", False, -1, Nothing, Nothing)
                oGridN.doAddListField("f.Sat", "Sat", False, -1, Nothing, Nothing)
            End If

            oGridN.doAddListField("r.U_JobTp", "Order Type", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ConNum", "Consignment Number", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_LorryCd", "Veh Reg. Code", True, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTime", "From", True, 0, "TIME", Nothing)

            oGridN.doAddListField("r.U_AETime", "Act ETime", True, 0, "TIME", Nothing)
            oGridN.doAddListField("e.U_Block", "Block", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, 0, Nothing, Nothing)

            '#MA Start 17-05-2017 issue#402 point 1&2
            'oGridN.doAddListField("ad.U_LAT", "Latitude", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("ad.U_LONG", "Longitude", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_LATI", "Latitude", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_LONGI", "Longitude", False, -1, Nothing, Nothing)

            '#MA End 17-05-2017
            oGridN.doAddListField("e.U_Contact", "Contact Person", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_SteId", "Site Id", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Price", "Haulage Total", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "Tip Total", False, 0, Nothing, Nothing)
            oGridN.doAddListField("CAST(((U_TCTotal + U_Price) - r.U_DisAmt) As Numeric(19,2))", "Sub After Discount", False, 0, Nothing, "SubAftDisc")
            'oGridN.doAddListField("(U_TCTotal +(U_CusQty * U_CusChr)) - r.U_DisAmt", "Sub After Discount", False, -1, Nothing, "SubBefDisc")
            oGridN.doAddListField("r.U_SLicCh", "Skip Lic Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CarrCd", "Carrier Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e.U_CZpCd", "Carrier PCode", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_JCost", "Total Order Cost", False, 0, Nothing, "TOrdCost")
            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("r.U_Comment","Comment"), False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASTime", "Time", True, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_RTimeT", "Req. Time To", True, 0, "TIME", Nothing)
            'oGridN.doAddListField("r.U_VehTyp","Vehicle"), True, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_IDHRTCD", "Route", True, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_IDHSEQ", "Sequence", True, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_Status", "Sales Status", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PStat", "Purchase Status", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItmGrp", "Container Grp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("r.U_Serial", "Item Serial No.", False, 0, Nothing, Nothing)
            '..
            '..
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)
            '..
            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RdWgt", "Read Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Total Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "TipCost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "TCharge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)
            'KA -- START -- 20121025
            'oGridN.doAddListField("r.U_WasDsc","Waste Description", False, 0, Nothing, Nothing)
            'KA -- END -- 20121025
            oGridN.doAddListField("r.U_Covera", "Coverage.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r." & IDH_JOBSHD._Rebate, "Rebate", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_WROrd", "WO No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WRRow", "WO Row No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CoverHst", "Coverage Hist.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtRt", "Tipping Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtRt", "Haulage Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtRt", "Tipping Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtRt", "Supplier Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtRt", "Haulage Vat Purchase Rate", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtGrp", "Tipping Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtGrp", "Haulage Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtGrp", "Tipping Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtGrp", "Supplier Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtGrp", "Haulage Vat Purchase Group", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RowSta", "Row Status", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Serial", "Serial No.", False, 70, Nothing, Nothing)

            '## MA Start 22-04-2014
            oGridN.doAddListField("r.U_AddCost", "Additional Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddCharge", "Additional Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ValDed", "Charge Deduction", False, 0, Nothing, Nothing)
            '## MA End 22-04-2014
        End Sub

        ''Need this for the auto fields linked to the search grid
        Protected Overrides Sub doSetGridFilters(ByVal oGridN As FilterGrid)
            MyBase.doSetGridFilters(oGridN)
            oGridN.doAddFilterField("IDH_PZIP", "e.U_CZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SERNUM", "r.U_Serial", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
        End Sub


        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)

            If Config.INSTANCE.getParameterAsBool("OSM4DCOV", False) = True Then
                Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")

                Dim sExtraSelect As String
                sExtraSelect = " ( SELECT Code,  " & _
                      " CONVERT(VARCHAR, (SUBSTRING(U_COVERA, LEN(U_COVERA)-15,8)),101) AS StartDate,   " & _
                      " CONVERT(VARCHAR, (SUBSTRING(U_COVERA, LEN(U_COVERA)-7,8)),101) AS EndDate, " & _
                      " SUBSTRING(U_COVERA, LEN(U_COVERA)-16,1) As Sat, " & _
                      " SUBSTRING(U_COVERA, LEN(U_COVERA)-17,1) As Fri, " & _
                      " SUBSTRING(U_COVERA, LEN(U_COVERA)-18,1) As Thu, " & _
                      " SUBSTRING(U_COVERA, LEN(U_COVERA)-19,1) As Wed, " & _
                      " SUBSTRING(U_COVERA, LEN(U_COVERA)-20,1) As Tue, " & _
                      " SUBSTRING(U_COVERA, LEN(U_COVERA)-21,1) As Mon, " & _
                      " SUBSTRING(U_COVERA, LEN(U_COVERA)-22,1) As Sun, " & _
                      " CAST(SUBSTRING(U_COVERA, LEN(U_COVERA)-25,3) AS INT) As Freq, " & _
                      " CASE WHEN SUBSTRING(U_COVERA, LEN(U_COVERA)-26,1) = 'O' THEN 'Other'  " & _
                      " WHEN SUBSTRING(U_COVERA, LEN(U_COVERA)-26,1) = 'D' THEN 'Daily'  " & _
                      " WHEN SUBSTRING(U_COVERA, LEN(U_COVERA)-26,1) = 'W' THEN 'Weekly' " & _
                      " WHEN SUBSTRING(U_COVERA, LEN(U_COVERA)-26,1) = 'M' THEN 'Monthly'  " & _
                      " WHEN SUBSTRING(U_COVERA, LEN(U_COVERA)-26,1) = 'Y' THEN 'Yearly'  " & _
                      " ELSE 'DN' " & _
                      " END AS 'Pattern', " & _
                      " CASE WHEN CAST(SUBSTRING(U_COVERA, LEN(U_COVERA)-7,8) AS INT) < 20060000 THEN 'ISFREQ' ELSE 'ISDATE' END As DATEORFREQ,    " & _
                      " U_COVERA " & _
             " FROM [@IDH_JOBSHD] " & _
             " WHERE " & _
             "    NOT (U_COVERA Is NULL OR U_COVERA LIKE 'Cre%' Or CHARINDEX('-', U_COVERA) > 0) " & _
             "    And U_COVERA > '' " & _
             "    And NOT U_COVERA LIKE 'PBI-%' " & _
             "	AND ( " & _
             "   		( " & _
             "			U_COVERA LIKE '_D000%' OR U_COVERA LIKE '_W000%' OR U_COVERA LIKE '_M000%' OR U_COVERA LIKE '_Y000%' " & _
             "			OR  " & _
             "			U_COVERA LIKE 'D000%' OR U_COVERA LIKE 'W000%' OR U_COVERA LIKE 'M000%' OR U_COVERA LIKE 'Y000%' " & _
             "		) " & _
             "		OR NOT ( " & _
             "       		(CHARINDEX('_', U_COVERA) = 1 AND SUBSTRING(U_COVERA, 3,3) = '000' ) " & _
             "        	OR " & _
             "       		(SUBSTRING(U_COVERA, 2,3) = '000') " & _
             "    	)" & _
             "    ) ) as f"

                'oGridN.setTableValue("[@IDH_JOBSHD] r, [@IDH_JOBENTR] e, IDH_VPROGRESS v, (" & sExtraSelect & ") As f ")

                'Added from the base
                'oGridN.doAddGridTable( New GridTable("@IDH_JOBSHD","r","Code",True), True)
                'oGridN.doAddGridTable( New GridTable("@IDH_JOBENTR","e","Code",True))
                'oGridN.doAddGridTable( New GridTable("@IDH_VPROGRESS","v"))
                oGridN.doAddGridTable(New GridTable(sExtraSelect), False, True)

                Dim sRFilter As String = oGridN.getRequiredFilter()
                sRFilter = sRFilter & _
                 " AND f.Code = r.Code "

                oGridN.setRequiredFilter(sRFilter)
            End If
        End Sub

    End Class
End Namespace

