Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.utils.Conversions
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.data
Imports com.idh.bridge.lookups
Imports com.idh.bridge

Namespace idh.forms.manager
    Public Class RoutePlanner
        Inherits idh.forms.manager.Templ

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHROUP", Nothing, "Route Planner.srf", iMenuPosition, "Route Planner")
        End Sub

        '        Protected Overrides Function getTitle() As String
        '            Return "Route Planner"
        '        End Function

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
        End Sub

        Protected Overrides Sub doSetGridFilters(ByVal oGridN As FilterGrid)
            '1st grid (ROUGRID from @IDH_ROUTES)
            Dim oRouteGrid As UpdateGrid = UpdateGrid.getInstance(oGridN.getSBOForm, "ROUGRID")
            oRouteGrid.doAddFilterField("IDH_ROUTE", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oRouteGrid.doAddFilterField("IDH_WDAY", "U_IDHWDAY", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            '2nd grid (JOBGRID from @IDH_JOBSHD with unassigned records)
            Dim oJobsGrid As UpdateGrid = UpdateGrid.getInstance(oGridN.getSBOForm, "JOBGRID")
            '17-11-2014
            oJobsGrid.doAddFilterField("IDH_REQSTF", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oJobsGrid.doAddFilterField("IDH_REQSTT", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oJobsGrid.doAddFilterField("IDH_NAME", "r.U_CustNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oJobsGrid.doAddFilterField("IDH_PBI", "r.U_LnkPBI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oJobsGrid.doAddFilterField("IDH_CODE", "r.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            '3rd grid (LINESGRID from @IDH_JOBSHD with assigned records)
            'oGridN.doAddFilterField("IDH_REQSTF", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            'oGridN.doAddFilterField("IDH_REQSTT", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_FROUTE", "r.U_IDHRTCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255) 'Route must not be empty = assigned
            oGridN.doAddFilterField("IDH_RTDT", "U_IDHRTDT", SAPbouiCOM.BoDataType.dt_DATE, "=", 10)

        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            'Changes as requested by 707 
            oGridN.doAddListField("r.U_IDHRTCD", "Route", True, 0, Nothing, Nothing) 'Route Code from jobshd
            oGridN.doAddListField("r.U_IDHSEQ", "Seq", True, -1, Nothing, Nothing) 'Route Sequence from jobshd
            oGridN.doAddListField("r.Code", "Row No", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, -1, Nothing, Nothing) ', -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e.U_Address", "Address", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_WasCd", "Waste Type", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_ItemCd", "Container Type", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RDate", "Req. Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTime", "Req. Time", False, -1, "TIME", Nothing)
            oGridN.doAddListField("r.U_IDHRTDT", "Route Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_IDHCMT", "Route Comments", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_IDHDRVI", "Driver Inst", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_IDHCHK", "Check", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_IDHPRTD", "Printed", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Lorry", "Vehicle Reg.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_LorryCd", "Veh Reg. Code", True, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", True, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobNr", "Order No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_ASDate", "Act. Start", True, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASTime", "Act. Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act EDate", True, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AETime", "Act ETime", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Serial", "Container Serial No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Street", "Street", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_SteId", "Site Id", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItmGrp", "Item Grp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Tip", "Supplier", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Cust. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RdWgt", "Read Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Price", "Price", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicCh", "SLicCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Charge Total", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_VehTyp", "Vehicle", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "TipCost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "TCharge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "TCTotal", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CarrCd", "Carrier Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_CarrNm", "Carrier Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WROrd", "WO No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WRRow", "WO Row No.", False, 0, Nothing, Nothing)
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                oForm.Title = gsTitle

                Dim oItem As SAPbouiCOM.Item
                oItem = doAddUF(oForm, "IDH_WEEK", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 2)
                oItem.AffectsFormMode = False
                oItem = doAddUF(oForm, "IDH_REQSTF", SAPbouiCOM.BoDataType.dt_DATE, 10)
                oItem.AffectsFormMode = False
                oItem = doAddUF(oForm, "IDH_REQSTT", SAPbouiCOM.BoDataType.dt_DATE, 10)
                oItem.AffectsFormMode = False
                oItem = doAddUF(oForm, "IDH_ROUTE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10)
                oItem.AffectsFormMode = False
                oItem = doAddUF(oForm, "IDH_PBI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10)
                oItem.AffectsFormMode = False
                oItem = doAddUF(oForm, "IDH_NAME", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30)
                oItem.AffectsFormMode = False
                oItem = doAddUF(oForm, "IDH_CODE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10)
                oItem.AffectsFormMode = False
                oItem = doAddUF(oForm, "IDH_FROUTE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10)
                oItem.AffectsFormMode = False
                oItem = doAddUF(oForm, "IDH_WDAY", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30)
                oItem.AffectsFormMode = False

                oItem = doAddUFCheck(oForm, "IDHMON", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False
                oItem = doAddUFCheck(oForm, "IDHTUE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False
                oItem = doAddUFCheck(oForm, "IDHWED", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False
                oItem = doAddUFCheck(oForm, "IDHTHU", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False
                oItem = doAddUFCheck(oForm, "IDHFRI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False
                oItem = doAddUFCheck(oForm, "IDHSAT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False
                oItem = doAddUFCheck(oForm, "IDHSUN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False

                oItem = doAddUF(oForm, "IDH_RTDT", SAPbouiCOM.BoDataType.dt_DATE, 10)
                oItem.AffectsFormMode = False

                Dim oRouteGrid As UpdateGrid = New UpdateGrid(Me, oForm, "ROUGRID", 7, 40, 420, 230)
                Dim oJobsGrid As UpdateGrid = New UpdateGrid(Me, oForm, "JOBGRID", 7, 302, 420, 208)
                Dim oGridN As OrderRowGrid = New OrderRowGrid(Me, oForm, "LINESGRID", 427, 40, 500, 470, "r")

                oRouteGrid.getSBOItem().AffectsFormMode = False
                oJobsGrid.getSBOItem().AffectsFormMode = False

                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False

                oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_FIND_MODE Or SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

                oForm.EnableMenu(Config.NAV_ADD, False)
                oForm.EnableMenu(Config.NAV_FIND, False)
                oForm.EnableMenu(Config.NAV_FIRST, False)
                oForm.EnableMenu(Config.NAV_NEXT, False)
                oForm.EnableMenu(Config.NAV_PREV, False)
                oForm.EnableMenu(Config.NAV_LAST, False)

                Dim fSettings As SAPbouiCOM.FormSettings
                fSettings = oForm.Settings
                fSettings.Enabled = False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try
        End Sub

        Protected Overridable Sub doSetJOBGRIDFilters(ByRef oJobsGrid As UpdateGrid)
        End Sub
        Protected Overridable Sub doSetROUGRIDFilters(ByRef oRouteGrid As UpdateGrid)
        End Sub

        Public Overridable Function getListJobRequiredStr() As String
            'Return "[U_Status] != 'Complete' AND [U_IDHRTCD] is NULL"
            'Return "[U_Status] NOT IN ('Ordered', 'FOC') AND [U_IDHRTCD] is NULL"

            '20140731: Added header table to display Customer Site in Unassigned Job grid 
            'Return "[U_Status] NOT IN ('Ordered', 'FOC') AND ([U_IDHRTCD] is NULL OR [U_IDHRTCD] = '') "
            '17-11-2014
            Return "r.U_JobNr = h.Code AND r.[U_Status] NOT IN ('Ordered', 'FOC') AND (r.[U_IDHRTCD] is NULL OR r.[U_IDHRTCD] = '') "
            'Return "r.[U_Status] NOT IN ('Ordered', 'FOC') AND (r.[U_IDHRTCD] is NULL OR r.[U_IDHRTCD] = '') "


            ''Return "r.[U_Status] NOT IN ('Ordered', 'FOC') AND (r.[U_IDHRTCD] is NULL OR r.[U_IDHRTCD] = '') AND r.U_JobNr = e.Code "
        End Function
        Public Overridable Function getListRouteRequiredStr() As String
            'Dim oForm As SAPbouiCOM.Form
            Dim sField As String = "Isnull([U_IDHSTATUS],'') != 'Y'"
            Return sField
        End Function

        Protected Overridable Sub doSetListJobsFields(ByRef oJobsGrid As IDHAddOns.idh.controls.UpdateGrid)
            oJobsGrid.doAddListField("r.Code", "Row No", False, -1, Nothing, Nothing)
            oJobsGrid.doAddListField("r.U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oJobsGrid.doAddListField("r.U_WasCd", "Waste Type", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oJobsGrid.doAddListField("r.U_ItemCd", "Container Type", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oJobsGrid.doAddListField("r.U_RDate", "Req. Date", False, -1, Nothing, Nothing)
            oJobsGrid.doAddListField("r.U_RTime", "Req. Time", False, -1, "TIME", Nothing)
            oJobsGrid.doAddListField("r.U_CustNm", "Customer", False, -1, Nothing, Nothing)
            '17-11-2014
            oJobsGrid.doAddListField("h.U_Address", "Site Address", False, -1, Nothing, Nothing)
            oJobsGrid.doAddListField("r.U_WasCd", "Waste", False, -1, Nothing, Nothing)
            oJobsGrid.doAddListField("r.U_LnkPBI", "PBI No", False, -1, Nothing, Nothing)
            oJobsGrid.doAddListField("r.U_IDHCMT", "Route Comment", False, -1, Nothing, Nothing)
            oJobsGrid.doAddListField("r.U_Status", "Status", False, -1, Nothing, Nothing)
            oJobsGrid.doAddListField("r.U_IDHRTCD", "Route", True, -1, Nothing, Nothing)
            oJobsGrid.doAddListField("r.U_IDHSEQ", "Sequence", True, -1, Nothing, Nothing)
            oJobsGrid.doAddListField("r.U_IDHRTDT", "Route Date", True, -1, Nothing, Nothing)

            oJobsGrid.doAddListField("h.U_CardCd", "Customer", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oJobsGrid.doAddListField("r.U_CarrCd", "Carrier", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oJobsGrid.doAddListField("r.U_SLicSp", "Site Lic Supplier", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oJobsGrid.doAddListField("h.U_PAddress", "Producer Address", False, 0, Nothing, Nothing)
            oJobsGrid.doAddListField("r.U_ProCd", "Supplier", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oJobsGrid.doAddListField("r.U_Tip", "Disposal Site", False, 0, Nothing, Nothing)


        End Sub

        Protected Overridable Sub doSetListRouteFields(ByRef oRouteGrid As IDHAddOns.idh.controls.UpdateGrid)
            oRouteGrid.doAddListField("Code", "Route No", False, 8, Nothing, Nothing)
            oRouteGrid.doAddListField("U_IDHAREA", "Description", False, 20, Nothing, Nothing)
            oRouteGrid.doAddListField("U_IDHZPCD", "Zone", False, 20, Nothing, Nothing)
            oRouteGrid.doAddListField("U_IDHWDAY", "Week Days", False, 20, Nothing, Nothing)
            oRouteGrid.doAddListField("U_IDHWCD", "Waste", False, 20, Nothing, Nothing)
            oRouteGrid.doAddListField("U_IDHCCD", "Container", False, 20, Nothing, Nothing)
            oRouteGrid.doAddListField("U_IDHVHRG", "Vehicle", False, 20, Nothing, Nothing)
            oRouteGrid.doAddListField("U_IDHDRVR", "Driver", False, 20, Nothing, Nothing)
			oRouteGrid.doAddListField("U_COST", "Cost", False, 20, Nothing, Nothing)
        End Sub

        Public Overrides Sub doHideTotals(ByVal oForm As SAPbouiCOM.Form)

        End Sub
        Protected Overrides Sub doItemGroupCombo(ByRef oForm As SAPbouiCOM.Form)

        End Sub
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            doAddWF(oForm, "RouteData", Nothing) 'Route Grid
            doAddWF(oForm, "JobsData", Nothing) 'unassigned Jobs Grid
            doAddWF(oForm, "JobData", Nothing) 'assigned Jobs Grid

            setUFValue(oForm, "IDH_FROUTE", "99999999")

            Dim oRouteGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "ROUGRID")
            If oRouteGrid Is Nothing Then
                oRouteGrid = New UpdateGrid(Me, oForm, "ROUGRID")
            End If
            oRouteGrid.doSetSBOAutoReSize(False)

            Dim oJobsGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "JOBGRID")
            If oJobsGrid Is Nothing Then
                oJobsGrid = New UpdateGrid(Me, oForm, "JOBGRID")
            End If
            oJobsGrid.doSetSBOAutoReSize(False)

            MyBase.doBeforeLoadData(oForm)

            doSetROUGRIDFilters(oRouteGrid)
            oRouteGrid.doAutoExpand(True)

            oRouteGrid.doAddGridTable(New GridTable("@IDH_ROUTES", Nothing, "Code", True, True), True)

            oRouteGrid.setOrderValue("Code")
            oRouteGrid.setRequiredFilter(getListRouteRequiredStr())
            oRouteGrid.doSetDoCount(False)
            oRouteGrid.doSetBlankLine(False)
            doSetListRouteFields(oRouteGrid)

            doSetJOBGRIDFilters(oJobsGrid)
            oJobsGrid.doAutoExpand(True)
            '17-11-2014
            oJobsGrid.doAddGridTable(New GridTable("@IDH_JOBSHD", "r", "Code", True, True), True)
            'oJobsGrid.doAddGridTable(New GridTable("@IDH_JOBSHD", "", "Code", True, True), True)

            '20140731: Added header table to display Customer Site in Unassigned Job grid 
            '17-11-2014
            oJobsGrid.doAddGridTable(New GridTable("@IDH_JOBENTR", "h", "Code", True, True))

            oJobsGrid.setOrderValue("r.Code")
            oJobsGrid.setRequiredFilter(getListJobRequiredStr())
            oJobsGrid.doSetDoCount(False)
            oJobsGrid.doSetBlankLine(False)
            doSetListJobsFields(oJobsGrid)

            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            oGridN.setOrderValue("r.U_IDHSEQ") 'Order by Sequence, in discussion: order by requested date and time, maybe switchable by a button between those? Or by config?
            oGridN.doSetDoCount(True)
            oGridN.doSetBlankLine(True)
        End Sub

        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            'doSetEnabled(oForm.Items.Item("IDH_WS"), True)
            Dim oBtn As SAPbouiCOM.Button

            oForm.Items.Item("IDH_RS2").Visible = True
            oBtn = oForm.Items.Item("IDH_RS2").Specific
            oBtn.Caption = Config.INSTANCE.getParameter("RSBTN2")

            oForm.Items.Item("IDH_BRTS").Visible = True
            oBtn = oForm.Items.Item("IDH_BRTS").Specific
            oBtn.Caption = Config.INSTANCE.getParameter("RSBTN1")

            doSetFocus(oForm, "IDH_ROUTE")
        End Sub

        Protected Sub doReloadJobs(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oJobsGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "JOBGRID")
                oJobsGrid.doReloadData()
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error reloading the unassigned Jobs.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRLUJ", {Nothing})
            Finally
            End Try
        End Sub
        Protected Sub doReloadRoute(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oRouteGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "ROUGRID")
                oRouteGrid.doReloadData()
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error reloading the Route.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            Finally
            End Try
        End Sub
        'Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
        Protected Overrides Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean)
            Dim sWeek As String = CStr(getUFValue(oForm, "IDH_WEEK"))
            If sWeek > "0" And sWeek < "53" Then
                doSetFilterDates(oForm, sWeek)
            End If
            doPrepareDayFilter(oForm)
            oForm.Freeze(True)
            MyBase.doReLoadData(oForm, bIsFirst)
            doReloadRoute(oForm)
            doReloadJobs(oForm)
            oForm.Freeze(False)
        End Sub


        Private Sub doSizeGrids(ByVal oForm As SAPbouiCOM.Form)
            Dim oRouteGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "ROUGRID")
            Dim oJobsGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "JOBGRID")
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If Not oRouteGrid Is Nothing Then
                oRouteGrid.getItem().Top = Int(oGridN.getItem().Top)
                oRouteGrid.getItem().Width = 420
                oRouteGrid.getItem().Height = Int(oGridN.getItem().Height / 2)
            End If
            If Not oJobsGrid Is Nothing Then
                oForm.Items.Item("lblJobs").Top = Int(oGridN.getItem().Top + Int(oGridN.getItem().Height / 2) + 1) 'Label position of unassigned Job grid (Top)
                oForm.Items.Item("lblPBI").Top = Int(oGridN.getItem().Top + Int(oGridN.getItem().Height / 2) + 1) 'Label position of unassigned Job grid (Top)
                oForm.Items.Item("IDH_PBI").Top = Int(oGridN.getItem().Top + Int(oGridN.getItem().Height / 2) + 1) 'Filter field position of unassigned Job grid (Top)
                oForm.Items.Item("lblCODE").Top = Int(oGridN.getItem().Top + Int(oGridN.getItem().Height / 2) + 1) 'Label position of unassigned Job grid (Top)
                oForm.Items.Item("IDH_CODE").Top = Int(oGridN.getItem().Top + Int(oGridN.getItem().Height / 2) + 1) 'Filter field position of unassigned Job grid (Top)
                oForm.Items.Item("lblNAME").Top = Int(oGridN.getItem().Top + Int(oGridN.getItem().Height / 2) + 16) 'Label position of unassigned Job grid (Top)
                oForm.Items.Item("IDH_NAME").Top = Int(oGridN.getItem().Top + Int(oGridN.getItem().Height / 2) + 16) 'Filter field position of unassigned Job grid (Top)

                oJobsGrid.getItem().Top = Int(oGridN.getItem().Top + Int(oGridN.getItem().Height / 2) + 32) 'Grid position of Job grid(Top)
                oJobsGrid.getItem().Width = 420
                oJobsGrid.getItem().Height = Int(oGridN.getItem().Height / 2 - 32)
            End If
            oForm.Freeze(False)

        End Sub


        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim bRes As Boolean = MyBase.doItemEvent(oForm, pVal, BubbleEvent)

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_RESIZE Then
                If pVal.BeforeAction = False Then
                    doSizeGrids(oForm)
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
                '*** Using this and not the Custom Event bacause of the drag and drop function, 
                '*** requiring the selection list before the button is realsed to register the normal selection
                Try
                    If pVal.BeforeAction = True Then
                        If pVal.ItemUID = "JOBGRID" Then
                            Dim oJobsGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "JOBGRID")
                            Dim sJobsCode As String = ""
                            If pVal.Row >= 0 AndAlso oJobsGrid.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                                'new to unassigned jobgrid: multiselsect
                                Dim jSelected As ArrayList
                                jSelected = oJobsGrid.getDataTableSelection()
                                Dim jRows As New ArrayList
                                Dim iRow As Integer = pVal.Row
                                Dim jClkRow As Integer = oJobsGrid.getSBOGrid().GetDataTableRowIndex(iRow)
                                Dim jRowInSelection As Boolean = False
                                If Not jSelected Is Nothing Then
                                    For iIndex As Integer = 0 To jSelected.Count - 1
                                        Dim oRow As New ArrayList
                                        iRow = jSelected.Item(iIndex)
                                        '17-11-2014
                                        sJobsCode = oJobsGrid.doGetFieldValue("r.Code", iRow)
                                        oRow.Add(sJobsCode)
                                        oRow.Add(iRow)
                                        jRows.Add(oRow)
                                        If iRow = jClkRow Then
                                            jRowInSelection = True
                                        End If
                                    Next
                                End If
                                'end multiselect
                                'oJobsGrid.setCurrentLineByClick(pVal.Row)
                                'sJobsCode = oJobsGrid.doGetFieldValue("Code")
                                'setWFValue(oForm, "JobsData", sJobsCode)
                                If jRowInSelection = False Then
                                    '** deselect previous and select the current **'
                                    If Not pVal.Modifiers = SAPbouiCOM.BoModifiersEnum.mt_SHIFT AndAlso _
                                     jRows.Count() > 0 Then
                                        Dim oSelectedR As SAPbouiCOM.SelectedRows
                                        oSelectedR = oJobsGrid.getSBOGrid.Rows.SelectedRows()
                                        oSelectedR.Clear()
                                        oSelectedR.Add(iRow)
                                    End If
                                    jRows.Clear()
                                    Dim oRow As New ArrayList
                                    '17-11-2014
                                    sJobsCode = oJobsGrid.doGetFieldValue("r.Code", jClkRow)
                                    oRow.Add(sJobsCode)
                                    oRow.Add(iRow)
                                    jRows.Add(oRow)
                                End If
                                setWFValue(oForm, "JobsData", jRows)
                                
                            End If
                        ElseIf pVal.ItemUID = "LINESGRID" Then
                            'Possible problem: the program hangs up if same row is clicked several times
                            'without clicking another row in the meantime (oRow problem?)
                            'this is not solved yet
                            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                            If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                                Dim oSelected As ArrayList
                                oSelected = oGridN.getDataTableSelection()
                                Dim oRows As New ArrayList
                                Dim iRow As Integer = pVal.Row
                                Dim iClkRow As Integer = oGridN.getSBOGrid().GetDataTableRowIndex(iRow)
                                Dim sTJobsRoute As String
                                Dim sRowCode As String
                                Dim bRowInSelection As Boolean = False
                                Dim sSequence As String
                                If Not oSelected Is Nothing Then
                                    For iIndex As Integer = 0 To oSelected.Count - 1
                                        Dim oRow As New ArrayList
                                        iRow = oSelected.Item(iIndex)
                                        sTJobsRoute = oGridN.doGetFieldValue("r.Code", iRow)
                                        sRowCode = oGridN.doGetFieldValue("r.Code", iRow)
                                        sSequence = oGridN.doGetFieldValue("r.U_IDHSEQ", iRow)
                                        oRow.Add(sTJobsRoute)
                                        oRow.Add(iRow)
                                        oRow.Add(sRowCode)
                                        oRow.Add(sSequence)
                                        oRows.Add(oRow)
                                        If iRow = iClkRow Then
                                            bRowInSelection = True
                                        End If
                                    Next
                                End If
                                If bRowInSelection = False Then
                                    '** deselect previous and select the current **'
                                    If Not pVal.Modifiers = SAPbouiCOM.BoModifiersEnum.mt_SHIFT AndAlso _
                                     oRows.Count() > 0 Then
                                        Dim oSelectedR As SAPbouiCOM.SelectedRows
                                        oSelectedR = oGridN.getSBOGrid.Rows.SelectedRows()
                                        oSelectedR.Clear()
                                        oSelectedR.Add(iRow)
                                    End If
                                    oRows.Clear()
                                    Dim oRow As New ArrayList
                                    sTJobsRoute = oGridN.doGetFieldValue("r.Code", iClkRow)
                                    sRowCode = oGridN.doGetFieldValue("r.Code", iClkRow)
                                    sSequence = oGridN.doGetFieldValue("r.U_IDHSEQ", iClkRow)
                                    oRow.Add(sTJobsRoute)
                                    oRow.Add(iClkRow) 'oData.Add(oGridN.getCurrentLine())
                                    oRow.Add(sRowCode)
                                    oRow.Add(sSequence)
                                    oRows.Add(oRow)
                                End If
                                setWFValue(oForm, "JobData", oRows)
                            End If
                        ElseIf pVal.ItemUID = "IDHMON" Then
                            setUFValue(oForm, "IDHTUE", "N")
                            setUFValue(oForm, "IDHWED", "N")
                            setUFValue(oForm, "IDHTHU", "N")
                            setUFValue(oForm, "IDHFRI", "N")
                            setUFValue(oForm, "IDHSAT", "N")
                            setUFValue(oForm, "IDHSUN", "N")
                            setUFValue(oForm, "IDH_FROUTE", "99999999")
                        ElseIf pVal.ItemUID = "IDHTUE" Then
                            setUFValue(oForm, "IDHMON", "N")
                            setUFValue(oForm, "IDHWED", "N")
                            setUFValue(oForm, "IDHTHU", "N")
                            setUFValue(oForm, "IDHFRI", "N")
                            setUFValue(oForm, "IDHSAT", "N")
                            setUFValue(oForm, "IDHSUN", "N")
                            setUFValue(oForm, "IDH_FROUTE", "99999999")
                        ElseIf pVal.ItemUID = "IDHWED" Then
                            setUFValue(oForm, "IDHMON", "N")
                            setUFValue(oForm, "IDHTUE", "N")
                            setUFValue(oForm, "IDHTHU", "N")
                            setUFValue(oForm, "IDHFRI", "N")
                            setUFValue(oForm, "IDHSAT", "N")
                            setUFValue(oForm, "IDHSUN", "N")
                            setUFValue(oForm, "IDH_FROUTE", "99999999")
                        ElseIf pVal.ItemUID = "IDHTHU" Then
                            setUFValue(oForm, "IDHMON", "N")
                            setUFValue(oForm, "IDHTUE", "N")
                            setUFValue(oForm, "IDHWED", "N")
                            setUFValue(oForm, "IDHFRI", "N")
                            setUFValue(oForm, "IDHSAT", "N")
                            setUFValue(oForm, "IDHSUN", "N")
                            setUFValue(oForm, "IDH_FROUTE", "99999999")
                        ElseIf pVal.ItemUID = "IDHFRI" Then
                            setUFValue(oForm, "IDHMON", "N")
                            setUFValue(oForm, "IDHTUE", "N")
                            setUFValue(oForm, "IDHWED", "N")
                            setUFValue(oForm, "IDHTHU", "N")
                            setUFValue(oForm, "IDHSAT", "N")
                            setUFValue(oForm, "IDHSUN", "N")
                            setUFValue(oForm, "IDH_FROUTE", "99999999")
                        ElseIf pVal.ItemUID = "IDHSAT" Then
                            setUFValue(oForm, "IDHMON", "N")
                            setUFValue(oForm, "IDHTUE", "N")
                            setUFValue(oForm, "IDHWED", "N")
                            setUFValue(oForm, "IDHTHU", "N")
                            setUFValue(oForm, "IDHFRI", "N")
                            setUFValue(oForm, "IDHSUN", "N")
                            setUFValue(oForm, "IDH_FROUTE", "99999999")
                        ElseIf pVal.ItemUID = "IDHSUN" Then
                            setUFValue(oForm, "IDHMON", "N")
                            setUFValue(oForm, "IDHTUE", "N")
                            setUFValue(oForm, "IDHWED", "N")
                            setUFValue(oForm, "IDHTHU", "N")
                            setUFValue(oForm, "IDHFRI", "N")
                            setUFValue(oForm, "IDHSAT", "N")
                            setUFValue(oForm, "IDH_FROUTE", "99999999")
                        End If
                    Else
                    End If
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing the item event - " & pVal.ItemUID & "->" & pVal.EventType)
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXITME", {pVal.ItemUID, pVal.EventType})
                End Try
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = True Then
                    If pVal.ItemUID = "JOBGRID" Then
                        'doDebug("ITEM_PRESSED - unassigned Jobs", 0)
                        DataHandler.INSTANCE.DebugTick2 = "ITEM_PRESSED - unassigned Jobs"
                    End If
                Else
                    If pVal.ItemUID = "IDH_ROUTE" Then
                        'doReloadRoute(oForm)
                    ElseIf pVal.ItemUID = "IDH_CTSEQ" Then
                        doUpdatePreSequaance(oForm)
                    ElseIf pVal.ItemUID = "IDH_MOVEDN" OrElse pVal.ItemUID = "IDH_MOVEUP" Then
                        doMoveRout(oForm, pVal.ItemUID)
                    ElseIf pVal.ItemUID = "ROUGRID" Then
                        Dim oRouteGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "ROUGRID")
                        Try
                            If oRouteGrid.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                                oRouteGrid.setCurrentLineByClick(pVal.Row)
                                Dim sRoute As String = oRouteGrid.doGetFieldValue("Code")
                                'doDebug("ITEM_PRESSED - Route Grid = " & sRoute, 0)
                                DataHandler.INSTANCE.DebugTick2 = "ITEM_PRESSED - Route Grid = " & sRoute
                                If Not sRoute Is Nothing AndAlso sRoute.Length > 0 Then
                                    setUFValue(oForm, "IDH_FROUTE", sRoute)
                                    'doReLoadData(oForm, False)
                                    oForm.Items.Item("1").Click()
                                Else
                                    setUFValue(oForm, "IDH_FROUTE", "99999999")
                                    'doReLoadData(oForm, False)
                                    oForm.Items.Item("1").Click()
                                End If
                            End If
                        Catch ex As Exception
                            setUFValue(oForm, "IDH_FROUTE", "99999999")
                            'doDebug("ITEM_PRESSED - Route Grid, no Route selected", 0)
                            DataHandler.INSTANCE.DebugTick2 = "ITEM_PRESSED - Route Grid, no Route selected"
                            oForm.Items.Item("1").Click()
                        End Try
                    ElseIf pVal.ItemUID = "JOBGRID" Then
                        Dim oRows As ArrayList = getWFValue(oForm, "JobData")
                         If Not oRows Is Nothing Then
                            oForm.Freeze(True)
                            Try
                                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                                Dim bChanged As Boolean = False
                                Dim oRow As ArrayList
                                Dim iRow As Integer
                                For iIndex As Integer = 0 To oRows.Count - 1
                                    oRow = oRows.Item(iIndex)
                                    iRow = oRow.Item(1) 'this is the row number on the planner grid
                                    oGridN.setCurrentDataRowIndex(iRow)
                                    oGridN.doSetFieldValue("r.U_IDHRTCD", iRow, "")
                                    oGridN.doSetFieldValue("r.U_IDHSEQ", iRow, 0)
                                    oGridN.doSetFieldValue("r.U_IDHRTDT", iRow, "")
                                    bChanged = True
                                Next
                                If bChanged = True Then
                                    doSetUpdate(oForm)
                                    oForm.Items.Item("1").Click()
                                End If
                                'doDebug("ITEM_PRESSED - Planner Grid", 0)
                                DataHandler.INSTANCE.DebugTick2 = "ITEM_PRESSED - Planner Grid"
                            Catch ex As Exception
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the Planner Grid")
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSPLG", {Nothing})
                            End Try
                            oForm.Freeze(False)
                        End If
                    ElseIf pVal.ItemUID = "LINESGRID" Then
                        Dim sJobsCode As String
                        Dim oRows As ArrayList = getWFValue(oForm, "JobsData")
                        Dim sRoute As String = getUFValue(oForm, "IDH_FROUTE")
                        Dim sDate As String = getUFValue(oForm, "IDH_RTDT")
                        Dim dDate As DateTime = com.idh.utils.Dates.doStrToDate(sDate)
                       
                        If Not oRows Is Nothing Then
                            oForm.Freeze(True)
                            Try
                                Dim oJobsGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "JOBGRID")
                                Dim oRow As ArrayList
                                Dim iRow As Integer
                                If sRoute <> "99999999" And sDate.Length <> 0 Then
                                    For iIndex As Integer = 0 To oRows.Count - 1
                                        oRow = oRows.Item(iIndex)
                                        iRow = oRow.Item(1) 'this is the row number on the job grid
                                        sJobsCode = oRow.Item(0) 'only for debug to show the code value of the record
                                        oJobsGrid.setCurrentDataRowIndex(iRow)
                                        '17-11-2014
                                        oJobsGrid.doSetFieldValue("r.U_IDHRTCD", iRow, sRoute)
                                        oJobsGrid.doSetFieldValue("r.U_IDHSEQ", iRow, 9999)
                                        oJobsGrid.doSetFieldValue("r.U_IDHRTDT", iRow, dDate)
                                        '17-11-2014
                                    Next
                                    doSetUpdate(oForm)
                                    doSaveOrderRows(oForm)
                                    'If oJobsGrid.doProcessData() Then
                                    'End If
                                    doReLoadData(oForm, True)
                                End If
                                'doDebug("ITEM_PRESSED - unassigned Jobs Grid", 0)
                                DataHandler.INSTANCE.DebugTick2 = "ITEM_PRESSED - unassigned Jobs Grid"
                            Catch ex As Exception
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the unassigned Jobs Grid")
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSUJG", {Nothing})
                            End Try
                            oForm.Freeze(False)
                        End If
                    End If 'END pVal.ItemUID = ...
                    setWFValue(oForm, "RouteData", Nothing) 'Key Code of Route Grid
                    setWFValue(oForm, "JobData", Nothing) 'Key Code of planner grid for moving to unassigned Job
                    setWFValue(oForm, "JobsData", Nothing) 'Key Code of unassigned Job for moving to planner grid
                End If 'END pVal.BeforeAction = ...
            End If 'END pVal.EventType = ...
            Return True
        End Function

        'Public Overrides Sub doButtonID1(oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        '    UpdatePreSequaance(oForm)
        '    MyBase.doButtonID1(oForm, pVal, BubbleEvent)
        'End Sub
        Protected Sub doMoveRout(oForm As SAPbouiCOM.Form, sItemUID As String)
            'IDH_MOVEDN
            'IDH_MOVEUP
            Dim oGridN As UpdateGrid
            Try
                If (oForm.Items.Item("1").Specific).Caption = "Update" Then
                    com.idh.bridge.DataHandler.INSTANCE.doResError("Please click Update to save un-committed data before updating rout priority.", "ERRPMVV1", Nothing)
                    Exit Sub
                End If
                oGridN = UpdateGrid.getInstance(oForm, "LINESGRID")
                If oGridN.getSelectedRows Is Nothing OrElse oGridN.getSelectedRows.Count <> 1 Then
                    com.idh.bridge.DataHandler.INSTANCE.doResError("Please select one row to update rout priority", "ERRPMVV2", Nothing)
                    Exit Sub
                End If
                Dim aRows As ArrayList = oGridN.getSelectedRows()
                If sItemUID = "IDH_MOVEDN" Then
                    If aRows(0) = oGridN.getRowCount - 1 Then
                        Return
                    End If
                    Dim seqno = CInt(oGridN.doGetFieldValue("r.U_IDHSEQ", aRows(0)))
                    'If seqno Then
                    oGridN.doSetFieldValue("r.U_IDHSEQ", aRows(0), seqno + 1)
                    oGridN.doSetFieldValue("r.U_IDHSEQ", aRows(0) + 1, seqno)
                    'End If
                ElseIf sItemUID = "IDH_MOVEUP" Then
                    If aRows(0) = 0 Then
                        Return
                    End If
                    Dim seqno = CInt(oGridN.doGetFieldValue("r.U_IDHSEQ", aRows(0)))
                    'If seqno Then
                    oGridN.doSetFieldValue("r.U_IDHSEQ", aRows(0), seqno - 1)
                    oGridN.doSetFieldValue("r.U_IDHSEQ", aRows(0) - 1, seqno)
                    'End If
                End If
                oForm.Freeze(True)
                doSetUpdate(oForm)
                oForm.Items.Item("1").Click()
                If sItemUID = "IDH_MOVEDN" Then
                    Dim g As SAPbouiCOM.Grid = oGridN.getSBOGrid
                    g.Rows.SelectedRows.Add(aRows(0) + 1)
                Else
                    Dim g As SAPbouiCOM.Grid = oGridN.getSBOGrid
                    g.Rows.SelectedRows.Add(aRows(0) - 1)
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error updating the rout's custom sequence")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXURCS", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
            ' oGridN.setCurrentLineByClick(16)
        End Sub

        Protected Sub doUpdatePreSequaance(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            Dim aRows As ArrayList = oGridN.doGetChangedRows()
            'Dim sField As String = ""
            'Dim oValue As Object = ""
            'Dim sKey1 As String = ""
            ' Dim oAuditObj1 As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_JOBSHD")
            Try
                'Dim iResult As Integer
                Dim sResult As String = Nothing
                Dim GridRowsCount As Int32 = oGridN.getRowCount
                'Dim aChangedFields As ArrayList
                If Not aRows Is Nothing Then
                    oForm.Freeze(True)
                    'Dim aCodes As New ArrayList
                    For iIndex As Integer = 0 To aRows.Count - 1
                        'oGridN.setCurrentDataRowIndex(aRows(iIndex))
                        Dim iSeq As Int32 = oGridN.doGetFieldValue("r.U_IDHSEQ", aRows(iIndex))

                        If iSeq <= aRows(iIndex) AndAlso iSeq + 1 < oGridN.getRowCount AndAlso iSeq - 1 > -1 Then
                            Dim FSeq As Int32 = oGridN.doGetFieldValue("r.U_IDHSEQ", iSeq - 1)
                            Dim TSeq As Int32 = oGridN.doGetFieldValue("r.U_IDHSEQ", aRows(iIndex) - 1)
                            Dim code As Int32 = oGridN.doGetFieldValue("r.Code", aRows(iIndex))

                            While FSeq <= TSeq
                                If code <> oGridN.doGetFieldValue("r.Code", FSeq - 1) Then
                                    oGridN.doSetFieldValue("r.U_IDHSEQ", FSeq - 1, FSeq + 1)
                                End If
                                FSeq += 1
                            End While
                        ElseIf iSeq >= aRows(iIndex) AndAlso iSeq < oGridN.getRowCount Then
                            Dim TSeq As Int32 = oGridN.doGetFieldValue("r.U_IDHSEQ", aRows(iIndex))
                            Dim FSeq As Int32 = oGridN.doGetFieldValue("r.U_IDHSEQ", aRows(iIndex) + 1)
                            Dim code As Int32 = oGridN.doGetFieldValue("r.Code", aRows(iIndex))

                            While FSeq <= TSeq
                                If code <> oGridN.doGetFieldValue("r.Code", FSeq - 1) Then
                                    oGridN.doSetFieldValue("r.U_IDHSEQ", FSeq - 1, FSeq - 1)
                                End If
                                FSeq += 1
                            End While

                            'oGridN.doSetFieldValue("r.U_IDHSEQ", iSeq - 1, iSeq)

                        End If
                        'If aRows(iIndex) > 0 Then
                        '    oGridN.doSetFieldValue("r.U_IDHSEQ", aRows(iIndex) - 1, iSeq - 1)
                        'End If

                        'aCodes.Add(sKey1)
                    Next
                    doSetUpdate(oForm)
                    oForm.Items.Item("1").Click()

                    'If oAuditObj1.setKeyPair(sKey1, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then 'AndAlso _
                    'aChangedFields = oGridN.doGetChangedFields(aRows(iIndex))
                    'For iField As Integer = 0 To aChangedFields.Count - 1

                    'sField = aChangedFields.Item(iField)
                    'oValue = oGridN.doGetFieldValue("r.U_IDHSEQ")

                    'If sField = ("r.U_IDHSEQ") Then
                    Dim nextval As Int32 = 0
                    For j As Int32 = 0 To GridRowsCount - 1
                        'Dim og As SAPbouiCOM.Grid = oGridN.getSBOGrid
                        If j < GridRowsCount - 1 Then
                            If CInt(oGridN.doGetFieldValue("r.U_IDHSEQ", j)) = CInt(oGridN.doGetFieldValue("r.U_IDHSEQ", j + 1)) AndAlso CInt(oGridN.doGetFieldValue("r.Code", j)) > CInt(oGridN.doGetFieldValue("r.Code", j + 1)) Then
                                nextval = CInt(oGridN.doGetFieldValue("r.U_IDHSEQ", j + 1))
                                oGridN.doSetFieldValue("r.U_IDHSEQ", j + 1, j + 1)
                                oGridN.doSetFieldValue("r.U_IDHSEQ", j, j + 2)
                                j += 1
                            Else
                                oGridN.doSetFieldValue("r.U_IDHSEQ", j, j + 1)
                            End If
                        Else
                            oGridN.doSetFieldValue("r.U_IDHSEQ", j, GridRowsCount)
                        End If
                    Next
                    'End If
                    'Next
                    'End If
                    ' Next
                    doSetUpdate(oForm)
                    oForm.Items.Item("1").Click()
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error updating the rout's custom sequence")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXURCS", {Nothing})
            Finally
                oForm.Freeze(False)
                '   IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj1)
            End Try

        End Sub
        Protected Overrides Function doSaveOrderRows(ByVal oForm As SAPbouiCOM.Form) As Boolean
            'Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "JOBGRID")
            doWarnInActiveBPnSites(oForm, oGridN)
            Dim aRows As ArrayList = oGridN.doGetChangedRows()

            Dim sField As String = ""
            Dim oValue As Object = ""
            Dim sKey1 As String = ""
            'Dim sKey2 As String = ""

            Dim oAuditObj1 As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_JOBSHD")
            ' Dim oAuditObj2 As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_JOBENTR")
            Try
                '                Dim sSOStatus As String
                '                Dim sPOStatus As String
                Dim iResult As Integer
                Dim sResult As String = Nothing

                Dim aChangedFields As ArrayList
                If Not aRows Is Nothing Then
                    For iIndex As Integer = 0 To aRows.Count - 1
                        oGridN.setCurrentDataRowIndex(aRows(iIndex))
                        sKey1 = oGridN.doGetFieldValue("r.Code")
                        'sKey2 = oGridN.doGetFieldValue("r.U_JobNr")

                        If oAuditObj1.setKeyPair(sKey1, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then 'AndAlso _
                            '  oAuditObj2.setKeyPair(sKey2, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then
                            aChangedFields = oGridN.doGetChangedFields(aRows(iIndex))
                            For iField As Integer = 0 To aChangedFields.Count - 1
                                sField = aChangedFields.Item(iField)
                                oValue = oGridN.doGetFieldValue(sField)

                                If sField.StartsWith("r.") Then
                                    sField = sField.Substring(2)
                                    oAuditObj1.setFieldValue(sField, oValue)
                                    'ElseIf sField.StartsWith("e.") Then
                                    '    sField = sField.Substring(2)
                                    '    oAuditObj2.setFieldValue(sField, oValue)
                                End If

                                ''USA Release 
                                ''Additional Items Surcharge Logic 
                                ''Put on HOLD because the columns required in 
                                ''surcharge row calculations like Manifest Qty, UOM etc 
                                ''are missing in the OSM Grids 
                                ''Need to include all those columns on all OSM 1...6 
                                ''START 
                                'If sField.Equals("U_AEDate") AndAlso _
                                '    (Not oValue Is Nothing) AndAlso _
                                '    Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                                '    Dim sAEDate As String = oValue.ToString()
                                '    If sAEDate.Length > 0 Then
                                '        doAddSurcharges(oForm, oGridN)
                                '    Else
                                '        doRemovesurcharges(oGridN)
                                '    End If
                                'End If
                                ''END 
                            Next

                            iResult = oAuditObj1.Commit()
                            If iResult <> 0 Then
                                goParent.goDICompany.GetLastError(iResult, sResult)
                                'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error saving the Order rows: " + sResult, "Error saving the Order rows - " & sKey1)
                                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error saving the Order rows: " + sResult + "Key: " + sKey1, "ERSYSIRD", {sKey1, com.idh.bridge.Translation.getTranslatedWord(sResult)})
                            Else
                                '     oGridN.doDeletePBIDetails()
                            End If
                            'iResult = oAuditObj2.Commit()
                            'If iResult <> 0 Then
                            '    goParent.goDICompany.GetLastError(iResult, sResult)
                            '    com.idh.bridge.DataHandler.INSTANCE.doError("System: Error saving the Order rows: " + sResult, "Error saving the Order rows - " & sKey2)
                            'End If
                        End If
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error saving the Order rows - " & sKey1 & "." & sField & "." & oValue)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBS", _
                                                                        {com.idh.bridge.Translation.getTranslatedWord("Order Rows"), sKey1, sField, oValue})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj1)
            End Try
            Return MyBase.doSaveOrderRows(oForm)
            'Return True
        End Function
        Public Function doCalendarWeek(ByVal nWeek As Integer, ByVal nYear As Integer) As Date
            Try
                Dim dStart As New Date(nYear, 1, 4)
                Dim nDay As Integer = (dStart.DayOfWeek + 6) Mod 7 + 1

                Dim dFirst As Date = dStart.AddDays(1 - nDay)

                Return dFirst.AddDays((nWeek - 1) * 7)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error converting week to a date")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCVWD", {Nothing})
            End Try
            Return Nothing
        End Function

        Public Sub doSetFilterDates(ByVal oForm As SAPbouiCOM.Form, ByVal sWeek As String)
            Dim iYear As Integer
            Dim iWeek As Integer
            Dim dStart As Date
            Dim dEnd As Date
            iYear = Year(Now())
            Try
                iWeek = CInt(sWeek)
                dStart = doCalendarWeek(iWeek, iYear)
                dEnd = dStart.AddDays(6)
                setUFValue(oForm, "IDH_REQSTF", goParent.doDateToStr(dStart))
                setUFValue(oForm, "IDH_REQSTT", goParent.doDateToStr(dEnd))
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting Start and End Date")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSSED", {Nothing})
            End Try
        End Sub
        Public Function doPrepareDayFilter(ByVal oForm As SAPbouiCOM.Form) As String
            Dim sMON As String = getUFValue(oForm, "IDHMON")
            Dim sTUE As String = getUFValue(oForm, "IDHTUE")
            Dim sWED As String = getUFValue(oForm, "IDHWED")
            Dim sTHU As String = getUFValue(oForm, "IDHTHU")
            Dim sFRI As String = getUFValue(oForm, "IDHFRI")
            Dim sSAT As String = getUFValue(oForm, "IDHSAT")
            Dim sSUN As String = getUFValue(oForm, "IDHSUN")

            Dim sFilter As String = "*"
            If sMON.Length <> 0 Then
                If sMON = "Y" Then
                    sFilter = sFilter + "M*"
                End If
            End If

            If sTUE.Length <> 0 Then
                If sTUE = "Y" Then
                    sFilter = sFilter + "Tu*"
                End If
            End If

            If sWED.Length <> 0 Then
                If sWED = "Y" Then
                    sFilter = sFilter + "W*"
                End If
            End If

            If sTHU.Length <> 0 Then
                If sTHU = "Y" Then
                    sFilter = sFilter + "Th*"
                End If
            End If

            If sFRI.Length <> 0 Then
                If sFRI = "Y" Then
                    sFilter = sFilter + "F*"
                End If
            End If

            If sSAT.Length <> 0 Then
                If sSAT = "Y" Then
                    sFilter = sFilter + "Sa*"
                End If
            End If

            If sSUN.Length <> 0 Then
                If sSUN = "Y" Then
                    sFilter = sFilter + "Su"
                End If
            End If
            If sFilter.Length <> 0 Then
                setUFValue(oForm, "IDH_WDAY", sFilter)
            Else
                setUFValue(oForm, "IDH_WDAY", "")
            End If
            Return sFilter
        End Function
    End Class
End Namespace
