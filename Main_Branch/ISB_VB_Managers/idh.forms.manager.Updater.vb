Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System.Net
Imports System.Web

Imports com.idh.dbObjects.User
Imports com.idh.bridge.lookups
Imports com.idh.bridge.resources
Imports WR1_Grids.idh.controls.grid
Imports SAPbouiCOM

Namespace idh.forms.manager
    Public Class Updater
        Inherits idh.forms.manager.Order

        ' Shared moTotalFields_Charge() As String = { _
        '    "IDH_SubAD", "IDH_TaxAmt", "IDH_Total", "IDH_DisAmt"
        '}
        ' Shared moTotalFields_Cost() As String = { _
        '    "IDH_AddEx"
        '}
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, iMenuPosition, "IDHUPDTR", "JobUpdater.srf", "WO updater")
        End Sub

        Public Overrides Function getListRequiredStr(OForm As SAPbouiCOM.Form) As String
            ''MA Issue#1159 Start 09-06-2016
            'Return "r.U_JobNr = e.Code And v.Code = r.Code And e.U_Status In ('1','2','3','4','5') AND " & _
            ' " r.U_Status NOT IN ('" & com.idh.bridge.lookups.FixedValues.getStatusOrdered() & "', '" & com.idh.bridge.lookups.FixedValues.getStatusInvoiced() & "') AND " & _
            '    " r.U_PStat Not Like '" & com.idh.bridge.lookups.FixedValues.getStatusOrdered() & "%' AND bp.CardCode = r.U_CustCd "
            Return "r.U_JobNr = e.Code And v.Code = r.Code And " &
                       " bp.CardCode = r.U_CustCd "
            ''MA End 01-03-2017
            ''MA Issue#1159 End 09-06-2016
        End Function

        Protected Overrides Sub doSetGridFilters(ByVal oGridN As FilterGrid)
            oGridN.doAddFilterField("IDH_STATUS", "r.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PSTAT", "r.U_PStat", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PROG", "v.State", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_REQSTF", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_REQSTT", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_ACTSTF", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_ACTSTT", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_CUST", "e.U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_NAME", "e.U_CardNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PROD", "r.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ITMGRP", "r.U_ItmGrp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_JOBTYP", "r.U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            'oGridN.doAddFilterField("IDH_ORDNO", "e.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ROWNO", "r.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ORDNO", "r.U_JobNr", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_VEHREG", "r.U_Lorry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DRIVER", "r.U_Driver", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ADDR", "e.U_Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_POSTCD", "e.U_ZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CUSPHO", "e.U_Phone1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SITETL", "e.U_SiteTl", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_REF", "r.U_CustRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WSCD", "r.U_WasCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WSDC", "r.U_WasDsc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            'Fix to avoid user ids with similar user code like davidM and davidP
            'oGridN.doAddFilterField("IDH_USER", "r.U_User", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_USER", "r.U_User", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)
            oGridN.doAddFilterField("IDH_BRANCH", "r.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)

            oGridN.doAddFilterField("IDH_STEID", "e.U_SteId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_STREET", "e.U_Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_CARCD", "r.U_CarrCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CARNM", "r.U_CarrNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_STATUS", "r.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PSTAT", "r.U_PStat", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_PROG", "v.State", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'KA -- START -- 20121022
            oGridN.doAddFilterField("IDH_BPSTAT", "bp.ValidFor", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 30)
            'KA -- END -- 20121022

            'New filters on Reference numbers and Maximo No on WOR 
            oGridN.doAddFilterField("IDH_PROREF", "r.U_ProRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DSPREF", "r.U_SiteRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SUPREF", "r.U_SupRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_MAXIMO", "r.U_MaximoNum", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            oGridN.doAddListField("v.State", "Progress", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("'" & IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\' + v.IMG", "", False, 10, "PICTURE", Nothing)
            oGridN.doAddListField("v.IMG", "", False, 10, "PICTURE", Nothing)
            oGridN.doAddListField("r.U_JobNr", "Order No.", False, 30, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Row No.", False, 30, Nothing, Nothing)
            oGridN.doAddListField("r.U_Comment", "Comment", False, 150, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobTp", "Order Type", False, 30, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 50, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_Address", "Order Address", False, 70, Nothing, Nothing)
            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_RDate", "Req. Start", False, 50, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASDate", "Act. Start", False, 50, Nothing, Nothing)
            oGridN.doAddListField("r.U_AUOM", "Alternative UOM", False, 20, Nothing, Nothing)
            oGridN.doAddListField("r.U_RdWgt", "Read Weight", False, 30, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Charge Weight", False, 20, Nothing, Nothing)
            oGridN.doAddListField("r.U_UOM", "Sales UOM", False, 20, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "T-Unit Charge", False, 30, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "Disposal Charge", False, 30, Nothing, Nothing)
            oGridN.doAddListField("r.U_CusQty", "Haulage Qty", False, 20, Nothing, Nothing)
            oGridN.doAddListField("r.U_CusChr", "H-Unit Charge", False, 30, Nothing, Nothing)
            oGridN.doAddListField("r.U_Price", "Haulage Total", False, 30, Nothing, Nothing)
            oGridN.doAddListField("(U_TCTotal + U_Price)", "Before Discount", False, 30, Nothing, "SubBefDisc")
            oGridN.doAddListField("r.U_TAddChrg", "Additional Expenses", False, 30, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Total Order Charge", False, 30, Nothing, Nothing)
            oGridN.doAddListField("e.U_City", "City", False, 30, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, 30, Nothing, Nothing)

            oGridN.doAddListField("r.U_PUOM", "Purchase UOM", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Lorry", "Vehicle Reg.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_LorryCd", "Veh Reg. Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTime", "From", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act. End", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AETime", "Act ETime", False, 0, "TIME", Nothing)
            oGridN.doAddListField("e.U_Street", "Street", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Block", "Block", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Phone1", "Main No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_SiteTl", "Site Tel No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_SteId", "Site Id", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("CAST(((U_TCTotal + U_Price) - r.U_DisAmt) As Numeric(19,2))", "Sub After Discount", False, 0, Nothing, "SubAftDisc")
            'oGridN.doAddListField("(U_TCTotal +(U_CusQty * U_CusChr)) - r.U_DisAmt", "Sub After Discount", False, -1, Nothing, "SubBefDisc")
            oGridN.doAddListField("r.U_SLicCh", "Skip Lic Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CarrCd", "Carrier Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_Tip", "Disposal Site", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_JCost", "Total Order Cost", False, 0, Nothing, "TOrdCost")
            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("r.U_Comment", "Comment", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASTime", "Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_RTimeT", "Req. Time To", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_VehTyp", "Vehicle", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Route", "Route", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Seq", "Sequence", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_Status", "Sales Status", False, -1, Nothing, Nothing) 'test
            oGridN.doAddListField("r.U_PStat", "Purchase Status", False, -1, Nothing, Nothing) 'test
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItmGrp", "Container Grp", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("r.U_Serial", "Item Serial No.", False, 0, Nothing, Nothing)
            '..
            '..
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)
            '..
            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "TipCost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "Disposal Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CarrNm", "Carrier Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Covera", "Coverage.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WROrd", "WO No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WRRow", "WO Row No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CoverHst", "Coverage Hist.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtRt", "Tipping Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtRt", "Haulage Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtRt", "Tipping Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtRt", "Supplier Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtRt", "Haulage Vat Purchase Rate", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtGrp", "Tipping Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtGrp", "Haulage Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtGrp", "Tipping Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtGrp", "Supplier Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtGrp", "Haulage Vat Purchase Group", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)

        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)

            Dim oItem As SAPbouiCOM.Item
            oItem = doAddUFCheck(oForm, "IDH_PRCFRC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
            oItem.AffectsFormMode = False
            setUFValue(oForm, "IDH_PRCFRC", "N")
            '#MA 20170309 start
            'Add exclude PO filter for excluding Purchase orders
            oItem = doAddUFCheck(oForm, "IDH_CHKPO", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
            oItem.AffectsFormMode = False
            'Add exclude SO filter for excluding Saler orders
            oItem = doAddUFCheck(oForm, "IDH_CHKSO", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
            oItem.AffectsFormMode = False
            '#MA 20170309 End

        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_UPDT" Then
                        Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                        Dim oSelectedRows As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows
                        If oSelectedRows Is Nothing OrElse oSelectedRows.Count = 0 Then
                            'com.idh.bridge.DataHandler.INSTANCE.doError("No rows selected.")
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("No rows selected.", "ERUSROWS", {Nothing})
                            Return False
                        End If

                        Dim iSelected As Integer = oSelectedRows.Count
                        Dim iRowNum As Integer
                        Dim sRow As String = ""

                        Dim saCodes(iSelected - 1) As String
                        If iSelected > 0 Then
                            For iIndex As Integer = 0 To iSelected - 1
                                iRowNum = oGridN.getDataTableRowIndex(oSelectedRows, iIndex)

                                saCodes(iIndex) = oGridN.doGetFieldValue("r.Code", iRowNum)
                            Next

                            If getUFValue(oForm, "IDH_PRCFRC") = "Y" Then
                                IDH_JOBSHD.doRefreshAllRowPrices(True, saCodes)
                            Else
                                IDH_JOBSHD.doRefreshAllRowPrices(False, saCodes)
                            End If

                            doWarnInActiveBPnSites(oForm, oGridN)
                            doOKMess("The Rows have been updated.")
                            doReLoadData(oForm, True)
                        End If
                    End If
                End If
            End If
            Return False
        End Function
        Protected Overrides Sub doWarnInActiveBPnSites(ByVal oForm As SAPbouiCOM.Form, oGridN As IDHGrid)
            Try
                If Config.ParameterAsBool("SHWINACW", False) = False Then
                    Exit Sub
                End If
                Dim sValue As String = ""

                Dim sBPCodes As String() '= {""}
                Dim sAddresses As String() '= {""}

                If oForm.TypeEx.IndexOf("IDHUPDTR") > -1 Then
                    sBPCodes = {"Customer", "e.U_CardCd", "Carrier", "r.U_CarrCd", "Disposal Site", "r.U_Tip",
                                                  "Site Lic Supplier", "r.U_SLicSp"}

                    sAddresses = {"Order Address", "e.U_CardCd", "e.U_Address", "Producer Address", "e.U_PCardCd", "e.U_PAddress"}
                Else
                    Exit Sub
                End If
                Dim sLabel, sBPField, sBPCode, sAddressField, sBPColName As String
                Dim oBPParam() As String = {"", ""}
                Dim oParamAddress() As String = {"", "", ""}
                Dim oSelectedRows As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows
                Dim iSelected As Integer = oSelectedRows.Count
                Dim iRowNum As Integer

                For iIndex As Integer = 0 To iSelected - 1
                    iRowNum = oGridN.getDataTableRowIndex(oSelectedRows, iIndex)
                    For iitem As Int32 = 0 To sBPCodes.Count - 1 Step 2
                        sLabel = sBPCodes(iitem)
                        sBPField = sBPCodes(iitem + 1)
                        sValue = oGridN.doGetFieldValue(sBPField, iRowNum)
                        If sValue IsNot Nothing AndAlso sValue.Trim <> "" AndAlso Not Config.INSTANCE.doCheckBPActive(sValue, DateTime.Now) Then
                            oBPParam(0) = com.idh.bridge.Translation.getTranslatedWord(sLabel)
                            oBPParam(1) = sValue
                            doWarnMess(Messages.getGMessage("WRINACBP", oBPParam), SAPbouiCOM.BoMessageTime.bmt_Short)
                        End If
                    Next
                    For iitem As Int32 = 0 To sAddresses.Count - 1 Step 3
                        sLabel = sAddresses(iitem)
                        sBPField = sAddresses(iitem + 1)
                        sAddressField = sAddresses(iitem + 2)

                        sValue = oGridN.doGetFieldValue(sAddressField, iRowNum)
                        sBPCode = oGridN.doGetFieldValue(sBPField, iRowNum)
                        sBPColName = oGridN.Columns.Item(oGridN.doFieldIndex(sBPField)).TitleObject.Caption
                        If sValue IsNot Nothing AndAlso sValue.Trim <> "" AndAlso Not Config.INSTANCE.doCheckBPAddressActive(sBPCode, sValue) Then
                            oParamAddress(0) = com.idh.bridge.Translation.getTranslatedWord(sLabel)
                            oParamAddress(1) = sValue
                            oParamAddress(2) = sBPColName
                            doWarnMess(Messages.getGMessage("WRINACAD", oParamAddress), SAPbouiCOM.BoMessageTime.bmt_Short)
                        End If
                    Next
                Next
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "doWarnInActiveBPnSites", {Nothing})
            End Try
        End Sub
        ' ''MA Start 05-11-2014 Issue#417 Issue#418
        'Public Overrides Sub doHideTotals(ByVal oForm As SAPbouiCOM.Form)
        '    ''MA Start 03-11-2014 Issue#417 Issue#418
        '    If (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) = False) OrElse (Config.INSTANCE.getParameterAsBool("HDTOTALS", False) = True) Then
        '        Dim oItem As SAPbouiCOM.Item
        '        Dim iBlankColor As Integer = 14930874
        '        For Each sItem As String In moTotalFields_Cost
        '            oItem = oForm.Items.Item(sItem)
        '            oItem.ForeColor = iBlankColor
        '            oItem.BackColor = iBlankColor
        '        Next
        '    End If
        '    If (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) = False) OrElse (Config.INSTANCE.getParameterAsBool("HDTOTALS", False) = True) Then
        '        Dim oItem As SAPbouiCOM.Item
        '        Dim iBlankColor As Integer = 14930874
        '        For Each sItem As String In moTotalFields_Charge
        '            oItem = oForm.Items.Item(sItem)
        '            oItem.ForeColor = iBlankColor
        '            oItem.BackColor = iBlankColor
        '        Next
        '    End If
        'End Sub
        ' ''MA End 05-11-2014 Issue#417 Issue#418
        Protected Overrides Sub doItemGroupCombo(ByRef oForm As SAPbouiCOM.Form)
        End Sub
        Public Overrides Sub doBeforeLoadData(oForm As SAPbouiCOM.Form)

            MyBase.doBeforeLoadData(oForm)
            setUFValue(oForm, "IDH_HDSTA", "")
        End Sub
        Protected Overrides Sub doSetFormSpecificFilters(oGridN As OrderRowGrid)
            Dim oForm As SAPbouiCOM.Form = oGridN.getSBOForm()
            '#MA 20170309 start
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_CHKPO") 'Exclude POs
            Dim chkPO As SAPbouiCOM.CheckBox = oItem.Specific
            oItem = oForm.Items.Item("IDH_CHKSO") 'Exclude SOs
            Dim chkSO As SAPbouiCOM.CheckBox = oItem.Specific
            Dim sReqFilter As String = oGridN.getRequiredFilter()
            If Not sReqFilter = "" AndAlso Not sReqFilter = Nothing Then
                If chkPO IsNot Nothing AndAlso chkPO.Checked And (chkSO IsNot Nothing AndAlso Not chkSO.Checked) Then ''Exclude POs Condition
                    sReqFilter = sReqFilter + "AND r.U_PStat NOT Like '" & com.idh.bridge.lookups.FixedValues.getStatusOrdered() & "%' "
                ElseIf chkSO IsNot Nothing AndAlso chkSO.Checked And (chkPO IsNot Nothing AndAlso Not chkPO.Checked) Then 'Exclude SOs Condition
                    sReqFilter = sReqFilter + "AND r.U_Status NOT IN ('" & com.idh.bridge.lookups.FixedValues.getStatusOrdered() & "', '" & com.idh.bridge.lookups.FixedValues.getStatusInvoiced() & "')"
                ElseIf (chkPO IsNot Nothing AndAlso chkPO.Checked) AndAlso (chkSO IsNot Nothing AndAlso chkSO.Checked) Then 'Exclude Both
                    sReqFilter = sReqFilter + "AND r.U_PStat NOT Like '" & com.idh.bridge.lookups.FixedValues.getStatusOrdered() & "%' " &
                        "AND r.U_Status NOT IN ('" & com.idh.bridge.lookups.FixedValues.getStatusOrdered() & "', '" & com.idh.bridge.lookups.FixedValues.getStatusInvoiced() & "')"
                End If
                oGridN.setRequiredFilter(sReqFilter)
            End If
            ''#MA 20170309 end
            MyBase.doSetFormSpecificFilters(oGridN)
        End Sub
        Public Overrides Function doCustomItemEvent(oForm As Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then '#MA 20170516
                Return False
            End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function
        Public Overrides Function doRightClickEvent(oForm As Form, ByRef pVal As ContextMenuInfo, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = BoEventTypes.et_RIGHT_CLICK Then '#MA 20170516
                Return False
            End If
            Return MyBase.doRightClickEvent(oForm, pVal, BubbleEvent)
        End Function
    End Class
End Namespace

