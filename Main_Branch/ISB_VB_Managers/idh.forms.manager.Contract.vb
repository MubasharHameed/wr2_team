﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge
Imports com.idh.bridge.data

Namespace idh.forms.manager
    Public Class Contract
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHCOMAN", "IDHPS", iMenuPosition, "ContractManager.srf", False, True, False, "Waste Contract Manager")
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                Dim oGridN As FilterGrid = New FilterGrid(Me, oForm, "LINESGRID", 7, 55, 705, 322, True)
                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False
                oForm.SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Find
                'oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", Nothing)
            End Try
        End Sub

        Protected Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_BOOSTF", "c.U_StarDat", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_BOOSTT", "c.U_EndDat", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_CUST", "c.U_Cust", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 30)
            oGridN.doAddFilterField("IDH_NAME", "c.U_CustNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 30)
            oGridN.doAddFilterField("IDH_CONNO", "c.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30)
            oGridN.doAddFilterField("IDH_STATUS", "c.U_Stat", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30)
            oGridN.doAddFilterField("IDH_ADDRES", "c.U_Addres", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 30)
            oGridN.doAddFilterField("IDH_STREET", "c.U_Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 30)
            oGridN.doAddFilterField("IDH_CITY", "c.U_City", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 30)
            oGridN.doAddFilterField("IDH_ZIPCD", "c.U_Zip", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 30)
        End Sub

        Protected Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("c.Code", "Contract", False, -1, Nothing, Nothing)
            oGridN.doAddListField("c.U_Cust", "Customer Code", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("c.U_CustNm", "Customer Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("c.U_Stat", "Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("c.U_StarDat", "Start Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("c.U_EndDat", "End Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("c.U_Addres", "Address Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("c.U_Street", "Street", False, -1, Nothing, Nothing)
            oGridN.doAddListField("c.U_City", "City", False, -1, Nothing, Nothing)
            oGridN.doAddListField("c.U_Zip", "Post Code", False, -1, Nothing, Nothing)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "LINESGRID", True)
                End If

                'oGridN.setTableValue("[@IDH_WASTCONT] c, [@IDH_JOBENTR] j")
                'oGridN.setKeyField("c.Code")
                oGridN.doAddGridTable(New GridTable("@IDH_WASTCONT", "c", "Code", True), True)
                '## Start 26-09-2013; commented, as per Haydan we are not using it
                '
                'oGridN.doAddGridTable( New GridTable( "@IDH_JOBENTR", "j"))

                'oGridN.setRequiredFilter("c.Code = j.U_CntrNo ")
                '## End
                oGridN.setInitialFilterValue("c.Code = 'ZZZ'")

                doSetFilterFields(oGridN)
                doSetListFields(oGridN)

                oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error preloading data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBPL", Nothing)
            End Try
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                oGridN.doReloadData()
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Loading Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLDBD", Nothing)
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            doLoadData(oForm)
            BubbleEvent = False
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "LINESGRID" Then
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = False Then
                        doGridDoubleClick(oForm, pVal, BubbleEvent)
                    End If
                End If
            End If
            Return False
        End Function

        Protected Overridable Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            'Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                oGridN.setCurrentLineByClick(pVal.Row)

                Dim sContract As String = oGridN.doGetFieldValue("c.Code")
                setSharedData(oForm, "IDH_CONTRN", sContract)
                goParent.doOpenModalForm("IDH_WASCON", oForm)
            End If
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing Modal results.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", Nothing)
            End Try
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
        End Sub


        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
