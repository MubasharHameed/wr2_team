'
' Created by SharpDevelop.
' User: Tjaard
' Date: 07/05/2009
' Time: 08:21 AM
'
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.idh.bridge.data
Imports com.idh.utils

Imports com.idh.bridge.lookups
Imports SAPbouiCOM

Namespace idh.forms.manager

    Public Class PreBook
        Inherits IDHAddOns.idh.forms.Base
        Private msRequiredFilter As String = IDH_PBI._IDHCCODE & " = " & BPAddress._CardCode &
                        " AND " & BPAddress._AddressType & " = 'S'" &
                        " AND (" & IDH_PBI._IDHSADDR & " = " & BPAddress._Address & " Or " _
                        & " ( IsNull(" & IDH_PBI._IDHSADLN & ",'') = " & BPAddress._AddrsCd & " ) " _
                        & " )"
        Shared sWeekDays As String() = {"IDH_MON", "IDH_TUE", "IDH_WED", "IDH_THU", "IDH_FRI", "IDH_SAT", "IDH_SUN"}
#Region "Contructor"

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_PBM", "IDHPS", iMenuPosition, "frmPreBookManager.srf", False, True, False, "PreBook Instruction Manager", load_Types.idh_LOAD_NORMAL)

            ''Add the item to the list of items to get their filepaths fixed.
            'doAddImagesToFix("BPCFL")
            'doAddImagesToFix("ADDCFL")
            'doAddImagesToFix("IDHCRCFL")
            'doAddImagesToFix("IDHDSCFL")

        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            'Dim oEdit As SAPbouiCOM.EditText = Nothing

            Try
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                If oGridN Is Nothing Then
                    oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
                End If
                'doSetFilterFields(oGridN)
                'doSetListFields(oGridN)

                '                Dim oChk As SAPbouiCOM.CheckBox
                '                
                '            	oChk = oForm.Items.Item("IDH_MON").Specific
                '	            oChk.ValOff = ""
                '                oChk.ValOn = "Y"
                '                
                '            	oChk = oForm.Items.Item("IDH_TUE").Specific
                '	            oChk.ValOff = ""
                '                oChk.ValOn = "Y"
                '                
                '            	oChk = oForm.Items.Item("IDH_WED").Specific
                '	            oChk.ValOff = ""
                '                oChk.ValOn = "Y"
                '                
                '            	oChk = oForm.Items.Item("IDH_THU").Specific
                '	            oChk.ValOff = ""
                '                oChk.ValOn = "Y"
                '                
                '            	oChk = oForm.Items.Item("IDH_FRI").Specific
                '	            oChk.ValOff = ""
                '                oChk.ValOn = "Y"
                '                
                '            	oChk = oForm.Items.Item("IDH_SAT").Specific
                '	            oChk.ValOff = ""
                '                oChk.ValOn = "Y"
                '                
                '            	oChk = oForm.Items.Item("IDH_SUN").Specific
                '	            oChk.ValOff = ""
                '                oChk.ValOn = "Y"

                Dim oItem As SAPbouiCOM.Item

                oItem = doAddUFCheck(oForm, "IDH_DOREAL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, True, False, "Y", "N")
                'oItem.AffectsFormMode = False
                'oItem.Visible = False
                setUFValue(oForm, "IDH_DOREAL", "Y")

                oItem = doAddUFCheck(oForm, "IDH_SHWEND", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                'oItem.AffectsFormMode = False
                'oItem.Visible = True
                setUFValue(oForm, "IDH_SHWEND", "N")

                oItem = doAddUF(oForm, "IDH_ACDATE", SAPbouiCOM.BoDataType.dt_DATE, 10)
                oItem.AffectsFormMode = False
                oItem.Visible = False

                oForm.Items.Item("2").Visible = False

                oGridN.getSBOItem().AffectsFormMode = True

                oForm.AutoManaged = True
                oForm.SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Find Or SAPbouiCOM.BoAutoFormMode.afm_Add
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            Finally
                'IDHAddOns.idh.data.Base.doReleaseObject(oEdit)
            End Try
        End Sub

        Protected Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            Try
                '        		oForm.Mode = iMode

                '                Dim sFormType As String = getParentSharedData(oForm, "FTYPE")
                '                Dim bIsSearch As Boolean = False
                '                If Not sFormType Is Nothing AndAlso sFormType.Equals("SEARCH") = True Then
                '                    bIsSearch = True
                '                End If
                '
                If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find")
                    '                    If bIsSearch Then
                    '                        oForm.Items.Item("2").Visible = True
                    '                        oForm.Items.Item("IDH_CHOOSE").Visible = True
                    '                    Else
                    '                        oForm.Items.Item("2").Visible = False
                    '                        oForm.Items.Item("IDH_CHOOSE").Visible = False
                    '                    End If
                ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update")
                    '                    oForm.Items.Item("2").Visible = True
                    '                    oForm.Items.Item("IDH_CHOOSE").Visible = False
                End If
                'oForm.Mode = iMode
            Catch ex As Exception
            End Try
        End Sub

        Protected Overridable Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_PBINUM", IDH_PBI._Code, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 15)

            oGridN.doAddFilterField("e_CardCode", IDH_PBI._IDHCCODE, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50)
            oGridN.doAddFilterField("e_CardName", IDH_PBI._IDHCNAME, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 150)
            oGridN.doAddFilterField("e_Addr", IDH_PBI._IDHSADDR, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 150)

            oGridN.doAddFilterField("IDH_POSTCD", BPAddress._ZipCode, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 15)
            oGridN.doAddFilterField("IDH_JOBTP", IDH_PBI._IDHJTYPE, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 20)
            oGridN.doAddFilterField("IDH_CONCD", IDH_PBI._IDHCOICD, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 20)
            oGridN.doAddFilterField("IDH_WSCD", IDH_PBI._IDHWCICD, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 50)

            oGridN.doAddFilterField("IDH_MON", IDH_PBI._IDHRDMON2, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1)
            oGridN.doAddFilterField("IDH_TUE", IDH_PBI._IDHRDTUE2, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1)
            oGridN.doAddFilterField("IDH_WED", IDH_PBI._IDHRDWED2, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1)
            oGridN.doAddFilterField("IDH_THU", IDH_PBI._IDHRDTHU2, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1)
            oGridN.doAddFilterField("IDH_FRI", IDH_PBI._IDHRDFRI2, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1)
            oGridN.doAddFilterField("IDH_SAT", IDH_PBI._IDHRDSAT2, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1)
            oGridN.doAddFilterField("IDH_SUN", IDH_PBI._IDHRDSUN2, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1)

            oGridN.doAddFilterField("IDHCARCD", IDH_PBI._IDHCARCD, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50)
            oGridN.doAddFilterField("IDHCARDC", IDH_PBI._IDHCARDC, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 150)

            oGridN.doAddFilterField("IDHDISCD", IDH_PBI._IDHDISCD, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50)
            oGridN.doAddFilterField("IDHDISDC", IDH_PBI._IDHDISDC, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 150)

            'oGridN.doAddFilterField("IDH_WSCD", IDH_PBI._IDHWCICD, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 20)
            doAddUF(oGridN.getSBOForm(), "IDH_RTN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50).AffectsFormMode = False

            Dim oChk As SAPbouiCOM.CheckBox
            oChk = oGridN.getSBOForm().Items.Item("IDH_MON").Specific
            oChk.ValOff = ""
            oChk.ValOn = "Y"

            oChk = oGridN.getSBOForm().Items.Item("IDH_TUE").Specific
            oChk.ValOff = ""
            oChk.ValOn = "Y"

            oChk = oGridN.getSBOForm().Items.Item("IDH_WED").Specific
            oChk.ValOff = ""
            oChk.ValOn = "Y"

            oChk = oGridN.getSBOForm().Items.Item("IDH_THU").Specific
            oChk.ValOff = ""
            oChk.ValOn = "Y"

            oChk = oGridN.getSBOForm().Items.Item("IDH_FRI").Specific
            oChk.ValOff = ""
            oChk.ValOn = "Y"

            oChk = oGridN.getSBOForm().Items.Item("IDH_SAT").Specific
            oChk.ValOff = ""
            oChk.ValOn = "Y"

            oChk = oGridN.getSBOForm().Items.Item("IDH_SUN").Specific
            oChk.ValOff = ""
            oChk.ValOn = "Y"

        End Sub

        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            'Convert.ToInt32(sWrkString, 16);
            Dim iDAY As Integer = &HBAF3C7
            Dim iROUTE As Integer = &HF3BAD9
            Dim iSEQ As Integer = &HBACDF3

            oGridN.doAddListField(IDH_PBI._Code, "PBI Number", False, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_PBI._IDHCCODE, "Customer Code", False, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_PBI._IDHCNAME, "Customer Name", False, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_PBI._IDHSADDR, "Site Address", False, 20, Nothing, Nothing)
            oGridN.doAddListField(BPAddress._ZipCode, "Postcode", False, 15, Nothing, Nothing)
            '#MA Start 17-05-2017  issue#402 point 1 & 2
            'oGridN.doAddListField("ad.U_LONG", "Longitude", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("ad.U_LAT", "Latitude", False, -1, Nothing, Nothing)
            oGridN.doAddListField(IDH_PBI._Longitude, "Longitude", False, -1, Nothing, Nothing)
            oGridN.doAddListField(IDH_PBI._Latitude, "Latitude", False, -1, Nothing, Nothing)
            ''#MA End 17-05-2017
            oGridN.doAddListField(IDH_PBI._IDHRDMON2, "M", True, 1, "CHECKBOX", Nothing, iDAY)
            '"SRC:IDHALTWISRC(AWST)[IDH_ALITM=" + IDH_BPFORECST._AltWasDsc + ";IDH_ITMCOD=#" + IDH_BPFORECST._ItemCd + ";IDH_ITMNAM=#" + IDH_BPFORECST._ItemDsc + "][ItemAltName;" + IDH_BPFORECST._AltWDSCd + "=ItemAltCode]"
            oGridN.doAddListField(IDH_PBI._IDHRCDM, "R", True, 3, "SRC:IDHROSRC(RTCODEMO)[IDH_RTCODE;IDH_RTWDY=*M*][IDH_RTCODE]", Nothing, iROUTE)
            'oGridN.doAddListField(IDH_PBI._IDHRCDM, "R", True, 3, Nothing, Nothing, iROUTE)
            oGridN.doAddListField(IDH_PBI._IDHRSQM, "S", True, 3, Nothing, Nothing, iSEQ)

            oGridN.doAddListField(IDH_PBI._IDHRDTUE2, "T", True, 1, "CHECKBOX", Nothing, iDAY)
            oGridN.doAddListField(IDH_PBI._IDHRCDTU, "R", True, 3, "SRC:IDHROSRC(RTCODETU)[IDH_RTCODE;IDH_RTWDY=*Tu*][IDH_RTCODE]", Nothing, iROUTE)
            'oGridN.doAddListField(IDH_PBI._IDHRCDTU, "R", True, 3, Nothing, Nothing, iROUTE)
            oGridN.doAddListField(IDH_PBI._IDHRSQTU, "S", True, 3, Nothing, Nothing, iSEQ)

            oGridN.doAddListField(IDH_PBI._IDHRDWED2, "W", True, 1, "CHECKBOX", Nothing, iDAY)
            oGridN.doAddListField(IDH_PBI._IDHRCDW, "R", True, 3, "SRC:IDHROSRC(RTCODEWE)[IDH_RTCODE;IDH_RTWDY=*W*][IDH_RTCODE]", Nothing, iROUTE)
            'oGridN.doAddListField(IDH_PBI._IDHRCDW, "R", True, 3, Nothing, Nothing, iROUTE)
            oGridN.doAddListField(IDH_PBI._IDHRSQW, "S", True, 3, Nothing, Nothing, iSEQ)

            oGridN.doAddListField(IDH_PBI._IDHRDTHU2, "T", True, 1, "CHECKBOX", Nothing, iDAY)
            oGridN.doAddListField(IDH_PBI._IDHRCDTH, "R", True, 3, "SRC:IDHROSRC(RTCODETR)[IDH_RTCODE;IDH_RTWDY=*Th*][IDH_RTCODE]", Nothing, iROUTE)
            'oGridN.doAddListField(IDH_PBI._IDHRCDTH, "R", True, 3, Nothing, Nothing, iROUTE)
            oGridN.doAddListField(IDH_PBI._IDHRSQTH, "S", True, 3, Nothing, Nothing, iSEQ)

            oGridN.doAddListField(IDH_PBI._IDHRDFRI2, "F", True, 1, "CHECKBOX", Nothing, iDAY)
            oGridN.doAddListField(IDH_PBI._IDHRCDF, "R", True, 3, "SRC:IDHROSRC(RTCODEFR)[IDH_RTCODE;IDH_RTWDY=*F*][IDH_RTCODE]", Nothing, iROUTE)
            'oGridN.doAddListField(IDH_PBI._IDHRCDF, "R", True, 3, Nothing, Nothing, iROUTE)
            oGridN.doAddListField(IDH_PBI._IDHRSQF, "S", True, 3, Nothing, Nothing, iSEQ)

            oGridN.doAddListField(IDH_PBI._IDHRDSAT2, "S", True, 1, "CHECKBOX", Nothing, iDAY)
            oGridN.doAddListField(IDH_PBI._IDHRCDSA, "R", True, 3, "SRC:IDHROSRC(RTCODESA)[IDH_RTCODE;IDH_RTWDY=*Sa*][IDH_RTCODE]", Nothing, iROUTE)
            'oGridN.doAddListField(IDH_PBI._IDHRCDSA, "R", True, 3, Nothing, Nothing, iROUTE)
            oGridN.doAddListField(IDH_PBI._IDHRSQSA, "S", True, 3, Nothing, Nothing, iSEQ)

            oGridN.doAddListField(IDH_PBI._IDHRDSUN2, "S", True, 1, "CHECKBOX", Nothing, iDAY)
            oGridN.doAddListField(IDH_PBI._IDHRCDSU, "R", True, 3, "SRC:IDHROSRC(RTCODESU)[IDH_RTCODE;IDH_RTWDY=*Su*][IDH_RTCODE]", Nothing, iROUTE)
            'oGridN.doAddListField(IDH_PBI._IDHRCDSU, "R", True, 3, Nothing, Nothing, iROUTE)
            oGridN.doAddListField(IDH_PBI._IDHRSQSU, "S", True, 3, Nothing, Nothing, iSEQ)

            oGridN.doAddListField(IDH_PBI._IDHJTYPE, "Job Type", False, 50, Nothing, Nothing)

            oGridN.doAddListField(IDH_PBI._IDHWCICD, "Waste Code", False, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_PBI._IDHWCIDC, "Waste Description", False, 100, Nothing, Nothing)

            oGridN.doAddListField(IDH_PBI._IDHCOICD, "Container Code", False, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_PBI._IDHCOIDC, "Container Description", False, 100, Nothing, Nothing)

            oGridN.doAddListField(IDH_PBI._IDHCOIQT, "Qty", False, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_PBI._IDHRECOM, "Frequency", False, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_PBI._IDHWO, "Linked WO", False, 0, Nothing, Nothing)

            oGridN.doAddListField(IDH_PBI._IDHCARCD, "Carrier Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField(IDH_PBI._IDHCARDC, "Carrier Name", False, -1, Nothing, Nothing)

            oGridN.doAddListField(IDH_PBI._IDHDISCD, "Disposal Site", False, -1, Nothing, Nothing)
            oGridN.doAddListField(IDH_PBI._IDHDISDC, "Disposal Name", False, -1, Nothing, Nothing)
            Dim sRunDate As String = Dates.doSimpleSQLDateNoTime(DateTime.Now)
            Dim sIsClosedPBISQL As String = " (Case when  U_IDHRECEN = 2 AND convert(datetime,U_IDHRECED,126) < convert(datetime,'" & sRunDate & "',126) then 'Y' Else 'N' End)"
            oGridN.doAddListField(sIsClosedPBISQL, "Is Closed", False, -1, Nothing, Nothing)

        End Sub

#End Region

#Region "Form Controls and Loads"
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If
            oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
            oGridN.BlockPopupCopyPaste = True
            doSetFilterFields(oGridN)
            doSetListFields(oGridN)

            'oGridN.setTableValue("[@IDH_PBI]")
            'oGridN.setKeyField("Code")
            oGridN.doAddGridTable(New GridTable("@IDH_PBI", Nothing, "Code", True, True), True)
            oGridN.doAddGridTable(New GridTable("CRD1", "ad", BPAddress._Address, False, True))

            oGridN.doSetDoCount(True)
            oGridN.doSetBlankLine(False)

            Dim sCode As String = Nothing
            Dim sName As String = Nothing
            Dim sAddr As String = Nothing

            If getHasSharedData(oForm) Then
                sCode = getParentSharedData(oForm, "e_CardCode")
                sName = getParentSharedData(oForm, "e_CardName")

                setUFValue(oForm, "e_CardCode", sCode)
                setUFValue(oForm, "e_CardName", sName)
            End If
            oGridN.setInitialFilterValue("CardCode = '-100'")

            doFillCombo(oForm, "IDH_JOBTP", "[@IDH_JOBTYPE]", "U_JobTp", Nothing, "U_OrdCat = 'Waste Order'", Nothing, True)
        End Sub

        '** Set the Calulated required field 
        Public Overridable Function doCalcRequiredStr(ByVal oForm As SAPbouiCOM.Form) As String
            Dim sReqStr As String = ""
            Dim sRoute As String = getUFValue(oForm, "IDH_RTN", True)
            If sRoute.Length > 0 Then
                'MA  Issue 20170425
                Dim sArr As String() = sRoute.Split(",")
                Dim sRoutes As String = ""
                If sArr.Length > 0 Then
                    For indx As Integer = 0 To sArr.Length - 1
                        sRoutes = sRoutes & "'" & sArr.GetValue(indx) & "',"
                    Next indx
                End If
                If getUFValue(oForm, "IDH_MON", True) = "Y" Then
                    sReqStr = sReqStr & " AND " & IDH_PBI._IDHRCDM & " in (" & sRoutes.TrimEnd(",") & ")"
                End If
                If getUFValue(oForm, "IDH_TUE", True) = "Y" Then
                    sReqStr = sReqStr & " AND " & IDH_PBI._IDHRCDTU & " in (" & sRoutes.TrimEnd(",") & ")"
                End If
                If getUFValue(oForm, "IDH_WED", True) = "Y" Then
                    sReqStr = sReqStr & " AND " & IDH_PBI._IDHRCDW & " in (" & sRoutes.TrimEnd(",") & ")"
                End If
                If getUFValue(oForm, "IDH_THU", True) = "Y" Then
                    sReqStr = sReqStr & " AND " & IDH_PBI._IDHRCDTH & " in (" & sRoutes.TrimEnd(",") & ")"
                End If
                If getUFValue(oForm, "IDH_FRI", True) = "Y" Then
                    sReqStr = sReqStr & " AND " & IDH_PBI._IDHRCDF & " in (" & sRoutes.TrimEnd(",") & ")"
                End If
                If getUFValue(oForm, "IDH_SAT", True) = "Y" Then
                    sReqStr = sReqStr & " AND " & IDH_PBI._IDHRCDSA & " in (" & sRoutes.TrimEnd(",") & ")"
                End If
                If getUFValue(oForm, "IDH_SUN", True) = "Y" Then
                    sReqStr = sReqStr & " AND " & IDH_PBI._IDHRCDSU & " in (" & sRoutes.TrimEnd(",") & ")"
                End If

                If sReqStr.Length = 0 Then
                    sReqStr = sReqStr &
                        " AND (" & IDH_PBI._IDHRCDM & " in (" & sRoutes.TrimEnd(",") & ")" &
                        " 		OR " & IDH_PBI._IDHRCDM & " in (" & sRoutes.TrimEnd(",") & ")" &
                        " 		OR " & IDH_PBI._IDHRCDTU & " in (" & sRoutes.TrimEnd(",") & ")" &
                        " 		OR " & IDH_PBI._IDHRCDW & " in (" & sRoutes.TrimEnd(",") & ")" &
                        " 		OR " & IDH_PBI._IDHRCDTH & " in (" & sRoutes.TrimEnd(",") & ")" &
                        " 		OR " & IDH_PBI._IDHRCDF & " in (" & sRoutes.TrimEnd(",") & ")" &
                        " 		OR " & IDH_PBI._IDHRCDSA & " in (" & sRoutes.TrimEnd(",") & ")" &
                        " 		OR " & IDH_PBI._IDHRCDSU & " in (" & sRoutes.TrimEnd(",") & ") )"
                End If

            End If
            'MA  Issue 20170425 end

            sReqStr = msRequiredFilter & sReqStr

            If getUFValue(oForm, "IDH_SHWEND") = "N" Then
                Dim sRunDate As String = Dates.doSimpleSQLDateNoTime(DateTime.Now)
                ''MA Start 13-05-2016 Issue#1080
                'sReqStr = sReqStr & " AND (  U_IDHRECEN = 2 AND convert(datetime,U_IDHRECED,126) >= convert(datetime,'" & sRunDate & "',126) )"
                sReqStr = sReqStr & " AND( " _
                            & " ( (U_IDHRECEN = 0 Or  ( U_IDHRECEN = 2 And convert(datetime,U_IDHRECED,126) >= convert(datetime,'" & sRunDate & "',126) ) )" _
                            & " Or " _
                            & " (U_IDHRECEN = 0) ) " _
                            & " Or " _
                            & " (( U_IDHRECEN=1  And (U_IDHRECCE Is Not  Null) And  U_IDHRECCE>0 ) ) " _
                            & " )"
                ''MA End 13-05-2016 Issue#1080
            End If

            Return sReqStr
        End Function

        '** The Initializer
        Protected Overridable Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean)
            Try
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

                Dim oGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                'If bIsFirst = False Then
                '    oGrid.getGridControl().doCreateFilterString()
                '    If oGrid.doCheckForQueryChange() = False Then
                '        Return
                '    End If
                'End If
                oGrid.setRequiredFilter(doCalcRequiredStr(oForm))
                oGrid.doReloadData()
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error reloading the Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", {Nothing})
            End Try
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            doReLoadData(oForm, True)
            doEnableAction(oForm, False)

            '            Dim oGrid As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            ''            Dim sCode As String = getUFValue(oForm, "e_CardCode")
            ''            Dim sName As String = getUFValue(oForm, "e_CardName")
            ''            Dim sAddr As String = getUFValue(oForm, "e_Addr")
            ''
            ''            If sCode.Length > 0 Then
            ''                oGrid.setRequiredFilter("U_IDHCCODE = '" & sCode & "'")
            ''            End If
            ''            If sName.Length > 0 Then
            ''                oGrid.setRequiredFilter("U_IDHCNAME = '" & sName & "'")
            ''            End If
            ''            If sAddr.Length > 0 Then
            ''                oGrid.setRequiredFilter("U_IDHSADDR = '" & sAddr & "'")
            ''            End If
            '
            '            oGrid.doReloadData()
        End Sub

        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            oForm.Visible = True

            Dim oGrid As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim iRows As Integer = oGrid.getDataTable().Rows.Count
            Dim sVal As String = oGrid.doGetFieldValue(2)
            If goParent.doCheckModal(oForm.UniqueID) = True AndAlso
             iRows > 0 AndAlso
             sVal.Length = 0 AndAlso
             goParent.doGetParentForm(oForm).TypeEx.Equals("134") Then
                'doNewPBI(oForm)
                oGrid.doReloadData()

            End If
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doClose()
        End Sub
#End Region

#Region "Event Handlers"
        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID.Equals("BPCFL") Then
                        doChooseCustomer(oForm)
                    ElseIf pVal.ItemUID.Equals("ADDCFL") Then 'Site Address Choose from
                        doChooseSiteAd(oForm)
                    ElseIf pVal.ItemUID.Equals("IDHPO") Then
                        doReportByBtn(oForm, "IDHPO")
                    ElseIf pVal.ItemUID.Equals("IDHDOC") Then
                        doReportByBtn(oForm, "IDHDOC")
                    ElseIf pVal.ItemUID.Equals("IDHCUST") Then
                        doReportByBtn(oForm, "IDHCUST")
                    ElseIf pVal.ItemUID.Equals("IDHCRCFL") Then
                        doChooseCarrier(oForm)
                    ElseIf pVal.ItemUID.Equals("IDHDSCFL") Then
                        doChooseDisposal(oForm)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                Dim sItem As String = pVal.ItemUID
                If pVal.BeforeAction = False Then
                    If pVal.ItemChanged Then
                        If sItem.Equals("e_CardCode") OrElse sItem.Equals("e_CardName") Then
                            Dim sCardCode As String = getUFValue(oForm, "e_Cardcode", True)
                            Dim sCardName As String = getUFValue(oForm, "e_CardName", True)
                            If sCardCode.Length > 0 AndAlso (sCardCode.StartsWith("*") OrElse sCardCode.EndsWith("*")) Then
                                setUFValue(oForm, "e_CardName", "")
                                setUFValue(oForm, "e_Addr", "")

                                doChooseCustomer(oForm)
                            ElseIf sCardName.Length > 0 AndAlso (sCardName.StartsWith("*") OrElse sCardName.EndsWith("*")) Then
                                setUFValue(oForm, "e_CardCode", "")
                                setUFValue(oForm, "e_Addr", "")

                                doChooseCustomer(oForm)
                            End If
                        ElseIf sItem.Equals("e_Addr") Then
                            Dim sAddr As String = getUFValue(oForm, "e_Addr", True)
                            If sAddr.Length > 0 AndAlso (sAddr.StartsWith("*") OrElse sAddr.EndsWith("*")) Then
                                doChooseSiteAd(oForm)
                            End If
                        ElseIf sItem.Equals("IDHCARCD") OrElse sItem.Equals("IDHCARDC") Then
                            Dim sCarrierCode As String = getUFValue(oForm, "IDHCARCD", True)
                            Dim sCarrierName As String = getUFValue(oForm, "IDHCARDC", True)
                            If sCarrierCode.Length > 0 AndAlso (sCarrierCode.StartsWith("*") OrElse sCarrierCode.EndsWith("*")) Then
                                'setUFValue(oForm, "IDHCARDC", "")
                                'setUFValue(oForm, "IDHCARCD", "")

                                doChooseCarrier(oForm)
                            ElseIf sCarrierName.Length > 0 AndAlso (sCarrierName.StartsWith("*") OrElse sCarrierName.EndsWith("*")) Then
                                'setUFValue(oForm, "IDHCARDC", "")
                                'setUFValue(oForm, "IDHCARCD", "")
                                doChooseCarrier(oForm)
                            End If
                        ElseIf sItem.Equals("IDHDISCD") OrElse sItem.Equals("IDHDISDC") Then
                            Dim sDisSiteCode As String = getUFValue(oForm, "IDHDISCD", True)
                            Dim sDisSiteName As String = getUFValue(oForm, "IDHDISDC", True)
                            If sDisSiteCode.Length > 0 AndAlso (sDisSiteCode.StartsWith("*") OrElse sDisSiteCode.EndsWith("*")) Then
                                setUFValue(oForm, "IDHDISCD", "")
                                setUFValue(oForm, "IDHDISDC", "")

                                doChooseDisposal(oForm)
                            ElseIf sDisSiteName.Length > 0 AndAlso (sDisSiteName.StartsWith("*") OrElse sDisSiteName.EndsWith("*")) Then
                                setUFValue(oForm, "IDHDISCD", "")
                                setUFValue(oForm, "IDHDISDC", "")

                                doChooseDisposal(oForm)
                            End If
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "LINESGRID" Then
                        Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                        Dim sPBICode As String = Nothing
                        Dim iCurDataRow As Integer
                        iCurDataRow = oGridN.getSBOGrid.GetDataTableRowIndex(pVal.Row)
                        If iCurDataRow > -1 Then
                            oGridN.setCurrentDataRowIndex(iCurDataRow)
                            sPBICode = oGridN.doGetFieldValue("Code")
                        End If

                        setSharedData(oForm, "PBICode", sPBICode)
                        setSharedData(oForm, "TRG", "IDH_PBI")
                        goParent.doOpenModalForm("IDH_PBI", oForm)
                    End If
                End If
            End If

            Return False
        End Function

        'Closing a Modal Form Retrieving Data from the Shared Buffer
        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form,
         ByVal sModalFormType As String,
         Optional ByVal sLastButton As String = Nothing)
            Try

                If sModalFormType.Equals("IDH_COMMFRM") Then
                    Dim sExtendedComments As String = Config.INSTANCE.getParameterWithDefault("PBIDELCM", "PBI Deleted no longer available")
                    sExtendedComments = sExtendedComments + " - " + getSharedData(oForm, "CMMNT")
                    If doDeleteSelectedRows(oForm, sExtendedComments) Then
                        doReLoadData(oForm, True)
                    End If
                ElseIf (sModalFormType.Equals("IDHCSRCH") OrElse sModalFormType.Equals("IDHNCSRCH")) Then
                    Dim sTrg As String = getSharedData(oForm, "TRG")
                    If sTrg = "CUS" Then
                        setUFValue(oForm, "e_CardCode", getSharedData(oForm, "CARDCODE"), True)
                        setUFValue(oForm, "e_CardName", getSharedData(oForm, "CARDNAME"), True)
                    ElseIf sTrg = "CAR" Then
                        setUFValue(oForm, "IDHCARCD", getSharedData(oForm, "CARDCODE"), True)
                        setUFValue(oForm, "IDHCARDC", getSharedData(oForm, "CARDNAME"), True)
                    ElseIf sTrg = "DIS" Then
                        setUFValue(oForm, "IDHDISCD", getSharedData(oForm, "CARDCODE"), True)
                        setUFValue(oForm, "IDHDISDC", getSharedData(oForm, "CARDNAME"), True)
                    End If
                ElseIf sModalFormType.Equals("IDHASRCH") Then
                    Dim sCardCode As String = getSharedData(oForm, "CARDCODE")
                    setUFValue(oForm, "e_CardCode", sCardCode, True)

                    Dim sBPName As String = Config.INSTANCE.doGetBPName(sCardCode)
                    setUFValue(oForm, "e_CardName", sBPName, True)
                    setUFValue(oForm, "e_Addr", getSharedData(oForm, "ADDRESS"), True)
                    '                    doLoadData(oForm)
                ElseIf sModalFormType.Equals("IDH_PBI") Then
                    doLoadData(oForm)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal results - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHMR", {sModalFormType})
            End Try
        End Sub

        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects
        '        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
        '            Return MyBase.doCustomItemEvent(oForm, pVal)
        '        End Function

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                If pVal.BeforeAction = True Then
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, pVal.ItemUID)
                    Dim oMenuItem As SAPbouiCOM.MenuItem
                    Dim sRMenuId As String = IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU
                    oMenuItem = goParent.goApplication.Menus.Item(sRMenuId)

                    Dim oMenus As SAPbouiCOM.Menus
                    Dim iMenuPos As Integer = 1
                    Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                    oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
                    oCreationPackage.Enabled = True
                    oMenus = oMenuItem.SubMenus

                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING

                    oCreationPackage.UniqueID = "NEW"
                    oCreationPackage.String = getTranslatedWord("New &PBI")
                    oCreationPackage.Position = iMenuPos
                    iMenuPos += 1
                    oMenus.AddEx(oCreationPackage)

                    oCreationPackage.UniqueID = "DEL"
                    oCreationPackage.String = getTranslatedWord("&Delete PBI")
                    oCreationPackage.Position = iMenuPos
                    iMenuPos += 1
                    oMenus.AddEx(oCreationPackage)

                    oCreationPackage.UniqueID = "MOD"
                    oCreationPackage.String = getTranslatedWord("&Modify PBI")
                    oCreationPackage.Position = iMenuPos
                    iMenuPos += 1
                    oMenus.AddEx(oCreationPackage)

                    '##MA Start 10-02-2017
                    If oGridN.getSBOGrid().Rows.SelectedRows.Count = 1 Then
                        oCreationPackage.UniqueID = "DUP"
                        oCreationPackage.String = getTranslatedWord("&Duplicate PBI")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)
                    End If
                    '##MA End 10-02-2017

                Else
                    If goParent.goApplication.Menus.Exists("NEW") Then
                        goParent.goApplication.Menus.RemoveEx("NEW")
                    End If

                    If goParent.goApplication.Menus.Exists("DEL") Then
                        goParent.goApplication.Menus.RemoveEx("DEL")
                    End If

                    If goParent.goApplication.Menus.Exists("MOD") Then
                        goParent.goApplication.Menus.RemoveEx("MOD")
                    End If

                    '##MA Start 10-02-2017
                    If goParent.goApplication.Menus.Exists("DUP") Then
                        goParent.goApplication.Menus.RemoveEx("DUP")
                    End If
                    '##MA End 10-02-2017

                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                If pVal.BeforeAction = False Then
                    If pVal.oData.MenuUID.Equals("NEW") Then
                        doNewPBI(oForm)
                        Return False
                    ElseIf pVal.oData.MenuUID.Equals("MOD") Then
                        Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                        doModifyPBI(oGridN)
                        Return False
                    ElseIf pVal.oData.MenuUID.Equals("DEL") Then
                        Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                        If doCheckAndDeleteRow(oGridN) = False Then
                            Return False
                        End If

                        '##MA Start 10-02-2017
                    ElseIf pVal.oData.MenuUID.Equals("DUP") Then
                        Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                        setSharedData(oForm, "PBICode", oGridN.doGetFieldValue("Code", pVal.Row).ToString())
                        setSharedData(oForm, "CallFromManagerToDuplicate", True)
                        goParent.doOpenModalForm("IDH_PBI", oForm)
                        setSharedData(oForm, "CallFromManagerToDuplicate", False)
                        '##MA End 10-02-2017
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
                If pVal.BeforeAction = True Then
                    If pVal.ColUID = IDH_PBI._IDHRDMON2 OrElse _
                     pVal.ColUID = IDH_PBI._IDHRDTUE2 OrElse _
                     pVal.ColUID = IDH_PBI._IDHRDWED2 OrElse _
                     pVal.ColUID = IDH_PBI._IDHRDTHU2 OrElse _
                     pVal.ColUID = IDH_PBI._IDHRDFRI2 OrElse _
                     pVal.ColUID = IDH_PBI._IDHRDSAT2 OrElse _
                     pVal.ColUID = IDH_PBI._IDHRDSUN2 Then

                        'If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        'If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find") Then
                        doEnableAction(oForm, True)
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        'End If
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_KEYDOWN Then
                If pVal.BeforeAction = True Then
                    If pVal.ColUID = IDH_PBI._IDHRCDM OrElse _
                     pVal.ColUID = IDH_PBI._IDHRSQM OrElse _
                     pVal.ColUID = IDH_PBI._IDHRCDTU OrElse _
                     pVal.ColUID = IDH_PBI._IDHRSQTU OrElse _
                     pVal.ColUID = IDH_PBI._IDHRCDW OrElse _
                     pVal.ColUID = IDH_PBI._IDHRSQW OrElse _
                     pVal.ColUID = IDH_PBI._IDHRCDTH OrElse _
                     pVal.ColUID = IDH_PBI._IDHRSQTH OrElse _
                     pVal.ColUID = IDH_PBI._IDHRCDF OrElse _
                     pVal.ColUID = IDH_PBI._IDHRSQF OrElse _
                     pVal.ColUID = IDH_PBI._IDHRCDSA OrElse _
                     pVal.ColUID = IDH_PBI._IDHRSQSA OrElse _
                     pVal.ColUID = IDH_PBI._IDHRCDSU OrElse _
                     pVal.ColUID = IDH_PBI._IDHRSQSU Then

                        'If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find") Then
                            doEnableAction(oForm, True)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        End If
                    End If
                End If
            End If
            Return True
        End Function
        
        Private Sub doEnableAction(ByVal oForm As SAPbouiCOM.Form, ByVal bEnable As Boolean)
            Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_DOREAL")
            oItem.Visible = bEnable
            setUFValue(oForm, "IDH_DOREAL", "Y")

            oItem = oForm.Items.Item("IDH_ACDATE")
            oItem.Visible = bEnable
            setUFValue(oForm, "IDH_ACDATE", "")

            oItem = oForm.Items.Item("37")
            oItem.Visible = bEnable
        End Sub

        Private Function doDeleteSelectedRows(ByVal oForm As SAPbouiCOM.Form, ByVal sComment As String) As Boolean
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oSelected As SAPbouiCOM.SelectedRows
            Dim iRow As Integer
            Dim bDeletePBI As Boolean
            Dim bDeleted As Boolean = False

            oSelected = oGridN.getGrid().Rows.SelectedRows()
            If Not oSelected Is Nothing Then
                Dim oRecordSet As SAPbobsCOM.Recordset = Nothing

                For iIndex As Integer = 0 To oSelected.Count - 1
                    bDeletePBI = True

                    iRow = oGridN.getDataTableRowIndex(oSelected, iIndex)
                    Dim sPBICode As String = oGridN.doGetFieldValue("Code", iRow)
                    Dim sWOCode As String = oGridN.doGetFieldValue("U_IDHWO", iRow)

                    Dim sQry As String = "SELECT * FROM [@IDH_JOBSHD] " & _
                 "WHERE " & _
                 "	U_LnkPBI = '" & sPBICode & "'  " & _
                 "	AND (" & _
                 "   	   (U_SAINV Is Not Null AND U_SAINV != '')" & _
                 "	 	OR (U_SAORD Is Not Null AND U_SAORD != '')" & _
                 "	 	OR (U_TIPPO Is Not Null AND U_TIPPO != '')" & _
                 "	 	OR (U_JOBPO Is Not Null AND U_JOBPO != '')" & _
                 "	 	OR (U_SLPO Is Not Null AND U_SLPO != '')" & _
                 "	 	OR (U_ProPO Is Not Null AND U_ProPO != '')" & _
                 "      OR (U_ProGRPO Is Not Null AND U_ProGRPO != '' )" & _
                 "      OR (U_SODlvNot Is Not Null AND U_SODlvNot != '' )" & _
                 "      OR (U_GRIn Is Not Null AND U_GRIn != '' )" & _
                 "	 )"
                    Try
                        oRecordSet = goParent.goDB.doSelectQuery(sQry)
                        If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
                            Dim sParams As String() = {sPBICode}
                            com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage("PBIDELE", sParams)
                            bDeletePBI = False
                        End If
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, com.idh.bridge.Translation.getTranslatedWord("Error retrieving Job rows."))
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRJBR", {Nothing})
                    Finally
                        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                    End Try


                    If bDeletePBI Then
                        com.idh.bridge.DataHandler.INSTANCE.StartTransaction()
                        Try
                            'Delete the WOR's
                            'Dim oWOR As IDH_JOBSHD = New IDH_JOBSHD()
                            'bDeletePBI = oWOR.doDeleteByPBI( sPBICode, sComment )
                            'IDHAddOns.idh.data.Base.doReleaseObject(oWOR)		            		

                            'Delete the WO if all the WOR got deleted

                            'Delete all the PBI Details linked to this PBI
                            If bDeletePBI Then
                                Dim oPBIDetail As IDH_PBIDTL = New IDH_PBIDTL()
                                bDeletePBI = oPBIDetail.doDeleteByPBI(sPBICode, sComment, False, True)
                                IDHAddOns.idh.data.Base.doReleaseObject(oPBIDetail)
                            End If

                            ''20160120: #961 - We dont need to delete the WOH now 
                            ''because as per NEW Logic, the WORs against the deleted PBI will be stamped 
                            ''with comment and not permanently deleted 
                            ''Dim sWOHCode As String 
                            'If bDeletePBI Then
                            '    Dim oWOH As IDH_JOBENTR = New IDH_JOBENTR()
                            '    bDeletePBI = oWOH.doDeleteIfNoRows(sWOCode, sComment)
                            '    IDHAddOns.idh.data.Base.doReleaseObject(oWOH)
                            'End If

                            'Delete the PBI
                            If bDeletePBI Then
                                Dim oPBI As IDH_PBI = New IDH_PBI()
                                bDeletePBI = oPBI.doDeleteByPBI(sPBICode, sComment)
                                IDHAddOns.idh.data.Base.doReleaseObject(oPBI)
                            End If

                        Catch ex As Exception
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error deleting the records.")
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBDE", {Nothing})

                            'com.idh.bridge.DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                            bDeletePBI = False
                        Finally
                            If bDeletePBI Then
                                com.idh.bridge.DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                            Else
                                com.idh.bridge.DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                            End If

                        End Try
                        bDeleted = True
                    End If
                Next
            End If
            Return bDeleted
        End Function

        Private Function doCheckAndDeleteRow(ByVal oGridN As FilterGrid) As Boolean
            Dim oSelected As SAPbouiCOM.SelectedRows
            oSelected = oGridN.getGrid().Rows.SelectedRows()
            If Not oSelected Is Nothing Then
                'Show the confirmation message.
                If com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageOC("PBIDEL", Nothing) = 1 Then
                    'Get the Deletion comment
                    goParent.doOpenModalForm("IDH_COMMFRM", oGridN.getSBOForm())
                Else
                    Return False
                End If
            End If
            Return True
        End Function

        Private Sub doNewPBI(ByVal oForm As SAPbouiCOM.Form)
            Dim sCardCode As String = getUFValue(oForm, "e_CardCode")
            Dim sCardName As String
            If sCardCode.Length > 0 Then
                sCardName = Config.INSTANCE.doGetBPName(sCardCode)
            Else
                sCardName = getUFValue(oForm, "e_CardName")
            End If

            Dim sAddr As String = getUFValue(oForm, "e_Addr")

            If sCardCode Is Nothing OrElse sCardCode.Length = 0 Then
                'DataHandler.INSTANCE.doUserError("Customer not set.")
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Customer not set.", "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("Customer")})
                Exit Sub
            End If

            setSharedData(oForm, "e_CardCode", sCardCode)
            setSharedData(oForm, "e_CardName", sCardName)
            setSharedData(oForm, "e_Addr", sAddr)
            setSharedData(oForm, "PBICode", "")
            setSharedData(oForm, "TRG", "IDH_PBI")
            goParent.doOpenModalForm("IDH_PBI", oForm)
        End Sub

        Private Sub doModifyPBI(ByVal oGridN As FilterGrid)
            Dim oSelected As SAPbouiCOM.SelectedRows
            oSelected = oGridN.getGrid().Rows.SelectedRows()
            If Not oSelected Is Nothing Then
                Dim oPBIs As ArrayList = New ArrayList()
                Dim iRow As Integer

                For iIndex As Integer = 0 To oSelected.Count - 1
                    iRow = oGridN.getDataTableRowIndex(oSelected, iIndex)
                    oPBIs.Add(oGridN.doGetFieldValue(IDH_PBI._Code, iRow))
                Next

                Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PBIModify = New WR1_FR2Forms.idh.forms.fr2.PBIModify(Nothing, oGridN.getSBOForm(), Nothing)
                oOOForm.PBINumbers = oPBIs
                oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandlePBIMODOk)
                'oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogCancelReturn(AddressOf doCancelAddUpdatePrices)
                oOOForm.doShowModal(oGridN.getSBOForm())
            End If
        End Sub

        Public Function doHandlePBIMODOk(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            doReLoadData(oOForm.SBOParentForm, True)
            oOForm.SBOForm.Close()
            Return True
        End Function
#End Region

#Region "Search form Procs"

        'Opening a Modal Form Moving the Data To Shared Buffer
        Protected Overridable Sub doChooseCustomer(ByVal oForm As SAPbouiCOM.Form)
            Dim sCardCode As String = getUFValue(oForm, "e_Cardcode", True)
            Dim sCardName As String = getUFValue(oForm, "e_CardName", True)

            setSharedData(oForm, "TRG", "CUS")
            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_TYPE", "F-C")
            setSharedData(oForm, "IDH_NAME", sCardName)

            'goParent.doOpenModalForm("IDHCSRCH", oForm)
            If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                goParent.doOpenModalForm("IDHCSRCH", oForm) 'Actual BP Search Form 
            Else
                If (getSharedData(oForm, "IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_BPSRCHOPEND").ToString.Trim = "") Then
                    goParent.doOpenModalForm("IDHNCSRCH", oForm) 'NEW BP Search Form 
                End If
            End If
        End Sub
        Protected Overridable Sub doChooseCarrier(ByVal oForm As SAPbouiCOM.Form)
            Dim sCarrierCode As String = getUFValue(oForm, "IDHCARCD", True)
            Dim sCarrierName As String = getUFValue(oForm, "IDHCARDC", True)

            setSharedData(oForm, "TRG", "CAR")
            setSharedData(oForm, "IDH_BPCOD", sCarrierCode)
            setSharedData(oForm, "IDH_NAME", sCarrierName)
            setSharedData(oForm, "IDH_TYPE", "S")
            setSharedData(oForm, "SILENT", "SHOWMULTI")

            'goParent.doOpenModalForm("IDHCSRCH", oForm)
            If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                goParent.doOpenModalForm("IDHCSRCH", oForm) 'Actual BP Search Form 
            Else
                If (getSharedData(oForm, "IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_BPSRCHOPEND").ToString.Trim = "") Then
                    goParent.doOpenModalForm("IDHNCSRCH", oForm) 'NEW BP Search Form 
                End If
            End If
        End Sub

        Protected Overridable Sub doChooseDisposal(ByVal oForm As SAPbouiCOM.Form)
            Dim sDisposalCode As String = getUFValue(oForm, "IDHDISCD", True)
            Dim sDisposalName As String = getUFValue(oForm, "IDHDISDC", True)

            setSharedData(oForm, "TRG", "DIS")
            setSharedData(oForm, "IDH_BPCOD", sDisposalCode)
            setSharedData(oForm, "IDH_NAME", sDisposalName)

            setSharedData(oForm, "IDH_TYPE", "S")
            setSharedData(oForm, "SILENT", "SHOWMULTI")

            'goParent.doOpenModalForm("IDHCSRCH", oForm)
            If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                goParent.doOpenModalForm("IDHCSRCH", oForm) 'Actual BP Search Form 
            Else
                If (getSharedData(oForm, "IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_BPSRCHOPEND").ToString.Trim = "") Then
                    goParent.doOpenModalForm("IDHNCSRCH", oForm) 'NEW BP Search Form 
                End If
            End If
        End Sub

        Protected Overridable Sub doChooseSiteAd(ByVal oForm As SAPbouiCOM.Form)
            Dim sCardCode As String = getUFValue(oForm, "e_Cardcode")
            Dim sAddr As String = getUFValue(oForm, "e_Addr")

            setSharedData(oForm, "TRG", "SA")
            setSharedData(oForm, "IDH_CUSCOD", sCardCode)
            setSharedData(oForm, "IDH_ADDRES", sAddr)
            setSharedData(oForm, "IDH_ADRTYP", "S")
            goParent.doOpenModalForm("IDHASRCH", oForm)
        End Sub

#End Region

#Region "Data Interface"

        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
            'This will now Automatically update all the current DB fields and
            'generate and set the Code value if the code was not set
            'Return doAutoUpdate(oForm, getHistoryTable(), "TRN1KEY")
            Return True
        End Function

#End Region

#Region "Processing"

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form,
                  ByRef pVal As SAPbouiCOM.ItemEvent,
                  ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = False Then
                '                'If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
                '            			If doUpdatePBI(oForm) Then        
                '            				doReLoadData(oForm, True)
                '            			End If
                '                    'End If
                '                ElseIf oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find") Then
                '                    doReLoadData(oForm, True)
                '                End If
                '                BubbleEvent = False
            Else
                If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
                    'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    If doUpdatePBI(oForm) Then
                        doReLoadData(oForm, True)
                        'oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                    End If
                ElseIf oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find") Then
                    'ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    doReLoadData(oForm, True)
                End If
                BubbleEvent = False
            End If
        End Sub

        '***
        '*** This function will only update the Routes and Sequences it the Length is bigger than 0
        '*** If any of the days are changed all the days will be updated along with the Routes and Seq 
        '**			Unselected days will be cleared along with their routes and seq
        '***
        Public Function doUpdatePBI(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            Dim aRows As ArrayList = oGridN.doGetChangedRows()
            Dim oPBI As IDH_PBI = New IDH_PBI()

            Dim sPBICode As String
            Dim sMonSelected, sTueSelected, sWedSelected, sThuSelected, sFriSelected, sSatSelected, sSunSelected As String
            Dim sMonRoute, sTueRoute, sWedRoute, sThuRoute, sFriRoute, sSatRoute, sSunRoute As String
            Dim sMonSeq, sTueSeq, sWedSeq, sThuSeq, sFriSeq, sSatSeq, sSunSeq As String

            Dim sDoRealTime As String = getUFValue(oForm, "IDH_DOREAL")

            If Not aRows Is Nothing AndAlso aRows.Count > 0 Then
                For iIndex As Integer = 0 To aRows.Count - 1
                    oGridN.setCurrentDataRowIndex(aRows(iIndex))
                    sPBICode = oGridN.doGetFieldValue(IDH_PBI._Code)

                    If oPBI.getByKey(sPBICode) Then
                        sMonSelected = oGridN.doGetFieldValue(IDH_PBI._IDHRDMON2)
                        sTueSelected = oGridN.doGetFieldValue(IDH_PBI._IDHRDTUE2)
                        sWedSelected = oGridN.doGetFieldValue(IDH_PBI._IDHRDWED2)
                        sThuSelected = oGridN.doGetFieldValue(IDH_PBI._IDHRDTHU2)
                        sFriSelected = oGridN.doGetFieldValue(IDH_PBI._IDHRDFRI2)
                        sSatSelected = oGridN.doGetFieldValue(IDH_PBI._IDHRDSAT2)
                        sSunSelected = oGridN.doGetFieldValue(IDH_PBI._IDHRDSUN2)

                        sMonRoute = oGridN.doGetFieldValue(IDH_PBI._IDHRCDM)
                        sTueRoute = oGridN.doGetFieldValue(IDH_PBI._IDHRCDTU)
                        sWedRoute = oGridN.doGetFieldValue(IDH_PBI._IDHRCDW)
                        sThuRoute = oGridN.doGetFieldValue(IDH_PBI._IDHRCDTH)
                        sFriRoute = oGridN.doGetFieldValue(IDH_PBI._IDHRCDF)
                        sSatRoute = oGridN.doGetFieldValue(IDH_PBI._IDHRCDSA)
                        sSunRoute = oGridN.doGetFieldValue(IDH_PBI._IDHRCDSU)

                        sMonSeq = oGridN.doGetFieldValue(IDH_PBI._IDHRSQM)
                        sTueSeq = oGridN.doGetFieldValue(IDH_PBI._IDHRSQTU)
                        sWedSeq = oGridN.doGetFieldValue(IDH_PBI._IDHRSQW)
                        sThuSeq = oGridN.doGetFieldValue(IDH_PBI._IDHRSQTH)
                        sFriSeq = oGridN.doGetFieldValue(IDH_PBI._IDHRSQF)
                        sSatSeq = oGridN.doGetFieldValue(IDH_PBI._IDHRSQSA)
                        sSunSeq = oGridN.doGetFieldValue(IDH_PBI._IDHRSQSU)

                        If sMonSelected = "Y" Then
                            oPBI.U_IDHRDMON2 = "Y"
                            If sMonRoute.Length > 0 Then oPBI.U_IDHRCDM = sMonRoute
                            If sMonSeq.Length > 0 Then oPBI.U_IDHRSQM = sMonSeq
                        Else
                            oPBI.U_IDHRDMON2 = "N"
                            oPBI.U_IDHRCDM = ""
                            oPBI.U_IDHRSQM = 0
                        End If

                        If sTueSelected = "Y" Then
                            oPBI.U_IDHRDTUE2 = "Y"
                            If sTueRoute.Length > 0 Then oPBI.U_IDHRCDTU = sTueRoute
                            If sTueSeq.Length > 0 Then oPBI.U_IDHRSQTU = sTueSeq
                        Else
                            oPBI.U_IDHRDTUE2 = "N"
                            oPBI.U_IDHRCDTU = ""
                            oPBI.U_IDHRSQTU = 0
                        End If

                        If sWedSelected = "Y" Then
                            oPBI.U_IDHRDWED2 = "Y"
                            If sWedRoute.Length > 0 Then oPBI.U_IDHRCDW = sWedRoute
                            If sWedSeq.Length > 0 Then oPBI.U_IDHRSQW = sWedSeq
                        Else
                            oPBI.U_IDHRDWED2 = "N"
                            oPBI.U_IDHRCDW = ""
                            oPBI.U_IDHRSQW = 0
                        End If

                        If sThuSelected = "Y" Then
                            oPBI.U_IDHRDTHU2 = "Y"
                            If sThuRoute.Length > 0 Then oPBI.U_IDHRCDTH = sThuRoute
                            If sThuSeq.Length > 0 Then oPBI.U_IDHRSQTH = sThuSeq
                        Else
                            oPBI.U_IDHRDTHU2 = "N"
                            oPBI.U_IDHRCDTH = ""
                            oPBI.U_IDHRSQTH = 0
                        End If

                        If sFriSelected = "Y" Then
                            oPBI.U_IDHRDFRI2 = "Y"
                            If sFriRoute.Length > 0 Then oPBI.U_IDHRCDF = sFriRoute
                            If sFriSeq.Length > 0 Then oPBI.U_IDHRSQF = sFriSeq
                        Else
                            oPBI.U_IDHRDFRI2 = "N"
                            oPBI.U_IDHRCDF = ""
                            oPBI.U_IDHRSQF = 0
                        End If

                        If sSatSelected = "Y" Then
                            oPBI.U_IDHRDSAT2 = "Y"
                            If sSatRoute.Length > 0 Then oPBI.U_IDHRCDSA = sSatRoute
                            If sSatSeq.Length > 0 Then oPBI.U_IDHRSQSA = sSatSeq
                        Else
                            oPBI.U_IDHRDSAT2 = "N"
                            oPBI.U_IDHRCDSA = ""
                            oPBI.U_IDHRSQSA = 0
                        End If

                        If sSunSelected = "Y" Then
                            oPBI.U_IDHRDSUN2 = "Y"
                            If sSunRoute.Length > 0 Then oPBI.U_IDHRCDSU = sSunRoute
                            If sSunSeq.Length > 0 Then oPBI.U_IDHRSQSU = sSunSeq
                        Else
                            oPBI.U_IDHRDSUN2 = "N"
                            oPBI.U_IDHRCDSU = ""
                            oPBI.U_IDHRSQSU = 0
                        End If

                        oPBI.U_IDHRECOM = oPBI.doDescribeCoverage()

                        'Check what has changed
                        Dim iSeqChange As Integer = oPBI.SeqChanged
                        Dim bDateMightHaveChanged As Boolean = oPBI.DatesMightHaveChanged

                        ''Save and process the PBI
                        If oPBI.doProcessData() Then
                            Dim sDate As String = getUFValue(oForm, "IDH_ACDATE")
                            Dim oActionDate As DateTime

                            If Not sDate Is Nothing AndAlso sDate.Length >= 6 Then
                                sDate = com.idh.utils.Dates.doSBODateToSQLDate(sDate)
                                oActionDate = Conversions.ToDateTime(sDate)
                            Else
                                oActionDate = DateTime.Now
                            End If

                            If bDateMightHaveChanged = False Then
                                oPBI.doUpdateWORSequences(oActionDate)
                            Else
                                If sDoRealTime = "Y" Then
                                    Dim oWOM As WR1_PBI.IDH_PBI_Handler = New WR1_PBI.IDH_PBI_Handler(DataHandler.INSTANCE.SBOCompany)

                                    Dim dSetNextRun As Date = oPBI.U_IDHNEXTR
                                    Dim dPBIStartDate As Date = oPBI.U_IDHRECSD
                                    Dim dCalculatedNextRun As Date = oActionDate

                                    'USED FOR DEBUGGING
                                    Dim sRunDate As String = dSetNextRun.ToString()
                                    Dim sCalcRunDate As String = dCalculatedNextRun.ToString()

                                    While Date.Compare(dCalculatedNextRun, dSetNextRun) < 0
                                        If oWOM.ProcessOrderRun(oPBI.Code, dCalculatedNextRun) = False Then
                                            Exit While
                                        Else
                                            dCalculatedNextRun = oWOM.CalculatedNextRun
                                            sCalcRunDate = dCalculatedNextRun.ToString()
                                        End If
                                    End While
                                Else
                                    'INSERT INTO SCHEDULER
                                    Dim oSched As IDH_Scheduler = New IDH_Scheduler()

                                    oSched.Description = "PBI Manager change"
                                    oSched.Class = "com.idh.processor.PBI"
                                    oSched.Processor = "doProcess"

                                    Dim sParam As String = "CODES=[" & oPBI.Code & "];" & Chr(13) & "TOEND=[Y];" & Chr(13)
                                    oSched.Parameters = sParam

                                    oSched.StartDate = oPBI.U_IDHRECSD
                                    'oSched.EndDate_AsString(tbEndDate.Text);

                                    oSched.CurrCount = 0
                                    oSched.MaxCount = -1
                                    oSched.MonthDay = 0
                                    oSched.PMonth = 0

                                    oSched.Su = Nothing
                                    oSched.Mo = Nothing
                                    oSched.Tu = Nothing
                                    oSched.We = Nothing
                                    oSched.Th = Nothing
                                    oSched.Fr = Nothing
                                    oSched.Sa = Nothing

                                    oSched.ProcDate = oActionDate

                                    oSched.ProcTime = Nothing

                                    oSched.LastRun = DateTime.Now
                                    oSched.Status = "Added"

                                    If oSched.doAddDataRow() = False Then
                                        'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Scheduling the Update.", "Error Scheduling the Update.")
                                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Scheduling the Update.", "ERSYESUP", {Nothing})
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next
            End If
            Return True
        End Function


#End Region

#Region "Reports"
        Protected Sub doReportByBtn(ByVal oForm As SAPbouiCOM.Form, ByVal sBtn As String)
            Dim sReportFile As String = Nothing
            Dim sRepId As String
            Select Case sBtn
                Case "IDHPO"
                    sRepId = "PBIPOR"
                    sReportFile = Config.Parameter(sRepId)
                Case "IDHDOC"
                    sRepId = "PBIDOCR"
                    sReportFile = Config.Parameter(sRepId)
                Case "IDHCUST"
                    sRepId = "PBICUSTR"
                    sReportFile = Config.Parameter(sRepId)
                Case Else
                    sRepId = "DontKnow"
            End Select

            If Not sReportFile Is Nothing AndAlso sReportFile.Length > 0 Then
                doReport(oForm, sReportFile)
            Else
                'com.idh.bridge.DataHandler.INSTANCE.doError("The report was not set: " & sRepId)
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("The report was not set: " & sRepId, "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("Report")})
            End If
        End Sub

        Protected Overridable Sub doReport(ByVal oForm As SAPbouiCOM.Form, ByVal sReportFile As String)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")

            Dim oParams As New Hashtable
            Dim oRows As ArrayList = oGridN.doPrepareListFromSelection("Code")
            If oRows.Count() = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("Select a PBI row.")
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Select a PBI row.", "ERUSSPBI", {Nothing})
                Exit Sub
            End If

            oParams.Add("PBINum", oRows)

            If Config.INSTANCE.getParameterAsBool("UNEWRPTV", False) Then
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
            Else
                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
            End If
        End Sub
#End Region

#Region "Other Procs"

        '        Sub GarCol(ByRef objectRef)
        '            Try
        '                System.Runtime.InteropServices.Marshal.ReleaseComObject(objectRef)
        '                objectRef = Nothing
        '                GC.Collect()
        '            Catch ex As Exception
        '            Finally
        '                GC.Collect()
        '            End Try
        '
        '        End Sub

#End Region

    End Class
End Namespace

