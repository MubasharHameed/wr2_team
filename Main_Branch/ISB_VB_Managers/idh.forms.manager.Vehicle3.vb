﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.utils.Conversions
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.data
Imports com.idh.utils
Imports com.idh.dbObjects.User

Imports com.idh.bridge.lookups
Imports com.idh.bridge.resources

Namespace idh.forms.manager
    Public Class Vehicle3
        Inherits idh.forms.manager.Templ

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHVECR3", Nothing, "Vehicle Manager3.srf", iMenuPosition, "Vehicle Schedule Manager 3")
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_MATRIX_COLLAPSE_PRESSED)
        End Sub

        Protected Overrides Sub doSetGridFilters(ByVal oGridN As FilterGrid)
            Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oGridN.getSBOForm, "VEHGRID")
            oVehicleGrid.doAddFilterField("IDH_VEHREG", "VehReg", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oVehicleGrid.doAddFilterField("IDH_VEHRT", "Route", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            'oVehicleGrid.doAddFilterField("IDH_VRQSTF", "ReqDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            'oVehicleGrid.doAddFilterField("IDH_VRQSTT", "ReqDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)

            'oVehicleGrid.doAddFilterField("IDH_REQSTF", "ReqDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            'oVehicleGrid.doAddFilterField("IDH_REQSTT", "ReqDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)

            oVehicleGrid.doAddFilterField("IDH_TYPE", "Type", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 100)
            'oVehicleGrid.doAddFilterField("IDH_ITMCD", "Container", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oVehicleGrid.doAddFilterField("IDH_ITMGRP", "ContainerGrpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)

            oGridN.doAddFilterField("IDH_REQSTF", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_REQSTT", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            'oGridN.doAddFilterField("IDH_ACTSTF", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            'oGridN.doAddFilterField("IDH_ACTSTT", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_CUST", "e.U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_NAME", "e.U_CardNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ITMCD", "r.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ITMGRP", "r.U_ItmGrp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)
            oGridN.doAddFilterField("IDH_ROUTE", "r.U_IDHRTCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_JOBTYP", "r.U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ORDNO", "e.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            ''oGridN.doAddFilterField("IDH_VEHREG", "r.U_Lorry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DRIVER", "r.U_Driver", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ADDR", "e.U_Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_POSTCD", "e.U_ZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_CUSPHO", "e.U_Phone1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_SITETL", "e.U_SiteTl", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_REF", "r.U_CustRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WSCD", "r.U_WasCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WSDC", "r.U_WasDsc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_USER", "r.U_User", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_BRANCH", "r.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)
            ''## MA Start 29-08-2014 Issue#421
            oGridN.doAddFilterField("IDH_BRANC2", "r.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            ''## MA End 29-08-2014 Issue#421

            'oGridN.doAddFilterField("IDH_STEID", "e.U_SteId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_STREET", "e.U_Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            '..Add the Progress Filter
            oGridN.doAddFilterField("IDH_PROG", "v.State", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            'Veh Reg filter on the Jobs grid 
            oGridN.doAddFilterField("IDH_JOBVRG", "r.U_Lorry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PROCD", "r.U_ProCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            '..Remove Filters  - SO Status, PO Status, Act Start Date, Pnone No., Site Phone No., Site Id 
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            oGridN.doAddListField("'LINK'", "", True, 15, "CHECKBOX", Nothing)
            'If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
            '    If doSetGrid(oGridN, "LINESGRID") Then
            '        Return
            '    End If
            'End If

            oGridN.doAddListField("r.U_Lorry", "Vehicle Reg.", False, 30, Nothing, Nothing)
            oGridN.doAddListField("r.U_LorryCd", "Veh Reg. Code", True, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", True, 30, Nothing, Nothing) '"SRC*IDHEMPSCR(DRIVER)[IDH_EMPFN][WGCODE;" + IDH_ENQITEM._WstGpNm + "=WGNAME]"

            oGridN.doAddListField("v.State", "Progress", False, 15, Nothing, Nothing)
            'oGridN.doAddListField("'" & IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\' + v.IMG", "", False, 10, "PICTURE", Nothing)
            oGridN.doAddListField("v.IMG", "", False, 10, "PICTURE", Nothing)
            oGridN.doAddListField("r.U_Status", "Sales Status", False, 15, Nothing, Nothing)
            oGridN.doAddListField("r.U_PStat", "Purchase Status", False, 15, Nothing, Nothing)

            oGridN.doAddListField("r.U_JobNr", "Order No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Row No.", False, -1, Nothing, Nothing)

            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_IDHRTCD", "Route", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r." + IDH_JOBSHD._ProCd, getTranslatedWord("Producer Code"), False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r." + IDH_JOBSHD._ProNm, getTranslatedWord("Producer Name"), False, 70, Nothing, Nothing)

            oGridN.doAddListField("r.U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RDate", "Req. Start", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTime", "Req. Time", False, -1, "TIME", Nothing)

            oGridN.doAddListField("r.U_ASDate", "Act. Start", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASTime", "Act. Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act EDate", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_AETime", "Act ETime", False, 0, "TIME", Nothing)

            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Serial", "Container Serial No.", False, -1, Nothing, Nothing)
            '..
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("e.U_Address", "Address", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_Street", "Street", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_SteId", "Site Id", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_ItmGrp", "Item Grp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)

            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Tip", "Supplier", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            '..
            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Cust. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RdWgt", "Read Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Price", "Price", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicCh", "SLicCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Charge Total", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_VehTyp", "Vehicle", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "TipCost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "TCharge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "TCTotal", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_CarrCd", "Carrier Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_CarrNm", "Carrier Name", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WROrd", "WO No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WRRow", "WO Row No.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_IDHSEQ", "Sequence", False, -1, Nothing, Nothing)
            '..Remove Fields  - SO Status, PO Status, Pnone No., Site Phone No.
            '..Add VehType, 
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                oForm.Title = gsTitle

                Dim oItem As SAPbouiCOM.Item

                oItem = doAddUFCheck(oForm, "IDH_AFOJR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False

                oItem = doAddUFCheck(oForm, "IDH_AFRDT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False

                Dim oVehicleGrid As UpdateGrid = New UpdateGrid(Me, oForm, "VEHGRID", 7, 108, 300, 362)
                Dim oGridN As OrderRowGrid = New OrderRowGrid(Me, oForm, "LINESGRID", 307, 108, 635, 362, "r")

                oVehicleGrid.getSBOItem().AffectsFormMode = False

                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False

                oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_FIND_MODE Or SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

                setUFValue(oForm, "IDH_AFRDT", "Y")

                oForm.EnableMenu(Config.NAV_ADD, False)
                oForm.EnableMenu(Config.NAV_FIND, False)
                oForm.EnableMenu(Config.NAV_FIRST, False)
                oForm.EnableMenu(Config.NAV_NEXT, False)
                oForm.EnableMenu(Config.NAV_PREV, False)
                oForm.EnableMenu(Config.NAV_LAST, False)

                Dim fSettings As SAPbouiCOM.FormSettings
                fSettings = oForm.Settings
                fSettings.Enabled = False
                '## MA Start 01-09-2014 Issue#421
                doAddUF(oForm, "IDH_BRANC1", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 2000, False, False)
                '## MA Start 01-09-2014 Issue#421
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try
        End Sub

        'Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
        '    If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
        '        If pVal.ItemUID = "LINESGRID" Then
        '            If pVal.BeforeAction = True Then
        '                Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)

        '                Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
        '                Dim iSelectionCount As Integer = oSelected.Count

        '                'If oGridN.getGrid().Rows.SelectedRows.Count > 0 Then
        '                If iSelectionCount = 1 Then
        '                End If
        '                'Else
        '            End If
        '        End If
        '    ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL Then
        '        If pVal.ItemUID = "LINESGRID" Then
        '            If pVal.BeforeAction = True Then
        '            End If
        '        End If
        '    ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL_MULTI Then
        '        If pVal.ItemUID = "LINESGRID" Then
        '            If pVal.BeforeAction = True Then
        '            End If
        '        End If
        '    ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
        '        If pVal.ItemUID = "LINESGRID" Then
        '            If pVal.BeforeAction = False Then
        '            End If
        '        End If
        '    End If
        '    Return True
        'End Function

        Protected Overridable Sub doSetVehGridFilters(ByRef oVehicleGrid As UpdateGrid)
        End Sub

        'Public Overridable Function getListVehRequiredStr() As String
        '    Return "VehReg != 'Zz-Ignore'"
        'End Function

        Protected Overrides Sub doTheGridLayout(ByVal oGridN As UpdateGrid)
            If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                Dim sFormTypeId As String = oGridN.getSBOForm().TypeEx
                Dim oFormSettings As IDH_FORMSET = getWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId)
                If oFormSettings Is Nothing Then
                    oFormSettings = New IDH_FORMSET()
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, doGetCapacityQuery())
                    setWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId, oFormSettings)

                    oFormSettings.doAddFieldToGrid(oGridN)

                    If oGridN.GridId = "VEHGRID" Then
                        doSetListVehFields(oGridN)
                    Else
                        doSetListFields(oGridN)
                        doAddAdditionalRequiredListFields(oGridN)
                    End If

                    'If oFormSettings.SkipFormSettings = False Then
                    '    oFormSettings.doSetGrid(oGridN)
                    'End If
                    oFormSettings.doSyncDB(oGridN)
                Else
                    If oFormSettings.SkipFormSettings Then
                        If oGridN.GridId = "VEHGRID" Then
                            doSetListVehFields(oGridN)
                        Else
                            doSetListFields(oGridN)
                            doAddAdditionalRequiredListFields(oGridN)
                        End If
                    Else
                        oFormSettings.doAddFieldToGrid(oGridN)
                        'oFormSettings.doSetGrid(oGridN)
                    End If
                End If
            Else
                If oGridN.GridId = "VEHGRID" Then
                    doSetListVehFields(oGridN)
                Else
                    doSetListFields(oGridN)
                    doAddAdditionalRequiredListFields(oGridN)
                End If
            End If
        End Sub

        Protected Overridable Sub doSetListVehFields(ByRef oVehicleGrid As IDHAddOns.idh.controls.UpdateGrid)
            'If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
            '    If doSetGrid(oVehicleGrid, "VEHGRID") Then
            '        Return
            '    End If
            'End If

            oVehicleGrid.doAddListField("VehReg", "Vehicle Reg.", False, 50, Nothing, Nothing)
            oVehicleGrid.doAddListField("Description", "Description", False, 100, Nothing, Nothing)
            oVehicleGrid.doAddListField("Driver", "Driver", False, 50, Nothing, Nothing)
            oVehicleGrid.doAddListField("ReqDate", "Requested Date", False, 50, Nothing, Nothing)
            'oVehicleGrid.doAddListField("'" & IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\' + " & doGetCapacityQuery(), "", False, 10, "PICTURE","CAP")
            oVehicleGrid.doAddListField(doGetCapacityQuery(), "", False, 10, "PICTURE", "CAP")
            oVehicleGrid.doAddListField("Total", "Day Jobs", False, 20, Nothing, Nothing)
            oVehicleGrid.doAddListField("NotStarted", "Waiting", False, 20, Nothing, Nothing)
            oVehicleGrid.doAddListField("Started", "Busy", False, 20, Nothing, Nothing)
            oVehicleGrid.doAddListField("DayCap", "Capacity", False, 20, Nothing, Nothing)
            ''MA Start 24-11-2014 Issue#483
            Dim iWidth As Int32 = -1
            If Config.INSTANCE.getParameterAsBool("VSM3CRCD", False) = False Then
                iWidth = 0
            End If
            oVehicleGrid.doAddListField("CARRCODE", "Carrier Code(Supp)", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oVehicleGrid.doAddListField("CARRNAME", "Carrier Name(Supp)", False, 0, Nothing, Nothing)
            ''MA Start 24-11-2014

        End Sub

        Public Overrides Sub doHideTotals(ByVal oForm As SAPbouiCOM.Form)

        End Sub
        Protected Overrides Sub doItemGroupCombo(ByRef oForm As SAPbouiCOM.Form)

        End Sub
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            doAddWF(oForm, "VehData", Nothing)
            doAddWF(oForm, "VDriver", Nothing)
            ''MA Start 21-11-2014
            doAddWF(oForm, "CarrCode", Nothing)
            doAddWF(oForm, "CarrName", Nothing)
            ''MA End 21-11-2014
            'doAddWF(oForm, "JobData", Nothing)

            Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "VEHGRID")
            If oVehicleGrid Is Nothing Then
                oVehicleGrid = New UpdateGrid(Me, oForm, "VEHGRID")
            End If
            oVehicleGrid.doSetSBOAutoReSize(False)

            'MyBase.doBeforeLoadData(oForm)

            setUFValue(oForm, "IDH_PROG", "!Complete")

            doSetVehGridFilters(oVehicleGrid)
            oVehicleGrid.doApplyExpand(IDHAddOns.idh.controls.IDHGrid.COLLAPSED)
            'oVehicleGrid.doAutoExpand(True)

            oVehicleGrid.doAddGridTable(New GridTable(Config.INSTANCE.getVehicleCapacityView()), True)

            oVehicleGrid.setOrderValue("VehReg, Description, ReqDate")
            'oVehicleGrid.setRequiredFilter(getListVehRequiredStr())
            oVehicleGrid.doSetDoCount(False)
            oVehicleGrid.doSetBlankLine(False)

            'doSetListVehFields(oVehicleGrid)
            'doTheVehicleGridLayout(oVehicleGrid)

            MyBase.doBeforeLoadData(oForm)
            '## MA Start 02-09-2014 Issue#421
            doSetMultiBranch(oForm)
            '## MA End 02-09-2014 Issue#421
            doTheGridLayout(oVehicleGrid)

            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            oGridN.doSetDoCount(False)
            oGridN.BlockPopupMenu = True

            doGetItemGroups(oForm)
            doFillTypes(oForm)

            'Set the initial filter date to tofays date
            Dim sToDay As String = com.idh.utils.Dates.doDateToSBODateStr(DateTime.Now)
            setUFValue(oForm, "IDH_REQSTF", sToDay)
            setUFValue(oForm, "IDH_REQSTT", sToDay)

            'Set default value of Type filter from WR Config 
            Dim sDefaultType As String = Config.ParameterWithDefault("DFLTYPE", "")
            If sDefaultType IsNot Nothing AndAlso sDefaultType.Length > 1 Then
                sDefaultType = sDefaultType.Substring(0, 1)
            End If
            setUFValue(oForm, "IDH_TYPE", sDefaultType)
        End Sub

        Public Sub doGetItemGroups(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_ITMGRP", "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpCod=jt.U_ItemGrp AND jt.U_OrdCat='Waste Order'", "ItmsGrpNam", True)
        End Sub

        Private Sub doFillTypes(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oCombo As SAPbouiCOM.ComboBox
            oItem = oForm.Items.Item("IDH_TYPE")
            oItem.DisplayDesc = True
            oCombo = oItem.Specific

            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("", getTranslatedWord("All"))
            oCombo.ValidValues.Add("A", getTranslatedWord("Asset-OnRoad"))
            oCombo.ValidValues.Add("O", getTranslatedWord("Asset-OffRoad"))
            oCombo.ValidValues.Add("S", getTranslatedWord("Sub Contract"))
            oCombo.ValidValues.Add("H", getTranslatedWord("Owner Driver"))
            oCombo.ValidValues.Add("A,O", getTranslatedWord("Asset On/Off Road"))
            oCombo.ValidValues.Add("A,S", getTranslatedWord("Asset On Road/Sub Contract"))
            oCombo.ValidValues.Add("A,H", getTranslatedWord("Asset On Road/Owner Driver"))
            ''## MA Start 06-08-2014 CR: Scott-->HH
            oCombo.ValidValues.Add("C", getTranslatedWord("Customer"))
            ''## MA End 06-08-2014
        End Sub

        Public Overridable Function getVehicleRequiredStr(ByVal oForm As SAPbouiCOM.Form) As String
            Dim sDateF As String = getUFValue(oForm, "IDH_REQSTF")
            Dim sDateT As String = getUFValue(oForm, "IDH_REQSTT")
            Dim sFilter As String = "VehReg != 'Zz-Ignore' "

            Dim sDateFilter As String = ""
            If sDateF.Length > 0 Then
                sDateFilter = "ReqDate >= '" & sDateF & "'"
            End If

            If sDateT.Length > 0 Then
                If sDateFilter.Length > 0 Then
                    sDateFilter = sDateFilter & " AND ReqDate <= '" & sDateT & "'"
                Else
                    sDateFilter = "ReqDate <= '" & sDateT & "'"
                End If
            End If

            If sDateFilter.Length > 0 Then
                sFilter = sFilter & " AND (" &
                                    "       ( " &
                                    "           ( " &
                                    "               SELECT COUNT(*) " &
                                    "               FROM [" + Config.INSTANCE.getVehicleCapacityView() + "] AS v" &
                                    "               WHERE v.VehReg = [" + Config.INSTANCE.getVehicleCapacityView() + "].VehReg " &
                                    "                   AND " & sDateFilter &
                                    "           ) = 0 " &
                                    "           AND ReqDate = '1900-01-01'" &
                                    "       ) OR ( " &
                                    "               " & sDateFilter &
                                    "       ) " &
                                    " ) "
            End If

            Return sFilter

            'oVehicleGrid.doAddFilterField("IDH_VRQSTF", "ReqDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            'oVehicleGrid.doAddFilterField("IDH_VRQSTT", "ReqDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)

            'oVehicleGrid.doAddFilterField("IDH_REQSTF", "ReqDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            'oVehicleGrid.doAddFilterField("IDH_REQSTT", "ReqDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
        End Function

        Protected Sub doReloadVehicle(ByVal oForm As SAPbouiCOM.Form)
            'oForm.Freeze(True)
            Try
                If getUFValue(oForm, "IDH_AFRDT").Equals("Y") Then
                    '    Dim sDateF As String = getUFValue(oForm, "IDH_REQSTF")
                    '    Dim sDateT As String = getUFValue(oForm, "IDH_REQSTT")

                    '    setUFValue(oForm, "IDH_VRQSTF", sDateF)
                    '    setUFValue(oForm, "IDH_VRQSTT", sDateT)
                Else
                    '    setUFValue(oForm, "IDH_VRQSTF", "")
                    '    setUFValue(oForm, "IDH_VRQSTT", "")
                End If

                'setUFValue(oForm, "IDH_VRQSTF", "")
                'setUFValue(oForm, "IDH_VRQSTT", "")

                ''..Add Container Group - If Job filtered on ItemGroups filter Vehicle also on that ItemGroup 
                'oGridN.doAddFilterField("IDH_ITMGRP", "r.U_ItmGrp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)

                ''..If Job filtered on Item Code filter Vehicle also on that Item Code 
                'oGridN.doAddFilterField("IDH_ITMCD", "r.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

                Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "VEHGRID")
                oVehicleGrid.setRequiredFilter(getVehicleRequiredStr(oForm))
                oVehicleGrid.doReloadData()
                Dim iIndex As Integer = oVehicleGrid.doFieldIndex("VehReg")
                If iIndex <> 0 Then
                    oVehicleGrid.doApplyExpand(IDHAddOns.idh.controls.IDHGrid.NORMAL)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error reloading the Vehicle.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRVEH", {Nothing})
                'Finally
                '    oForm.Freeze(False)
            End Try
        End Sub

        'Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
        Protected Overrides Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean)
            Dim sDefStatus As String = Config.Parameter("VMDFSTA")
            If Not sDefStatus Is Nothing AndAlso sDefStatus.Length > 0 Then
                setUFValue(oForm, "IDH_HDSTA", sDefStatus)
            Else
                setUFValue(oForm, "IDH_HDSTA", "1,2,3,4,5,9")
            End If

            MyBase.doReLoadData(oForm, False) ', bIsFirst)
            doReloadVehicle(oForm)
            setWFValue(oForm, "ExpRow", -1)
            doClearSharedData(oForm)
        End Sub

        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            doSetFocus(oForm, "IDH_VEHREG")
        End Sub
        Private Sub doSizeGrids(ByVal oForm As SAPbouiCOM.Form)
            Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "VEHGRID")
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If Not oVehicleGrid Is Nothing Then
                oVehicleGrid.getItem().Width = 300
                oVehicleGrid.getItem().Height = oGridN.getItem().Height
            End If
        End Sub

        Public Sub doCheckVehicleRows(ByVal oForm As SAPbouiCOM.Form, ByVal sVehReg As String)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            Dim oDataTable As SAPbouiCOM.DataTable = oGridN.getDataTable()
            Dim iCol As Integer = oGridN.doIndexField("r.U_Lorry")
            Dim sJobVehReg As String
            For iRow As Integer = 0 To oGridN.getRowCount() - 1
                sJobVehReg = oGridN.doGetFieldValue(iCol, iRow)
                If sJobVehReg = sVehReg Then
                    oGridN.doSetFieldValue("'LINK'", iRow, "Y")
                Else
                    oGridN.doSetFieldValue("'LINK'", iRow, "N")
                End If
            Next
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim bRes As Boolean = MyBase.doItemEvent(oForm, pVal, BubbleEvent)

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_RESIZE Then
                If pVal.BeforeAction = False Then
                    doSizeGrids(oForm)
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK OrElse
                   pVal.EventType = SAPbouiCOM.BoEventTypes.et_MATRIX_COLLAPSE_PRESSED Then
                ''*** Using this and not the Custom Event bacause of the drag and drop function, 
                ''*** requiring the selection list before the button is realsed to register the normal selection
                Try
                    If pVal.BeforeAction = True Then
                        If pVal.ItemUID = "VEHGRID" Then
                            Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "VEHGRID")
                            Dim sVehReg As String = ""
                            Dim sVDriver As String = ""
                            Dim CarrCode As String = ""
                            Dim CarrName As String = ""
                            Dim iRow As Integer = pVal.Row
                            If iRow >= 0 Then
                                If oVehicleGrid.getExpandState() = IDHAddOns.idh.controls.IDHGrid.COLLAPSED Then
                                    If pVal.EventType = SAPbouiCOM.BoEventTypes.et_MATRIX_COLLAPSE_PRESSED Then
                                        If oVehicleGrid.getSBOGrid.Rows.IsExpanded(iRow) = False Then
                                            setWFValue(oForm, "ExpRow", iRow)
                                            iRow = iRow + 1

                                            Dim oSelected As SAPbouiCOM.SelectedRows = oVehicleGrid.getSBOGrid().Rows.SelectedRows
                                            If Not oSelected Is Nothing Then
                                                oSelected.Clear()
                                                oSelected.Add(iRow - 1)
                                            End If
                                            'oVehicleGrid.getSBOGrid().Rows.SelectedRows.Add(iRow)

                                            Dim iDataRow As Integer = oVehicleGrid.getSBOGrid().GetDataTableRowIndex(iRow)
                                            sVehReg = oVehicleGrid.doGetFieldValue("VehReg", iDataRow)
                                            sVDriver = oVehicleGrid.doGetFieldValue("Driver", iDataRow)
                                            ''MA Start 21-11-2014
                                            CarrCode = oVehicleGrid.doGetFieldValue("CARRCODE")
                                            CarrName = oVehicleGrid.doGetFieldValue("CARRNAME")
                                            'setWFValue(oForm, "VehData", sVehReg)
                                            doSetSelectedRegistration(oForm, sVehReg, sVDriver, CarrCode, CarrName)
                                            ''MA End 21-11-2014
                                        Else
                                            setWFValue(oForm, "ExpRow", -1)
                                        End If
                                    Else
                                        If oVehicleGrid.getSBOGrid.Rows.IsExpanded(iRow) = True Then
                                            Dim iExpRow As Integer = getWFValue(oForm, "ExpRow")
                                            If iExpRow = iRow Then
                                                setWFValue(oForm, "ExpRow", -1)
                                            End If
                                            oVehicleGrid.getSBOGrid().Rows.Collapse(iRow)
                                        Else
                                            Dim iExpRow As Integer = getWFValue(oForm, "ExpRow")

                                            If oVehicleGrid.getSBOGrid().Rows.IsLeaf(iRow) = False Then
                                                oVehicleGrid.getSBOGrid().Rows.Expand(iRow)
                                                setWFValue(oForm, "ExpRow", iRow)
                                                iRow = iRow + 1
                                                'oVehicleGrid.getSBOGrid().Rows.SelectedRows.Add(iRow)
                                            End If

                                            Dim iDataRow As Integer = oVehicleGrid.getSBOGrid().GetDataTableRowIndex(iRow)
                                            sVehReg = oVehicleGrid.doGetFieldValue("VehReg", iDataRow)
                                            sVDriver = oVehicleGrid.doGetFieldValue("Driver", iDataRow)
                                            ''MA Start 21-11-2014
                                            CarrCode = oVehicleGrid.doGetFieldValue("CARRCODE")
                                            CarrName = oVehicleGrid.doGetFieldValue("CARRNAME")
                                            'setWFValue(oForm, "VehData", sVehReg)
                                            doSetSelectedRegistration(oForm, sVehReg, sVDriver, CarrCode, CarrName)
                                            ''MA End 21-11-2014
                                            If iExpRow > -1 Then
                                                oVehicleGrid.getSBOGrid().Rows.Collapse(iExpRow)
                                                'setWFValue(oForm, "ExpRow", -1)
                                            End If

                                            doCheckVehicleRows(oForm, sVehReg)
                                        End If
                                    End If
                                Else
                                    oVehicleGrid.setCurrentLineByClick(iRow)
                                    sVehReg = oVehicleGrid.doGetFieldValue("VehReg")
                                    sVDriver = oVehicleGrid.doGetFieldValue("Driver")
                                    ''MA Start 21-11-2014
                                    CarrCode = oVehicleGrid.doGetFieldValue("CARRCODE")
                                    CarrName = oVehicleGrid.doGetFieldValue("CARRNAME")
                                    'setWFValue(oForm, "VehData", sVehReg)
                                    doSetSelectedRegistration(oForm, sVehReg, sVDriver, CarrCode, CarrName)
                                    ''MA End 21-11-2014
                                    doCheckVehicleRows(oForm, sVehReg)
                                End If
                            End If
                        End If
                    Else
                    End If
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing the item event - " & pVal.ItemUID & "->" & pVal.EventType)
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXITME", {pVal.ItemUID, pVal.EventType})
                End Try
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = True Then
                    If pVal.ItemUID = "VEHGRID" Then
                    End If
                Else
                    If pVal.ItemUID = "IDH_WOF" Then
                        MyBase.doReLoadData(oForm, True)
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_OK_MODE)
                    ElseIf pVal.ItemUID = "IDH_VEHRF" Then
                        doReloadVehicle(oForm)
                    ElseIf pVal.ItemUID = "IDH_VEHM" Then
                        If Config.ParameterAsBool("VMUSENEW", False) = False Then
                            goParent.doOpenForm("IDHVEAD", oForm, False)
                        Else
                            goParent.doOpenForm("IDH_VEHM", oForm, False)
                        End If
                    ElseIf pVal.ItemUID = "LINESGRID" Then
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        'Dim iCurRow As Integer = oGridN.getGrid().GetDataTableRowIndex(pVal.Row)
                        oGridN.setCurrentLineByClick(pVal.Row)
                        ' oGridN.setCurrentDataRowIndex(iCurRow)
                        If oGridN.doCheckIsSameCol(pVal.ColUID, "'LINK'") Then
                            Dim sVehReg As String = doGetSelectedRegistration(oForm)
                            Dim sVehicleDriver As String = "" 'getWFValue(oForm, "VDriver").ToString()
                            If getWFValue(oForm, "VDriver") IsNot Nothing Then
                                sVehicleDriver = getWFValue(oForm, "VDriver")
                            End If
                            Dim sCarrCode As String = "" 'getWFValue(oForm, "VDriver").ToString()
                            Dim sCarrName As String = ""
                            If getWFValue(oForm, "CarrCode") IsNot Nothing Then
                                sCarrCode = getWFValue(oForm, "CarrCode")
                            End If
                            If getWFValue(oForm, "CarrName") IsNot Nothing Then
                                sCarrName = getWFValue(oForm, "CarrName")
                            End If

                            If Not sVehReg Is Nothing Then
                                Try
                                    oForm.Freeze(True)
                                    'Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

                                    If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                                        oGridN.setCurrentLineByClick(pVal.Row)
                                        Dim sChecked As String = oGridN.doGetFieldValue("'LINK'") 'oGridN.doGetFieldValue("Col1")

                                        Dim sTVehReg As String = oGridN.doGetFieldValue("r.U_Lorry")
                                        'If sTVehReg Is Nothing OrElse sTVehReg.Equals(sVehReg) = False Then
                                        If sTVehReg Is Nothing OrElse sTVehReg.Equals(sVehReg) = False Then
                                            If sChecked = "Y" Then
                                                oGridN.doSetFieldValue("r.U_Lorry", sVehReg)
                                                oGridN.doSetFieldValue("r.U_Driver", sVehicleDriver)
                                                '....Realtime ImageChange

                                                'Dim sRDate As String = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_RDate"), True)
                                                'Dim sSDate As String = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_ASDate"), True)
                                                'Dim sEDate As String = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_AEDate"), True)

                                                Dim dRDate As DateTime = oGridN.doGetFieldValue("r.U_RDate")

                                                '....setDFValue(StartDate)
                                                'Dim dSDate As DateTime = oGridN.doGetFieldValue("r.U_ASDate")
                                                Dim dSDate As DateTime = DateTime.Now
                                                ''MA Start 19-11-2014 Issue#482
                                                dSDate = dRDate
                                                oGridN.doSetFieldValue("r.U_ASDate", dSDate)
                                                ''MA End 19-11-2014
                                                oGridN.doSetFieldValue("v.State", getTranslatedWord("Started"))

                                                ''MA 20-11-2014 Commented as here exception throws, Asked Louis, Louis replied: I'll check it
                                                'oGridN.doSetFieldValue("v.IMG", IDHAddOns.idh.addon.Base.doFixImagePath("Started.png"))
                                                ''MA 20-11-2014 Commented as here exception throws, Asked Louis, Louis replied: I'll check it

                                                ' IDHAddOns.idh.addon.Base.doFixImagePath( "Closed.png" ) 
                                                ' IDHAddOns.idh.addon.Base.doFixImagePath( "Late.png" )
                                                ' IDHAddOns.idh.addon.Base.doFixImagePath( "No Request.png" )
                                                ' IDHAddOns.idh.addon.Base.doFixImagePath( "Started.png" )
                                                ' IDHAddOns.idh.addon.Base.doFixImagePath( "Complete.png" )
                                                ' IDHAddOns.idh.addon.Base.doFixImagePath( "Waiting.png" )
                                                If Config.INSTANCE.getParameterAsBool("VSM3CRCD", False) = True Then
                                                    setSharedData(oForm, "CarrCd" & oGridN.doGetFieldValue("r.Code"), oGridN.doGetFieldValue("r.U_CarrCd"))
                                                    setSharedData(oForm, "CarrNm" & oGridN.doGetFieldValue("r.Code"), oGridN.doGetFieldValue("r.U_CarrNm"))
                                                    oGridN.doSetFieldValue("r.U_CarrCd", sCarrCode)
                                                    oGridN.doSetFieldValue("r.U_CarrNm", sCarrName)
                                                End If



                                                Dim dEDate As DateTime = oGridN.doGetFieldValue("r.U_AEDate")

                                                doUpdateVehicleToCap(oForm, sVehReg, dRDate, dSDate, dEDate, 1)
                                                doWarnInActiveBPnSites(oForm, oGridN)
                                                'Dim iExpRow As Integer = getWFValue(oForm, "ExpRow")
                                                'If iExpRow > 0 Then
                                                '    oGridN.getSBOGrid().SetCellFocus(iExpRow, 0)
                                                'End If
                                            End If
                                        Else
                                            If sChecked = "N" Then
                                                oGridN.doSetFieldValue("r.U_Lorry", "")
                                                oGridN.doSetFieldValue("r.U_Driver", "")
                                                '...cleareStartDate
                                                '....Realtime ImageChange

                                                'Dim sRDate As String = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_RDate"), True)
                                                'Dim sSDate As String = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_ASDate"), True)
                                                'Dim sEDate As String = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_AEDate"), True)
                                                Dim dRDate As DateTime = oGridN.doGetFieldValue("r.U_RDate")
                                                Dim dSDate As DateTime = oGridN.doGetFieldValue("r.U_ASDate")
                                                Dim dEDate As DateTime = oGridN.doGetFieldValue("r.U_AEDate")
                                                doUpdateVehicleToCap(oForm, sVehReg, dRDate, dSDate, dEDate, -1)

                                                oGridN.doSetFieldValue("r.U_ASDate", "")
                                                oGridN.doSetFieldValue("v.State", getTranslatedWord("Late"))
                                                ''MA 20-11-2014 Commented as here exception throws, Asked Louis, Louis replied: I'll check it
                                                'oGridN.doSetFieldValue("v.IMG", IDHAddOns.idh.addon.Base.doFixImagePath("Late.png"))
                                                ''MA 20-11-2014 Commented as here exception throws, Asked Louis, Louis replied: I'll check it
                                                ''MA Start 28-11-2014
                                                Dim CarrCd = getSharedData(oForm, "CarrCd" & oGridN.doGetFieldValue("r.Code"))
                                                If CarrCd IsNot Nothing Then
                                                    oGridN.doSetFieldValue("r.U_CarrCd", CarrCd.ToString)
                                                End If
                                                Dim CarrNM = getSharedData(oForm, "CarrNm" & oGridN.doGetFieldValue("r.Code"))
                                                If CarrNM IsNot Nothing Then
                                                    oGridN.doSetFieldValue("r.U_CarrNm", CarrNM.ToString)
                                                End If
                                                ''MA End 28-11-2014

                                                'Dim iExpRow As Integer = getWFValue(oForm, "ExpRow")
                                                'If iExpRow > 0 Then
                                                '    oGridN.getSBOGrid().SetCellFocus(iExpRow, 0)
                                                'End If
                                            End If
                                        End If
                                    End If
                                Catch ex As Exception
                                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the registration..")
                                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSREG", {Nothing})
                                Finally
                                    oForm.Freeze(False)
                                End Try
                            Else
                                If getUFValue(oForm, "IDH_AFOJR").Equals("Y") Then
                                    'Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                                    If oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                                        oGridN.setCurrentLineByClick(pVal.Row)
                                        Dim sRoute As String = oGridN.doGetFieldValue("r.U_IDHRTCD")
                                        If Not sRoute Is Nothing AndAlso sRoute.Length > 0 Then
                                            setUFValue(oForm, "IDH_VEHRT", sRoute)
                                        Else
                                            setUFValue(oForm, "IDH_VEHRT", "")
                                        End If
                                        doReloadVehicle(oForm)
                                    End If
                                End If
                            End If
                        End If
                        'setWFValue(oForm, "JobData", Nothing)
                        'setWFValue(oForm, "VehData", Nothing)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_REQSTF" Then
                        If Config.INSTANCE.getParameterAsBool("VSEDFSD", True) Then
                            setUFValue(oForm, "IDH_REQSTT", getUFValue(oForm, "IDH_REQSTF"))
                        End If
                    End If
                End If
            End If
            Return True
        End Function
        Protected Overrides Sub doWarnInActiveBPnSites(ByVal oForm As SAPbouiCOM.Form, oGridN As IDHGrid)
            Try
                If Config.ParameterAsBool("SHWINACW", False) = False Then
                    Exit Sub
                End If
                Dim sValue As String = ""
                Dim sBPCodes As String() '= {""}
                Dim sAddresses As String() '= {""}
                If oForm.TypeEx = "IDHVECR3" Then
                    sBPCodes = {"Customer", "e.U_CardCd", "SLicSp", "r.U_SLicSp", "Carrier Code", "r.U_CarrCd"}

                    sAddresses = {"Customer Address", "e.U_CardCd", "e.U_Address"}
                Else
                    Exit Sub
                End If
                Dim sLabel, sBPField, sBPCode, sAddressField, sBPColName As String
                Dim oBPParam() As String = {"", ""}
                Dim oAddressParam() As String = {"", "", ""}

                For iitem As Int32 = 0 To sBPCodes.Count - 1 Step 2
                    sLabel = sBPCodes(iitem)
                    sBPField = sBPCodes(iitem + 1)
                    sValue = oGridN.doGetFieldValue(sBPField)
                    If sValue IsNot Nothing AndAlso sValue.Trim <> "" AndAlso Not Config.INSTANCE.doCheckBPActive(sValue, DateTime.Now) Then
                        oBPParam(0) = com.idh.bridge.Translation.getTranslatedWord(sLabel)
                        oBPParam(1) = sValue
                        doWarnMess(Messages.getGMessage("WRINACBP", oBPParam), SAPbouiCOM.BoMessageTime.bmt_Short)
                    End If
                Next
                For iitem As Int32 = 0 To sAddresses.Count - 1 Step 3
                    sLabel = sAddresses(iitem)
                    sBPField = sAddresses(iitem + 1)
                    sAddressField = sAddresses(iitem + 2)

                    sValue = oGridN.doGetFieldValue(sAddressField)
                    sBPCode = oGridN.doGetFieldValue(sBPField)
                    sBPColName = oGridN.Columns.Item(oGridN.doFieldIndex(sBPField)).TitleObject.Caption
                    If sValue IsNot Nothing AndAlso sValue.Trim <> "" AndAlso Not Config.INSTANCE.doCheckBPAddressActive(sBPCode, sValue) Then
                        oAddressParam(0) = com.idh.bridge.Translation.getTranslatedWord(sLabel)
                        oAddressParam(1) = sValue
                        oAddressParam(2) = sBPColName
                        doWarnMess(Messages.getGMessage("WRINACAD", oAddressParam), SAPbouiCOM.BoMessageTime.bmt_Short)
                    End If
                Next
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "doWarnInActiveBPnSites", {Nothing})
            End Try
        End Sub

        ''MA Start 21-11-2014
        Private Sub doSetSelectedRegistration(ByVal oForm As SAPbouiCOM.Form, ByVal sReg As String, ByVal sVDriver As String,
                                              sCarrCode As String, sCarrName As String)
            setWFValue(oForm, "VehData", sReg)
            setWFValue(oForm, "VDriver", sVDriver)

            setWFValue(oForm, "CarrCode", sCarrCode)
            setWFValue(oForm, "CarrName", sCarrName)

            If sReg Is Nothing Then
                sReg = ""
            End If
            setItemValue(oForm, "IDH_SELREG", sReg)
        End Sub
        ''MA End 21-11-2014

        Private Function doGetSelectedRegistration(ByVal oForm As SAPbouiCOM.Form) As String
            Return getWFValue(oForm, "VehData")
        End Function

        'Private Sub doUpdateVehicleToCap(ByVal oForm As SAPbouiCOM.Form, ByVal sReg As String, ByVal sRDate As String, ByVal sSDate As String, ByVal sEDate As String, ByVal iQty As Integer)
        Private Sub doUpdateVehicleToCap(ByVal oForm As SAPbouiCOM.Form, ByVal sReg As String, ByVal dRDate As DateTime, ByVal dSDate As DateTime, ByVal dEDate As DateTime, ByVal iQty As Integer)
            'If Not sEDate Is Nothing AndAlso sEDate.Length > 0 Then
            'Exit Sub
            'End If
            Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "VEHGRID")
            Dim sTVehReg As String
            'Dim sTRDate As String
            Dim iTDJ As Integer
            Dim iTWJ As Integer
            Dim iTBJ As Integer
            Dim iCap As Integer = 1
            Dim sImg As String

            Dim iIndex As Integer
            Dim bFound As Boolean = False

            Dim bVehFound As Boolean = False
            Dim iRowCount As Integer = oVehicleGrid.getRowCount()

            'First find the Vehcile record for this date
            For iIndex = 0 To iRowCount - 1
                sTVehReg = oVehicleGrid.doGetFieldValue("VehReg", iIndex)
                If Not sTVehReg Is Nothing AndAlso sTVehReg.Equals(sReg) Then
                    bVehFound = True
                    iCap = oVehicleGrid.doGetFieldValue("DayCap", iIndex)

                    Dim dTRDate As DateTime = oVehicleGrid.doGetFieldValue("ReqDate", iIndex)
                    'sTRDate = goParent.doDateToStr()
                    'If sTRDate Is Nothing OrElse sTRDate = "19000101" OrElse (Not sTRDate Is Nothing AndAlso sTRDate.Equals(sRDate)) Then
                    If Dates.isValidDate(dTRDate) = False OrElse (Dates.isValidDate(dRDate) AndAlso Dates.CompareDatePartOnly(dTRDate, dRDate) = 0) Then
                        'If (sTRDate Is Nothing OrElse sTRDate = "19000101") Then
                        If Dates.isValidDate(dTRDate) = False Then
                            'oVehicleGrid.doSetFieldValue("ReqDate", iIndex, com.idh.utils.dates.doStrToDate(sRDate))
                            oVehicleGrid.doSetFieldValue("ReqDate", iIndex, dRDate)
                            oVehicleGrid.doSetFieldValue("DayCap", iIndex, iCap)
                            iTDJ = 0
                        Else
                            iTDJ = oVehicleGrid.doGetFieldValue("Total", iIndex)
                        End If
                        iTDJ += iQty

                        If iTDJ < 1 Then
                            'oVehicleGrid.doRemoveRow(iIndex)
                            iTDJ = 0
                            'Exit For
                            oVehicleGrid.doSetFieldValue("Started", iIndex, 0)
                            oVehicleGrid.doSetFieldValue("NotStarted", iIndex, 0)
                            Dim oDate As DateTime = New DateTime(1900, 1, 1)
                            oVehicleGrid.doSetFieldValue("ReqDate", iIndex, oDate)
                        Else
                            iTWJ = oVehicleGrid.doGetFieldValue("NotStarted", iIndex)
                            iTBJ = oVehicleGrid.doGetFieldValue("Started", iIndex)

                            'If Not sSDate Is Nothing AndAlso sSDate.Length > 0 Then
                            If Dates.isValidDate(dSDate) Then
                                oVehicleGrid.doSetFieldValue("Started", iIndex, iTBJ + 1)
                            Else
                                oVehicleGrid.doSetFieldValue("NotStarted", iIndex, iTWJ + 1)
                            End If
                        End If

                        oVehicleGrid.doSetFieldValue("Total", iIndex, iTDJ)

                        Dim sPerc As String = Config.Parameter("VSFULLW")
                        If sPerc Is Nothing OrElse sPerc.Length = 0 Then
                            sPerc = "50"
                        End If

                        Dim dPerc As Double = ToDouble(sPerc)
                        Dim sFull, sNearly, sAvailable As String
                        If Config.ParameterWithDefault("IMGPATH", "").Trim = "" Then
                            sFull = IDHAddOns.idh.addon.Base.doFixImagePath("full.png")
                            sNearly = IDHAddOns.idh.addon.Base.doFixImagePath("nearly.png")
                            sAvailable = IDHAddOns.idh.addon.Base.doFixImagePath("available.png")

                        Else
                            Dim sImgPath As String = Config.ParameterWithDefault("IMGPATH", "").Trim
                            If Not sImgPath.EndsWith("\") Then
                                sImgPath &= "\"
                            End If
                            sFull = sImgPath & ("full.png")
                            sNearly = sImgPath & ("nearly.png")
                            sAvailable = sImgPath & ("available.png")
                        End If
                        If iTDJ >= iCap Then
                            sImg = sFull
                        ElseIf iTDJ > (iCap * dPerc / 100) Then
                            sImg = sNearly
                        Else
                            sImg = sAvailable
                        End If
                        oVehicleGrid.doSetFieldValue("CAP", iIndex, sImg)

                        bFound = True
                        Exit For
                    End If
                Else
                    If bVehFound AndAlso oVehicleGrid.doGetAddedRows().Count = 0 Then
                        If oVehicleGrid.getGridControl().getOrderString().StartsWith(" ORDER BY VehReg") Then
                            Exit For
                        End If
                    End If
                    bVehFound = False
                End If
            Next
            If iQty > 0 Then
                If bFound = False Then
                    oVehicleGrid.doAddRow()
                    iIndex = oVehicleGrid.getLastRowIndex()
                    oVehicleGrid.doSetFieldValue("VehReg", iIndex, sReg)
                    'oVehicleGrid.doSetFieldValue("ReqDate", iIndex, com.idh.utils.dates.doStrToDate(sRDate))
                    oVehicleGrid.doSetFieldValue("ReqDate", iIndex, dRDate)
                    oVehicleGrid.doSetFieldValue("DayCap", iIndex, iCap)

                    oVehicleGrid.doSetFieldValue("Total", iIndex, 1)
                    'If Not sSDate Is Nothing AndAlso sSDate.Length > 0 Then
                    If Dates.isValidDate(dSDate) Then
                        oVehicleGrid.doSetFieldValue("Started", iIndex, 1)
                        oVehicleGrid.doSetFieldValue("NotStarted", iIndex, 0)
                    Else
                        oVehicleGrid.doSetFieldValue("NotStarted", iIndex, 1)
                        oVehicleGrid.doSetFieldValue("Started", iIndex, 0)
                    End If

                    Dim sPerc As String = Config.Parameter("VSFULLW")
                    If sPerc Is Nothing OrElse sPerc.Length = 0 Then
                        sPerc = "50"
                    End If
                    Dim dPerc As Double = ToDouble(sPerc)
                    Dim sFull, sNearly, sAvailable As String
                    If Config.ParameterWithDefault("IMGPATH", "").Trim = "" Then
                        sFull = IDHAddOns.idh.addon.Base.doFixImagePath("full.png")
                        sNearly = IDHAddOns.idh.addon.Base.doFixImagePath("nearly.png")
                        sAvailable = IDHAddOns.idh.addon.Base.doFixImagePath("available.png")

                    Else
                        Dim sImgPath As String = Config.ParameterWithDefault("IMGPATH", "").Trim
                        If Not sImgPath.EndsWith("\") Then
                            sImgPath &= "\"
                        End If
                        sFull = sImgPath & ("full.png")
                        sNearly = sImgPath & ("nearly.png")
                        sAvailable = sImgPath & ("available.png")
                    End If
                    If 1 >= iCap Then
                        sImg = sFull
                    ElseIf 1 > (iCap * dPerc / 100) Then
                        sImg = sNearly
                    Else
                        sImg = sAvailable
                    End If
                    oVehicleGrid.doSetFieldValue("CAP", iIndex, sImg)

                    oVehicleGrid.getSBOGrid().Rows.CollapseAll()
                    Dim iExtRow As Integer = getWFValue(oForm, "ExpRow")
                    If iExtRow > -1 Then
                        oVehicleGrid.getSBOGrid().Rows.Expand(iExtRow)
                    End If
                End If
            End If
            doSetUpdate(oForm)
        End Sub
    End Class
End Namespace


