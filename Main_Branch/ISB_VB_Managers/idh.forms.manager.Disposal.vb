Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.bridge.reports
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge
Imports com.idh.bridge.data
Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User
Imports com.idh.bridge.action

Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.resources

Namespace idh.forms.manager
    Public Class Disposal
        Inherits IDHAddOns.idh.forms.Base

        Private ghStatusChanged As New ArrayList
        Shared moTotalFields() As String = {
        "IDH_CstWgt", "IDH_SubAD", "IDH_SbADCt", "IDH_Total",
        "IDH_DisAmt", "IDH_AddEx", "IDH_TaxAmt",
        "IDH_RdWgt",
        "IDH_TAdCst", "IDH_TAdChg", "IDH_TxAmCo",
        "IDH_TotChg", "IDH_ChgDed"
    }
        Shared moTotalFields_Charge() As String = {
                 "IDH_SubAD", "IDH_TAdChg", "IDH_ChgDed", "IDH_TaxAmt", "IDH_Total", "IDH_DisAmt"
             }
        Shared moTotalFields_Cost() As String = {
           "IDH_SbADCt", "IDH_TAdCst", "IDH_AddEx", "IDH_TxAmCo", "IDH_TotChg"
       }

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer, ByVal sType As String, ByVal sSRF As String, ByVal sTitle As String)
            MyBase.New(oParent, (If(sType Is Nothing, "IDHDOM", sType)), "IDHPS", iMenuPosition, (If(sSRF Is Nothing, "Disposal Manager.srf", sSRF)), True, True, False, (If(sTitle Is Nothing, "Disposal Order Manager", sTitle)), load_Types.idh_LOAD_NORMAL)
        End Sub

        Public Sub doGetStatus(ByVal oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_DELETE) Then
                Try
                    doFillCombo(oForm, "IDH_HDSTA", "[@IDH_WRSTATUS]", "Code", "Name", Nothing, "CAST(Code As Numeric)", getTranslatedWord("Default"), "1,2,3,4,5")
                Catch ex As Exception
                End Try
            Else
                Try
                    Dim oItem As SAPbouiCOM.Item
                    oItem = oForm.Items.Item("IDH_HDSTA")
                    Dim oCombo As SAPbouiCOM.ComboBox
                    oCombo = oItem.Specific()

                    Dim oValidValues As SAPbouiCOM.ValidValues
                    oValidValues = oCombo.ValidValues
                    doClearValidValues(oValidValues)

                    oValidValues.Add("1,2,3,4,5", com.idh.bridge.lookups.FixedValues.getStatusOpen())
                    oValidValues.Add("9", com.idh.bridge.lookups.FixedValues.getStatusClosed())
                Catch ex As Exception
                End Try
            End If
        End Sub

        Private Sub doRowStatusCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oRowStaCombo As SAPbouiCOM.ComboBox
            oRowStaCombo = oForm.Items.Item("IDH_ROWSTA").Specific
            oRowStaCombo.ValidValues.Add("", getTranslatedWord("All"))
            oRowStaCombo.ValidValues.Add("!" & com.idh.bridge.lookups.FixedValues.getStatusDeleted(), com.idh.bridge.lookups.FixedValues.getStatusOpen())
            oRowStaCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getStatusDeleted(), com.idh.bridge.lookups.FixedValues.getStatusDeleted())
            oRowStaCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getStatusCanceled(), com.idh.bridge.lookups.FixedValues.getStatusCanceled())
        End Sub

        '*** Read input params
        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            If getHasSharedData(oForm) Then
                Dim oGridN As FilterGrid
                oGridN = FilterGrid.getInstance(oForm, "LINESGRID")

                Dim iIndex As Integer
                For iIndex = 0 To oGridN.getGridControl().getFilterFields().Count - 1
                    Dim oField As com.idh.controls.strct.FilterField = oGridN.getGridControl().getFilterFields().Item(iIndex)
                    Dim sFieldName As String = oField.msFieldName
                    Dim sFieldValue As String = getParentSharedData(oForm, sFieldName)
                    Try
                        setUFValue(oForm, sFieldName, sFieldValue)
                    Catch ex As Exception
                    End Try
                Next
            End If
        End Sub

        Protected Overridable Sub doSetGridFilters(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            oGridN.doAddFilterField("IDH_ORDNO", "r.U_JobNr", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 30)
            oGridN.doAddFilterField("IDH_JOBTYP", "r.U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 30)

            oGridN.doAddFilterField("IDH_BOOSTF", "e.U_BDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_BOOSTT", "e.U_BDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)

            oGridN.doAddFilterField("IDH_CUST", "e.U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_NAME", "e.U_CardNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_PROD", "r.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ITMGRP", "r.U_ItmGrp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)

            oGridN.doAddFilterField("IDH_WSCD", "r.U_WasCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WSDC", "r.U_WasDsc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_VEHCOD", "r.U_LorryCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_VEHREG", "r.U_Lorry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DRIVER", "r.U_Driver", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DISPAD", "e.U_SAddress", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_STATUS", "r.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, Config.Parameter("DOMSOS")) '"!Ordered")
            oGridN.doAddFilterField("IDH_REF", "r.U_CustRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_USER", "r.U_User", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_BRANCH", "r.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)

            oGridN.doAddFilterField("IDH_ROWSTA", "r.U_RowSta", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_HDSTA", "e.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 30)

            'New filters on Reference numbers and Maximo No on WOR 
            oGridN.doAddFilterField("IDH_PROREF", "r.U_ProRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DSPREF", "r.U_SiteRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SUPREF", "r.U_SupRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_MAXIMO", "r.U_MaximoNum", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
        End Sub

        Public Sub doAddAdditionalRequiredListFields(ByVal oGridN As OrderRowGrid)
            oGridN.doAddListFieldNotPinned("e." & IDH_DISPORD._Status, "WO Status", False, 0, Nothing, Nothing)

            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._Status, "Sales Status", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._PStat, "Purchase Status", False, 0, Nothing, Nothing)

            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._SAINV, "Sales Invoice", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._SLPO, "Skip licence PO", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._SAORD, "Sales Order", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._GRIn, "Goods Receipt Incomming", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._TIPPO, "Tipping PO", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._JOBPO, "Haulage PO", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._ProPO, "Producer PO", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._CustGR, "Customer Goods Receipt", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._ProGRPO, "Producer Goods Receipt PO", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._SODlvNot, "Do Stock Movement", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._Jrnl, "Journal Entry", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._TPCN, "Disposal Purchase Credit Note", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._TCCN, "Customer Credit Note", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._WHTRNF, getTranslatedWord("Stock Transfer"), False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._ReqArch, "Request Archiving", False, 0, Nothing, Nothing)

            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._JCost, "Total Order Cost", False, 0, Nothing, "TOrdCost")
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._VtCostAmt, "Purchase Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._RowSta, "Row Status", False, -1, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._AddCost, "Additional Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._AddCharge, "Additional Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListFieldNotPinned("r." & IDH_DISPROW._ValDed, "Charge Deduction", False, 0, Nothing, Nothing)

            oGridN.doFinalize()
        End Sub

        Protected Overridable Sub doTheGridLayout(ByVal oGridN As OrderRowGrid)
            If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                Dim sFormTypeId As String = oGridN.getSBOForm().TypeEx
                Dim oFormSettings As IDH_FORMSET = getWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId)
                If oFormSettings Is Nothing Then
                    oFormSettings = New IDH_FORMSET()
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "") 'doGetCapacityQuery())
                    setWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId, oFormSettings)

                    oFormSettings.doAddFieldToGrid(oGridN)

                    doSetListFields(oGridN)
                    doAddAdditionalRequiredListFields(oGridN)

                    'If oFormSettings.SkipFormSettings = False Then
                    '    oFormSettings.doSetGrid(oGridN)
                    'End If
                    oFormSettings.doSyncDB(oGridN)
                Else
                    If oFormSettings.SkipFormSettings Then
                        doSetListFields(oGridN)
                        doAddAdditionalRequiredListFields(oGridN)
                    Else
                        'oFormSettings.doSetGrid(oGridN)
                        oFormSettings.doAddFieldToGrid(oGridN)
                    End If
                End If
            Else
                doSetListFields(oGridN)
                doAddAdditionalRequiredListFields(oGridN)
            End If
        End Sub

        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            oGridN.doAddListField("r.U_Status", "Sales Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_PStat", "Purchase Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobNr", "Order No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Row No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_SAINV", "Invoice", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Invoice)
            oGridN.doAddListField("r.U_SAORD", "Sales Order", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Order)
            oGridN.doAddListField("r.U_TipPO", "Tipping PO", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder)
            oGridN.doAddListField("r.U_JOBPO", "Haulage PO", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder)
            oGridN.doAddListField("r.U_ProPO", "Producer PO", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder)

            oGridN.doAddListField("r.U_GRIn", "Goods Receipt In", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_GoodsReceipt)
            oGridN.doAddListField("r.U_ProGRPO", "Producer GR PO", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_GoodsReceiptPO)
            oGridN.doAddListField("r.U_SODlvNot", "Delivery note", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_DeliveryNotes)

            If Config.INSTANCE.getParameterAsBool("NEWDOJRN", False) = True Then
                oGridN.doAddListField("r.U_JStat", "Journal Status", False, -1, Nothing, Nothing)
                oGridN.doAddListField("r.U_Jrnl", "Journals", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_JournalPosting)
            End If

            oGridN.doAddListField("r.U_WHTRNF", getTranslatedWord("Stock Transfer"), False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_StockTransfers)

            oGridN.doAddListField("r.U_WROrd", "WO No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_WRRow", "WO Row No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Lorry", "Vehicle Reg.", False, 50, Nothing, Nothing)
            oGridN.doAddListField("r.U_VehTyp", "Vehicle", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_CCardCd", "Carrier Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e.U_CCardNM", "Carrier Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e." + IDH_DISPORD._PCardCd, getTranslatedWord("Producer Code"), False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e." + IDH_DISPORD._PCardNM, getTranslatedWord("Producer Name"), False, 70, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, 90, Nothing, Nothing, -1)
            oGridN.doAddListField("r.U_ConNum", "Consignment Number", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 50, Nothing, Nothing)
            oGridN.doAddListField("r.U_RdWgt", "Read Weight", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Charge Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_UOM", "Charge UOM", False, -1, Nothing, Nothing)

            ''MA Start 03-11-2014 Issue#417 Issue#418
            Dim bShowCost As Boolean = True, bShowCharge As Boolean = True
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso
              Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
                bShowCharge = True
            Else
                bShowCharge = False
            End If
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso
               Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) Then
                bShowCost = True
            Else
                bShowCost = False
            End If
            'If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) Then
            '    oGridN.doAddListField("r.U_TCharge", "Tip Charge", False, -1, Nothing, Nothing)
            '    oGridN.doAddListField("r.U_TCTotal", "Tip Total", False, -1, Nothing, Nothing)
            '    oGridN.doAddListField("CAST(((U_TCTotal + U_Price) - r.U_DisAmt) As Numeric(9,2))", "Sub After Discount", False, -1, Nothing, "SubAftDisc")
            '    'oGridN.doAddListField("(U_TCTotal +(U_CusQty * U_CusChr)) - r.U_DisAmt", "Sub After Discount", False, -1, Nothing, "SubBefDisc")
            '    oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            '    oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 30, Nothing, Nothing)
            '    oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            '    oGridN.doAddListField("r.U_Total", "Total Charge", False, -1, Nothing, Nothing)
            'End If
            If bShowCharge Then
                oGridN.doAddListField("r.U_TCharge", "Tip Charge", False, -1, Nothing, Nothing)
                oGridN.doAddListField("r.U_TCTotal", "Tip Total", False, -1, Nothing, Nothing)
                oGridN.doAddListField("CAST(((U_TCTotal + U_Price) - r.U_DisAmt) As Numeric(19,2))", "Sub After Discount", False, -1, Nothing, "SubAftDisc")
                'oGridN.doAddListField("(U_TCTotal +(U_CusQty * U_CusChr)) - r.U_DisAmt", "Sub After Discount", False, -1, Nothing, "SubBefDisc")
                oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            End If
            If bShowCost Then
                oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 30, Nothing, Nothing)
            End If
            If bShowCharge Then
                oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
                oGridN.doAddListField("r.U_Total", "Total Charge", False, -1, Nothing, Nothing)
            End If
            ''MA End 03-11-2014 Issue#417 Issue#418

            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_PayStat", "Payment Status", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_CustRef", "Customer Ref No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Tip", "Disposal Site", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_SAddress", "Disp.Sit.Add", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_Address", "Order Address", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_JCost", "Total Order Cost", False, -1, Nothing, "TOrdCost")

            oGridN.doAddListField("r.U_LorryCd", "Vehicle Code.", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("r.U_PStat", "Purchase Status", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            'oGridN.doAddListField("e.U_CardNM", "Customer Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Street", "Street", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Block", "Block", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RDate", "Req. Start", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTime", "Req. Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_ASDate", "Act. Start", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASTime", "Act. Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act. End", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AETime", "Act ETime", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_ItmGrp", "Container Grp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("r.U_Serial", "Item Serial No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, 30, Nothing, Nothing)

            ''MA Start 03-10-2014 Issue#417 Issue#418
            'If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) Then
            '    oGridN.doAddListField("r.U_TipCost", "TipCost", False, 30, Nothing, Nothing)
            '    oGridN.doAddListField("r.U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)
            '    oGridN.doAddListField("r.U_TipTot", "TipTot", False, 30, Nothing, Nothing)

            '    oGridN.doAddListField("r.U_Price", "Price", False, 0, Nothing, Nothing)
            '    oGridN.doAddListField("r.U_SLicCh", "SLicCh", False, 0, Nothing, Nothing)
            'End If
            If bShowCost Then
                oGridN.doAddListField("r.U_TipCost", "TipCost", False, 30, Nothing, Nothing)
                oGridN.doAddListField("r.U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)
                oGridN.doAddListField("r.U_TipTot", "TipTot", False, 30, Nothing, Nothing)
            End If
            If bShowCharge Then
                oGridN.doAddListField("r.U_Price", "Price", False, 0, Nothing, Nothing)
                oGridN.doAddListField("r.U_SLicCh", "SLicCh", False, 0, Nothing, Nothing)
            End If
            ''MA End 03-10-2014 Issue#417 Issue#418

            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PARCPT", "Payment", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Receipt)

            oGridN.doAddListField("r.U_CCNum", "CC Num.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CCType", "CC Type", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CCStat", "CC Status", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtRt", "Tipping Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtRt", "Haulage Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtRt", "Tipping Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtRt", "Supplier Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtRt", "Haulage Vat Purchase Rate", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtGrp", "Tipping Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtGrp", "Haulage Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtGrp", "Tipping Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtGrp", "Supplier Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtGrp", "Haulage Vat Purchase Group", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)

            '## MA Start 22-04-2014
            oGridN.doAddListField("r.U_AddCost", "Additional Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddCharge", "Additional Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ValDed", "Charge Deduction", False, 0, Nothing, Nothing)
            '## MA End 22-04-2014
            '## MA Start 27-07-2015 Issue#867
            oGridN.doAddListField("r.U_PROINV", getTranslatedWord("AP Invoice"), False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseInvoice)
            oGridN.doAddListField("r.U_SAINV", getTranslatedWord("AR Invoice"), False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Invoice)
            '## MA End 27-07-2015 Issue#867
        End Sub

        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                oForm.Title = gsTitle

                Dim oGridN As OrderRowGrid = New OrderRowGrid(Me, oForm, "LINESGRID", 7, 66, IIf(gsType = "IDHDOM", 990, 785), IIf(gsType = "IDHDOM", 285, 252), "r") '252

                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

                oForm.EnableMenu(Config.NAV_ADD, False)
                oForm.EnableMenu(Config.NAV_FIND, False)
                oForm.EnableMenu(Config.NAV_FIRST, False)
                oForm.EnableMenu(Config.NAV_NEXT, False)
                oForm.EnableMenu(Config.NAV_PREV, False)
                oForm.EnableMenu(Config.NAV_LAST, False)

                Dim fSettings As SAPbouiCOM.FormSettings
                fSettings = oForm.Settings
                fSettings.Enabled = False

                doAddTotalFields(oForm)

                Dim oItem As SAPbouiCOM.Item
                setVisible(oForm, "IDH_CERT", False)
                oItem = doAddUFCheck(oForm, "IDH_RLBILL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", Nothing)
                BubbleEvent = False
            End Try
        End Sub

        Public Sub doAddTotalFields(ByVal oForm As SAPbouiCOM.Form)
            doAddUF(oForm, "IDH_CstWgt", SAPbouiCOM.BoDataType.dt_QUANTITY, 20, False, False)
            doAddUF(oForm, "IDH_SubAD", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_SbADCt", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_Total", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_DisAmt", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_AddEx", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_TaxAmt", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)

            doAddUF(oForm, "IDH_RdWgt", SAPbouiCOM.BoDataType.dt_QUANTITY, 20, False, False)
            doAddUF(oForm, "IDH_TAdCst", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_TAdChg", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_TxAmCo", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_TotChg", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
            doAddUF(oForm, "IDH_ChgDed", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)
        End Sub


        Public Overridable Sub setTables(ByVal oGridN As UpdateGrid)
            oGridN.doAddGridTable(New GridTable("@IDH_DISPROW", "r", "Code", True, True), True)
            oGridN.doAddGridTable(New GridTable("@IDH_DISPORD", "e", "Code", True, True))
        End Sub

        Public Overridable Function getListRequiredStr() As String
            'Return "r.U_JobNr = e.Code And e.U_Status In ('1','2','3','4','5') "
            Return "r.U_JobNr = e.Code "
        End Function

        Public Overridable Function getListOrderStr() As String
            Return "e.U_BDate, r.Code" 'CAST(r.Code As NUMERIC) "
        End Function

        Public Sub doHideTotals(ByVal oForm As SAPbouiCOM.Form)
            ''MA Start 03-11-2014 Issue#417 Issue#418
            If (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) = False) OrElse (Config.INSTANCE.getParameterAsBool("HDTOTALS", False) = True) Then
                Dim oItem As SAPbouiCOM.Item
                Dim iBlankColor As Integer = 14930874
                For Each sItem As String In moTotalFields_Cost
                    oItem = oForm.Items.Item(sItem)
                    oItem.ForeColor = iBlankColor
                    oItem.BackColor = iBlankColor
                Next
            End If
            If (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) = False) OrElse (Config.INSTANCE.getParameterAsBool("HDTOTALS", False) = True) Then
                Dim oItem As SAPbouiCOM.Item
                Dim iBlankColor As Integer = 14930874
                For Each sItem As String In moTotalFields_Charge
                    oItem = oForm.Items.Item(sItem)
                    oItem.ForeColor = iBlankColor
                    oItem.BackColor = iBlankColor
                Next
            End If
            ''MA End 03-11-2014 Issue#417 Issue#418
            'If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) Then
            '    doSetVisible(oForm, True, moTotalFields)
            'Else
            '    doSetVisible(oForm, False, moTotalFields)
            'End If
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            doHideTotals(oForm)

            oForm.Top = goParent.goApplication.Desktop.Height() - (oForm.Height + 125)
            oForm.Left = 200


            doBranchCombo(oForm)

            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New OrderRowGrid(Me, oForm, "LINESGRID", "r")
            End If
            oGridN.msWR1OrdType = "DO"
            oGridN.doSetDeleteActive(True)

            doSetGridFilters(oGridN)

            setTables(oGridN)
            'oGridN.setTableValue(getListTableStr())
            'oGridN.setKeyField("r.Code")

            oGridN.setOrderValue(getListOrderStr())
            oGridN.setRequiredFilter(getListRequiredStr())

            doGetStatus(oForm)
            doRowStatusCombo(oForm)

            Dim bHasParams As Boolean = False
            If getHasSharedData(oForm) Then
                doReadInputParams(oForm)
            End If

            If bHasParams = False Then
                oGridN.setInitialFilterValue("r.U_JobNr = '-100'")
            End If

            Dim sDefStatus As String = Config.Parameter("DOMDFSTA")
            If Not sDefStatus Is Nothing AndAlso sDefStatus.Length > 0 Then
                setUFValue(oForm, "IDH_HDSTA", sDefStatus)
            Else
                setUFValue(oForm, "IDH_HDSTA", "1,2,3,4,5")
            End If

            setUFValue(oForm, "IDH_ROWSTA", "!" & com.idh.bridge.lookups.FixedValues.getStatusDeleted())

            oGridN.doSetDoCount(True)
            oGridN.doSetBlankLine(False)

            doTheGridLayout(oGridN)
            'doSetListFields(oGridN)
            'doAddAdditionalRequiredListFields(oGridN)

            Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_ROWSTA")
            If oItem.Visible = True Then
                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_DELETE) Then
                    oItem.Left = oForm.Items.Item("IDH_HDSTA").Left + oForm.Items.Item("IDH_HDSTA").Width + 1 ' 451

                    oItem = oForm.Items.Item("IDH_HDSTA")
                    oItem.Width = 39
                Else
                    oItem.Left = -100

                    oItem = oForm.Items.Item("IDH_HDSTA")
                    oItem.Width = 80
                End If
            End If
            'oGridN.doFinalize()

            'USA Release 
            'Make the Branch and User fields set to default as per logged in user 
            If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                Try
                    Dim oCmb As SAPbouiCOM.ComboBox = oForm.Items.Item("IDH_BRANCH").Specific
                    oCmb.Select(Config.INSTANCE.doGetBranch(goParent.gsUserName), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                Catch ex As Exception
                End Try
                Try
                    Dim oUser As SAPbouiCOM.EditText = oForm.Items.Item("IDH_USER").Specific
                    oUser.Value = goParent.gsUserName
                Catch ex As Exception
                End Try
                Try
                    'set focus back to the first field 
                    '## Start 26-07-2013
                    'doSetFocus(oForm, "IDH_REQSTF")
                    doSetFocus(oForm, "IDH_BOOSTF")
                    '## End
                Catch ex As Exception
                End Try
            End If

        End Sub

        Private Sub doBranchCombo(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks", Nothing, Nothing, True)
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            doReLoadData(oForm, Not getHasSharedData(oForm))
        End Sub

        Protected Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find")
                oForm.Items.Item("2").Visible = False
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update")
                oForm.Items.Item("2").Visible = True
            End If
        End Sub

        '** The Initializer
        Protected Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form)
            doReLoadData(oForm, False)
        End Sub

        '** The Initializer
        Protected Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean)
            oForm.Freeze(True)
            Try
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

                Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                oGridN.msWR1OrdType = "DO"
                'If bIsFirst = False Then
                '    oGridN.getGridControl().doCreateFilterString()
                '    If oGridN.doCheckForQueryChange() = False Then
                '        Return
                '    End If
                'End If
                oGridN.doReloadData()

                If Config.Parameter("DOMATO") = "TRUE" Then
                    doGridTotals(oGridN, False)
                    setVisible(oForm, "IDH_CALC", False)
                    'oForm.Items.Item("IDH_CALC").Visible = False
                Else
                    doGridTotals(oGridN, True)
                    setVisible(oForm, "IDH_CALC", True)
                    'oForm.Items.Item("IDH_CALC").Visible = True
                End If

                doSwitchRealtimeBilling(oForm, False)
                setUFValue(oForm, "IDH_RLBILL", "N")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error reloading the Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", Nothing)
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        '*
        ' Before using this ensure that the required Form Fields exists
        ' Using the form to get the grid with an ID of 'LINESGRID'
        '*
        Protected Sub doGridTotals(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoZero As Boolean = False)
            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            doGridTotals(oGridN, bDoZero)
        End Sub
        '*
        ' Before using this ensure that the required Form Fields exists
        '*
        Protected Sub doGridTotals(ByVal oGridN As OrderRowGrid, Optional ByVal bDoZero As Boolean = False)
            ''MA 05-01-2015 We will not calculate totals
            If oGridN.getSBOForm.TypeEx = "IDHCONMWO" OrElse oGridN.getSBOForm.TypeEx = "IDHCONMDO" OrElse (oGridN.getSBOForm.TypeEx.IndexOf("IDHJOBR") > -1 AndAlso com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("TOTALOSM", "TRUE").Trim.ToUpper <> "TRUE") OrElse
                (oGridN.getSBOForm.TypeEx.IndexOf("IDHWOBI") > -1 AndAlso com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("TOTALBM", "TRUE").Trim.ToUpper <> "TRUE") Then
                Exit Sub
            End If
            'MA 05-01-2015 end change

            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
                Dim oTotals As RowGridTotals

                ''New
                ''## MA test 27-08-2014 
                'Dim sTime As DateTime = Now
                If com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("TOTALTYP", "OLD").Trim.ToUpper = "OLD" Then
                    oTotals = oGridN.doTotals(bDoZero)
                Else
                    oTotals = oGridN.doTotalsNew(bDoZero)
                End If
                ''## MA test 27-08-2014
                ''Current
                ''MA Start 30-10-2014 Issue#417 Issue#418
                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) Then
                    'Do the Totals
                    setUFValue(oGridN.getSBOForm(), "IDH_SbADCt", oTotals.CostTotal)
                    setUFValue(oGridN.getSBOForm(), "IDH_TAdCst", oTotals.AdditionalCostTotal)
                    setUFValue(oGridN.getSBOForm(), "IDH_TxAmCo", oTotals.TxAmtCostTotal)
                    setUFValue(oGridN.getSBOForm(), "IDH_TotChg", oTotals.JCostTotal)

                    setUFValue(oGridN.getSBOForm(), "IDH_AddEx", oTotals.AddEx)

                End If
                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
                    setUFValue(oGridN.getSBOForm(), "IDH_SubAD", oTotals.AftDisc)
                    setUFValue(oGridN.getSBOForm(), "IDH_TAdChg", oTotals.AdditionalChargeTotal)
                    setUFValue(oGridN.getSBOForm(), "IDH_ChgDed", oTotals.ChargeDed)
                    setUFValue(oGridN.getSBOForm(), "IDH_TaxAmt", oTotals.TxAmt)
                    setUFValue(oGridN.getSBOForm(), "IDH_Total", oTotals.Total)

                    setUFValue(oGridN.getSBOForm(), "IDH_DisAmt", oTotals.DisAmt)

                End If
                setUFValue(oGridN.getSBOForm(), "IDH_CstWgt", oTotals.CstWgt)
                setUFValue(oGridN.getSBOForm(), "IDH_RdWgt", oTotals.RdWgt)
                ''MA End 30-10-2014 Issue#417 Issue#418
            End If
        End Sub
        'Protected Sub doGridTotals(ByVal oGridN As OrderRowGrid, Optional ByVal bDoZero As Boolean = False)
        '    ''MA Start 03-11-2014 Issue#417 Issue#418
        '    If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) OrElse
        '         Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) OrElse
        '         Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
        '        Dim oTotals As RowGridTotals
        '        oTotals = oGridN.doTotals(False)

        '        setUFValue(oGridN.getSBOForm(), "IDH_CstWgt", oTotals.CstWgt)
        '        'setUFValue(oGridN.getSBOForm(), "IDH_SubAD", oTotals.AftDisc)
        '        'setUFValue(oGridN.getSBOForm(), "IDH_Total", oTotals.Total)
        '        'setUFValue(oGridN.getSBOForm(), "IDH_DisAmt", oTotals.DisAmt)
        '        'setUFValue(oGridN.getSBOForm(), "IDH_AddEx", oTotals.AddEx)
        '        'setUFValue(oGridN.getSBOForm(), "IDH_TaxAmt", oTotals.TxAmt)


        '        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
        '            ''charge
        '            setUFValue(oGridN.getSBOForm(), "IDH_DisAmt", oTotals.DisAmt)
        '            setUFValue(oGridN.getSBOForm(), "IDH_SubAD", oTotals.AftDisc)
        '            setUFValue(oGridN.getSBOForm(), "IDH_Total", oTotals.Total)
        '            setUFValue(oGridN.getSBOForm(), "IDH_TaxAmt", oTotals.TxAmt)
        '        End If

        '        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) Then
        '            ''cost
        '            setUFValue(oGridN.getSBOForm(), "IDH_AddEx", oTotals.AddEx)
        '        End If
        '    End If
        '    ''MA End 03-11-2014 Issue#417 Issue#418
        'End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            If sModalFormType = "IDH_DISPORD" Then
                doReLoadData(oForm, True)
            End If
        End Sub

        Private Function doProcessRemovedRows(ByVal oGridN As UpdateGrid) As Boolean
            Dim aDeletedRows As ArrayList = oGridN.getDeletedRowList()
            If Not aDeletedRows Is Nothing OrElse aDeletedRows.Count > 0 Then
                Dim sDORow As String
                Dim sRowsToUnLink As String = ""
                'Dim iwResult As Integer
                Dim swResult As String = Nothing

                Dim iIndex As Integer = 0
                'Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_JOBSHD")
                'Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                'Try

                Dim oDOR As IDH_DISPROW = New IDH_DISPROW()
                While iIndex < aDeletedRows.Count
                    sDORow = aDeletedRows.Item(iIndex)
                    oDOR.U_WRRow = ""
                    oDOR.doProcessData()
                    'If sRowsToUnLink.Length > 0 Then
                    '    sRowsToUnLink = sRowsToUnLink & ","
                    'End If
                    'sRowsToUnLink = sRowsToUnLink & "'" & sDORow & "'"
                    'iIndex = iIndex + 1
                End While

                '    If sRowsToUnLink.Length > 0 Then
                '        Dim sQry As String = "SELECT Code, U_WRRow from [@IDH_JOBSHD] where U_WRRow in (" & sRowsToUnLink & ")"
                '        oRecordSet = goParent.goDB.doSelectQuery("doProcessRemovedRows", sQry)
                '        If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
                '            Dim iCount As Integer = oRecordSet.RecordCount
                '            Dim sWORow As String
                '            For iRec As Integer = 0 To iCount - 1
                '                sWORow = oRecordSet.Fields.Item(0).Value
                '                sDORow = oRecordSet.Fields.Item(1).Value
                '                If oAuditObj.setKeyPair(sWORow, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) = True Then
                '                    oAuditObj.setFieldValue("U_WROrd", "")
                '                    oAuditObj.setFieldValue("U_WRRow", "")
                '                    oAuditObj.setFieldValue("U_AEDate", Nothing)
                '                    oAuditObj.setFieldValue("U_AETime", Nothing)
                '                    iwResult = oAuditObj.Update()

                '                    If iwResult <> 0 Then
                '                        goParent.goDICompany.GetLastError(iwResult, swResult)
                '                        'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Updating: " + swResult, "Error processing the removed rows - WO Row: " & sWORow & " - DO Row: " & sDORow)
                '                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Updating: " + swResult + " Error processing the removed rows - WO Row: " & sWORow & " - DO Row: " & sDORow, "ERSYPREM", {com.idh.bridge.Translation.getTranslatedWord(swResult), sWORow, sDORow})
                '                        Return False
                '                    End If
                '                End If
                '            Next
                '        End If
                '    End If

                'Catch ex As Exception
                '    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error removing the rows.")
                '    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRGEN", Nothing)
                'Finally
                '    IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
                '    IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                'End Try
            End If
            Return oGridN.doProcessRemovedRows("[@IDH_DISPROW]")
        End Function

        Private Function doSaveOrderRows(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

            If doProcessRemovedRows(oGridN) = False Then
                Return False
            End If

            Dim aRows As ArrayList = oGridN.doGetChangedRows()

            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_DISPROW")
            Try
                Dim sKey As String
                '                Dim sSOStatus As String
                '                Dim sPOStatus As String
                Dim iResult As Integer
                Dim sResult As String = Nothing

                Dim aChangedFields As ArrayList
                Dim oValue As Object
                Dim sField As String
                If Not aRows Is Nothing Then
                    For iIndex As Integer = 0 To aRows.Count - 1
                        oGridN.setCurrentDataRowIndex(aRows(iIndex))
                        sKey = oGridN.doGetFieldValue("r.Code")
                        If oAuditObj.setKeyPair(sKey, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then
                            aChangedFields = oGridN.doGetChangedFields(aRows(iIndex))
                            For iField As Integer = 0 To aChangedFields.Count - 1
                                sField = aChangedFields.Item(iField)
                                If sField.StartsWith("r.") Then
                                    oValue = oGridN.doGetFieldValue(sField)
                                    sField = sField.Substring(2)
                                    oAuditObj.setFieldValue(sField, oValue)
                                End If
                            Next

                            iResult = oAuditObj.Commit()
                            If iResult <> 0 Then
                                goParent.goDICompany.GetLastError(iResult, sResult)
                                'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error saving the Order rows: " + sResult, "Error saving the Order rows - " & sKey)
                                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error saving the Order rows: " + sResult + " Error saving the Order rows - " & sKey, "ERSYSORD", {sKey, com.idh.bridge.Translation.getTranslatedWord(sResult)})
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error saving the Order rows.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSORD", Nothing)
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
            End Try
            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            '            Dim oButton As SAPbouiCOM.Button
            If pVal.BeforeAction = True Then
                If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
                    Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")

                    Dim aRows As ArrayList = oGridN.doGetChangedRows()
                    If doSaveOrderRows(oForm) = True Then
                        Dim sHead As String
                        Dim sRow As String
                        Dim sSOStatus As String
                        Dim sPOStatus As String
                        Dim sRowStatus As String
                        Dim sJrnlStatus As String

                        If Not aRows Is Nothing Then

                            If getUFValue(oForm, "IDH_RLBILL", True).Equals("Y") Then
                                If oForm.TypeEx <> "IDHCONMDO" Then
                                	oGridN.mbForceRealTimeBilling = True
                            	End If
                            Else
                                oGridN.mbForceRealTimeBilling = False
                            End If

                            Dim bDoInBack As Boolean = Config.INSTANCE.getParameterAsBool("MDCMIBG", False)
                            If bDoInBack = False OrElse oGridN.mbForceRealTimeBilling = True Then
                                For iIndex As Integer = 0 To aRows.Count - 1
                                    oGridN.setCurrentDataRowIndex(aRows(iIndex))
                                    sHead = oGridN.doGetFieldValue("r." & IDH_DISPROW._JobNr)
                                    sRow = oGridN.doGetFieldValue("r." & IDH_DISPROW._Code)
                                    sSOStatus = oGridN.doGetFieldValue("r." & IDH_DISPROW._Status)
                                    sPOStatus = oGridN.doGetFieldValue("r." & IDH_DISPROW._PStat)
                                    sRowStatus = oGridN.doGetFieldValue("r." & IDH_DISPROW._RowSta)
                                    sJrnlStatus = oGridN.doGetFieldValue("r." & IDH_DISPROW._JStat)

                                    If sSOStatus.StartsWith(FixedValues.getDoOrderStatus()) OrElse
                                     sPOStatus.StartsWith(FixedValues.getDoOrderStatus()) Then
                                        MarketingDocs.doOrders("@IDH_DISPORD", "@IDH_DISPROW", "DO", sHead, sRow)
                                    End If

                                    If sSOStatus.StartsWith(FixedValues.getDoInvoiceStatus()) OrElse
                                     sSOStatus.StartsWith(FixedValues.getDoPInvoiceStatus()) Then
                                        MarketingDocs.doInvoicesAR("@IDH_DISPORD", "@IDH_DISPROW", "DO", sHead, sRow)
                                    End If

                                    If sSOStatus.StartsWith(FixedValues.getDoRebateStatus()) OrElse
                                     sPOStatus.StartsWith(FixedValues.getDoRebateStatus()) Then
                                        MarketingDocs.doRebates("@IDH_DISPORD", "@IDH_DISPROW", "DO", sHead, sRow)
                                    End If

                                    If sSOStatus.StartsWith(FixedValues.getDoFocStatus()) Then
                                        MarketingDocs.doFoc("@IDH_DISPORD", "@IDH_DISPROW", "DO", sHead, sRow)
                                    End If

                                    If Config.INSTANCE.getParameterAsBool("NEWDOJRN", False) = True AndAlso sJrnlStatus.StartsWith(FixedValues.getDoJrnlStatus()) Then
                                        Dim sResult As Boolean = MarketingDocs.doJournals("@IDH_DISPORD", "@IDH_DISPROW", "DO", sHead, sRow)
                                        If sResult Then
                                            doWarnMess("The Journal entry for selected row(s) was posted successfully.")
                                        End If
                                    End If

                                    'idh.action.MarketingDocs.doOrders(goParent, "@IDH_DISPORD", "@IDH_DISPROW", "DO", sHead, sRow)
                                    'idh.action.MarketingDocs.doInvoices(goParent, "@IDH_DISPORD", "@IDH_DISPROW", "DO", sHead, sRow)
                                    'idh.action.MarketingDocs.doFoc(goParent, "@IDH_DISPORD", "@IDH_DISPROW", "DO", sHead, sRow)
                                    MarketingDocs.doPayment("@IDH_DISPORD", "@IDH_DISPROW", "DO", sHead, sRow)
                                    'idh.action.MarketingDocs.doRebates(goParent, "@IDH_DISPORD", "@IDH_DISPROW", "DO", sHead, sRow)
                                Next
                            End If
                        End If
                        doReLoadData(oForm, True)
                    End If
                ElseIf oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find") Then
                    doReLoadData(oForm, True)
                End If
                BubbleEvent = False
            End If
        End Sub

        Public Overrides Sub doButtonID2(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                doReLoadData(oForm, True)
                BubbleEvent = False
            End If
        End Sub

        '** Send By Custom Events
        'Return True done
        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                If pVal.BeforeAction = True Then
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, pVal.ItemUID)
                    Dim oMenuItem As SAPbouiCOM.MenuItem
                    oMenuItem = goParent.goApplication.Menus.Item(IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU)

                    Dim oMenus As SAPbouiCOM.Menus
                    Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                    oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
                    oCreationPackage.Enabled = True
                    oMenus = oMenuItem.SubMenus
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING

                    oCreationPackage.UniqueID = "NEW"
                    oCreationPackage.String = getTranslatedWord("New &Order")
                    oCreationPackage.Position = 1
                    oMenus.AddEx(oCreationPackage)

                    Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
                    Dim iSelectionCount As Integer = oSelected.Count
                    Dim iRow As Integer
                    Dim iMenuPos As Integer = 1
                    iRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)
                    If iSelectionCount > 0 Then
                        If iSelectionCount > 1 Then
                            If oGridN.doGetDeleteActive() Then
                                If Not (pVal.Row = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)) Then
                                    oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM
                                    oCreationPackage.String = getTranslatedWord("Delete &Selection")
                                    oCreationPackage.Enabled = True
                                    oMenus.AddEx(oCreationPackage)
                                End If
                            End If

                            ''If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_) = False OrElse _
                            ''   Config.INSTANCE.doGetIsSupperUser() Then
                            'oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREOPEN
                            'oCreationPackage.String = getTranslatedWord("ReOpen Rows")
                            'oCreationPackage.Enabled = True
                            'oMenus.AddEx(oCreationPackage)
                            ''End If

                            ''If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_) = False OrElse _
                            ''    Config.INSTANCE.doGetIsSupperUser() Then
                            'oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUCANCELMARKDOCS
                            'oCreationPackage.String = getTranslatedWord("Cancel Marketing Documents")
                            'oCreationPackage.Enabled = True
                            'oMenus.AddEx(oCreationPackage)
                            ''End If
                        Else
                            Dim sCurrentJobType As String
                            'Dim sJob As String
                            Dim sJobNr As String
                            Dim sRowNr As String
                            Dim oJobTypes As ArrayList = Nothing
                            Dim sStatus As String
                            Dim sWORStatus As String

                            sJobNr = oGridN.doGetFieldValue("r.U_JobNr", iRow)
                            sRowNr = oGridN.doGetFieldValue("r.Code", iRow)
                            sCurrentJobType = oGridN.doGetFieldValue("r.U_JobTp", iRow)
                            sStatus = oGridN.doGetFieldValue("e.U_Status", iRow)
                            sWORStatus = oGridN.doGetFieldValue("r.U_RowSta", iRow)

                            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_DELETE) AndAlso
                                sWORStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusDeleted()) Then
                                If iRow >= 0 Then
                                    oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML
                                    oCreationPackage.String = getTranslatedWord("UnDelete Disposal Order Row ") & " (" & (iRow + 1) & ")[" & sRowNr & "]"
                                    oCreationPackage.Enabled = True
                                    oMenus.AddEx(oCreationPackage)
                                End If
                            End If

                            If sWORStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusDeleted()) = False AndAlso oGridN.doGetDeleteActive() Then
                                If iRow >= 0 Then
                                    oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML
                                    oCreationPackage.String = getTranslatedWord("Delete Disposal Order Row ") & " (" & (iRow + 1) & ")[" & sRowNr & "]"
                                    oCreationPackage.Enabled = True
                                    oMenus.AddEx(oCreationPackage)
                                End If
                            End If
                        End If

                        oCreationPackage.UniqueID = "DOSOPO"
                        oCreationPackage.String = getTranslatedWord("Generate S/P &Orders for Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOSO"
                        oCreationPackage.String = getTranslatedWord("Generate &Sales Orders from Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOPO"
                        oCreationPackage.String = getTranslatedWord("Generate &Purchase Orders from Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOJRNL"
                        oCreationPackage.String = getTranslatedWord("Generate &Journals from Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOFOC"
                        oCreationPackage.String = getTranslatedWord("Set as &Free Of Charge from Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOINV"
                        oCreationPackage.String = getTranslatedWord("Generate &Invoices from Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOPAY"
                        oCreationPackage.String = getTranslatedWord("Proccess the Payments")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOCANCEL"
                        oCreationPackage.String = getTranslatedWord("Cancel the Shipment")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        ''If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_) = False OrElse _
                        ''   Config.INSTANCE.doGetIsSupperUser() Then
                        'oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREOPEN
                        'oCreationPackage.String = getTranslatedWord("ReOpen Row")
                        'oCreationPackage.Enabled = True
                        'oMenus.AddEx(oCreationPackage)
                        ''End If

                        ''If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_) = False OrElse _
                        ''    Config.INSTANCE.doGetIsSupperUser() Then
                        'oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUCANCELMARKDOCS
                        'oCreationPackage.String = getTranslatedWord("Cancel Marketing Documents")
                        'oCreationPackage.Enabled = True
                        'oMenus.AddEx(oCreationPackage)
                        ''End If
                    End If
                    Return False
                Else
                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM)
                    End If
                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML)
                    End If
                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML)
                    End If
                    If goParent.goApplication.Menus.Exists("NEW") Then
                        goParent.goApplication.Menus.RemoveEx("NEW")
                    End If
                    If goParent.goApplication.Menus.Exists("DOSOPO") Then
                        goParent.goApplication.Menus.RemoveEx("DOSOPO")
                    End If
                    If goParent.goApplication.Menus.Exists("DOSO") Then
                        goParent.goApplication.Menus.RemoveEx("DOSO")
                    End If
                    If goParent.goApplication.Menus.Exists("DOPO") Then
                        goParent.goApplication.Menus.RemoveEx("DOPO")
                    End If
                    If goParent.goApplication.Menus.Exists("DOJRNL") Then
                        goParent.goApplication.Menus.RemoveEx("DOJRNL")
                    End If
                    If goParent.goApplication.Menus.Exists("DOFOC") Then
                        goParent.goApplication.Menus.RemoveEx("DOFOC")
                    End If
                    If goParent.goApplication.Menus.Exists("DOINV") Then
                        goParent.goApplication.Menus.RemoveEx("DOINV")
                    End If

                    If goParent.goApplication.Menus.Exists("DOPAY") Then
                        goParent.goApplication.Menus.RemoveEx("DOPAY")
                    End If

                    If goParent.goApplication.Menus.Exists("DOCANCEL") Then
                        goParent.goApplication.Menus.RemoveEx("DOCANCEL")
                    End If

                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREOPEN) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREOPEN)
                    End If

                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUCANCELMARKDOCS) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUCANCELMARKDOCS)
                    End If

                    Return True
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                If pVal.BeforeAction = False Then
                    Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                    If pVal.oData.MenuUID.Equals("UDEL") Then
                        oGridN.doReactivateSelection()
                        doSwitchRealtimeBilling(oForm, True)
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        Return False
                    ElseIf pVal.oData.MenuUID.Equals("NEW") Then
                        doNewOrder(oForm)
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                    ElseIf pVal.oData.MenuUID.Equals("DOSOPO") Then
                        'doMarkAsToOrder(oForm, True, True)
                        oGridN.doMarkSelectionAsOrder(True, True)
                        doSwitchRealtimeBilling(oForm, True)
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                    ElseIf pVal.oData.MenuUID.Equals("DOSO") Then
                        'doMarkAsToOrder(oForm, True, False)
                        oGridN.doMarkSelectionAsOrder(True, False)
                        doSwitchRealtimeBilling(oForm, True)
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                    ElseIf pVal.oData.MenuUID.Equals("DOPO") Then
                        'doMarkAsToOrder(oForm, False, True)
                        oGridN.doMarkSelectionAsOrder(False, True)
                        doSwitchRealtimeBilling(oForm, True)
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                    ElseIf pVal.oData.MenuUID.Equals("DOJRNL") Then
                        oGridN.doMarkSelectionAsJournal()
                        doSwitchRealtimeBilling(oForm, True)
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                    ElseIf pVal.oData.MenuUID.Equals("DOFOC") Then
                        'doMarkAsToFoc(oForm)
                        oGridN.doMarkSelectionAsFoc()
                        doSwitchRealtimeBilling(oForm, True)
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                    ElseIf pVal.oData.MenuUID.Equals("DOINV") Then
                        'doMarkAsToInvoice(oForm)
                        oGridN.doMarkSelectionAsInvoice()
                        doSwitchRealtimeBilling(oForm, True)
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                    ElseIf pVal.oData.MenuUID.Equals("DOPAY") Then
                        'doMarkAsToPay(oForm)
                        oGridN.doMarkSelectionAsPay()
                        doSwitchRealtimeBilling(oForm, True)
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                    ElseIf pVal.oData.MenuUID.Equals("DOCANCEL") Then
                        oGridN.doRequestShipmentCancelation()
                        'doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                    ElseIf pVal.oData.MenuUID.Equals(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREOPEN) Then
                        oGridN.doRequestReOpen()
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        Return False
                    ElseIf pVal.oData.MenuUID.Equals(IDHAddOns.idh.controls.IDHGrid.GRIDMENUCANCELMARKDOCS) Then
                        oGridN.doRequestCancelMarketingDocs()
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        Return False
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FORMAT_FIELDS Then
                If pVal.BeforeAction = False Then
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
                If pVal.BeforeAction = True Then
                    Dim sField As String
                    sField = pVal.oData
                    Dim sValueB As String
                    Dim sValueA As String
                    If sField.Equals("r.U_Lorry") Then
                        Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, pVal.ItemUID)
                        sValueB = oGridN.doGetFieldValue(sField)
                        sValueA = com.idh.bridge.utils.General.doFixReg(sValueB)
                        setSharedData(oForm, "IDH_VEHREG", sValueA)
                        setSharedData(oForm, "IDH_WR1OT", "^DO")
                        setSharedData(oForm, "SHOWACTIVITY", "FALSE")
                        goParent.doOpenModalForm("IDHLOSCH", oForm)
                    End If
                    doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = True Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                        oGridN.doCommentDeletion()
                        Return False
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL_MULTI Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = True Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                        oGridN.doCommentDeletion()
                        Return False
                    End If
                End If
            End If
            Return True
        End Function

        Private Sub doSwitchRealtimeBilling(ByVal oForm As SAPbouiCOM.Form, ByVal bVisible As Boolean)
            Dim bDoInBack As Boolean = Config.INSTANCE.getParameterAsBool("MDCMIBG", False)
            If bDoInBack = False Then
                bVisible = False
            End If

            Try
                oForm.Items.Item("IDH_RLBILL").Visible = bVisible
            Catch ex As Exception
            End Try
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType.Equals("IDH_COMMFRM") Then
                    Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                    oGridN.doCheckAndDeleteSelection(getSharedData(oForm, "CMMNT"))
                ElseIf sModalFormType = "IDHLOSCH" Then
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                    Dim iRow As Integer = oGridN.getCurrentDataRowIndex()
                    Dim sReg As String = getSharedData(oForm, "REG")
                    oGridN.doSetFieldValue("r.U_Lorry", sReg)
                ElseIf sModalFormType = "IDHPAYMET" Then
                    oForm.Freeze(True)
                    Try
                        Dim oToSelectPayment As ArrayList = getSharedData(oForm, "ROWS")
                        If Not oToSelectPayment Is Nothing Then
                            Dim sPayMeth As String = getSharedData(oForm, "METHOD")
                            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                            Dim iIndex As Integer
                            Dim dTotal As Double = 0
                            Dim sCustomerCd As String = Nothing
                            Dim sCustomerNM As String = Nothing
                            Dim bDoContinue As Boolean = True

                            'First check the Customers if teh Payment Method is CC
                            If sPayMeth.Equals(Config.INSTANCE.getCreditCardName()) Then
                                For iIndex = 0 To oToSelectPayment.Count - 1
                                    If iIndex = 0 Then
                                        sCustomerCd = oGridN.doGetFieldValue("e.U_CardCd", oToSelectPayment(iIndex))
                                        sCustomerNM = oGridN.doGetFieldValue("e.U_CardNM", oToSelectPayment(iIndex))
                                    Else
                                        If sCustomerCd.Equals(oGridN.doGetFieldValue("e.U_CardCd", oToSelectPayment(iIndex))) = False Then
                                            ''com.idh.bridge.DataHandler.INSTANCE.doError("Can not select a CreditCard payment with multiple customers in the selection.")
                                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Can not select a CreditCard payment with multiple customers in the selection.", "ERUSPCP", Nothing)
                                            bDoContinue = False
                                            Exit For
                                        End If
                                    End If
                                    dTotal = dTotal + oGridN.doGetFieldValue("r.U_Total", oToSelectPayment(iIndex))
                                Next
                            End If

                            If bDoContinue Then
                                For iIndex = 0 To oToSelectPayment.Count - 1
                                    oGridN.doForceFieldValue("r.U_PayStat", oToSelectPayment(iIndex), com.idh.bridge.lookups.FixedValues.getStatusPaid())
                                    oGridN.doForceFieldValue("r.U_PayMeth", oToSelectPayment(iIndex), sPayMeth)
                                    oGridN.doForceFieldValue("r.U_CCNum", oToSelectPayment(iIndex), "")
                                    oGridN.doForceFieldValue("r.U_CCType", oToSelectPayment(iIndex), "0")
                                    oGridN.doForceFieldValue("r.U_CCStat", oToSelectPayment(iIndex), "")
                                    oGridN.doForceFieldValue("r.U_CCIs", oToSelectPayment(iIndex), "")
                                    oGridN.doForceFieldValue("r.U_CCSec", oToSelectPayment(iIndex), "")
                                    oGridN.doForceFieldValue("r.U_CCHNum", oToSelectPayment(iIndex), "")
                                    oGridN.doForceFieldValue("r.U_CCPCd", oToSelectPayment(iIndex), "")
                                Next

                                If sPayMeth.Equals(Config.INSTANCE.getCreditCardName()) Then
                                    setSharedData(oForm, "ROWS", oToSelectPayment)
                                    setSharedData(oForm, "CUSTCD", sCustomerCd)
                                    setSharedData(oForm, "CUSTNM", sCustomerNM)
                                    setSharedData(oForm, "AMOUNT", dTotal)
                                    goParent.doOpenModalForm("IDHCCPAY", oForm)
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing Modal results - " & sModalFormType)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
                    End Try
                    oForm.Freeze(False)
                ElseIf sModalFormType = "IDHCCPAY" Then
                    Dim sCCStatPre As String
                    Dim sPStat As String
                    Dim sCCNum As String
                    Dim sCCType As String
                    Dim sCCStat As String

                    Dim sCCIs As String
                    Dim sCCSec As String
                    Dim sCCHNm As String
                    Dim sCCPCd As String
                    Dim sCCExp As String
                    oForm.Freeze(True)
                    Try
                        sCCNum = getSharedData(oForm, "CCNUM")
                        sPStat = getSharedData(oForm, "PSTAT")
                        sCCType = getSharedData(oForm, "CCTYPE")
                        sCCStat = getSharedData(oForm, "CCSTAT")

                        sCCIs = getSharedData(oForm, "CCIs")
                        sCCSec = getSharedData(oForm, "CCSec")
                        sCCHNm = getSharedData(oForm, "CCHNm")
                        sCCPCd = getSharedData(oForm, "CCPCd")
                        sCCExp = getSharedData(oForm, "CCEXP")

                        If sPStat.Equals(com.idh.bridge.lookups.FixedValues.getStatusPaid()) Then
                            sCCStatPre = "APPROVED: "
                        Else
                            sCCStatPre = "DECLINED: "
                        End If

                        Dim oToSelectPayment As ArrayList = getSharedData(oForm, "ROWS")
                        If Not oToSelectPayment Is Nothing Then
                            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                            Dim iIndex As Integer
                            For iIndex = 0 To oToSelectPayment.Count - 1
                                oGridN.doForceFieldValue("r.U_CCNum", oToSelectPayment(iIndex), sCCNum)
                                oGridN.doForceFieldValue("r.U_CCType", oToSelectPayment(iIndex), sCCType)
                                oGridN.doForceFieldValue("r.U_CCStat", oToSelectPayment(iIndex), sCCStatPre & sCCStat)
                                oGridN.doForceFieldValue("r.U_PayStat", oToSelectPayment(iIndex), sPStat)
                                oGridN.doForceFieldValue("r.U_CCExp", oToSelectPayment(iIndex), sCCExp)
                                oGridN.doForceFieldValue("r.U_CCIs", oToSelectPayment(iIndex), sCCIs)
                                oGridN.doForceFieldValue("r.U_CCSec", oToSelectPayment(iIndex), sCCSec)
                                oGridN.doForceFieldValue("r.U_CCHNum", oToSelectPayment(iIndex), sCCHNm)
                                oGridN.doForceFieldValue("r.U_CCPCd", oToSelectPayment(iIndex), sCCPCd)
                            Next
                        End If
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing Modal results - " & sModalFormType)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
                    End Try
                    oForm.Freeze(False)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing Modal results - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        Protected Overrides Sub doReturnCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            Try
                If sModalFormType = "IDHLOSCH" Then
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                    oGridN.doSetFieldValue("r.U_Lorry", oGridN.moLastFieldValue)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error canceling from Modal - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCMR", {sModalFormType})
            End Try
        End Sub

        'Creates a comma seperated list
        Public Function doCreateCSL(ByRef oData As ArrayList) As String
            Dim sList As String = ""
            Dim iIndex As Integer
            For iIndex = 0 To oData.Count - 1
                If sList.Length > 0 Then
                    sList = sList & ","
                End If
                sList = sList & (oData.Item(iIndex) + 1)
            Next
            Return sList
        End Function

        Public Sub doMarkAsToOrder(ByVal oForm As SAPbouiCOM.Form, ByVal bDoSO As Boolean, ByVal bDoPO As Boolean)
            If bDoSO = False AndAlso bDoPO = False Then
                Exit Sub
            End If

            oForm.Freeze(True)
            Try
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = oGridN.getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim iCurrentDataRow As Integer
                    Dim bFound As Boolean = False
                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iCurrentDataRow = oGridN.getDataTableRowIndex(oSelected, iIndex)

                        Dim sStatus As String
                        If bDoSO Then
                            sStatus = oGridN.doGetFieldValue("r.U_Status", iCurrentDataRow)
                            If sStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) = False Then
                                oGridN.doForceFieldValue("r.U_Status", iCurrentDataRow, com.idh.bridge.lookups.FixedValues.getDoOrderStatus())
                                oGridN.doForceFieldValue("r.U_PayMeth", iCurrentDataRow, "Accounts")
                                oGridN.doForceFieldValue("r.U_PayStat", iCurrentDataRow, com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
                                oGridN.doForceFieldValue("r.U_CCNum", iCurrentDataRow, "")
                                oGridN.doForceFieldValue("r.U_CCType", iCurrentDataRow, "0")
                                oGridN.doForceFieldValue("r.U_CCStat", iCurrentDataRow, "")
                            End If
                        End If

                        If bDoPO Then
                            sStatus = oGridN.doGetFieldValue("r.U_PStat", iCurrentDataRow)
                            If sStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) = False Then
                                Dim iIndexChr As Integer = sStatus.IndexOf("-")
                                If iIndexChr > 0 Then
                                    sStatus = sStatus.Substring(iIndexChr)
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoOrderStatus() & sStatus
                                Else
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoOrderStatus()
                                End If
                                oGridN.doForceFieldValue("r.U_PStat", iCurrentDataRow, sStatus)
                                'oGridN.doForceFieldValue("r.U_PStat", iRow, "DoOrder-111")
                            End If
                        End If

                        If bDoPO = False AndAlso bDoSO = False Then
                            oSelected.Remove(iIndex)
                        End If
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error marking to Order.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXMTOO", Nothing)
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_DOC" Then
                        doDocReport(oForm)
                    ElseIf pVal.ItemUID = "IDH_GENCNU" Then
                        doGenConNumbers(oForm)
                    ElseIf pVal.ItemUID = "IDH_WS" Then
                        Dim sReportFile As String
                        sReportFile = Config.Parameter("OSMDWO")

                        Dim oParams As New Hashtable
                        oParams.Add("OrdNum", "")
                        oParams.Add("RowNum", "")

                        If (Config.INSTANCE.useNewReportViewer()) Then
                            IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                            IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                            IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                        Else
                            IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                        End If
                    ElseIf pVal.ItemUID = "IDH_CALC" Then
                        doGridTotals(oForm, False)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = False Then
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                            oGridN.setCurrentLineByClick(pVal.Row)

                            Dim sSeqNr As String = oGridN.doGetFieldValue("r.Code")
                            Dim sJobEntr As String = oGridN.doGetFieldValue("r.U_JobNr")

                            Dim oData As New ArrayList
                            If sJobEntr.Length = 0 Then
                                'com.idh.bridge.DataHandler.INSTANCE.doError("A Job Entry must be selected.")
                                com.idh.bridge.DataHandler.INSTANCE.doResUserError("A Job Entry must be selected.", "ERUSJOBS", Nothing)
                            Else
                                'oData.Add("DoUpdate")
                                'oData.Add(sJobEntr)
                                'setSharedData(oForm, "ROWCODE", sSeqNr)
                                'goParent.doOpenModalForm("IDH_DISPORD", oForm, oData)

                                Try
                                    Dim oOrder As com.isb.core.forms.DisposalOrder = New com.isb.core.forms.DisposalOrder(oForm.UniqueID, sJobEntr, sSeqNr)
                                    oOrder.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                                    oOrder.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                                    oOrder.doShowModal()
                                Catch Ex As Exception
                                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                                End Try

                            End If
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
            End If
            Return False
        End Function

        Public Function Handler_WOROKReturn(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            doReLoadData(oOForm.SBOParentForm, True)
            Return True
        End Function

        Public Function Handler_WORCancelReturn(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            'doReLoadData(oOForm.SBOParentForm, True)
            Return True
        End Function

        Protected Overridable Sub doDocReport(ByVal oForm As SAPbouiCOM.Form)
            If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
                If Messages.INSTANCE.doResourceMessageYNAsk("MANRUC", Nothing, 2) = 2 Then
                    Exit Sub
                End If
            End If

            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim oRows As ArrayList = oGridN.doPrepareListFromSelection("r.Code")
            If oRows.Count() = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("Select a disposal order row.")
                DataHandler.INSTANCE.doResUserError("Select a disposal order row.", "ERUSDOR", Nothing)
                Exit Sub
            End If

            'OnTime Ticket 25926 - DOC Report crahses application 
            'if report starts with http display PDF report in web browser control
            Dim sReportFile As String
            sReportFile = Config.Parameter("WDOCDOC")

            If sReportFile.StartsWith("http://") Then
                'Start ISB Code 
                Dim oParams As New Hashtable
                oParams.Add("RowNum", oRows.Item(0))

                'implementation of doCallReportDefaults 
                doCallHTTPRpts(goParent, oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType, Config.INSTANCE.doGetBranch(goParent.gsUserName))
                'End ISB Code 
            Else
                Dim oReport As DocCon = New DocCon(Me, oForm, "DO", gsType, True)
                Dim bNewConsignments As Boolean = oReport.doDocConReport(oRows)
                doAfterConsignmentReport(oForm, oReport)
                oReport = Nothing
            End If
        End Sub

        Protected Overridable Sub doGenConNumbers(ByVal oForm As SAPbouiCOM.Form)
            If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
                If Messages.INSTANCE.doResourceMessageYNAsk("MANRUC", Nothing, 2) = 2 Then
                    Exit Sub
                End If
            End If

            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim oRows As ArrayList = oGridN.doPrepareListFromSelection("r.Code")
            If oRows.Count() = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("Select a disposal order row.")
                DataHandler.INSTANCE.doResUserError("Select a disposal order row.", "ERUSDOR", Nothing)
                Exit Sub
            End If

            Dim oReport As DocCon = New DocCon(Me, oForm, "DO", gsType, True)
            Dim bNewConsignments As Boolean = oReport.doGenerateConNumbers(oRows)
            doAfterConsignmentReport(oForm, oReport)
            oReport = Nothing
        End Sub

        Public Sub doAfterConsignmentReport(ByVal oForm As SAPbouiCOM.Form, ByVal oReport As DocCon)
            'Prompt for the JJK Number if it is an USA version
            Dim bISUSARelease As Boolean = Config.INSTANCE.getParameterAsBool("USAREL", False)
            If bISUSARelease = True Then
                If Not oReport.ConNumbers Is Nothing AndAlso oReport.ConNumbers.Count > 0 Then
                    For Each sConNum As String In oReport.ConNumbers
                        Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.JJKNumber = New WR1_FR2Forms.idh.forms.fr2.JJKNumber(Nothing, oForm, Nothing)
                        oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doReturnFromJJK)
                        oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doReturnFromJJK)
                        'oOOForm.RowCodes = oReport.ConRowsToUpdate
                        oOOForm.TempConNum = sConNum 'oReport.LastConsignmentNumber
                        oOOForm.RowTable = "@IDH_DISPROW"
                        oOOForm.doShowModal()
                    Next
                End If
            Else
                If oReport.HasNewConsignmentNote Then
                    doReLoadData(oForm, False)
                End If
            End If
        End Sub

        Public Function doReturnFromJJK(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oPForm As WR1_FR2Forms.idh.forms.fr2.JJKNumber = oOForm
            doReLoadData(oOForm.SBOParentForm, False)
            Return True
        End Function

        Public Overridable Sub doNewOrder(ByVal oForm As SAPbouiCOM.Form)
            Dim oData As New ArrayList
            setSharedData(oForm, "ACTION", "DoAdd")
            setSharedData(oForm, "Code", "")
            goParent.doOpenModalForm("IDH_DISPORD", oForm, oData)
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
            UpdateGrid.doRemoveGrid(oForm, "LINESGRID")
        End Sub

        Public Overrides Sub doClose()
        End Sub

        'OnTime Ticket 25926 - DOC Report crahses application 
        Public Function doCallHTTPRpts(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sReport As String, ByVal sDoShow As String, ByVal sDoPrint As String, ByVal sCopies As String, ByRef oParams As Hashtable, ByRef sFromForm As String, ByRef sBranch As String) As Boolean
            Dim sDoPrintDialog As String = Config.Parameter("MDSWPD")
            If sDoPrintDialog Is Nothing Then
                sDoPrintDialog = "TRUE"
            Else
                sDoPrintDialog = sDoPrintDialog.ToUpper()
            End If

            Dim sDoAutoPrint As String = Config.Parameter("AUTOPRNT")
            If sDoAutoPrint Is Nothing Then
                sDoAutoPrint = "TRUE"
            Else
                sDoAutoPrint = sDoAutoPrint.ToUpper()
            End If

            'Get the printer from the DB
            'Dim oPrinterSettings As idh.data.PrintSettings = Nothing
            'oPrinterSettings = Config.INSTANCE.getPrinterSetting(oParent, sReport, sFromForm, sBranch)

            Dim oPrinterSettings As com.idh.bridge.PrintSettings = Nothing
            oPrinterSettings = PrintSettings.getPrinterSetting(sReport, sFromForm, sBranch)

            Dim sPrinterName As String = Nothing
            Dim iWidth As Integer = 0
            Dim iHeight As Integer = 0
            If Not oPrinterSettings Is Nothing Then
                sPrinterName = oPrinterSettings.msPrinterName
                iWidth = oPrinterSettings.miWidth
                iHeight = oPrinterSettings.miHeight
            End If
            Return doCallHTTPRptsContainer(oParent, oForm, sReport, sDoShow, sDoAutoPrint, sDoPrintDialog, sCopies, oParams, sPrinterName, iWidth, iHeight)

        End Function

        Public Function doCallHTTPRptsContainer(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sReport As String, ByVal sDoShow As String, ByVal sDoPrint As String, ByVal sDoDialog As String, ByVal sCopies As String, ByRef oParams As Hashtable, ByRef sPrinterName As String, ByRef iWidth As Integer, ByRef iHeight As Integer) As Boolean
            'Get The print option
            Dim bDoPrint As Boolean = False
            If (Not sDoPrint Is Nothing) AndAlso sDoPrint.ToUpper().Equals("TRUE") Then
                bDoPrint = True
            End If

            'Get The show the report
            Dim bDoShow As Boolean = True
            If (Not sDoShow Is Nothing) AndAlso sDoShow.ToUpper().Equals("FALSE") Then
                bDoShow = False
                bDoPrint = True
            End If

            'Get The show printdialog option
            Dim bDoDialog As Boolean = True
            If (Not sDoDialog Is Nothing) AndAlso sDoDialog.ToUpper().Equals("FALSE") Then
                bDoDialog = False
            End If

            'Get The number of copies
            Dim iCopies As Integer = 1
            If Not sCopies Is Nothing Then
                Try
                    iCopies = Val(sCopies)
                Catch ex As Exception
                End Try
            End If

            setSharedData(oForm, "DOPRINT", sDoPrint)
            setSharedData(oForm, "DOSHOWPRNDIALOG", sDoDialog)
            setSharedData(oForm, "DOSHOW", sDoShow)
            setSharedData(oForm, "NUMCOPIES", sCopies)
            setSharedData(oForm, "PARAMS", oParams)
            setSharedData(oForm, "PRINTERNAME", sPrinterName)

            setSharedData(oForm, "WIDTH", iWidth)
            setSharedData(oForm, "HEIGHT", iHeight)

            If sReport.StartsWith("http://") Then
                Dim sURL As String = sReport

                If oParams.Count > 0 Then
                    Dim en As IEnumerator = oParams.Keys.GetEnumerator()
                    Dim sKey As String
                    Dim sValue As String

                    While en.MoveNext
                        sKey = en.Current().ToString()
                        If Not sKey Is Nothing Then
                            sValue = oParams.Item(sKey)
                            sURL = sURL & "&" & sKey & "=" & sValue
                        End If
                    End While
                End If

                If bDoShow = False Then
                    'Dim oHTML As idh.report.HTMLViewer
                    'oHTML = idh.report.HTMLViewer.getInstance(oParent)
                    'oHTML.doPrintSilent(sURL, bDoDialog, iCopies)
                Else
                    setSharedData(oForm, "URL", sURL)
                    oParent.doOpenModalForm("IDHHTML", oForm)
                End If
            Else
                'Dim sReportChk As String = idh.report.CrystalViewer.getReportChk(oParent, sReport)
                Dim sReportChk As String = getReportChk(oParent, sReport)
                If Not sReportChk Is Nothing Then
                    setSharedData(oForm, "CHKREPORT", sReportChk)
                    setSharedData(oForm, "REPORT", sReport)
                    setSharedData(oForm, "ZOOM", "69")
                    setSharedData(oForm, "DSPGROUPTREE", "FALSE")
                    setSharedData(oForm, "DSPTOOLBAR", "TRUE")
                    oParent.doOpenModalForm("IDHREPORT", oForm)
                Else
                    Return False
                End If
            End If
            Return True

        End Function

        Public Function getReportChk(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sReportIn As String) As String
            Dim sReport As String = Nothing
            Dim sPath As String = Config.Parameter("REPDIR")
            If Not sPath Is Nothing AndAlso sPath.Trim().Length() > 0 Then
                If sPath.EndsWith("\") = False Then
                    sPath = sPath & "\"
                End If
            Else
                'sPath = Directory.GetCurrentDirectory & "\reports\"
                sPath = IDHAddOns.idh.addon.Base.REPORTPATH
            End If

            sReport = sReportIn.Trim()
            If sReport Is Nothing OrElse sReport.Length() = 0 Then
                Return Nothing
            End If
            sReport = sPath & sReport

            'If oParent.gsAdditionalSRFPath Is Nothing Then
            If IDHAddOns.idh.addon.Base.REPORTPATH Is Nothing Then
                If File.Exists(sReport) = False Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Config: The report file could not be found - " & sReportIn, "The report file could not be found - " & sReportIn)
                    DataHandler.INSTANCE.doResConfigError("The report file could not be found - " & sReportIn, "ERCONREP", {sReportIn})
                    Return Nothing
                Else
                    Return sReport
                End If
            Else
                If File.Exists(sReport) = False Then
                    'sPath = oParent.gsAdditionalSRFPath & "\reports\"
                    sPath = IDHAddOns.idh.addon.Base.REPORTPATH & "\"
                    sReport = sPath & sReportIn.Trim()
                    If File.Exists(sReport) = False Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Config: The report file could not be found - " & sReportIn, "The report file could not be found - " & sReportIn)
                        DataHandler.INSTANCE.doResConfigError("The report file could not be found - " & sReportIn, "ERCONREP", {sReportIn})
                        Return Nothing
                    Else
                        Return sReport
                    End If
                Else
                    Return sReport
                End If
            End If
        End Function

    End Class
End Namespace
