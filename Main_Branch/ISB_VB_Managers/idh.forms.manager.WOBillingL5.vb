Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.lookups
Imports com.idh.bridge
Imports com.idh.bridge.data
Imports com.idh.dbObjects.User

Namespace idh.forms.manager
    Public Class WOBillingL5
        Inherits idh.forms.manager.Templ

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHWOBIL5", Nothing, "Billing ManagerL5.srf", iMenuPosition, "WO Billing Manager L5")
        End Sub

        Protected Overrides Sub doSetGridFilters(ByVal oGridN As FilterGrid)
            oGridN.doAddFilterField("IDH_REQSTF", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_REQDTT", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)

            oGridN.doAddFilterField("IDH_STARDF", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_STARDT", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)

            oGridN.doAddFilterField("IDH_ENDDF", "r.U_AEDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_ENDDT", "r.U_AEDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)

            oGridN.doAddFilterField("IDH_STATUS", "r.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PURSTS", "r.U_PStat", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)


            oGridN.doAddFilterField("IDH_ORDNO", "r.U_JobNr", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ROWNO", "r.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            'oGridN.doAddFilterField("IDH_PROG", "v.State", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_JOBTYP", "r.U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_DPRREF", "r.U_DPRRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, Config.INSTANCE.getParameterWithDefault("DPRSCOP", "AUTO"), 255)
            oGridN.doAddFilterField("IDH_CUST", "e.U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)


            'BPI Ref is adding when its combo is getting filled
            oGridN.doAddFilterField("IDH_NAME", "e.U_CardNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_HDSTA", "e.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 30)

            oGridN.doAddFilterField("IDH_BRANCH", "r.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)

			''## MA Start 29-08-2014 Issue#421
            oGridN.doAddFilterField("IDH_BRANC2", "r.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            ''## MA End 29-08-2014 Issue#421
            oGridN.doAddFilterField("IDH_USER", "r.U_User", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            'Self Billing            ''it is in doreloaddata 


            oGridN.doAddFilterField("IDH_ADDR", "e.U_Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_STREET", "e.U_Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_POSTCD", "e.U_ZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_REF", "r.U_CustRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_PROD", "r.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_PRODES", "r.U_ItemDsc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_WSCD", "r.U_WasCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WSDC", "r.U_WasDsc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            '#########
            'EWC
            oGridN.doAddFilterField("IDH_EWC", "i.U_EWC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            '#########
            'U_Sched
            oGridN.doAddFilterField("IDH_SCHADH", "r.U_Sched", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)

            oGridN.doAddFilterField("IDH_SUPCD", "r.U_Tip", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SUPNM", "r.U_TipNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_CARCD", "r.U_CarrCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255) 'Carrier Code
            oGridN.doAddFilterField("IDH_CARNM", "r.U_CarrNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255) 'Carrier Name

            oGridN.doAddFilterField("IDH_PRSCOD", "r.U_ProCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255) 'Prod Code
            oGridN.doAddFilterField("IDH_PRSNAM", "r.U_ProNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255) 'Prod Name

            oGridN.doAddFilterField("IDH_STEID", "e.U_SteId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_REBAT", "r.U_Obligated", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            '20140515: New filters on Reference numbers and Maximo No on WOR 
            oGridN.doAddFilterField("IDH_PROREF", "r.U_ProRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DSPREF", "r.U_SiteRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SUPREF", "r.U_SupRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_MAXIMO", "r.U_MaximoNum", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_PBISTA", "p.PBIType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            ''Billing Manager L5 
            'If Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
            '    If doSetGrid(oGridN) Then
            '        Return
            '    End If
            'End If
            oGridN.doAddListField(doGetProgressStatusQuery, "Progress", False, -1, Nothing, Nothing)
            oGridN.doAddListField(doGetProgressImageQuery, "", False, 10, "PICTURE", Nothing)
            'oGridN.doAddListField("v.State", "Progress", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("v.IMG", "", False, 10, "PICTURE", Nothing)
            oGridN.doAddListField("r.U_Status", "Sales Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_PStat", "Purchase Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobNr", "Order No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Row No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Obligated", "Exception/Rebate", False, -1, Nothing, Nothing) 't
            oGridN.doAddListField("r.U_DPRRef", "DPR#", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_SAORD", "Sales Order", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Order)
            oGridN.doAddListField("r.U_JOBPO", "Haulage PO", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder)
            oGridN.doAddListField("r.U_TIPPO", "Tipping PO", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder)
            oGridN.doAddListField("r.U_WHTRNF", "Stock Transfer", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_StockTransfers)
            oGridN.doAddListField("r.U_RDate", "Req. Date", False, -1, Nothing, Nothing) 't
            oGridN.doAddListField("r.U_ASDate", "Start Date", False, -1, Nothing, Nothing) 't
            oGridN.doAddListField("r.U_AEDate", "End Date", False, -1, Nothing, Nothing) 't
            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_Address", "Address Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_Street", "Street", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_SteId", "Site Id", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r." + IDH_JOBSHD._ProCd, "Producer Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r." + IDH_JOBSHD._ProNm, "Producer Name", False, 70, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_WasDsc", "Waste Desc", False, -1, Nothing, Nothing)
            oGridN.doAddListField("i.U_EWC", "EWC", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Sched", "Sched/Adhoc", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_LnkPBI", "PBI Instruction", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Disp Chg Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "Disp Chg Unit Price", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "Disp Chg Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_HlSQty", "Trns Chg Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CusChr", "Trns Chg Unit Price", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Price", "Trns Chg Total", False, -1, Nothing, Nothing)
            '' So far we left these as showing these fields on Grid has issue bcz of db struction and relationship and further to the fixed equi join in 
            '' Framwork for Grid Query building
            'Rebate Chg Qty
            'Rebate Chg Unit Price
            'Rebate Chg Total
            'Add Chg Qty
            'Add Chg Unit Price

            '' Add this rebate as discussed with KA to handle the rebate requirment
            Dim sObligated As String
            sObligated = Config.Parameter("OBTRUE")
            If sObligated Is Nothing OrElse sObligated.Length = 0 Then
                sObligated = getTranslatedWord("Obligated")
            End If
            oGridN.doAddListField("(case when isnull(r.U_Obligated,'')='" & sObligated & "' Then 'Y' else '' end)", "Is Rebate", False, -1, Nothing, Nothing)
            'Add Chg Total; if for somehow we start using addional tab table then will change this field to addional tab table according to requirment
            oGridN.doAddListField("r.U_TAddChrg", "Add Chg Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Tip", "Disp Sup Code", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_TipNm", "Disp Sup Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipWgt", "Disp Cost Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "Disp Cost Unit Price", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "Disp Cost Chg Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CarrCd", "Trns Sup Code", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_CarrNm", "Trns Sup Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_OrdWgt", "Trns Cost Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_OrdCost", "Trns Cost Unit Price", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_OrdTot", "Trns Cost Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ProCd", "Pur Sup Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ProNm", "Pur Sup Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ProWgt", "Pur Cost Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Pcost", "Pur Cost Unit Price", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCTotal", "Pur Cost Chg Total", False, -1, Nothing, Nothing)

            '' So far we left these as showing these fields on Grid has issue bcz of db struction and relationship and further to the fixed equi join in 
            '' Framwork for Grid Query building
            'Add Sup Code
            'Add Sup Name
            'Add Cost Qty
            'Add Cost Unit Price
            'Add Cost Total

            oGridN.doAddListField("r.U_SAINV", "Supplier Invoice No", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Invoice)

            ''######### END OF DONT SHOW ################
            oGridN.doAddListField("r.U_CusQty", "Container Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Block", "Block", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_RTime", "Req. Time", False, 0, "TIME", Nothing) 't
            oGridN.doAddListField("r.U_ASTime", "Act. Time", False, 0, "TIME", Nothing) 't
            oGridN.doAddListField("r.U_AETime", "Act ETime", False, 0, "TIME", Nothing) 't

            oGridN.doAddListField("r.U_Driver", "Driver", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Phone1", "Tel No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_SiteTl", "Site Tel No.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_ItmGrp", "Container Grp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("r.U_Serial", "Item Serial No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)


            oGridN.doAddListField("r.U_RdWgt", "Read Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Total Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_VehTyp", "Vehicle", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WROrd", "WO No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WRRow", "WO Row No.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_CCNum", "CC Num.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CCType", "CC Type", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CCStat", "CC Status", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtRt", "Tipping Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtRt", "Haulage Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtRt", "Tipping Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtRt", "Supplier Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtRt", "Haulage Vat Purchase Rate", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtGrp", "Tipping Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtGrp", "Haulage Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtGrp", "Tipping Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtGrp", "Supplier Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtGrp", "Haulage Vat Purchase Group", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WHTRNF", "Stock Transfer", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_StockTransfers)

            'CR 20150601: The DO Journals functionality build for USA Clients can now be used for all under new WR Config Key "NEWDOJRN" 
            ''For USA Release 
            'If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
            If Config.INSTANCE.getParameterAsBool("NEWDOJRN", False) = True Then
                oGridN.doAddListField("r.U_JStat", "Journals Status", False, 0, Nothing, Nothing)
                oGridN.doAddListField("r.U_Jrnl", "Journals", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_JournalPosting)
            End If
            oGridN.doAddListField("r.U_ProPO", "Producer PO", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder)
            oGridN.doAddListField("r.U_GRIn", "Goods Receipt In", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_GoodsReceipt)
            oGridN.doAddListField("r.U_ProGRPO", "Producer GR PO", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_GoodsReceiptPO)
            oGridN.doAddListField("r.U_SODlvNot", "Delivery note", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_DeliveryNotes)
            oGridN.doAddListField("r.U_TCCN", "AR Credit Note", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_InvoiceCreditMemo)
            oGridN.doAddListField("r.U_TPCN", "AP Credit Note", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_PurchaseInvoiceCreditMemo)
            oGridN.doAddListField("r.U_MDChngd", "Original linked invoice", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_PurchaseInvoiceCreditMemo)
            oGridN.doAddListField("(SELECT CASE count(*) WHEN 0 THEN ' ' ELSE 'Y' END FROM [@IDH_WOADDEXP] WITH(NOLOCK) WHERE U_RowNr = r.Code AND (U_SONr!='' OR U_SINVNr!='' Or U_PONr!='' OR U_PINVNr!=''))", "Add. Marketing Doc's", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PARCPT", "Payment", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_Receipt)
            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, 0, Nothing, Nothing, 0, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("CAST(((U_TCTotal + U_Price) - r.U_DisAmt) As Numeric(19,2))", "Sub After Discount", False, 0, Nothing, "SubAftDisc")
            oGridN.doAddListField("(U_TCTotal +(U_CusQty * U_CusChr)) - r.U_DisAmt", "Sub After Discount", False, 0, Nothing, "SubBefDisc")
            oGridN.doAddListField("r.U_SLicCh", "Skip Lic Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Lorry", "Veh Reg.", False, 0, Nothing, Nothing) 't
            oGridN.doAddListField("r.U_LorryCd", "Veh Reg. Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PayStat", "Payment Status", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Total Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_JCost", "Total Cost", False, 0, Nothing, Nothing)
            '## MA Start 22-04-2014
            oGridN.doAddListField("r.U_AddCost", "Additional Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddCharge", "Additional Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ValDed", "Charge Deduction", False, 0, Nothing, Nothing)
            '## MA End 22-04-2014


        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            doAddTotalFields(oForm)
			'## MA Start 01-09-2014 Issue#421
            doAddUF(oForm, "IDH_BRANC1", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 2000, False, False)
            '## MA Start 01-09-2014 Issue#421

            'Add Null filter field for DPR Ref 
            Dim oItem As SAPbouiCOM.Item
            oItem = doAddUFCheck(oForm, "IDH_RLBILL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
            oItem.AffectsFormMode = False

            oItem = doAddUFCheck(oForm, "IDH_CHKDPR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
            oItem.AffectsFormMode = False

            oItem = doAddUF(oForm, "IDH_LKPSBL", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 100, False, False)
            oItem.AffectsFormMode = False

            doAddComboBoxValidValues(oForm)
        End Sub

        Private Sub doAddComboBoxValidValues(ByVal oForm As SAPbouiCOM.Form)
            'Is Scheduled Combobox as Filter
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_SCHADH")
            oItem.DisplayDesc = True
            Dim oJobType As SAPbouiCOM.ComboBox = oItem.Specific

            doClearValidValues(oJobType.ValidValues)
            Try
                oJobType.ValidValues.Add("", "All")
                oJobType.ValidValues.Add("Y", "Scheduled")
                oJobType.ValidValues.Add("N", "Adhoc")
                oJobType.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index)
            Catch ex As Exception
            End Try

            'Is Rebate Combo as Filter
            oItem = oForm.Items.Item("IDH_REBAT")
            oItem.DisplayDesc = True
            oJobType = oItem.Specific

            doClearValidValues(oJobType.ValidValues)
            Try
                Dim sWord As String
                oJobType.ValidValues.Add("", "All")
                sWord = Config.Parameter("OBTRUE")
                If sWord Is Nothing OrElse sWord.Length = 0 Then
                    sWord = getTranslatedWord("Obligated")
                End If
                oJobType.ValidValues.Add(sWord, sWord)

                sWord = Config.Parameter("OBFALSE")
                If sWord Is Nothing OrElse sWord.Length = 0 Then
                    sWord = getTranslatedWord("Non-Obligated")
                End If
                oJobType.ValidValues.Add(sWord, sWord)
                oJobType.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index)
            Catch ex As Exception
            End Try

        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)

			'## MA Start 02-09-2014 Issue#421
            doSetMultiBranch(oForm)
            '## MA End 02-09-2014 Issue#421
            doSelfBillCombo(oForm)
            
            'doFillObligatedCombo(oForm)
            'Dim sDefStatus As String = Config.Parameter("OSMDFSTA")
            'If Not sDefStatus Is Nothing AndAlso sDefStatus.Length > 0 Then
            '    setUFValue(oForm, "IDH_HDSTA", sDefStatus)
            'Else
            '    setUFValue(oForm, "IDH_HDSTA", "1,2,3,4,5,9")
            'End If
            doPBIStatusCombo(oForm)
            'Note: added a view to get results for pbi and non-pbi jobs 
            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New OrderRowGrid(Me, oForm, "LINESGRID", "r")
            End If
            'LINESGRID View gives the RowCode and accosiated PBI with its Frequesncy value
            'so based on this filter can be apply
            oGridN.doAddGridTable(New GridTable("IDH_BILMGRJOB", "p", "RCode", True, True))
            oGridN.doAddGridTable(New GridTable("OITM", "i", "ItemCode", True, True))

            'oGridN.doAddFilterField("IDH_PZIP", "e.U_CZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255) 'Carrier PostCode 
            'oGridN.doAddFilterField("IDH_PBISTA", "p.PBIType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            'oGridN.setRequiredFilter(" r.U_JobNr = e.Code And v.Code = r.Code AND r.Code = p.RCode AND r.U_ItemCd=i.ItemCode AND (r." + IDH_JOBSHD._CustCd + " = bp.CardCode Or (r." + IDH_JOBSHD._CustCd + " = '' AND r." + IDH_JOBSHD._ProCd + " = bp.CardCode))")
            oGridN.setRequiredFilter(" r.U_JobNr = e.Code AND r.Code = p.RCode AND r.U_ItemCd=i.ItemCode AND (r." + IDH_JOBSHD._CustCd + " = bp.CardCode Or (r." + IDH_JOBSHD._CustCd + " = '' AND r." + IDH_JOBSHD._ProCd + " = bp.CardCode))")
        End Sub

        Protected Overrides Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean)
            'MyBase.doReLoadData(oForm, bIsFirst)

            Try
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

                'Mitie: Chage Request 20130430
                'Adding Checkbox to include NULL/EmptyString filter for DPR Ref No. Fields 
                'START
                Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                If oGridN Is Nothing Then
                    oGridN = New OrderRowGrid(Me, oForm, "LINESGRID", "r")
                End If
                oGridN.doRemoveGridSource("CRD1", "ad")

                Dim sReqFilter As String = getWFValue(oForm, "ORIGREQ")
                If sReqFilter Is Nothing Then
                    sReqFilter = oGridN.getRequiredFilter()
                    If sReqFilter Is Nothing Then
                        sReqFilter = ""
                    End If
                    setWFValue(oForm, "ORIGREQ", sReqFilter)
                End If

                Dim oItem As SAPbouiCOM.Item

                'Self Bill Filter 
                oItem = oForm.Items.Item("IDH_LKPSBL")
                Dim oLKPSelfBill As SAPbouiCOM.ComboBox = oItem.Specific
                Dim oValid As SAPbouiCOM.ValidValues
                Dim oSelected As SAPbouiCOM.ValidValue
                oValid = oLKPSelfBill.ValidValues
                oSelected = oLKPSelfBill.Selected

                Dim sSelfBill As String

                If Not oValid Is Nothing AndAlso _
                    oValid.Count > 0 AndAlso _
                    Not oSelected Is Nothing Then
                    sSelfBill = oLKPSelfBill.Selected.Value()
                Else
                    sSelfBill = ""
                End If

                If sSelfBill = "" Then
                    oGridN.doRemoveGridTable("IDH_VSELFBIL", "s")
                    If sReqFilter.Contains(" AND (not r.Code in (Select s.RCode from [IDH_VSELFBIL] s where s.Customer=e.U_CardCd)) ") Then
                        sReqFilter = sReqFilter.Replace(" AND (not r.Code in (Select s.RCode from [IDH_VSELFBIL] s where s.Customer=e.U_CardCd)) ", "")
                    ElseIf sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(r.Code, '') AND IsNull(s.RCode, '') <> ''  ") Then
                        sReqFilter = sReqFilter.Replace(" AND IsNull(s.RCode, '') = IsNull(r.Code, '') AND IsNull(s.RCode, '') <> ''  ", "")
                    End If
                ElseIf sSelfBill = "Y" Then
                    oGridN.doAddGridTable(New GridTable("IDH_VSELFBIL", "s"))
                    'sReqFilter = sReqFilter + " AND IsNull(s.RCode, '') = IsNull(r.Code, '') AND IsNull(s.RCode, '') <> ''  "
                    If sReqFilter.Contains(" AND (not r.Code in (Select s.RCode from [IDH_VSELFBIL] s where s.Customer=e.U_CardCd)) ") Then
                        sReqFilter = sReqFilter.Replace(" AND (not r.Code in (Select s.RCode from [IDH_VSELFBIL] s where s.Customer=e.U_CardCd)) ", " AND IsNull(s.RCode, '') = IsNull(r.Code, '') AND IsNull(s.RCode, '') <> ''  ")
                    Else
                        If Not sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(r.Code, '') AND IsNull(s.RCode, '') <> ''  ") Then
                            sReqFilter = sReqFilter + " AND IsNull(s.RCode, '') = IsNull(r.Code, '') AND IsNull(s.RCode, '') <> ''  "
                        End If
                    End If
                ElseIf sSelfBill = "N" Then
                    oGridN.doRemoveGridTable("IDH_VSELFBIL", "s")
                    'sReqFilter = sReqFilter + " AND IsNull(s.RCode, '') = IsNull(r.Code, '') AND IsNull(s.RCode, '') = ''  "
                    If sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(r.Code, '') AND IsNull(s.RCode, '') <> ''  ") Then
                        sReqFilter = sReqFilter.Replace(" AND IsNull(s.RCode, '') = IsNull(r.Code, '') AND IsNull(s.RCode, '') <> ''  ", " AND (not r.Code in (Select s.RCode from [IDH_VSELFBIL] s where s.Customer=e.U_CardCd)) ")
                    Else
                        If Not sReqFilter.Contains(" AND (not r.Code in (Select s.RCode from [IDH_VSELFBIL] s where s.Customer=e.U_CardCd)) ") Then
                            sReqFilter = sReqFilter + " AND (not r.Code in (Select s.RCode from [IDH_VSELFBIL] s where s.Customer=e.U_CardCd)) "
                        End If
                    End If
                End If


                If sReqFilter IsNot Nothing AndAlso sReqFilter.Length > 0 Then
                    'get checkbox value 
                    oItem = oForm.Items.Item("IDH_CHKDPR")
                    Dim chkDPRRef As SAPbouiCOM.CheckBox = oItem.Specific
                    If chkDPRRef IsNot Nothing AndAlso chkDPRRef.Checked Then
                        sReqFilter = sReqFilter + " AND IsNull(r.U_DPRRef, '') = '' "
                    Else
                        sReqFilter = sReqFilter.Replace("AND IsNull(r.U_DPRRef, '') = ''", "")
                    End If
                    oGridN.setRequiredFilter(sReqFilter)
                End If
                'END 

                'If bIsFirst = False Then
                '    oGridN.getGridControl().doCreateFilterString()
                '    If oGridN.doCheckForQueryChange() = False Then
                '        Return
                '    End If
                'End If
				'##MA Issue No 869 Start 30-07-2015
                If Config.ParameterAsBool("ENBODRWO", False) AndAlso Not Config.ParameterAsBool("FORMSET", False) Then
                    oGridN.setOrderValue("Cast(r.Code as Int)")
                End If
                '##MA Issue No 869 End 30-07-2015
                Dim sFilter As String = oGridN.getRequiredFilter()
                doSetStatusFilter(oGridN)
                doUpdateStatuses(oGridN)
                oGridN.doReloadData()
                oGridN.setRequiredFilter(sFilter)

                If Config.Parameter("OSMATO") = "TRUE" Then
                    doGridTotals(oForm, False)
                    setVisible(oForm, "IDH_CALC", False)
                    'oForm.Items.Item("IDH_CALC").Visible = False
                Else
                    doGridTotals(oForm, True)
                    setVisible(oForm, "IDH_CALC", True)
                    'oForm.Items.Item("IDH_CALC").Visible = True
                End If

                doSwitchRealtimeBilling(oForm, False)
                setUFValue(oForm, "IDH_RLBILL", "N")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error reloading the Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", {Nothing})
            End Try
        End Sub

        Private Sub doPBIStatusCombo(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oPBIStaCombo As SAPbouiCOM.ComboBox
                oPBIStaCombo = oForm.Items.Item("IDH_PBISTA").Specific
                oPBIStaCombo.ValidValues.Add("", getTranslatedWord("All"))
                oPBIStaCombo.ValidValues.Add(WR1_PBI.IDH_PBI_Handler.PER_Months, WR1_PBI.IDH_PBI_Handler.PER_Months)
                oPBIStaCombo.ValidValues.Add(WR1_PBI.IDH_PBI_Handler.PER_Quarts, WR1_PBI.IDH_PBI_Handler.PER_Quarts)
            Catch ex As Exception
            End Try
        End Sub

        '        Protected Overrides Sub doTotals(ByVal oGridN As UpdateGrid, Optional ByVal bDoZero As Boolean = False)
        '            Dim dCstWgt As Double = 0
        '            Dim dAftDisc As Double = 0
        '            Dim dTotal As Double = 0
        '            Dim dDisAmt As Double = 0
        '            Dim dAddEx As Double = 0
        '            Dim dTxAmt As Double = 0
        '
        '            If bDoZero = False Then
        '                Dim iCount As Integer = oGridN.getRowCount()
        '                Dim oData As SAPbouiCOM.DataTable = oGridN.getDataTable()
        '                If iCount > 0 Then
        '                    For iRow As Integer = 0 To iCount - 1
        '                        dCstWgt = dCstWgt + oData.GetValue(oGridN.doIndexField("r.U_CstWgt"), iRow)
        '                        dTotal = dTotal + oData.GetValue(oGridN.doIndexField("r.U_Total"), iRow)
        '
        '                        dAftDisc = dAftDisc + oData.GetValue(oGridN.doIndexField("SubAftDisc"), iRow)
        '
        '                        dDisAmt = dDisAmt + oData.GetValue(oGridN.doIndexField("r.U_DisAmt"), iRow)
        '                        dAddEx = dAddEx + oData.GetValue(oGridN.doIndexField("r.U_AddEx"), iRow)
        '                        dTxAmt = dTxAmt + oData.GetValue(oGridN.doIndexField("r.U_TaxAmt"), iRow)
        '                    Next
        '                End If
        '            End If
        '
        '            setUFValue(oGridN.getSBOForm(), "IDH_CstWgt", dCstWgt)
        '            setUFValue(oGridN.getSBOForm(), "IDH_SubAD", dAftDisc)
        '            setUFValue(oGridN.getSBOForm(), "IDH_Total", dTotal)
        '            setUFValue(oGridN.getSBOForm(), "IDH_DisAmt", dDisAmt)
        '            setUFValue(oGridN.getSBOForm(), "IDH_AddEx", dAddEx)
        '            setUFValue(oGridN.getSBOForm(), "IDH_TaxAmt", dTxAmt)
        '        End Sub

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = False Then
                        doGridDoubleClick(oForm, pVal)
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                If pVal.BeforeAction = True Then
                    Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                    Dim oMenuItem As SAPbouiCOM.MenuItem
                    oMenuItem = goParent.goApplication.Menus.Item(IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU)

                    Dim oMenus As SAPbouiCOM.Menus
                    Dim iMenuPos As Integer = 1
                    Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                    oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
                    oCreationPackage.Enabled = True
                    oMenus = oMenuItem.SubMenus
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING

                    Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
                    Dim iSelectionCount As Integer = oSelected.Count

                    Dim iCurrentDataRow As Integer
                    'Fix for Right Click Menu when Grid is in Group Mode 
                    If oGridN.getSBOGrid().CollapseLevel > 0 Then
                        iCurrentDataRow = oGridN.GetDataTableRowIndex(pVal.Row)
                    Else
                        iCurrentDataRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)
                    End If

                    If iSelectionCount > 0 Then
                        setWFValue(oForm, "LASTJOBMENU", Nothing)

                        'Dim iRow As Integer
                        'iRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)

                        oCreationPackage.UniqueID = "DOORDERS"
                        oCreationPackage.String = getTranslatedWord("Generate S/P &Orders for Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOSO"
                        oCreationPackage.String = getTranslatedWord("Generate &Sales Orders from Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOPO"
                        oCreationPackage.String = getTranslatedWord("Generate &Purchase Orders from Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        'CR 20150601: The DO Journals functionality build for USA Clients can now be used for all under new WR Config Key "NEWDOJRN" 
                        'If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
                        If Config.INSTANCE.getParameterAsBool("NEWDOJRN", False) = True Then
                            oCreationPackage.UniqueID = "JRNL"
                            oCreationPackage.String = getTranslatedWord("Generate &Journals from Selection")
                            oCreationPackage.Position = iMenuPos
                            iMenuPos += 1
                            oMenus.AddEx(oCreationPackage)
                        End If

                        oCreationPackage.UniqueID = "DOFOC"
                        oCreationPackage.String = getTranslatedWord("Set as &Free Of Charge from Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOINV"
                        oCreationPackage.String = getTranslatedWord("Generate &Invoices from Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOPAY"
                        oCreationPackage.String = getTranslatedWord("Proccess the Payments")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOARREB"
                        oCreationPackage.String = getTranslatedWord("Generate AR Rebates from Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOAPREB"
                        oCreationPackage.String = getTranslatedWord("Generate AP Rebates from Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        oCreationPackage.UniqueID = "DOREBS"
                        oCreationPackage.String = getTranslatedWord("Generate AR/AP Rebates from Selection")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_ARCHIVE) = False OrElse _
                            Config.INSTANCE.doGetIsSupperUser() Then
                            oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE
                            oCreationPackage.String = getTranslatedWord("Archive Rows")
                            oCreationPackage.Enabled = True
                            oMenus.AddEx(oCreationPackage)
                        End If
                    Else
                        Dim sRowNr As String
                        sRowNr = oGridN.doGetFieldValue("r.Code", iCurrentDataRow)

                        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_ARCHIVE) = False OrElse _
                            Config.INSTANCE.doGetIsSupperUser() Then
                            If iCurrentDataRow >= 0 Then
                                oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE
                                oCreationPackage.String = getTranslatedWord("Archive Row ") & " (" & (iCurrentDataRow + 1) & ")[" & sRowNr & "]"
                                oCreationPackage.Enabled = True
                                oMenus.AddEx(oCreationPackage)
                            End If
                        End If
                    End If
                    Return False
                Else
                    Dim oMenuList As ArrayList = getWFValue(oForm, "LASTJOBMENU")
                    Dim sJob As String
                    If Not oMenuList Is Nothing AndAlso oMenuList.Count > 0 Then
                        For iJob As Integer = 0 To oMenuList.Count - 1
                            sJob = oMenuList.Item(iJob)(0)
                            If goParent.goApplication.Menus.Exists(sJob) Then
                                goParent.goApplication.Menus.RemoveEx(sJob)
                            End If
                        Next
                    End If
                    If goParent.goApplication.Menus.Exists("DOORDERS") Then
                        goParent.goApplication.Menus.RemoveEx("DOORDERS")
                    End If
                    If goParent.goApplication.Menus.Exists("DOSO") Then
                        goParent.goApplication.Menus.RemoveEx("DOSO")
                    End If
                    If goParent.goApplication.Menus.Exists("DOPO") Then
                        goParent.goApplication.Menus.RemoveEx("DOPO")
                    End If
                    'CR 20150601: The DO Journals functionality build for USA Clients can now be used for all under new WR Config Key "NEWDOJRN" 
                    ''For USA Release 
                    'If Config.INSTANCE.getParameterAsBool("USAREL", False) = True AndAlso goParent.goApplication.Menus.Exists("JRNL") Then
                    If Config.INSTANCE.getParameterAsBool("NEWDOJRN", False) = True AndAlso goParent.goApplication.Menus.Exists("JRNL") Then
                        goParent.goApplication.Menus.RemoveEx("JRNL")
                    End If

                    If goParent.goApplication.Menus.Exists("DOFOC") Then
                        goParent.goApplication.Menus.RemoveEx("DOFOC")
                    End If
                    If goParent.goApplication.Menus.Exists("DOINV") Then
                        goParent.goApplication.Menus.RemoveEx("DOINV")
                    End If
                    If goParent.goApplication.Menus.Exists("DOPAY") Then
                        goParent.goApplication.Menus.RemoveEx("DOPAY")
                    End If
                    If goParent.goApplication.Menus.Exists("DOARREB") Then
                        goParent.goApplication.Menus.RemoveEx("DOARREB")
                    End If
                    If goParent.goApplication.Menus.Exists("DOAPREB") Then
                        goParent.goApplication.Menus.RemoveEx("DOAPREB")
                    End If
                    If goParent.goApplication.Menus.Exists("DOREBS") Then
                        goParent.goApplication.Menus.RemoveEx("DOREBS")
                    End If

                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE)
                    End If

                    setWFValue(oForm, "LASTJOBMENU", Nothing)
                    Return True
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                If pVal.BeforeAction = False Then
                    Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                    If pVal.oData.MenuUID.Equals("DOORDERS") Then
                        If oGridN.doProcessPOForIntraComapny() AndAlso doValidateWasteDescription(oForm, oGridN) = True Then '#MA issue:324 20170426
                            oGridN.doMarkSelectionAsOrder(True, True)
                            doSwitchRealtimeBilling(oForm, True)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        End If
                    ElseIf pVal.oData.MenuUID.Equals("DOSO") Then
                        If doValidateWasteDescription(oForm, oGridN) = True Then
                            oGridN.doMarkSelectionAsOrder(True, False)
                            doSwitchRealtimeBilling(oForm, True)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        End If
                    ElseIf pVal.oData.MenuUID.Equals("DOPO") Then
                        If oGridN.doProcessPOForIntraComapny() AndAlso doValidateWasteDescription(oForm, oGridN) = True Then '#MA issue:324 20170426
                            oGridN.doMarkSelectionAsOrder(False, True)
                            doSwitchRealtimeBilling(oForm, True)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        End If
                    ElseIf pVal.oData.MenuUID.Equals("JRNL") Then
                            'For USA Release 
                            oGridN.doMarkSelectionAsJournal()
                            doSwitchRealtimeBilling(oForm, True)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        ElseIf pVal.oData.MenuUID.Equals("DOFOC") Then
                            oGridN.doMarkSelectionAsFoc()
                            doSwitchRealtimeBilling(oForm, True)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        ElseIf pVal.oData.MenuUID.Equals("DOINV") Then
                            oGridN.doMarkSelectionAsInvoice()
                            doSwitchRealtimeBilling(oForm, True)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        ElseIf pVal.oData.MenuUID.Equals("DOPAY") Then
                            oGridN.doMarkSelectionAsPay()
                            doSwitchRealtimeBilling(oForm, True)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        ElseIf pVal.oData.MenuUID.Equals("DOARREB") Then
                            oGridN.doMarkSelectionAsRebate(True, False)
                            doSwitchRealtimeBilling(oForm, True)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        ElseIf pVal.oData.MenuUID.Equals("DOAPREB") Then
                            oGridN.doMarkSelectionAsRebate(False, True)
                            doSwitchRealtimeBilling(oForm, True)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        ElseIf pVal.oData.MenuUID.Equals("DOREBS") Then
                            oGridN.doMarkSelectionAsRebate(True, True)
                            doSwitchRealtimeBilling(oForm, True)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        ElseIf pVal.oData.MenuUID.Equals(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE) Then
                            oGridN.doRequestArchive()
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                            Return False
                        ElseIf pVal.oData.MenuUID.Equals("DOEDIT") Then
                            'Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                            If oGridN.getSBOGrid().Rows.SelectedRows.Count = 1 Then
                                oGridN.setCurrentLineByClick(pVal.Row)

                                Dim sVehReg As String = oGridN.doGetFieldValue("r.U_Lorry")
                                setSharedData(oForm, "IDH_VEHREG", sVehReg)
                            End If
                            goParent.doOpenModalForm("IDH_OSMEDIT", oForm)
                            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                        ElseIf pVal.oData.MenuUID.Equals("IDHSORTA") AndAlso (oForm.TypeEx.IndexOf("IDHJOBR") > -1 OrElse
                          oForm.TypeEx.IndexOf("IDHWOBI") > -1) Then
                            Dim sFilter As String = oGridN.getRequiredFilter()
                            doSetStatusFilter(oGridN)
                            oGridN.doSort(pVal.ColUID, "ASC")
                            oGridN.setRequiredFilter(sFilter)
                            Return False
                        ElseIf pVal.oData.MenuUID.Equals("IDHSORTD") AndAlso (oForm.TypeEx.IndexOf("IDHJOBR") > -1 OrElse
                              oForm.TypeEx.IndexOf("IDHWOBI") > -1) Then
                            Dim sFilter As String = oGridN.getRequiredFilter()
                        doSetStatusFilter(oGridN)
                        oGridN.doSort(pVal.ColUID, "DESC")
                        oGridN.setRequiredFilter(sFilter)
                        Return False
                    End If
                    'doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                    'End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FORMAT_FIELDS Then
                If pVal.BeforeAction = False Then
                End If
            End If
            Return True
        End Function

        Private Sub doSwitchRealtimeBilling(ByVal oForm As SAPbouiCOM.Form, ByVal bVisible As Boolean)
            Dim bDoInBack As Boolean = Config.INSTANCE.getParameterAsBool("MDCMIBG", False)
            If bDoInBack = False Then
                bVisible = False
            End If

            Try
                oForm.Items.Item("IDH_RLBILL").Visible = bVisible
            Catch ex As Exception
            End Try
        End Sub

        'Protected Overridable Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Protected Overrides Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base)
            Dim oGridN As UpdateGrid = pVal.oGrid 'UpdateGrid.getInstance(oForm, "LINESGRID")
            Dim bContiue As Boolean = True
            If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                If Not pVal.ColUID Is Nothing AndAlso pVal.ColUID.IndexOf("[@IDH_WOADDEXP]") > -1 Then
                    'Already set
                    'oGridN.setCurrentLineByClick(pVal.Row)
                    'oGridN.setCurrentLine(pVal.Row)

                    Dim sRowCode As String = oGridN.doGetFieldValue("r.Code")

                    Dim sFlag As String = oGridN.doGetFieldValue(pVal.oData)

                    If Not sFlag Is Nothing AndAlso sFlag.Length > 0 Then
                        setSharedData(oForm, "IDH_WOR", sRowCode)

                        Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.popup.AdditionalMarkDocs = New WR1_FR2Forms.idh.forms.fr2.popup.AdditionalMarkDocs(Nothing, oForm.UniqueID, Nothing)
                        oOOForm.doShowModal(oForm)
                        bContiue = False
                    End If
                End If
            End If
            If bContiue Then
                MyBase.doGridDoubleClick(oForm, pVal)
            End If

            '            If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
            '                oGridN.setCurrentLineByClick(pVal.Row)
            '
            '                Dim sRowCode As String = oGridN.doGetFieldValue("r.Code")
            '                Dim sJobEntr As String = oGridN.doGetFieldValue("r.U_JobNr")
            '
            '                Dim oData As New ArrayList
            '                If sJobEntr.Length = 0 Then
            '                    com.idh.bridge.DataHandler.INSTANCE.doError("A Job Entry must be selected.")
            '                Else
            '                	If Not pVal.ColUID Is Nothing AndAlso pVal.ColUID.IndexOf("[@IDH_WOADDEXP]") > -1 Then
            '                		Dim sFlag As String = oGridN.doGetFieldValue(pVal.oData)
            '                		If Not sFlas Is Nothing AndAlso sFlag.Length > 0 Then
            '	                    	setSharedData( oForm, "IDH_WOR", sRowCode)
            '	        
            '	        				Dim oOOForm As idh.forms.fr2.popup.AdditionalMarkDocs = New idh.forms.fr2.popup.AdditionalMarkDocs(Nothing, oForm.UniqueID, Nothing)
            '	        				oOOForm.doShowModal(oForm)
            '	        			End If
            '                	Else	
            '	                	oData.Add("DoUpdate")
            '	                	If oGridN.doCheckIsSameCol(pVal.ColUID, "e.U_ZpCd") Then
            '	                		
            '	                	ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "e.U_ZpCd") Then
            '	                        oData.Add("PC=" & oGridN.doGetFieldValue("e.U_ZpCd"))
            '	                        goParent.doOpenModalForm("IDHMAP", oForm, oData)
            '	                    ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "r.U_JobNr") Then
            '	                        oData.Add(sJobEntr)
            '	                        goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
            '	                    Else
            '	                        If sRowCode.Length > 0 Then
            '	                            oData.Add(sRowCode)
            '	                            oData.Add(oGridN.doGetFieldValue("e.U_BDate"))
            '	                            oData.Add(goParent.doTimeStrToStr(oGridN.doGetFieldValue("e.U_BTime")))
            '	                            oData.Add(oGridN.doGetFieldValue("e.U_ZpCd"))
            '	                            oData.Add(oGridN.doGetFieldValue("e.U_Address"))
            '	                            oData.Add("CANNOTDOCOVERAGE")
            '	                            oData.Add(oGridN.doGetFieldValue("e.U_SteId"))
            '	                            
            '	                            'Added to handle the BP Switching
            '	                            oData.Add(Nothing)
            '	                            oData.Add(oGridN.doGetFieldValue("e.U_PAddress"))
            '	                            oData.Add(oGridN.doGetFieldValue("e.U_PZpCd"))
            '	                            oData.Add(oGridN.doGetFieldValue("e.U_PPhone1"))
            '	                            oData.Add(oGridN.doGetFieldValue("e.U_FirstBP"))
            '	                            
            '	                            goParent.doOpenModalForm("IDHJOBS", oForm, oData)
            '	                        End If
            '	                    End If
            '                	End If
            '                End If
            '            End If
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
                    If getUFValue(oForm, "IDH_RLBILL") = "Y" Then
                        'Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                        oGridN.mbForceRealTimeBilling = True
                    Else
                        oGridN.mbForceRealTimeBilling = False
                    End If
                    If doValidateWasteDescription(oForm, oGridN) = False Then
                        BubbleEvent = False
                        Exit Sub
                    End If
                End If
                MyBase.doButtonID1(oForm, pVal, BubbleEvent)
            End If
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim bResult As Boolean = MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_DPR" Then
                        doDPRReport(oForm)
                        Return False
                    End If
                End If
            End If
            Return bResult
        End Function

        Protected Overridable Sub doDPRReport(ByVal oForm As SAPbouiCOM.Form)
            Dim bDoReload As Boolean = False
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim sReportFile As String
            sReportFile = Config.Parameter("DPRRPT")

            Dim oSelectedRows As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows
            If oSelectedRows Is Nothing OrElse oSelectedRows.Count = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("No rows selected.")
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("No rows selected. ", "ERUSROWS", {Nothing})
                Exit Sub
            End If

            Dim iSelected As Integer = oSelectedRows.Count
            Dim iRowNum As Integer
            Dim sRowCode As String
            Dim sDPRCode As String
            Dim sNewDPRCode As String = Nothing
            Dim aListNumbers As New ArrayList

            For iIndex As Integer = 0 To iSelected - 1
                iRowNum = oGridN.getDataTableRowIndex(oSelectedRows, iIndex)

                sRowCode = oGridN.doGetFieldValue("r.Code", iRowNum)
                sDPRCode = oGridN.doGetFieldValue("r.U_DPRRef", iRowNum)

                If sDPRCode Is Nothing OrElse sDPRCode.Length = 0 Then
                    If sNewDPRCode Is Nothing Then
                        sNewDPRCode = DataHandler.INSTANCE.getSingleCode("DPRREF")
                    End If
                    sDPRCode = sNewDPRCode

                    Dim sQry As String = "UPDATE [@IDH_JOBSHD] SET U_DPRRef = '" & sDPRCode & "' WHERE Code = '" & sRowCode & "'"
                    bDoReload = DataHandler.INSTANCE.doUpdateQuery(sQry)

                End If

                If aListNumbers.Contains(sDPRCode) = False Then
                    aListNumbers.Add(sDPRCode)
                End If
            Next

            Dim oParams As New Hashtable
            oParams.Add("DPRRef", aListNumbers)
            If (Config.INSTANCE.useNewReportViewer()) Then
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
            Else
                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
            End If

            If bDoReload Then
                doReLoadData(oForm, False)
            End If
        End Sub

        Public Overrides Sub doGetStatus(ByVal oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_DELETE) Then
                Try
                    doFillCombo(oForm, "IDH_HDSTA", "[@IDH_WRSTATUS]", "Code", "Name", Nothing, "CAST(Code As Numeric)", getTranslatedWord("Default"), "1,2,3,4,5,9")
                Catch ex As Exception
                End Try
            Else
                Try
                    Dim oItem As SAPbouiCOM.Item
                    oItem = oForm.Items.Item("IDH_HDSTA")
                    Dim oCombo As SAPbouiCOM.ComboBox
                    oCombo = oItem.Specific()

                    Dim oValidValues As SAPbouiCOM.ValidValues
                    oValidValues = oCombo.ValidValues
                    doClearValidValues(oValidValues)

                    oValidValues.Add("1,2,3,4,5,9", com.idh.bridge.lookups.FixedValues.getStatusOpen())
                    oValidValues.Add("9", com.idh.bridge.lookups.FixedValues.getStatusClosed())
                Catch ex As Exception
                End Try
            End If
        End Sub

        Private Sub doFillObligatedCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_OBLIG")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oItem.Specific
            doClearValidValues(oCombo.ValidValues)

            Dim sWord As String

            sWord = getTranslatedWord("None")
            oCombo.ValidValues.Add("", sWord)

            sWord = Config.Parameter("OBTRUE")
            If sWord Is Nothing OrElse sWord.Length = 0 Then
                sWord = getTranslatedWord("Obligated")
            End If
            oCombo.ValidValues.Add(sWord, sWord)

            sWord = Config.Parameter("OBFALSE")
            If sWord Is Nothing OrElse sWord.Length = 0 Then
                sWord = getTranslatedWord("Non-Obligated")
            End If
            oCombo.ValidValues.Add(sWord, sWord)
        End Sub

        Private Sub doSelfBillCombo(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oBPStaCombo As SAPbouiCOM.ComboBox
                oBPStaCombo = oForm.Items.Item("IDH_LKPSBL").Specific
                oBPStaCombo.ValidValues.Add("", getTranslatedWord("Both"))
                oBPStaCombo.ValidValues.Add("Y", getTranslatedWord("Yes"))
                oBPStaCombo.ValidValues.Add("N", getTranslatedWord("No"))
            Catch ex As Exception
            End Try
        End Sub

    End Class
End Namespace
