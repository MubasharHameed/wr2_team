﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.controls
Imports com.idh.controls.strct
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge.data
Imports com.idh.bridge
Imports WR1_Grids.idh.controls.grid

Imports com.idh.bridge.lookups
Imports com.idh.bridge.res
'Imports com.idh.bridge.resources

Namespace idh.forms.manager
    Public Class ARDocWizard
        Inherits IDHAddOns.idh.forms.Base
        Dim oGridN As FilterGrid
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHARDWZ", "IDHPS", iMenuPosition, "ARDocWizard.srf", False, True, False, "Mass Sales Order Manager", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                'Add total field 
                doAddUF(oForm, "IDH_TOTAL", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)

                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "ARGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "ARGRID", 7, 70, 1050, 322, False)
                End If

                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto

                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False

                '#MA Start 30-03-2017
                Dim fSettings As SAPbouiCOM.FormSettings
                fSettings = oGridN.getSBOForm().Settings
                fSettings.Enabled = True
                '#MA End 30-03-2017

                oForm.SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_All

                Dim oItem As SAPbouiCOM.Item

                ''If Config.INSTANCE.getParameterAsBool("ARCLSDOC", False) = False Then
                ''End If

                'Add Flag to close PO field 
                oItem = doAddUFCheck(oForm, "IDH_SOFLAG", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.Visible = True
                oItem.AffectsFormMode = False

                'Add Null filter field for DPR Ref 
                'Dim oItem As SAPbouiCOM.Item
                oItem = doAddUFCheck(oForm, "IDH_SOOHLD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False

                ''IDH_EXSELF
                'oItem = doAddUFCheck(oForm, "IDH_EXSELF", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                'oItem.AffectsFormMode = False

                oItem = doAddUF(oForm, "IDH_LKPSBL", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 100, False, False)
                oItem.DisplayDesc = True
                oItem.AffectsFormMode = False
                doSelfBillCombo(oForm)

                oItem = doAddUF(oForm, "IDH_SCHADH", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 100, False, False)
                oItem.DisplayDesc = True
                oItem.AffectsFormMode = False
                doAddComboBoxValidValues(oForm)
                'Add Combox Filter for Filtering Site on Stop. 'MA  20170303 start
                oItem = doAddUF(oForm, "IDH_SITESP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 5, False, False)
                oItem.DisplayDesc = True
                oItem.AffectsFormMode = False
                doSiteStopCombo(oForm)
                'MA  20170303  End
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)
                'oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", Nothing)
            End Try
        End Sub
        Protected Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                oForm.Items.Item("1").Specific.Caption = com.idh.bridge.Translation.getTranslatedWord("Find")
                'oForm.Items.Item("2").Visible = False
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                oForm.Items.Item("1").Specific.Caption = com.idh.bridge.Translation.getTranslatedWord("Update")
                'oForm.Items.Item("2").Visible = True
                ' doSetUpdate(oForm)
            End If
        End Sub
        Protected Overridable Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_ASDT", "vARG.ActStartDt", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_AEDT", "vARG.ActEndDt", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_CUSTCD", "vARG.CustCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CUSTNM", "vARG.CustNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CONTCD", "vARG.ContainerCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CONTNM", "vARG.ContainerNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WOHNO", "vARG.OrderNo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WORNO", "vARG.OrderRowNo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SITEID", "vARG.CustSiteID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WASCD", "vARG.WasteCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ADDRES", "vARG.CustAddress", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_STZPCD", "vARG.CustZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_UNTSIP", "vARG.UnitSIP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_SOOHLD", "vARG.OnHold", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 1)
            oGridN.doAddFilterField("IDH_SORNUM", "vARG.DocEntry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_SUPPCD", "vARG.SupplierCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SUPPNM", "vARG.SupplierNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_LKPSBL", "vARG.SelfBillCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 1)
            oGridN.doAddFilterField("IDH_SCHADH", "vARG.IsScheduled", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 1)
            oGridN.doAddFilterField("IDH_REASON", "vARG.HoldComments", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
        End Sub

        '#MA Start 30-03-2017
        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                Dim sFormTypeId As String = oGridN.getSBOForm().TypeEx
                Dim oform As SAPbouiCOM.Form = oGridN.getSBOForm
                Dim oFormSettings As com.idh.dbObjects.User.IDH_FORMSET = getWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId)
                If oFormSettings Is Nothing Then
                    oFormSettings = New com.idh.dbObjects.User.IDH_FORMSET()
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "")
                    setWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId, oFormSettings)
                    oFormSettings.doAddFieldToGrid(oGridN)
                    doAddListFields(oGridN)
                    oFormSettings.doSyncDB(oGridN)
                Else
                    If oFormSettings.SkipFormSettings Then
                        doAddListFields(oGridN)
                    Else
                        oFormSettings.doAddFieldToGrid(oGridN)
                    End If
                End If
            Else
                doAddListFields(oGridN)
            End If
        End Sub
        '#MA End 30-03-2017

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            Try
                'doSelfBillCombo(oForm)

                ' ''Set Flag to Close PO visability 
                ''If Config.INSTANCE.getParameterAsBool("ARCLSDOC", False) Then
                ''    Dim oItm As SAPbouiCOM.Item
                ''    oItm = oForm.Items.Item("IDH_SOFLAG")
                ''    oItm.Visible = False
                ''End If

                oGridN = FilterGrid.getInstance(oForm, "ARGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "ARGRID", 7, 70, 1050, 322, True)
                End If

                oGridN.doAddGridTable(New GridTable("IDH_ARDOCGEN", "vARG", "DocEntry", False, True), True)

                Dim sReqFilter As String = " vARG.IsDrafted != 'Zz-Ignore' "
                oGridN.setRequiredFilter(sReqFilter)

                doSetFilterFields(oGridN)
                doSetListFields(oGridN)

                oGridN.setInitialFilterValue("vARG.DocEntry = -1")

                If ghOldDialogParams.Contains(oForm.UniqueID) Then
                    Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                    If oData IsNot Nothing AndAlso oData.Count > 0 Then
                        Dim sSuppCd As String = oData.Item(0)
                        Dim sSuppNm As String = oData.Item(1)

                        setUFValue(oForm, "IDH_SUPPNM", sSuppNm)
                        oGridN.doReloadData()
                    End If
                End If

                If getHasSharedData(oForm) Then
                    setUFValue(oForm, "IDH_SUPPNM", getParentSharedData(oForm, "IDH_SUPPNM"))
                    oGridN.doReloadData()
                End If

                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Generate SI Document...")
                doAddWF((oForm), "APHasChanged", False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error preloading data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBPL", Nothing)
            End Try
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "ARGRID")
                Dim oItem As SAPbouiCOM.Item
                Dim sReqFilter As String = oGridN.getRequiredFilter()

                ' ''get Flag to Close checkbox value 
                ''If Config.INSTANCE.getParameterAsBool("ARCLSDOC", False) = False Then
                ''End If

                oItem = oForm.Items.Item("IDH_SOFLAG")
                Dim chkToClose As SAPbouiCOM.CheckBox = oItem.Specific

                If chkToClose IsNot Nothing AndAlso chkToClose.Checked Then
                    If sReqFilter.Contains(" AND (IsNull(vARG.FlagToClose, '') = '' OR IsNull(vARG.FlagToClose, '') = 'N') ") Then
                        sReqFilter = sReqFilter.Replace(" AND (IsNull(vARG.FlagToClose, '') = '' OR IsNull(vARG.FlagToClose, '') = 'N')", " AND IsNull(vARG.FlagToClose, '') = 'Y' ")
                    Else
                        If Not sReqFilter.Contains(" AND IsNull(vARG.FlagToClose, '') = 'Y' ") Then
                            sReqFilter = sReqFilter + " AND IsNull(vARG.FlagToClose, '') = 'Y' "
                        End If
                    End If
                Else
                    If sReqFilter.Contains(" AND IsNull(vARG.FlagToClose, '') = 'Y' ") Then
                        sReqFilter = sReqFilter.Replace(" AND IsNull(vARG.FlagToClose, '') = 'Y' ", " AND (IsNull(vARG.FlagToClose, '') = '' OR IsNull(vARG.FlagToClose, '') = 'N') ")
                    Else
                        If Not sReqFilter.Contains(" AND (IsNull(vARG.FlagToClose, '') = '' OR IsNull(vARG.FlagToClose, '') = 'N') ") Then
                            sReqFilter = sReqFilter + " AND (IsNull(vARG.FlagToClose, '') = '' OR IsNull(vARG.FlagToClose, '') = 'N') "
                        End If
                    End If
                End If

                'get On-hold checkbox value 
                oItem = oForm.Items.Item("IDH_SOOHLD")
                Dim chkOnhold As SAPbouiCOM.CheckBox = oItem.Specific
                'sReqFilter = oGridN.getRequiredFilter() 

                If chkOnhold IsNot Nothing AndAlso chkOnhold.Checked Then
                    If sReqFilter.Contains(" AND (IsNull(vARG.OnHold, '') = '' OR IsNull(vARG.OnHold, '') = 'N') ") Then
                        sReqFilter = sReqFilter.Replace(" AND (IsNull(vARG.OnHold, '') = '' OR IsNull(vARG.OnHold, '') = 'N')", " AND IsNull(vARG.OnHold, '') = 'Y' ")
                    Else
                        If Not sReqFilter.Contains(" AND IsNull(vARG.OnHold, '') = 'Y' ") Then
                            sReqFilter = sReqFilter + " AND IsNull(vARG.OnHold, '') = 'Y' "
                        End If
                    End If
                Else
                    If sReqFilter.Contains(" AND IsNull(vARG.OnHold, '') = 'Y' ") Then
                        sReqFilter = sReqFilter.Replace(" AND IsNull(vARG.OnHold, '') = 'Y' ", " AND (IsNull(vARG.OnHold, '') = '' OR IsNull(vARG.OnHold, '') = 'N') ")
                    Else
                        If Not sReqFilter.Contains(" AND (IsNull(vARG.OnHold, '') = '' OR IsNull(vARG.OnHold, '') = 'N') ") Then
                            sReqFilter = sReqFilter + " AND (IsNull(vARG.OnHold, '') = '' OR IsNull(vARG.OnHold, '') = 'N') "
                        End If
                    End If
                End If

                ''Self Bill Filter 
                ''oItem = oForm.Items.Item("IDH_LKPSBL")
                ''Dim oLKPSelfBill As SAPbouiCOM.ComboBox = oItem.Specific
                ''Dim oValid As SAPbouiCOM.ValidValues
                ''Dim oSelected As SAPbouiCOM.ValidValue
                ''oValid = oLKPSelfBill.ValidValues
                ''oSelected = oLKPSelfBill.Selected

                ''Dim sSelfBill As String

                ''If Not oValid Is Nothing AndAlso _
                ''    oValid.Count > 0 AndAlso _
                ''    Not oSelected Is Nothing Then
                ''    sSelfBill = oLKPSelfBill.Selected.Value()
                ''Else
                ''    sSelfBill = ""
                ''End If

                ''If sSelfBill = "" Then
                ''    oGridN.doRemoveGridTable("IDH_VSELFBIL", "s")
                ''    If sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  ") Then
                ''        sReqFilter = sReqFilter.Replace(" AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  ", "")
                ''    ElseIf sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  ") Then
                ''        sReqFilter = sReqFilter.Replace(" AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  ", "")
                ''    End If
                ''ElseIf sSelfBill = "Y" Then
                ''    oGridN.doAddGridTable(New GridTable("IDH_VSELFBIL", "s"))
                ''    'sReqFilter = sReqFilter + " AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  "
                ''    If sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  ") Then
                ''        sReqFilter = sReqFilter.Replace(" AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  ", " AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  ")
                ''    Else
                ''        If Not sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  ") Then
                ''            sReqFilter = sReqFilter + " AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  "
                ''        End If
                ''    End If
                ''ElseIf sSelfBill = "N" Then
                ''    oGridN.doAddGridTable(New GridTable("IDH_VSELFBIL", "s"))
                ''    'sReqFilter = sReqFilter + " AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  "
                ''    If sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  ") Then
                ''        sReqFilter = sReqFilter.Replace(" AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  ", " AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  ")
                ''    Else
                ''        If Not sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  ") Then
                ''            sReqFilter = sReqFilter + " AND IsNull(s.RCode, '') = IsNull(vARG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  "
                ''        End If
                ''    End If
                ''End If
                'MA  20170303 start
                If getUFValue(oForm, "IDH_SITESP", True) <> Nothing AndAlso (getUFValue(oForm, "IDH_SITESP", True).ToString() <> "") Then  'Combo not empty 
                    Dim sSiteonStopQry As String = ""
                    If (getUFValue(oForm, "IDH_SITESP", True).ToString() = "Y") Then ' yes before
                        sSiteonStopQry = "vARG.CustAddress In( Select distinct CRD1.Address From CRD1 WITH(NOLOCK)  Where vARG.CustCd=CRD1.CardCode AND vARG.CustAddress=CRD1.Address and IsNull(CRD1.U_onStop,'')='Y' and CRD1.AdresType='S') "
                    Else
                        sSiteonStopQry = "vARG.CustAddress In( Select distinct CRD1.Address From CRD1 WITH(NOLOCK)  Where vARG.CustCd=CRD1.CardCode AND vARG.CustAddress=CRD1.Address and IsNull(CRD1.U_onStop,'')<>'Y' and CRD1.AdresType='S') "
                    End If
                    If sSiteonStopQry <> "" Then
                        If oGridN.getRequiredFilter().Trim <> "" Then
                            If sReqFilter.Contains("IsNull(CRD1.U_onStop,'')='Y'") Then
                                If sSiteonStopQry.Contains("IsNull(CRD1.U_onStop,'')<>'Y'") Then
                                    sReqFilter = sReqFilter.Replace("IsNull(CRD1.U_onStop,'')='Y'", "IsNull(CRD1.U_onStop,'')<>'Y'")
                                End If
                                sReqFilter = sReqFilter
                            ElseIf sReqFilter.Contains("IsNull(CRD1.U_onStop,'')<>'Y'") Then
                                If sSiteonStopQry.Contains("IsNull(CRD1.U_onStop,'')='Y'") Then
                                    sReqFilter = sReqFilter.Replace("IsNull(CRD1.U_onStop,'')<>'Y'", "IsNull(CRD1.U_onStop,'')='Y'")
                                End If
                                sReqFilter = sReqFilter
                            Else

                                sReqFilter = sReqFilter + " And " + sSiteonStopQry
                            End If

                        Else
                            oGridN.setRequiredFilter(sSiteonStopQry)
                        End If
                    End If
                Else 'Combo empty
                    If Not (getUFValue(oForm, "IDH_SITESP", True).ToString() = "Y") And Not (getUFValue(oForm, "IDH_SITESP", True).ToString() = "N") Then
                        If oGridN.getRequiredFilter().Trim <> "" Then
                            If sReqFilter.Contains("IsNull(CRD1.U_onStop,'')='Y'") Then 'Yes before
                                sReqFilter = sReqFilter.Replace(" And vARG.CustAddress In( Select distinct CRD1.Address From CRD1 WITH(NOLOCK)  Where vARG.CustCd=CRD1.CardCode AND vARG.CustAddress=CRD1.Address and IsNull(CRD1.U_onStop,'')='Y' and CRD1.AdresType='S')", " ")
                            ElseIf sReqFilter.Contains("IsNull(CRD1.U_onStop,'')<>'Y'") 'no before
                                sReqFilter = sReqFilter.Replace(" And vARG.CustAddress In( Select distinct CRD1.Address From CRD1 WITH(NOLOCK)  Where vARG.CustCd=CRD1.CardCode AND vARG.CustAddress=CRD1.Address and IsNull(CRD1.U_onStop,'')<>'Y' and CRD1.AdresType='S')", " ")
                            End If
                        End If
                    End If
                End If
                'MA  20170303 END
                oGridN.setRequiredFilter(sReqFilter)

                oGridN.doReloadData()
                doSetGridTotals(oForm)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Loading Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLDBD", Nothing)
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                oForm.Freeze(True)
                If pVal.BeforeAction Then
                    If pVal.ItemUID = "1" Then
                        Dim bIsValid As Boolean = False

                        'Validate selection and Supplier filter requirement 
                        Dim oItm As SAPbouiCOM.EditText
                        oItm = oForm.Items.Item("IDH_CUSTCD").Specific
                        Dim sFilterCust As String = oItm.Value

                        If sFilterCust.Length > 0 Then
                            bIsValid = True
                        End If

                        If bIsValid Then
                            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "ARGRID")
                            Dim oSelectedRows As SAPbouiCOM.SelectedRows
                            oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
                            Dim iRowCnt As Integer = oSelectedRows.Count
                            Dim oData As ArrayList = Nothing

                            If iRowCnt > 0 Then
                                Dim oSInvoice As SAPbobsCOM.Documents
                                'oSInvoice = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oSalesInvoices)
                                oSInvoice = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts)

                                Dim sCUSTCardCd As String = oGridN.doGetFieldValue("vARG.CustCd")
                                Dim sCUSTCardNm As String = oGridN.doGetFieldValue("vARG.CustNm")

                                oSInvoice.CardCode = sCUSTCardCd
                                oSInvoice.CardName = sCUSTCardNm

                                'Check Admin Info for Supplier Ref. No. 
                                Dim oAdminInfo As SAPbobsCOM.AdminInfo = goParent.goDICompany.GetCompanyService.GetAdminInfo()
                                'Dim oExtAdminInfo As SAPbobsCOM.ExtendedAdminInfo

                                'oItm = oForm.Items.Item("IDH_SUPREF").Specific
                                'Dim sSupRef As String = oItm.Value
                                'oSInvoice.NumAtCard = sSupRef

                                'Set the AP Invoice to Draft 
                                oSInvoice.DocObjectCode = SAPbobsCOM.BoObjectTypes.oInvoices

                                Dim oDraftDocNumberList As ArrayList = oGridN.doPrepareListFromSelection("vARG.DraftDocNumber")

                                Dim oSONos As ArrayList = oGridN.doPrepareListFromSelection("vARG.DocEntry")
                                Dim oSOLineNos As ArrayList = oGridN.doPrepareListFromSelection("vARG.LineNum")

                                Dim sParams(3) As String
                                sParams(0) = sCUSTCardCd
                                sParams(1) = sCUSTCardNm
                                sParams(2) = ""

                                For iLines As Integer = 0 To iRowCnt - 1
                                    If iLines > 0 Then
                                        oSInvoice.Lines.Add()
                                        sParams(2) = sParams(2) + ", "
                                    End If

                                    oSInvoice.Lines.ItemCode = oGridN.doGetFieldValue("vARG.ItemCd", oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(iLines, SAPbouiCOM.BoOrderType.ot_RowOrder)))
                                    oSInvoice.Lines.ItemDescription = oGridN.doGetFieldValue("vARG.ItemNm", oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(iLines, SAPbouiCOM.BoOrderType.ot_RowOrder)))

                                    oSInvoice.Lines.BaseEntry = oGridN.doGetFieldValue("vARG.DocEntry", oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(iLines, SAPbouiCOM.BoOrderType.ot_RowOrder)))
                                    oSInvoice.Lines.BaseLine = oGridN.doGetFieldValue("vARG.LineNum", oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(iLines, SAPbouiCOM.BoOrderType.ot_RowOrder)))
                                    oSInvoice.Lines.BaseType = SAPbobsCOM.BoObjectTypes.oOrders

                                    sParams(2) = sParams(2) + "SO: " + oSInvoice.Lines.BaseEntry.ToString() + " - " + oSInvoice.Lines.BaseLine.ToString()
                                Next

                                'restrict message concatination to avoid huge messages 
                                If iRowCnt > 50 Then
                                    sParams(2) = "more than 50."
                                End If

                                'Get user confirmation 
                                If Messages.INSTANCE.doResourceMessageYN("ARDOCMSG", sParams, 1) = 1 Then
                                    goParent.goDICompany.StartTransaction()

                                    Dim iResult As Integer = oSInvoice.Add()
                                    Dim sResult As String = Nothing
                                    If iResult <> 0 Then
                                        BubbleEvent = False
                                        goParent.goDICompany.GetLastError(iResult, sResult)
                                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & sResult, "Error doing AR Invoice Generation. " & sResult)
                                        DataHandler.INSTANCE.doResExceptionError(sResult, "EREXAPIG", {com.idh.bridge.Translation.getTranslatedWord(sResult)})
                                    Else
                                        'reset Supplier Ref# 
                                        'oItm.Value = ""

                                        Dim sFinalResult As String = ""
                                        sFinalResult = doRemoveDraftDocument(oForm, oDraftDocNumberList)

                                        If sFinalResult.Length > 0 Then
                                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                            'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & sFinalResult, "Error removing draft document(s).")
                                            DataHandler.INSTANCE.doResExceptionError(sFinalResult, "EREXDDOC", Nothing)
                                        Else
                                            If goParent.goDICompany.InTransaction Then
                                                goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                            End If
                                            doWarnMess("Draft AP Invoice generated successfully. You can review the document and post it in the system.", SAPbouiCOM.BoMessageTime.bmt_Medium)
                                            setSharedData(oForm, "IDH_CUSTCD", sFilterCust)
                                            goParent.doOpenModalForm("IDHDRFSRCSI", oForm)
                                        End If
                                    End If

                                    doLoadData(oForm)
                                Else
                                    BubbleEvent = False
                                End If
                            Else
                                doWarnMess("No rows selected.", SAPbouiCOM.BoMessageTime.bmt_Medium)
                                BubbleEvent = False
                            End If
                        Else
                            'com.idh.bridge.DataHandler.INSTANCE.doError("The AR Invoice will be generated against the customer. Provide customer filter.")
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("The AR Invoice will be generated against the customer. Provide customer filter.", "ERUSAPIS", Nothing)
                            BubbleEvent = False
                        End If
                    End If
                End If
            Catch ex As Exception
                If goParent.goDICompany.InTransaction Then
                    goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                End If
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error doing AR Invoice Generation.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXAPIG", Nothing)
                com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            'If Not BubbleEvent Then
            '    Me.setWFValue((oForm), "APHasChanged", False)
            '    Return False
            'End If
            If (pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE) Then
                'If ((pVal.BeforeAction = False) AndAlso Not pVal.ItemUID.Equals("ARGRID")) Then
                '    If pVal.ItemChanged Then
                '        setWFValue((oForm), "APHasChanged", True)
                '    Else
                '        setWFValue((oForm), "APHasChanged", False)
                '    End If
                'End If
            ElseIf (pVal.EventType = SAPbouiCOM.BoEventTypes.et_LOST_FOCUS) Then
                'If Not pVal.BeforeAction Then
                '    If getWFValue((oForm), "APHasChanged").Equals(False) Then
                '        Dim oGridN As FilterGrid = DirectCast(IDHGrid.getInstance((oForm), "ARGRID"), FilterGrid)
                '        If (oGridN.getGridControl.getFilterFields.indexOfFilterField(pVal.ItemUID) > -1) Then
                '            doLoadData((oForm))
                '        End If
                '    End If
                '    setWFValue((oForm), "APHasChanged", False)
                'End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
                'If pVal.BeforeAction Then
                '    If pVal.ItemUID = "IDH_FIND" Then
                '        doLoadData(oForm)
                '    ElseIf pVal.ItemUID = "IDH_RESET" Then
                '        'clear filter fields 
                '        doResetFilters(oForm)
                '    ElseIf pVal.ItemUID = "IDH_DRFTRP" Then
                '        Dim oItm As SAPbouiCOM.EditText
                '        oItm = oForm.Items.Item("IDH_CUSTCD").Specific
                '        Dim sFilterCust As String = oItm.Value
                '        setSharedData(oForm, "IDH_CUSTCD", sFilterCust)
                '        goParent.doOpenModalForm("IDHDRFSRCSI", oForm)
                '    End If
                'Else
                '    If pVal.ItemUID = "IDH_SOOHLD" Then
                '        Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_SOOHLD")
                '        Dim chkOnhold As SAPbouiCOM.CheckBox = oItem.Specific
                '        If chkOnhold IsNot Nothing AndAlso chkOnhold.Checked Then
                '            oForm.Items.Item("1").Enabled = True
                '        Else
                '            oForm.Items.Item("1").Enabled = False
                '        End If
                '    End If
                'End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction Then
                    If pVal.ItemUID = "IDH_FIND" Then
                        doLoadData(oForm)
                    ElseIf pVal.ItemUID = "IDH_RESET" Then
                        'clear filter fields 
                        doResetFilters(oForm)
                    ElseIf pVal.ItemUID = "IDH_DRFTRP" Then
                        Dim oItm As SAPbouiCOM.EditText
                        oItm = oForm.Items.Item("IDH_CUSTCD").Specific
                        Dim sFilterCust As String = oItm.Value
                        setSharedData(oForm, "IDH_CUSTCD", sFilterCust)
                        goParent.doOpenModalForm("IDHDRFSRCSI", oForm)
                    End If
                Else
                    If pVal.ItemUID = "IDH_SOOHLD" Then
                        Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_SOOHLD")
                        Dim chkOnhold As SAPbouiCOM.CheckBox = oItem.Specific
                        If chkOnhold IsNot Nothing AndAlso chkOnhold.Checked Then
                            oForm.Items.Item("1").Enabled = False
                        Else
                            oForm.Items.Item("1").Enabled = True
                        End If
                    ElseIf pVal.ItemUID = "IDH_SOFLAG" Then
                        Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_SOFLAG")
                        Dim chkFlagPO As SAPbouiCOM.CheckBox = oItem.Specific
                        If chkFlagPO IsNot Nothing AndAlso chkFlagPO.Checked Then
                            oForm.Items.Item("1").Enabled = False
                        Else
                            oForm.Items.Item("1").Enabled = True
                        End If
                    End If
                End If
            End If
            'Return False
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oParentForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            'MyBase.doHandleModalResultShared(oParentForm, sModalFormType, sLastButton)
            If sModalFormType = "IDHDRFSRCSI" Then
                doLoadData(oParentForm)
                doSetGridTotals(oParentForm)
            ElseIf sModalFormType = "IDH_COMMFRM" Then
                'Dim sTarget As String = getSharedData(oParentForm, "TARGET")
                'If sTarget IsNot Nothing AndAlso sTarget.Length > 0 Then
                '    Dim sReason As String = getSharedData(oParentForm, "CMMNT")

                '    If sTarget = "FLAGSO" Then
                '        doFlagToCloseSalesOrders(oParentForm, "Y", sReason)
                '    ElseIf sTarget = "UNFLAGSO" Then
                '        doFlagToCloseSalesOrders(oParentForm, "N", sReason)
                '        'ElseIf sTarget = "CLOSEPO" Then
                '        '    doClosePurchaseOrders(oParentForm, sReason)
                '    ElseIf sTarget = "HOLDSO" Then
                '        doHoldSalesOrders(oParentForm, "Y", sReason)
                '    ElseIf sTarget = "UNHOLDSO" Then
                '        doHoldSalesOrders(oParentForm, "N", sReason)
                '    End If
                'End If
                Dim sTarget As String = getSharedData(oParentForm, "TARGET")
                If sTarget IsNot Nothing AndAlso sTarget.Length > 0 Then
                    Dim sReason As String = getSharedData(oParentForm, "CMMNT")

                    ' ''Note - 20160509: Allow system to CLOSE the document permamnently 
                    ' ''based on config but with a restricted count
                    ''If Config.INSTANCE.getParameterAsBool("ARCLSDOC", False) = False Then
                    ''    If sTarget = "FLAGSO" Then
                    ''        doFlagToCloseSalesOrders(oParentForm, "Y", sReason)
                    ''    ElseIf sTarget = "UNFLAGSO" Then
                    ''        doFlagToCloseSalesOrders(oParentForm, "N", sReason)
                    ''    End If
                    ''Else
                    ''    'Now check no. of rows allowed in live system 
                    ''    'based on WRConfig.ARCDCONT 
                    ''    Dim oGridN As FilterGrid = FilterGrid.getInstance(oParentForm, "ARGRID")
                    ''    Dim oSONos As ArrayList = oGridN.doPrepareListFromSelection("vARG.DocEntry")
                    ''    Dim iSelectedRows As Integer = oGridN.getSelectedRows().Count
                    ''    Dim iAllowedCount As Integer = Config.INSTANCE.getParameterAsInt("ARCDCONT", 0, True)
                    ''    oGridN = Nothing
                    ''    If iAllowedCount > 0 AndAlso oSONos.Count <= iAllowedCount Then
                    ''        doCloseSalesOrders(oParentForm, sReason)
                    ''    Else
                    ''        doWarnMess("Selected rows not in permissible range! Please check selected rows and/or WRConfig.ARCDCONT", SAPbouiCOM.BoMessageTime.bmt_Medium)
                    ''    End If

                    ''End If

                    If sTarget = "FLAGSO" Then
                        doFlagToCloseSalesOrders(oParentForm, "Y", sReason)
                    ElseIf sTarget = "UNFLAGSO" Then
                        doFlagToCloseSalesOrders(oParentForm, "N", sReason)
                    ElseIf sTarget = "CLOSESO" Then
                        Dim oGridN As FilterGrid = FilterGrid.getInstance(oParentForm, "ARGRID")
                        Dim oSONos As ArrayList = oGridN.doPrepareListFromSelection("vARG.DocEntry")
                        Dim iSelectedRows As Integer = oGridN.getSelectedRows().Count
                        Dim iAllowedCount As Integer = Config.INSTANCE.getParameterAsInt("ARCDCONT", 0, True)
                        oGridN = Nothing
                        If iAllowedCount > 0 AndAlso oSONos.Count <= iAllowedCount Then
                            doCloseSalesOrders(oParentForm, sReason)
                        Else
                            doWarnMess("Selected sales orders (" + oSONos.Count.ToString() + ") to close are more than allowed limit of " + iAllowedCount.ToString() + ". ", SAPbouiCOM.BoMessageTime.bmt_Medium)
                        End If
                    ElseIf sTarget = "HOLDSO" Then
                        doHoldSalesOrders(oParentForm, "Y", sReason)
                    ElseIf sTarget = "UNHOLDSO" Then
                        doHoldSalesOrders(oParentForm, "N", sReason)
                    End If

                    ''If sTarget = "HOLDPO" Then
                    ''    doHoldSalesOrders(oParentForm, "Y", sReason)
                    ''ElseIf sTarget = "UNHOLDPO" Then
                    ''    doHoldSalesOrders(oParentForm, "N", sReason)
                    ''End If
                End If
            End If
        End Sub

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then
                If pVal.ItemUID = "ARGRID" Then
                    If pVal.BeforeAction = False Then
                        'doGridDoubleClick(oForm, pVal)
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                'Set menu creation package settings 
                If pVal.BeforeAction = True Then
                    'Show Right Click Menu 
                    Dim oGridN As FilterGrid = DirectCast(IDHGrid.getInstance(oForm, "ARGRID"), FilterGrid)
                    Dim oMenuItem As SAPbouiCOM.MenuItem
                    Dim sMenuID As String = IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU
                    oMenuItem = goParent.goApplication.Menus.Item(sMenuID)

                    Dim oMenus As SAPbouiCOM.Menus
                    Dim iMenuPos As Integer = 1
                    Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                    oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
                    oCreationPackage.Enabled = True
                    oMenus = oMenuItem.SubMenus
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING

                    'Get selected rows from the grid 
                    Dim oSelectedRows As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
                    Dim iSelectedRowsCnt As Integer = oSelectedRows.Count

                    If iSelectedRowsCnt > 0 Then
                        'oCreationPackage.UniqueID = "GENARINV"
                        'oCreationPackage.String = "Generate AP Invoice"
                        'oCreationPackage.Position = iMenuPos
                        'iMenuPos += 1
                        'oMenus.AddEx(oCreationPackage)


                        ''get On-hold checkbox value 
                        Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_SOOHLD")
                        Dim chkOnhold As SAPbouiCOM.CheckBox = oItem.Specific

                        If chkOnhold IsNot Nothing AndAlso chkOnhold.Checked Then
                            oCreationPackage.UniqueID = "UNHOLDSO"
                            oCreationPackage.String = "Un-hold SO"
                            oCreationPackage.Position = iMenuPos
                            iMenuPos += 1
                            oMenus.AddEx(oCreationPackage)
                        Else
                            oCreationPackage.UniqueID = "HOLDSO"
                            oCreationPackage.String = "Hold SO"
                            oCreationPackage.Position = iMenuPos
                            iMenuPos += 1
                            oMenus.AddEx(oCreationPackage)
                        End If

                        ''get Flag to Close checkbox 
                        'If Config.INSTANCE.getParameterAsBool("ARCLSDOC", False) = False Then
                        'Else
                        '    oCreationPackage.UniqueID = "FLAGSO"
                        '    oCreationPackage.String = "Close SO"
                        '    oCreationPackage.Position = iMenuPos
                        '    iMenuPos += 1
                        '    oMenus.AddEx(oCreationPackage)
                        'End If

                        oItem = oForm.Items.Item("IDH_SOFLAG")
                        Dim chkToClose As SAPbouiCOM.CheckBox = oItem.Specific

                        If chkToClose IsNot Nothing AndAlso chkToClose.Checked Then
                            oCreationPackage.UniqueID = "UNFLAGSO"
                            oCreationPackage.String = "Un-flag SO"
                            oCreationPackage.Position = iMenuPos
                            iMenuPos += 1
                            oMenus.AddEx(oCreationPackage)
                        Else
                            oCreationPackage.UniqueID = "FLAGSO"
                            oCreationPackage.String = "Flag to Close SO"
                            oCreationPackage.Position = iMenuPos
                            iMenuPos += 1
                            oMenus.AddEx(oCreationPackage)
                        End If

                        oCreationPackage.UniqueID = "CLOSESO"
                        oCreationPackage.String = "Close SO"
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)
                    End If

                Else
                    'Hide Right Click Menu 
                    If goParent.goApplication.Menus.Exists("GENARINV") Then
                        goParent.goApplication.Menus.RemoveEx("GENARINV")
                    End If
                    If goParent.goApplication.Menus.Exists("FLAGSO") Then
                        goParent.goApplication.Menus.RemoveEx("FLAGSO")
                    End If
                    If goParent.goApplication.Menus.Exists("UNFLAGSO") Then
                        goParent.goApplication.Menus.RemoveEx("UNFLAGSO")
                    End If
                    If goParent.goApplication.Menus.Exists("UNHOLDSO") Then
                        goParent.goApplication.Menus.RemoveEx("UNHOLDSO")
                    End If
                    If goParent.goApplication.Menus.Exists("HOLDSO") Then
                        goParent.goApplication.Menus.RemoveEx("HOLDSO")
                    End If
                    If goParent.goApplication.Menus.Exists("CLOSESO") Then
                        goParent.goApplication.Menus.RemoveEx("CLOSESO")
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                If pVal.BeforeAction = False Then
                    Dim oGridN As FilterGrid = DirectCast(IDHGrid.getInstance(oForm, "ARGRID"), FilterGrid)
                    Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows() 'getGrid().Rows.SelectedRows()
                    If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then

                        Dim oSONos As ArrayList = oGridN.doPrepareListFromSelection("vARG.DocEntry")
                        Dim sSONos As String = String.Join(",", CType(oSONos.ToArray(GetType(String)), String()))

                        Dim sParams(1) As String
                        If oSONos.Count < 51 Then
                            sParams(0) = sSONos 'String.Join(",", oSONos.ToArray(GetType(String)))
                        Else
                            sParams(0) = "more than 50."
                        End If

                        If pVal.oData.MenuUID.Equals("CLOSESO") Then
                            If Messages.INSTANCE.doResourceMessageYN("ARDCLSSO", sParams, 1) = 1 Then
                                setSharedData(oForm, "TARGET", "CLOSESO")
                                PARENT.doOpenModalForm("IDH_COMMFRM", oForm)
                            End If
                        ElseIf pVal.oData.MenuUID.Equals("FLAGSO") Then
                            If Messages.INSTANCE.doResourceMessageYN("ARDFCLSO", sParams, 1) = 1 Then
                                setSharedData(oForm, "TARGET", "FLAGSO")
                                PARENT.doOpenModalForm("IDH_COMMFRM", oForm)
                            End If

                        ElseIf pVal.oData.MenuUID.Equals("UNFLAGSO") Then
                            If Messages.INSTANCE.doResourceMessageYN("ARDUFCSO", sParams, 1) = 1 Then
                                setSharedData(oForm, "TARGET", "UNFLAGSO")
                                PARENT.doOpenModalForm("IDH_COMMFRM", oForm)
                            End If
                        ElseIf pVal.oData.MenuUID.Equals("UNHOLDSO") Then
                            If Messages.INSTANCE.doResourceMessageYN("AUNHLDSO", sParams, 1) = 1 Then
                                setSharedData(oForm, "TARGET", "UNHOLDSO")
                                PARENT.doOpenModalForm("IDH_COMMFRM", oForm)
                            End If
                        ElseIf pVal.oData.MenuUID.Equals("HOLDSO") Then
                            If Messages.INSTANCE.doResourceMessageYN("ARDHLDSO", sParams, 1) = 1 Then
                                setSharedData(oForm, "TARGET", "HOLDSO")
                                PARENT.doOpenModalForm("IDH_COMMFRM", oForm)
                            End If
                        End If
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_SELECT OrElse _
                    pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DESELECT OrElse _
                    pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MULTI_SELECT Then
                If pVal.BeforeAction = False Then
                    doSetGridTotals(oForm)
                    Return True
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doClose()
        End Sub

        Public Sub doSetGridTotals(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "ARGRID")
            Dim oSelectedRows As SAPbouiCOM.SelectedRows
            Dim iRowCount As Integer
            Dim oRowTotalsData As ArrayList
            Dim dTotal As Double = 0

            Try
                oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
                iRowCount = oSelectedRows.Count

                If iRowCount > 0 Then
                    'The Total Charge should be of all the Open Row Totals 
                    'oRowTotalsData = oGridN.doPrepareListFromSelection("vARG.RowTotal")
                    oRowTotalsData = oGridN.doPrepareListFromSelection("vARG.OpenRowTotal")
                    For i As Integer = 0 To iRowCount - 1
                        dTotal = dTotal + Convert.ToDouble(oGridN.doGetFieldValue("vARG.OpenRowTotal", oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(i, SAPbouiCOM.BoOrderType.ot_RowOrder))))
                    Next
                    setUFValue(oForm, "IDH_TOTAL", dTotal)
                Else
                    setUFValue(oForm, "IDH_TOTAL", 0)
                End If
            Catch ex As Exception
            Finally
                oGridN = Nothing
                oSelectedRows = Nothing
                iRowCount = Nothing
                oRowTotalsData = Nothing
                dTotal = Nothing
                'Garbage collection 
                GC.Collect()
            End Try
        End Sub

        Public Sub doSetGridRowsColor(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "ARGRID")
            Dim iRowCount As Integer

            Dim sIsDraftCol As String = ""
            sIsDraftCol = sIsDraftCol + " CASE WHEN (select count(*) from DRF1 DRF WHERE drf.BaseEntry = vARG.DocEntry AND drf.BaseLine = vARG.LineNum ) >= 1 "
            sIsDraftCol = sIsDraftCol + " THEN 'DRAFTED' "
            sIsDraftCol = sIsDraftCol + " ELSE '' END "

            Try
                Dim oCmnSetting As SAPbouiCOM.CommonSetting
                oCmnSetting = oGridN.getSBOGrid.CommonSetting
                iRowCount = oGridN.getSBOGrid.Rows.Count

                Dim iColorRowNo As Integer
                For ii As Integer = 1 To iRowCount
                    If oGridN.doGetFieldValue("Is Drafted", ii) = "DRAFTED" Then
                        If ii = 1 Then
                            iColorRowNo = ii - 1
                        Else
                            iColorRowNo = ii + 1
                        End If
                        oCmnSetting.SetRowBackColor(iColorRowNo, (0 * 65536 + 255 * 256 + 0))
                    End If
                Next
            Catch ex As Exception

            End Try

        End Sub

        Public Sub doResetFilters(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)

            Dim oEdit As SAPbouiCOM.EditText
            doSetFocus(oForm, "IDH_AEDT")
            oEdit = oForm.Items.Item("IDH_ASDT").Specific
            '   Dim str As String = oEdit.DataBind.Alias
            '  str = oEdit.DataBind.TableName
            'oEdit.Value = DBNull.Value.ToString
            oEdit.String = ""
            'oForm.DataSources.UserDataSources.Item("IDH_ASDT").ValueEx = ""
            'oForm.DataSources.UserDataSources.Item("IDH_ASDT").Value = ""
            'oForm.Items.Item("IDH_ASDT").

            oEdit = oForm.Items.Item("IDH_AEDT").Specific
            oEdit.Value = ""

            oEdit = oForm.Items.Item("IDH_SUPPCD").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_SUPPNM").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_CONTCD").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_WOHNO").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_WORNO").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_SITEID").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_WASCD").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_ADDRES").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_STZPCD").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_UNTSIP").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_CUSTCD").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_CUSTNM").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_SORNUM").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_REASON").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_CONTNM").Specific
            oEdit.Value = ""

            'Reset checkbox 
            Dim oChkSOHold As SAPbouiCOM.CheckBox
            oChkSOHold = oForm.Items.Item("IDH_SOOHLD").Specific
            oChkSOHold.Checked = False

            'Reset Flag to Close PO checkbox
            If Config.INSTANCE.getParameterAsBool("ARCLSDOC", False) = False Then
                Dim oChkToClose As SAPbouiCOM.CheckBox
                oChkToClose = oForm.Items.Item("IDH_SOFLAG").Specific
                oChkToClose.Checked = False
            End If

            'Reset Selfbill LKP 
            Dim oBPStaCombo As SAPbouiCOM.ComboBox
            oBPStaCombo = oForm.Items.Item("IDH_LKPSBL").Specific
            oBPStaCombo.Select(0)

            'Reset Scheduled LKP 
            Dim oSchCombo As SAPbouiCOM.ComboBox
            oSchCombo = oForm.Items.Item("IDH_SCHADH").Specific
            oSchCombo.Select(0)

            'Clear Grid from results now 
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "ARGRID")
            If oGridN IsNot Nothing Then
                oGridN.setInitialFilterValue("vARG.DocEntry = -1")
                oGridN.doReloadData()
            End If

            doSetFocus(oForm, "IDH_ASDT")
            'doLoadData(oForm) ''Grid is being refreshed with -1 clause to clear all rows. 
            ' oForm.Refresh()
            oForm.Freeze(False)
        End Sub

        Public Sub doCloseSalesOrders(ByVal oForm As SAPbouiCOM.Form, ByVal sReason As String)
            Try
                oForm.Freeze(True)
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "ARGRID")
                Dim oSelectedRows As SAPbouiCOM.SelectedRows
                oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
                Dim iRowCnt As Integer = oSelectedRows.Count
                Dim oData As ArrayList = Nothing

                If iRowCnt > 0 Then
                    Dim sUserCode As String = goParent.goDICompany.UserName
                    Dim oSONos As ArrayList = oGridN.doPrepareListFromSelection("vARG.DocEntry")
                    Dim oSO As SAPbobsCOM.Documents
                    Dim sResult As String = Nothing
                    Dim sFinalResult As String = ""

                    oSO = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders)
                    goParent.goDICompany.StartTransaction()

                    For iCnt As Integer = 0 To oSONos.Count - 1
                        oSO.GetByKey(Convert.ToInt32(oSONos.Item(iCnt)))
                        oSO.UserFields.Fields.Item("U_HLDCOMT").Value = sReason

                        Dim sComments As String = "AR Doc Gen: SO Closed By: '" + sUserCode + "'" + Chr(13) + "On: " + DateTime.Now.ToString()

                        If Config.INSTANCE.getParameterAsBool("APNDCMT", True) AndAlso oSO.Comments.Length > 0 Then
                            If (oSO.Comments + Chr(13) + sComments).Length < 255 Then
                                oSO.Comments = oSO.Comments + Chr(13) + sComments
                            Else
                                oSO.Comments = sComments
                            End If
                        Else
                            oSO.Comments = sComments
                        End If

                        oSO.Update()

                        Dim iResult As Integer = oSO.Close()
                        If iResult <> 0 Then
                            goParent.goDICompany.GetLastError(iResult, sResult)
                            sFinalResult = sFinalResult + Chr(13) + sResult
                            sResult = ""
                            iResult = Nothing
                        End If
                    Next
                    If sFinalResult.Length > 0 Then
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        End If
                        'com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & sFinalResult, "Error closing Sales Order(s).")
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(sFinalResult, "EREXCSOR", Nothing)
                    Else
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                        End If
                        doWarnMess("Sales Order(s) Closed successfully.", SAPbouiCOM.BoMessageTime.bmt_Long)
                    End If
                    doLoadData(oForm)

                Else
                    doWarnMess("No rows selected.", SAPbouiCOM.BoMessageTime.bmt_Medium)
                End If

                oForm.Freeze(False)
            Catch ex As Exception
                If goParent.goDICompany.InTransaction Then
                    goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                End If
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error closing sales orders. Review Error Log.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCSOR", Nothing)
                DataHandler.INSTANCE.doBufferedErrors()
            End Try

        End Sub

        Public Sub doFlagToCloseSalesOrders(ByVal oForm As SAPbouiCOM.Form, ByVal sFlagToClose As String, ByVal sReason As String)
            Try
                oForm.Freeze(True)
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "ARGRID")
                Dim oSelectedRows As SAPbouiCOM.SelectedRows
                oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
                Dim iRowCnt As Integer = oSelectedRows.Count
                Dim oData As ArrayList = Nothing

                If iRowCnt > 0 Then
                    Dim sUserCode As String = goParent.goDICompany.UserName
                    Dim oSONos As ArrayList = oGridN.doPrepareListFromSelection("vARG.DocEntry")
                    Dim oSO As SAPbobsCOM.Documents
                    Dim sResult As String = Nothing
                    Dim sFinalResult As String = ""
                    Dim sParams(1) As String

                    sParams(0) = String.Join(",", oSONos.ToArray(GetType(String)))
                    oSO = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders)

                    Dim sAlertKey As String = IIf(sFlagToClose = "Y", "ARDCLSSO", "ARDUCLSO")

                    goParent.goDICompany.StartTransaction()

                    'Dim sUpdateQry = "Update [@IDH_JOBENTR] set U_Status = '6' where Code = '" & sU_JOBENTR & "'"
                    'DataHandler.INSTANCE.doUpdateQuery(sUpdateQry)
                    Dim sUpdateQry = "" '"Update ORDR set U_FlagToClose = 'sFlagToClose', U_HLDCOMT = 'sReason', Comments = 'sComments' where DocEntry = Convert.ToInt32(oPONos.Item(iCnt));"
                    For iCnt As Integer = 0 To oSONos.Count - 1
                        oSO.GetByKey(Convert.ToInt32(oSONos.Item(iCnt)))
                        Dim sComments As String
                        If sFlagToClose = "Y" Then
                            sComments = "AR Doc Gen: SO Flag to Close By: " + sUserCode + " On: " + DateTime.Now.ToString()
                        Else
                            sComments = "AR Doc Gen: SO Un-flag to Close By: " + sUserCode + " On: " + DateTime.Now.ToString()
                        End If
                        oSO.UserFields.Fields.Item("U_HLDCOMT").Value = sReason

                        'Dim sCommentQry As String
                        'If Config.INSTANCE.getParameterAsBool("APNDCMT", True) AndAlso sComments.Length > 0 Then
                        '    'sCommentQry = "Comments = Comments + '" + sComments + "'"
                        '    sCommentQry = "Comments = CASE WHEN LEN(Comments + '" + sComments + "') > 254 THEN '" + sComments + "' ELSE Comments + '" + sComments + "' END "
                        'Else
                        '    sCommentQry = "Comments = '" + sComments + "'"
                        'End If
                        'sUpdateQry = sUpdateQry + "Update ORDR set U_FlagToClose = '" + sFlagToClose + "', U_HLDCOMT = '" + sReason + "', " + sCommentQry + " where DocEntry = " + Convert.ToInt32(oSONos.Item(iCnt)).ToString() + "; "

                        If Config.INSTANCE.getParameterAsBool("APNDCMT", True) AndAlso oSO.Comments.Length > 0 Then
                            If (oSO.Comments + Chr(13) + sComments).Length < 255 Then
                                oSO.Comments = oSO.Comments + Chr(13) + sComments
                            Else
                                oSO.Comments = sComments
                            End If
                        Else
                            oSO.Comments = sComments
                        End If

                        oSO.UserFields.Fields.Item("U_FlagToClose").Value = sFlagToClose

                        Dim iResult As Integer = oSO.Update()
                        If iResult <> 0 Then
                            goParent.goDICompany.GetLastError(iResult, sResult)
                            sFinalResult = sFinalResult + Chr(13) + sResult
                            sResult = ""
                            iResult = Nothing
                        End If
                    Next

                    'If Not goParent.goDB.doUpdateQuery("doFlagToCloseSalesOrders()", sUpdateQry) Then
                    '    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & sFinalResult, "Error Flag To Close Sales Order(s).")
                    '    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(sFinalResult, "EREXCSOR", Nothing)
                    'Else
                    '    doWarnMess("Sales Order(s) flaged to close successfully.", SAPbouiCOM.BoMessageTime.bmt_Long)
                    'End If
                    If sFinalResult.Length > 0 Then
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        End If
                        'com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & sFinalResult, "Error closing Purchase Order(s).")
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(sFinalResult, "EREXCSOR", Nothing)
                    Else
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                        End If
                        doWarnMess("Sales Order(s) flaged to close successfully.", SAPbouiCOM.BoMessageTime.bmt_Long)
                    End If
                    doLoadData(oForm)
                Else
                    doWarnMess("No rows selected.", SAPbouiCOM.BoMessageTime.bmt_Medium)
                End If

                oForm.Freeze(False)
            Catch ex As Exception
                If goParent.goDICompany.InTransaction Then
                    goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                End If
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error flaging to close sales orders. Review Error Log.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCSOR", Nothing)
                DataHandler.INSTANCE.doBufferedErrors()
            End Try

        End Sub

        Public Sub doHoldSalesOrders(ByVal oForm As SAPbouiCOM.Form, ByVal sHoldStatus As String, ByVal sReason As String)
            Try
                oForm.Freeze(True)
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "ARGRID")
                Dim oSelectedRows As SAPbouiCOM.SelectedRows
                oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
                Dim iRowCnt As Integer = oSelectedRows.Count
                Dim oData As ArrayList = Nothing

                If iRowCnt > 0 Then
                    Dim sUserCode As String = goParent.goDICompany.UserName
                    Dim oSONos As ArrayList = oGridN.doPrepareListFromSelection("vARG.DocEntry")
                    Dim oSO As SAPbobsCOM.Documents
                    Dim sResult As String = Nothing
                    Dim sFinalResult As String = ""

                    oSO = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders)
                    goParent.goDICompany.StartTransaction()

                    For iCnt As Integer = 0 To oSONos.Count - 1
                        oSO.GetByKey(Convert.ToInt32(oSONos.Item(iCnt)))
                        Dim sComments As String
                        If sHoldStatus = "Y" Then
                            sComments = "SO On Hold by: '" + sUserCode + "'" + Chr(13) + "On: " + DateTime.Now.ToString()
                        Else
                            sComments = "SO Un-hold by: '" + sUserCode + "'" + Chr(13) + "On: " + DateTime.Now.ToString()
                        End If

                        oSO.UserFields.Fields.Item("U_HLDCOMT").Value = sReason
                        If Config.INSTANCE.getParameterAsBool("APNDCMT", True) AndAlso oSO.Comments.Length > 0 Then
                            If (oSO.Comments + Chr(13) + sComments).Length < 255 Then
                                oSO.Comments = oSO.Comments + Chr(13) + sComments
                            Else
                                oSO.Comments = sComments
                            End If
                        Else
                            oSO.Comments = sComments
                        End If
                        oSO.UserFields.Fields.Item("U_IDH_HLD").Value = sHoldStatus

                        Dim iResult As Integer = oSO.Update()
                        If iResult <> 0 Then
                            goParent.goDICompany.GetLastError(iResult, sResult)
                            sFinalResult = sFinalResult + Chr(13) + sResult
                            sResult = ""
                            iResult = Nothing
                        End If
                    Next
                    If sFinalResult.Length > 0 Then
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        End If
                        'com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & sFinalResult, "Error closing Sales Order(s).")
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(sFinalResult, "EREXCSOR", Nothing)
                    Else
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                        End If
                        doWarnMess("Sales Order(s) Closed successfully.", SAPbouiCOM.BoMessageTime.bmt_Long)
                    End If
                    doLoadData(oForm)
                Else
                    doWarnMess("No rows selected.", SAPbouiCOM.BoMessageTime.bmt_Medium)
                End If

                oForm.Freeze(False)
            Catch ex As Exception
                If goParent.goDICompany.InTransaction Then
                    goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                End If
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error closing Sales orders. Review Error Log.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCSOR", Nothing)
                DataHandler.INSTANCE.doBufferedErrors()
            End Try

        End Sub

        Public Function doRemoveDraftDocument(ByVal oForm As SAPbouiCOM.Form, ByVal oDraftDocNumberList As ArrayList) As String
            Dim sFinalResult As String = ""
            Try
                If oDraftDocNumberList.Count > 0 Then
                    Dim oDrfat As SAPbobsCOM.Documents
                    Dim sResult As String = Nothing
                    Dim sParams(1) As String

                    sParams(0) = String.Join(",", oDraftDocNumberList.ToArray(GetType(String)))
                    oDrfat = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts)
                    oDrfat.DocObjectCode = SAPbobsCOM.BoObjectTypes.oPurchaseInvoices

                    For iCnt As Integer = 0 To oDraftDocNumberList.Count - 1
                        Dim iDocNo As Integer = Convert.ToInt32(oDraftDocNumberList.Item(iCnt))
                        If iDocNo > 0 Then
                            oDrfat.GetByKey(iDocNo)

                            Dim iResult As Integer = oDrfat.Remove()
                            If iResult <> 0 Then
                                goParent.goDICompany.GetLastError(iResult, sResult)
                                sFinalResult = sFinalResult + Chr(13) + sResult
                                sResult = ""
                                iResult = Nothing
                                Exit For
                            End If
                        End If
                    Next
                End If
                'Return sFinalResult
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error removing Draft Document(s). Review Error Log.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDDOC", Nothing)
            End Try
            Return sFinalResult
        End Function

        Private Sub doSelfBillCombo(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oBPStaCombo As SAPbouiCOM.ComboBox
                oBPStaCombo = oForm.Items.Item("IDH_LKPSBL").Specific
                oBPStaCombo.ValidValues.Add("", getTranslatedWord("Both"))
                oBPStaCombo.ValidValues.Add("Y", "Yes")
                oBPStaCombo.ValidValues.Add("N", "No")
            Catch ex As Exception
            End Try
        End Sub

        Private Sub doAddComboBoxValidValues(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_SCHADH")
            oItem.DisplayDesc = True
            Dim oJobType As SAPbouiCOM.ComboBox = oItem.Specific

            doClearValidValues(oJobType.ValidValues)
            Try
                oJobType.ValidValues.Add("", "Both")
                oJobType.ValidValues.Add("Y", "Scheduled")
                oJobType.ValidValues.Add("N", "Adhoc")
                oJobType.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index)
            Catch ex As Exception
            End Try

        End Sub

        Private Sub doSiteStopCombo(ByVal oForm As SAPbouiCOM.Form) 'MA  20170303 start
            Try
                Dim oRowStaCombo As SAPbouiCOM.ComboBox
                oRowStaCombo = oForm.Items.Item("IDH_SITESP").Specific
                oRowStaCombo.ValidValues.Add("", getTranslatedWord("All"))
                oRowStaCombo.ValidValues.Add(getTranslatedWord("Y"), getTranslatedWord("Yes"))
                oRowStaCombo.ValidValues.Add(getTranslatedWord("N"), getTranslatedWord("No"))
            Catch ex As Exception
            End Try
        End Sub 'MA  20170303 END


        '#MA Start 29-03-2017
        Private Sub doAddListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("vARG.IsDrafted", "Is Drafted", False, -1, Nothing, Nothing, -1)
            oGridN.doAddListField("vARG.DraftDocNumber", "Draft Doc#", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Drafts)

            oGridN.doAddListField("vARG.DocEntry", "SO", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Order)
            oGridN.doAddListField("vARG.LineNum", "SO Row", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.DocNum", "SO DocNum", False, -1, Nothing, Nothing)

            'Add Supplier details from POH 
            'oGridN.doAddListField("vARG.SupplierCd", "Supplier Code", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("vARG.SupplierNm", "Supplier Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.CustCd", "Customer Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.CustNm", "Customer Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.ActStartDt", "Act. Start Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.ActEndDt", "Act. End Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.OrderNo", "Order No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.OrderRowNo", "Order Row", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.Rebate", "Rebate", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.CustSiteID", "Site ID", False, 0, Nothing, Nothing)
            oGridN.doAddListField("vARG.CustAddress", "Site Address", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.CustZpCd", "Site Post Code", False, -1, Nothing, Nothing)


            oGridN.doAddListField("vARG.ItemCd", "Item No", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.ItemNm", "Item Description", False, -1, Nothing, Nothing)

            oGridN.doAddListField("vARG.ContainerCd", "Container Cd", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.ContainerNm", "Container Name", False, -1, Nothing, Nothing)

            oGridN.doAddListField("vARG.ContainerGrp", "Container Group", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.OrderType", "Order Type", False, -1, Nothing, Nothing)

            oGridN.doAddListField("vARG.WasteCd", "Waste Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.WasteNm", "Waste Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.UnitSIP", "Unit SIP", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.Quantity", "Quantity", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.RowTotal", "Row Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.OpenQuantity", "Open Quantity", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.OpenRowTotal", "Open Row Total", False, -1, Nothing, Nothing)

            oGridN.doAddListField("vARG.OnHold", "OnHold", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.HoldComments", "Reason", False, -1, Nothing, Nothing)

            oGridN.doAddListField("vARG.SelfBillCode", "Is SelfBill", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vARG.IsScheduled", "Is Scheduled", False, -1, Nothing, Nothing)
        End Sub
        '#MA End 29-03-2017
    End Class
End Namespace
