Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System.Net
Imports System.Web
Imports com.idh.bridge.lookups
Imports com.idh.bridge
Imports com.idh.dbObjects.User

Namespace idh.forms.manager
    Public Class DeLink
        Inherits idh.forms.manager.Order
        ''removed as totals are same as like OSM:: Issue# 1025
        ' ''MA Start 05-11-2014 Issue#417 Issue#418
        'Shared moTotalFields_Charge() As String = { _
        '   "IDH_SubAD", "IDH_TaxAmt", "IDH_Total", "IDH_DisAmt"
        '}
        'Shared moTotalFields_Cost() As String = { _
        '   "IDH_AddEx"
        '}
        ' ''MA End 05-11-2014 Issue#417 Issue#418

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, iMenuPosition, "IDHDELNK", "DeLinker.srf", "Orders UnLink")
        End Sub

        '        Protected Overrides Function getTitle() As String
        '            Return "Orders UnLink"
        '        End Function

        Protected Overrides Sub doSetGridFilters(ByVal oGridN As FilterGrid)
            oGridN.doAddFilterField("IDH_REQSTF", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_REQSTT", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_ACTSTF", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_ACTSTT", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_CUST", "e.U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_NAME", "e.U_CardNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PROD", "r.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ITMGRP", "r.U_ItmGrp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_JOBTYP", "r.U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ORDNO", "e.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WOROW", "r.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DONO", "r.U_WROrd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DOROW", "r.U_WRRow", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_VEHREG", "r.U_Lorry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DRIVER", "r.U_Driver", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ADDR", "e.U_Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_POSTCD", "e.U_ZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CUSPHO", "e.U_Phone1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SITETL", "e.U_SiteTl", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_REF", "r.U_CustRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_USER", "r.U_User", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_BRANCH", "r.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)

            oGridN.doAddFilterField("IDH_STEID", "e.U_SteId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_STREET", "e.U_Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            oGridN.doAddListField("r.U_Status", "Sales Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobNr", "Order No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Row No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_WROrd", "DO No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_WRRow", "DO Row No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_SAINV", "Invoice", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Invoice)
            oGridN.doAddListField("r.U_SAORD", "Sales Order", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Order)
            oGridN.doAddListField("r.U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Lorry", "Vehicle Reg.", False, 50, Nothing, Nothing)
            oGridN.doAddListField("r.U_VehTyp", "Vehicle", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CarrCd", "Carrier Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_CarrNm", "Carrier Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, 90, Nothing, Nothing, -1)
            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 50, Nothing, Nothing)
            oGridN.doAddListField("r.U_RdWgt", "Read Weight", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Charge Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_UOM", "Charge UOM", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "Tip Charge", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "Tip Total", False, -1, Nothing, Nothing)

            ''oGridN.doAddListField("(U_TCTotal +(U_CusQty * U_CusChr)) - r.U_DisAmt", "Sub After Discount", False, -1, Nothing, "SubBefDisc")
            oGridN.doAddListField("CAST(((U_TCTotal + U_Price) - r.U_DisAmt) As Numeric(19,2))", "Sub After Discount", False, -1, Nothing, "SubAftDisc")
            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Total Charge", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_PayStat", "Payment Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Tip", "Disposal Site", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)

            oGridN.doAddListField("r.U_LorryCd", "Vehicle Code.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "TipCost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PStat", "Purchase Status", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            'oGridN.doAddListField("e.U_CardNM", "Customer Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Address", "Contact Person", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_SteId", "Site Id", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Street", "Street", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Block", "Block", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, 0, Nothing, Nothing)

            'Added to handle the BP Switching
            oGridN.doAddListField("e.U_PAddress", "Producer Address", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_PZpCd", "Producer Post Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_PPhone1", "Producer Phone", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_FirstBP", "First BP Selected", False, 0, Nothing, Nothing)

            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RDate", "Req. Start", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTime", "Req. Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_ASDate", "Act. Start", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASTime", "Act. Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act. End", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AETime", "Act ETime", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_ItmGrp", "Container Grp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("r.U_Serial", "Item Serial No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Price", "Price", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicCh", "SLicCh", False, 0, Nothing, Nothing)

            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PARCPT", "Payment", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Receipt)

            oGridN.doAddListField("r.U_CCNum", "CC Num.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CCType", "CC Type", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CCStat", "CC Status", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtRt", "Tipping Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtRt", "Haulage Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtRt", "Tipping Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtRt", "Supplier Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtRt", "Haulage Vat Purchase Rate", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtGrp", "Tipping Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtGrp", "Haulage Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtGrp", "Tipping Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtGrp", "Supplier Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtGrp", "Haulage Vat Purchase Group", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            ' oForm.Items.Item("IDH_CERT").Visible = False
        End Sub

        'Protected Overrides Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Protected Overrides Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base)
            Dim oGridN As UpdateGrid = pVal.oGrid 'UpdateGrid.getInstance(oForm, "LINESGRID")
            If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                oGridN.setCurrentLineByClick(pVal.Row)

                Dim sRowCode As String = oGridN.doGetFieldValue("r.Code")
                Dim sJobEntr As String = oGridN.doGetFieldValue("r.U_JobNr")
                Dim sDORowCode As String = oGridN.doGetFieldValue("r.U_WRRow")
                Dim sDOJobEntr As String = oGridN.doGetFieldValue("r.U_WROrd")

                Dim oData As New ArrayList
                If sJobEntr.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("A Job Entry must be selected.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("A Job Entry must be selected.", "ERUSJOBS", Nothing)
                Else
                    oData.Add("DoUpdate")
                    If oGridN.doCheckIsSameCol(pVal.ColUID, "e.U_ZpCd") Then
                        oData.Add("PC=" & oGridN.doGetFieldValue("e.U_ZpCd"))
                        goParent.doOpenModalForm("IDHMAP", oForm, oData)
                    ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "r.U_JobNr") Then
                        oData.Add(sJobEntr)
                        goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
                    ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "r.U_WROrd") OrElse _
                     oGridN.doCheckIsSameCol(pVal.ColUID, "r.U_WRRow") Then
                        oData.Add(sDOJobEntr)
                        setSharedData(oForm, "ROWCODE", sDORowCode)
                        goParent.doOpenModalForm("IDH_DISPORD", oForm, oData)
                    Else
                        If sRowCode.Length > 0 Then
                            'oData.Add(sRowCode)
                            'oData.Add(oGridN.doGetFieldValue("e.U_BDate"))
                            'oData.Add(goParent.doTimeStrToStr(oGridN.doGetFieldValue("e.U_BTime")))
                            'oData.Add(oGridN.doGetFieldValue("e.U_ZpCd"))
                            'oData.Add(oGridN.doGetFieldValue("e.U_Address"))
                            'oData.Add("CANNOTDOCOVERAGE")
                            'oData.Add(oGridN.doGetFieldValue("e.U_SteId"))

                            ''Added to handle the BP Switching
                            'oData.Add(Nothing)
                            'oData.Add(oGridN.doGetFieldValue("e.U_PAddress"))
                            'oData.Add(oGridN.doGetFieldValue("e.U_PZpCd"))
                            'oData.Add(oGridN.doGetFieldValue("e.U_PPhone1"))
                            'oData.Add(oGridN.doGetFieldValue("e.U_FirstBP"))

                            'goParent.doOpenModalForm("IDHJOBS", oForm, oData)

                            Try
                                Dim oOrderRow As com.isb.core.forms.OrderRow = New com.isb.core.forms.OrderRow(oForm.UniqueID, sRowCode)
                                oOrderRow.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                                'oOrderRow.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                                oOrderRow.doShowModal()
                            Catch Ex As Exception
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                            End Try
                        End If
                    End If
                End If
            End If
        End Sub
        ' ''MA Start 05-11-2014 Issue#417 Issue#418
        'Public Overrides Sub doHideTotals(ByVal oForm As SAPbouiCOM.Form)
        '    ''MA Start 03-11-2014 Issue#417 Issue#418
        '    If (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) = False) OrElse (Config.INSTANCE.getParameterAsBool("HDTOTALS", False) = True) Then
        '        Dim oItem As SAPbouiCOM.Item
        '        Dim iBlankColor As Integer = 14930874
        '        For Each sItem As String In moTotalFields_Cost
        '            oItem = oForm.Items.Item(sItem)
        '            oItem.ForeColor = iBlankColor
        '            oItem.BackColor = iBlankColor
        '        Next
        '    End If
        '    If (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) = False) OrElse (Config.INSTANCE.getParameterAsBool("HDTOTALS", False) = True) Then
        '        Dim oItem As SAPbouiCOM.Item
        '        Dim iBlankColor As Integer = 14930874
        '        For Each sItem As String In moTotalFields_Charge
        '            oItem = oForm.Items.Item(sItem)
        '            oItem.ForeColor = iBlankColor
        '            oItem.BackColor = iBlankColor
        '        Next
        '    End If
        'End Sub
        ' ''MA End 05-11-2014 Issue#417 Issue#418
        Protected Overrides Sub doItemGroupCombo(ByRef oForm As SAPbouiCOM.Form)

        End Sub
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)

            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If

            oGridN.doRemoveGridSource("OCRD", "bp")

            'Dim sRequiredFilter As String = "r.U_JobNr = e.Code AND bp.CardCode = r.U_CustCd AND v.Code = r.Code AND e.U_Status In ('1','2','3','4','5') " & _
            Dim sRequiredFilter As String = "r.U_JobNr = e.Code AND v.Code = r.Code AND e.U_Status In ('1','2','3','4','5') " & _
                        " AND ((r.U_WRRow Is Not NULL And r.U_WRRow != '') OR (r.U_WROrd Is Not NULL AND r.U_WROrd != '')) " & _
                        " AND (r.U_PStat Is NULL Or r.U_PStat = '' " & _
                            " Or r.U_PStat = '" & com.idh.bridge.lookups.FixedValues.getStatusOpen() & "'" & _
                            " Or r.U_PStat LIKE '" & com.idh.bridge.lookups.FixedValues.getStatusPreFixReq() & "%'" & _
                            " Or r.U_PStat LIKE '" & com.idh.bridge.lookups.FixedValues.getStatusPreFixDo() & "%'" & _
                            " Or r.U_PStat LIKE '" & com.idh.bridge.lookups.FixedValues.getStatusPreFixReDo() & "%'" & _
                            ") " & _
                        " AND (r.U_Status Is NULL Or r.U_Status = '' " & _
                            " Or r.U_Status = '" & com.idh.bridge.lookups.FixedValues.getStatusOpen() & "'" & _
                            " Or r.U_Status LIKE '" & com.idh.bridge.lookups.FixedValues.getStatusPreFixReq() & "%'" & _
                            " Or r.U_Status LIKE '" & com.idh.bridge.lookups.FixedValues.getStatusPreFixDo() & "%'" & _
                            " Or r.U_Status LIKE '" & com.idh.bridge.lookups.FixedValues.getStatusPreFixReDo() & "%'" & _
                            ") "
            oGridN.setRequiredFilter(sRequiredFilter)
        End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            If sModalFormType = "IDHJOBS" OrElse _
             sModalFormType = "IDHJOBE" OrElse _
             sModalFormType = "IDH_WASTORD" OrElse _
             sModalFormType = "IDH_DISPORD" Then
                doReLoadData(oForm, True)
            End If
        End Sub

        '** Send By Custom Events
        'Return True done
        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                If pVal.BeforeAction = True Then
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, pVal.ItemUID)
                    Dim oMenuItem As SAPbouiCOM.MenuItem
                    oMenuItem = goParent.goApplication.Menus.Item(IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU)

                    Dim oMenus As SAPbouiCOM.Menus
                    Dim iMenuPos As Integer = 1
                    Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                    oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
                    oCreationPackage.Enabled = True
                    oMenus = oMenuItem.SubMenus
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING

                    Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
                    Dim iSelectionCount As Integer = oSelected.Count
                    If iSelectionCount > 0 Then
                        oCreationPackage.UniqueID = "DELINK"
                        oCreationPackage.String = getTranslatedWord("UnLink Orders")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)

                        setWFValue(oForm, "LASTJOBMENU", Nothing)
                    End If
                    Return False
                Else
                    If goParent.goApplication.Menus.Exists("DELINK") Then
                        goParent.goApplication.Menus.RemoveEx("DELINK")
                    End If
                    Return True
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                If pVal.BeforeAction = False Then
                    If pVal.oData.MenuUID.Equals("DELINK") Then
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows()
                        If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then
                            Dim iRow As Integer
                            Dim oWOR As IDH_JOBSHD = New IDH_JOBSHD()
                            For iIndex As Integer = 0 To oSelected.Count - 1
                                'iRow = oSelected.Item(iIndex, SAPbouiCOM.BoOrderType.ot_RowOrder)
                                'iRow = oGridN.getSBOGrid().GetDataTableRowIndex(iRow)
                                iRow = oGridN.GetDataTableRowIndex(oSelected, iIndex)

                                Dim sRowCode As String = oGridN.doGetFieldValue("r.Code", iRow)

                                If oWOR.getByKey(sRowCode) Then
                                    oWOR.doDeLinkDO()

                                    'oWOR.U_WRRow = ""
                                    oWOR.doProcessData()
                                End If

                                ''Dim sJobEntr As String = oGridN.doGetFieldValue("r.U_JobNr", iRow)
                                'Dim sDORowCode As String = oGridN.doGetFieldValue("r.U_WRRow", iRow)
                                ''Dim sDOJobEntr As String = oGridN.doGetFieldValue("r.U_WROrd", iRow)

                                'Dim sQry As String
                                ''DeLink the WO - this will remove the linked Disposal Order Header and Row Numbers, and clear the End Date
                                'sQry = "UPDATE [@IDH_JOBSHD] Set U_WROrd = '', U_WRRow = '', U_AEDATE = NULL WHERE Code = '" & sRowCode & "'"
                                'If DataHandler.INSTANCE.doUpdateQuery(sQry) Then
                                '    'DeLink The DO - this will remove the linked Waste Order Header and Row Numbers
                                '    sQry = "UPDATE [@IDH_DISPROW] Set U_WROrd = '', U_WRRow = '', U_AEDATE = NULL WHERE Code = '" & sDORowCode & "'"
                                '    DataHandler.INSTANCE.doUpdateQuery(sQry)
                                'End If
                            Next
                            doReLoadData(oForm, True)
                        End If
                        Return False
                    End If
                End If
            End If
            Return True
        End Function

    End Class
End Namespace
