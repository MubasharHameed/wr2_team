Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.controls
Imports com.idh.controls.strct
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge.data
Imports WR1_Grids.idh.controls.grid

Namespace idh.forms.manager
    Public Class TFSSummary
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_TFSMGR", "IDHTFS", iMenuPosition, "TFS Summary.srf", False, True, False, "TFS Manager", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "TSFGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "TSFGRID", 7, 105, 850, 322, True)
                End If

                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto

                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False
                oForm.SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Find

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        Protected Overridable Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_CODE", "h.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_TFSNO", "h.U_TFSNUM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_TFSTYP", "h.U_Type", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_STAT", "h.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_NOTCD", "h.U_NOTCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_NOTNM", "h.U_NOTName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_CARCD", "h.U_CARCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CARNM", "h.U_CARName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_CONCD", "h.U_CONCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CONNM", "h.U_CONName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_GENCD", "h.U_GENCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_GENNM", "h.U_GENName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_COECD", "h.U_COECd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_COENM", "h.U_COEName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_DSPCD", "h.U_DSPCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DSPNM", "h.U_DSPName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_FNDCD", "h.U_FDSCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_FNDNM", "h.U_FDSName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_STRTDT", "h.U_SDtFrm", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_ENDDT", "h.U_SDtTo", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)

        End Sub

        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("h.Code", "TFS Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("h.U_TFSNUM", "TFS Number", False, -1, Nothing, Nothing)

            'oGridN.doAddListField("h.U_Status", "Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("(select U_SName from [@IDH_TFSStatus] where U_SCode = h.U_Status ) ", "Status", False, -1, Nothing, Nothing)

            'oGridN.doAddListField("h.U_Type", "Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("(select U_TName from [@IDH_TFSTYPE] where U_TCode = h.U_Type )", "Type", False, -1, Nothing, Nothing)

            oGridN.doAddListField("h.U_NOTCd", "Notifier Cd", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("h.U_NOTName", "Notifier", False, -1, Nothing, Nothing)

            oGridN.doAddListField("h.U_CARCd", "Carrier Cd", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("h.U_CARName", "Carrier", False, -1, Nothing, Nothing)

            oGridN.doAddListField("h.U_CONCd", "Consignor Cd", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("h.U_CONName", "Consignor", False, -1, Nothing, Nothing)

            oGridN.doAddListField("h.U_GENCd", "Generator Cd", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("h.U_GENName", "Generator", False, -1, Nothing, Nothing)

            oGridN.doAddListField("h.U_COECd", "Consignee Cd", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("h.U_COEName", "Consignee", False, -1, Nothing, Nothing)

            oGridN.doAddListField("h.U_DSPCd", "Disposal Facility Cd", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("h.U_DSPName", "Disposal Facility", False, -1, Nothing, Nothing)

            oGridN.doAddListField("h.U_SDtFrm", "Start Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("h.U_SDtTo", "End Date", False, -1, Nothing, Nothing)

            'oGridN.doAddListField("h.U_FDSCd", "No. Of Shipment", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("h.U_FDSName", "Shipments Used", False, -1, Nothing, Nothing)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            'Fill TFS Type and Status combos 
            doFillCombo(oForm, "IDH_TFSTYP", "[@IDH_TFSType]", "U_TCode", "U_TName", "", "", True) ', Nothing, "Cast(Code as Numeric)")
            doFillCombo(oForm, "IDH_STAT", "[@IDH_TFSStatus]", "U_SCode", "U_SName", "", "", True)

            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "TSFGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "TSFGRID", 7, 105, 850, 322, True)
                End If

                'Note: added a view to get results for pbi and non-pbi jobs 
                oGridN.doAddGridTable(New GridTable("@IDH_TFSMAST", "h", "Code", False), True)

                oGridN.setRequiredFilter("")

                doSetFilterFields(oGridN)
                doSetListFields(oGridN)

                oGridN.doSetDoCount(True)
                oGridN.doSetBlankLine(False)

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                doAddWF((oForm), "TFSHasChanged", False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error preloading data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBPL", {Nothing})
            End Try
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "TSFGRID")
                oGridN.doReloadData()
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Loading Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLDBD", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            doLoadData(oForm)
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If Not BubbleEvent Then
                Me.setWFValue((oForm), "TFSHasChanged", False)
                Return False
            End If

            If (pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE) Then
                If ((pVal.BeforeAction = False) AndAlso Not pVal.ItemUID.Equals("TSFGRID")) Then ' If (Not pVal.BeforeAction AndAlso Not pVal.ItemUID.Equals("TSFGRID")) Then
                    If pVal.ItemChanged Then
                        setWFValue((oForm), "TFSHasChanged", True)
                    Else
                        setWFValue((oForm), "TFSHasChanged", False)
                    End If
                End If
            ElseIf (pVal.EventType = SAPbouiCOM.BoEventTypes.et_LOST_FOCUS) Then
                If Not pVal.BeforeAction Then
                    If getWFValue((oForm), "TFSHasChanged").Equals(True) Then
                        Dim oGridN As FilterGrid = DirectCast(IDHGrid.getInstance((oForm), "TSFGRID"), FilterGrid)
                        If (oGridN.getGridControl.getFilterFields.indexOfFilterField(pVal.ItemUID) > -1) Then
                            doLoadData(oForm)
                        End If
                    End If
                    setWFValue((oForm), "TFSHasChanged", False)
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = True Then
                    If pVal.ItemUID = "IDH_NEWTFS" Then
                        setSharedData(oForm, "NEWTFSNO", "NEWTFSNO")
                        goParent.doOpenModalForm("IDH_TFSNEWNo", oForm)
                    End If
                End If
            End If
            Return False
        End Function

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then
                If pVal.ItemUID = "TSFGRID" Then
                    If pVal.BeforeAction = False Then
                        doGridDoubleClick(oForm, pVal)
                    End If
                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalBufferedResult(oParentForm As SAPbouiCOM.Form, ByRef oData As Object, sModalFormType As String, Optional sLastButton As String = Nothing)
            MyBase.doHandleModalBufferedResult(oParentForm, oData, sModalFormType, sLastButton)
        End Sub
        'Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        '    If sModalFormType.Equals("IDH_TFSNEWNo") Then
        '        doLoadData(oForm)
        '    ElseIf sModalFormType.Equals("IDH_TFSMAST") Then
        '        doLoadData(oForm)
        '    End If
        'End Sub

        'Protected Overrides Sub doHandleModalResultShared(oParentForm As SAPbouiCOM.Form, sModalFormType As String, Optional sLastButton As String = Nothing)
        Protected Overrides Sub doHandleModalResultShared(ByVal oParentForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            MyBase.doHandleModalResultShared(oParentForm, sModalFormType, sLastButton)
            doLoadData(oParentForm)
        End Sub

        Protected Overridable Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base)
            Dim oGridN As FilterGrid = pVal.oGrid 'FilterGrid.getInstance(oForm, "TSFGRID")
            If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then

                Dim sTFSCode As String = oGridN.doGetFieldValue("h.Code")
                Dim sTFSNum As String = oGridN.doGetFieldValue("h.U_TFSNUM")

                Dim oData As New ArrayList
                If sTFSCode.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("A TFS Entry must be selected.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("A TFS Entry must be selected.", "ERUSNTFS", {Nothing})
                Else
                    oData.Add("DoUpdate")
                    If sTFSNum.Length > 0 Then
                        oData.Add(sTFSCode)
                        setSharedData(oForm, "TFSCODE", sTFSCode)
                        setSharedData(oForm, "TFSNUM", sTFSNum)
                        goParent.doOpenModalForm("IDH_TFSMAST", oForm, oData)
                    End If
                End If
            End If
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
