Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.controls
Imports com.idh.controls.strct
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge.data
Imports com.idh.bridge
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User


Namespace idh.forms.manager
    Public Class EVN
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHEVNMGR", "IDHPS", iMenuPosition, "EVN Manager.srf", False, True, False, "EVN Manager", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "EVNGRID")
                If oGridN Is Nothing Then
                    'Dim oGridN As OrderRowGrid = New OrderRowGrid(Me, oForm, "LINESGRID", 7, 66, IIf(gsType = "IDHDOM", 990, 785), IIf(gsType = "IDHDOM", 285, 252), "r") '252
                    oGridN = New FilterGrid(Me, oForm, "EVNGRID", 7, 95, 970, 320, True)
                End If

                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto

                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False
                oForm.SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Find

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        Protected Overridable Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_EVNNUM", "evn.U_EVNNumber", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_STDT", "evn.U_StartDt", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WASCD", "evn.U_WasCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WASDC", "evn.U_WasNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_EWC", "evn.U_EWCCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_DSPCD", "evn.U_DispSiteCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DSPNM", "evn.U_DispSiteNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DSPAD", "evn.U_DSAddress", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DSPPC", "evn.U_DSPostCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DSPST", "evn.U_DSStreet", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            'oGridN.doAddFilterField("IDH_PROCD", "evn.U_DispSiteCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_PRONM", "evn.U_DispSiteNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_PROAD", "evn.U_DSAddress", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_PROPC", "evn.U_DSPostCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_PROST", "evn.U_DSStreet", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            'oGridN.doAddFilterField("IDH_HAUCD", "evn.U_DispSiteCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_HAUNM", "evn.U_DispSiteNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_HAUAD", "evn.U_DSAddress", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_HAUPC", "evn.U_DSPostCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_HAUST", "evn.U_DSStreet", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_ISMULT", "evn.U_Duration", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DURATI", "evn.U_Duration", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_UOM", "evn.U_UOM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

        End Sub

        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("evn.Code", "Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("evn.U_EVNNumber", "EVN Number", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_CreateDt", "Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_WasCd", "Waste Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_WasDsc", "Waste Description", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_EWCCd", "EWC Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_EWCDsc", "EWC Desc", False, -1, Nothing, Nothing)

            oGridN.doAddListField("evn.U_DispSiteCd", "Disposal Site Cd", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("evn.U_DispSiteNm", "Disposal Site Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_DSAddress", "Address", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_DSStreet", "Street", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_DSBlock", "Block", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_DSCity", "City", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_DSPostCd", "Post Code", False, -1, Nothing, Nothing)

            oGridN.doAddListField("evn.U_StartDt", "Start Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_Duration", "Duration", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_UOM", "UOM", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_Quantity", "Quanity", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_Year1", "Year 1", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_Year2", "Year 2", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_Year3", "Year 3", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_Year4", "Year 4", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_Year5", "Year 5", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.U_Activity", "Activity", False, -1, Nothing, Nothing)

            oGridN.doAddListField("evn.UsageCurrentYear", "UsageCurrentYear", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.CurrentYearBalance", "CurrentYearBalance", False, -1, Nothing, Nothing)
            oGridN.doAddListField("evn.BalanceRemaining", "Balance Remaining", False, -1, Nothing, Nothing)

            'isPrinted Column 
            Dim sIsPrintedYes As String = IDHAddOns.idh.addon.Base.doFixImagePath("IsPrinted_Yes.png")
            Dim sIsPrintedNo As String = IDHAddOns.idh.addon.Base.doFixImagePath("IsPrinted_No.png")


            oGridN.doAddListField("U_IsPrinted", "Is Printed Flag", False, 0, Nothing, Nothing)
            oGridN.doAddListField("( CASE WHEN ISNULL(U_IsPrinted, '') = 'Y' THEN  ( select '" + sIsPrintedYes + "') ELSE (select '" + sIsPrintedNo + "') END )", "Is Printed", False, 10, "PICTURE", Nothing)

        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            'Fill TFS Type and Status combos 
            'doFillCombo(oForm, "IDH_TFSTYP", "[@IDH_TFSType]", "U_TCode", "U_TName", "", "", True) ', Nothing, "Cast(Code as Numeric)")
            'doFillCombo(oForm, "IDH_STAT", "[@IDH_TFSStatus]", "U_SCode", "U_SName", "", "", True)

            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "EVNGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "EVNGRID", 7, 105, 850, 322, True)
                End If

                'Note: added a view to get results for pbi and non-pbi jobs 
                'oGridN.doAddGridTable(New GridTable("@IDH_EVNMASTER", "h", "Code", False), True)
                oGridN.doAddGridTable(New GridTable("IDH_VEVNMGR", "evn", "Code", False), True)

                oGridN.setRequiredFilter("")

                doSetFilterFields(oGridN)
                doSetListFields(oGridN)

                oGridN.doSetDoCount(True)
                oGridN.doSetBlankLine(False)

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                doAddWF((oForm), "TFSHasChanged", False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error preloading data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBPL", {Nothing})
            End Try
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "EVNGRID")
                oGridN.doReloadData()
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Loading Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLDBD", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            doLoadData(oForm)
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If Not BubbleEvent Then
                Me.setWFValue((oForm), "TFSHasChanged", False)
                Return False
            End If

            If (pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE) Then
                If ((pVal.BeforeAction = False) AndAlso Not pVal.ItemUID.Equals("EVNGRID")) Then ' If (Not pVal.BeforeAction AndAlso Not pVal.ItemUID.Equals("EVNGRID")) Then
                    If pVal.ItemChanged Then
                        setWFValue((oForm), "TFSHasChanged", True)
                    Else
                        setWFValue((oForm), "TFSHasChanged", False)
                    End If
                End If
            ElseIf (pVal.EventType = SAPbouiCOM.BoEventTypes.et_LOST_FOCUS) Then
                If Not pVal.BeforeAction Then
                    If getWFValue((oForm), "TFSHasChanged").Equals(True) Then
                        Dim oGridN As FilterGrid = DirectCast(IDHGrid.getInstance((oForm), "EVNGRID"), FilterGrid)
                        If (oGridN.getGridControl.getFilterFields.indexOfFilterField(pVal.ItemUID) > -1) Then
                            doLoadData(oForm)
                        End If
                    End If
                    setWFValue((oForm), "TFSHasChanged", False)
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = True Then
                    If pVal.ItemUID = "IDH_NEWEVN" Then
                        'setSharedData(oForm, "EVNCODE", "-1")
                        goParent.doOpenModalForm("IDHEVNMAS", oForm)
                    ElseIf pVal.ItemUID = "IDH_PRINT" Then
                        doEVNReport(oForm, "IDH_PRINT")
                        doLoadData(oForm)
                    End If
                End If
            End If
            Return False
        End Function

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then
                If pVal.ItemUID = "EVNGRID" Then
                    If pVal.BeforeAction = False Then
                        doGridDoubleClick(oForm, pVal)
                    End If
                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalBufferedResult(oParentForm As SAPbouiCOM.Form, ByRef oData As Object, sModalFormType As String, Optional sLastButton As String = Nothing)
            MyBase.doHandleModalBufferedResult(oParentForm, oData, sModalFormType, sLastButton)
        End Sub
        'Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        '    If sModalFormType.Equals("IDH_TFSNEWNo") Then
        '        doLoadData(oForm)
        '    ElseIf sModalFormType.Equals("IDH_TFSMAST") Then
        '        doLoadData(oForm)
        '    End If
        'End Sub

        'Protected Overrides Sub doHandleModalResultShared(oParentForm As SAPbouiCOM.Form, sModalFormType As String, Optional sLastButton As String = Nothing)
        Protected Overrides Sub doHandleModalResultShared(ByVal oParentForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            MyBase.doHandleModalResultShared(oParentForm, sModalFormType, sLastButton)
            doLoadData(oParentForm)
        End Sub

        Protected Overridable Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base)
            Dim oGridN As FilterGrid = pVal.oGrid 'FilterGrid.getInstance(oForm, "EVNGRID")
            If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then

                Dim sEVNCode As String = oGridN.doGetFieldValue("evn.Code")
                Dim sEVNNum As String = oGridN.doGetFieldValue("evn.U_EVNNumber")

                Dim oData As New ArrayList
                If sEVNCode.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("A TFS Entry must be selected.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("A row must be selected.", "ERUSNTFS", {Nothing})
                Else
                    oData.Add("DoUpdate")
                    If sEVNNum.Length > 0 Then
                        oData.Add(sEVNCode)
                        setSharedData(oForm, "EVNCODE", sEVNCode)
                        setSharedData(oForm, "EVNNUM", sEVNNum)
                        goParent.doOpenModalForm("IDHEVNMAS", oForm, oData)
                    End If
                End If
            End If
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doClose()
        End Sub

        Protected Overridable Function doEVNReport(ByRef oForm As SAPbouiCOM.Form, ByRef oBtnSource As String) As Boolean
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "EVNGRID")
            If oGridN.getSelectedRows().Count < 1 Then
                DataHandler.INSTANCE.doResUserError("No rows selected. Select a row to print.", "ERUSSRT", {Nothing})
                Return False
            End If

            Dim sEVNCode As String = "'" + String.Join("','", oGridN.doPrepareListFromSelection("evn.Code").ToArray()) + "'"
            If sEVNCode = String.Empty Or sEVNCode = "99999999" Then
                DataHandler.INSTANCE.doResUserError("Can not print EVN with out a valid EVN Code/Number.", "ERUSSRT", {Nothing})
                Return False
            End If

            Dim sReportFile As String = String.Empty
            If (oBtnSource = "IDH_PRINT") Then
                sReportFile = Config.Parameter("EVNRPT")
            End If

            If sReportFile.Length > 0 Then
                Dim oParams As New Hashtable
                oParams.Add("EVNCode", sEVNCode)
                If (Config.INSTANCE.useNewReportViewer()) Then
                    doUpdateIsPrinted(oForm, True, sEVNCode.Trim("'"), oGridN.doPrepareListFromSelection("evn.Code"))
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                    IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                Else
                    doUpdateIsPrinted(oForm, True, sEVNCode, oGridN.doPrepareListFromSelection("evn.Code"))
                    IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                End If
                Return True
            End If

            Return False

        End Function

        Private Sub doUpdateIsPrinted(ByRef oForm As SAPbouiCOM.Form, ByRef bIsPrinted As Boolean, ByRef sKey As String, ByRef aKeys As ArrayList)
            Dim oEVN As IDH_EVNMASTER
            For Each indx As String In aKeys
                oEVN = New IDH_EVNMASTER()
                oEVN.getByKey(indx)
                oEVN.setValue(IDH_EVNMASTER._IsPrinted, "Y")
                oEVN.doUpdateDataRow()
                oEVN = Nothing
            Next
        End Sub

    End Class
End Namespace
