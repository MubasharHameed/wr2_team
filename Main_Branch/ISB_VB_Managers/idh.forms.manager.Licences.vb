Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge
Imports com.idh.bridge.data
Imports WR1_Grids.idh.controls.grid

Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User

Namespace idh.forms.manager
    Public Class Licence
        Inherits IDHAddOns.idh.forms.Base

        Private ghStatusChanged As New ArrayList

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHPM", "IDHPS", iMenuPosition, "Skip Expiry Report.srf", True, True, False, "Permit Manager", load_Types.idh_LOAD_NORMAL )
        End Sub
        
        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
        	oGridN.doAddListField("JobNr", "WO Number", False, 50, Nothing, Nothing)
        	oGridN.doAddListField("RowNr", "Row Number", False, 50, Nothing, Nothing)
        	oGridN.doAddListField("Address", "Site Address", False, 200, Nothing, Nothing)
        	oGridN.doAddListField("ItemGrp", "Container Group", False, 60, Nothing, Nothing)
        	oGridN.doAddListField("LicNr", "Licence Number", False, 60, Nothing, Nothing)
        	oGridN.doAddListField("LicExp", "Expire Date", False, 60, Nothing, Nothing)
        	
        	oGridN.doAddListField("CustCd", "Customer Code", False, 100, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
        	oGridN.doAddListField("CardName", "Customer Name", False, 180, Nothing, Nothing)
        	oGridN.doAddListField("SiteTel", "Site Tel.", False, 100, Nothing, Nothing)
        	oGridN.doAddListField("ReqDate", "Request Date", False, 60, Nothing, Nothing)
        	
        	oGridN.doAddListField("Licences", "Licences", False, 40, Nothing, Nothing)
        	oGridN.doAddListField("OnSite", "OnSite", False, 40, Nothing, Nothing)
        	oGridN.doAddListField("ToDeliver", "To Deliver", False, 40, Nothing, Nothing)
        	oGridN.doAddListField("OnRoadOut", "On Road Out", False, 40, Nothing, Nothing)
        	oGridN.doAddListField("ToCollect", "To Collect", False, 40, Nothing, Nothing)
        	
        	oGridN.doAddListField("h.U_BDate", "Booking Date", False, 0, Nothing, Nothing)
        	oGridN.doAddListField("h.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
        	oGridN.doAddListField("h.U_ZpCd", "Zip Code", False, 0, Nothing, Nothing)
        	oGridN.doAddListField("h.U_SteId", "Site Id", False, 0, Nothing, Nothing)
        	
            'Added to handle the BP Switching
            oGridN.doAddListField("h.U_PAddress", "Producer Address", False, 0, Nothing, Nothing)
            oGridN.doAddListField("h.U_PZpCd", "Producer Post Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("h.U_PPhone1", "Producer Phone", False, 0, Nothing, Nothing)     
            oGridN.doAddListField("h.U_FirstBP", "First BP Selected", False, 0, Nothing, Nothing)
        	
        	'oGridN.doAddListField("U_Comment", "Comment", False, 0, Nothing, Nothing)
        	'oGridN.doAddListField("U_IntComm", "IntComment", False, 0, Nothing, Nothing)
        End Sub
        
        Protected Overridable Sub doSetGridFilters(ByVal oGridN As FilterGrid)
            'oGridN.doAddFilterField("IDH_FDATE", "LicExp", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            'oGridN.doAddFilterField("IDH_TDATE", "LicExp", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
        End Sub
        
        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub
        
        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        	oForm.Freeze(True)
            Try
            	oForm.Title = gsTitle
            	
'                Dim oItem As SAPbouiCOM.Item
                Dim oGridN As FilterGrid = New FilterGrid(Me, oForm, "LINESGRID", 5, 26, 770, 245, False)

                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

                oForm.EnableMenu(Config.NAV_ADD, False)
                oForm.EnableMenu(Config.NAV_FIND, False)
                oForm.EnableMenu(Config.NAV_FIRST, False)
                oForm.EnableMenu(Config.NAV_NEXT, False)
                oForm.EnableMenu(Config.NAV_PREV, False)
                oForm.EnableMenu(Config.NAV_LAST, False)

                Dim fSettings As SAPbouiCOM.FormSettings
                fSettings = oForm.Settings
                fSettings.Enabled = False
                
                doAddUF(oForm, "IDH_FDATE", SAPbouiCOM.BoDataType.dt_DATE, 10, False, False)
                doAddUF(oForm, "IDH_TDATE", SAPbouiCOM.BoDataType.dt_DATE, 10, False, False)

                doSetAffectsMode(oForm, False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try
            oForm.Freeze(False)
        End Sub
        
        
        Private Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find")
                oForm.Items.Item("2").Visible = False
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update")
                oForm.Items.Item("2").Visible = True
            End If
        End Sub
        
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
        	doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New FilterGrid(Me, oForm, "LINESGRID", False)
            End If
            '#MA Start 01-06-2017  -------------> Make Tables nolock, blocking occured at mitie
            oGridN.doAddGridTable(New GridTable("IDH_VLICMAN", Nothing, Nothing, Nothing, True))
            oGridN.doAddGridTable(New GridTable("@IDH_JOBENTR", "h", Nothing, Nothing, True))
            '#MA End 01-06-2017
            
            oGridN.doSetDoCount(True)
            oGridN.doSetBlankLine(False)
            doSetListFields(oGridN)
            doSetGridFilters(oGridN)

            'oGridN.setRequiredFilter("JobNr = h.Code AND JobNr != 'Zz-Ignore'")
            oGridN.setOrderValue("Licences, LicExp, ReqDate")
            
            Dim sToDay As String = goParent.doDateToStr()
            setUFValue(oForm, "IDH_FDATE", sToDay)
            setUFValue(oForm, "IDH_TDATE", sToDay)
        End Sub
        
        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
'            Dim oButton As SAPbouiCOM.Button
            If pVal.BeforeAction = True Then
                If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find") Then
                    doReLoadData(oForm, True)
                End If
                BubbleEvent = False
            End If
        End Sub
        
        Protected Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean )
        	doLoadData(oForm)
        End Sub
        
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                Dim sRequiredFilter As String
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            	If oGridN Is Nothing Then
                	oGridN = New FilterGrid(Me, oForm, "LINESGRID", False)
            	End If
    
    			sRequiredFilter = "JobNr = h.Code AND JobNr != 'Zz-Ignore'"
    
            	Dim sDateFilterLic As String = ""
            	Dim sDateFilterReq As String = ""
                Dim sFDATE As String = getUFValue(oForm, "IDH_FDATE")
                If Not sFDATE Is Nothing OrElse sFDATE.Length > 0 Then
					sDateFilterLic = "CAST(CONVERT(VARCHAR, LicExp,101) As DATETIME) >= CAST('" & sFDATE & "' AS DATETIME)"
					sDateFilterReq = "CAST(CONVERT(VARCHAR, ReqDate,101) AS DATETIME) >= CAST('" & sFDATE & "' AS DATETIME)"
                End If
                Dim sTDATE As String = getUFValue(oForm, "IDH_TDATE")
                If Not sTDATE Is Nothing OrElse sTDATE.Length > 0 Then
                	If sDateFilterLic.Length > 0 Then
                		sDateFilterLic = sDateFilterLic & " AND "
                		sDateFilterReq = sDateFilterReq & " AND "
                	End If
					sDateFilterLic = sDateFilterLic & "CAST(CONVERT(VARCHAR, LicExp,101) As DATETIME) <= CAST('" & sTDATE & "' AS DATETIME)"
					sDateFilterReq = sDateFilterReq & "CAST(CONVERT(VARCHAR, ReqDate,101) AS DATETIME) <= CAST('" & sTDATE & "' AS DATETIME)"
                End If
                
                If sDateFilterLic.Length > 0 Then
                	sRequiredFilter = sRequiredFilter & _
                			" AND " & _
                			"( " & _
                			"   (" & sDateFilterLic & ") " & _
                			"OR (" & _
                			"		( LicNr Is NULL Or LicNr = '')" & _
                			"       AND ( " & sDateFilterReq & ")" & _
                			"   )" & _
                			")"
                End If
                oGridN.setRequiredFilter(sRequiredFilter)
            	oGridN.doReloadData(True)
            Catch ex As Exception
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Protected Overridable Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                oGridN.setCurrentLineByClick(pVal.Row)

                Dim sJobEntr As String = oGridN.doGetFieldValue("JobNr")
                Dim sRowCode As String = oGridN.doGetFieldValue("RowNr")

                Dim oData As New ArrayList
                If sJobEntr.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("A Job Entry must be selected.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("A Job Entry must be selected.", "ERUSJOBS", {Nothing})
                Else
                    oData.Add("DoUpdate")
                    If oGridN.doCheckIsSameCol(pVal.ColUID, "JobNr") Then
                        oData.Add(sJobEntr)
                        goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
                    Else
                        If sRowCode.Length > 0 Then
                            'oData.Add(sRowCode)
                            'oData.Add(oGridN.doGetFieldValue("h.U_BDate"))
                            'oData.Add(goParent.doTimeStrToStr(oGridN.doGetFieldValue("h.U_BTime")))
                            'oData.Add(oGridN.doGetFieldValue("h.U_ZpCd"))
                            'oData.Add(oGridN.doGetFieldValue("Address"))
                            'oData.Add("CANNOTDOCOVERAGE")
                            'oData.Add(oGridN.doGetFieldValue("h.U_SteId"))

                            ''Added to handle the BP Switching
                            'oData.Add(Nothing)
                            'oData.Add(oGridN.doGetFieldValue("h.U_PAddress"))
                            'oData.Add(oGridN.doGetFieldValue("h.U_PZpCd"))
                            'oData.Add(oGridN.doGetFieldValue("h.U_PPhone1"))
                            'oData.Add(oGridN.doGetFieldValue("h.U_FirstBP"))

                            'goParent.doOpenModalForm("IDHJOBS", oForm, oData)

                            Try
                                Dim oOrderRow As com.isb.core.forms.OrderRow = New com.isb.core.forms.OrderRow(oForm.UniqueID, sRowCode)
                                oOrderRow.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                                'oOrderRow.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                                oOrderRow.doShowModal()
                            Catch Ex As Exception
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                            End Try
                        End If
                    End If
                End If
            End If
        End Sub
        
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = False Then
                        doGridDoubleClick(oForm, pVal, BubbleEvent)
                    End If
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doClose()
        End Sub
        
        
                '** Send By Custom Events
        'Return True done
        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                If pVal.BeforeAction = True Then
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, pVal.ItemUID)
                    Dim oMenuItem As SAPbouiCOM.MenuItem
                    oMenuItem = goParent.goApplication.Menus.Item(IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU)

                    Dim oMenus As SAPbouiCOM.Menus
                    Dim iMenuPos As Integer = 1
                    Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                    oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
                    oCreationPackage.Enabled = True
                    oMenus = oMenuItem.SubMenus
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING

					setWFValue(oForm, "LASTJOBMENU", Nothing)
                    Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
                    Dim iSelectionCount As Integer = oSelected.Count
                    Dim sColJob As String = Config.Parameter("WOJBCOL")
                    Dim sLicJob As String = Config.Parameter("WOJBLRN")
                    If iSelectionCount > 0 Then
                        If iSelectionCount = 1 Then
                        	Dim oMenuList As New ArrayList
                            Dim sJob As String

                        	If Not sColJob Is Nothing AndAlso sColJob.Length > 0 Then
	                            Dim oDt(2) As Object

	                            sJob = sColJob
	                            oDt(1) = sJob
	                            oCreationPackage.String = getTranslatedWord("Create a [%1] Order", sJob)
	                            
	                            If sJob.Length > 8 Then
	                                sJob = "JB_" & sJob.Substring(0, 6).ToUpper()
	                            Else
	                                sJob = "JB_" & sJob.ToUpper()
	                            End If
	
	                            oDt(0) = sJob
	                            oMenuList.Add(oDt)
	                            If goParent.goApplication.Menus.Exists(sJob) = False Then
	                                oCreationPackage.UniqueID = sJob
	                                oCreationPackage.Position = iMenuPos
	                                iMenuPos += 1
	                                oMenus.AddEx(oCreationPackage)
	                            End If
                        	End If

                        	If Not sLicJob Is Nothing AndAlso sLicJob.Length > 0 Then
	                            Dim oDt(2) As Object

	                            sJob = sLicJob
	                            oDt(1) = sJob
	                            oCreationPackage.String = getTranslatedWord("Create a [%1] Order", sJob)
	                            
	                            If sJob.Length > 8 Then
	                                sJob = "JB_" & sJob.Substring(0, 6).ToUpper()
	                            Else
	                                sJob = "JB_" & sJob.ToUpper()
	                            End If
	
	                            oDt(0) = sJob
	                            oMenuList.Add(oDt)
	                            If goParent.goApplication.Menus.Exists(sJob) = False Then
	                                oCreationPackage.UniqueID = sJob
	                                oCreationPackage.Position = iMenuPos
	                                iMenuPos += 1
	                                oMenus.AddEx(oCreationPackage)
	                            End If
                        	End If

                        	setWFValue(oForm, "LASTJOBMENU", oMenuList)
                        End If
                    End If
                    Return False
                Else
                    Dim oMenuList As ArrayList = getWFValue(oForm, "LASTJOBMENU")
                    Dim sJob As String
                    If Not oMenuList Is Nothing AndAlso oMenuList.Count > 0 Then
                        For iJob As Integer = 0 To oMenuList.Count - 1
                            sJob = oMenuList.Item(iJob)(0)
                            If goParent.goApplication.Menus.Exists(sJob) Then
                                goParent.goApplication.Menus.RemoveEx(sJob)
                            End If
                        Next
                    End If
                    setWFValue(oForm, "LASTJOBMENU", Nothing)
                    Return True
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                If pVal.BeforeAction = False Then
					If pVal.oData.MenuUID.StartsWith("JB_") Then
                        Dim oMenuList As ArrayList = getWFValue(oForm, "LASTJOBMENU")
                        Dim sMenuItem As String
                        Dim sJob As String
                        If Not oMenuList Is Nothing AndAlso oMenuList.Count > 0 Then
                            For iJob As Integer = 0 To oMenuList.Count - 1
                                sMenuItem = oMenuList.Item(iJob)(0)
                                If pVal.oData.MenuUID.Equals(sMenuItem) Then
                                    sJob = oMenuList.Item(iJob)(1)
                                    Dim sRowCode As String = doDuplicate(oForm, sJob)
                                    If Not sRowCode Is Nothing AndAlso sRowCode.Length > 0 Then
                                        If sRowCode.Length > 0 Then

                                            'Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, pVal.ItemUID)
                                            '                                 oGridN.setCurrentLineByClick(pVal.Row)

                                            '                                 Dim oData As New ArrayList
                                            '                                 oData.Add("DoAuto")
                                            '                                 oData.Add(sRowCode)
                                            '                 				oData.Add(oGridN.doGetFieldValue("h.U_BDate"))
                                            '                 				oData.Add(goParent.doTimeStrToStr(oGridN.doGetFieldValue("h.U_BTime")))
                                            '                 				oData.Add(oGridN.doGetFieldValue("h.U_ZpCd"))
                                            '                 				oData.Add(oGridN.doGetFieldValue("Address"))
                                            '                                 oData.Add("CANNOTDOCOVERAGE")
                                            '                                 oData.Add(oGridN.doGetFieldValue("h.U_SteId"))

                                            '                                 Dim sLicJob As String = Config.Parameter("WOJBLRN")
                                            '                                 If Not sLicJob Is Nothing AndAlso sJob.Equals(sLicJob) Then
                                            '                                 	oData.Add("DOADDLIC")
                                            '                                 Else
                                            '                                 	oData.Add(Nothing)
                                            '                                 End If

                                            '                                 'Added to handle the BP Switching
                                            '                 				oData.Add(oGridN.doGetFieldValue("h.U_PAddress"))
                                            '                 				oData.Add(oGridN.doGetFieldValue("h.U_PZpCd"))
                                            '                 				oData.Add(oGridN.doGetFieldValue("h.U_PPhone1"))
                                            '                                 oData.Add(oGridN.doGetFieldValue("h.U_FirstBP"))

                                            '                                 goParent.doOpenModalForm("IDHJOBS", oForm, oData)

                                            Try
                                                Dim oOrderRow As com.isb.core.forms.OrderRow = New com.isb.core.forms.OrderRow(oForm.UniqueID, sRowCode)
                                                oOrderRow.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                                                'oOrderRow.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                                                oOrderRow.doShowModal()
                                            Catch Ex As Exception
                                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                                            End Try
                                            Return False
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FORMAT_FIELDS Then
                If pVal.BeforeAction = False Then
                End If
            End If
            Return True
        End Function

        Private Function doDuplicate(ByVal oForm As SAPbouiCOM.Form, Optional ByVal sJob As String = Nothing) As String
            Dim sNewCode As String = Nothing

            Dim oTableSrc As SAPbobsCOM.UserTable = Nothing
            Try
                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                oTableSrc = goParent.goDICompany.UserTables.Item("IDH_JOBSHD")
                oSelected = oGridN.getGrid().Rows.SelectedRows()
                
                Dim bCopyComments As Boolean = False
                Dim bCopyPrice As Boolean = False
                Dim bCopyQty As Boolean = False

                If Not oSelected Is Nothing Then
                    Dim iRow As Integer
                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iRow = oGridN.GetDataTableRowIndex(oSelected, iIndex)

                        Dim sCode As String
                        sCode = oGridN.doGetFieldValue("RowNr", iRow)

                        'sNewCode = idh.forms.OrderRow.doDuplicateRow_(goParent, sCode, bCopyComments, bCopyPrice, bCopyQty, sJob, True, True)
                        sNewCode = IDH_JOBSHD.doDuplicateRow(sCode, bCopyComments, bCopyPrice, bCopyQty, sJob, True, True)
                    Next
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error duplicating rows.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBDS", {String.Empty, String.Empty})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oTableSrc)
            End Try
            Return sNewCode
        End Function

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            'If sModalFormType = "IDHJOBS" OrElse 
            If sModalFormType = "IDHJOBE" OrElse sModalFormType = "IDH_WASTORD" Then
                doLoadData(oForm)
            End If
        End Sub

        Public Function Handler_WOROKReturn(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            doReLoadData(oOForm.SBOParentForm, True)
            Return True
        End Function
    End Class
End Namespace
