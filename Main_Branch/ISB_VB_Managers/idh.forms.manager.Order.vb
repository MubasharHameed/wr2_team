Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System.Net
Imports System.Web
Imports System
Imports com.idh.utils.Conversions
Imports com.idh.utils
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.data
Imports com.idh.dbObjects.User
Imports com.idh.bridge.lookups
Imports SAPbouiCOM

Namespace idh.forms.manager
    Public Class Order
        Inherits idh.forms.manager.Templ

        Dim count As String
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            Me.New(oParent, iMenuPosition, Nothing, Nothing, Nothing)
        End Sub

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer, ByVal sType As String, ByVal sSRF As String, ByVal sTitle As String)
            MyBase.New(oParent, (If(sType Is Nothing, "IDHJOBR", sType)), Nothing, (If(sSRF Is Nothing, "Job Manager.srf", sSRF)), iMenuPosition, (If(sTitle Is Nothing, "Order Schedule Manager L1", sTitle)))
        End Sub


        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            'If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
            '    If doSetGrid(oGridN) Then
            '        Return
            '    End If
            'End If
            'Dim sComment As String = "CASE when r.U_Comment Is NULL Or r.U_Comment = '' then '' else '*' end"
            'oGridN.doAddListField(sComment, "C", False, -1, Nothing, Nothing)

            'oGridN.doAddListField("v.State", getTranslatedWord("Progress"), False, -1, Nothing, Nothing)
            'oGridN.doAddListField("'" & IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\' + v.IMG", "", False, 10, "PICTURE", Nothing)
            'oGridN.doAddListField("v.IMG", "", False, 10, "PICTURE", Nothing)

            ''MA 04-04-2016 skip adding field if form setting is true and skip syncronization is set to true
            ''test in progress
            ' If (Config.INSTANCE.getParameterAsBool("FORMSET", False)) = False OrElse (Config.INSTANCE.getParameterAsBool("FSSYNCSP", False)) = False Then
            oGridN.doAddListField(doGetProgressStatusQuery, "Progress", False, -1, Nothing, Nothing)
            oGridN.doAddListField(doGetProgressImageQuery, "", False, 10, "PICTURE", Nothing)
            'End If



            oGridN.doAddListField("r.U_JobNr", "Order No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Row No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_CusQty", "Cont Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HlSQty", "Cont Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RDate", "Req. Start", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTime", "Req. Time", True, -1, "TIME", Nothing)
            oGridN.doAddListField("r.U_ASDate", "Act. Start", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASTime", "Act STime", True, -1, "TIME", Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act. End", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_AETime", "Act ETime", True, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_Lorry", "Veh Reg.", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_LorryCd", "Veh Reg. Code", True, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", True, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, -1, Nothing, Nothing)
            '#MA Start 17-05-2017 issue#402 point 1&2
            'oGridN.doAddListField("ad.U_LAT", "Latitude", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("ad.U_LONG", "Longitude", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_LATI", "Latitude", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_LONGI", "Longitude", False, -1, Nothing, Nothing)

            '#MA End 17-05-2017
            'Column to display Additional Items 
            'OLD Query: (SELECT CASE count(*) WHEN 0 THEN ' ' ELSE 'Y' END FROM [@IDH_WOADDEXP] WHERE U_RowNr = r.Code AND (U_SONr!='' OR U_SINVNr!='' Or U_PONr!='' OR U_PINVNr!=''))
            'oGridN.doAddListField("(SELECT CASE count(*) WHEN 0 THEN ' ' ELSE 'Y' END FROM [@IDH_WOADDEXP] WITH(NOLOCK) WHERE U_RowNr = r.Code )", "Additional Items", False, -1, Nothing, Nothing)

            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_Address", "Site Address", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_Street", "Street", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_Block", "Block", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_City", "City", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_Phone1", "Main No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_SiteTl", "Site Tel No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_Contact", "Contact Person", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_SteId", "Site Id", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r." + IDH_JOBSHD._ProCd, "Producer Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r." + IDH_JOBSHD._ProNm, "Producer Name", False, 70, Nothing, Nothing)
            oGridN.doAddListField("r.U_Comment", "Comment", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_None, True, True)

            oGridN.doAddListField("r.U_ConNum", "Consignment Number", True, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_CstWgt", "Waste Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Price", "Haulage Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "Tip Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("CAST(((U_TCTotal + U_Price) - r.U_DisAmt) As Numeric(19,2))", "Sub After Discount", False, -1, Nothing, "SubAftDisc")
            'oGridN.doAddListField("(U_TCTotal +(U_CusQty * U_CusChr)) - r.U_DisAmt", "Sub After Discount", False, -1, Nothing, "SubBefDisc")
            oGridN.doAddListField("r.U_SLicCh", "Skip Lic Charge", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CarrCd", "Carrier Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_CarrNm", "Carrier Name", False, 70, Nothing, Nothing)
            oGridN.doAddListField("r.U_Tip", "Disposal Site", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_JCost", "Total Order Cost", False, -1, Nothing, "TOrdCost")
            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTimeT", "Req. Time To", True, -1, "TIME", Nothing)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_VehTyp", "Vehicle", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_IDHRTCD", "Route", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_IDHSEQ", "Sequence", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_Status", "Sales Status", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PStat", "Purchase Status", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItmGrp", "Container Grp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("r.U_Serial", "Item Serial No.", False, 0, Nothing, Nothing)
            '..
            '..
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)
            '..
            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RdWgt", "Read Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Total Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "TipCost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "TCharge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Covera", "Coverage.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r." & IDH_JOBSHD._Rebate, "Rebate", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_WROrd", "WO No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WRRow", "WO Row No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CoverHst", "Coverage Hist.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtRt", "Tipping Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtRt", "Haulage Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtRt", "Tipping Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtRt", "Supplier Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtRt", "Haulage Vat Purchase Rate", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtGrp", "Tipping Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtGrp", "Haulage Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtGrp", "Tipping Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtGrp", "Supplier Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtGrp", "Haulage Vat Purchase Group", False, 0, Nothing, Nothing)

            ''Marketing Documents
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._SAINV, "Sales Invoice", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._SLPO, "Skip licence PO", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._SAORD, "Sales Order", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._GRIn, "Goods Receipt Incomming", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._TIPPO, "Tipping PO", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._JOBPO, "Haulage PO", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._ProPO, "Producer PO", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._CustGR, "Customer Goods Receipt", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._ProGRPO, "Producer Goods Receipt PO", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._SODlvNot, "Do Stock Movement", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._Jrnl, "Journal Entry", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._TPCN, "Disposal Purchase Credit Note", False, 0, Nothing, Nothing)
            'oGridN.doAddListFieldNotPinned("r." & IDH_JOBSHD._TCCN, "Customer Credit Note", False, 0, Nothing, Nothing)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            doAddTotalFields(oForm)
            '## MA Start 01-09-2014 Issue#421
            doAddUF(oForm, "IDH_BRANC1", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 2000, False, False)
            '## MA Start 01-09-2014 Issue#421
            setVisible(oForm, "IDH_CERT", True)
            'oForm.Items.Item("IDH_CERT").Visible = True
        End Sub
        Public Overrides Sub doBeforeLoadData(oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
            '## MA Start 02-09-2014 Issue#421
            doSetMultiBranch(oForm)
            '## MA End 02-09-2014 Issue#421
        End Sub

        Protected Overrides Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean)
            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            '##MA Issue No 869 Start 30-07-2015
            If Config.ParameterAsBool("ENBODRWO", False) AndAlso Not Config.ParameterAsBool("FORMSET", False) Then
                oGridN.setOrderValue("Cast(r.Code as Int)")
            End If
            '#MA 20170523 issue:424
            If Not String.IsNullOrEmpty(getParentSharedData(oForm, "IDH_CUSTREF")) Then
                setItemValue(oForm, "IDH_REF", getParentSharedData(oForm, "IDH_CUSTREF"), True)
            End If
            '#MA 20170523 endss
            MyBase.doReLoadData(oForm, bIsFirst)

            oGridN.doSetDeleteActive(True)
            If Config.Parameter("OSMATO") = "TRUE" Then
                doGridTotals(oGridN, False)
                setVisible(oForm, "IDH_CALC", False)
                'oForm.Items.Item("IDH_CALC").Visible = False
            Else
                doGridTotals(oGridN, True)
                setVisible(oForm, "IDH_CALC", True)
                'oForm.Items.Item("IDH_CALC").Visible = True
            End If
        End Sub

        '        Protected Overrides Sub doTotals(ByVal oGridN As UpdateGrid, Optional ByVal bDoZero As Boolean = False)
        '            Dim dCstWgt As Double = 0
        '            Dim dAftDisc As Double = 0
        '            Dim dTotal As Double = 0
        '            Dim dDisAmt As Double = 0
        '            Dim dAddEx As Double = 0
        '            Dim dTxAmt As Double = 0
        '
        '            If bDoZero = False Then
        '                Dim iCount As Integer = oGridN.getRowCount()
        '                Dim oData As SAPbouiCOM.DataTable = oGridN.getDataTable()
        '                If iCount > 0 Then
        '                    For iRow As Integer = 0 To iCount - 1
        '                        dCstWgt = dCstWgt + oData.GetValue(oGridN.doIndexField("r.U_CstWgt"), iRow)
        '                        dTotal = dTotal + oData.GetValue(oGridN.doIndexField("r.U_Total"), iRow)
        '
        '                        dAftDisc = dAftDisc + oData.GetValue(oGridN.doIndexField("SubAftDisc"), iRow)
        '
        '                        dDisAmt = dDisAmt + oData.GetValue(oGridN.doIndexField("r.U_DisAmt"), iRow)
        '                        dAddEx = dAddEx + oData.GetValue(oGridN.doIndexField("r.U_AddEx"), iRow)
        '                        dTxAmt = dTxAmt + oData.GetValue(oGridN.doIndexField("r.U_TaxAmt"), iRow)
        '                    Next
        '                End If
        '            End If
        '
        '            setUFValue(oGridN.getSBOForm(), "IDH_CstWgt", dCstWgt)
        '            setUFValue(oGridN.getSBOForm(), "IDH_SubAD", dAftDisc)
        '            setUFValue(oGridN.getSBOForm(), "IDH_Total", dTotal)
        '            setUFValue(oGridN.getSBOForm(), "IDH_DisAmt", dDisAmt)
        '            setUFValue(oGridN.getSBOForm(), "IDH_AddEx", dAddEx)
        '            setUFValue(oGridN.getSBOForm(), "IDH_TaxAmt", dTxAmt)
        '        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_WABT" Then
                        Dim sCustomer As String = getUFValue(oForm, "IDH_CUST")
                        Dim sSiteAddr As String = getUFValue(oForm, "IDH_ADDR")

                        If sCustomer.Trim().Length > 0 Then
                            Dim sReportFile As String
                            sReportFile = Config.Parameter("WOCWAB")

                            Dim oParams As New Hashtable
                            oParams.Add("Customer", sCustomer)
                            oParams.Add("SiteAddr", sSiteAddr)
                            If (com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("UNEWRPTV", False)) Then
                                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                            Else
                                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                            End If
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_CUST" AndAlso oForm.TypeEx <> "IDHUPDTR" Then
                        Dim sCustomer As String = getUFValue(oForm, "IDH_CUST")
                        Try
                            Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_WABT")
                            If sCustomer.Length > 0 Then
                                oItem.Visible = True
                            Else
                                oItem.Visible = False
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                    'Else
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.ItemUID = "LINESGRID" AndAlso pVal.CharPressed <> 9 AndAlso pVal.Row > -1 Then
                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    ''##MA Start 16-09-2015 Issue#831 'do not allow completed job to edit
                    'If oGridN.getSBOGrid().Rows.SelectedRows.Count > 0 Then
                    'For iRow As Int16 = 0 To oGridN.getSBOGrid().Rows.SelectedRows.Count - 1
                    Dim sProgress As String = oGridN.doGetFieldValue("Progress")
                    If sProgress IsNot Nothing AndAlso sProgress.Trim.ToUpper = com.idh.bridge.lookups.FixedValues.getProgressDone(True).ToUpper.Trim Then
                        If Config.INSTANCE.getParameterWithDefault("OSMOPNCL", "").Trim <> String.Empty Then
                            Dim sEditableFields() As String = Config.INSTANCE.getParameterWithDefault("OSMOPNCL", "").Trim.Split(",")
                            For iColList As Int16 = 0 To sEditableFields.Length - 1
                                If oGridN.Columns.Item(pVal.ColUID).TitleObject.Caption = sEditableFields(iColList) Then
                                    'If oGridN.doCheckIsSameCol(pVal.ColUID, sEditableFields(iColList)) Then
                                    Return False
                                End If
                            Next
                        End If
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("You cannot edit a completed job.", "ERJBCOMP", Nothing)
                        BubbleEvent = False
                    End If
                    'Next
                    'End If
                    ''##MA End 16-09-2015 Issue#831 'do not allow completed job to edit
                End If

            End If
            Return False
        End Function

        Protected Overrides Sub doWorksheetBtn(ByVal oForm As SAPbouiCOM.Form)
            setSharedData(oForm, "PAR1", "001")
            goParent.doOpenModalForm("IDH_WTWSOP", oForm)
        End Sub

        Protected Overrides Function doSaveOrderRows(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            Dim bResult As Boolean = True
            bResult = bResult And oGridN.doProcessRemovedRows("[@IDH_JOBSHD]")
            bResult = bResult And MyBase.doSaveOrderRows(oForm)
            Return bResult
        End Function

        Protected Sub doNotifyDriver(ByVal oForm As SAPbouiCOM.Form)
            'http://10.0.0.230:8080/IDH.SMSWeb/smssender.jsp?SMSCommand=SMS&username=null&password=null&destination=%2B447957246439&message=Test+sms+from+sa&from=Louis+Viljoen

            Dim sDestination As String = Config.INSTANCE.getDriverNumber("Test")
            If sDestination Is Nothing OrElse sDestination.Length < 12 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("The Contact number is incorrect - ICR Contact: " & sDestination)
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Contact number is incorrect - ICR Contact: " + sDestination, "ERUSICRC", {com.idh.bridge.Translation.getTranslatedWord(sDestination)})
                Exit Sub
            End If
            Dim sSMSUrl As String = Config.Parameter("WTSMSU")
            If sSMSUrl Is Nothing OrElse sSMSUrl.Length < 10 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("The SMS server URL is incorrect - ICR URL: " & sSMSUrl)
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("The SMS server URL is incorrect - ICR URL: " + sSMSUrl, "ERUSICRU", {sSMSUrl})
                Exit Sub
            End If

            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim soSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows()
            Dim iSelected As Integer = soSelected.Count
            If iSelected = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("No Jobs were selected.")
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("No Jobs were selected.", "ERUSJOBS", {Nothing})
                Exit Sub
            Else
                Dim sBP As String
                Dim sWO As String
                Dim sRow As String
                Dim sContainer As String
                Dim sWasteCode As String
                Dim sOrderType As String
                Dim sMessage As String
                Dim sQty As String

                Dim iRowNum As Integer
                For iIndex As Integer = 0 To iSelected - 1
                    'iRowNum = soSelected.Item(iIndex, SAPbouiCOM.BoOrderType.ot_SelectionOrder)
                    'iRowNum = oGridN.getGrid.GetDataTableRowIndex(iRowNum)
                    iRowNum = oGridN.getDataTableRowIndex(soSelected, iIndex)

                    sDestination = System.Web.HttpUtility.UrlEncode(sDestination)
                    sBP = System.Web.HttpUtility.UrlEncode(oGridN.doGetFieldValue("e.U_CardCd", iRowNum))
                    sWO = oGridN.doGetFieldValue("r.U_JobNr", iRowNum)
                    sRow = oGridN.doGetFieldValue("r.Code", iRowNum)
                    sContainer = System.Web.HttpUtility.UrlEncode(oGridN.doGetFieldValue("r.U_ItemCd", iRowNum))
                    sWasteCode = System.Web.HttpUtility.UrlEncode(oGridN.doGetFieldValue("r.U_WasCd", iRowNum))
                    sOrderType = System.Web.HttpUtility.UrlEncode(oGridN.doGetFieldValue("r.U_JobTp", iRowNum))
                    sQty = System.Web.HttpUtility.UrlEncode(ToString(oGridN.doGetFieldValue("r.U_CstWgt", iRowNum)))
                    sMessage = "Cust - " & sBP & ToChar(13) &
                                             "Job - " & sOrderType & ToChar(13) &
                                             "WO.RO - " & sWO & "." & sRow & ToChar(13) &
                                             "Container - " & sContainer & ToChar(13) &
                                             "Barcode - ................" & ToChar(13) &
                                             "Waste - " & sWasteCode & ToChar(13) &
                                             "QTY - " & sQty
                    If sMessage.Length > 150 Then
                        sMessage = sMessage.Substring(0, 150)
                    End If
                    sMessage = System.Web.HttpUtility.UrlEncode(sMessage)

                    Dim sURL As String = sSMSUrl &
                                "SMSCommand=SSMS" &
                                "&username=ISB" &
                                "&password=SBO" &
                                "&destination=" & sDestination &
                                "&message=" & sMessage &
                                "&from=SBO_OSM"

                    Dim client As New WebClient
                    client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)")

                    Dim data As Stream = client.OpenRead(sURL)
                    Dim reader As New StreamReader(data)
                    Dim s As String = reader.ReadToEnd()
                    data.Close()
                Next
            End If
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDH_WTWSOP" Then
                    Dim sPrintWorksheet As String = getSharedData(oForm, "IDH_WTPW")
                    Dim sNotifyDriver As String = getSharedData(oForm, "IDH_WTND")

                    If sPrintWorksheet.Equals("Y") Then
                        MyBase.doWorksheetBtn(oForm)
                    End If

                    If sNotifyDriver.Equals("Y") Then
                        doNotifyDriver(oForm)
                    End If
                    'ElseIf sModalFormType = "IDH_OSMEDIT" Then
                    '    MyBase.doSetValues(oForm)
                Else
                    MyBase.doHandleModalResultShared(oForm, sModalFormType, sLastButton)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal results - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub
        '##MA 1252017 start
        Public Overrides Sub doButtonID1(oForm As Form, ByRef pVal As ItemEvent, ByRef BubbleEvent As Boolean)
            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            If ValidateCustRefonSelection(oForm, oGridN) = False Then
                BubbleEvent = False
                Exit Sub
            End If
            MyBase.doButtonID1(oForm, pVal, BubbleEvent)
            '#MA 20170102 Start
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE AndAlso BubbleEvent = False AndAlso oGridN.getRowCount() < count Then
                Dim TaRows As String = oGridN.getRowCount()
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    setParentSharedData(oForm, "rowDeleted", "True")
                End If
            End If
            '#MA 20170102 End
        End Sub
        '##MA 1252017
        Public Overrides Sub doClose()
            MyBase.doClose()
        End Sub

        Public Function ValidateCustRefonSelection(ByVal oForm As SAPbouiCOM.Form, ByVal oGridN As OrderRowGrid) As Boolean
            Dim aRowsList As ArrayList = New ArrayList()
            Dim aRows As ArrayList = Nothing
            Dim sParams As String()
            aRows = oGridN.doGetChangedRows()
            If Not aRows Is Nothing Then
                For index As Integer = 0 To aRows.Count - 1
                    oGridN.setCurrentDataRowIndex(CType(aRows(index), Integer))
                    Dim sAddress1 As String = oGridN.doGetFieldValue("e.U_Address")
                    Dim sCustRef1 As String = oGridN.doGetFieldValue("r.U_CustRef")
                    Dim sCustomer1 As String = oGridN.doGetFieldValue("e.U_CardCd")
                    If sCustRef1 Is Nothing OrElse sCustRef1.Length = 0 Then
                        Dim sRowSta As String = oGridN.doGetFieldValue("r.U_RowSta")
                        If Config.INSTANCE.doGetCustRefRequired(sCustomer1, sAddress1) = True AndAlso com.idh.bridge.lookups.FixedValues.isDeleted(sRowSta) = False Then
                            aRowsList.Add(aRows(index))
                        End If
                    End If
                Next
            End If
            sParams = New String() {General.doCreateIntCSL(aRowsList, True)}
            If aRowsList IsNot Nothing AndAlso aRowsList.Count > 0 Then
                doResError("Error doing Button 1.", "WORCRR1", sParams)
                Return False
            End If
            Return True
        End Function
    End Class
End Namespace
