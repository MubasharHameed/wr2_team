Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.data
Imports com.idh.bridge.lookups

Namespace idh.forms.manager
    Public Class Track
        Inherits idh.forms.manager.Templ

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTTT", "IDHTTS", "Waste Manager.srf", iMenuPosition, "Waste Tracking")
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            oGridN.doAddListField("t.U_WTWO", "Order No.", False, -1, Nothing, Nothing, &HEECCCC)
            oGridN.doAddListField("t.U_WTRO", "Row No.", False, -1, Nothing, Nothing, &HEECCCC)
            oGridN.doAddListField("t.U_WTSD", "Scanned Date", False, -1, Nothing, Nothing, &HEECCCC)
            oGridN.doAddListField("t.U_WTST", "Scanned Time", False, -1, Nothing, "TIME", &HEECCCC)
            oGridN.doAddListField("t.Code", "Tracking No.", False, -1, Nothing, Nothing, &HEECCCC)
            oGridN.doAddListField("t.U_WTAT", "Action Type", False, -1, Nothing, Nothing, &HEECCCC)
            oGridN.doAddListField("t.U_WTCB", "Container Barcode", False, -1, Nothing, Nothing, &HEECCCC)
            oGridN.doAddListField("t.U_WTLC", "Location", False, -1, Nothing, Nothing, &HEECCCC)
            oGridN.doAddListField("t.U_WTWC", "Waste Code", False, -1, Nothing, Nothing, &HEECCCC)
            oGridN.doAddListField("t.U_WTWD", "Waste Desc", False, 0, Nothing, Nothing, &HEECCCC)
            oGridN.doAddListField("t.U_WTWN", "UN No.", False, -1, Nothing, Nothing, &HEECCCC)
            oGridN.doAddListField("t.U_WTTFS", "TFS No.", False, -1, Nothing, Nothing, &HEECCCC)
            oGridN.doAddListField("t.U_WTWQ", "Scan Qty", False, -1, Nothing, Nothing, &HEECCCC)

            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, 0, Nothing, Nothing)

            oGridN.doAddListField("e.Code", "Order No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Row No.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_CusQty", "Cont Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RDate", "Req. Start", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTime", "Req. Time", False, -1, "TIME", Nothing)
            oGridN.doAddListField("r.U_ASDate", "Act. Start", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASTime", "Act. Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act. End", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_AETime", "Act ETime", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_Lorry", "Veh Reg.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_Address", "Address Name", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_Street", "Street", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_Block", "Block", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_Phone1", "Main No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_SiteTl", "Site Tel No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_SteId", "Site Id", False, 0, Nothing, Nothing)
            
            'Added to handle the BP Switching
            oGridN.doAddListField("e.U_PAddress", "Producer Address", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_PZpCd", "Producer Post Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_PPhone1", "Producer Phone", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_FirstBP", "First BP Selected", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Price", "Haulage Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "Tip Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("(U_TCTotal +(U_CusQty * U_CusChr)) - r.U_DisAmt", "Sub After Discount", False, -1, Nothing, "SubBefDisc")
            oGridN.doAddListField("r.U_SLicCh", "Skip Lic Charge", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CarrCd", "Carrier Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Tip", "Disposal Site", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JCost", "Total Order Cost", False, -1, Nothing, "TOrdCost")
            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Comment", "Comment", False, -1, Nothing, Nothing)

            oGridN.doAddListField("e.U_Route", "Route", True, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_Seq", "Sequence", True, -1, Nothing, Nothing)

            'oGridN.doAddListField("'" & IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\' + State + '.png'", "P", False, 10, "PICTURE", Nothing)

            'oGridN.doAddListField("'Late'", "Progress", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("'IMG'", "P", False, 10, "PICTURE", Nothing)

            oGridN.doAddListField("r.U_Status", "Sales Status", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PStat", "Purchase Status", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItmGrp", "Container Grp", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Serial", "Item Serial No.", False, 0, Nothing, Nothing)
            '..
            '..
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)
            '..
            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Cust. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RdWgt", "Read Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Total Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_VehTyp", "Vehicle", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "TipCost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "TCharge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CarrNm", "Carrier Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_Covera", "Coverage.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtRt", "Tipping Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtRt", "Haulage Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtRt", "Tipping Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtRt", "Supplier Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtRt", "Haulage Vat Purchase Rate", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("r.U_TChrgVtGrp", "Tipping Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtGrp", "Haulage Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtGrp", "Tipping Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtGrp", "Supplier Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtGrp", "Haulage Vat Purchase Group", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("r.U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)

        End Sub
        
        Protected Overrides Sub doSetGridFilters(ByVal oGridN As FilterGrid)
            oGridN.doAddFilterField("IDH_REQSTF", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_REQSTT", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_ACTSTF", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_ACTSTT", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)

            oGridN.doAddFilterField("IDH_CUST", "e.U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_NAME", "e.U_CardNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_PROD", "r.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ITMGRP", "r.U_ItmGrp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_JOBTYP", "r.U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ORDNO", "e.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_VEHREG", "r.U_Lorry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_DRIVER", "r.U_Driver", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            'oGridN.doAddFilterField("IDH_STATUS", "r.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Config.Parameter( "OSMSOS")) '"!Ordered")
            'oGridN.doAddFilterField("IDH_PSTAT", "r.U_PStat", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Config.Parameter( "OSMPOS")) '"!Ordered")
            'oGridN.doAddFilterField("IDH_PROG", "v.State", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Config.Parameter( "OSMPRO")) '"!Complete")
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            doAddTotalFields(oForm)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            doAddWF(oForm, "LASTJOBMENU", Nothing)

            oForm.Top = goParent.goApplication.Desktop.Height() - (oForm.Height + 120)
            oForm.Left = 200

            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New OrderRowGrid(Me, oForm, "LINESGRID", "r")
            End If

            'oGridN.setTableValue("[@IDH_JOBSHD] r, IDH_VJOBROWS v, [@IDH_JOBENTR] e, [@IDH_WTWT] t ")
            'oGridN.setKeyField("r.Code")

            oGridN.doAddGridTable(New GridTable("@IDH_JOBSHD", "r", "Code", True, True), True)
            oGridN.doAddGridTable(New GridTable("@IDH_JOBENTR", "e", "Code", True, True))
            oGridN.doAddGridTable(New GridTable("IDH_VJOBROWS", "v"))
            oGridN.doAddGridTable(New GridTable("@IDH_WTWT", "t"))

            oGridN.setOrderValue("t.U_WTWO, t.U_WTRO, t.U_WTSD, t.U_WTST")
            oGridN.setRequiredFilter("r.U_JobNr = e.Code And v.Code = r.Code And e.U_Status in ('1','2','3','4') And t.U_WTWO = r.U_JobNr And t.U_WTRO = r.Code")
            oGridN.setInitialFilterValue("r.U_JobNr = '-100'")

            oGridN.doSetDoCount(True)
            oGridN.doSetBlankLine(False)
            oGridN.doApplyExpand(IDHGrid.EXPANDED)
            doSetListFields(oGridN)
            oGridN.doFinalize()
        End Sub

        Protected Overrides Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean)
            MyBase.doReLoadData(oForm, bIsFirst)

            If Config.Parameter("OSMATO") = "TRUE" Then
                doGridTotals(oForm, False)
                setVisible(oForm, "IDH_CALC", False)
                'oForm.Items.Item("IDH_CALC").Visible = False
            Else
                doGridTotals(oForm, True)
                setVisible(oForm, "IDH_CALC", True)
                'oForm.Items.Item("IDH_CALC").Visible = True
            End If

        End Sub

'        Protected Overrides Sub doTotals(ByVal oGridN As UpdateGrid, Optional ByVal bDoZero As Boolean = False)
'            Dim dCstWgt As Double = 0
'            Dim dAftDisc As Double = 0
'            Dim dTotal As Double = 0
'            Dim dDisAmt As Double = 0
'            Dim dAddEx As Double = 0
'            Dim dTxAmt As Double = 0
'
'            If bDoZero = False Then
'                Dim iCount As Integer = oGridN.getRowCount()
'                Dim oData As SAPbouiCOM.DataTable = oGridN.getDataTable()
'                If iCount > 0 Then
'                    For iRow As Integer = 0 To iCount - 1
'                        dCstWgt = dCstWgt + oData.GetValue(oGridN.doIndexField("r.U_CstWgt"), iRow)
'                        dTotal = dTotal + oData.GetValue(oGridN.doIndexField("r.U_Total"), iRow)
'
'                        dAftDisc = dAftDisc + oData.GetValue(oGridN.doIndexField("SubBefDisc"), iRow)
'
'                        dDisAmt = dDisAmt + oData.GetValue(oGridN.doIndexField("r.U_DisAmt"), iRow)
'                        dAddEx = dAddEx + oData.GetValue(oGridN.doIndexField("r.U_AddEx"), iRow)
'                        dTxAmt = dTxAmt + oData.GetValue(oGridN.doIndexField("r.U_TaxAmt"), iRow)
'                    Next
'                End If
'            End If
'
'            setUFValue(oGridN.getSBOForm(), "IDH_CstWgt", dCstWgt)
'            setUFValue(oGridN.getSBOForm(), "IDH_SubAD", dAftDisc)
'            setUFValue(oGridN.getSBOForm(), "IDH_Total", dTotal)
'            setUFValue(oGridN.getSBOForm(), "IDH_DisAmt", dDisAmt)
'            setUFValue(oGridN.getSBOForm(), "IDH_AddEx", dAddEx)
'            setUFValue(oGridN.getSBOForm(), "IDH_TaxAmt", dTxAmt)
'        End Sub

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                If pVal.BeforeAction = True Then
                    Return False
                Else
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                If pVal.BeforeAction = False Then
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FORMAT_FIELDS Then
                If pVal.BeforeAction = False Then
                    'doSetColours(oForm)
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
                If pVal.BeforeAction = True Then
                End If
            End If
            Return True
        End Function

        'Protected Overrides Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Protected Overrides Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base)
            Dim oGridN As FilterGrid = pVal.oGrid 'FilterGrid.getInstance(oForm, "LINESGRID")
            If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                oGridN.setCurrentLineByClick(pVal.Row)

                Dim sRowCode As String = oGridN.doGetFieldValue("r.Code")
                Dim sJobEntr As String = oGridN.doGetFieldValue("r.U_JobNr")

                Dim oData As New ArrayList
                If sJobEntr.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("A Job Entry must be selected.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("A Job Entry must be selected.", "ERUSJOBS", {Nothing})
                Else
                    oData.Add("DoUpdate")
                    If oGridN.doCheckIsSameCol(pVal.ColUID, "e.U_ZpCd") Then
                        oData.Add("PC=" & oGridN.doGetFieldValue("e.U_ZpCd"))
                        goParent.doOpenModalForm("IDHMAP", oForm, oData)
                    ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "t.U_WTWO") OrElse oGridN.doCheckIsSameCol(pVal.ColUID, "r.U_JobNr") Then
                        oData.Add(sJobEntr)
                        goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
                    ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "t.Code") Then
                        Dim sScan As String = oGridN.doGetFieldValue("t.Code")
                        doClearSharedData(oForm)
                        setSharedData(oForm, "SCANCODE", sScan)
                        goParent.doOpenModalForm("IDH_WTWT", oForm, oData)
                    Else
                        If sRowCode.Length > 0 Then
                            oData.Add(sRowCode)
                            oData.Add(oGridN.doGetFieldValue("e.U_BDate"))
                            oData.Add(goParent.doTimeStrToStr(oGridN.doGetFieldValue("e.U_BTime")))
                            oData.Add(oGridN.doGetFieldValue("e.U_ZpCd"))
                            oData.Add(oGridN.doGetFieldValue("e.U_Address"))
                            oData.Add("CANNOTDOCOVERAGE")
                            oData.Add(oGridN.doGetFieldValue("e.U_SteId"))

                            'Added to handle the BP Switching
                            oData.Add(Nothing)
                            oData.Add(oGridN.doGetFieldValue("e.U_PAddress"))
                            oData.Add(oGridN.doGetFieldValue("e.U_PZpCd"))
                            oData.Add(oGridN.doGetFieldValue("e.U_PPhone1"))
                            oData.Add(oGridN.doGetFieldValue("e.U_FirstBP"))

                            goParent.doOpenModalForm("IDHJOBS", oForm, oData)
                        End If
                    End If
                End If
            End If
        End Sub

    End Class
End Namespace

