Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge
Imports com.idh.bridge.data

Namespace idh.forms.manager
    Public Class BPForecast
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHBPFORCST", "IDHPS", iMenuPosition, "BP Forecast.srf", False, True, False, "BP Forecast and Monitoring")
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                Dim oGridN As FilterGrid = New FilterGrid(Me, oForm, "LINESGRID", 7, 55, 785, 262, True)

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", Nothing)
            End Try
        End Sub

        Protected Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_ACTFRM", "bf.U_ActiveFrom", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_ACTTO", "bf.U_ActiveTo", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_CUST", "bf.U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_NAME", "bf.U_CardName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CUSTTP", "bf.U_CardType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_WASCD", "bf.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WASNM", "bf.U_ItemDsc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ESTQTY", "bf.U_EstQty", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_REMQTY", "bf.U_RemQty", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_COMQTY", "bf.U_ComQty", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_UOM", "bf.U_UOM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_BUYPRC", "bf.U_BuyingPrice", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SELPRC", "bf.U_SellingPrice", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_LNKWOH", "bf.U_LnkWOH", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_STATUS", "bf.U_MatStatus", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

        End Sub

        Protected Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("bf.Code", "Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("bf.U_MatStatus", "Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_DocType", "Doc Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_ItemCd", "Waste Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_ItemDsc", "Waste Description", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_EstQty", "Estimated Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_ComQty", "Committed Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_RemCQty", "Uncommitted Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_DoneQty", "Completed Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_RemQty", "Remaining Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_UOM", "UOM", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_ActiveFrom", "Active From", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_ActiveTo", "Active To", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_BuyingPrice", "Buying Price", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_SellingPrice", "Selling Price", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_CardCd", "Customer", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("bf.U_CardName", "Customer Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bp.Currency", "Customer Currecny", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_CardType", "Customer Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("bf.U_LnkWOH", "Linked WOH", False, -1, Nothing, Nothing)

        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "LINESGRID", True)
                End If
                'oGridN.setTableValue("CRD1 a, OCRD c")
                'oGridN.setKeyField("a.Address")
                oGridN.doAddGridTable(New GridTable("@IDH_BPFORECST", "bf", "Code", False, True), True)
                oGridN.doAddGridTable(New GridTable("OCRD", "bp", Nothing, False, True))

                oGridN.setRequiredFilter("bf.U_CardCd = bp.CardCode ")

                doSetFilterFields(oGridN)
                doSetListFields(oGridN)
                oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find")
                dofillStatusCombo(oForm)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing Pre Load.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBPL", Nothing)
            End Try
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                oGridN.doReloadData()
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error loading the Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLDBD", Nothing)
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find") Then
                doLoadData(oForm)
            ElseIf oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
                doUpdate(oForm)
            End If
            BubbleEvent = False
        End Sub

        Private Function doUpdate(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim bAllOk As Boolean = True
            Try
                'Dim oFields As ArrayList
                Dim iRow As Integer

                Dim sCardCode As String
                Dim sAddrName As String
                Dim iLineNumber As Integer
                Dim sRoute As String
                Dim iSeq As Integer
                'Dim iwResult As Integer
                'Dim swResult As String
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                Dim oList As ArrayList = oGridN.doGetChangedRows()
                Dim iIndex As Integer = 0

                While iIndex < oList.Count
                    iRow = oList.Item(iIndex)
                    oGridN.setCurrentDataRowIndex(iRow)
                    sCardCode = oGridN.doGetFieldValue("a.CardCode")
                    sAddrName = oGridN.doGetFieldValue("a.Address")
                    iLineNumber = oGridN.doGetFieldValue("a.LineNum")
                    sRoute = oGridN.doGetFieldValue("a.U_ROUTE")
                    iSeq = oGridN.doGetFieldValue("a.U_IDHSEQ")

                    Try
                        If Not (sCardCode Is Nothing) AndAlso sCardCode.Length > 0 Then
                            Dim sQry As String
                            sQry = "UPDATE [@IDH_BPFORECST] set U_ROUTE = '" & sRoute & "', U_IDHSEQ = " & iSeq & " WHERE CardCode = '" & sCardCode & "' AND LineNum = " & iLineNumber

                            If goParent.goDB.doUpdateQuery(sQry) = False Then
                                bAllOk = False
                            Else
                                oGridN.doRemoveChangedRowFlag(iRow)

                                If getUFValue(oForm, "IDH_URTS") = "Y" Then
                                    sQry = "UPDATE [@IDH_JOBENTR] " & _
                                           " SET U_Route = '" & sRoute & "', U_Seq = " & iSeq & _
                                           " WHERE U_CardCd = '" & sCardCode & "' " & _
                                           " AND U_Address = '" & IDHAddOns.idh.data.Base.doFixForSQL(sAddrName) & "' " & _
                                           " AND (U_Route is NULL OR U_Route = '') " & _
                                           " AND (U_Seq is NULL OR U_Seq=0) " & _
                                           " AND Code not in ( " & _
                                           "    SELECT U_JobNr FROM [@IDH_JOBSHD] WHERE NOT (U_AEDATE is NULL OR U_AEDATE = '') " & _
                                           " )"
                                    goParent.goDB.doUpdateQuery(sQry)
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing the update.")
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBUP", Nothing)
                        bAllOk = False
                    End Try

                    iIndex = iIndex + 1
                End While

                If bAllOk = True Then
                    oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find")
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error doing the update.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBUP", Nothing)
                Return False
            Finally
            End Try
        End Function


        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "LINESGRID" Then
                        Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                        Dim iCurRow As Integer = oGridN.getGrid().GetDataTableRowIndex(pVal.Row)
                        oGridN.setCurrentDataRowIndex(iCurRow)
                        If oGridN.doCheckIsSameCol(pVal.ColUID, "a.U_ROUTE") Then
                            Dim sValue As String = oGridN.doGetFieldValue("a.U_ROUTE")
                            If sValue.Length() = 0 Then
                                doChooseRoute(oForm, oGridN)
                            ElseIf pVal.ItemChanged Then
                                oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update")
                            End If
                        ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "a.U_IDHSEQ") Then
                            If pVal.ItemChanged Then
                                oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update")
                            End If
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "LINESGRID" Then
                    End If
                End If
            End If
            Return False
        End Function

        Protected Overridable Sub doChooseRoute(ByVal oForm As SAPbouiCOM.Form, ByVal oGridN As FilterGrid)
            goParent.doOpenModalForm("IDHROSRC", oForm)
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDHROSRC" Then
                    Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                    Dim sRoute As String = getSharedData(oForm, "IDH_RTCODE")
                    Dim iSeq As String = getSharedData(oForm, "IDH_RTSEQ")

                    oGridN.doSetFieldValue("a.U_ROUTE", sRoute)
                    oGridN.doSetFieldValue("a.U_IDHSEQ", iSeq)

                    doClearSharedData(oForm)
                    oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update")
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error handeling the modal result.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHMR", Nothing)
            End Try
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
        End Sub


        Public Overrides Sub doClose()
        End Sub
        Public Sub dofillStatusCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oForm.Items.Item("IDH_STATUS").Specific

            doClearValidValues(oCombo.ValidValues)

            Try
                oCombo.ValidValues.Add("", getTranslatedWord("All"))
                oCombo.ValidValues.Add("Y", getTranslatedWord("Active"))
                oCombo.ValidValues.Add("N", getTranslatedWord("Inactive"))
            Catch ex As Exception
            End Try
        End Sub

    End Class
End Namespace
