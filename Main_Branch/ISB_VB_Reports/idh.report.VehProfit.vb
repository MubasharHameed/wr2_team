Imports System.IO
Imports System.Collections

Namespace idh.report
    Public Class VehProfit
        Inherits IDHAddOns.idh.report.CrystalViewer
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHRVP", iMenuPosition, "Vehicle Profitability")
        End Sub

        Protected Overrides Function getReport(ByVal oForm As SAPbouiCOM.Form) As String
            Dim sReport As String
            'sReport = IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\reports\" & "\Vehicle Profitability Report v1.6.rpt"
			sReport = IDHAddOns.idh.addon.Base.REPORTPATH & "\Vehicle Profitability Report v1.6.rpt"
            Return sReport
        End Function

    End Class
End Namespace