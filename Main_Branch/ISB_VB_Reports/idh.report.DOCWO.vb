Imports System.IO
Imports System.Collections

Namespace idh.report
    Public Class DOCWO
        Inherits IDHAddOns.idh.report.CrystalViewer

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHDOCWO", iMenuPosition, "Duty of Care Report (WO)")
        End Sub

        Protected Overrides Function getReport(ByVal oForm As SAPbouiCOM.Form) As String
            Dim sReport As String
            'sReport = IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\reports\" & "\AW Duty of Care v1.3 WO.rpt"
			sReport = IDHAddOns.idh.addon.Base.REPORTPATH & "\AW Duty of Care v1.3 WO.rpt"
            Return sReport
        End Function

    End Class
End Namespace