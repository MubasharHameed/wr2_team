Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.controls
Imports com.idh.controls.strct
Imports System

Imports com.idh.utils
Imports com.idh.bridge

Imports com.idh.bridge.lookups

Namespace idh.report
    Public Class OnSite
    Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHONST", "IDHRE", iMenuPosition, "OnSite Report.srf", True, True, False, "Containers OnSite", load_Types.idh_LOAD_NORMAL )
        End Sub

'        '*** Returns the Form Title
'        Protected Overridable Function getTitle() As String
'            Return "Containers OnSite"
'        End Function
        
        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
        	oGridN.doAddListField("CustCd", "Customer Code", False, 100, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
        	oGridN.doAddListField("CardName", "Customer Name", False, 180, Nothing, Nothing)
        	oGridN.doAddListField("Address", "Site Address", False, 220, Nothing, Nothing )
        	oGridN.doAddListField("JobNr", "WO Number", False, 60, Nothing, Nothing)
        	oGridN.doAddListField("ItemGrp", "Container Group", False, 60, Nothing, Nothing)
            oGridN.doAddListField("ItemCd", "Container", False, 40, Nothing, Nothing)
            oGridN.doAddListField("WasteCode", "Waste Code", False, 60, Nothing, Nothing)
            oGridN.doAddListField("WasteDesc", "Waste Description", False, 100, Nothing, Nothing)
        	oGridN.doAddListField("OnSite", "OnSite", False, 40, Nothing, Nothing)
        End Sub
        
        Protected Overridable Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
        	oGridN.doAddFilterField("IDH_CUST", "CustCd", SAPbouiCOM.BoDataType.dt_LONG_TEXT, "AUTO", 1024, Nothing, True)
            oGridN.doAddFilterField("IDH_ADDRES", "Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100, Nothing, True)
            oGridN.doAddFilterField("IDH_WONR", "JobNr", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 20, Nothing, True)
            oGridN.doAddFilterField("IDH_CONT", "ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 20, Nothing, True)
        End Sub
        
'        '*** Create Sub-Menu
'        Protected Overrides Sub doCreateSubMenu()
'        	doCreateFormMenu("IDHRE", getTitle())
'        End Sub
        
        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
        End Sub
        
        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        	oForm.Freeze(True)
            Try
            	'oForm.Title = getTitle()
            	oForm.Title = gsTitle
            	
'                Dim oItem As SAPbouiCOM.Item
                Dim oGridN As FilterGrid = New FilterGrid(Me, oForm, "LINESGRID", 5, 26, 770, 245, False)

                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

                oForm.EnableMenu(Config.NAV_ADD, False)
                oForm.EnableMenu(Config.NAV_FIND, False)
                oForm.EnableMenu(Config.NAV_FIRST, False)
                oForm.EnableMenu(Config.NAV_NEXT, False)
                oForm.EnableMenu(Config.NAV_PREV, False)
                oForm.EnableMenu(Config.NAV_LAST, False)

                Dim fSettings As SAPbouiCOM.FormSettings
                fSettings = oForm.Settings
                fSettings.Enabled = False

                doSetAffectsMode(oForm, False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try
            oForm.Freeze(False)
        End Sub
        
        Private Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            'If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find")
                'oForm.Items.Item("2").Visible = False
            'ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
            '    oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update")
                'oForm.Items.Item("2").Visible = True
            'End If
        End Sub
        
                '*** Read input params
        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            'Dim sParentID As String = goParent.doGetParentID(oForm.UniqueID)
            If getHasSharedData(oForm) Then
                Dim oGridN As FilterGrid
                oGridN = FilterGrid.getInstance(oForm, "LINESGRID")
                Dim iIndex As Integer

                For iIndex = 0 To oGridN.getGridControl().getFilterFields().Count - 1
                    Dim oField As FilterField = oGridN.getGridControl().getFilterFields().Item(iIndex)
                    Dim sFieldName As String = oField.msFieldName
                    Dim sFieldValue As String = getParentSharedData(oForm, sFieldName)
                    Try
                        setUFValue(oForm, sFieldName, sFieldValue)
                    Catch ex As Exception
                    End Try
                Next

                '                Dim aList As ArrayList = oGridN.getFilterList()
                '                Dim iIndex As Integer
                '
                '                For iIndex = 0 To aList.Count - 1
                '                    Dim oField() As Object = aList.Item(iIndex)
                '                    Dim sFieldName As String = oField(0)
                '                    Dim sFieldValue As String = getParentSharedData(oForm, oField(0))
                '                    Try
                '                        setUFValue(oForm, sFieldName, sFieldValue)
                '                    Catch ex As Exception
                '                    End Try
                '                Next
            End If
        End Sub
        
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
        	doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New FilterGrid(Me, oForm, "LINESGRID", False)
            End If
            oGridN.doSetDoCount(True)
            oGridN.doSetBlankLine(False)
            doSetListFields(oGridN)
            doSetFilterFields(oGridN)
            doReadInputParams(oForm)
        End Sub
        
        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
'            Dim oButton As SAPbouiCOM.Button
            If pVal.BeforeAction = True Then
                If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find") Then
                    doReLoadData(oForm, True)
                End If
                BubbleEvent = False
            End If
        End Sub
        
        Protected Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean )
        	doLoadData(oForm)
        End Sub
        
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                Dim sCustomer As String = getUFValue(oForm, "IDH_CUST")
				Dim sRequiredFilter As String = " JobNr != 'Zz-Ignore' "
				Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")

            	If oGridN Is Nothing Then
                	oGridN = New FilterGrid(Me, oForm, "LINESGRID", False)
            	End If

            	'oGridN.setTableValue("IDH_VWOMOV")
                oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("IDH_VWOMOVT"), True)
            	
            	oGridN.setRequiredFilter(sRequiredFilter)
            	oGridN.doReloadData(True)
            Catch ex As Exception
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Protected Overridable Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                oGridN.setCurrentLineByClick(pVal.Row)

                Dim sJobEntr As String = oGridN.doGetFieldValue("JobNr")
                Dim oData As New ArrayList
                If sJobEntr.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("A Job Entry must be selected.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("A Job Entry must be selected.", "ERUSJOBS", {Nothing})
                Else
                    oData.Add("DoUpdate")
                    'If oGridN.doCheckIsSameCol(pVal.ColUID, "r.U_JobNr") Then
                        oData.Add(sJobEntr)
                        goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
                    'End If
                End If
            End If
        End Sub
        
        Private Sub doFilterAsYouType(ByVal oForm As SAPbouiCOM.Form, ByVal sItemId As String)
            Dim oGridN As FilterGrid
            oGridN = FilterGrid.getInstance(oForm, "LINESGRID")
            oGridN.doFilterAsYouType(sItemId)
        End Sub
        
        '** The ItemEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            'doSubItemEventBefore(oForm, pVal, BubbleEvent)
            If BubbleEvent = False Then
                setWFValue(oForm, "HasChanged", False)
                Return False
            End If
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = False Then
                    Dim oGridN As FilterGrid
                    oGridN = FilterGrid.getInstance(oForm, "LINESGRID")
                    If pVal.CharPressed = 13 Then
                        doLoadData(oForm)
                    Else
                        doFilterAsYouType(oForm, pVal.ItemUID)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = False Then
                        doGridDoubleClick(oForm, pVal, BubbleEvent)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_LOST_FOCUS Then
                If pVal.BeforeAction = False Then
                    If getWFValue(oForm, "HasChanged") = True Then
                        Dim oGridN As FilterGrid
                        oGridN = FilterGrid.getInstance(oForm, "LINESGRID")
                        If oGridN.getGridControl().getFilterFields().indexOfFilterField(pVal.ItemUID) > -1 Then
                            doLoadData(oForm)
                        End If
                        
'                        Dim aList As ArrayList = oGridN.getFilterList()
'                        Dim iIndex As Integer
'
'                        For iIndex = 0 To aList.Count - 1
'                            Dim oField() As Object = aList.Item(iIndex)
'                            If oField(0).Equals(pVal.ItemUID) Then
'                                doLoadData(oForm)
'                                Exit For
'                            End If
'                        Next
                    End If
                    setWFValue(oForm, "HasChanged", False)
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID.Equals("LINESGRID") = False Then
                        If pVal.ItemChanged Then
                            setWFValue(oForm, "HasChanged", True)
                        Else
                            setWFValue(oForm, "HasChanged", False)
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemChanged Then
                        Dim oGridN As FilterGrid
                        oGridN = FilterGrid.getInstance(oForm, "LINESGRID")
                        If oGridN.getGridControl().getFilterFields().indexOfFilterField(pVal.ItemUID) > -1 Then
                        	doLoadData(oForm)
                        End If
                        
'                        Dim aList As ArrayList = oGridN.getFilterList()
'                        Dim iIndex As Integer
'
'                        For iIndex = 0 To aList.Count - 1
'                            Dim oField() As Object = aList.Item(iIndex)
'                            If oField(0).Equals(pVal.ItemUID) Then
'                                doLoadData(oForm)
'                                Exit For
'                            End If
'                        Next
                    End If
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doClose()
        End Sub
    End Class
End Namespace
