Imports System.IO
Imports System.Collections

Namespace idh.report
    Public Class DynamicMenu

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            Dim sPath As String
            'sPath = IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\reports\dynamic\"
			sPath = IDHAddOns.idh.addon.Base.REPORTPATH & "\dynamic\"
			
            'Get filelist
            Dim saFiles As String() = Directory.GetFiles(sPath)
            For iFile As Integer = 0 To saFiles.Length() - 1
                Dim sFileName As String = saFiles(iFile)
                Dim sTitle As String = sFileName.Substring(0, 30) & ".."
                oParent.doAddForm(New IDHAddOns.idh.report.CrystalViewer(oParent, "Rep_" & iFile, "IDHRE", iFile, sTitle, sFileName))
            Next
        End Sub
    End Class
End Namespace