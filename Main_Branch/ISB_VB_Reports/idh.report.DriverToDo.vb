Imports System.IO
Imports System.Collections

Namespace idh.report
    Public Class DriverToDo
        Inherits IDHAddOns.idh.report.CrystalViewer

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHDTD", iMenuPosition, "Driver To Do List")
        End Sub

        Protected Overrides Function getReport(ByVal oForm As SAPbouiCOM.Form) As String
            Dim sReport As String
            'sReport = IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\reports\" & "\AW Driver To Do List v1.0.rpt"
			sReport = IDHAddOns.idh.addon.Base.REPORTPATH & "\AW Driver To Do List v1.0.rpt"
            Return sReport
        End Function

    End Class
End Namespace