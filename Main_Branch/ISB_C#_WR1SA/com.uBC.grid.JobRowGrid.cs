﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using com.idh.win.controls;
using com.idh.bridge.grid;
using com.idh.dbObjects.User;
using com.idh.controls;

using com.idh.dbObjects;
using com.idh.bridge;
using com.uBC.grid;
using com.idh.bridge.lookups;
using com.idh.win.controls.SBO;

namespace com.uBC.grid {
    public partial class JobRowGrid : WR1Grid {
        private JobRow.Controller moWORGridControl;
        private bool mbNewRowNeeded;

        private ContextMenu moPopUpMenu = new ContextMenu();

        public JobRowGrid()
            : base() {
            //doRegister(this.GetType().FullName);
            doInitialize();
        }

        public void doInitialize() {
            this.GrayReadOnly = true;

            ///*
            // * Setting the events
            // */
            //this.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridCellDoubleClick);
            //this.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridRowEnter);
            //this.RowsAdded += new DataGridViewRowsAddedEventHandler(this.GridRowAdded);
            //this.NewRowNeeded += new DataGridViewRowEventHandler(this.GridNewRowNeeded);
            //this.CellValidating += new DataGridViewCellValidatingEventHandler(this.GridCellValidating);
            //this.CellValueNeeded += new DataGridViewCellValueEventHandler(this.GridCellValueNeeded);
            //this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GridKeyPress);

            this.MouseUp += new MouseEventHandler(this.GridMouseUp);
        }

        public DBBase getDBObject() {
            return GridControl.DBObject;
        }

#region eventsection
        protected virtual void GridCellDoubleClick(object sender, DataGridViewCellEventArgs e) {
        }

        protected virtual void GridKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e) {
        }

        protected virtual void GridRowEnter(object sender, DataGridViewCellEventArgs e) {
        }

        private void GridNewRowNeeded(object sender, DataGridViewRowEventArgs e) {
            mbNewRowNeeded = true;
        }

        protected virtual void GridRowAdded(object sender, DataGridViewRowsAddedEventArgs e) {
            if (mbNewRowNeeded) {
                mbNewRowNeeded = false;
                GridControl.DBObject.doAddEmptyRow(true, true);
            }
        }

        private void MakeActive(object sender, EventArgs e) {
            object ss = sender;
        }

        private void GridMouseUp(object sender, MouseEventArgs e) {
            // Load context menu on right mouse click
            DataGridView.HitTestInfo hitTestInfo;
            if (e.Button == MouseButtons.Right) {
                hitTestInfo = this.HitTest(e.X, e.Y);
                if (hitTestInfo.Type == DataGridViewHitTestType.Cell) {
                    this.ClearSelection();
                    this.SetSelectedRowCore(hitTestInfo.RowIndex, true);
                    moPopUpMenu.Show(this, new System.Drawing.Point(e.X, e.Y));
                }
            }
        }

        private void GridCellValueNeeded(object sender, DataGridViewCellValueEventArgs e) {
            object oValue = e.Value;

            int iRow = e.RowIndex;
            int iCol = e.ColumnIndex;
        }

        protected override void OnCellValueChanged(DataGridViewCellEventArgs e) {
        }

        public void doLoadGrid(string sWR1Type, string sWhereClause, string sOrderBy) {
            if (moWORGridControl == null) {
                if (sWR1Type == "WO")
                    moWORGridControl = new JobRow.Controller(new IDH_JOBSHD(), this);
                else
                    moWORGridControl = new JobRow.Controller(new IDH_DISPROW(), this);
            } 

            int iPreviewLimit = Config.ParameterAsInt("PRELIMIT",10,true);

            getDBObject().getData(sWhereClause, sOrderBy, iPreviewLimit);
            doRefreshGridFromDBObject();
        }

        private void GridCellValidating(object sender, DataGridViewCellValidatingEventArgs e) {
            Rows[e.RowIndex].ErrorText = "";

            if (Rows[e.RowIndex].IsNewRow) { return; }
        }
#endregion

    }
}
