﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using com.uBC.utils;
using com.idh.bridge;
using com.idh.win.forms;
using com.idh.bridge.lookups;
using com.idh.dbObjects.User;

namespace com.uBC.toolControl {
    public partial class PreSelect : UserControl {
        private Control moLastChanged = null;
        private bool mbCombosLoaded = false;
        private int miPreviewLimit = 0;

        private com.uBC.sa.Main moTopForm;
        public com.uBC.sa.Main TopForm {
            get { return moTopForm; }
            set { moTopForm = value; }
        }

        public PreSelect() {
            InitializeComponent();

            //btnClose.Text = "Close";
            //btnOk.Text = "Ok";

            uBCWR1O.Text = "DO";
            uBCWR1O.Enabled = false;
        }

        [Browsable(true)]
        //public new event EventHandler CloseButtonClick {
        public event EventHandler CloseButtonClick {
            add { btnClose.Click += value; }
            remove { btnClose.Click -= value; }
        }

        [Browsable(true)]
        //public new event EventHandler OkButtonClick {
        public event EventHandler OkButtonClick {
            add { btnOk.Click += value; }
            remove { btnOk.Click -= value; }
        }

        [Browsable(true)]
        //public new event System.Windows.Forms.DataGridViewCellEventHandler PreviewGridCellDoubleClick {
        public event System.Windows.Forms.DataGridViewCellEventHandler PreviewGridCellDoubleClick {
            add { moWORGrid.CellDoubleClick += value; }
            remove { moWORGrid.CellDoubleClick -= value; }
        }

        /*
         * sField can bet the SQL column name or the set FieldID
         */
        public virtual object getReturnValueRAW(string sField) {
            return moWORGrid.getSelectedRowValue(sField);
        }

        private void moWORGrid_PreviewGridCellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            string sDOCode = (string)moWORGrid.getSelectedRowValue(IDH_DISPROW._JobNr);
            string sDORCode = (string)moWORGrid.getSelectedRowValue(IDH_DISPROW._Code);
            doOpenDO(sDOCode, sDORCode);
        }

        private void btnOk_Click(object sender, EventArgs e) {
            doOpenDO(null, null);
        }

        private void doOpenDO(string sDO, string sDOR) {
            com.uBC.wr1.form.MainForm oMainForm = new com.uBC.wr1.form.MainForm(
                ReleaseInfo.AppName,
                ReleaseInfo.ISV,
                "WR1 Standalone",
                ReleaseInfo.BaseVersion,
                ReleaseInfo.RelCount,
                ReleaseInfo.LicSupport,
                ReleaseInfo.DebugLevel,
                ReleaseInfo.ConfigTable,
                ReleaseInfo.DCode,
                false, TopForm.Message
            );
            oMainForm.MdiParent = TopForm;
           
            //oMainForm.Visible = false;
            //oMainForm.Show();

            if (oMainForm != null && oMainForm.Started) {
                oMainForm.doPostInitializeComponent();

                com.uBC.data.PreSelect oPreSelect = new com.uBC.data.PreSelect();
                oPreSelect.VehReg = uBCVehReg.Text;
                oPreSelect.WasteCode = uBCWasteCD.Text;
                oPreSelect.WasteDescription = uBCWasteNM.Text;
                oPreSelect.BPCode = uBCBPCD.Text;
                oPreSelect.BPName = uBCBPNM.Text;
                oPreSelect.TransactionCode = uBCTranCD.Text;
                oPreSelect.JobCode = sDO;
                oPreSelect.JobRowCode = sDOR;
                oMainForm.PreSelect = oPreSelect;
            }
            //oMainForm.Visible = true;
            oMainForm.Show();
        }

        private void uBCBPCF_Click(object sender, EventArgs e) {
            doSearchBP();
        }

        public string getDialogValue(SearchBase oSearch, string sField) {
            object oVal = oSearch.getReturnValue(sField);
            if (oVal != null) {
                if (oVal is string)
                    return (string)oVal;
                else
                    return oVal.ToString();
            } else
                return null;
        }

        private void uBCVehRgCF_Click(object sender, EventArgs e) {
            doSearchVehicleReg();
        }

        private void uBCWasteCF_Click(object sender, EventArgs e) {
            doSearchWasteMaterial();
        }

        private void PreSelect_VisibleChanged(object sender, EventArgs e) {
            //if (this.Visible) {
            //    FillCombos.WR1OrderTypeCombo(uBCWR1O);
            //    FillCombos.TransactionCodeCombo(uBCTranCD, null, uBCWR1O.Text);
            //}
        }

        public void doFillFirstCombos() {
            if ( !mbCombosLoaded ) {
                FillCombos.WR1OrderTypeCombo(uBCWR1O);
                 FillCombos.TransactionCodeCombo(uBCTranCD, uBCBPCD.Text, null, uBCWR1O.Text);
            }
        }

        private void sboClear_Click(object sender, EventArgs e) {
            uBCVehReg.Text = "";
            uBCWasteCD.Text = "";
            uBCWasteNM.Text = "";
            uBCBPCD.Text = "";
            uBCBPNM.Text = "";
            uBCTranCD.Text = "";
        }

        private void doSearchVehicleReg() {
            idh.wr1.form.search.Lorry2 oSearch = new idh.wr1.form.search.Lorry2();
            object oDialog = oSearch;

            oSearch.Text = "Vehicle Search";
            
            oSearch.doSetupControlNormalSearch();

            //oSearch.setFilterFieldValue("SILENT", "SHOWMULTI");
            oSearch.setFilterFieldValue("TRG", "LORRY");
            oSearch.setFilterFieldValue("IDH_VEHCUS", "");
            oSearch.setFilterFieldValue("IDH_VEHREG", uBCVehReg.Text);
            oSearch.setFilterFieldValue("IDH_VEHTP", "");
            oSearch.setFilterFieldValue("IDH_ITMGRP", "");
            oSearch.DoShowMulti = true;
            
            if (oSearch.ShowDialog(this) == DialogResult.OK) {
                uBCVehReg.Text = getDialogValue(oSearch, "REG");
            }
            doLoadPrevious();
        }

        private void doSearchWasteMaterial() {
            idh.wr1.form.search.WasteItem oSearch = new idh.wr1.form.search.WasteItem();
            object oDialog = oSearch;

            oSearch.Text = "Waste Item";

            string sTransactionCode = uBCTranCD.Text;
            if (sTransactionCode != null && sTransactionCode.Length > 0) {
                IDH_TRANCD oTransactionCode = new IDH_TRANCD();
                if (!oTransactionCode.getByKey(sTransactionCode)) {
                    DataHandler.INSTANCE.doUserError("Invalid Transaction Code. [" + sTransactionCode + "]");
                } else {
                    if (Config.INSTANCE.isIncomingJob(null, oTransactionCode.U_OrdType))
                        oSearch.setFilterFieldValue("IDH_PURWB", "Y");
                    else
                        oSearch.setFilterFieldValue("IDH_PURWB", "^N");
                }
            }
            
            //oSearch.setFilterFieldValue("SILENT", "SHOWMULTI");
            oSearch.setFilterFieldValue("TRG", "WASTECODE");
            oSearch.setFilterFieldValue("IDH_GRPCOD", "");
            oSearch.setFilterFieldValue("IDH_CRDCD", "");
            oSearch.setFilterFieldValue("IDH_INVENT", "");
            oSearch.setFilterFieldValue("IDH_ITMCOD", uBCWasteCD.Text);
            oSearch.setFilterFieldValue("IDH_ITMNAM", uBCWasteNM.Text);
            oSearch.DoShowMulti = true;
            if (oSearch.ShowDialog(this) == DialogResult.OK) {
                uBCWasteCD.Text = getDialogValue(oSearch, "ITEMCODE");
                uBCWasteNM.Text = getDialogValue(oSearch, "ITEMNAME");
            }
            doLoadPrevious();
        }

        private void doSearchBP() {
            idh.wr1.form.search.BP oSearch = new idh.wr1.form.search.BP(null);
            object oDialog = oSearch;

            oSearch.Text = "Customer Search";

            //oSearch.setFilterFieldValue("SILENT", "SHOWMULTI");
            oSearch.setFilterFieldValue("TRG", "CUST");
            oSearch.setFilterFieldValue("IDH_TYPE", "");
            oSearch.setFilterFieldValue("IDH_GROUP", "");
            oSearch.setFilterFieldValue("IDH_BRANCH", "");
            oSearch.setFilterFieldValue("IDH_BPCOD", uBCBPCD.Text);
            oSearch.setFilterFieldValue("IDH_NAME", uBCBPNM.Text);
            oSearch.DoShowMulti = true;
            if (oSearch.ShowDialog(this) == DialogResult.OK) {
                uBCBPCD.Text = getDialogValue(oSearch, "CARDCODE");
                uBCBPNM.Text = getDialogValue(oSearch, "CARDNAME");
            }
            FillCombos.TransactionCodeCombo(uBCTranCD, uBCBPCD.Text, null, uBCWR1O.Text);
            doLoadPrevious();
        }

        public void doLoadPrevious() {
            if (uBCShwPrev.Checked) {
                string sWR1 = uBCWR1O.Text;
                string sWhereClause = "";

                int iPreviewLimit = Config.ParameterAsInt("PRELIMIT");
                if (iPreviewLimit != miPreviewLimit) {
                    miPreviewLimit = iPreviewLimit;
                   uBCShwPrev.Text = "Preview Last " + iPreviewLimit + " Jobs";
                }
                //moGrid.setSoftLimit(iPreviewLimit);

                string sCardCode = uBCBPCD.Text;
                if (sCardCode.Length > 0) {
                    sWhereClause += (sWhereClause.Length > 0 ? " AND " : "") + "(" + IDH_JOBSHD._CustCd + " = '" + sCardCode + "' OR " + IDH_JOBSHD._ProCd + " = '" + sCardCode + "')";
                }

                string sWasteCode = uBCWasteCD.Text;
                if (sWasteCode.Length > 0) {
                    sWhereClause += (sWhereClause.Length > 0 ? " AND " : "") + IDH_JOBSHD._WasCd + " = '" + sWasteCode + "'";
                }

                string sVehReg = uBCVehReg.Text;
                if (sVehReg.Length > 0) {
                    sWhereClause += (sWhereClause.Length > 0 ? " AND " : "") + IDH_JOBSHD._Lorry + " = '" + sVehReg + "'";
                }

                string sTransCode = uBCTranCD.Text;
                if (sTransCode.Length > 0) {
                    sWhereClause += (sWhereClause.Length > 0 ? " AND " : "") + IDH_JOBSHD._TrnCode + " = '" + sTransCode + "'";
                }

                if (sWhereClause != null && sWhereClause.Length > 0) {
                    sWhereClause += (sWhereClause.Length > 0 ? " AND " : "") + IDH_JOBSHD._AEDate + " Is Not NULL AND " + IDH_JOBSHD._RowSta + " != '" + FixedValues.getStatusDeleted() + "'";
                    moWORGrid.doLoadGrid(uBCWR1O.Text, sWhereClause, "CAST(" + IDH_JOBSHD._Name + " AS Numeric)");
                    moWORGrid.Visible = true;
                }
            } else {
                moWORGrid.Visible = false;
            }
        }

        private void uBCVehReg_Validate(object sender, EventArgs e) {
            if ( moLastChanged == sender)
                doSearchVehicleReg();
            moLastChanged = null;
        }

        private void uBCWasteCD_Validate(object sender, EventArgs e) {
            if (moLastChanged == sender) {
                uBCWasteNM.Text = "";
                doSearchWasteMaterial();
            }
            moLastChanged = null;
        }

        private void uBCWasteNM_Validate(object sender, EventArgs e) {
            if (moLastChanged == sender) {
                uBCWasteCD.Text = "";
                doSearchWasteMaterial();
            }
            moLastChanged = null;
        }

        private void uBCBPCD_Validate(object sender, EventArgs e) {
            if (moLastChanged == sender) {
                uBCBPNM.Text = "";
                doSearchBP();
            }
            moLastChanged = null;
        }

        private void uBCBPNM_Validate(object sender, EventArgs e) {
            if (moLastChanged == sender) {
                uBCBPCD.Text = "";
                doSearchBP();
            }
            moLastChanged = null;
        }

        private void All_TextChanged(object sender, EventArgs e) {
            moLastChanged = (Control)sender;
        }

        private void uBCShwPrev_CheckedChanged(object sender, EventArgs e) {
            doLoadPrevious();
        }

        private void uBCTranCD_ValueChanged(object sender, idh.win.controls.LPVComboBox.ValueChangedEventArgs e) {
            doLoadPrevious();
        }
    }
}
