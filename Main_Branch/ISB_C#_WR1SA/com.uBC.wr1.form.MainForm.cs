﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2008/12/16
 * Time: 06:00 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Timers;

using com.idh.bridge;
using com.idh.bridge.form;
using com.idh.bridge.lookups;
using com.idh.bridge.reports;
using com.idh.bridge.data;

using com.idh.controls;
using com.idh.win;
using com.idh.win.controls;

using com.idh.bridge.utils;
using com.idh.dbObjects.User;
using com.idh.bridge.error;
using com.idh.wr1;
using com.idh.wr1.form;

using com.idh.win.controls.SBO;
using com.idh.dbObjects;
using com.uBC.wr1.form;

namespace com.uBC.wr1.form {
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public partial class MainForm : AppForm {
        protected DisposalOrder moDisposal;

        protected int miReaderFreq;
        protected bool mbEnableAuto;
        protected bool mbKeepBridgeOpen;
        protected string[] moAvailablePorts = null;

        protected string msBackColor = null;
        protected string msBranch;
        protected string msPrinterName;
        protected string msLocalReportDirectory;
        protected string msWeighBridge;
        protected string msDisposalSite;

        protected string msCommServerIP = null;
        protected int miCommServerPort = 0;
        
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        /// 
        //private bool mbDoBP = false;
        private System.ComponentModel.IContainer components = null;

        private string[] soIgnoreControls = { "IDH_ADDCO", "IDH_ADDCH" };

        protected override void doReadConfig() {
            base.doReadConfig();

            //idh.utils.XmlParameters oReader = new idh.utils.XmlParameters();
            //oReader.setFileInExecutionPath("Config.xml");
            msBranch = ConfigReader.getParameter("Branch");
            msPrinterName = ConfigReader.getParameter("PrinterName");
            msLocalReportDirectory = ConfigReader.getParameter("ReportFolder");
            msWeighBridge = ConfigReader.getParameter("WeighBridge");

            msBackColor = ConfigReader.getParameter("BackgroundColor");

            string sVal = ConfigReader.getParameter("AutoEnabled");
            if (sVal == null || sVal.Equals("FALSE", StringComparison.OrdinalIgnoreCase))
                mbEnableAuto = false;
            else
                mbEnableAuto = true;

            sVal = ConfigReader.getParameter("Frequency");
            if (sVal == null || sVal.Length == 0)
                miReaderFreq = 1000;
            else
                miReaderFreq = int.Parse(sVal);

            sVal = ConfigReader.getParameter("KeepWBOpen");
            if (sVal == null || sVal.Equals("FALSE", StringComparison.OrdinalIgnoreCase))
                mbKeepBridgeOpen = false;
            else
                mbKeepBridgeOpen = true;

            sVal = ConfigReader.getParameter("AvailPorts");
            if (sVal == null || sVal.Length == 0 )
                moAvailablePorts = null;
            else
                moAvailablePorts = sVal.Split(',');
            //doWriteLineToLog("AvailPorts: [" + sVal +']');

            sVal = ConfigReader.getParameter("DateFormat");
            if (sVal != null && sVal.Length > 4)
                Dates.DATEFORMAT = sVal;

            msDisposalSite = ConfigReader.getParameter("DisposalSite");

            msCommServerIP = ConfigReader.getParameter("CommServerIP");
            //doWriteLineToLog("CommServerIP: [" + msCommServerIP + ']');

            sVal = ConfigReader.getParameter("CommServerPort");
            if (sVal == null || sVal.Length == 0)
                miCommServerPort = 0;
            else
                miCommServerPort = int.Parse(sVal);
            //doWriteLineToLog("CommServerPort: [" + sVal + ']');

            int iDebugLevel = 0;
            sVal = ConfigReader.getParameter("DebugLevel");
            if (!string.IsNullOrWhiteSpace(sVal)){
                iDebugLevel = int.Parse(sVal);
                Config.DEBUGLEVEL = iDebugLevel;
            }

            int iGridLoadType = 1;
            sVal = ConfigReader.getParameter("GridLoadType");
            if (sVal == null || sVal.Length == 0)
                WR1Grid.LOADTYPE = WR1Grid.LOADTYPES.DEFAULT;
            else
                WR1Grid.LOADTYPE = WR1Grid.LOADTYPES.PREWORK;

            //Get overriding parameter values
            //Get the Config settings tag
            //Now find and replace these values from the config object
        }

        private void doUpdateDBStructure() {
            string sTableName;

            sTableName = "SA_IDH_USER";
            if (!DataHandler.INSTANCE.doMDTableExist(sTableName)) {
                if (DataHandler.INSTANCE.doMDCreateTable(sTableName, "User Table")) {
                    DataHandler.INSTANCE.doMDCreateField(sTableName, "Username", "Username", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, true, true);
                    DataHandler.INSTANCE.doMDCreateField(sTableName, "Password", "Password", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    DataHandler.INSTANCE.doMDCreateField(sTableName, "Role", "User Authorization", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                }
            }
        }
        
        public MainForm(string sAppName, string sISV, string sDesc, string sBaseVersion, double dRelCount, string sLicSupport, int iDebugLevel, string sConfigTable, string sDCode, bool bDoStepBackRCount, idh.bridge.resources.Messages oMessages)
            : base(sAppName, sISV, sDesc, sBaseVersion, dRelCount, sLicSupport, iDebugLevel, sConfigTable, sDCode, bDoStepBackRCount, oMessages) { //"242849"

            if ( Config.INSTANCE == null )
                new com.idh.bridge.lookups.Config("[@" + msConfigTable + "]");

            if (!Started)
                return;

            if ( DataHandler.INSTANCE.mfErrorHandler == null )
                DataHandler.INSTANCE.mfErrorHandler += ExternalErrorHandler;
            
            //if ( DataHandler.INSTANCE.mfInfoHandler == null )
            //    DataHandler.INSTANCE.mfInfoHandler = doWriteLineToLog;

            //if ( DataHandler.INSTANCE.mfDebugHandler == null )
            //    DataHandler.INSTANCE.mfDebugHandler = DebugHandler;

            moDisposal = new DisposalOrder(gsDatabaseName);
            if (gsUserName != null)
                moDisposal.doSetDefaultValue(IDH_DISPORD._User, gsUserName);

            if (msBranch != null)
                moDisposal.DisposalRows.doSetDefaultValue(IDH_DISPROW._Branch, msBranch);

            if (msDisposalSite != null && msDisposalSite.Length > 0) {
                moDisposal.doSetDefaultValue(IDH_DISPORD._SCardCd, msDisposalSite);
                moDisposal.DisposalRows.doSetDefaultValue(IDH_DISPROW._Tip, msDisposalSite);

                string sSiteName = Config.INSTANCE.doGetBPName(msDisposalSite);
                if (sSiteName != null && sSiteName.Length > 0) {
                    moDisposal.doSetDefaultValue(IDH_DISPORD._SCardNM, sSiteName);
                    moDisposal.DisposalRows.doSetDefaultValue(IDH_DISPROW._TipNm, sSiteName);
                }
            }
            
            moDisposal.TimerInterval = miReaderFreq;
            moDisposal.EnableTimer = mbEnableAuto;
            moDisposal.WeighBridge = msWeighBridge;
            moDisposal.DisposalRows.KeepWBOpen = mbKeepBridgeOpen;
            moDisposal.DisposalRows.AvailablePorts = moAvailablePorts;
            moDisposal.DisposalRows.CommServerIP = msCommServerIP;
            moDisposal.DisposalRows.CommServerPort = miCommServerPort;
            moDisposal.DatabaseServer = gsDatabaseServer;
            moDisposal.DatabaseUsername = gsDatabaseUsername;
            moDisposal.DatabasePassword = gsDatabasePassword;
            moDisposal.DatabaseName = gsDatabaseName;
            moDisposal.Branch = msBranch;
            moDisposal.PrinterName = msPrinterName;
            moDisposal.LocalReportDirectory = msLocalReportDirectory;
            moDisposal.WeighBridge = msWeighBridge;

            Form oForm = this;
            moDisposal.setWinForm(ref oForm);

            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            this.SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            DoubleBuffered = true;

            InitializeComponent();

            if (!Config.INSTANCE.getParameterAsBool("FLALTITM", true)) {
                IDH_ALTITM.Visible = false;
                label80.Visible = false;
            }
            
            Application.DoEvents();

            if ( msBackColor != null && msBackColor.Length > 0 ) {
                Color oColor = System.Drawing.ColorTranslator.FromHtml(msBackColor); //"#FFCC66";
                BackColor = oColor;

                this.oTPWeighing.BackColor = oColor;
                this.oTPCarrier.BackColor = oColor;
                this.oTPCustomer.BackColor = oColor;
                this.oTPProducer.BackColor = oColor;
                this.oTPSite.BackColor = oColor;
                this.oAdditional.BackColor = oColor;
                this.oDeductions.BackColor = oColor;

            }

            moItemInfoTextBox = ItemInfo;

            moDisposal.registerControls();

            if (!DataHandler.INSTANCE.IsConnected) {
                stl01.BackColor = Color.DarkRed;
                stl01.ForeColor = Color.LightYellow;
                stl01.Text = "Could not connect to Database: " + msConnectionString;
            } else {
                stl01.Text = "Connect to Database: " + gsDatabaseServer + " - " + gsDatabaseName; //sConnectionString ;
            }

            LblVersion.Text = " Version DB: " + msBaseDBVersion + " - " + msSAVersion;

            moDisposal.doLinkItems();

            moDisposal.Button1 = btn1;
            moDisposal.TimerButton = IDH_TMR;

            DataHandler.INSTANCE.mfErrorHandler = ExternalErrorHandler;

            oAddGrid.doInitialize(moDisposal, moDisposal.DisposalRows);
            oDeductionGrid.doInitialize(moDisposal, moDisposal.DisposalRows);

            oDORows.GridControl = new com.uBC.grid.JobRow.Controller(moDisposal.DisposalRows.DBObject);
            oDORows.doApplyControl();
            //oDORows.H
            oDORows.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridCellDoubleClick);
            
            //oDORows.GridControl.DBObject = moDisposal.DisposalRows.DBObject;
            moDisposal.DBObject.Handler_RecordLoaded = doHandle_DORecordLoad;
            moDisposal.DisposalRows.DBObject.Handler_RecordLoaded = doHandle_DORRecordLoad;
            moDisposal.DisposalRows.DBObject.Handler_FieldHasChanged = doHandler_DORFieldHasChanged;

        }

        protected virtual void GridCellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            int iRow = e.RowIndex;
            moDisposal.DisposalRows.DBObject.gotoRow(iRow);
        }

        //public static void SetDoubleBuffered(this Panel panel) {
        //    typeof(Panel).InvokeMember(
        //       "DoubleBuffered",
        //       System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.SetProperty,
        //       null,
        //       panel,
        //       new object[] { true });
        //}

        protected override CreateParams CreateParams {
            get {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                //cp.Style &= ~0x02000000;  // Turn off WS_CLIPCHILDREN
                return cp;
            }
        } 

        /**
         * Compile the field information text
         */
        protected override string getFieldInfo(string sFieldName) {
            string sDBFieldName = moDisposal.getDBFieldFromItemID(sFieldName);
            if (sDBFieldName == null) {
                sDBFieldName = moDisposal.DisposalRows.getDBFieldFromItemID(sFieldName);
            }
            if (sDBFieldName == null)
                return " Item: " + sFieldName;
            else
                return " Item: " + sFieldName + (sDBFieldName != null ? " - DB: " + sDBFieldName : "");
        }

        //public void DebugHandler(string sDescription, string sMessage) {
        //    doWriteLineToLog(sDescription + " - " + sMessage);
        //}

        public void ExternalErrorHandler(string sClass, string sFunction, string sError, string sDisplError, string sLevel) {
            try {
                if (!lblLastError.Visible)
                    lblLastError.Visible = true;

                if (sLevel == "User") {
                    stl01.BackColor = Color.OrangeRed;
                    stl01.ForeColor = Color.White;
                } else {
                    stl01.BackColor = Color.DarkRed;
                    stl01.ForeColor = Color.Yellow;
                }
                
                string sMessage = sDisplError;
                stl01.ToolTipText = sMessage;
                if (sMessage.Length > 70)
                    sMessage = sMessage.Substring(0, 70) + "....";
                stl01.Text = sMessage + "  ";

                mfTimerAction = doClearStatusBar;
                initializeTimer();

                //doWriteLineToLog("Error: " + sError);
            } catch (Exception) { }
        }

        private void doClearStatusBar() {
            try {
                stl01.Text = idh.bridge.lookups.Config.EMPTYSTR;
                stl01.BackColor = System.Drawing.SystemColors.ControlLight;
                stl01.ForeColor = System.Drawing.SystemColors.ControlDark;
                DoStopTimer();
            } catch (Exception) { }
        }

        public void ProgressHandler(float fRate) {
            prg1.Value = (int)(prg1.Maximum * fRate);
        }

        public void ProgressDone() {
            prg1.Value = 0;
        }

 #region TimerFunction
        private System.Timers.Timer moStatusbarTimer = null;
        private delegate void TimerAction();
        private com.uBC.wr1.form.MainForm.TimerAction mfTimerAction;
        public void DoStopTimer() {
            if (moStatusbarTimer != null) {
                moStatusbarTimer.Stop();
                moStatusbarTimer.Enabled = false;

                object oObject = moStatusbarTimer;
                DataHandler.INSTANCE.doReleaseObject(ref oObject);
            }
        }
        public void initializeTimer() {
            moStatusbarTimer = new System.Timers.Timer();
            moStatusbarTimer.Elapsed += new ElapsedEventHandler(oTimer_Elapsed);
            try {
                moStatusbarTimer.Interval = 10000;
                moStatusbarTimer.Enabled = true;
                moStatusbarTimer.Start();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            } finally {
                GC.Collect();
            }
        }

        private void oTimer_Elapsed(object oSender, ElapsedEventArgs eArgs) {
            try {
                moStatusbarTimer.Stop();
                mfTimerAction();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
                DoStopTimer();
            } finally {
                GC.Collect();
            }
        }
 #endregion

        private void IDH_BOOKIN_CheckedChanged(object sender, EventArgs e) {
            moDisposal.DisposalRows.doSetStockMovement();
        }

        private void IDH_USERE_CheckedChanged(object sender, EventArgs e) {
            RadioButton oRadio = (RadioButton)sender;
            if (oRadio.Checked)
                moDisposal.DisposalRows.doSetUseReadWeight();
        }

        private void IDH_USEAU_CheckedChanged(object sender, EventArgs e) {
            RadioButton oRadio = (RadioButton)sender;
            if (oRadio.Checked)
                moDisposal.DisposalRows.doSetUseAUOMWeight();
        }

        private void doCheckToGenConNum() {
            if (IDH_AVDOC.Checked) {
                string sWasteCode = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._WasCd);
                string sConNum = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._ConNum);

                double dWei1 = moDisposal.DisposalRows.getFormDFValueAsDouble(IDH_DISPROW._Wei1);
                double dWei2 = moDisposal.DisposalRows.getFormDFValueAsDouble(IDH_DISPROW._Wei2);
                double dRdWgt = moDisposal.DisposalRows.getFormDFValueAsDouble(IDH_DISPROW._RdWgt);
                double dAQt = moDisposal.DisposalRows.getFormDFValueAsDouble(IDH_DISPROW._AUOMQt);
                if (((dWei1 > 0 && dWei2 > 0) || dRdWgt != 0 || dAQt != 0) &&
                    sWasteCode != null && sWasteCode.Length > 0 &&
                    (sConNum == null || sConNum.Length == 0)) {
                    if (Config.INSTANCE.isHazardousItem(sWasteCode)) {
                        string sCardCode = moDisposal.getFormDFValueAsString(IDH_DISPORD._CardCd);
                        string sCardName = moDisposal.getFormDFValueAsString(IDH_DISPORD._PCardNM);
                        string sAddress = moDisposal.getFormDFValueAsString(IDH_DISPORD._Address);
                        sConNum = idh.bridge.utils.General.doCreateConNumber(sCardCode, sCardName, sAddress);

                        moDisposal.DisposalRows.setFormDFValue(IDH_DISPROW._ConNum, sConNum);
                    }
                }
            }
        }

#region Navigation
        void BtFindClick(object sender, EventArgs e) {
            try {
                moDisposal.DBObject.BlockChangeNotif = true;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = true;

                moDisposal.Mode = FormBridge.Mode_FIND;
                btn1.Text = FormBridge.Mode_FIND;
                doLoadDOSupportData();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            } finally {
                moDisposal.DBObject.BlockChangeNotif = false;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = false;
            }
        }

        void BtAddClick(object sender, EventArgs e) {
            try {
                moDisposal.DBObject.BlockChangeNotif = true;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = true;

                moDisposal.Mode = FormBridge.Mode_ADD;
                doLoadDOSupportData();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            } finally {
                moDisposal.DBObject.BlockChangeNotif = false;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = false;
            }
        }

        void BtFirstClick(object sender, EventArgs e) {
            try {
                moDisposal.DBObject.BlockChangeNotif = true;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = true;

                if (moDisposal.getByKeyFirst()) {
                    //doLoadSupportData();
                }
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            } finally {
                moDisposal.DBObject.BlockChangeNotif = false;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = false;
            }
        }

        void BtPreviousClick(object sender, EventArgs e) {
            try {
                moDisposal.DBObject.BlockChangeNotif = true;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = true;

                if (moDisposal.getByKeyPrevious()) {
                    //doLoadSupportData();
                }
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            } finally {
                moDisposal.DBObject.BlockChangeNotif = false;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = false;
            }
        }

        void BtNextClick(object sender, EventArgs e) {
            try {
                moDisposal.DBObject.BlockChangeNotif = true;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = true;

                if (moDisposal.getByKeyNext()) {
                    //doLoadSupportData();
                }
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            } finally {
                moDisposal.DBObject.BlockChangeNotif = false;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = false;
            }
        }

        void BtLastClick(object sender, EventArgs e) {
            try {
                moDisposal.DBObject.BlockChangeNotif = true;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = true;

                if (moDisposal.getByKeyLast()) {
                    //doLoadSupportData();
                }
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            } finally {
                moDisposal.DBObject.BlockChangeNotif = false;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = false;
            }
        }
#endregion
        /**
         * Handler when a ROW field Value has changed, gives us the opertunity to update the grid
         */
        public void doHandler_DORFieldHasChanged(DBBase oCaller, object oFieldName) {
            string sFieldName = (string)oFieldName;
            if (oDORows.GridControl.ListFlds.ContainsFieldName(sFieldName)) {
                oDORows.doRefreshGridFromDBObject();
            }
        }

        /**
         * Handler To Handle the Order load 
         */
        public void doHandle_DORecordLoad(DBBase oCaller) {
            //oDORows.GridControl.DBObject = moDisposal.DisposalRows.DBObject;
            //oDORows.doRefreshGridFromDBObject();

            doLoadDOSupportData();
        }

        public void doHandle_DORRecordLoad(DBBase oCaller) {
            //oDORows.GridControl.DBObject = moDisposal.DisposalRows.DBObject;
            //oDORows.doRefreshGridFromDBObject();

            doLoadDORSupportData();
        }

        void doLoadDOSupportData() {
            //oDORows.GridControl.DBObject = moDisposal.DisposalRows.DBObject;
            oDORows.doRefreshGridFromDBObject();
            doLoadDORSupportData();
        }

        void doLoadDORSupportData() {
            doLoadAdditionalExpenses();
            doLoadDeductions();

            moDisposal.DisposalRows.doUpdateTareFields();
            //IDH_TARWEI.Text = "0.00";
            //IDH_TRLTar.Text = "0.00";
        }

        void doReloadData() {
            try {
                moDisposal.DBObject.BlockChangeNotif = true;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = true;

                if (moDisposal.doReLoadData() > 0) {
                    //doLoadSupportData();
                }
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            } finally {
                moDisposal.DBObject.BlockChangeNotif = false;
                moDisposal.DisposalRows.DBObject.BlockChangeNotif = false;
            }
        }

        private void doLoadAdditionalExpenses() {
            oAddGrid.doRefreshGridFromDBObject();
            moDisposal.DisposalRows.setUFValue("IDH_ADDCH", ((IDH_DOADDEXP)oAddGrid.GridControl.DBObject).ChargeTotal.ToString());
            moDisposal.DisposalRows.setUFValue("IDH_ADDCO", ((IDH_DOADDEXP)oAddGrid.GridControl.DBObject).CostTotal.ToString());
        }

        private void doLoadDeductions() {
            oDeductionGrid.doRefreshGridFromDBObject();
            moDisposal.DisposalRows.setUFValue("IDH_DEDWEI", ((IDH_DODEDUCT)oDeductionGrid.GridControl.DBObject).WeightTotal.ToString());
            moDisposal.DisposalRows.setUFValue("IDH_DEDVAL", ((IDH_DODEDUCT)oDeductionGrid.GridControl.DBObject).ValueTotal.ToString());
        }

        //private System.ComponentModel.BackgroundWorker moBackgroundWorker;

        //public override void doPostInitializeComponent() {
        //    this.moBackgroundWorker = new System.ComponentModel.BackgroundWorker();
        //    this.moBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackgroundWorker_DoWork);
        //    moBackgroundWorker.RunWorkerAsync(2000);
        //}
        //private void BackgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e) {
        //    doPostInitializeComponentAct();
        //}

        //public void doPostInitializeComponentAct() {
        //    Invoke((MethodInvoker)delegate {
        //        try {
        //            base.doPostInitializeComponent();

        //            moDisposal.doInitialize();
        //            moDisposal.doSetInitialFormState();
        //        } catch (Exception ex) {
        //            DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
        //        }
        //    });
        //}

        public override void doPostInitializeComponent() {
            //Invoke((MethodInvoker)delegate {
            try {
                base.doPostInitializeComponent();

                moDisposal.doInitialize();
                moDisposal.doSetInitialFormState();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
            ///});
        }

        void MainFormLoad(object sender, EventArgs e) {
            try {
                moDisposal.doInitialize();
                moDisposal.doSetInitialFormState();
                moDisposal.doSetVisible("IDH_WOCFL", false, 0);
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void MainFormFormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e) {
            try {
                moDisposal.doClose();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        private void doShowErrorWindow() {
            DataHandler oData = DataHandler.INSTANCE;
            ErrorBuffer oErrs = oData.ErrorBuffer;
            if (oErrs != null && oErrs.Buffer.Count > 0) {
                InfoForm oInfo = new InfoForm(oErrs);
                oInfo.Show();
            }
        }

        void ToolStripStatusLabel1Click(object sender, EventArgs e) {
            doShowErrorWindow();
        }

        void StatusStrip1ItemClicked(object sender, ToolStripItemClickedEventArgs e) {
            doShowErrorWindow();
        }

#region ClickActions
        void Btn1Click(object sender, EventArgs e) {
            try {
                string sCaption = btn1.Text;
                if (moDisposal.Mode == FormBridge.Mode_FIND) {
                    string sCode = IDH_BOOREF.Text;
                    if (sCode.Length > 0) {
                        if (moDisposal.getByKey(sCode)) {
                            moDisposal.doUpdateFormData();
                            moDisposal.Mode = FormBridge.Mode_VIEW;
                        }
                    }
                } else if (moDisposal.Mode == FormBridge.Mode_UPDATE) {
                    //doUpdateDB();

                    doCheckToGenConNum();

                    if (moDisposal.doUpDateDataRow()) {

                        if (IDH_AVCONV.Checked)
                            moDisposal.doConvReport();
                        if (IDH_AVDOC.Checked)
                            moDisposal.doDocReport();

                        string sDONr = moDisposal.getFormDFValueAsString(IDH_DISPORD._Code);
                        moDisposal.getByKey(sDONr);
                        moDisposal.Mode = FormBridge.Mode_OK;

                        //doLoadSupportData();

                       // IDH_VEHREG.Focus();
                    }
                } else if (moDisposal.Mode == FormBridge.Mode_ADD) {
                    //doUpdateDB();

                    doCheckToGenConNum();

                    if (moDisposal.doAddDataRow()) {
                        moDisposal.doUpdateFormData();

                        if (IDH_AVCONV.Checked)
                            moDisposal.doConvReport();
                        if (IDH_AVDOC.Checked)
                            moDisposal.doDocReport();

                        moDisposal.Mode = FormBridge.Mode_FIND;

                        doLoadDOSupportData();
                        //IDH_VEHREG.Focus();
                    }

                } else if (moDisposal.Mode == FormBridge.Mode_OK || moDisposal.Mode == FormBridge.Mode_VIEW) {
                    moDisposal.Mode = FormBridge.Mode_FIND;

                    doLoadDOSupportData();
                }
                //doLoadSupportData();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error doing the action.");
            }
        }

        private void IDH_CONGEN_Click(object sender, EventArgs e) {
            try {
                moDisposal.doGenerateConsignmentNo();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void IDH_REFClick(object sender, EventArgs e) {
            try {
                moDisposal.doReadWeight("CR");
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void IDH_ACCBClick(object sender, EventArgs e) {
            try {
                moDisposal.doReadWeight("ACCB");
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void IDH_ACC1Click(object sender, EventArgs e) {
            try {
                moDisposal.doReadWeight("ACC1");
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void IDH_ACC2Click(object sender, EventArgs e) {
            try {
                moDisposal.doReadWeight("ACC2");
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void IDH_TMRClick(object sender, EventArgs e) {
            try {
                //moDisposal.getRows().doReadWeight("CR");
                if (!moDisposal.TimerStarted) {
                    moDisposal.DoStartTimer();
                    IDH_TMR.Text = "Stop Reading";
                } else {
                    moDisposal.DoStopTimer();
                    IDH_TMR.Text = "Start Reading";
                }
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void IDH_Tare1Click(object sender, EventArgs e) {
            double dTar1 = Convert.ToDouble(IDH_TARWEI.Text);
            double dTar2 = Convert.ToDouble(IDH_TRLTar.Text);

            moDisposal.DisposalRows.doUseTareWei1(dTar1, dTar2);
        }

        void IDH_Tare2Click(object sender, EventArgs e) {
            double dTar1 = Convert.ToDouble(IDH_TARWEI.Text);
            double dTar2 = Convert.ToDouble(IDH_TRLTar.Text);

            moDisposal.DisposalRows.doUseTareWei2(dTar1, dTar2);
        }

        private void IDH_WRROW_Click(object sender, EventArgs e) {
            try {
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        private void IDH_DOC_Click(object sender, EventArgs e) {
            moDisposal.doDocReport();
        }

        private void IDH_CON_Click(object sender, EventArgs e) {
            moDisposal.doConvReport();
        }


        private void btnHelp_Click(object sender, EventArgs e) {
            WR1Help oHelp = new WR1Help();
            string sFile = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\help.html"; ;
            oHelp.setHelpFile(sFile);
            oHelp.Show();
        }

        void Stl01Click(object sender, EventArgs e) {
            doShowErrorWindow();
        }
#endregion

#region ExternalProperies
        private com.uBC.data.PreSelect moPreSelect;
        public com.uBC.data.PreSelect PreSelect {
            set {
                moDisposal.Mode = DisposalOrder.Mode_ADD;
                moPreSelect = value;
                if (moPreSelect.JobRowCode == null || moPreSelect.JobRowCode.Length == 0) {
                    //moDisposal.Mode = DisposalOrder.Mode_ADD;

                    //IDH_VEHREG.Text = moPreSelect.VehReg;
                    //IDH_WASCL1.Text = moPreSelect.WasteCode;
                    //IDH_WASMAT.Text = moPreSelect.WasteDescription;

                    moDisposal.DisposalRows.DBObject.U_Lorry = moPreSelect.VehReg;
                    moDisposal.DisposalRows.DBObject.U_WasCd = moPreSelect.WasteCode;
                    moDisposal.DisposalRows.DBObject.U_WasDsc = moPreSelect.WasteDescription;

                    //uBC_TRNCd.Text = moPreSelect.TransactionCode;
                    moDisposal.DBObject.U_TRNCd = moPreSelect.TransactionCode;
                    moDisposal.DisposalRows.DBObject.U_TrnCode = moPreSelect.TransactionCode;

                    if (moPreSelect.BPCode.Length > 0) {
                        BP oBP = new BP();
                        if (oBP.doGetBPInfo(moPreSelect.BPCode, true) > 0) {
                            if (oBP.CardType == "C") {
                                //IDH_CUST.Text = moPreSelect.BPCode;
                                //IDH_CUSTNM.Text = moPreSelect.BPName;

                                moDisposal.doSetChoosenCustomer(ref oBP, true);
                            } else if (oBP.CardType == "S") {
                                //IDH_WPRODU.Text = moPreSelect.BPCode;
                                //IDH_WNAM.Text = moPreSelect.BPName;

                                moDisposal.doSetChoosenProducer(ref oBP, true, true);
                            }
                        }
                    }

                    //if (moPreSelect.VehReg.Length > 0) {
                    //    doVehicleSearch(false, true, false);
                    //    //doLookupBP("Customer Search", DisposalOrder.BP_CUSTOMER, null, true, false);
                    //}
                } else {
                    moDisposal.Mode = DisposalOrder.Mode_UPDATE;
                    //doFindRow();

                    string sCode = moPreSelect.JobCode;
                    string sRowCode = moPreSelect.JobRowCode;
                    if (sCode.Length > 0) {
                        if (moDisposal.getGetByHeadAndRow(sCode,sRowCode)) {
                            moDisposal.doUpdateFormData();
                            moDisposal.Mode = FormBridge.Mode_VIEW;
                        }
                    }
                }
                moDisposal.doUpdateFormData();
            }
            get { return moPreSelect; }
        }
#endregion

#region UserFormFieldUpdates
        /**
         * Use this to validate the field value... return false if it fails
         */
        public override bool doValidate(Control oControl, int iChanged) {
            if (iChanged == CHANGED_NO)
                return true;

            if (oControl == IDH_VEHREG)
                return doRegCheck();
                //return doVehicleSearch(false, true, false);
            else if (oControl == IDH_CUST ||
                  oControl == IDH_CUSTNM ||
                  oControl == IDH_CARRIE ||
                  oControl == IDH_CARNAM ||
                  oControl == IDH_WPRODU ||
                  oControl == IDH_WNAM ||
                  oControl == IDH_DISSIT ||
                  oControl == IDH_DISNAM ||
                  oControl == IDH_PROCD ||
                  oControl == IDH_PRONM ||
                  oControl == IDH_DISPCD ||
                  oControl == IDH_DISPNM)
                //return doBPCheck(oControl);
                return doBPSearch(oControl, true, false);
            else if (oControl == IDH_WRORD ||
                    oControl == IDH_WRROW)
                return doCheckLinkWO(oControl);
                //return doLinkWOSearch(true, false);
            else if (oControl == IDH_ITMCOD ||
                       oControl == IDH_DESC)
                return doCheckContainer(oControl);
                //return doContainerSearch(true, false);
            else if (oControl == IDH_WASCL1 ||
                      oControl == IDH_WASMAT)
                return doCheckWaste(oControl);
                //return doWasteSearch(true, false);
            else
                return moDisposal.doValidateUserInput(oControl.Name, com.idh.win.controls.utils.General.getFormFieldValue(oControl));
        }

        private bool doRegCheck() {
            string sCurrentValue = moDisposal.DisposalRows.DBObject.U_Lorry;
            string sItemValue = IDH_VEHREG.Text;

            if (sItemValue != sCurrentValue) {
                if (LastMouseOverControl != null && LastMouseOverControl == IDH_OS) {
                    if (!doVehicleSearch(true, false, false)) {
                        moDisposal.doReactivateLastControl();
                        return false;
                    }
                } else {
                    if (!doVehicleSearch(false, false, false)) {
                        moDisposal.doReactivateLastControl();
                        return false;
                    }
                }
            }
            return true;
        }

        private bool doCheckLinkWO(Control oControl) {
            string sCurrentValue = "";
            string sItemValue = oControl.Text;

            if (oControl == IDH_WRORD)
                sCurrentValue = moDisposal.DisposalRows.DBObject.U_WROrd;
            else if (oControl == IDH_WRROW)
                sCurrentValue = moDisposal.DisposalRows.DBObject.U_WRRow;

            if (sItemValue != sCurrentValue) {
                return doLinkWOSearch(true, false,false);
            }
            return true;
        }

        private bool doCheckContainer(Control oControl) {
            string sCurrentValue = "";
            string sItemValue = oControl.Text;

            if (oControl == IDH_ITMCOD)
                sCurrentValue = moDisposal.DisposalRows.DBObject.U_ItemCd;
            else if (oControl == IDH_DESC)
                sCurrentValue = moDisposal.DisposalRows.DBObject.U_ItemDsc;

            if (sItemValue != sCurrentValue) {
                return doContainerSearch(true, false);
            }
            return true;
        }

        private bool doCheckWaste(Control oControl) {
            string sCurrentValue = "";
            string sItemValue = oControl.Text;

            if (oControl == IDH_WASCL1)
                sCurrentValue = moDisposal.DisposalRows.DBObject.U_WasCd;
            else if (oControl == IDH_WASMAT)
                sCurrentValue = moDisposal.DisposalRows.DBObject.U_WasDsc;

            if (sItemValue != sCurrentValue) {
                if (!Config.INSTANCE.getParameterAsBool("DOCWD", true) && oControl == IDH_WASMAT) {
                    sItemValue = sItemValue.Trim();
                    if (sItemValue.Length == 0 ||
                        (!sItemValue.StartsWith("*") &&
                         !sItemValue.EndsWith("*"))) {
                        moDisposal.DisposalRows.setFormDFValueFromUser(IDH_DISPROW._WasDsc, sItemValue);
                        return true;
                    }
                }
                if (!doWasteSearch(true, false)) {
                    if (oControl == IDH_WASCL1)
                        IDH_WASCL1.Text = sItemValue;
                    else if (oControl == IDH_WASMAT)
                        IDH_WASMAT.Text = sItemValue;
                    return false;
                }
            }
            return true;
        }

        /**
          * Use this where the Business Object needs to be updated from the User changes
          */
        public override void doSetFieldHasChanged(Control oControl) {
            try {
                if (moDisposal.Mode == FormBridge.Mode_VIEW ||
                    moDisposal.Mode == FormBridge.Mode_OK) {
                    moDisposal.Mode = FormBridge.Mode_UPDATE; //doSetToUpdateMode();
                    btn1.Text = FormBridge.Mode_UPDATE;
                }
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }

            if (oControl == IDH_ORIGIN ||
                oControl == IDH_ORIGIN2 ||
                oControl == IDH_RORIGI)
                doOriginSearch(oControl.Name);
            //else if (oControl == IDH_JOBTTP) {
            //    moDisposal.DisposalRows.doJobTypeChange();
            //} 
            else if ( oControl is SBOComboBox && 
                ( 
                    oControl == IDH_TIPCOS ||
                    oControl == IDH_ORDCOS ||
                    oControl == IDH_PRCOST ||
                    oControl == IDH_CUSCHG
                )) {
                    moDisposal.doUpdateDBFieldFromUser(oControl);
                moDisposal.doUpdateFormChangedData();
            } else if (oControl == uBC_TRNCd) {
                moDisposal.doUpdateDBFieldFromUser(oControl);
                moDisposal.doUpdateFormChangedData();
                moDisposal.doUpdateFormData();

                if (!string.IsNullOrWhiteSpace(moDisposal.DisposalRows.DBObject.U_TrnCode)) {
                    moDisposal.doSwitchTransactionCodeFields();
                }
            } else {
                //moDisposal.setWasSetFromUser(oControl.Name);
                moDisposal.doUpdateDBFieldFromUser(oControl);
                //moDisposal.doHandleFieldChange(oControl.Name);

                moDisposal.doUpdateFormChangedData();
            }
        }

        /**
          * Identify the Conrols that should not trigger validations
          */
        protected override void setIgnoreValidationControls() {
            oPNavigation.CausesValidation = false;
            btFind.CausesValidation = false;
            btAdd.CausesValidation = false;
            btFirst.CausesValidation = false;
            btPrevious.CausesValidation = false;
            btNext.CausesValidation = false;
            btLast.CausesValidation = false;

            //IDH_AVCONV.CausesValidation = false;
            //IDH_AVDOC.CausesValidation = false;
        }

        //void All_ComboChanged(object sender, EventArgs e) {
        //    try {
        //        string sValue = moDisposal.getFormFieldValue((Control)sender);
        //        if (!moDisposal.doUpdateDBFieldValueUsingFormFieldId(((Control)sender).Name, sValue)) {
        //            moDisposal.DisposalRows.doUpdateDBFieldValueUsingFormFieldId(((Control)sender).Name, sValue);
        //        }

        //        if (((Control)sender).Name.Equals("IDH_JOBTTP")) {
        //            if (mbDoBP) {
        //                moDisposal.DisposalRows.doJobTypeChange();
        //                mbDoBP = false;
        //            }
        //        } else {
        //            //moDisposal.DisposalRows.doHandleFieldChange(((Control)sender).Name);
        //        }

        //        if (moDisposal.Mode == FormBridge.Mode_VIEW ||
        //            moDisposal.Mode == FormBridge.Mode_OK) {
        //            moDisposal.Mode = FormBridge.Mode_UPDATE;
        //            btn1.Text = FormBridge.Mode_UPDATE;
        //        }
        //    } catch (Exception ex) {
        //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
        //    }
        //}
#endregion

#region SearchClick
        void IDH_OSClick(object sender, EventArgs e) {
            try {
                if (!doVehicleSearch(true, false, false))
                    moDisposal.doReactivateLastControl();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void IDH_REGLButton1Click(object sender, EventArgs e) {
            try {
                if (!doVehicleSearch(false, false, false))
                    moDisposal.doReactivateLastControl();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void LinkWOClick(object sender, EventArgs e) {
            try {
                if (!doLinkWOSearch(false, false, false))
                    moDisposal.doReactivateLastControl();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void LinkWOLClick(object sender, EventArgs e) {
            try {
                if (!doLinkWOSearch(false, false, true))
                    moDisposal.doReactivateLastControl();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void BPSearchClick(object sender, EventArgs e) {
            try {
                if (!doBPSearch((Control)sender, false, false))
                    moDisposal.doReactivateLastControl();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void AddressSearchClick(object sender, EventArgs e) {
            try {
                if (!doAddressSearch((Control)sender, false))
                    moDisposal.doReactivateLastControl();
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            }
        }

        void IDH_WASCFClick(object sender, EventArgs e) {
            if (!doWasteSearch(false, false))
                moDisposal.doReactivateLastControl();
        }

        void IDH_ITCFClick(object sender, EventArgs e) {
            if (!doContainerSearch(false, false))
                moDisposal.doReactivateLastControl();
        }

        void IDH_ORLKClick(object sender, EventArgs e) {
            if (!doOriginSearch(((Control)sender).Name))
                moDisposal.doReactivateLastControl();
        }

#endregion

#region SearchesForms
        public bool doVehicleSearch(bool bOnsite, bool bFromFocus, bool bIgnoreData) {
            if (!Config.ParameterAsBool("VMUSENEW", false)) 
                return doVehicleSearch1(bOnsite, bFromFocus, bIgnoreData);
            else
                return doVehicleSearch2(bOnsite, bFromFocus, bIgnoreData);
        }

        private bool doVehicleSearch1(bool bOnsite, bool bFromFocus, bool bIgnoreData) {
            idh.wr1.form.search.Lorry oSearch = new idh.wr1.form.search.Lorry();

            oSearch.Target = "LORRY";
            moDisposal.ISSecondWeigh = false;
            if (bOnsite)
                oSearch.doSetupControlOnsiteSearch();
            else
                oSearch.doSetupControlNormalSearch();

            //		   oSearch.Bridge.getWinControl("btCreate").Visible = true;		    
            object oDialog = oSearch;
            try {
                if (moDisposal.doSetVehicleSearchParams(ref oDialog, "LORRY", true, bFromFocus, bIgnoreData)) {
                    DialogResult oResult = oSearch.ShowDialog();
                    if (oResult == DialogResult.OK) {
                        moDisposal.doHandleLorryResult(ref oDialog, bFromFocus, false);
                        return true;
                    } else if (oResult == DialogResult.Ignore) {
                        moDisposal.doHandleLorryResult(ref oDialog, bFromFocus, true);
                        return true;
                    } else
                        return false;
                } else
                    return true;
            } finally {
                if (moDisposal.ISSecondWeigh) {
                    //doLoadSupportData();
                }
            }
        }

        private bool doVehicleSearch2(bool bOnsite, bool bFromFocus, bool bIgnoreData) {
            idh.wr1.form.search.Lorry2 oSearch = new idh.wr1.form.search.Lorry2();

            oSearch.Target = "LORRY";
            moDisposal.ISSecondWeigh = false;
            if (bOnsite)
                oSearch.doSetupControlOnsiteSearch();
            else
                oSearch.doSetupControlNormalSearch();

            //		   oSearch.Bridge.getWinControl("btCreate").Visible = true;		    
            object oDialog = oSearch;
            try {
                if (moDisposal.doSetVehicleSearchParams(ref oDialog, "LORRY", true, bFromFocus, bIgnoreData)) {
                    DialogResult oResult = oSearch.ShowDialog();
                    if (oResult == DialogResult.OK) {
                        moDisposal.doHandleLorryResult(ref oDialog, bFromFocus, false);
                        return true;
                    } else if (oResult == DialogResult.Ignore) {
                        moDisposal.doHandleLorryResult(ref oDialog, bFromFocus, true);
                        return true;
                    } else
                        return false;
                } else
                    return true;
            } finally {
                if (moDisposal.ISSecondWeigh) {
                    //doLoadSupportData();
                }
            }
        }

        private bool doLinkWOSearch(bool bFromFocus, bool bIgnoreData, bool bUseLast) {
            idh.wr1.form.search.LinkWO oSearch = new idh.wr1.form.search.LinkWO();
            oSearch.Target = "LINKWO";

            //string sOrd = IDH_BOOREF.Text;
            //string sRow = IDH_ROW.Text;
            //oSearch.doSetupControl(sOrd, sRow);

            object oDialog = oSearch;
            if (moDisposal.doSetLinkWOParams(ref oDialog, "LINKWO", true, bFromFocus, bIgnoreData, bUseLast)) {
                DialogResult oResult = oSearch.ShowDialog();
                if (oResult == DialogResult.OK) {
                    moDisposal.doHandleLinkWOResult(ref oDialog);
                    return true;
                } else
                    return false;
            }
            return false;
        }

        private bool doBPSearch(Control oControl, bool bFromFocus, bool bIgnoreData) {
            try {
                string sIsCS = null;
                string sTarget;
                string sTitle;
                if (oControl == IDH_CUSL || oControl == IDH_CUST) {
                    sTarget = DisposalOrder.BP_CUSTOMER;
                    sTitle = "Customer Search";
                } else if (oControl == IDH_CUSTNM) {
                    sTarget = DisposalOrder.BP_CUSTOMERNAME;
                    sTitle = "Customer Search";
                } else if (oControl == IDH_CARL || oControl == IDH_CARRIE) {
                    sTarget = DisposalOrder.BP_CARRIER;
                    sTitle = "Waste Carrier Search";
                } else if (oControl == IDH_CARNAM) {
                    sTarget = DisposalOrder.BP_CARRIERNAME;
                    sTitle = "Waste Carrier Search";
                } else if (oControl == IDH_PRDL || oControl == IDH_WPRODU) {
                    sTarget = DisposalOrder.BP_PRODUCER;
                    sTitle = "Producer Search";
                } else if (oControl == IDH_PROCF || oControl == IDH_PROCD) {
                    sTarget = DisposalOrder.BP_BUYFROM;
                    sTitle = "Producer Search";
                } else if (oControl == IDH_WNAM) {
                    sTarget = DisposalOrder.BP_PRODUCERNAME;
                    sTitle = "Producer Search";
                } else if (oControl == IDH_PRONM) {
                    sTarget = DisposalOrder.BP_BUYFROMNAME;
                    sTitle = "Producer Search";
                } else if (oControl == IDH_SITL || oControl == IDH_DISSIT) {
                    sTarget = DisposalOrder.BP_DISPOSALSITE;
                    sTitle = "DisposalSite Search";
                } else if (oControl == IDH_DISPCF || oControl == IDH_DISPCD) {
                    sTarget = DisposalOrder.BP_DISPOSALSITE2;
                    sTitle = "DisposalSite Search";
                } else if (oControl == IDH_DISNAM) {
                    sTarget = DisposalOrder.BP_DISPOSALSITENAME;
                    sTitle = "DisposalSite Search";
                } else if (oControl == IDH_DISPNM) {
                    sTarget = DisposalOrder.BP_DISPOSALSITENAME2;
                    sTitle = "DisposalSite Search";
                } else if (oControl == IDH_CSCF) {
                    sTarget = DisposalOrder.BP_COMPLIANCE;
                    sTitle = "Compliance Scheme Search";
                    sIsCS = "Y";
                } else {
                    sTarget = DisposalOrder.BP_CUSTOMER;
                    sTitle = "Business Partner Search";
                }

                return doLookupBP(sTitle,sTarget, sIsCS, bFromFocus, bIgnoreData);

                //idh.wr1.form.search.BP oSearch = new idh.wr1.form.search.BP(sIsCS);
                //object oDialog = oSearch;

                //oSearch.Text = sTitle;

                //if (moDisposal.doSetBPSearchParams(ref oDialog, sTarget, true, bFromFocus, bIgnoreData)) {
                //    if (oSearch.ShowDialog(this) == DialogResult.OK) {
                //        if (!moDisposal.doHandleBPResult(ref oDialog, sTarget)) {
                //            if (sTarget.Equals(DisposalOrder.BP_CUSTOMER) ||
                //                sTarget.Equals(DisposalOrder.BP_CUSTOMERNAME)) {
                //                IDH_CUST.Text = "";
                //                IDH_CUSTNM.Text = "";
                //            } else if (sTarget.Equals(DisposalOrder.BP_CARRIER) ||
                //                  sTarget.Equals(DisposalOrder.BP_CARRIERNAME)) {
                //                IDH_CARRIE.Text = "";
                //                IDH_CARNAM.Text = "";
                //            } else if (sTarget.Equals(DisposalOrder.BP_PRODUCER) ||
                //                  sTarget.Equals(DisposalOrder.BP_PRODUCERNAME) ||
                //                sTarget.Equals(DisposalOrder.BP_BUYFROM) ||
                //                   sTarget.Equals(DisposalOrder.BP_BUYFROMNAME)) {
                //                IDH_WPRODU.Text = "";
                //                IDH_WNAM.Text = "";
                //                IDH_PROCD.Text = "";
                //                IDH_PRONM.Text = "";
                //            } else if (sTarget.Equals(DisposalOrder.BP_DISPOSALSITE) ||
                //                  sTarget.Equals(DisposalOrder.BP_DISPOSALSITENAME) ||
                //                  sTarget.Equals(DisposalOrder.BP_DISPOSALSITE2) ||
                //                  sTarget.Equals(DisposalOrder.BP_DISPOSALSITENAME2)) {
                //                IDH_DISSIT.Text = "";
                //                IDH_DISNAM.Text = "";
                //                IDH_DISPCD.Text = "";
                //                IDH_DISPNM.Text = "";
                //            }
                //            return false;
                //        } else {
                //            if (sTarget.Equals(DisposalOrder.BP_CUSTOMER)) {
                //                if (moDisposal.Mode == FormBridge.Mode_ADD || moDisposal.Mode == FormBridge.Mode_UPDATE || moDisposal.Mode == FormBridge.Mode_OK) {
                //                    oAddGrid.doRefreshGridFromDBObject();
                //                    oDeductionGrid.doRefreshGridFromDBObject();
                //                }
                //                oAddGrid.Refresh();
                //                oDeductionGrid.Refresh();
                //            }
                //            return true;
                //        }
                //    } else
                //        return false;
                //} else {
                //    return true;
                //}
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
                return false;
            }
        }

        private bool doLookupBP(string sTitle, string sTarget, string sIsCS, bool bFromFocus, bool bIgnoreData) {
            idh.wr1.form.search.BP oSearch = new idh.wr1.form.search.BP(sIsCS);
            object oDialog = oSearch;

            oSearch.Text = sTitle;

            if (moDisposal.doSetBPSearchParams(ref oDialog, sTarget, true, bFromFocus, bIgnoreData)) {
                if (oSearch.ShowDialog(this) == DialogResult.OK) {
                    if (!moDisposal.doHandleBPResult(ref oDialog, sTarget)) {
                        if (sTarget.Equals(DisposalOrder.BP_CUSTOMER) ||
                            sTarget.Equals(DisposalOrder.BP_CUSTOMERNAME)) {
                            IDH_CUST.Text = "";
                            IDH_CUSTNM.Text = "";
                        } else if (sTarget.Equals(DisposalOrder.BP_CARRIER) ||
                              sTarget.Equals(DisposalOrder.BP_CARRIERNAME)) {
                            IDH_CARRIE.Text = "";
                            IDH_CARNAM.Text = "";
                        } else if (sTarget.Equals(DisposalOrder.BP_PRODUCER) ||
                              sTarget.Equals(DisposalOrder.BP_PRODUCERNAME) ||
                            sTarget.Equals(DisposalOrder.BP_BUYFROM) ||
                               sTarget.Equals(DisposalOrder.BP_BUYFROMNAME)) {
                            IDH_WPRODU.Text = "";
                            IDH_WNAM.Text = "";
                            IDH_PROCD.Text = "";
                            IDH_PRONM.Text = "";
                        } else if (sTarget.Equals(DisposalOrder.BP_DISPOSALSITE) ||
                              sTarget.Equals(DisposalOrder.BP_DISPOSALSITENAME) ||
                              sTarget.Equals(DisposalOrder.BP_DISPOSALSITE2) ||
                              sTarget.Equals(DisposalOrder.BP_DISPOSALSITENAME2)) {
                            IDH_DISSIT.Text = "";
                            IDH_DISNAM.Text = "";
                            IDH_DISPCD.Text = "";
                            IDH_DISPNM.Text = "";
                        }
                        return false;
                    } else {
                        if (sTarget.Equals(DisposalOrder.BP_CUSTOMER)) {
                            if (moDisposal.Mode == FormBridge.Mode_ADD || moDisposal.Mode == FormBridge.Mode_UPDATE || moDisposal.Mode == FormBridge.Mode_OK) {
                                oAddGrid.doRefreshGridFromDBObject();
                                oDeductionGrid.doRefreshGridFromDBObject();
                            }
                            oAddGrid.Refresh();
                            oDeductionGrid.Refresh();
                        }
                        return true;
                    }
                } else {
                    if (sTarget.Equals(DisposalOrder.BP_CUSTOMER))
                        IDH_CUSTNM.Text = "";
                    else if (sTarget.Equals(DisposalOrder.BP_CUSTOMERNAME)) 
                        IDH_CUST.Text = "";
                    else if (sTarget.Equals(DisposalOrder.BP_CARRIER))
                        IDH_CARNAM.Text = "";
                    else if (sTarget.Equals(DisposalOrder.BP_CARRIERNAME))
                        IDH_CARRIE.Text = "";
                    else if (sTarget.Equals(DisposalOrder.BP_PRODUCER)) 
                        IDH_PRONM.Text = "";
                    else if (sTarget.Equals(DisposalOrder.BP_PRODUCERNAME))
                        IDH_PROCD.Text = "";
                    else if (sTarget.Equals(DisposalOrder.BP_DISPOSALSITE)) {
                        //IDH_DISPNM.Text = "";
                        IDH_DISNAM.Text = "";
                    }
                    else if (sTarget.Equals(DisposalOrder.BP_DISPOSALSITENAME)) {
                        //IDH_DISPCD.Text = "";
                        IDH_DISSIT.Text = "";
                    }
                    else if ( sTarget.Equals(DisposalOrder.BP_DISPOSALSITE2))
                        IDH_DISPNM.Text = "";
                    else if (sTarget.Equals(DisposalOrder.BP_DISPOSALSITENAME2))
                        IDH_DISPCD.Text = "";
                    return false;
                }
            } else {
                return true;
            }
        }

        private bool doAddressSearch(Control oControl, bool bFromFocus) {
            try {
                idh.wr1.form.search.Address oSearch = new idh.wr1.form.search.Address();

                object oDialog = oSearch;
                string sTarget;
                string sTitle;
                if (oControl == IDH_CUSAL) {
                    sTarget = DisposalOrder.BP_CUSTOMER;
                    sTitle = "Customer Search";
                } else if (oControl == IDH_WASAL) {
                    sTarget = DisposalOrder.BP_CARRIER;
                    sTitle = "Waste Carrier Search";
                } else if (oControl == IDH_PROAL) {
                    sTarget = DisposalOrder.BP_PRODUCER;
                    sTitle = "Producer Search";
                } else if (oControl == IDH_SITAL) {
                    sTarget = DisposalOrder.BP_DISPOSALSITE;
                    sTitle = "DisposalSite Search";
                } else {
                    sTarget = DisposalOrder.BP_CUSTOMER;
                    sTitle = "Business Partner Search";
                }
                oSearch.Text = sTitle;

                moDisposal.doSetAddressSearchParams(ref oDialog, sTarget, true);

                if (oSearch.ShowDialog(this) == DialogResult.OK) {
                    moDisposal.doHandleAddressResult(ref oDialog, sTarget);
                    return false;
                }
                return true;
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
                return false;
            }
        }

        private bool doWasteSearch(bool bFromFocus, bool bIgnoreData) {
            idh.wr1.form.search.WasteItem oSearch = new idh.wr1.form.search.WasteItem();

            object oDialog = oSearch;
            moDisposal.doSetWasteItemSearchParams(ref oDialog, true, bFromFocus, bIgnoreData);

            if (oSearch.ShowDialog(this) == DialogResult.OK) {
                moDisposal.doHandleWasteItemResult(ref oDialog);
                if (moDisposal.Mode == FormBridge.Mode_ADD || moDisposal.Mode == FormBridge.Mode_UPDATE || moDisposal.Mode == FormBridge.Mode_OK) {
                    oAddGrid.doRefreshGridFromDBObject();
                    oDeductionGrid.doRefreshGridFromDBObject();
                }
                return true;
            } else {
                return false;
            }
        }

        private bool doContainerSearch(bool bFromFocus, bool bIgnoreData) {
            idh.wr1.form.search.WRItem oSearch = new idh.wr1.form.search.WRItem();
            oSearch.doSetupControl();

            object oDialog = oSearch;
            moDisposal.doSetItemSearchParams(ref oDialog, true, bFromFocus, bIgnoreData);

            if (oSearch.ShowDialog(this) == DialogResult.OK) {
                moDisposal.doHandleItemResult(ref oDialog);
                return true;
            } else
                return true;
        }

        private bool doOriginSearch(string sControlName) {
            idh.wr1.form.search.Origin oSearch = new idh.wr1.form.search.Origin();
            oSearch.doSetupControl();

            object oDialog = oSearch;
            moDisposal.doSetOriginSearchParams(sControlName, ref oDialog, true);

            if (oSearch.ShowDialog(this) == DialogResult.OK) {
                moDisposal.doHandleOriginResult(ref oDialog);
                return true;
            } else
                return false;
        }
#endregion

#region CheckboxTriggers
        void IDH_NOVATCheckedChanged(object sender, EventArgs e) {
            moDisposal.DisposalRows.doSwitchVatGroups();
        }

        void IDH_ACCCheckedChanged(object sender, EventArgs e) {
            CheckBox oCheckBox = (CheckBox)sender;
            if (oCheckBox.Checked) {
                moDisposal.DisposalRows.doCheckAccountingCheckBox(oCheckBox.Name, false);
                moDisposal.DisposalRows.doSwitchAccountingValues(oCheckBox.Name);
            }
        }
#endregion

        void IDH_WEIBRGSelectedIndexChanged(object sender, EventArgs e) {
            string sID = moDisposal.getUFValue(((Control)sender).Name);
            moDisposal.DisposalRows.doConfigWeighBridge(sID);
            moDisposal.SetWeighBridgeNoSwitch(sID);
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e) {
            if (e.Control) {
                string sKey = e.KeyCode.ToString();
                if (sKey == "L") {
                    if (IDH_REGL.Enabled)
                        doVehicleSearch(false, false, e.Alt);

                    e.Handled = true;
                } else if (sKey == "O") {
                    if (IDH_OS.Enabled)
                        doVehicleSearch(true, false, e.Alt);

                    e.Handled = true;
                } else if (sKey == "I") {
                    if (IDH_WOCF.Enabled)
                        doLinkWOSearch(false, e.Alt, false);

                    e.Handled = true;
                } else if (sKey == "U") {
                    if (IDH_CUSL.Enabled)
                        doBPSearch(IDH_CUST, false, e.Alt);

                    e.Handled = true;
                } else if (sKey == "R") {
                    if (IDH_CARL.Enabled)
                        doBPSearch(IDH_CARRIE, false, e.Alt);

                    e.Handled = true;
                } else if (sKey == "P") {
                    if (IDH_PRDL.Enabled)
                        doBPSearch(IDH_WPRODU, false, e.Alt);

                    e.Handled = true;
                } else if (sKey == "D") {
                    if (IDH_SITL.Enabled)
                        doBPSearch(IDH_DISSIT, false, e.Alt);

                    e.Handled = true;
                } else if (sKey == "W") {
                    if (IDH_WASCF.Enabled)
                        doWasteSearch(false, e.Alt);

                    e.Handled = true;
                } else if (sKey == "B") {
                    if (IDH_ITCF.Enabled)
                        doContainerSearch(false, e.Alt);

                    e.Handled = true;
                } else if (sKey == "F") {
                    moDisposal.Mode = FormBridge.Mode_FIND;
                    btn1.Text = FormBridge.Mode_FIND;
                    e.Handled = true;
                } else if (sKey == "A") {
                    moDisposal.Mode = FormBridge.Mode_ADD;
                    e.Handled = true;
                } else if (sKey == "Down") {
                    moDisposal.getByKeyFirst();
                    e.Handled = true;
                } else if (sKey == "Left") {
                    moDisposal.getByKeyPrevious();
                    e.Handled = true;
                } else if (sKey == "Right") {
                    moDisposal.getByKeyNext();
                    e.Handled = true;
                } else if (sKey == "Up") {
                    moDisposal.getByKeyLast();
                    e.Handled = true;
                } else if (sKey == "1") {
                    moDisposal.doReadWeight("ACC1");
                    e.Handled = true;
                } else if (sKey == "2") {
                    moDisposal.doReadWeight("ACC2");
                    e.Handled = true;
                }
            } else
                if ((int)e.KeyCode == 112) {
                    WR1Help oHelp = new WR1Help();
                    string sFile = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\help.html"; ;
                    oHelp.setHelpFile(sFile);
                    oHelp.Show();

                    e.Handled = true;
                }
        }

        protected override void doEnterControl(Control oControl) {
            doTempSwitchValidation(oControl, false);
        }

        protected override void doLeaveControl(Control oControl) {
            doTempSwitchValidation(oControl, true);
        }

        private void doTempSwitchValidation(Control oControl, bool bTriggerValidation) {
            if (oControl == IDH_VEHREG) {
                IDH_OS.CausesValidation = bTriggerValidation;
                IDH_REGL.CausesValidation = bTriggerValidation;
            } else if (oControl == IDH_CUST)
                IDH_CUSL.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_ONCS)
                IDH_CSCF.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_CARRIE)
                IDH_CARL.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_ITMCOD)
                IDH_ITCF.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_PROCD)
                IDH_PROCF.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_DISPCD)
                IDH_DISPCF.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_WASCL1)
                IDH_WASCF.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_RORIGI)
                IDH_ORLK.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_ADDRES)
                IDH_WASAL.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_CUSADD)
                IDH_CUSAL.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_ROUTE)
                IDH_ROUTL.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_ORIGIN2)
                IDH_ORIGLU2.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_WPRODU)
                IDH_PRDL.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_PRDADD)
                IDH_PROAL.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_ORIGIN)
                IDH_ORIGLU.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_DISSIT)
                IDH_SITL.CausesValidation = bTriggerValidation;
            else if (oControl == IDH_SITADD)
                IDH_SITAL.CausesValidation = bTriggerValidation;
        }

        private void IDH_CNA_Click(object sender, EventArgs e) {
            moDisposal.doAddCustomerShipToAddress();
        }

        private void label19_HelpRequested(object sender, HelpEventArgs hlpevent) {
            WR1Help oHelp = new WR1Help();
            string sFile = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\help.html"; ;
            oHelp.setHelpFile(sFile);
            oHelp.Show();
        }

        private void IDH_Amend_Click(object sender, EventArgs e) {
            DOAmend oAmend = new DOAmend();
            string sDOR = moDisposal.DisposalRows.DBObject.Code;
            oAmend.DORCode = sDOR;
            DialogResult oResult = oAmend.ShowDialog(this);

            oAmend.Dispose();

            if (oResult == DialogResult.OK) {
                string sDONr = moDisposal.getFormDFValueAsString(IDH_DISPORD._Code);
                moDisposal.getByKey(sDONr);
                moDisposal.Mode = FormBridge.Mode_OK;
                doLoadDOSupportData();
            }
        }

        private void btnReadConfig_Load(object sender, EventArgs e) {

        }

        private void btnSummary_Click(object sender, EventArgs e) {
            IDH_DOTXDT oTax = new IDH_DOTXDT();
            string sCode = moDisposal.DisposalRows.DBObject.Code;
            //oTax.doLoadDISPROW(sCode);
            oTax.doLoadDISPROW(moDisposal.DisposalRows.DBObject);

            IDH_DOADDEXP oAdditionalDBO = (IDH_DOADDEXP)oAddGrid.getDBObject();
            Hashtable oAddItems = new Hashtable();

            if (oAdditionalDBO != null && oAdditionalDBO.Count > 0) {//If Not oGrid Is Nothing Then
                oAdditionalDBO.first();
                while (oAdditionalDBO.next()) {
                    if ( oAdditionalDBO.U_ItemCd.Trim().Length == 0)
                        continue;

                    string[] saAdditional = new string[3];

                    saAdditional[0] = oAdditionalDBO.U_ItemCd;
                    saAdditional[1] = com.idh.utils.Conversions.doDoubleToMoney(oAdditionalDBO.U_ItmChrg * oAdditionalDBO.U_Quantity); //.ToString("##0.000");
                    saAdditional[2] = oAdditionalDBO.U_ItemDsc;
                    oAddItems.Add(oAdditionalDBO.CurrentRowIndex, saAdditional);
                }
            }
            
            
            oTax.doFillDataObject(oAddItems);

            TaxSummary oTaxSummary = new TaxSummary();
            string sText = oTax.toHtml().ToString();
            oTaxSummary.HTML = sText;
            oTaxSummary.ShowDialog(this);
        }

        private void IDH_NEWWEI_Click(object sender, EventArgs e) {
            if (moDisposal.DBObject.doValidation(true)) {
                if (moDisposal.DisposalRows.DBObject.doValidation(true)) {
                    doSetMultiWeighDefaults();
                    try {
                        //Add a new Empty Line to the Rows and Update the Children
                        moDisposal.DisposalRows.DBObject.doAddEmptyRow(true, true);
                        if ( !string.IsNullOrWhiteSpace(moDisposal.DisposalRows.DBObject.U_TrnCode)) {
                            moDisposal.DisposalRows.DBObject.doHandleTransactionCodeChange();
                        }
                        oDORows.doRefreshGridFromDBObject();
                        moDisposal.doUpdateFormData();
                    } catch (Exception ex) {
                        throw (ex);
                    } finally {
                        doClearMultiWeighDefaults();
                    }
                } else {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Data Row Validation error. The following needs to be set: " + moDisposal.DisposalRows.DBObject.LastValidationError,
                        "ERUSDRVE", new string[] { idh.bridge.Translation.getTranslatedWord(moDisposal.DisposalRows.DBObject.LastValidationError) });
                }
            } else {
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Data Row Validation error. The following needs to be set: " + moDisposal.DBObject.LastValidationError,
                        "ERUSDRVE", new string[] { idh.bridge.Translation.getTranslatedWord(moDisposal.DBObject.LastValidationError) });
            }
        }

        private void doSetMultiWeighDefaults() {
            IDH_DISPROW oRow = moDisposal.DisposalRows.DBObject;
            
            oRow.doSetDefaultValue(IDH_DISPROW._JobTp, oRow.U_JobTp);
            oRow.doSetDefaultValue(IDH_DISPROW._Lorry, oRow.U_Lorry);
            oRow.doSetDefaultValue(IDH_DISPROW._LorryCd, oRow.U_LorryCd);
            oRow.doSetDefaultValue(IDH_DISPROW._VehTyp, oRow.U_VehTyp);
            oRow.doSetDefaultValue(IDH_DISPROW._TRLReg, oRow.U_TRLReg);
            oRow.doSetDefaultValue(IDH_DISPROW._TRLNM, oRow.U_TRLNM);
            oRow.doSetDefaultValue(IDH_DISPROW._Driver, oRow.U_Driver);
            oRow.doSetDefaultValue(IDH_DISPROW._SAddress, oRow.U_SAddress);
            oRow.doSetDefaultValue(IDH_DISPROW._SAddrsLN, oRow.U_SAddrsLN);
        }

        private void doClearMultiWeighDefaults() {
            IDH_DISPROW oRow = moDisposal.DisposalRows.DBObject;

            oRow.doSetDefaultValue(IDH_DISPROW._JobTp, "");
            oRow.doSetDefaultValue(IDH_DISPROW._Lorry, "");
            oRow.doSetDefaultValue(IDH_DISPROW._LorryCd, "");
            oRow.doSetDefaultValue(IDH_DISPROW._VehTyp, "");
            oRow.doSetDefaultValue(IDH_DISPROW._TRLReg, "");
            oRow.doSetDefaultValue(IDH_DISPROW._TRLNM, "");
            oRow.doSetDefaultValue(IDH_DISPROW._Driver, "");
            oRow.doSetDefaultValue(IDH_DISPROW._SAddress, "");
            oRow.doSetDefaultValue(IDH_DISPROW._SAddrsLN, "");
        }

    }


#region OldCode
    ///**
    // * Use this method to do Checks and Controls when the focus is lost from a control
    // */
    //private void All_Enter(object sender, EventArgs e) {
    //    try {
    //        moLastEnter = sender;
    //        if (sender != oPNavigation &&
    //             sender != btFind && sender != btAdd &&
    //             sender != btFirst && sender != btPrevious &&
    //             sender != btNext && sender != btLast) {
    //            if (moLastLeave != null && moLastLeave != sender) {
    //                Control oLastLeave = (Control)moLastLeave;
    //                if (oLastLeave.Name == "IDH_VEHREG") {
    //                    if (sender != IDH_WRORD && sender != IDH_WRROW &&
    //                        sender != IDH_OS && sender != IDH_REGL && sender != IDH_WOCF) {
    //                        if (!doRegCheck())
    //                            oLastLeave.Focus();
    //                    }
    //                } else if (oLastLeave == IDH_CUST ||
    //                      oLastLeave == IDH_CUSTNM ||
    //                      oLastLeave == IDH_CARRIE ||
    //                      oLastLeave == IDH_CARNAM ||
    //                      oLastLeave == IDH_WPRODU ||
    //                      oLastLeave == IDH_WNAM ||
    //                      oLastLeave == IDH_DISSIT ||
    //                      oLastLeave == IDH_DISNAM ||
    //                      oLastLeave == IDH_PROCD ||
    //                      oLastLeave == IDH_PRONM ||
    //                      oLastLeave == IDH_DISPCD ||
    //                      oLastLeave == IDH_DISPNM) {
    //                    if (!doBPCheck(oLastLeave))
    //                        oLastLeave.Focus();
    //                } else if (oLastLeave == IDH_WRORD ||
    //                            oLastLeave == IDH_WRROW) {
    //                    if (!doCheckLinkWO(oLastLeave))
    //                        oLastLeave.Focus();
    //                } else if (oLastLeave == IDH_ITMCOD ||
    //                           oLastLeave == IDH_DESC) {
    //                    if (sender != IDH_ITCF) {
    //                        if (!doCheckContainer(oLastLeave))
    //                            oLastLeave.Focus();
    //                    }
    //                } else if (oLastLeave == IDH_WASCL1 ||
    //                          oLastLeave == IDH_WASMAT) {
    //                    if (sender != IDH_WASCF) {
    //                        if (!doCheckWaste(oLastLeave))
    //                            oLastLeave.Focus();
    //                    }
    //                } else if (oLastLeave == IDH_ORIGIN ||
    //                            oLastLeave == IDH_ORIGIN2 ||
    //                            oLastLeave == IDH_RORIGI) {
    //                    if (sender != IDH_ORIGLU &&
    //                        sender != IDH_ORLK) {
    //                        string sCurrentValue = "";
    //                        string sItemValue = oLastLeave.Text;
    //                        sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._Origin);
    //                        if (!sItemValue.Equals(sCurrentValue))
    //                            doOriginSearch();
    //                    }
    //                } else if (oLastLeave == IDH_DOORD ||
    //                            oLastLeave == IDH_DOARI ||
    //                            oLastLeave == IDH_AINV ||
    //                            oLastLeave == IDH_DOARIP ||
    //                            oLastLeave == IDH_FOC ||
    //                            oLastLeave == IDH_DOPO) {
    //                } else
    //                    doUpdateDB();
    //            }
    //        }
    //        moLastLeave = null;
    //    } catch (Exception ex) {
    //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
    //    }
    //}

    //private void All_Leave(object sender, EventArgs e) {
    //    try {
    //        if (isEditableField(sender))
    //            moLastLeave = sender;
    //        else
    //            moLastEnter = null;

    //        if (sender == IDH_CUSCM)
    //            IDH_WASTTN.Focus();
    //    } catch (Exception ex) {
    //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
    //    }
    //}

    //private void All_TextChanged(object sender, EventArgs e) {
    //    try {
    //        if (moDisposal.Mode == FormBridge.Mode_VIEW ||
    //            moDisposal.Mode == FormBridge.Mode_OK) {
    //            moLastLeave = sender;
    //            string sName = ((Control)sender).Name;
    //            if (Array.IndexOf(soIgnoreControls, sName) == -1) {
    //                moDisposal.Mode = FormBridge.Mode_UPDATE; //doSetToUpdateMode();
    //                btn1.Text = FormBridge.Mode_UPDATE;
    //            }
    //        }
    //    } catch (Exception ex) {
    //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
    //    }
    //}

    ///**
    // * Only update fields where the Edit form fields has been changed
    // */
    //private void doUpdateDB() {
    //    if (moLastLeave == null)
    //        return;
    //    Control oControl = (Control)moLastLeave;
    //    if ((oControl is TextBox && ((TextBox)oControl).Modified) ||
    //        !(oControl is TextBox)) {
    //        moDisposal.setWasSetFromUser(oControl.Name);
    //        moDisposal.doUpdateDBFieldFromFormField(oControl);
    //        moDisposal.doHandleFieldChange(oControl.Name);
    //        if (oControl is TextBox) {
    //            ((TextBox)oControl).Modified = false;
    //        }
    //    }
    //}

    //private bool doBPCheck(Control oControl) {
    //    string sFieldName = oControl.Name;

    //    string sCurrentValue = "";
    //    string sItemValue = oControl.Text;
    //    IDH_DISPORD oDisp = moDisposal.DBObject;

    //    if (oControl == IDH_CUST)
    //        sCurrentValue = oDisp.U_CardCd;
    //    else if (oControl == IDH_CUSTNM)
    //        sCurrentValue = oDisp.U_CardNM;
    //    else if (oControl == IDH_CARRIE)
    //        sCurrentValue = oDisp.U_CCardCd;
    //    else if (oControl == IDH_CARNAM)
    //        sCurrentValue = oDisp.U_CCardNM;
    //    else if (oControl == IDH_WPRODU)
    //        sCurrentValue = oDisp.U_PCardCd;
    //    else if (oControl == IDH_PROCF)
    //        sCurrentValue = oDisp.U_PCardCd;
    //    else if (oControl == IDH_WNAM)
    //        sCurrentValue = oDisp.U_PCardNM;
    //    else if (oControl == IDH_DISSIT)
    //        sCurrentValue = oDisp.U_SCardCd;
    //    else if (oControl == IDH_DISNAM)
    //        sCurrentValue = oDisp.U_SCardNM;
    //    else if (oControl == IDH_PROCD)
    //        sCurrentValue = oDisp.U_PCardCd;
    //    else if (oControl == IDH_PRONM)
    //        sCurrentValue = oDisp.U_PCardNM;
    //    else if (oControl == IDH_DISPCD)
    //        sCurrentValue = oDisp.U_SCardCd;
    //    else if (oControl == IDH_DISPNM)
    //        sCurrentValue = oDisp.U_SCardNM;

    //    if (!sItemValue.Equals(sCurrentValue)) {
    //        if (!doBPSearch(oControl, true, false)) {
    //            return false;
    //        }
    //    }
    //    return true;
    //}
#endregion
}
