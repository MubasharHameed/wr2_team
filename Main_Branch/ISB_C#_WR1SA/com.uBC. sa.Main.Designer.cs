﻿using com.idh.win.forms;
using com.idh.bridge;

namespace com.uBC.sa {
    partial class Main : SBOForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.moModulesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moPreSelectMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moDOMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moWindowsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moMainMenu = new System.Windows.Forms.MenuStrip();
            this.moTools = new System.Windows.Forms.ToolStripMenuItem();
            this.moConfigValues = new System.Windows.Forms.ToolStripMenuItem();
            this.poQuickTool = new System.Windows.Forms.Panel();
            this.moPreSelect = new com.uBC.toolControl.PreSelect();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.moMainMenu.SuspendLayout();
            this.poQuickTool.SuspendLayout();
            this.SuspendLayout();
            // 
            // moModulesMenuItem
            // 
            this.moModulesMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moPreSelectMenuItem,
            this.moDOMenuItem});
            this.moModulesMenuItem.Name = "moModulesMenuItem";
            this.moModulesMenuItem.Size = new System.Drawing.Size(65, 20);
            this.moModulesMenuItem.Text = "&Modules";
            // 
            // moPreSelectMenuItem
            // 
            this.moPreSelectMenuItem.Name = "moPreSelectMenuItem";
            this.moPreSelectMenuItem.Size = new System.Drawing.Size(151, 22);
            this.moPreSelectMenuItem.Text = "&PreSelect";
            this.moPreSelectMenuItem.Click += new System.EventHandler(this.moPreSelectMenuItem_Click);
            // 
            // moDOMenuItem
            // 
            this.moDOMenuItem.Name = "moDOMenuItem";
            this.moDOMenuItem.Size = new System.Drawing.Size(151, 22);
            this.moDOMenuItem.Text = "&Disposal Order";
            this.moDOMenuItem.Click += new System.EventHandler(this.disposalOrderToolStripMenuItem_Click);
            // 
            // moWindowsMenuItem
            // 
            this.moWindowsMenuItem.Name = "moWindowsMenuItem";
            this.moWindowsMenuItem.Size = new System.Drawing.Size(68, 20);
            this.moWindowsMenuItem.Text = "&Windows";
            // 
            // moMainMenu
            // 
            this.moMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moModulesMenuItem,
            this.moWindowsMenuItem,
            this.moTools});
            this.moMainMenu.Location = new System.Drawing.Point(0, 0);
            this.moMainMenu.MdiWindowListItem = this.moWindowsMenuItem;
            this.moMainMenu.Name = "moMainMenu";
            this.moMainMenu.Size = new System.Drawing.Size(860, 24);
            this.moMainMenu.TabIndex = 1;
            this.moMainMenu.Text = "menuStrip1";
            // 
            // moTools
            // 
            this.moTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moConfigValues});
            this.moTools.Name = "moTools";
            this.moTools.Size = new System.Drawing.Size(48, 20);
            this.moTools.Text = "&Tools";
            this.moTools.Click += new System.EventHandler(this.moTools_Click);
            // 
            // moConfigValues
            // 
            this.moConfigValues.Name = "moConfigValues";
            this.moConfigValues.Size = new System.Drawing.Size(147, 22);
            this.moConfigValues.Text = "Config &Values";
            this.moConfigValues.Click += new System.EventHandler(this.configValuesToolStripMenuItem_Click);
            // 
            // poQuickTool
            // 
            this.poQuickTool.Controls.Add(this.moPreSelect);
            this.poQuickTool.Dock = System.Windows.Forms.DockStyle.Top;
            this.poQuickTool.Location = new System.Drawing.Point(0, 24);
            this.poQuickTool.Name = "poQuickTool";
            this.poQuickTool.Size = new System.Drawing.Size(860, 100);
            this.poQuickTool.TabIndex = 3;
            this.poQuickTool.Visible = false;
            // 
            // moPreSelect
            // 
            this.moPreSelect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.moPreSelect.Location = new System.Drawing.Point(0, 0);
            this.moPreSelect.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.moPreSelect.Name = "moPreSelect";
            this.moPreSelect.Size = new System.Drawing.Size(860, 100);
            this.moPreSelect.TabIndex = 0;
            this.moPreSelect.TopForm = null;
            this.moPreSelect.CloseButtonClick += new System.EventHandler(this.moPreSelect_CloseButtonClick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 660);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(860, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(860, 682);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.poQuickTool);
            this.Controls.Add(this.moMainMenu);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.moMainMenu;
            this.Name = "Main";
            this.Text = "WR1 Standalone";
            this.TransparencyKey = System.Drawing.Color.Transparent;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.Shown += new System.EventHandler(this.Main_Shown);
            this.moMainMenu.ResumeLayout(false);
            this.moMainMenu.PerformLayout();
            this.poQuickTool.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        //private toolControl.PreSelect poPreSelect;
        //private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        //private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        //private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        //private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        //private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.ToolStripMenuItem moModulesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moPreSelectMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moDOMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moWindowsMenuItem;
        private System.Windows.Forms.MenuStrip moMainMenu;
        private System.Windows.Forms.Panel poQuickTool;
        private toolControl.PreSelect moPreSelect;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem moTools;
        private System.Windows.Forms.ToolStripMenuItem moConfigValues;
    }
}