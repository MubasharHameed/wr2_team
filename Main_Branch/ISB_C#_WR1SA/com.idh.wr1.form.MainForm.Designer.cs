﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2008/12/16
 * Time: 06:00 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Collections;
using System.Windows.Forms;

using com.idh.bridge;
using com.idh.bridge.utils;
using com.idh.bridge.form;
using com.idh.bridge.lookups;
using com.idh.bridge.reports;
using com.idh.dbObjects.User;
using com.idh.bridge.error;

//using com.idh.win.controls.SBO;

namespace com.idh.wr1.form
{
	partial class MainForm
	{
        ///// <summary>
        ///// Designer variable used to keep track of non-visual components.
        ///// </summary>
        ///// 
        //private bool mbDoBP = false;
        //private System.ComponentModel.IContainer components = null;
        //private object moLastEnter = null;
        //private object moLastLeave = null;
        
        //private string[] soIgnoreControls = {"IDH_ADDCO","IDH_ADDCH"};

		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.IDH_JOBTTP = new System.Windows.Forms.ComboBox();
            this.IDH_VEHREG = new System.Windows.Forms.TextBox();
            this.IDH_WRORD = new System.Windows.Forms.TextBox();
            this.IDH_WRROW = new System.Windows.Forms.TextBox();
            this.IDH_VEH = new System.Windows.Forms.TextBox();
            this.IDH_DRIVER = new System.Windows.Forms.TextBox();
            this.IDH_TRLReg = new System.Windows.Forms.TextBox();
            this.IDH_TRLNM = new System.Windows.Forms.TextBox();
            this.oMainTabs = new System.Windows.Forms.TabControl();
            this.oTPWeighing = new System.Windows.Forms.TabPage();
            this.IDH_EXPLDW = new System.Windows.Forms.TextBox();
            this.label129 = new System.Windows.Forms.Label();
            this.IDH_USEAU = new System.Windows.Forms.RadioButton();
            this.IDH_USERE = new System.Windows.Forms.RadioButton();
            this.PO_GRP = new System.Windows.Forms.GroupBox();
            this.label128 = new System.Windows.Forms.Label();
            this.IDH_ADDEX = new System.Windows.Forms.TextBox();
            this.label127 = new System.Windows.Forms.Label();
            this.IDH_ADDCOS = new System.Windows.Forms.TextBox();
            this.IDH_PUOM = new System.Windows.Forms.ComboBox();
            this.label118 = new System.Windows.Forms.Label();
            this.IDH_PURUOM = new System.Windows.Forms.ComboBox();
            this.label105 = new System.Windows.Forms.Label();
            this.IDH_ORDTOT = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.IDH_PURWGT = new System.Windows.Forms.TextBox();
            this.IDH_ORDCOS = new System.Windows.Forms.TextBox();
            this.IDH_PRCOST = new System.Windows.Forms.TextBox();
            this.IDH_ORDQTY = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.IDH_PURTOT = new System.Windows.Forms.TextBox();
            this.IDH_DOPO = new System.Windows.Forms.CheckBox();
            this.label121 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.IDH_TIPTOT = new System.Windows.Forms.TextBox();
            this.IDH_TAXO = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.IDH_TIPCOS = new System.Windows.Forms.TextBox();
            this.IDH_PSTAT = new System.Windows.Forms.TextBox();
            this.IDH_TIPWEI = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.IDH_TOTCOS = new System.Windows.Forms.TextBox();
            this.SO_GRP = new System.Windows.Forms.GroupBox();
            this.IDH_UOM = new System.Windows.Forms.ComboBox();
            this.label90 = new System.Windows.Forms.Label();
            this.IDH_VALDED = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.IDH_CUSTOT = new System.Windows.Forms.TextBox();
            this.IDH_DOORD = new System.Windows.Forms.CheckBox();
            this.IDH_DSCPRC = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.IDH_ADDCHR = new System.Windows.Forms.TextBox();
            this.IDH_DOARIP = new System.Windows.Forms.CheckBox();
            this.IDH_SUBTOT = new System.Windows.Forms.TextBox();
            this.IDH_DOARI = new System.Windows.Forms.CheckBox();
            this.IDH_TAX = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.IDH_NOVAT = new System.Windows.Forms.CheckBox();
            this.IDH_CASHMT = new System.Windows.Forms.TextBox();
            this.IDH_TOTAL = new System.Windows.Forms.TextBox();
            this.IDH_FOC = new System.Windows.Forms.CheckBox();
            this.label82 = new System.Windows.Forms.Label();
            this.IDH_RSTAT = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.IDH_DISCNT = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.IDH_CUSWEI = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.IDH_CUSCHG = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.IDH_AINV = new System.Windows.Forms.CheckBox();
            this.label87 = new System.Windows.Forms.Label();
            this.IDH_ISTRL = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label126 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.IDH_DISPAD = new System.Windows.Forms.TextBox();
            this.IDH_DISPCF = new System.Windows.Forms.Button();
            this.IDH_DISPNM = new System.Windows.Forms.TextBox();
            this.IDH_DISPCD = new System.Windows.Forms.TextBox();
            this.label124 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.IDH_ITMGRN = new System.Windows.Forms.TextBox();
            this.IDH_ITMGRC = new System.Windows.Forms.TextBox();
            this.Lbl1000007 = new System.Windows.Forms.Label();
            this.IDH_ITMCOD = new System.Windows.Forms.TextBox();
            this.IDH_DESC = new System.Windows.Forms.TextBox();
            this.IDH_WASCL1 = new System.Windows.Forms.TextBox();
            this.IDH_WASMAT = new System.Windows.Forms.TextBox();
            this.IDH_RORIGI = new System.Windows.Forms.TextBox();
            this.IDH_ITCF = new System.Windows.Forms.Button();
            this.IDH_WASCF = new System.Windows.Forms.Button();
            this.IDH_ORLK = new System.Windows.Forms.Button();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.IDH_PRONM = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.IDH_PROCD = new System.Windows.Forms.TextBox();
            this.IDH_PROCF = new System.Windows.Forms.Button();
            this.IDH_ADJWGT = new System.Windows.Forms.TextBox();
            this.IDH_WGTDED = new System.Windows.Forms.TextBox();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.IDH_TAR2 = new System.Windows.Forms.Button();
            this.IDH_BUF2 = new System.Windows.Forms.Button();
            this.IDH_ACC2 = new System.Windows.Forms.Button();
            this.IDH_TAR1 = new System.Windows.Forms.Button();
            this.IDH_BUF1 = new System.Windows.Forms.Button();
            this.IDH_ACC1 = new System.Windows.Forms.Button();
            this.IDH_ACCB = new System.Windows.Forms.Button();
            this.IDH_TMR = new System.Windows.Forms.Button();
            this.IDH_REF = new System.Windows.Forms.Button();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.IDH_CUSCM = new System.Windows.Forms.TextBox();
            this.IDH_RDWGT = new System.Windows.Forms.TextBox();
            this.IDH_WDT2 = new System.Windows.Forms.TextBox();
            this.IDH_SER2 = new System.Windows.Forms.TextBox();
            this.IDH_WEI2 = new System.Windows.Forms.TextBox();
            this.IDH_WDT1 = new System.Windows.Forms.TextBox();
            this.IDH_SER1 = new System.Windows.Forms.TextBox();
            this.IDH_WEI1 = new System.Windows.Forms.TextBox();
            this.IDH_TRLTar = new System.Windows.Forms.TextBox();
            this.IDH_TARWEI = new System.Windows.Forms.TextBox();
            this.IDH_WDTB = new System.Windows.Forms.TextBox();
            this.IDH_SERB = new System.Windows.Forms.TextBox();
            this.IDH_WEIB = new System.Windows.Forms.TextBox();
            this.IDH_WEIG = new System.Windows.Forms.TextBox();
            this.IDH_WEIBRG = new System.Windows.Forms.ComboBox();
            this.oTPCarrier = new System.Windows.Forms.TabPage();
            this.IDH_LICREG = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.IDH_PHONE = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.IDH_ZIP = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.IDH_CITY = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.IDH_BLOCK = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.IDH_WASAL = new System.Windows.Forms.Button();
            this.IDH_ADDRES = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.IDH_STREET = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.IDH_CARREF = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.IDH_CONTNM = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.oTPCustomer = new System.Windows.Forms.TabPage();
            this.IDH_ORIGLU2 = new System.Windows.Forms.Button();
            this.IDH_ORIGIN2 = new System.Windows.Forms.TextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.IDH_SLCNO = new System.Windows.Forms.TextBox();
            this.IDH_PCD = new System.Windows.Forms.TextBox();
            this.IDH_SteId = new System.Windows.Forms.TextBox();
            this.IDH_ROUTL = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.IDH_SEQ = new System.Windows.Forms.TextBox();
            this.IDH_ROUTE = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.IDH_CNA = new System.Windows.Forms.Button();
            this.IDH_CUSAL = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.IDH_CUSLIC = new System.Windows.Forms.TextBox();
            this.IDH_SITETL = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.IDH_CUSPHO = new System.Windows.Forms.TextBox();
            this.IDH_CUSPOS = new System.Windows.Forms.TextBox();
            this.IDH_COUNTY = new System.Windows.Forms.TextBox();
            this.IDH_CUSCIT = new System.Windows.Forms.TextBox();
            this.IDH_CUSBLO = new System.Windows.Forms.TextBox();
            this.IDH_CUSSTR = new System.Windows.Forms.TextBox();
            this.IDH_CUSADD = new System.Windows.Forms.TextBox();
            this.IDH_CUSCON = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.oTPProducer = new System.Windows.Forms.TabPage();
            this.IDH_PROAL = new System.Windows.Forms.Button();
            this.IDH_ORIGLU = new System.Windows.Forms.Button();
            this.IDH_PRDL = new System.Windows.Forms.Button();
            this.IDH_ORIGIN = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.IDH_PRDPHO = new System.Windows.Forms.TextBox();
            this.IDH_PRDPOS = new System.Windows.Forms.TextBox();
            this.IDH_PRDCIT = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.IDH_PRDBLO = new System.Windows.Forms.TextBox();
            this.IDH_PRDSTR = new System.Windows.Forms.TextBox();
            this.IDH_PRDADD = new System.Windows.Forms.TextBox();
            this.IDH_PRDCRF = new System.Windows.Forms.TextBox();
            this.IDH_PRDCON = new System.Windows.Forms.TextBox();
            this.IDH_WNAM = new System.Windows.Forms.TextBox();
            this.IDH_WPRODU = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.oTPSite = new System.Windows.Forms.TabPage();
            this.IDH_SITAL = new System.Windows.Forms.Button();
            this.IDH_SITL = new System.Windows.Forms.Button();
            this.IDH_SITPHO = new System.Windows.Forms.TextBox();
            this.IDH_SITPOS = new System.Windows.Forms.TextBox();
            this.IDH_SITCIT = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.IDH_SITBLO = new System.Windows.Forms.TextBox();
            this.IDH_SITSTR = new System.Windows.Forms.TextBox();
            this.IDH_SITADD = new System.Windows.Forms.TextBox();
            this.IDH_SITCRF = new System.Windows.Forms.TextBox();
            this.IDH_SITCON = new System.Windows.Forms.TextBox();
            this.IDH_DISNAM = new System.Windows.Forms.TextBox();
            this.IDH_DISSIT = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.oAdditional = new System.Windows.Forms.TabPage();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.IDH_ADDCH = new System.Windows.Forms.TextBox();
            this.IDH_ADDCO = new System.Windows.Forms.TextBox();
            this.oAddGrid = new com.idh.wr1.grid.AdditionalExpenses();
            this.oDeductions = new System.Windows.Forms.TabPage();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.IDH_DEDVAL = new System.Windows.Forms.TextBox();
            this.IDH_DEDWEI = new System.Windows.Forms.TextBox();
            this.oDeductionGrid = new com.idh.wr1.grid.Deductions();
            this.oPNavigation = new System.Windows.Forms.Panel();
            this.btFind = new System.Windows.Forms.Button();
            this.btAdd = new System.Windows.Forms.Button();
            this.btLast = new System.Windows.Forms.Button();
            this.btNext = new System.Windows.Forms.Button();
            this.btPrevious = new System.Windows.Forms.Button();
            this.btFirst = new System.Windows.Forms.Button();
            this.IDH_SPECIN = new System.Windows.Forms.TextBox();
            this.IDH_CUST = new System.Windows.Forms.TextBox();
            this.IDH_CUSTNM = new System.Windows.Forms.TextBox();
            this.IDH_CUSCRF = new System.Windows.Forms.TextBox();
            this.IDH_ONCS = new System.Windows.Forms.TextBox();
            this.IDH_USER = new System.Windows.Forms.TextBox();
            this.IDH_CARNAM = new System.Windows.Forms.TextBox();
            this.IDH_CARRIE = new System.Windows.Forms.TextBox();
            this.IDH_SITREF = new System.Windows.Forms.TextBox();
            this.IDH_HAZCN = new System.Windows.Forms.TextBox();
            this.IDH_WASTTN = new System.Windows.Forms.TextBox();
            this.IDH_STATUS = new System.Windows.Forms.TextBox();
            this.IDH_BOOKDT = new System.Windows.Forms.TextBox();
            this.IDH_ROW = new System.Windows.Forms.TextBox();
            this.IDH_BOOREF = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.IDH_EXTWEI = new System.Windows.Forms.TextBox();
            this.IDH_CONTNR = new System.Windows.Forms.TextBox();
            this.IDH_SEALNR = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.IDH_BOOKTM = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.IDH_REGL = new System.Windows.Forms.Button();
            this.IDH_WOCF = new System.Windows.Forms.Button();
            this.IDH_CUSL = new System.Windows.Forms.Button();
            this.IDH_CSCF = new System.Windows.Forms.Button();
            this.IDH_CARL = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.IDH_BRANCH = new System.Windows.Forms.ComboBox();
            this.IDH_TZONE = new System.Windows.Forms.ComboBox();
            this.IDH_OBLED = new System.Windows.Forms.ComboBox();
            this.IDH_AVDOC = new System.Windows.Forms.CheckBox();
            this.IDH_AVCONV = new System.Windows.Forms.CheckBox();
            this.IDH_CON = new System.Windows.Forms.Button();
            this.IDH_DOC = new System.Windows.Forms.Button();
            this.IDH_OS = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.prg1 = new System.Windows.Forms.ToolStripProgressBar();
            this.LblVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.ItemInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.stl01 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblLastError = new System.Windows.Forms.ToolStripStatusLabel();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnHelp = new System.Windows.Forms.Button();
            this.IDH_CONGEN = new System.Windows.Forms.Button();
            this.IDH_BOOKIN = new System.Windows.Forms.CheckBox();
            this.IDH_BOKSTA = new System.Windows.Forms.TextBox();
            this.oMainTabs.SuspendLayout();
            this.oTPWeighing.SuspendLayout();
            this.PO_GRP.SuspendLayout();
            this.SO_GRP.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.oTPCarrier.SuspendLayout();
            this.oTPCustomer.SuspendLayout();
            this.oTPProducer.SuspendLayout();
            this.oTPSite.SuspendLayout();
            this.oAdditional.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oAddGrid)).BeginInit();
            this.oDeductions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oDeductionGrid)).BeginInit();
            this.oPNavigation.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Disposal Type";
            // 
            // IDH_JOBTTP
            // 
            this.IDH_JOBTTP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_JOBTTP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_JOBTTP.FormattingEnabled = true;
            this.IDH_JOBTTP.Location = new System.Drawing.Point(110, 3);
            this.IDH_JOBTTP.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_JOBTTP.Name = "IDH_JOBTTP";
            this.IDH_JOBTTP.Size = new System.Drawing.Size(155, 20);
            this.IDH_JOBTTP.TabIndex = 0;
            // 
            // IDH_VEHREG
            // 
            this.IDH_VEHREG.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_VEHREG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_VEHREG.Location = new System.Drawing.Point(110, 24);
            this.IDH_VEHREG.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_VEHREG.Name = "IDH_VEHREG";
            this.IDH_VEHREG.Size = new System.Drawing.Size(138, 18);
            this.IDH_VEHREG.TabIndex = 1;
            // 
            // IDH_WRORD
            // 
            this.IDH_WRORD.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_WRORD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WRORD.Location = new System.Drawing.Point(110, 43);
            this.IDH_WRORD.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_WRORD.Name = "IDH_WRORD";
            this.IDH_WRORD.Size = new System.Drawing.Size(55, 18);
            this.IDH_WRORD.TabIndex = 162;
            // 
            // IDH_WRROW
            // 
            this.IDH_WRROW.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_WRROW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WRROW.Location = new System.Drawing.Point(210, 43);
            this.IDH_WRROW.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_WRROW.Name = "IDH_WRROW";
            this.IDH_WRROW.Size = new System.Drawing.Size(55, 18);
            this.IDH_WRROW.TabIndex = 163;
            this.IDH_WRROW.Click += new System.EventHandler(this.IDH_WRROW_Click);
            // 
            // IDH_VEH
            // 
            this.IDH_VEH.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_VEH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_VEH.Location = new System.Drawing.Point(110, 67);
            this.IDH_VEH.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_VEH.Name = "IDH_VEH";
            this.IDH_VEH.Size = new System.Drawing.Size(155, 18);
            this.IDH_VEH.TabIndex = 165;
            // 
            // IDH_DRIVER
            // 
            this.IDH_DRIVER.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_DRIVER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DRIVER.Location = new System.Drawing.Point(110, 86);
            this.IDH_DRIVER.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_DRIVER.Name = "IDH_DRIVER";
            this.IDH_DRIVER.Size = new System.Drawing.Size(155, 18);
            this.IDH_DRIVER.TabIndex = 166;
            // 
            // IDH_TRLReg
            // 
            this.IDH_TRLReg.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_TRLReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_TRLReg.Location = new System.Drawing.Point(110, 105);
            this.IDH_TRLReg.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_TRLReg.Name = "IDH_TRLReg";
            this.IDH_TRLReg.Size = new System.Drawing.Size(155, 18);
            this.IDH_TRLReg.TabIndex = 167;
            // 
            // IDH_TRLNM
            // 
            this.IDH_TRLNM.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_TRLNM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_TRLNM.Location = new System.Drawing.Point(110, 124);
            this.IDH_TRLNM.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_TRLNM.Name = "IDH_TRLNM";
            this.IDH_TRLNM.Size = new System.Drawing.Size(155, 18);
            this.IDH_TRLNM.TabIndex = 168;
            // 
            // oMainTabs
            // 
            this.oMainTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.oMainTabs.Controls.Add(this.oTPWeighing);
            this.oMainTabs.Controls.Add(this.oTPCarrier);
            this.oMainTabs.Controls.Add(this.oTPCustomer);
            this.oMainTabs.Controls.Add(this.oTPProducer);
            this.oMainTabs.Controls.Add(this.oTPSite);
            this.oMainTabs.Controls.Add(this.oAdditional);
            this.oMainTabs.Controls.Add(this.oDeductions);
            this.oMainTabs.Location = new System.Drawing.Point(5, 226);
            this.oMainTabs.Name = "oMainTabs";
            this.oMainTabs.SelectedIndex = 0;
            this.oMainTabs.Size = new System.Drawing.Size(830, 388);
            this.oMainTabs.TabIndex = 4;
            // 
            // oTPWeighing
            // 
            this.oTPWeighing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPWeighing.Controls.Add(this.IDH_EXPLDW);
            this.oTPWeighing.Controls.Add(this.label129);
            this.oTPWeighing.Controls.Add(this.IDH_USEAU);
            this.oTPWeighing.Controls.Add(this.IDH_USERE);
            this.oTPWeighing.Controls.Add(this.PO_GRP);
            this.oTPWeighing.Controls.Add(this.SO_GRP);
            this.oTPWeighing.Controls.Add(this.IDH_ISTRL);
            this.oTPWeighing.Controls.Add(this.groupBox1);
            this.oTPWeighing.Controls.Add(this.IDH_ADJWGT);
            this.oTPWeighing.Controls.Add(this.IDH_WGTDED);
            this.oTPWeighing.Controls.Add(this.label114);
            this.oTPWeighing.Controls.Add(this.label113);
            this.oTPWeighing.Controls.Add(this.pictureBox1);
            this.oTPWeighing.Controls.Add(this.IDH_TAR2);
            this.oTPWeighing.Controls.Add(this.IDH_BUF2);
            this.oTPWeighing.Controls.Add(this.IDH_ACC2);
            this.oTPWeighing.Controls.Add(this.IDH_TAR1);
            this.oTPWeighing.Controls.Add(this.IDH_BUF1);
            this.oTPWeighing.Controls.Add(this.IDH_ACC1);
            this.oTPWeighing.Controls.Add(this.IDH_ACCB);
            this.oTPWeighing.Controls.Add(this.IDH_TMR);
            this.oTPWeighing.Controls.Add(this.IDH_REF);
            this.oTPWeighing.Controls.Add(this.label102);
            this.oTPWeighing.Controls.Add(this.label101);
            this.oTPWeighing.Controls.Add(this.label100);
            this.oTPWeighing.Controls.Add(this.label99);
            this.oTPWeighing.Controls.Add(this.label98);
            this.oTPWeighing.Controls.Add(this.label97);
            this.oTPWeighing.Controls.Add(this.label96);
            this.oTPWeighing.Controls.Add(this.label95);
            this.oTPWeighing.Controls.Add(this.label94);
            this.oTPWeighing.Controls.Add(this.IDH_CUSCM);
            this.oTPWeighing.Controls.Add(this.IDH_RDWGT);
            this.oTPWeighing.Controls.Add(this.IDH_WDT2);
            this.oTPWeighing.Controls.Add(this.IDH_SER2);
            this.oTPWeighing.Controls.Add(this.IDH_WEI2);
            this.oTPWeighing.Controls.Add(this.IDH_WDT1);
            this.oTPWeighing.Controls.Add(this.IDH_SER1);
            this.oTPWeighing.Controls.Add(this.IDH_WEI1);
            this.oTPWeighing.Controls.Add(this.IDH_TRLTar);
            this.oTPWeighing.Controls.Add(this.IDH_TARWEI);
            this.oTPWeighing.Controls.Add(this.IDH_WDTB);
            this.oTPWeighing.Controls.Add(this.IDH_SERB);
            this.oTPWeighing.Controls.Add(this.IDH_WEIB);
            this.oTPWeighing.Controls.Add(this.IDH_WEIG);
            this.oTPWeighing.Controls.Add(this.IDH_WEIBRG);
            this.oTPWeighing.Location = new System.Drawing.Point(4, 21);
            this.oTPWeighing.Name = "oTPWeighing";
            this.oTPWeighing.Padding = new System.Windows.Forms.Padding(3);
            this.oTPWeighing.Size = new System.Drawing.Size(822, 363);
            this.oTPWeighing.TabIndex = 0;
            this.oTPWeighing.Text = "Weighing";
            // 
            // IDH_EXPLDW
            // 
            this.IDH_EXPLDW.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_EXPLDW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_EXPLDW.Enabled = false;
            this.IDH_EXPLDW.Location = new System.Drawing.Point(101, 98);
            this.IDH_EXPLDW.Name = "IDH_EXPLDW";
            this.IDH_EXPLDW.Size = new System.Drawing.Size(54, 18);
            this.IDH_EXPLDW.TabIndex = 1010;
            this.IDH_EXPLDW.TabStop = false;
            this.IDH_EXPLDW.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(4, 100);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(97, 12);
            this.label129.TabIndex = 1009;
            this.label129.Text = "Expected Load Weight";
            // 
            // IDH_USEAU
            // 
            this.IDH_USEAU.AutoSize = true;
            this.IDH_USEAU.Location = new System.Drawing.Point(159, 294);
            this.IDH_USEAU.Name = "IDH_USEAU";
            this.IDH_USEAU.Size = new System.Drawing.Size(40, 16);
            this.IDH_USEAU.TabIndex = 1008;
            this.IDH_USEAU.Text = "Use";
            this.IDH_USEAU.UseVisualStyleBackColor = true;
            this.IDH_USEAU.CheckedChanged += new System.EventHandler(this.IDH_USEAU_CheckedChanged);
            // 
            // IDH_USERE
            // 
            this.IDH_USERE.AutoSize = true;
            this.IDH_USERE.Checked = true;
            this.IDH_USERE.Location = new System.Drawing.Point(159, 274);
            this.IDH_USERE.Name = "IDH_USERE";
            this.IDH_USERE.Size = new System.Drawing.Size(40, 16);
            this.IDH_USERE.TabIndex = 1007;
            this.IDH_USERE.TabStop = true;
            this.IDH_USERE.Text = "Use";
            this.IDH_USERE.UseVisualStyleBackColor = true;
            this.IDH_USERE.CheckedChanged += new System.EventHandler(this.IDH_USERE_CheckedChanged);
            // 
            // PO_GRP
            // 
            this.PO_GRP.Controls.Add(this.label128);
            this.PO_GRP.Controls.Add(this.IDH_ADDEX);
            this.PO_GRP.Controls.Add(this.label127);
            this.PO_GRP.Controls.Add(this.IDH_ADDCOS);
            this.PO_GRP.Controls.Add(this.IDH_PUOM);
            this.PO_GRP.Controls.Add(this.label118);
            this.PO_GRP.Controls.Add(this.IDH_PURUOM);
            this.PO_GRP.Controls.Add(this.label105);
            this.PO_GRP.Controls.Add(this.IDH_ORDTOT);
            this.PO_GRP.Controls.Add(this.label104);
            this.PO_GRP.Controls.Add(this.label119);
            this.PO_GRP.Controls.Add(this.IDH_PURWGT);
            this.PO_GRP.Controls.Add(this.IDH_ORDCOS);
            this.PO_GRP.Controls.Add(this.IDH_PRCOST);
            this.PO_GRP.Controls.Add(this.IDH_ORDQTY);
            this.PO_GRP.Controls.Add(this.label103);
            this.PO_GRP.Controls.Add(this.label120);
            this.PO_GRP.Controls.Add(this.IDH_PURTOT);
            this.PO_GRP.Controls.Add(this.IDH_DOPO);
            this.PO_GRP.Controls.Add(this.label121);
            this.PO_GRP.Controls.Add(this.label106);
            this.PO_GRP.Controls.Add(this.IDH_TIPTOT);
            this.PO_GRP.Controls.Add(this.IDH_TAXO);
            this.PO_GRP.Controls.Add(this.label116);
            this.PO_GRP.Controls.Add(this.label107);
            this.PO_GRP.Controls.Add(this.IDH_TIPCOS);
            this.PO_GRP.Controls.Add(this.IDH_PSTAT);
            this.PO_GRP.Controls.Add(this.IDH_TIPWEI);
            this.PO_GRP.Controls.Add(this.label108);
            this.PO_GRP.Controls.Add(this.label117);
            this.PO_GRP.Controls.Add(this.IDH_TOTCOS);
            this.PO_GRP.Enabled = false;
            this.PO_GRP.Location = new System.Drawing.Point(383, 88);
            this.PO_GRP.Name = "PO_GRP";
            this.PO_GRP.Size = new System.Drawing.Size(439, 128);
            this.PO_GRP.TabIndex = 1006;
            this.PO_GRP.TabStop = false;
            // 
            // label128
            // 
            this.label128.Location = new System.Drawing.Point(139, 89);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(70, 15);
            this.label128.TabIndex = 811;
            this.label128.Text = "Add. Expenses";
            // 
            // IDH_ADDEX
            // 
            this.IDH_ADDEX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ADDEX.Location = new System.Drawing.Point(214, 86);
            this.IDH_ADDEX.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ADDEX.Name = "IDH_ADDEX";
            this.IDH_ADDEX.Size = new System.Drawing.Size(52, 18);
            this.IDH_ADDEX.TabIndex = 812;
            this.IDH_ADDEX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label127
            // 
            this.label127.Location = new System.Drawing.Point(279, 89);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(70, 15);
            this.label127.TabIndex = 809;
            this.label127.Text = "Additional Cost";
            // 
            // IDH_ADDCOS
            // 
            this.IDH_ADDCOS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ADDCOS.Enabled = false;
            this.IDH_ADDCOS.Location = new System.Drawing.Point(368, 86);
            this.IDH_ADDCOS.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ADDCOS.Name = "IDH_ADDCOS";
            this.IDH_ADDCOS.Size = new System.Drawing.Size(68, 18);
            this.IDH_ADDCOS.TabIndex = 810;
            this.IDH_ADDCOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_PUOM
            // 
            this.IDH_PUOM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_PUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_PUOM.FormattingEnabled = true;
            this.IDH_PUOM.Location = new System.Drawing.Point(108, 9);
            this.IDH_PUOM.Name = "IDH_PUOM";
            this.IDH_PUOM.Size = new System.Drawing.Size(54, 20);
            this.IDH_PUOM.TabIndex = 808;
            // 
            // label118
            // 
            this.label118.Location = new System.Drawing.Point(3, 12);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(62, 15);
            this.label118.TabIndex = 109;
            this.label118.Text = "Disposal Qty";
            // 
            // IDH_PURUOM
            // 
            this.IDH_PURUOM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_PURUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_PURUOM.FormattingEnabled = true;
            this.IDH_PURUOM.Location = new System.Drawing.Point(108, 46);
            this.IDH_PURUOM.Name = "IDH_PURUOM";
            this.IDH_PURUOM.Size = new System.Drawing.Size(54, 20);
            this.IDH_PURUOM.TabIndex = 807;
            // 
            // label105
            // 
            this.label105.Location = new System.Drawing.Point(3, 50);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(62, 15);
            this.label105.TabIndex = 96;
            this.label105.Text = "Purchase Qty";
            // 
            // IDH_ORDTOT
            // 
            this.IDH_ORDTOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ORDTOT.Enabled = false;
            this.IDH_ORDTOT.Location = new System.Drawing.Point(368, 29);
            this.IDH_ORDTOT.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ORDTOT.Name = "IDH_ORDTOT";
            this.IDH_ORDTOT.Size = new System.Drawing.Size(68, 18);
            this.IDH_ORDTOT.TabIndex = 115;
            this.IDH_ORDTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label104
            // 
            this.label104.Location = new System.Drawing.Point(164, 51);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(44, 15);
            this.label104.TabIndex = 92;
            this.label104.Text = "Cost";
            // 
            // label119
            // 
            this.label119.Location = new System.Drawing.Point(280, 31);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(70, 15);
            this.label119.TabIndex = 114;
            this.label119.Text = "Order Cost";
            // 
            // IDH_PURWGT
            // 
            this.IDH_PURWGT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PURWGT.Location = new System.Drawing.Point(66, 47);
            this.IDH_PURWGT.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PURWGT.Name = "IDH_PURWGT";
            this.IDH_PURWGT.Size = new System.Drawing.Size(41, 18);
            this.IDH_PURWGT.TabIndex = 90;
            this.IDH_PURWGT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_ORDCOS
            // 
            this.IDH_ORDCOS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ORDCOS.Location = new System.Drawing.Point(214, 29);
            this.IDH_ORDCOS.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ORDCOS.Name = "IDH_ORDCOS";
            this.IDH_ORDCOS.Size = new System.Drawing.Size(52, 18);
            this.IDH_ORDCOS.TabIndex = 113;
            this.IDH_ORDCOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_PRCOST
            // 
            this.IDH_PRCOST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PRCOST.Location = new System.Drawing.Point(214, 48);
            this.IDH_PRCOST.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PRCOST.Name = "IDH_PRCOST";
            this.IDH_PRCOST.Size = new System.Drawing.Size(52, 18);
            this.IDH_PRCOST.TabIndex = 93;
            this.IDH_PRCOST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_ORDQTY
            // 
            this.IDH_ORDQTY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ORDQTY.Location = new System.Drawing.Point(66, 28);
            this.IDH_ORDQTY.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ORDQTY.Name = "IDH_ORDQTY";
            this.IDH_ORDQTY.Size = new System.Drawing.Size(41, 18);
            this.IDH_ORDQTY.TabIndex = 110;
            this.IDH_ORDQTY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label103
            // 
            this.label103.Location = new System.Drawing.Point(280, 51);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(70, 15);
            this.label103.TabIndex = 94;
            this.label103.Text = "Purchase Cost";
            // 
            // label120
            // 
            this.label120.Location = new System.Drawing.Point(137, 29);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(76, 15);
            this.label120.TabIndex = 112;
            this.label120.Text = "Cost ( per Unit )";
            // 
            // IDH_PURTOT
            // 
            this.IDH_PURTOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PURTOT.Enabled = false;
            this.IDH_PURTOT.Location = new System.Drawing.Point(368, 48);
            this.IDH_PURTOT.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PURTOT.Name = "IDH_PURTOT";
            this.IDH_PURTOT.Size = new System.Drawing.Size(68, 18);
            this.IDH_PURTOT.TabIndex = 95;
            this.IDH_PURTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_DOPO
            // 
            this.IDH_DOPO.AutoSize = true;
            this.IDH_DOPO.Location = new System.Drawing.Point(3, 108);
            this.IDH_DOPO.Name = "IDH_DOPO";
            this.IDH_DOPO.Size = new System.Drawing.Size(118, 16);
            this.IDH_DOPO.TabIndex = 803;
            this.IDH_DOPO.Text = "Create Purchase Order";
            this.IDH_DOPO.UseVisualStyleBackColor = true;
            this.IDH_DOPO.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // label121
            // 
            this.label121.Location = new System.Drawing.Point(3, 31);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(62, 15);
            this.label121.TabIndex = 116;
            this.label121.Text = "Order Qty";
            // 
            // label106
            // 
            this.label106.Location = new System.Drawing.Point(279, 70);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(70, 15);
            this.label106.TabIndex = 97;
            this.label106.Text = "Tax";
            // 
            // IDH_TIPTOT
            // 
            this.IDH_TIPTOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_TIPTOT.Enabled = false;
            this.IDH_TIPTOT.Location = new System.Drawing.Point(368, 10);
            this.IDH_TIPTOT.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_TIPTOT.Name = "IDH_TIPTOT";
            this.IDH_TIPTOT.Size = new System.Drawing.Size(68, 18);
            this.IDH_TIPTOT.TabIndex = 108;
            this.IDH_TIPTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_TAXO
            // 
            this.IDH_TAXO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_TAXO.Enabled = false;
            this.IDH_TAXO.Location = new System.Drawing.Point(368, 67);
            this.IDH_TAXO.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_TAXO.Name = "IDH_TAXO";
            this.IDH_TAXO.Size = new System.Drawing.Size(68, 18);
            this.IDH_TAXO.TabIndex = 98;
            this.IDH_TAXO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label116
            // 
            this.label116.Location = new System.Drawing.Point(280, 12);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(70, 15);
            this.label116.TabIndex = 107;
            this.label116.Text = "Disposal Cost";
            // 
            // label107
            // 
            this.label107.Location = new System.Drawing.Point(164, 107);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(50, 16);
            this.label107.TabIndex = 100;
            this.label107.Text = "Status";
            // 
            // IDH_TIPCOS
            // 
            this.IDH_TIPCOS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_TIPCOS.Location = new System.Drawing.Point(214, 10);
            this.IDH_TIPCOS.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_TIPCOS.Name = "IDH_TIPCOS";
            this.IDH_TIPCOS.Size = new System.Drawing.Size(52, 18);
            this.IDH_TIPCOS.TabIndex = 106;
            this.IDH_TIPCOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_PSTAT
            // 
            this.IDH_PSTAT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PSTAT.Enabled = false;
            this.IDH_PSTAT.Location = new System.Drawing.Point(214, 105);
            this.IDH_PSTAT.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PSTAT.Name = "IDH_PSTAT";
            this.IDH_PSTAT.Size = new System.Drawing.Size(52, 18);
            this.IDH_PSTAT.TabIndex = 99;
            // 
            // IDH_TIPWEI
            // 
            this.IDH_TIPWEI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_TIPWEI.Location = new System.Drawing.Point(66, 9);
            this.IDH_TIPWEI.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_TIPWEI.Name = "IDH_TIPWEI";
            this.IDH_TIPWEI.Size = new System.Drawing.Size(41, 18);
            this.IDH_TIPWEI.TabIndex = 103;
            this.IDH_TIPWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label108
            // 
            this.label108.Location = new System.Drawing.Point(280, 108);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(70, 15);
            this.label108.TabIndex = 101;
            this.label108.Text = "Total Cost";
            // 
            // label117
            // 
            this.label117.Location = new System.Drawing.Point(164, 10);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(44, 15);
            this.label117.TabIndex = 105;
            this.label117.Text = "Cost";
            // 
            // IDH_TOTCOS
            // 
            this.IDH_TOTCOS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_TOTCOS.Enabled = false;
            this.IDH_TOTCOS.Location = new System.Drawing.Point(368, 105);
            this.IDH_TOTCOS.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_TOTCOS.Name = "IDH_TOTCOS";
            this.IDH_TOTCOS.Size = new System.Drawing.Size(68, 18);
            this.IDH_TOTCOS.TabIndex = 102;
            this.IDH_TOTCOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SO_GRP
            // 
            this.SO_GRP.Controls.Add(this.IDH_UOM);
            this.SO_GRP.Controls.Add(this.label90);
            this.SO_GRP.Controls.Add(this.IDH_VALDED);
            this.SO_GRP.Controls.Add(this.label93);
            this.SO_GRP.Controls.Add(this.label115);
            this.SO_GRP.Controls.Add(this.label92);
            this.SO_GRP.Controls.Add(this.label91);
            this.SO_GRP.Controls.Add(this.IDH_CUSTOT);
            this.SO_GRP.Controls.Add(this.IDH_DOORD);
            this.SO_GRP.Controls.Add(this.IDH_DSCPRC);
            this.SO_GRP.Controls.Add(this.label89);
            this.SO_GRP.Controls.Add(this.IDH_ADDCHR);
            this.SO_GRP.Controls.Add(this.IDH_DOARIP);
            this.SO_GRP.Controls.Add(this.IDH_SUBTOT);
            this.SO_GRP.Controls.Add(this.IDH_DOARI);
            this.SO_GRP.Controls.Add(this.IDH_TAX);
            this.SO_GRP.Controls.Add(this.label88);
            this.SO_GRP.Controls.Add(this.IDH_NOVAT);
            this.SO_GRP.Controls.Add(this.IDH_CASHMT);
            this.SO_GRP.Controls.Add(this.IDH_TOTAL);
            this.SO_GRP.Controls.Add(this.IDH_FOC);
            this.SO_GRP.Controls.Add(this.label82);
            this.SO_GRP.Controls.Add(this.IDH_RSTAT);
            this.SO_GRP.Controls.Add(this.label83);
            this.SO_GRP.Controls.Add(this.IDH_DISCNT);
            this.SO_GRP.Controls.Add(this.label84);
            this.SO_GRP.Controls.Add(this.IDH_CUSWEI);
            this.SO_GRP.Controls.Add(this.label85);
            this.SO_GRP.Controls.Add(this.IDH_CUSCHG);
            this.SO_GRP.Controls.Add(this.label86);
            this.SO_GRP.Controls.Add(this.IDH_AINV);
            this.SO_GRP.Controls.Add(this.label87);
            this.SO_GRP.Location = new System.Drawing.Point(383, 213);
            this.SO_GRP.Margin = new System.Windows.Forms.Padding(2);
            this.SO_GRP.Name = "SO_GRP";
            this.SO_GRP.Padding = new System.Windows.Forms.Padding(2);
            this.SO_GRP.Size = new System.Drawing.Size(439, 146);
            this.SO_GRP.TabIndex = 1006;
            this.SO_GRP.TabStop = false;
            // 
            // IDH_UOM
            // 
            this.IDH_UOM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_UOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_UOM.FormattingEnabled = true;
            this.IDH_UOM.Location = new System.Drawing.Point(109, 8);
            this.IDH_UOM.Name = "IDH_UOM";
            this.IDH_UOM.Size = new System.Drawing.Size(54, 20);
            this.IDH_UOM.TabIndex = 806;
            // 
            // label90
            // 
            this.label90.Location = new System.Drawing.Point(4, 12);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(62, 15);
            this.label90.TabIndex = 89;
            this.label90.Text = "Cust. Qty";
            // 
            // IDH_VALDED
            // 
            this.IDH_VALDED.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_VALDED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_VALDED.Enabled = false;
            this.IDH_VALDED.Location = new System.Drawing.Point(369, 104);
            this.IDH_VALDED.Name = "IDH_VALDED";
            this.IDH_VALDED.Size = new System.Drawing.Size(67, 18);
            this.IDH_VALDED.TabIndex = 157;
            this.IDH_VALDED.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label93
            // 
            this.label93.Location = new System.Drawing.Point(165, 107);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(49, 14);
            this.label93.TabIndex = 92;
            this.label93.Text = "Pay Mth.";
            // 
            // label115
            // 
            this.label115.Location = new System.Drawing.Point(281, 105);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(70, 15);
            this.label115.TabIndex = 156;
            this.label115.Text = "Deduction";
            // 
            // label92
            // 
            this.label92.Location = new System.Drawing.Point(165, 87);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(49, 13);
            this.label92.TabIndex = 91;
            this.label92.Text = "Status";
            // 
            // label91
            // 
            this.label91.Location = new System.Drawing.Point(165, 31);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(49, 15);
            this.label91.TabIndex = 25;
            this.label91.Text = "Discount";
            // 
            // IDH_CUSTOT
            // 
            this.IDH_CUSTOT.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_CUSTOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSTOT.Enabled = false;
            this.IDH_CUSTOT.Location = new System.Drawing.Point(369, 9);
            this.IDH_CUSTOT.Name = "IDH_CUSTOT";
            this.IDH_CUSTOT.Size = new System.Drawing.Size(67, 18);
            this.IDH_CUSTOT.TabIndex = 147;
            this.IDH_CUSTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_DOORD
            // 
            this.IDH_DOORD.Location = new System.Drawing.Point(4, 60);
            this.IDH_DOORD.Name = "IDH_DOORD";
            this.IDH_DOORD.Size = new System.Drawing.Size(82, 15);
            this.IDH_DOORD.TabIndex = 800;
            this.IDH_DOORD.Text = "Account";
            this.IDH_DOORD.UseVisualStyleBackColor = true;
            this.IDH_DOORD.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // IDH_DSCPRC
            // 
            this.IDH_DSCPRC.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_DSCPRC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DSCPRC.Location = new System.Drawing.Point(369, 28);
            this.IDH_DSCPRC.Name = "IDH_DSCPRC";
            this.IDH_DSCPRC.Size = new System.Drawing.Size(67, 18);
            this.IDH_DSCPRC.TabIndex = 149;
            this.IDH_DSCPRC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label89
            // 
            this.label89.Location = new System.Drawing.Point(164, 12);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(44, 15);
            this.label89.TabIndex = 18;
            this.label89.Text = "Charge";
            // 
            // IDH_ADDCHR
            // 
            this.IDH_ADDCHR.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_ADDCHR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ADDCHR.Enabled = false;
            this.IDH_ADDCHR.Location = new System.Drawing.Point(369, 85);
            this.IDH_ADDCHR.Name = "IDH_ADDCHR";
            this.IDH_ADDCHR.Size = new System.Drawing.Size(67, 18);
            this.IDH_ADDCHR.TabIndex = 150;
            this.IDH_ADDCHR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_DOARIP
            // 
            this.IDH_DOARIP.Enabled = false;
            this.IDH_DOARIP.Location = new System.Drawing.Point(4, 91);
            this.IDH_DOARIP.Name = "IDH_DOARIP";
            this.IDH_DOARIP.Size = new System.Drawing.Size(133, 15);
            this.IDH_DOARIP.TabIndex = 802;
            this.IDH_DOARIP.Text = "AR Invoice + Payment";
            this.IDH_DOARIP.UseVisualStyleBackColor = true;
            this.IDH_DOARIP.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // IDH_SUBTOT
            // 
            this.IDH_SUBTOT.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_SUBTOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SUBTOT.Enabled = false;
            this.IDH_SUBTOT.Location = new System.Drawing.Point(369, 47);
            this.IDH_SUBTOT.Name = "IDH_SUBTOT";
            this.IDH_SUBTOT.Size = new System.Drawing.Size(67, 18);
            this.IDH_SUBTOT.TabIndex = 151;
            this.IDH_SUBTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_DOARI
            // 
            this.IDH_DOARI.Location = new System.Drawing.Point(4, 75);
            this.IDH_DOARI.Name = "IDH_DOARI";
            this.IDH_DOARI.Size = new System.Drawing.Size(80, 15);
            this.IDH_DOARI.TabIndex = 801;
            this.IDH_DOARI.Text = "Cash";
            this.IDH_DOARI.UseVisualStyleBackColor = true;
            this.IDH_DOARI.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // IDH_TAX
            // 
            this.IDH_TAX.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_TAX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_TAX.Enabled = false;
            this.IDH_TAX.Location = new System.Drawing.Point(369, 66);
            this.IDH_TAX.Name = "IDH_TAX";
            this.IDH_TAX.Size = new System.Drawing.Size(67, 18);
            this.IDH_TAX.TabIndex = 153;
            this.IDH_TAX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label88
            // 
            this.label88.Location = new System.Drawing.Point(251, 31);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(18, 23);
            this.label88.TabIndex = 27;
            this.label88.Text = "%";
            // 
            // IDH_NOVAT
            // 
            this.IDH_NOVAT.Location = new System.Drawing.Point(154, 67);
            this.IDH_NOVAT.Name = "IDH_NOVAT";
            this.IDH_NOVAT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.IDH_NOVAT.Size = new System.Drawing.Size(113, 15);
            this.IDH_NOVAT.TabIndex = 805;
            this.IDH_NOVAT.Text = "Don\'t Charge VAT  ";
            this.IDH_NOVAT.UseVisualStyleBackColor = true;
            this.IDH_NOVAT.CheckedChanged += new System.EventHandler(this.IDH_NOVATCheckedChanged);
            // 
            // IDH_CASHMT
            // 
            this.IDH_CASHMT.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_CASHMT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CASHMT.Enabled = false;
            this.IDH_CASHMT.Location = new System.Drawing.Point(215, 104);
            this.IDH_CASHMT.Name = "IDH_CASHMT";
            this.IDH_CASHMT.Size = new System.Drawing.Size(51, 18);
            this.IDH_CASHMT.TabIndex = 154;
            // 
            // IDH_TOTAL
            // 
            this.IDH_TOTAL.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_TOTAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_TOTAL.Enabled = false;
            this.IDH_TOTAL.Location = new System.Drawing.Point(369, 123);
            this.IDH_TOTAL.Name = "IDH_TOTAL";
            this.IDH_TOTAL.Size = new System.Drawing.Size(67, 18);
            this.IDH_TOTAL.TabIndex = 155;
            this.IDH_TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_FOC
            // 
            this.IDH_FOC.Location = new System.Drawing.Point(4, 123);
            this.IDH_FOC.Name = "IDH_FOC";
            this.IDH_FOC.Size = new System.Drawing.Size(74, 15);
            this.IDH_FOC.TabIndex = 804;
            this.IDH_FOC.Text = "FOC";
            this.IDH_FOC.UseVisualStyleBackColor = true;
            this.IDH_FOC.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // label82
            // 
            this.label82.Location = new System.Drawing.Point(281, 11);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(70, 15);
            this.label82.TabIndex = 20;
            this.label82.Text = "Cust. Charge";
            // 
            // IDH_RSTAT
            // 
            this.IDH_RSTAT.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_RSTAT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_RSTAT.Enabled = false;
            this.IDH_RSTAT.Location = new System.Drawing.Point(215, 85);
            this.IDH_RSTAT.Name = "IDH_RSTAT";
            this.IDH_RSTAT.Size = new System.Drawing.Size(51, 18);
            this.IDH_RSTAT.TabIndex = 152;
            // 
            // label83
            // 
            this.label83.Location = new System.Drawing.Point(281, 30);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(70, 15);
            this.label83.TabIndex = 28;
            this.label83.Text = "Amount";
            // 
            // IDH_DISCNT
            // 
            this.IDH_DISCNT.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_DISCNT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DISCNT.Location = new System.Drawing.Point(214, 28);
            this.IDH_DISCNT.Name = "IDH_DISCNT";
            this.IDH_DISCNT.Size = new System.Drawing.Size(31, 18);
            this.IDH_DISCNT.TabIndex = 148;
            this.IDH_DISCNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label84
            // 
            this.label84.Location = new System.Drawing.Point(281, 86);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(70, 15);
            this.label84.TabIndex = 30;
            this.label84.Text = "Add. Expenses";
            // 
            // IDH_CUSWEI
            // 
            this.IDH_CUSWEI.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_CUSWEI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSWEI.Location = new System.Drawing.Point(67, 9);
            this.IDH_CUSWEI.Name = "IDH_CUSWEI";
            this.IDH_CUSWEI.Size = new System.Drawing.Size(41, 18);
            this.IDH_CUSWEI.TabIndex = 142;
            this.IDH_CUSWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label85
            // 
            this.label85.Location = new System.Drawing.Point(281, 47);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(70, 15);
            this.label85.TabIndex = 32;
            this.label85.Text = "Order Subtotal";
            // 
            // IDH_CUSCHG
            // 
            this.IDH_CUSCHG.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_CUSCHG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSCHG.Location = new System.Drawing.Point(214, 9);
            this.IDH_CUSCHG.Name = "IDH_CUSCHG";
            this.IDH_CUSCHG.Size = new System.Drawing.Size(52, 18);
            this.IDH_CUSCHG.TabIndex = 146;
            this.IDH_CUSCHG.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label86
            // 
            this.label86.Location = new System.Drawing.Point(281, 68);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(70, 15);
            this.label86.TabIndex = 35;
            this.label86.Text = "Tax";
            // 
            // IDH_AINV
            // 
            this.IDH_AINV.Enabled = false;
            this.IDH_AINV.Location = new System.Drawing.Point(4, 107);
            this.IDH_AINV.Name = "IDH_AINV";
            this.IDH_AINV.Size = new System.Drawing.Size(105, 15);
            this.IDH_AINV.TabIndex = 803;
            this.IDH_AINV.Text = "Already Invoiced";
            this.IDH_AINV.UseVisualStyleBackColor = true;
            this.IDH_AINV.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // label87
            // 
            this.label87.Location = new System.Drawing.Point(281, 124);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(70, 15);
            this.label87.TabIndex = 38;
            this.label87.Text = "Total Charge";
            // 
            // IDH_ISTRL
            // 
            this.IDH_ISTRL.AutoSize = true;
            this.IDH_ISTRL.Location = new System.Drawing.Point(211, 99);
            this.IDH_ISTRL.Name = "IDH_ISTRL";
            this.IDH_ISTRL.Size = new System.Drawing.Size(63, 16);
            this.IDH_ISTRL.TabIndex = 811;
            this.IDH_ISTRL.Text = "Trail Load";
            this.IDH_ISTRL.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label126);
            this.groupBox1.Controls.Add(this.label125);
            this.groupBox1.Controls.Add(this.IDH_DISPAD);
            this.groupBox1.Controls.Add(this.IDH_DISPCF);
            this.groupBox1.Controls.Add(this.IDH_DISPNM);
            this.groupBox1.Controls.Add(this.IDH_DISPCD);
            this.groupBox1.Controls.Add(this.label124);
            this.groupBox1.Controls.Add(this.label109);
            this.groupBox1.Controls.Add(this.IDH_ITMGRN);
            this.groupBox1.Controls.Add(this.IDH_ITMGRC);
            this.groupBox1.Controls.Add(this.Lbl1000007);
            this.groupBox1.Controls.Add(this.IDH_ITMCOD);
            this.groupBox1.Controls.Add(this.IDH_DESC);
            this.groupBox1.Controls.Add(this.IDH_WASCL1);
            this.groupBox1.Controls.Add(this.IDH_WASMAT);
            this.groupBox1.Controls.Add(this.IDH_RORIGI);
            this.groupBox1.Controls.Add(this.IDH_ITCF);
            this.groupBox1.Controls.Add(this.IDH_WASCF);
            this.groupBox1.Controls.Add(this.IDH_ORLK);
            this.groupBox1.Controls.Add(this.label76);
            this.groupBox1.Controls.Add(this.label77);
            this.groupBox1.Controls.Add(this.label78);
            this.groupBox1.Controls.Add(this.IDH_PRONM);
            this.groupBox1.Controls.Add(this.label79);
            this.groupBox1.Controls.Add(this.label80);
            this.groupBox1.Controls.Add(this.label81);
            this.groupBox1.Controls.Add(this.IDH_PROCD);
            this.groupBox1.Controls.Add(this.IDH_PROCF);
            this.groupBox1.Location = new System.Drawing.Point(0, -3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(826, 91);
            this.groupBox1.TabIndex = 810;
            this.groupBox1.TabStop = false;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(555, 68);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(58, 12);
            this.label126.TabIndex = 148;
            this.label126.Text = "Site Address";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(283, 68);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(30, 12);
            this.label125.TabIndex = 147;
            this.label125.Text = "Name";
            // 
            // IDH_DISPAD
            // 
            this.IDH_DISPAD.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_DISPAD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DISPAD.Location = new System.Drawing.Point(667, 67);
            this.IDH_DISPAD.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_DISPAD.Name = "IDH_DISPAD";
            this.IDH_DISPAD.Size = new System.Drawing.Size(153, 18);
            this.IDH_DISPAD.TabIndex = 146;
            // 
            // IDH_DISPCF
            // 
            this.IDH_DISPCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_DISPCF.Image")));
            this.IDH_DISPCF.Location = new System.Drawing.Point(532, 50);
            this.IDH_DISPCF.Name = "IDH_DISPCF";
            this.IDH_DISPCF.Size = new System.Drawing.Size(16, 16);
            this.IDH_DISPCF.TabIndex = 145;
            this.IDH_DISPCF.UseVisualStyleBackColor = true;
            // 
            // IDH_DISPNM
            // 
            this.IDH_DISPNM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DISPNM.Location = new System.Drawing.Point(377, 67);
            this.IDH_DISPNM.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_DISPNM.Name = "IDH_DISPNM";
            this.IDH_DISPNM.Size = new System.Drawing.Size(155, 18);
            this.IDH_DISPNM.TabIndex = 144;
            // 
            // IDH_DISPCD
            // 
            this.IDH_DISPCD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DISPCD.Location = new System.Drawing.Point(377, 48);
            this.IDH_DISPCD.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_DISPCD.Name = "IDH_DISPCD";
            this.IDH_DISPCD.Size = new System.Drawing.Size(155, 18);
            this.IDH_DISPCD.TabIndex = 143;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(283, 49);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(59, 12);
            this.label124.TabIndex = 142;
            this.label124.Text = "Disposal Site";
            // 
            // label109
            // 
            this.label109.Location = new System.Drawing.Point(283, 30);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(90, 15);
            this.label109.TabIndex = 104;
            this.label109.Text = "Name";
            // 
            // IDH_ITMGRN
            // 
            this.IDH_ITMGRN.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_ITMGRN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ITMGRN.Enabled = false;
            this.IDH_ITMGRN.Location = new System.Drawing.Point(156, 10);
            this.IDH_ITMGRN.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ITMGRN.Name = "IDH_ITMGRN";
            this.IDH_ITMGRN.Size = new System.Drawing.Size(100, 18);
            this.IDH_ITMGRN.TabIndex = 136;
            // 
            // IDH_ITMGRC
            // 
            this.IDH_ITMGRC.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_ITMGRC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ITMGRC.Enabled = false;
            this.IDH_ITMGRC.Location = new System.Drawing.Point(100, 10);
            this.IDH_ITMGRC.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ITMGRC.Name = "IDH_ITMGRC";
            this.IDH_ITMGRC.Size = new System.Drawing.Size(55, 18);
            this.IDH_ITMGRC.TabIndex = 135;
            // 
            // Lbl1000007
            // 
            this.Lbl1000007.Location = new System.Drawing.Point(283, 11);
            this.Lbl1000007.Name = "Lbl1000007";
            this.Lbl1000007.Size = new System.Drawing.Size(90, 15);
            this.Lbl1000007.TabIndex = 103;
            this.Lbl1000007.Text = "Buying From";
            // 
            // IDH_ITMCOD
            // 
            this.IDH_ITMCOD.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_ITMCOD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ITMCOD.Location = new System.Drawing.Point(100, 29);
            this.IDH_ITMCOD.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ITMCOD.Name = "IDH_ITMCOD";
            this.IDH_ITMCOD.Size = new System.Drawing.Size(156, 18);
            this.IDH_ITMCOD.TabIndex = 5;
            // 
            // IDH_DESC
            // 
            this.IDH_DESC.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_DESC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DESC.Location = new System.Drawing.Point(100, 48);
            this.IDH_DESC.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_DESC.Name = "IDH_DESC";
            this.IDH_DESC.Size = new System.Drawing.Size(156, 18);
            this.IDH_DESC.TabIndex = 138;
            // 
            // IDH_WASCL1
            // 
            this.IDH_WASCL1.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_WASCL1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WASCL1.Location = new System.Drawing.Point(667, 10);
            this.IDH_WASCL1.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_WASCL1.Name = "IDH_WASCL1";
            this.IDH_WASCL1.Size = new System.Drawing.Size(138, 18);
            this.IDH_WASCL1.TabIndex = 6;
            // 
            // IDH_WASMAT
            // 
            this.IDH_WASMAT.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_WASMAT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WASMAT.Location = new System.Drawing.Point(667, 29);
            this.IDH_WASMAT.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_WASMAT.Name = "IDH_WASMAT";
            this.IDH_WASMAT.Size = new System.Drawing.Size(153, 18);
            this.IDH_WASMAT.TabIndex = 140;
            // 
            // IDH_RORIGI
            // 
            this.IDH_RORIGI.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_RORIGI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_RORIGI.Location = new System.Drawing.Point(667, 48);
            this.IDH_RORIGI.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_RORIGI.Name = "IDH_RORIGI";
            this.IDH_RORIGI.Size = new System.Drawing.Size(138, 18);
            this.IDH_RORIGI.TabIndex = 7;
            // 
            // IDH_ITCF
            // 
            this.IDH_ITCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ITCF.Image")));
            this.IDH_ITCF.Location = new System.Drawing.Point(256, 30);
            this.IDH_ITCF.Name = "IDH_ITCF";
            this.IDH_ITCF.Size = new System.Drawing.Size(16, 16);
            this.IDH_ITCF.TabIndex = 137;
            this.IDH_ITCF.UseVisualStyleBackColor = true;
            this.IDH_ITCF.Click += new System.EventHandler(this.IDH_ITCFClick);
            // 
            // IDH_WASCF
            // 
            this.IDH_WASCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_WASCF.Image")));
            this.IDH_WASCF.Location = new System.Drawing.Point(805, 11);
            this.IDH_WASCF.Name = "IDH_WASCF";
            this.IDH_WASCF.Size = new System.Drawing.Size(16, 16);
            this.IDH_WASCF.TabIndex = 139;
            this.IDH_WASCF.UseVisualStyleBackColor = true;
            this.IDH_WASCF.Click += new System.EventHandler(this.IDH_WASCFClick);
            // 
            // IDH_ORLK
            // 
            this.IDH_ORLK.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ORLK.Image")));
            this.IDH_ORLK.Location = new System.Drawing.Point(805, 49);
            this.IDH_ORLK.Name = "IDH_ORLK";
            this.IDH_ORLK.Size = new System.Drawing.Size(16, 16);
            this.IDH_ORLK.TabIndex = 141;
            this.IDH_ORLK.UseVisualStyleBackColor = true;
            this.IDH_ORLK.Click += new System.EventHandler(this.IDH_ORLKClick);
            // 
            // label76
            // 
            this.label76.Location = new System.Drawing.Point(5, 11);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(90, 15);
            this.label76.TabIndex = 69;
            this.label76.Text = "Container Group";
            // 
            // label77
            // 
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(5, 30);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(90, 15);
            this.label77.TabIndex = 70;
            this.label77.Text = "Container Code";
            // 
            // label78
            // 
            this.label78.Location = new System.Drawing.Point(5, 49);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(90, 15);
            this.label78.TabIndex = 71;
            this.label78.Text = "Description";
            // 
            // IDH_PRONM
            // 
            this.IDH_PRONM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PRONM.Location = new System.Drawing.Point(377, 29);
            this.IDH_PRONM.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PRONM.Name = "IDH_PRONM";
            this.IDH_PRONM.Size = new System.Drawing.Size(155, 18);
            this.IDH_PRONM.TabIndex = 65;
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(555, 11);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(100, 15);
            this.label79.TabIndex = 72;
            this.label79.Text = "Waste Code";
            // 
            // label80
            // 
            this.label80.Location = new System.Drawing.Point(555, 30);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(100, 15);
            this.label80.TabIndex = 73;
            this.label80.Text = "Waste Material";
            // 
            // label81
            // 
            this.label81.Location = new System.Drawing.Point(555, 49);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(100, 15);
            this.label81.TabIndex = 74;
            this.label81.Text = "Origin Waste";
            // 
            // IDH_PROCD
            // 
            this.IDH_PROCD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PROCD.Location = new System.Drawing.Point(377, 10);
            this.IDH_PROCD.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PROCD.Name = "IDH_PROCD";
            this.IDH_PROCD.Size = new System.Drawing.Size(155, 18);
            this.IDH_PROCD.TabIndex = 14;
            // 
            // IDH_PROCF
            // 
            this.IDH_PROCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_PROCF.Image")));
            this.IDH_PROCF.Location = new System.Drawing.Point(532, 11);
            this.IDH_PROCF.Name = "IDH_PROCF";
            this.IDH_PROCF.Size = new System.Drawing.Size(16, 16);
            this.IDH_PROCF.TabIndex = 64;
            this.IDH_PROCF.UseVisualStyleBackColor = true;
            this.IDH_PROCF.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_ADJWGT
            // 
            this.IDH_ADJWGT.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_ADJWGT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ADJWGT.Location = new System.Drawing.Point(101, 331);
            this.IDH_ADJWGT.Name = "IDH_ADJWGT";
            this.IDH_ADJWGT.Size = new System.Drawing.Size(54, 18);
            this.IDH_ADJWGT.TabIndex = 809;
            this.IDH_ADJWGT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_WGTDED
            // 
            this.IDH_WGTDED.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_WGTDED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WGTDED.Location = new System.Drawing.Point(101, 312);
            this.IDH_WGTDED.Name = "IDH_WGTDED";
            this.IDH_WGTDED.Size = new System.Drawing.Size(54, 18);
            this.IDH_WGTDED.TabIndex = 808;
            this.IDH_WGTDED.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label114
            // 
            this.label114.Location = new System.Drawing.Point(4, 333);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(90, 15);
            this.label114.TabIndex = 807;
            this.label114.Text = "Adjusted Weight";
            // 
            // label113
            // 
            this.label113.Location = new System.Drawing.Point(4, 313);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(90, 15);
            this.label113.TabIndex = 806;
            this.label113.Text = "Weight Deduction (Kg)";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(265, 279);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(77, 69);
            this.pictureBox1.TabIndex = 115;
            this.pictureBox1.TabStop = false;
            // 
            // IDH_TAR2
            // 
            this.IDH_TAR2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_TAR2.Location = new System.Drawing.Point(342, 255);
            this.IDH_TAR2.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_TAR2.Name = "IDH_TAR2";
            this.IDH_TAR2.Size = new System.Drawing.Size(37, 20);
            this.IDH_TAR2.TabIndex = 134;
            this.IDH_TAR2.Text = "Tare";
            this.IDH_TAR2.UseVisualStyleBackColor = false;
            this.IDH_TAR2.Click += new System.EventHandler(this.IDH_Tare2Click);
            // 
            // IDH_BUF2
            // 
            this.IDH_BUF2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_BUF2.Location = new System.Drawing.Point(310, 255);
            this.IDH_BUF2.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_BUF2.Name = "IDH_BUF2";
            this.IDH_BUF2.Size = new System.Drawing.Size(33, 20);
            this.IDH_BUF2.TabIndex = 133;
            this.IDH_BUF2.Text = "BU";
            this.IDH_BUF2.UseVisualStyleBackColor = false;
            // 
            // IDH_ACC2
            // 
            this.IDH_ACC2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_ACC2.Location = new System.Drawing.Point(266, 255);
            this.IDH_ACC2.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_ACC2.Name = "IDH_ACC2";
            this.IDH_ACC2.Size = new System.Drawing.Size(45, 20);
            this.IDH_ACC2.TabIndex = 500;
            this.IDH_ACC2.Text = "Bridge";
            this.IDH_ACC2.UseVisualStyleBackColor = false;
            this.IDH_ACC2.Click += new System.EventHandler(this.IDH_ACC2Click);
            // 
            // IDH_TAR1
            // 
            this.IDH_TAR1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_TAR1.Location = new System.Drawing.Point(342, 235);
            this.IDH_TAR1.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_TAR1.Name = "IDH_TAR1";
            this.IDH_TAR1.Size = new System.Drawing.Size(37, 20);
            this.IDH_TAR1.TabIndex = 129;
            this.IDH_TAR1.Text = "Tare";
            this.IDH_TAR1.UseVisualStyleBackColor = false;
            this.IDH_TAR1.Click += new System.EventHandler(this.IDH_Tare1Click);
            // 
            // IDH_BUF1
            // 
            this.IDH_BUF1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_BUF1.Location = new System.Drawing.Point(310, 235);
            this.IDH_BUF1.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_BUF1.Name = "IDH_BUF1";
            this.IDH_BUF1.Size = new System.Drawing.Size(33, 20);
            this.IDH_BUF1.TabIndex = 128;
            this.IDH_BUF1.Text = "BU";
            this.IDH_BUF1.UseVisualStyleBackColor = false;
            // 
            // IDH_ACC1
            // 
            this.IDH_ACC1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_ACC1.Location = new System.Drawing.Point(266, 235);
            this.IDH_ACC1.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_ACC1.Name = "IDH_ACC1";
            this.IDH_ACC1.Size = new System.Drawing.Size(45, 20);
            this.IDH_ACC1.TabIndex = 400;
            this.IDH_ACC1.Text = "Bridge";
            this.IDH_ACC1.UseVisualStyleBackColor = false;
            this.IDH_ACC1.Click += new System.EventHandler(this.IDH_ACC1Click);
            // 
            // IDH_ACCB
            // 
            this.IDH_ACCB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_ACCB.Location = new System.Drawing.Point(266, 179);
            this.IDH_ACCB.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_ACCB.Name = "IDH_ACCB";
            this.IDH_ACCB.Size = new System.Drawing.Size(113, 20);
            this.IDH_ACCB.TabIndex = 121;
            this.IDH_ACCB.Text = "buffer";
            this.IDH_ACCB.UseVisualStyleBackColor = false;
            this.IDH_ACCB.Click += new System.EventHandler(this.IDH_ACCBClick);
            // 
            // IDH_TMR
            // 
            this.IDH_TMR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_TMR.Location = new System.Drawing.Point(266, 160);
            this.IDH_TMR.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_TMR.Name = "IDH_TMR";
            this.IDH_TMR.Size = new System.Drawing.Size(113, 20);
            this.IDH_TMR.TabIndex = 120;
            this.IDH_TMR.Text = "Start Reading";
            this.IDH_TMR.UseVisualStyleBackColor = false;
            this.IDH_TMR.Click += new System.EventHandler(this.IDH_TMRClick);
            // 
            // IDH_REF
            // 
            this.IDH_REF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_REF.Location = new System.Drawing.Point(266, 141);
            this.IDH_REF.Margin = new System.Windows.Forms.Padding(0);
            this.IDH_REF.Name = "IDH_REF";
            this.IDH_REF.Size = new System.Drawing.Size(113, 20);
            this.IDH_REF.TabIndex = 119;
            this.IDH_REF.Text = "Read";
            this.IDH_REF.UseVisualStyleBackColor = false;
            this.IDH_REF.Click += new System.EventHandler(this.IDH_REFClick);
            // 
            // label102
            // 
            this.label102.Location = new System.Drawing.Point(4, 295);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(90, 15);
            this.label102.TabIndex = 101;
            this.label102.Text = "Units";
            // 
            // label101
            // 
            this.label101.Location = new System.Drawing.Point(4, 277);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(90, 15);
            this.label101.TabIndex = 100;
            this.label101.Text = "Net Weight (Kg)";
            // 
            // label100
            // 
            this.label100.Location = new System.Drawing.Point(4, 258);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(90, 15);
            this.label100.TabIndex = 99;
            this.label100.Text = "2nd Weigh (Kg)";
            // 
            // label99
            // 
            this.label99.Location = new System.Drawing.Point(4, 239);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(90, 15);
            this.label99.TabIndex = 98;
            this.label99.Text = "1st Weigh (Kg)";
            // 
            // label98
            // 
            this.label98.Location = new System.Drawing.Point(4, 220);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(90, 15);
            this.label98.TabIndex = 97;
            this.label98.Text = "2nd Tare Weight (kg)";
            // 
            // label97
            // 
            this.label97.Location = new System.Drawing.Point(4, 201);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(90, 15);
            this.label97.TabIndex = 96;
            this.label97.Text = "Vehicle Tare Weight";
            // 
            // label96
            // 
            this.label96.Location = new System.Drawing.Point(4, 183);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(90, 15);
            this.label96.TabIndex = 95;
            this.label96.Text = "Weight Buffer (Kg)";
            // 
            // label95
            // 
            this.label95.Location = new System.Drawing.Point(4, 153);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(90, 15);
            this.label95.TabIndex = 94;
            this.label95.Text = "Current Reading";
            // 
            // label94
            // 
            this.label94.Location = new System.Drawing.Point(4, 123);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(90, 15);
            this.label94.TabIndex = 93;
            this.label94.Text = "Weighbridge";
            // 
            // IDH_CUSCM
            // 
            this.IDH_CUSCM.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_CUSCM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSCM.Location = new System.Drawing.Point(101, 293);
            this.IDH_CUSCM.Name = "IDH_CUSCM";
            this.IDH_CUSCM.Size = new System.Drawing.Size(54, 18);
            this.IDH_CUSCM.TabIndex = 9;
            this.IDH_CUSCM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_RDWGT
            // 
            this.IDH_RDWGT.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_RDWGT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_RDWGT.Location = new System.Drawing.Point(101, 274);
            this.IDH_RDWGT.Name = "IDH_RDWGT";
            this.IDH_RDWGT.Size = new System.Drawing.Size(54, 18);
            this.IDH_RDWGT.TabIndex = 6;
            this.IDH_RDWGT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_WDT2
            // 
            this.IDH_WDT2.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_WDT2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WDT2.Enabled = false;
            this.IDH_WDT2.Location = new System.Drawing.Point(211, 255);
            this.IDH_WDT2.Name = "IDH_WDT2";
            this.IDH_WDT2.Size = new System.Drawing.Size(54, 18);
            this.IDH_WDT2.TabIndex = 131;
            // 
            // IDH_SER2
            // 
            this.IDH_SER2.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_SER2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SER2.Enabled = false;
            this.IDH_SER2.Location = new System.Drawing.Point(156, 255);
            this.IDH_SER2.Name = "IDH_SER2";
            this.IDH_SER2.Size = new System.Drawing.Size(54, 18);
            this.IDH_SER2.TabIndex = 130;
            // 
            // IDH_WEI2
            // 
            this.IDH_WEI2.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_WEI2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WEI2.Location = new System.Drawing.Point(101, 255);
            this.IDH_WEI2.Name = "IDH_WEI2";
            this.IDH_WEI2.Size = new System.Drawing.Size(54, 18);
            this.IDH_WEI2.TabIndex = 5;
            this.IDH_WEI2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_WDT1
            // 
            this.IDH_WDT1.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_WDT1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WDT1.Enabled = false;
            this.IDH_WDT1.Location = new System.Drawing.Point(211, 236);
            this.IDH_WDT1.Name = "IDH_WDT1";
            this.IDH_WDT1.Size = new System.Drawing.Size(54, 18);
            this.IDH_WDT1.TabIndex = 126;
            // 
            // IDH_SER1
            // 
            this.IDH_SER1.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_SER1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SER1.Enabled = false;
            this.IDH_SER1.Location = new System.Drawing.Point(156, 236);
            this.IDH_SER1.Name = "IDH_SER1";
            this.IDH_SER1.Size = new System.Drawing.Size(54, 18);
            this.IDH_SER1.TabIndex = 125;
            // 
            // IDH_WEI1
            // 
            this.IDH_WEI1.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_WEI1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WEI1.Location = new System.Drawing.Point(101, 236);
            this.IDH_WEI1.Name = "IDH_WEI1";
            this.IDH_WEI1.Size = new System.Drawing.Size(54, 18);
            this.IDH_WEI1.TabIndex = 4;
            this.IDH_WEI1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_TRLTar
            // 
            this.IDH_TRLTar.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_TRLTar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_TRLTar.Location = new System.Drawing.Point(101, 217);
            this.IDH_TRLTar.Name = "IDH_TRLTar";
            this.IDH_TRLTar.Size = new System.Drawing.Size(54, 18);
            this.IDH_TRLTar.TabIndex = 124;
            this.IDH_TRLTar.Text = "0.00";
            this.IDH_TRLTar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_TARWEI
            // 
            this.IDH_TARWEI.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_TARWEI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_TARWEI.Location = new System.Drawing.Point(101, 198);
            this.IDH_TARWEI.Name = "IDH_TARWEI";
            this.IDH_TARWEI.Size = new System.Drawing.Size(54, 18);
            this.IDH_TARWEI.TabIndex = 123;
            this.IDH_TARWEI.Text = "0.00";
            this.IDH_TARWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_WDTB
            // 
            this.IDH_WDTB.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_WDTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WDTB.Enabled = false;
            this.IDH_WDTB.Location = new System.Drawing.Point(211, 179);
            this.IDH_WDTB.Name = "IDH_WDTB";
            this.IDH_WDTB.Size = new System.Drawing.Size(54, 18);
            this.IDH_WDTB.TabIndex = 4;
            this.IDH_WDTB.TabStop = false;
            this.IDH_WDTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_SERB
            // 
            this.IDH_SERB.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_SERB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SERB.Enabled = false;
            this.IDH_SERB.Location = new System.Drawing.Point(156, 179);
            this.IDH_SERB.Name = "IDH_SERB";
            this.IDH_SERB.Size = new System.Drawing.Size(54, 18);
            this.IDH_SERB.TabIndex = 3;
            this.IDH_SERB.TabStop = false;
            this.IDH_SERB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_WEIB
            // 
            this.IDH_WEIB.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_WEIB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WEIB.Enabled = false;
            this.IDH_WEIB.Location = new System.Drawing.Point(101, 179);
            this.IDH_WEIB.Name = "IDH_WEIB";
            this.IDH_WEIB.Size = new System.Drawing.Size(54, 18);
            this.IDH_WEIB.TabIndex = 2;
            this.IDH_WEIB.TabStop = false;
            this.IDH_WEIB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IDH_WEIG
            // 
            this.IDH_WEIG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.IDH_WEIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WEIG.Enabled = false;
            this.IDH_WEIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WEIG.ForeColor = System.Drawing.Color.MidnightBlue;
            this.IDH_WEIG.Location = new System.Drawing.Point(101, 141);
            this.IDH_WEIG.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_WEIG.Name = "IDH_WEIG";
            this.IDH_WEIG.Size = new System.Drawing.Size(164, 38);
            this.IDH_WEIG.TabIndex = 1;
            this.IDH_WEIG.TabStop = false;
            this.IDH_WEIG.Text = "Waiting";
            this.IDH_WEIG.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // IDH_WEIBRG
            // 
            this.IDH_WEIBRG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_WEIBRG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_WEIBRG.FormattingEnabled = true;
            this.IDH_WEIBRG.Location = new System.Drawing.Point(101, 120);
            this.IDH_WEIBRG.Name = "IDH_WEIBRG";
            this.IDH_WEIBRG.Size = new System.Drawing.Size(164, 20);
            this.IDH_WEIBRG.TabIndex = 118;
            this.IDH_WEIBRG.SelectedIndexChanged += new System.EventHandler(this.IDH_WEIBRGSelectedIndexChanged);
            // 
            // oTPCarrier
            // 
            this.oTPCarrier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPCarrier.Controls.Add(this.IDH_LICREG);
            this.oTPCarrier.Controls.Add(this.label54);
            this.oTPCarrier.Controls.Add(this.IDH_PHONE);
            this.oTPCarrier.Controls.Add(this.label53);
            this.oTPCarrier.Controls.Add(this.IDH_ZIP);
            this.oTPCarrier.Controls.Add(this.label52);
            this.oTPCarrier.Controls.Add(this.IDH_CITY);
            this.oTPCarrier.Controls.Add(this.label51);
            this.oTPCarrier.Controls.Add(this.IDH_BLOCK);
            this.oTPCarrier.Controls.Add(this.label50);
            this.oTPCarrier.Controls.Add(this.IDH_WASAL);
            this.oTPCarrier.Controls.Add(this.IDH_ADDRES);
            this.oTPCarrier.Controls.Add(this.label49);
            this.oTPCarrier.Controls.Add(this.IDH_STREET);
            this.oTPCarrier.Controls.Add(this.label48);
            this.oTPCarrier.Controls.Add(this.IDH_CARREF);
            this.oTPCarrier.Controls.Add(this.label47);
            this.oTPCarrier.Controls.Add(this.IDH_CONTNM);
            this.oTPCarrier.Controls.Add(this.label46);
            this.oTPCarrier.Location = new System.Drawing.Point(4, 21);
            this.oTPCarrier.Name = "oTPCarrier";
            this.oTPCarrier.Padding = new System.Windows.Forms.Padding(3);
            this.oTPCarrier.Size = new System.Drawing.Size(822, 363);
            this.oTPCarrier.TabIndex = 1;
            this.oTPCarrier.Text = "Carrier";
            // 
            // IDH_LICREG
            // 
            this.IDH_LICREG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_LICREG.Location = new System.Drawing.Point(130, 171);
            this.IDH_LICREG.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_LICREG.Name = "IDH_LICREG";
            this.IDH_LICREG.Size = new System.Drawing.Size(155, 18);
            this.IDH_LICREG.TabIndex = 8;
            // 
            // label54
            // 
            this.label54.Location = new System.Drawing.Point(4, 174);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(100, 17);
            this.label54.TabIndex = 107;
            this.label54.Text = "Waste Lic. Reg. No.";
            // 
            // IDH_PHONE
            // 
            this.IDH_PHONE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PHONE.Location = new System.Drawing.Point(130, 152);
            this.IDH_PHONE.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PHONE.Name = "IDH_PHONE";
            this.IDH_PHONE.Size = new System.Drawing.Size(155, 18);
            this.IDH_PHONE.TabIndex = 7;
            // 
            // label53
            // 
            this.label53.Location = new System.Drawing.Point(4, 155);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(100, 17);
            this.label53.TabIndex = 105;
            this.label53.Text = "Phone No.";
            // 
            // IDH_ZIP
            // 
            this.IDH_ZIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ZIP.Location = new System.Drawing.Point(130, 133);
            this.IDH_ZIP.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ZIP.Name = "IDH_ZIP";
            this.IDH_ZIP.Size = new System.Drawing.Size(155, 18);
            this.IDH_ZIP.TabIndex = 6;
            // 
            // label52
            // 
            this.label52.Location = new System.Drawing.Point(4, 136);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(100, 17);
            this.label52.TabIndex = 103;
            this.label52.Text = "Post Code";
            // 
            // IDH_CITY
            // 
            this.IDH_CITY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CITY.Location = new System.Drawing.Point(130, 114);
            this.IDH_CITY.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CITY.Name = "IDH_CITY";
            this.IDH_CITY.Size = new System.Drawing.Size(155, 18);
            this.IDH_CITY.TabIndex = 5;
            // 
            // label51
            // 
            this.label51.Location = new System.Drawing.Point(4, 117);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(100, 17);
            this.label51.TabIndex = 101;
            this.label51.Text = "City";
            // 
            // IDH_BLOCK
            // 
            this.IDH_BLOCK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_BLOCK.Location = new System.Drawing.Point(130, 95);
            this.IDH_BLOCK.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_BLOCK.Name = "IDH_BLOCK";
            this.IDH_BLOCK.Size = new System.Drawing.Size(155, 18);
            this.IDH_BLOCK.TabIndex = 4;
            // 
            // label50
            // 
            this.label50.Location = new System.Drawing.Point(4, 98);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(100, 17);
            this.label50.TabIndex = 99;
            this.label50.Text = "Block";
            // 
            // IDH_WASAL
            // 
            this.IDH_WASAL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_WASAL.Image")));
            this.IDH_WASAL.Location = new System.Drawing.Point(285, 59);
            this.IDH_WASAL.Name = "IDH_WASAL";
            this.IDH_WASAL.Size = new System.Drawing.Size(16, 16);
            this.IDH_WASAL.TabIndex = 98;
            this.IDH_WASAL.UseVisualStyleBackColor = true;
            this.IDH_WASAL.Click += new System.EventHandler(this.AddressSearchClick);
            // 
            // IDH_ADDRES
            // 
            this.IDH_ADDRES.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ADDRES.Location = new System.Drawing.Point(130, 57);
            this.IDH_ADDRES.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ADDRES.Name = "IDH_ADDRES";
            this.IDH_ADDRES.Size = new System.Drawing.Size(155, 18);
            this.IDH_ADDRES.TabIndex = 2;
            // 
            // label49
            // 
            this.label49.Location = new System.Drawing.Point(4, 60);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(100, 20);
            this.label49.TabIndex = 96;
            this.label49.Text = "Address";
            // 
            // IDH_STREET
            // 
            this.IDH_STREET.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_STREET.Location = new System.Drawing.Point(130, 76);
            this.IDH_STREET.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_STREET.Name = "IDH_STREET";
            this.IDH_STREET.Size = new System.Drawing.Size(155, 18);
            this.IDH_STREET.TabIndex = 3;
            // 
            // label48
            // 
            this.label48.Location = new System.Drawing.Point(4, 79);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(100, 17);
            this.label48.TabIndex = 86;
            this.label48.Text = "Street";
            // 
            // IDH_CARREF
            // 
            this.IDH_CARREF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CARREF.Location = new System.Drawing.Point(130, 34);
            this.IDH_CARREF.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CARREF.Name = "IDH_CARREF";
            this.IDH_CARREF.Size = new System.Drawing.Size(155, 18);
            this.IDH_CARREF.TabIndex = 1;
            // 
            // label47
            // 
            this.label47.Location = new System.Drawing.Point(4, 37);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(123, 18);
            this.label47.TabIndex = 2;
            this.label47.Text = "Waste Carrier Ref. No.";
            // 
            // IDH_CONTNM
            // 
            this.IDH_CONTNM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CONTNM.Location = new System.Drawing.Point(130, 15);
            this.IDH_CONTNM.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CONTNM.Name = "IDH_CONTNM";
            this.IDH_CONTNM.Size = new System.Drawing.Size(155, 18);
            this.IDH_CONTNM.TabIndex = 0;
            // 
            // label46
            // 
            this.label46.Location = new System.Drawing.Point(4, 18);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(100, 18);
            this.label46.TabIndex = 0;
            this.label46.Text = "Contact Person";
            // 
            // oTPCustomer
            // 
            this.oTPCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPCustomer.Controls.Add(this.IDH_ORIGLU2);
            this.oTPCustomer.Controls.Add(this.IDH_ORIGIN2);
            this.oTPCustomer.Controls.Add(this.label110);
            this.oTPCustomer.Controls.Add(this.label43);
            this.oTPCustomer.Controls.Add(this.label44);
            this.oTPCustomer.Controls.Add(this.label45);
            this.oTPCustomer.Controls.Add(this.IDH_SLCNO);
            this.oTPCustomer.Controls.Add(this.IDH_PCD);
            this.oTPCustomer.Controls.Add(this.IDH_SteId);
            this.oTPCustomer.Controls.Add(this.IDH_ROUTL);
            this.oTPCustomer.Controls.Add(this.label42);
            this.oTPCustomer.Controls.Add(this.IDH_SEQ);
            this.oTPCustomer.Controls.Add(this.IDH_ROUTE);
            this.oTPCustomer.Controls.Add(this.label41);
            this.oTPCustomer.Controls.Add(this.IDH_CNA);
            this.oTPCustomer.Controls.Add(this.IDH_CUSAL);
            this.oTPCustomer.Controls.Add(this.label40);
            this.oTPCustomer.Controls.Add(this.IDH_CUSLIC);
            this.oTPCustomer.Controls.Add(this.IDH_SITETL);
            this.oTPCustomer.Controls.Add(this.label39);
            this.oTPCustomer.Controls.Add(this.IDH_CUSPHO);
            this.oTPCustomer.Controls.Add(this.IDH_CUSPOS);
            this.oTPCustomer.Controls.Add(this.IDH_COUNTY);
            this.oTPCustomer.Controls.Add(this.IDH_CUSCIT);
            this.oTPCustomer.Controls.Add(this.IDH_CUSBLO);
            this.oTPCustomer.Controls.Add(this.IDH_CUSSTR);
            this.oTPCustomer.Controls.Add(this.IDH_CUSADD);
            this.oTPCustomer.Controls.Add(this.IDH_CUSCON);
            this.oTPCustomer.Controls.Add(this.label38);
            this.oTPCustomer.Controls.Add(this.label37);
            this.oTPCustomer.Controls.Add(this.label36);
            this.oTPCustomer.Controls.Add(this.label35);
            this.oTPCustomer.Controls.Add(this.label34);
            this.oTPCustomer.Controls.Add(this.label33);
            this.oTPCustomer.Controls.Add(this.label32);
            this.oTPCustomer.Controls.Add(this.label31);
            this.oTPCustomer.Location = new System.Drawing.Point(4, 21);
            this.oTPCustomer.Name = "oTPCustomer";
            this.oTPCustomer.Size = new System.Drawing.Size(822, 363);
            this.oTPCustomer.TabIndex = 2;
            this.oTPCustomer.Text = "Customer";
            // 
            // IDH_ORIGLU2
            // 
            this.IDH_ORIGLU2.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ORIGLU2.Image")));
            this.IDH_ORIGLU2.Location = new System.Drawing.Point(285, 256);
            this.IDH_ORIGLU2.Name = "IDH_ORIGLU2";
            this.IDH_ORIGLU2.Size = new System.Drawing.Size(16, 16);
            this.IDH_ORIGLU2.TabIndex = 117;
            this.IDH_ORIGLU2.UseVisualStyleBackColor = true;
            this.IDH_ORIGLU2.Click += new System.EventHandler(this.IDH_ORLKClick);
            // 
            // IDH_ORIGIN2
            // 
            this.IDH_ORIGIN2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ORIGIN2.Location = new System.Drawing.Point(130, 254);
            this.IDH_ORIGIN2.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ORIGIN2.Name = "IDH_ORIGIN2";
            this.IDH_ORIGIN2.Size = new System.Drawing.Size(155, 18);
            this.IDH_ORIGIN2.TabIndex = 115;
            // 
            // label110
            // 
            this.label110.Location = new System.Drawing.Point(4, 257);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(100, 23);
            this.label110.TabIndex = 116;
            this.label110.Text = "Origin";
            // 
            // label43
            // 
            this.label43.Location = new System.Drawing.Point(309, 234);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(100, 23);
            this.label43.TabIndex = 107;
            this.label43.Text = "Site Licence";
            // 
            // label44
            // 
            this.label44.Location = new System.Drawing.Point(309, 215);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(95, 23);
            this.label44.TabIndex = 106;
            this.label44.Text = "Premesis Code";
            // 
            // label45
            // 
            this.label45.Location = new System.Drawing.Point(309, 192);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(108, 23);
            this.label45.TabIndex = 105;
            this.label45.Text = "Site Id";
            // 
            // IDH_SLCNO
            // 
            this.IDH_SLCNO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SLCNO.Enabled = false;
            this.IDH_SLCNO.Location = new System.Drawing.Point(417, 231);
            this.IDH_SLCNO.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SLCNO.Name = "IDH_SLCNO";
            this.IDH_SLCNO.Size = new System.Drawing.Size(244, 18);
            this.IDH_SLCNO.TabIndex = 14;
            // 
            // IDH_PCD
            // 
            this.IDH_PCD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PCD.Enabled = false;
            this.IDH_PCD.Location = new System.Drawing.Point(417, 212);
            this.IDH_PCD.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PCD.Name = "IDH_PCD";
            this.IDH_PCD.Size = new System.Drawing.Size(244, 18);
            this.IDH_PCD.TabIndex = 12;
            // 
            // IDH_SteId
            // 
            this.IDH_SteId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SteId.Enabled = false;
            this.IDH_SteId.Location = new System.Drawing.Point(417, 189);
            this.IDH_SteId.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SteId.Name = "IDH_SteId";
            this.IDH_SteId.Size = new System.Drawing.Size(244, 18);
            this.IDH_SteId.TabIndex = 10;
            // 
            // IDH_ROUTL
            // 
            this.IDH_ROUTL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ROUTL.Image")));
            this.IDH_ROUTL.Location = new System.Drawing.Point(285, 214);
            this.IDH_ROUTL.Name = "IDH_ROUTL";
            this.IDH_ROUTL.Size = new System.Drawing.Size(16, 16);
            this.IDH_ROUTL.TabIndex = 101;
            this.IDH_ROUTL.UseVisualStyleBackColor = true;
            // 
            // label42
            // 
            this.label42.Location = new System.Drawing.Point(4, 234);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(100, 23);
            this.label42.TabIndex = 100;
            this.label42.Text = "Sequence";
            // 
            // IDH_SEQ
            // 
            this.IDH_SEQ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SEQ.Enabled = false;
            this.IDH_SEQ.Location = new System.Drawing.Point(130, 231);
            this.IDH_SEQ.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SEQ.Name = "IDH_SEQ";
            this.IDH_SEQ.Size = new System.Drawing.Size(155, 18);
            this.IDH_SEQ.TabIndex = 13;
            // 
            // IDH_ROUTE
            // 
            this.IDH_ROUTE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ROUTE.Enabled = false;
            this.IDH_ROUTE.Location = new System.Drawing.Point(130, 212);
            this.IDH_ROUTE.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ROUTE.Name = "IDH_ROUTE";
            this.IDH_ROUTE.Size = new System.Drawing.Size(155, 18);
            this.IDH_ROUTE.TabIndex = 11;
            // 
            // label41
            // 
            this.label41.Location = new System.Drawing.Point(4, 215);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(100, 23);
            this.label41.TabIndex = 97;
            this.label41.Text = "Route";
            // 
            // IDH_CNA
            // 
            this.IDH_CNA.Location = new System.Drawing.Point(417, 46);
            this.IDH_CNA.Name = "IDH_CNA";
            this.IDH_CNA.Size = new System.Drawing.Size(135, 23);
            this.IDH_CNA.TabIndex = 96;
            this.IDH_CNA.Text = "Create &New Address";
            this.IDH_CNA.UseVisualStyleBackColor = true;
            this.IDH_CNA.Click += new System.EventHandler(this.IDH_CNA_Click);
            // 
            // IDH_CUSAL
            // 
            this.IDH_CUSAL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_CUSAL.Image")));
            this.IDH_CUSAL.Location = new System.Drawing.Point(285, 39);
            this.IDH_CUSAL.Name = "IDH_CUSAL";
            this.IDH_CUSAL.Size = new System.Drawing.Size(16, 16);
            this.IDH_CUSAL.TabIndex = 95;
            this.IDH_CUSAL.UseVisualStyleBackColor = true;
            this.IDH_CUSAL.Click += new System.EventHandler(this.AddressSearchClick);
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(4, 192);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(106, 23);
            this.label40.TabIndex = 94;
            this.label40.Text = "Waste Lic. Reg. No.";
            // 
            // IDH_CUSLIC
            // 
            this.IDH_CUSLIC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSLIC.Enabled = false;
            this.IDH_CUSLIC.Location = new System.Drawing.Point(130, 189);
            this.IDH_CUSLIC.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CUSLIC.Name = "IDH_CUSLIC";
            this.IDH_CUSLIC.Size = new System.Drawing.Size(155, 18);
            this.IDH_CUSLIC.TabIndex = 9;
            // 
            // IDH_SITETL
            // 
            this.IDH_SITETL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SITETL.Location = new System.Drawing.Point(130, 170);
            this.IDH_SITETL.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SITETL.Name = "IDH_SITETL";
            this.IDH_SITETL.Size = new System.Drawing.Size(155, 18);
            this.IDH_SITETL.TabIndex = 8;
            // 
            // label39
            // 
            this.label39.Location = new System.Drawing.Point(4, 173);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(100, 23);
            this.label39.TabIndex = 91;
            this.label39.Text = "Site Phone No.";
            // 
            // IDH_CUSPHO
            // 
            this.IDH_CUSPHO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSPHO.Location = new System.Drawing.Point(130, 151);
            this.IDH_CUSPHO.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CUSPHO.Name = "IDH_CUSPHO";
            this.IDH_CUSPHO.Size = new System.Drawing.Size(155, 18);
            this.IDH_CUSPHO.TabIndex = 7;
            // 
            // IDH_CUSPOS
            // 
            this.IDH_CUSPOS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSPOS.Location = new System.Drawing.Point(130, 132);
            this.IDH_CUSPOS.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CUSPOS.Name = "IDH_CUSPOS";
            this.IDH_CUSPOS.Size = new System.Drawing.Size(155, 18);
            this.IDH_CUSPOS.TabIndex = 6;
            // 
            // IDH_COUNTY
            // 
            this.IDH_COUNTY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_COUNTY.Location = new System.Drawing.Point(130, 113);
            this.IDH_COUNTY.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_COUNTY.Name = "IDH_COUNTY";
            this.IDH_COUNTY.Size = new System.Drawing.Size(155, 18);
            this.IDH_COUNTY.TabIndex = 5;
            // 
            // IDH_CUSCIT
            // 
            this.IDH_CUSCIT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSCIT.Location = new System.Drawing.Point(130, 94);
            this.IDH_CUSCIT.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CUSCIT.Name = "IDH_CUSCIT";
            this.IDH_CUSCIT.Size = new System.Drawing.Size(155, 18);
            this.IDH_CUSCIT.TabIndex = 4;
            // 
            // IDH_CUSBLO
            // 
            this.IDH_CUSBLO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSBLO.Location = new System.Drawing.Point(130, 75);
            this.IDH_CUSBLO.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CUSBLO.Name = "IDH_CUSBLO";
            this.IDH_CUSBLO.Size = new System.Drawing.Size(155, 18);
            this.IDH_CUSBLO.TabIndex = 3;
            // 
            // IDH_CUSSTR
            // 
            this.IDH_CUSSTR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSSTR.Location = new System.Drawing.Point(130, 56);
            this.IDH_CUSSTR.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CUSSTR.Name = "IDH_CUSSTR";
            this.IDH_CUSSTR.Size = new System.Drawing.Size(155, 18);
            this.IDH_CUSSTR.TabIndex = 2;
            // 
            // IDH_CUSADD
            // 
            this.IDH_CUSADD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSADD.Location = new System.Drawing.Point(130, 37);
            this.IDH_CUSADD.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CUSADD.Name = "IDH_CUSADD";
            this.IDH_CUSADD.Size = new System.Drawing.Size(155, 18);
            this.IDH_CUSADD.TabIndex = 1;
            // 
            // IDH_CUSCON
            // 
            this.IDH_CUSCON.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSCON.Location = new System.Drawing.Point(130, 15);
            this.IDH_CUSCON.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CUSCON.Name = "IDH_CUSCON";
            this.IDH_CUSCON.Size = new System.Drawing.Size(155, 18);
            this.IDH_CUSCON.TabIndex = 0;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(4, 154);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(100, 23);
            this.label38.TabIndex = 82;
            this.label38.Text = "Phone No.";
            // 
            // label37
            // 
            this.label37.Location = new System.Drawing.Point(4, 135);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(100, 23);
            this.label37.TabIndex = 81;
            this.label37.Text = "Post Code";
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(4, 115);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(100, 23);
            this.label36.TabIndex = 80;
            this.label36.Text = "County";
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(4, 96);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(100, 23);
            this.label35.TabIndex = 79;
            this.label35.Text = "City";
            // 
            // label34
            // 
            this.label34.Location = new System.Drawing.Point(4, 78);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(100, 23);
            this.label34.TabIndex = 78;
            this.label34.Text = "Block";
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(4, 56);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(100, 23);
            this.label33.TabIndex = 77;
            this.label33.Text = "Street";
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(4, 37);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(100, 23);
            this.label32.TabIndex = 76;
            this.label32.Text = "Address";
            // 
            // label31
            // 
            this.label31.Location = new System.Drawing.Point(4, 18);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(100, 18);
            this.label31.TabIndex = 75;
            this.label31.Text = "Contact Person";
            // 
            // oTPProducer
            // 
            this.oTPProducer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPProducer.Controls.Add(this.IDH_PROAL);
            this.oTPProducer.Controls.Add(this.IDH_ORIGLU);
            this.oTPProducer.Controls.Add(this.IDH_PRDL);
            this.oTPProducer.Controls.Add(this.IDH_ORIGIN);
            this.oTPProducer.Controls.Add(this.label65);
            this.oTPProducer.Controls.Add(this.IDH_PRDPHO);
            this.oTPProducer.Controls.Add(this.IDH_PRDPOS);
            this.oTPProducer.Controls.Add(this.IDH_PRDCIT);
            this.oTPProducer.Controls.Add(this.label62);
            this.oTPProducer.Controls.Add(this.label63);
            this.oTPProducer.Controls.Add(this.label64);
            this.oTPProducer.Controls.Add(this.IDH_PRDBLO);
            this.oTPProducer.Controls.Add(this.IDH_PRDSTR);
            this.oTPProducer.Controls.Add(this.IDH_PRDADD);
            this.oTPProducer.Controls.Add(this.IDH_PRDCRF);
            this.oTPProducer.Controls.Add(this.IDH_PRDCON);
            this.oTPProducer.Controls.Add(this.IDH_WNAM);
            this.oTPProducer.Controls.Add(this.IDH_WPRODU);
            this.oTPProducer.Controls.Add(this.label55);
            this.oTPProducer.Controls.Add(this.label56);
            this.oTPProducer.Controls.Add(this.label57);
            this.oTPProducer.Controls.Add(this.label58);
            this.oTPProducer.Controls.Add(this.label59);
            this.oTPProducer.Controls.Add(this.label60);
            this.oTPProducer.Controls.Add(this.label61);
            this.oTPProducer.Location = new System.Drawing.Point(4, 21);
            this.oTPProducer.Name = "oTPProducer";
            this.oTPProducer.Size = new System.Drawing.Size(822, 363);
            this.oTPProducer.TabIndex = 3;
            this.oTPProducer.Text = "Producer";
            // 
            // IDH_PROAL
            // 
            this.IDH_PROAL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_PROAL.Image")));
            this.IDH_PROAL.Location = new System.Drawing.Point(287, 97);
            this.IDH_PROAL.Name = "IDH_PROAL";
            this.IDH_PROAL.Size = new System.Drawing.Size(16, 16);
            this.IDH_PROAL.TabIndex = 115;
            this.IDH_PROAL.UseVisualStyleBackColor = true;
            this.IDH_PROAL.Click += new System.EventHandler(this.AddressSearchClick);
            // 
            // IDH_ORIGLU
            // 
            this.IDH_ORIGLU.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ORIGLU.Image")));
            this.IDH_ORIGLU.Location = new System.Drawing.Point(287, 214);
            this.IDH_ORIGLU.Name = "IDH_ORIGLU";
            this.IDH_ORIGLU.Size = new System.Drawing.Size(16, 16);
            this.IDH_ORIGLU.TabIndex = 114;
            this.IDH_ORIGLU.UseVisualStyleBackColor = true;
            this.IDH_ORIGLU.Click += new System.EventHandler(this.IDH_ORLKClick);
            // 
            // IDH_PRDL
            // 
            this.IDH_PRDL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_PRDL.Image")));
            this.IDH_PRDL.Location = new System.Drawing.Point(287, 17);
            this.IDH_PRDL.Name = "IDH_PRDL";
            this.IDH_PRDL.Size = new System.Drawing.Size(16, 16);
            this.IDH_PRDL.TabIndex = 113;
            this.IDH_PRDL.UseVisualStyleBackColor = true;
            this.IDH_PRDL.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_ORIGIN
            // 
            this.IDH_ORIGIN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ORIGIN.Location = new System.Drawing.Point(132, 212);
            this.IDH_ORIGIN.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ORIGIN.Name = "IDH_ORIGIN";
            this.IDH_ORIGIN.Size = new System.Drawing.Size(155, 18);
            this.IDH_ORIGIN.TabIndex = 10;
            // 
            // label65
            // 
            this.label65.Location = new System.Drawing.Point(4, 215);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(100, 23);
            this.label65.TabIndex = 111;
            this.label65.Text = "Origin";
            // 
            // IDH_PRDPHO
            // 
            this.IDH_PRDPHO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PRDPHO.Location = new System.Drawing.Point(132, 190);
            this.IDH_PRDPHO.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PRDPHO.Name = "IDH_PRDPHO";
            this.IDH_PRDPHO.Size = new System.Drawing.Size(155, 18);
            this.IDH_PRDPHO.TabIndex = 9;
            // 
            // IDH_PRDPOS
            // 
            this.IDH_PRDPOS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PRDPOS.Location = new System.Drawing.Point(132, 171);
            this.IDH_PRDPOS.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PRDPOS.Name = "IDH_PRDPOS";
            this.IDH_PRDPOS.Size = new System.Drawing.Size(155, 18);
            this.IDH_PRDPOS.TabIndex = 8;
            // 
            // IDH_PRDCIT
            // 
            this.IDH_PRDCIT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PRDCIT.Location = new System.Drawing.Point(132, 152);
            this.IDH_PRDCIT.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PRDCIT.Name = "IDH_PRDCIT";
            this.IDH_PRDCIT.Size = new System.Drawing.Size(155, 18);
            this.IDH_PRDCIT.TabIndex = 7;
            // 
            // label62
            // 
            this.label62.Location = new System.Drawing.Point(4, 193);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(100, 23);
            this.label62.TabIndex = 107;
            this.label62.Text = "Phone No.";
            // 
            // label63
            // 
            this.label63.Location = new System.Drawing.Point(4, 173);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(100, 23);
            this.label63.TabIndex = 106;
            this.label63.Text = "Post Code";
            // 
            // label64
            // 
            this.label64.Location = new System.Drawing.Point(4, 155);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(100, 23);
            this.label64.TabIndex = 105;
            this.label64.Text = "City";
            // 
            // IDH_PRDBLO
            // 
            this.IDH_PRDBLO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PRDBLO.Location = new System.Drawing.Point(132, 133);
            this.IDH_PRDBLO.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PRDBLO.Name = "IDH_PRDBLO";
            this.IDH_PRDBLO.Size = new System.Drawing.Size(155, 18);
            this.IDH_PRDBLO.TabIndex = 6;
            // 
            // IDH_PRDSTR
            // 
            this.IDH_PRDSTR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PRDSTR.Location = new System.Drawing.Point(132, 114);
            this.IDH_PRDSTR.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PRDSTR.Name = "IDH_PRDSTR";
            this.IDH_PRDSTR.Size = new System.Drawing.Size(155, 18);
            this.IDH_PRDSTR.TabIndex = 5;
            // 
            // IDH_PRDADD
            // 
            this.IDH_PRDADD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PRDADD.Location = new System.Drawing.Point(132, 95);
            this.IDH_PRDADD.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PRDADD.Name = "IDH_PRDADD";
            this.IDH_PRDADD.Size = new System.Drawing.Size(155, 18);
            this.IDH_PRDADD.TabIndex = 4;
            // 
            // IDH_PRDCRF
            // 
            this.IDH_PRDCRF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PRDCRF.Location = new System.Drawing.Point(132, 72);
            this.IDH_PRDCRF.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PRDCRF.Name = "IDH_PRDCRF";
            this.IDH_PRDCRF.Size = new System.Drawing.Size(155, 18);
            this.IDH_PRDCRF.TabIndex = 3;
            // 
            // IDH_PRDCON
            // 
            this.IDH_PRDCON.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_PRDCON.Location = new System.Drawing.Point(132, 53);
            this.IDH_PRDCON.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_PRDCON.Name = "IDH_PRDCON";
            this.IDH_PRDCON.Size = new System.Drawing.Size(155, 18);
            this.IDH_PRDCON.TabIndex = 2;
            // 
            // IDH_WNAM
            // 
            this.IDH_WNAM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WNAM.Location = new System.Drawing.Point(132, 34);
            this.IDH_WNAM.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_WNAM.Name = "IDH_WNAM";
            this.IDH_WNAM.Size = new System.Drawing.Size(155, 18);
            this.IDH_WNAM.TabIndex = 1;
            // 
            // IDH_WPRODU
            // 
            this.IDH_WPRODU.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WPRODU.Location = new System.Drawing.Point(132, 15);
            this.IDH_WPRODU.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_WPRODU.Name = "IDH_WPRODU";
            this.IDH_WPRODU.Size = new System.Drawing.Size(155, 18);
            this.IDH_WPRODU.TabIndex = 0;
            // 
            // label55
            // 
            this.label55.Location = new System.Drawing.Point(4, 135);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(100, 23);
            this.label55.TabIndex = 97;
            this.label55.Text = "Block";
            // 
            // label56
            // 
            this.label56.Location = new System.Drawing.Point(4, 117);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(100, 23);
            this.label56.TabIndex = 96;
            this.label56.Text = "Street";
            // 
            // label57
            // 
            this.label57.Location = new System.Drawing.Point(4, 98);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(100, 23);
            this.label57.TabIndex = 95;
            this.label57.Text = "Address";
            // 
            // label58
            // 
            this.label58.Location = new System.Drawing.Point(4, 75);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(100, 23);
            this.label58.TabIndex = 94;
            this.label58.Text = "Producer Ref. No.";
            // 
            // label59
            // 
            this.label59.Location = new System.Drawing.Point(4, 53);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(100, 23);
            this.label59.TabIndex = 93;
            this.label59.Text = "Contact Person";
            // 
            // label60
            // 
            this.label60.Location = new System.Drawing.Point(4, 34);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(100, 23);
            this.label60.TabIndex = 92;
            this.label60.Text = "Name";
            // 
            // label61
            // 
            this.label61.Location = new System.Drawing.Point(4, 18);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(100, 18);
            this.label61.TabIndex = 91;
            this.label61.Text = "Producer";
            // 
            // oTPSite
            // 
            this.oTPSite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPSite.Controls.Add(this.IDH_SITAL);
            this.oTPSite.Controls.Add(this.IDH_SITL);
            this.oTPSite.Controls.Add(this.IDH_SITPHO);
            this.oTPSite.Controls.Add(this.IDH_SITPOS);
            this.oTPSite.Controls.Add(this.IDH_SITCIT);
            this.oTPSite.Controls.Add(this.label66);
            this.oTPSite.Controls.Add(this.label67);
            this.oTPSite.Controls.Add(this.label68);
            this.oTPSite.Controls.Add(this.IDH_SITBLO);
            this.oTPSite.Controls.Add(this.IDH_SITSTR);
            this.oTPSite.Controls.Add(this.IDH_SITADD);
            this.oTPSite.Controls.Add(this.IDH_SITCRF);
            this.oTPSite.Controls.Add(this.IDH_SITCON);
            this.oTPSite.Controls.Add(this.IDH_DISNAM);
            this.oTPSite.Controls.Add(this.IDH_DISSIT);
            this.oTPSite.Controls.Add(this.label69);
            this.oTPSite.Controls.Add(this.label70);
            this.oTPSite.Controls.Add(this.label71);
            this.oTPSite.Controls.Add(this.label72);
            this.oTPSite.Controls.Add(this.label73);
            this.oTPSite.Controls.Add(this.label74);
            this.oTPSite.Controls.Add(this.label75);
            this.oTPSite.Location = new System.Drawing.Point(4, 21);
            this.oTPSite.Name = "oTPSite";
            this.oTPSite.Size = new System.Drawing.Size(822, 363);
            this.oTPSite.TabIndex = 4;
            this.oTPSite.Text = "Disposal Site";
            // 
            // IDH_SITAL
            // 
            this.IDH_SITAL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_SITAL.Image")));
            this.IDH_SITAL.Location = new System.Drawing.Point(286, 97);
            this.IDH_SITAL.Name = "IDH_SITAL";
            this.IDH_SITAL.Size = new System.Drawing.Size(16, 16);
            this.IDH_SITAL.TabIndex = 137;
            this.IDH_SITAL.UseVisualStyleBackColor = true;
            this.IDH_SITAL.Click += new System.EventHandler(this.AddressSearchClick);
            // 
            // IDH_SITL
            // 
            this.IDH_SITL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_SITL.Image")));
            this.IDH_SITL.Location = new System.Drawing.Point(286, 17);
            this.IDH_SITL.Name = "IDH_SITL";
            this.IDH_SITL.Size = new System.Drawing.Size(16, 16);
            this.IDH_SITL.TabIndex = 136;
            this.IDH_SITL.UseVisualStyleBackColor = true;
            this.IDH_SITL.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_SITPHO
            // 
            this.IDH_SITPHO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SITPHO.Location = new System.Drawing.Point(131, 190);
            this.IDH_SITPHO.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SITPHO.Name = "IDH_SITPHO";
            this.IDH_SITPHO.Size = new System.Drawing.Size(155, 18);
            this.IDH_SITPHO.TabIndex = 135;
            // 
            // IDH_SITPOS
            // 
            this.IDH_SITPOS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SITPOS.Location = new System.Drawing.Point(131, 171);
            this.IDH_SITPOS.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SITPOS.Name = "IDH_SITPOS";
            this.IDH_SITPOS.Size = new System.Drawing.Size(155, 18);
            this.IDH_SITPOS.TabIndex = 134;
            // 
            // IDH_SITCIT
            // 
            this.IDH_SITCIT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SITCIT.Location = new System.Drawing.Point(131, 152);
            this.IDH_SITCIT.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SITCIT.Name = "IDH_SITCIT";
            this.IDH_SITCIT.Size = new System.Drawing.Size(155, 18);
            this.IDH_SITCIT.TabIndex = 133;
            // 
            // label66
            // 
            this.label66.Location = new System.Drawing.Point(4, 193);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(100, 23);
            this.label66.TabIndex = 132;
            this.label66.Text = "Phone No.";
            // 
            // label67
            // 
            this.label67.Location = new System.Drawing.Point(4, 174);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(100, 23);
            this.label67.TabIndex = 131;
            this.label67.Text = "Post Code";
            // 
            // label68
            // 
            this.label68.Location = new System.Drawing.Point(4, 155);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(100, 23);
            this.label68.TabIndex = 130;
            this.label68.Text = "City";
            // 
            // IDH_SITBLO
            // 
            this.IDH_SITBLO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SITBLO.Location = new System.Drawing.Point(131, 133);
            this.IDH_SITBLO.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SITBLO.Name = "IDH_SITBLO";
            this.IDH_SITBLO.Size = new System.Drawing.Size(155, 18);
            this.IDH_SITBLO.TabIndex = 129;
            // 
            // IDH_SITSTR
            // 
            this.IDH_SITSTR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SITSTR.Location = new System.Drawing.Point(131, 114);
            this.IDH_SITSTR.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SITSTR.Name = "IDH_SITSTR";
            this.IDH_SITSTR.Size = new System.Drawing.Size(155, 18);
            this.IDH_SITSTR.TabIndex = 128;
            // 
            // IDH_SITADD
            // 
            this.IDH_SITADD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SITADD.Location = new System.Drawing.Point(131, 95);
            this.IDH_SITADD.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SITADD.Name = "IDH_SITADD";
            this.IDH_SITADD.Size = new System.Drawing.Size(155, 18);
            this.IDH_SITADD.TabIndex = 127;
            // 
            // IDH_SITCRF
            // 
            this.IDH_SITCRF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SITCRF.Location = new System.Drawing.Point(131, 72);
            this.IDH_SITCRF.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SITCRF.Name = "IDH_SITCRF";
            this.IDH_SITCRF.Size = new System.Drawing.Size(155, 18);
            this.IDH_SITCRF.TabIndex = 126;
            // 
            // IDH_SITCON
            // 
            this.IDH_SITCON.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SITCON.Location = new System.Drawing.Point(131, 53);
            this.IDH_SITCON.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SITCON.Name = "IDH_SITCON";
            this.IDH_SITCON.Size = new System.Drawing.Size(155, 18);
            this.IDH_SITCON.TabIndex = 125;
            // 
            // IDH_DISNAM
            // 
            this.IDH_DISNAM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DISNAM.Location = new System.Drawing.Point(131, 34);
            this.IDH_DISNAM.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_DISNAM.Name = "IDH_DISNAM";
            this.IDH_DISNAM.Size = new System.Drawing.Size(155, 18);
            this.IDH_DISNAM.TabIndex = 124;
            // 
            // IDH_DISSIT
            // 
            this.IDH_DISSIT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DISSIT.Location = new System.Drawing.Point(131, 15);
            this.IDH_DISSIT.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_DISSIT.Name = "IDH_DISSIT";
            this.IDH_DISSIT.Size = new System.Drawing.Size(155, 18);
            this.IDH_DISSIT.TabIndex = 123;
            // 
            // label69
            // 
            this.label69.Location = new System.Drawing.Point(4, 135);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(100, 23);
            this.label69.TabIndex = 122;
            this.label69.Text = "Block";
            // 
            // label70
            // 
            this.label70.Location = new System.Drawing.Point(4, 116);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(100, 23);
            this.label70.TabIndex = 121;
            this.label70.Text = "Street";
            // 
            // label71
            // 
            this.label71.Location = new System.Drawing.Point(4, 98);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(100, 23);
            this.label71.TabIndex = 120;
            this.label71.Text = "Address";
            // 
            // label72
            // 
            this.label72.Location = new System.Drawing.Point(4, 75);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(100, 23);
            this.label72.TabIndex = 119;
            this.label72.Text = "Disp. Rec. Site. Ref. No.";
            // 
            // label73
            // 
            this.label73.Location = new System.Drawing.Point(4, 55);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(100, 23);
            this.label73.TabIndex = 118;
            this.label73.Text = "Contact Person";
            // 
            // label74
            // 
            this.label74.Location = new System.Drawing.Point(4, 36);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(100, 23);
            this.label74.TabIndex = 117;
            this.label74.Text = "Name";
            // 
            // label75
            // 
            this.label75.Location = new System.Drawing.Point(4, 19);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(100, 18);
            this.label75.TabIndex = 116;
            this.label75.Text = "Disposal Site";
            // 
            // oAdditional
            // 
            this.oAdditional.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oAdditional.Controls.Add(this.label112);
            this.oAdditional.Controls.Add(this.label111);
            this.oAdditional.Controls.Add(this.IDH_ADDCH);
            this.oAdditional.Controls.Add(this.IDH_ADDCO);
            this.oAdditional.Controls.Add(this.oAddGrid);
            this.oAdditional.Location = new System.Drawing.Point(4, 21);
            this.oAdditional.Name = "oAdditional";
            this.oAdditional.Padding = new System.Windows.Forms.Padding(3);
            this.oAdditional.Size = new System.Drawing.Size(822, 363);
            this.oAdditional.TabIndex = 5;
            this.oAdditional.Text = "Additional";
            // 
            // label112
            // 
            this.label112.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label112.Location = new System.Drawing.Point(666, 333);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(100, 16);
            this.label112.TabIndex = 4;
            this.label112.Text = "Additional Charge";
            // 
            // label111
            // 
            this.label111.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label111.Location = new System.Drawing.Point(666, 314);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(100, 15);
            this.label111.TabIndex = 3;
            this.label111.Text = "Additional Cost";
            // 
            // IDH_ADDCH
            // 
            this.IDH_ADDCH.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_ADDCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ADDCH.Location = new System.Drawing.Point(766, 331);
            this.IDH_ADDCH.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ADDCH.Name = "IDH_ADDCH";
            this.IDH_ADDCH.ReadOnly = true;
            this.IDH_ADDCH.Size = new System.Drawing.Size(50, 18);
            this.IDH_ADDCH.TabIndex = 2;
            // 
            // IDH_ADDCO
            // 
            this.IDH_ADDCO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_ADDCO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ADDCO.Location = new System.Drawing.Point(766, 311);
            this.IDH_ADDCO.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ADDCO.Name = "IDH_ADDCO";
            this.IDH_ADDCO.ReadOnly = true;
            this.IDH_ADDCO.Size = new System.Drawing.Size(50, 18);
            this.IDH_ADDCO.TabIndex = 1;
            // 
            // oAddGrid
            // 
            this.oAddGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.oAddGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.oAddGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oAddGrid.GrayReadOnly = false;
            this.oAddGrid.Location = new System.Drawing.Point(0, 0);
            this.oAddGrid.Name = "oAddGrid";
            this.oAddGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.oAddGrid.Size = new System.Drawing.Size(821, 306);
            this.oAddGrid.TabIndex = 0;
            // 
            // oDeductions
            // 
            this.oDeductions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oDeductions.Controls.Add(this.label122);
            this.oDeductions.Controls.Add(this.label123);
            this.oDeductions.Controls.Add(this.IDH_DEDVAL);
            this.oDeductions.Controls.Add(this.IDH_DEDWEI);
            this.oDeductions.Controls.Add(this.oDeductionGrid);
            this.oDeductions.Location = new System.Drawing.Point(4, 21);
            this.oDeductions.Name = "oDeductions";
            this.oDeductions.Size = new System.Drawing.Size(822, 363);
            this.oDeductions.TabIndex = 6;
            this.oDeductions.Text = "Deductions";
            // 
            // label122
            // 
            this.label122.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label122.Location = new System.Drawing.Point(647, 333);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(119, 16);
            this.label122.TabIndex = 8;
            this.label122.Text = "Total Value Deduction";
            // 
            // label123
            // 
            this.label123.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label123.Location = new System.Drawing.Point(645, 314);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(121, 15);
            this.label123.TabIndex = 7;
            this.label123.Text = "Total Weight Deduction (kg)";
            // 
            // IDH_DEDVAL
            // 
            this.IDH_DEDVAL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_DEDVAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DEDVAL.Location = new System.Drawing.Point(766, 331);
            this.IDH_DEDVAL.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_DEDVAL.Name = "IDH_DEDVAL";
            this.IDH_DEDVAL.ReadOnly = true;
            this.IDH_DEDVAL.Size = new System.Drawing.Size(50, 18);
            this.IDH_DEDVAL.TabIndex = 6;
            // 
            // IDH_DEDWEI
            // 
            this.IDH_DEDWEI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_DEDWEI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DEDWEI.Location = new System.Drawing.Point(766, 311);
            this.IDH_DEDWEI.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_DEDWEI.Name = "IDH_DEDWEI";
            this.IDH_DEDWEI.ReadOnly = true;
            this.IDH_DEDWEI.Size = new System.Drawing.Size(50, 18);
            this.IDH_DEDWEI.TabIndex = 5;
            // 
            // oDeductionGrid
            // 
            this.oDeductionGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.oDeductionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oDeductionGrid.GrayReadOnly = false;
            this.oDeductionGrid.Location = new System.Drawing.Point(0, 0);
            this.oDeductionGrid.Name = "oDeductionGrid";
            this.oDeductionGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.oDeductionGrid.Size = new System.Drawing.Size(821, 306);
            this.oDeductionGrid.TabIndex = 0;
            // 
            // oPNavigation
            // 
            this.oPNavigation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.oPNavigation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.oPNavigation.CausesValidation = false;
            this.oPNavigation.Controls.Add(this.btFind);
            this.oPNavigation.Controls.Add(this.btAdd);
            this.oPNavigation.Controls.Add(this.btLast);
            this.oPNavigation.Controls.Add(this.btNext);
            this.oPNavigation.Controls.Add(this.btPrevious);
            this.oPNavigation.Controls.Add(this.btFirst);
            this.oPNavigation.Location = new System.Drawing.Point(584, 617);
            this.oPNavigation.Name = "oPNavigation";
            this.oPNavigation.Size = new System.Drawing.Size(205, 24);
            this.oPNavigation.TabIndex = 76;
            // 
            // btFind
            // 
            this.btFind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(235)))));
            this.btFind.CausesValidation = false;
            this.btFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btFind.Location = new System.Drawing.Point(3, 3);
            this.btFind.Margin = new System.Windows.Forms.Padding(0);
            this.btFind.Name = "btFind";
            this.btFind.Size = new System.Drawing.Size(28, 18);
            this.btFind.TabIndex = 904;
            this.btFind.Text = "F";
            this.btFind.UseVisualStyleBackColor = false;
            this.btFind.Click += new System.EventHandler(this.BtFindClick);
            // 
            // btAdd
            // 
            this.btAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(235)))));
            this.btAdd.CausesValidation = false;
            this.btAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd.Location = new System.Drawing.Point(36, 3);
            this.btAdd.Margin = new System.Windows.Forms.Padding(0);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(28, 18);
            this.btAdd.TabIndex = 905;
            this.btAdd.Text = "*";
            this.btAdd.UseVisualStyleBackColor = false;
            this.btAdd.Click += new System.EventHandler(this.BtAddClick);
            // 
            // btLast
            // 
            this.btLast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(235)))));
            this.btLast.CausesValidation = false;
            this.btLast.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLast.Location = new System.Drawing.Point(168, 3);
            this.btLast.Margin = new System.Windows.Forms.Padding(0);
            this.btLast.Name = "btLast";
            this.btLast.Size = new System.Drawing.Size(28, 18);
            this.btLast.TabIndex = 910;
            this.btLast.Text = ">>";
            this.btLast.UseVisualStyleBackColor = false;
            this.btLast.Click += new System.EventHandler(this.BtLastClick);
            // 
            // btNext
            // 
            this.btNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(235)))));
            this.btNext.CausesValidation = false;
            this.btNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btNext.Location = new System.Drawing.Point(135, 3);
            this.btNext.Margin = new System.Windows.Forms.Padding(0);
            this.btNext.Name = "btNext";
            this.btNext.Size = new System.Drawing.Size(28, 18);
            this.btNext.TabIndex = 908;
            this.btNext.Text = ">";
            this.btNext.UseVisualStyleBackColor = false;
            this.btNext.Click += new System.EventHandler(this.BtNextClick);
            // 
            // btPrevious
            // 
            this.btPrevious.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(235)))));
            this.btPrevious.CausesValidation = false;
            this.btPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPrevious.Location = new System.Drawing.Point(102, 3);
            this.btPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.btPrevious.Name = "btPrevious";
            this.btPrevious.Size = new System.Drawing.Size(28, 18);
            this.btPrevious.TabIndex = 907;
            this.btPrevious.Text = "<";
            this.btPrevious.UseVisualStyleBackColor = false;
            this.btPrevious.Click += new System.EventHandler(this.BtPreviousClick);
            // 
            // btFirst
            // 
            this.btFirst.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(235)))));
            this.btFirst.CausesValidation = false;
            this.btFirst.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btFirst.Location = new System.Drawing.Point(69, 3);
            this.btFirst.Margin = new System.Windows.Forms.Padding(0);
            this.btFirst.Name = "btFirst";
            this.btFirst.Size = new System.Drawing.Size(28, 18);
            this.btFirst.TabIndex = 906;
            this.btFirst.Text = "<<";
            this.btFirst.UseVisualStyleBackColor = false;
            this.btFirst.Click += new System.EventHandler(this.BtFirstClick);
            // 
            // IDH_SPECIN
            // 
            this.IDH_SPECIN.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_SPECIN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SPECIN.Location = new System.Drawing.Point(110, 165);
            this.IDH_SPECIN.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SPECIN.Multiline = true;
            this.IDH_SPECIN.Name = "IDH_SPECIN";
            this.IDH_SPECIN.Size = new System.Drawing.Size(431, 55);
            this.IDH_SPECIN.TabIndex = 169;
            // 
            // IDH_CUST
            // 
            this.IDH_CUST.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_CUST.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUST.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUST.Location = new System.Drawing.Point(386, 5);
            this.IDH_CUST.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CUST.Name = "IDH_CUST";
            this.IDH_CUST.Size = new System.Drawing.Size(155, 18);
            this.IDH_CUST.TabIndex = 2;
            // 
            // IDH_CUSTNM
            // 
            this.IDH_CUSTNM.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_CUSTNM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSTNM.Location = new System.Drawing.Point(386, 24);
            this.IDH_CUSTNM.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CUSTNM.Name = "IDH_CUSTNM";
            this.IDH_CUSTNM.Size = new System.Drawing.Size(155, 18);
            this.IDH_CUSTNM.TabIndex = 180;
            // 
            // IDH_CUSCRF
            // 
            this.IDH_CUSCRF.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_CUSCRF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CUSCRF.Location = new System.Drawing.Point(386, 43);
            this.IDH_CUSCRF.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CUSCRF.Name = "IDH_CUSCRF";
            this.IDH_CUSCRF.Size = new System.Drawing.Size(155, 18);
            this.IDH_CUSCRF.TabIndex = 181;
            this.IDH_CUSCRF.Validated += new System.EventHandler(this.IDH_WRROW_Click);
            // 
            // IDH_ONCS
            // 
            this.IDH_ONCS.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_ONCS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ONCS.Location = new System.Drawing.Point(386, 62);
            this.IDH_ONCS.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ONCS.Name = "IDH_ONCS";
            this.IDH_ONCS.Size = new System.Drawing.Size(155, 18);
            this.IDH_ONCS.TabIndex = 182;
            // 
            // IDH_USER
            // 
            this.IDH_USER.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_USER.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_USER.Location = new System.Drawing.Point(386, 124);
            this.IDH_USER.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_USER.Name = "IDH_USER";
            this.IDH_USER.Size = new System.Drawing.Size(155, 18);
            this.IDH_USER.TabIndex = 186;
            // 
            // IDH_CARNAM
            // 
            this.IDH_CARNAM.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_CARNAM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CARNAM.Location = new System.Drawing.Point(386, 105);
            this.IDH_CARNAM.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CARNAM.Name = "IDH_CARNAM";
            this.IDH_CARNAM.Size = new System.Drawing.Size(155, 18);
            this.IDH_CARNAM.TabIndex = 186;
            // 
            // IDH_CARRIE
            // 
            this.IDH_CARRIE.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_CARRIE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CARRIE.Location = new System.Drawing.Point(386, 86);
            this.IDH_CARRIE.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CARRIE.Name = "IDH_CARRIE";
            this.IDH_CARRIE.Size = new System.Drawing.Size(155, 18);
            this.IDH_CARRIE.TabIndex = 3;
            // 
            // IDH_SITREF
            // 
            this.IDH_SITREF.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_SITREF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SITREF.Location = new System.Drawing.Point(675, 140);
            this.IDH_SITREF.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SITREF.Name = "IDH_SITREF";
            this.IDH_SITREF.Size = new System.Drawing.Size(155, 18);
            this.IDH_SITREF.TabIndex = 12;
            // 
            // IDH_HAZCN
            // 
            this.IDH_HAZCN.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_HAZCN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_HAZCN.Enabled = false;
            this.IDH_HAZCN.Location = new System.Drawing.Point(675, 121);
            this.IDH_HAZCN.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_HAZCN.Name = "IDH_HAZCN";
            this.IDH_HAZCN.Size = new System.Drawing.Size(107, 18);
            this.IDH_HAZCN.TabIndex = 11;
            // 
            // IDH_WASTTN
            // 
            this.IDH_WASTTN.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_WASTTN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WASTTN.Location = new System.Drawing.Point(675, 102);
            this.IDH_WASTTN.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_WASTTN.Name = "IDH_WASTTN";
            this.IDH_WASTTN.Size = new System.Drawing.Size(155, 18);
            this.IDH_WASTTN.TabIndex = 10;
            // 
            // IDH_STATUS
            // 
            this.IDH_STATUS.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_STATUS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_STATUS.Enabled = false;
            this.IDH_STATUS.Location = new System.Drawing.Point(675, 62);
            this.IDH_STATUS.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_STATUS.Name = "IDH_STATUS";
            this.IDH_STATUS.Size = new System.Drawing.Size(155, 18);
            this.IDH_STATUS.TabIndex = 191;
            // 
            // IDH_BOOKDT
            // 
            this.IDH_BOOKDT.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_BOOKDT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_BOOKDT.Location = new System.Drawing.Point(675, 43);
            this.IDH_BOOKDT.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_BOOKDT.Name = "IDH_BOOKDT";
            this.IDH_BOOKDT.Size = new System.Drawing.Size(70, 18);
            this.IDH_BOOKDT.TabIndex = 16;
            // 
            // IDH_ROW
            // 
            this.IDH_ROW.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_ROW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ROW.Enabled = false;
            this.IDH_ROW.Location = new System.Drawing.Point(675, 24);
            this.IDH_ROW.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ROW.Name = "IDH_ROW";
            this.IDH_ROW.Size = new System.Drawing.Size(155, 18);
            this.IDH_ROW.TabIndex = 189;
            // 
            // IDH_BOOREF
            // 
            this.IDH_BOOREF.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_BOOREF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_BOOREF.Enabled = false;
            this.IDH_BOOREF.Location = new System.Drawing.Point(675, 5);
            this.IDH_BOOREF.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_BOOREF.Name = "IDH_BOOREF";
            this.IDH_BOOREF.Size = new System.Drawing.Size(155, 18);
            this.IDH_BOOREF.TabIndex = 188;
            this.IDH_BOOREF.Validated += new System.EventHandler(this.IDH_WRROW_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 15);
            this.label2.TabIndex = 27;
            this.label2.Text = "Vehicle Reg. No.";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(5, 48);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 15);
            this.label3.TabIndex = 28;
            this.label3.Text = "Linked Waste Order";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(169, 48);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 15);
            this.label4.TabIndex = 29;
            this.label4.Text = "Row";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(5, 71);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 15);
            this.label5.TabIndex = 30;
            this.label5.Text = "Vehicle";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(5, 88);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 15);
            this.label6.TabIndex = 31;
            this.label6.Text = "Driver";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(5, 107);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 15);
            this.label7.TabIndex = 32;
            this.label7.Text = "2nd Tare Id";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(4, 126);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 15);
            this.label8.TabIndex = 33;
            this.label8.Text = "2nd Tare Name";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(9, 167);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 15);
            this.label9.TabIndex = 34;
            this.label9.Text = "Delivery Instruction";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(292, 10);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 15);
            this.label10.TabIndex = 35;
            this.label10.Text = "Customer";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(292, 29);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 15);
            this.label11.TabIndex = 36;
            this.label11.Text = "Name";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(292, 48);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 15);
            this.label12.TabIndex = 37;
            this.label12.Text = "Customer Ref. No.";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(292, 67);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 15);
            this.label13.TabIndex = 38;
            this.label13.Text = "Compl. Scheme";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(292, 89);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 15);
            this.label14.TabIndex = 39;
            this.label14.Text = "Waste Carrier";
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(292, 108);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 15);
            this.label15.TabIndex = 40;
            this.label15.Text = "Name";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(292, 127);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 15);
            this.label16.TabIndex = 41;
            this.label16.Text = "User";
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(292, 145);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 15);
            this.label17.TabIndex = 42;
            this.label17.Text = "Branch";
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(563, 7);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(91, 15);
            this.label18.TabIndex = 43;
            this.label18.Text = "WB Ticket No.";
            this.label18.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.label19_HelpRequested);
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(563, 29);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(91, 15);
            this.label19.TabIndex = 44;
            this.label19.Text = "Row No.";
            this.label19.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.label19_HelpRequested);
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(563, 48);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 15);
            this.label20.TabIndex = 45;
            this.label20.Text = "Booking Date";
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(563, 67);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(91, 15);
            this.label21.TabIndex = 46;
            this.label21.Text = "Status";
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(563, 86);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(91, 15);
            this.label22.TabIndex = 47;
            this.label22.Text = "Tip Zone";
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(564, 106);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(91, 15);
            this.label23.TabIndex = 48;
            this.label23.Text = "Waste Trnsf. No.";
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(563, 125);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(91, 15);
            this.label24.TabIndex = 49;
            this.label24.Text = "Consign. Note No.";
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(563, 144);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(91, 15);
            this.label25.TabIndex = 50;
            this.label25.Text = "Ste Ref. No.";
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(563, 163);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(91, 15);
            this.label26.TabIndex = 51;
            this.label26.Text = "Extr Weighbridge No.";
            // 
            // label27
            // 
            this.label27.Location = new System.Drawing.Point(563, 179);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(101, 15);
            this.label27.TabIndex = 52;
            this.label27.Text = "Container Number";
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(563, 198);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(110, 15);
            this.label28.TabIndex = 53;
            this.label28.Text = "Seal Number/Pentane";
            // 
            // IDH_EXTWEI
            // 
            this.IDH_EXTWEI.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_EXTWEI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_EXTWEI.Location = new System.Drawing.Point(675, 159);
            this.IDH_EXTWEI.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_EXTWEI.Name = "IDH_EXTWEI";
            this.IDH_EXTWEI.Size = new System.Drawing.Size(155, 18);
            this.IDH_EXTWEI.TabIndex = 13;
            // 
            // IDH_CONTNR
            // 
            this.IDH_CONTNR.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_CONTNR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_CONTNR.Location = new System.Drawing.Point(675, 178);
            this.IDH_CONTNR.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_CONTNR.Name = "IDH_CONTNR";
            this.IDH_CONTNR.Size = new System.Drawing.Size(155, 18);
            this.IDH_CONTNR.TabIndex = 14;
            // 
            // IDH_SEALNR
            // 
            this.IDH_SEALNR.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_SEALNR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_SEALNR.Location = new System.Drawing.Point(675, 197);
            this.IDH_SEALNR.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_SEALNR.Name = "IDH_SEALNR";
            this.IDH_SEALNR.Size = new System.Drawing.Size(155, 18);
            this.IDH_SEALNR.TabIndex = 15;
            // 
            // label29
            // 
            this.label29.Location = new System.Drawing.Point(563, 220);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(91, 15);
            this.label29.TabIndex = 36;
            this.label29.Text = "Obligated";
            // 
            // IDH_BOOKTM
            // 
            this.IDH_BOOKTM.BackColor = System.Drawing.SystemColors.Window;
            this.IDH_BOOKTM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_BOOKTM.Location = new System.Drawing.Point(775, 43);
            this.IDH_BOOKTM.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_BOOKTM.Name = "IDH_BOOKTM";
            this.IDH_BOOKTM.Size = new System.Drawing.Size(55, 18);
            this.IDH_BOOKTM.TabIndex = 190;
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(745, 47);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(30, 15);
            this.label30.TabIndex = 59;
            this.label30.Text = "Time";
            // 
            // IDH_REGL
            // 
            this.IDH_REGL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_REGL.Image")));
            this.IDH_REGL.Location = new System.Drawing.Point(265, 26);
            this.IDH_REGL.Name = "IDH_REGL";
            this.IDH_REGL.Size = new System.Drawing.Size(16, 16);
            this.IDH_REGL.TabIndex = 161;
            this.IDH_REGL.UseVisualStyleBackColor = true;
            this.IDH_REGL.Click += new System.EventHandler(this.IDH_REGLButton1Click);
            // 
            // IDH_WOCF
            // 
            this.IDH_WOCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_WOCF.Image")));
            this.IDH_WOCF.Location = new System.Drawing.Point(265, 45);
            this.IDH_WOCF.Name = "IDH_WOCF";
            this.IDH_WOCF.Size = new System.Drawing.Size(16, 16);
            this.IDH_WOCF.TabIndex = 164;
            this.IDH_WOCF.UseVisualStyleBackColor = true;
            this.IDH_WOCF.Click += new System.EventHandler(this.LinkWOClick);
            // 
            // IDH_CUSL
            // 
            this.IDH_CUSL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_CUSL.Image")));
            this.IDH_CUSL.Location = new System.Drawing.Point(541, 6);
            this.IDH_CUSL.Name = "IDH_CUSL";
            this.IDH_CUSL.Size = new System.Drawing.Size(16, 16);
            this.IDH_CUSL.TabIndex = 179;
            this.IDH_CUSL.UseVisualStyleBackColor = true;
            this.IDH_CUSL.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_CSCF
            // 
            this.IDH_CSCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_CSCF.Image")));
            this.IDH_CSCF.Location = new System.Drawing.Point(541, 64);
            this.IDH_CSCF.Name = "IDH_CSCF";
            this.IDH_CSCF.Size = new System.Drawing.Size(16, 16);
            this.IDH_CSCF.TabIndex = 184;
            this.IDH_CSCF.UseVisualStyleBackColor = true;
            this.IDH_CSCF.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_CARL
            // 
            this.IDH_CARL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_CARL.Image")));
            this.IDH_CARL.Location = new System.Drawing.Point(541, 88);
            this.IDH_CARL.Name = "IDH_CARL";
            this.IDH_CARL.Size = new System.Drawing.Size(16, 16);
            this.IDH_CARL.TabIndex = 185;
            this.IDH_CARL.UseVisualStyleBackColor = true;
            this.IDH_CARL.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // btn1
            // 
            this.btn1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.btn1.Location = new System.Drawing.Point(6, 617);
            this.btn1.Margin = new System.Windows.Forms.Padding(1);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(75, 23);
            this.btn1.TabIndex = 1000;
            this.btn1.Text = "Find";
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.Btn1Click);
            // 
            // IDH_BRANCH
            // 
            this.IDH_BRANCH.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_BRANCH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_BRANCH.FormattingEnabled = true;
            this.IDH_BRANCH.Location = new System.Drawing.Point(386, 143);
            this.IDH_BRANCH.Name = "IDH_BRANCH";
            this.IDH_BRANCH.Size = new System.Drawing.Size(155, 20);
            this.IDH_BRANCH.TabIndex = 187;
            // 
            // IDH_TZONE
            // 
            this.IDH_TZONE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_TZONE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_TZONE.FormattingEnabled = true;
            this.IDH_TZONE.Location = new System.Drawing.Point(675, 81);
            this.IDH_TZONE.Name = "IDH_TZONE";
            this.IDH_TZONE.Size = new System.Drawing.Size(155, 20);
            this.IDH_TZONE.TabIndex = 192;
            // 
            // IDH_OBLED
            // 
            this.IDH_OBLED.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_OBLED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_OBLED.FormattingEnabled = true;
            this.IDH_OBLED.Location = new System.Drawing.Point(675, 216);
            this.IDH_OBLED.Name = "IDH_OBLED";
            this.IDH_OBLED.Size = new System.Drawing.Size(155, 20);
            this.IDH_OBLED.TabIndex = 28;
            // 
            // IDH_AVDOC
            // 
            this.IDH_AVDOC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IDH_AVDOC.Location = new System.Drawing.Point(214, 622);
            this.IDH_AVDOC.Name = "IDH_AVDOC";
            this.IDH_AVDOC.Size = new System.Drawing.Size(93, 15);
            this.IDH_AVDOC.TabIndex = 901;
            this.IDH_AVDOC.Text = "Auto Print DOC";
            this.IDH_AVDOC.UseVisualStyleBackColor = true;
            // 
            // IDH_AVCONV
            // 
            this.IDH_AVCONV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IDH_AVCONV.Location = new System.Drawing.Point(104, 622);
            this.IDH_AVCONV.Name = "IDH_AVCONV";
            this.IDH_AVCONV.Size = new System.Drawing.Size(104, 15);
            this.IDH_AVCONV.TabIndex = 900;
            this.IDH_AVCONV.Text = "Auto Print WB Tkt";
            this.IDH_AVCONV.UseVisualStyleBackColor = true;
            // 
            // IDH_CON
            // 
            this.IDH_CON.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_CON.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_CON.Location = new System.Drawing.Point(448, 617);
            this.IDH_CON.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_CON.Name = "IDH_CON";
            this.IDH_CON.Size = new System.Drawing.Size(65, 23);
            this.IDH_CON.TabIndex = 902;
            this.IDH_CON.Text = "Conv Note";
            this.IDH_CON.UseVisualStyleBackColor = false;
            this.IDH_CON.Click += new System.EventHandler(this.IDH_CON_Click);
            // 
            // IDH_DOC
            // 
            this.IDH_DOC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_DOC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_DOC.Location = new System.Drawing.Point(516, 617);
            this.IDH_DOC.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_DOC.Name = "IDH_DOC";
            this.IDH_DOC.Size = new System.Drawing.Size(65, 23);
            this.IDH_DOC.TabIndex = 903;
            this.IDH_DOC.Text = "DOC";
            this.IDH_DOC.UseVisualStyleBackColor = false;
            this.IDH_DOC.Click += new System.EventHandler(this.IDH_DOC_Click);
            // 
            // IDH_OS
            // 
            this.IDH_OS.Image = ((System.Drawing.Image)(resources.GetObject("IDH_OS.Image")));
            this.IDH_OS.Location = new System.Drawing.Point(248, 26);
            this.IDH_OS.Margin = new System.Windows.Forms.Padding(0);
            this.IDH_OS.Name = "IDH_OS";
            this.IDH_OS.Size = new System.Drawing.Size(16, 16);
            this.IDH_OS.TabIndex = 160;
            this.IDH_OS.UseVisualStyleBackColor = true;
            this.IDH_OS.Click += new System.EventHandler(this.IDH_OSClick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prg1,
            this.LblVersion,
            this.ItemInfo,
            this.stl01,
            this.lblLastError});
            this.statusStrip1.Location = new System.Drawing.Point(0, 643);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(838, 24);
            this.statusStrip1.TabIndex = 105;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.StatusStrip1ItemClicked);
            // 
            // prg1
            // 
            this.prg1.Name = "prg1";
            this.prg1.Size = new System.Drawing.Size(100, 18);
            // 
            // LblVersion
            // 
            this.LblVersion.Name = "LblVersion";
            this.LblVersion.Size = new System.Drawing.Size(106, 19);
            this.LblVersion.Text = " Version db: 577 - 0.0001";
            // 
            // ItemInfo
            // 
            this.ItemInfo.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.ItemInfo.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.ItemInfo.Name = "ItemInfo";
            this.ItemInfo.Size = new System.Drawing.Size(44, 19);
            this.ItemInfo.Text = "ItemInfo";
            // 
            // stl01
            // 
            this.stl01.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.stl01.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.stl01.Name = "stl01";
            this.stl01.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.stl01.Size = new System.Drawing.Size(48, 19);
            this.stl01.Text = "Welcome";
            this.stl01.Click += new System.EventHandler(this.Stl01Click);
            // 
            // lblLastError
            // 
            this.lblLastError.BackColor = System.Drawing.Color.Linen;
            this.lblLastError.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblLastError.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter;
            this.lblLastError.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lblLastError.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(64, 19);
            this.lblLastError.Text = "Get LastError";
            this.lblLastError.Visible = false;
            // 
            // btnHelp
            // 
            this.btnHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(230)))), ((int)(((byte)(200)))));
            this.btnHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHelp.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnHelp.Location = new System.Drawing.Point(794, 620);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(27, 19);
            this.btnHelp.TabIndex = 1001;
            this.btnHelp.Text = "?";
            this.btnHelp.UseVisualStyleBackColor = false;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // IDH_CONGEN
            // 
            this.IDH_CONGEN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_CONGEN.Location = new System.Drawing.Point(784, 120);
            this.IDH_CONGEN.Margin = new System.Windows.Forms.Padding(0);
            this.IDH_CONGEN.Name = "IDH_CONGEN";
            this.IDH_CONGEN.Size = new System.Drawing.Size(46, 20);
            this.IDH_CONGEN.TabIndex = 1003;
            this.IDH_CONGEN.Text = "GEN";
            this.IDH_CONGEN.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.IDH_CONGEN.UseVisualStyleBackColor = false;
            this.IDH_CONGEN.Click += new System.EventHandler(this.IDH_CONGEN_Click);
            // 
            // IDH_BOOKIN
            // 
            this.IDH_BOOKIN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IDH_BOOKIN.AutoSize = true;
            this.IDH_BOOKIN.Location = new System.Drawing.Point(313, 622);
            this.IDH_BOOKIN.Name = "IDH_BOOKIN";
            this.IDH_BOOKIN.Size = new System.Drawing.Size(110, 16);
            this.IDH_BOOKIN.TabIndex = 1004;
            this.IDH_BOOKIN.Text = "Do Stock Movement";
            this.IDH_BOOKIN.UseVisualStyleBackColor = true;
            this.IDH_BOOKIN.CheckedChanged += new System.EventHandler(this.IDH_BOOKIN_CheckedChanged);
            // 
            // IDH_BOKSTA
            // 
            this.IDH_BOKSTA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IDH_BOKSTA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_BOKSTA.Enabled = false;
            this.IDH_BOKSTA.Location = new System.Drawing.Point(421, 620);
            this.IDH_BOKSTA.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_BOKSTA.Name = "IDH_BOKSTA";
            this.IDH_BOKSTA.Size = new System.Drawing.Size(18, 18);
            this.IDH_BOKSTA.TabIndex = 1005;
            // 
            // MainForm
            // 
            this.AcceptButton = this.btn1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(838, 667);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.IDH_BOKSTA);
            this.Controls.Add(this.IDH_CONGEN);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.IDH_BOOKIN);
            this.Controls.Add(this.IDH_OBLED);
            this.Controls.Add(this.IDH_AVDOC);
            this.Controls.Add(this.IDH_TZONE);
            this.Controls.Add(this.IDH_AVCONV);
            this.Controls.Add(this.IDH_BRANCH);
            this.Controls.Add(this.IDH_CON);
            this.Controls.Add(this.IDH_DOC);
            this.Controls.Add(this.IDH_CARL);
            this.Controls.Add(this.oPNavigation);
            this.Controls.Add(this.IDH_CSCF);
            this.Controls.Add(this.IDH_CUSL);
            this.Controls.Add(this.IDH_WOCF);
            this.Controls.Add(this.IDH_OS);
            this.Controls.Add(this.IDH_REGL);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.IDH_BOOKTM);
            this.Controls.Add(this.IDH_SEALNR);
            this.Controls.Add(this.IDH_CONTNR);
            this.Controls.Add(this.IDH_EXTWEI);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.IDH_SITREF);
            this.Controls.Add(this.IDH_HAZCN);
            this.Controls.Add(this.IDH_WASTTN);
            this.Controls.Add(this.IDH_STATUS);
            this.Controls.Add(this.IDH_BOOKDT);
            this.Controls.Add(this.IDH_ROW);
            this.Controls.Add(this.IDH_BOOREF);
            this.Controls.Add(this.IDH_USER);
            this.Controls.Add(this.IDH_CARNAM);
            this.Controls.Add(this.IDH_CARRIE);
            this.Controls.Add(this.IDH_ONCS);
            this.Controls.Add(this.IDH_CUSCRF);
            this.Controls.Add(this.IDH_CUSTNM);
            this.Controls.Add(this.IDH_CUST);
            this.Controls.Add(this.IDH_SPECIN);
            this.Controls.Add(this.oMainTabs);
            this.Controls.Add(this.IDH_TRLNM);
            this.Controls.Add(this.IDH_TRLReg);
            this.Controls.Add(this.IDH_DRIVER);
            this.Controls.Add(this.IDH_VEH);
            this.Controls.Add(this.IDH_WRROW);
            this.Controls.Add(this.IDH_WRORD);
            this.Controls.Add(this.IDH_VEHREG);
            this.Controls.Add(this.IDH_JOBTTP);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Disposal Order";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFormFormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.oMainTabs.ResumeLayout(false);
            this.oTPWeighing.ResumeLayout(false);
            this.oTPWeighing.PerformLayout();
            this.PO_GRP.ResumeLayout(false);
            this.PO_GRP.PerformLayout();
            this.SO_GRP.ResumeLayout(false);
            this.SO_GRP.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.oTPCarrier.ResumeLayout(false);
            this.oTPCarrier.PerformLayout();
            this.oTPCustomer.ResumeLayout(false);
            this.oTPCustomer.PerformLayout();
            this.oTPProducer.ResumeLayout(false);
            this.oTPProducer.PerformLayout();
            this.oTPSite.ResumeLayout(false);
            this.oTPSite.PerformLayout();
            this.oAdditional.ResumeLayout(false);
            this.oAdditional.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oAddGrid)).EndInit();
            this.oDeductions.ResumeLayout(false);
            this.oDeductions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oDeductionGrid)).EndInit();
            this.oPNavigation.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
		private System.Windows.Forms.TextBox IDH_ADDCO;
		private System.Windows.Forms.TextBox IDH_ADDCH;
		private System.Windows.Forms.Label label111;
		private System.Windows.Forms.Label label112;
		//private com.idh.win.controls.WR1Grid oAddGrid;
		private idh.wr1.grid.AdditionalExpenses oAddGrid;
		private System.Windows.Forms.TabPage oAdditional;
		private System.Windows.Forms.ToolStripStatusLabel lblLastError;
#region variables
        private System.Windows.Forms.TextBox IDH_PROCD;
		private System.Windows.Forms.Button IDH_PROCF;
		private System.Windows.Forms.TextBox IDH_PRONM;
		private System.Windows.Forms.Label label105;
		private System.Windows.Forms.Label label104;
		private System.Windows.Forms.TextBox IDH_PURWGT;
		private System.Windows.Forms.TextBox IDH_PRCOST;
		private System.Windows.Forms.Label label103;
        private System.Windows.Forms.TextBox IDH_PURTOT;
		private System.Windows.Forms.Label label106;
		private System.Windows.Forms.TextBox IDH_TAXO;
		private System.Windows.Forms.Label label107;
		private System.Windows.Forms.TextBox IDH_PSTAT;
		private System.Windows.Forms.Label label108;
		private System.Windows.Forms.TextBox IDH_TOTCOS;
		private System.Windows.Forms.Label Lbl1000007;
        private System.Windows.Forms.Label label109;
		private System.Windows.Forms.ToolStripProgressBar prg1;
		private System.Windows.Forms.ToolStripStatusLabel stl01;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.Panel oPNavigation;
		private System.Windows.Forms.Button IDH_TMR;
		private System.Windows.Forms.Button IDH_OS;
		private System.Windows.Forms.CheckBox IDH_NOVAT;
		private System.Windows.Forms.Button IDH_REGL;
		private System.Windows.Forms.Button IDH_WOCF;
		private System.Windows.Forms.Button IDH_CUSL;
		private System.Windows.Forms.Button IDH_CSCF;
		private System.Windows.Forms.Button IDH_CARL;
		private System.Windows.Forms.Button IDH_ORLK;
		private System.Windows.Forms.Button IDH_WASCF;
		private System.Windows.Forms.Button IDH_ITCF;
		private System.Windows.Forms.Button IDH_DOC;
		private System.Windows.Forms.Button IDH_CON;
		private System.Windows.Forms.CheckBox IDH_AVCONV;
		private System.Windows.Forms.CheckBox IDH_AVDOC;
		private System.Windows.Forms.Button btAdd;
		private System.Windows.Forms.Button IDH_WASAL;
		private System.Windows.Forms.TextBox IDH_ADDRES;
		private System.Windows.Forms.TextBox IDH_STREET;
		private System.Windows.Forms.TextBox IDH_CARREF;
		private System.Windows.Forms.CheckBox IDH_DOORD;
		private System.Windows.Forms.CheckBox IDH_DOARI;
		private System.Windows.Forms.CheckBox IDH_DOARIP;
		private System.Windows.Forms.CheckBox IDH_AINV;
		private System.Windows.Forms.CheckBox IDH_FOC;
		private System.Windows.Forms.TextBox IDH_CUSTOT;
		private System.Windows.Forms.TextBox IDH_RORIGI;
		private System.Windows.Forms.TextBox IDH_WASMAT;
		private System.Windows.Forms.TextBox IDH_WASCL1;
		private System.Windows.Forms.TextBox IDH_DESC;
		private System.Windows.Forms.TextBox IDH_ITMCOD;
		private System.Windows.Forms.TextBox IDH_ITMGRC;
		private System.Windows.Forms.TextBox IDH_ITMGRN;
		private System.Windows.Forms.TextBox IDH_CUSCM;
		private System.Windows.Forms.TextBox IDH_RDWGT;
		private System.Windows.Forms.TextBox IDH_WDT2;
		private System.Windows.Forms.TextBox IDH_SER2;
		private System.Windows.Forms.TextBox IDH_WEI2;
		private System.Windows.Forms.TextBox IDH_CASHMT;
		private System.Windows.Forms.TextBox IDH_RSTAT;
		private System.Windows.Forms.TextBox IDH_DISCNT;
        private System.Windows.Forms.TextBox IDH_CUSWEI;
		private System.Windows.Forms.TextBox IDH_CUSCHG;
		private System.Windows.Forms.TextBox IDH_TOTAL;
		private System.Windows.Forms.TextBox IDH_TAX;
		private System.Windows.Forms.TextBox IDH_SUBTOT;
		private System.Windows.Forms.TextBox IDH_ADDCHR;
        private System.Windows.Forms.TextBox IDH_DSCPRC;
		private System.Windows.Forms.Button IDH_ACCB;
		private System.Windows.Forms.Button IDH_ACC1;
		private System.Windows.Forms.Button IDH_BUF1;
		private System.Windows.Forms.Button IDH_TAR1;
		private System.Windows.Forms.Button IDH_ACC2;
		private System.Windows.Forms.Button IDH_BUF2;
		private System.Windows.Forms.Button IDH_TAR2;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.TextBox IDH_WDT1;
		private System.Windows.Forms.TextBox IDH_SER1;
		private System.Windows.Forms.TextBox IDH_WEI1;
		private System.Windows.Forms.TextBox IDH_TRLTar;
		private System.Windows.Forms.TextBox IDH_TARWEI;
		private System.Windows.Forms.TextBox IDH_WDTB;
		private System.Windows.Forms.TextBox IDH_SERB;
		private System.Windows.Forms.TextBox IDH_WEIB;
		private System.Windows.Forms.TextBox IDH_WEIG;
		private System.Windows.Forms.ComboBox IDH_WEIBRG;
		private System.Windows.Forms.Label label88;
		private System.Windows.Forms.Label label89;
		private System.Windows.Forms.Label label90;
		private System.Windows.Forms.Label label91;
		private System.Windows.Forms.Label label92;
		private System.Windows.Forms.Label label93;
		private System.Windows.Forms.Label label94;
		private System.Windows.Forms.Label label95;
		private System.Windows.Forms.Label label96;
		private System.Windows.Forms.Label label97;
		private System.Windows.Forms.Label label98;
		private System.Windows.Forms.Label label99;
		private System.Windows.Forms.Label label100;
		private System.Windows.Forms.Label label101;
		private System.Windows.Forms.Label label102;
		private System.Windows.Forms.Label label76;
		private System.Windows.Forms.Label label77;
		private System.Windows.Forms.Label label78;
		private System.Windows.Forms.Label label79;
		private System.Windows.Forms.Label label80;
		private System.Windows.Forms.Label label81;
		private System.Windows.Forms.Label label82;
		private System.Windows.Forms.Label label83;
		private System.Windows.Forms.Label label84;
		private System.Windows.Forms.Label label85;
		private System.Windows.Forms.Label label86;
		private System.Windows.Forms.Label label87;
		private System.Windows.Forms.Button IDH_SITAL;
		private System.Windows.Forms.Button IDH_SITL;
		private System.Windows.Forms.TextBox IDH_SITPHO;
		private System.Windows.Forms.TextBox IDH_SITPOS;
		private System.Windows.Forms.TextBox IDH_SITCIT;
		private System.Windows.Forms.TextBox IDH_SITBLO;
		private System.Windows.Forms.TextBox IDH_SITSTR;
		private System.Windows.Forms.TextBox IDH_SITADD;
		private System.Windows.Forms.TextBox IDH_SITCRF;
		private System.Windows.Forms.TextBox IDH_SITCON;
		private System.Windows.Forms.TextBox IDH_DISNAM;
		private System.Windows.Forms.TextBox IDH_DISSIT;
		private System.Windows.Forms.Label label75;
		private System.Windows.Forms.Label label74;
		private System.Windows.Forms.Label label73;
		private System.Windows.Forms.Label label72;
		private System.Windows.Forms.Label label71;
		private System.Windows.Forms.Label label70;
		private System.Windows.Forms.Label label69;
		private System.Windows.Forms.Label label68;
		private System.Windows.Forms.Label label67;
		private System.Windows.Forms.Label label66;
		private System.Windows.Forms.Label label64;
		private System.Windows.Forms.Label label63;
		private System.Windows.Forms.Label label62;
		private System.Windows.Forms.TextBox IDH_PRDCIT;
		private System.Windows.Forms.TextBox IDH_PRDPOS;
		private System.Windows.Forms.TextBox IDH_PRDPHO;
		private System.Windows.Forms.Label label65;
		private System.Windows.Forms.TextBox IDH_ORIGIN;
		private System.Windows.Forms.Button IDH_PRDL;
		private System.Windows.Forms.Button IDH_ORIGLU;
		private System.Windows.Forms.Button IDH_PROAL;
		private System.Windows.Forms.TextBox IDH_PRDBLO;
		private System.Windows.Forms.TextBox IDH_PRDSTR;
		private System.Windows.Forms.TextBox IDH_PRDADD;
		private System.Windows.Forms.TextBox IDH_PRDCRF;
		private System.Windows.Forms.TextBox IDH_PRDCON;
		private System.Windows.Forms.TextBox IDH_WNAM;
		private System.Windows.Forms.TextBox IDH_WPRODU;
		private System.Windows.Forms.Label label61;
		private System.Windows.Forms.Label label60;
		private System.Windows.Forms.Label label59;
		private System.Windows.Forms.Label label58;
		private System.Windows.Forms.Label label57;
		private System.Windows.Forms.Label label56;
		private System.Windows.Forms.Label label55;
		private System.Windows.Forms.TextBox IDH_CONTNM;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.Label label49;
		private System.Windows.Forms.Label label50;
		private System.Windows.Forms.TextBox IDH_BLOCK;
		private System.Windows.Forms.Label label51;
		private System.Windows.Forms.TextBox IDH_CITY;
		private System.Windows.Forms.Label label52;
		private System.Windows.Forms.TextBox IDH_ZIP;
		private System.Windows.Forms.Label label53;
		private System.Windows.Forms.TextBox IDH_PHONE;
		private System.Windows.Forms.Label label54;
		private System.Windows.Forms.TextBox IDH_LICREG;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.Label label41;
		private System.Windows.Forms.TextBox IDH_ROUTE;
		private System.Windows.Forms.TextBox IDH_SEQ;
		private System.Windows.Forms.Label label42;
		private System.Windows.Forms.Button IDH_ROUTL;
		private System.Windows.Forms.TextBox IDH_SteId;
		private System.Windows.Forms.TextBox IDH_PCD;
		private System.Windows.Forms.TextBox IDH_SLCNO;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.Button IDH_CUSAL;
		private System.Windows.Forms.Button IDH_CNA;
		private System.Windows.Forms.ComboBox IDH_OBLED;
		private System.Windows.Forms.TextBox IDH_EXTWEI;
		private System.Windows.Forms.TextBox IDH_BOOREF;
		private System.Windows.Forms.TextBox IDH_TRLReg;
		private System.Windows.Forms.Button btFirst;
		private System.Windows.Forms.Button btPrevious;
		private System.Windows.Forms.Button btNext;
		private System.Windows.Forms.Button btLast;
		private System.Windows.Forms.Button btFind;
		private System.Windows.Forms.Label label39;
		private System.Windows.Forms.TextBox IDH_SITETL;
		private System.Windows.Forms.TextBox IDH_CUSLIC;
		private System.Windows.Forms.Label label40;
		private System.Windows.Forms.TextBox IDH_CUSPHO;
		private System.Windows.Forms.TextBox IDH_CUSPOS;
		private System.Windows.Forms.TextBox IDH_COUNTY;
		private System.Windows.Forms.TextBox IDH_CUSCIT;
		private System.Windows.Forms.TextBox IDH_CUSBLO;
		private System.Windows.Forms.TextBox IDH_CUSSTR;
		private System.Windows.Forms.TextBox IDH_CUSADD;
		private System.Windows.Forms.TextBox IDH_CUSCON;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.TextBox IDH_CUSCRF;
		private System.Windows.Forms.Button btn1;
		private System.Windows.Forms.ComboBox IDH_JOBTTP;
		private System.Windows.Forms.TextBox IDH_VEH;
		private System.Windows.Forms.TextBox IDH_DRIVER;
		private System.Windows.Forms.TextBox IDH_TRLNM;
        private System.Windows.Forms.TextBox IDH_SPECIN;
		private System.Windows.Forms.TextBox IDH_CUSTNM;
		private System.Windows.Forms.TextBox IDH_ONCS;
		private System.Windows.Forms.ComboBox IDH_BRANCH;
		private System.Windows.Forms.TextBox IDH_USER;
		private System.Windows.Forms.TextBox IDH_CARNAM;
		private System.Windows.Forms.TextBox IDH_CARRIE;
		private System.Windows.Forms.TextBox IDH_SITREF;
		private System.Windows.Forms.TextBox IDH_HAZCN;
		private System.Windows.Forms.TextBox IDH_WASTTN;
		private System.Windows.Forms.ComboBox IDH_TZONE;
		private System.Windows.Forms.TextBox IDH_STATUS;
		private System.Windows.Forms.TextBox IDH_BOOKDT;
		private System.Windows.Forms.TextBox IDH_ROW;
		private System.Windows.Forms.TextBox IDH_CONTNR;
		private System.Windows.Forms.TextBox IDH_SEALNR;
		private System.Windows.Forms.TextBox IDH_BOOKTM;
		private System.Windows.Forms.TextBox IDH_WRROW;
		private System.Windows.Forms.TextBox IDH_WRORD;
		private System.Windows.Forms.TextBox IDH_VEHREG;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TabPage oTPSite;
		private System.Windows.Forms.TabPage oTPProducer;
		private System.Windows.Forms.TabPage oTPCustomer;
		private System.Windows.Forms.TabPage oTPCarrier;
		private System.Windows.Forms.TabPage oTPWeighing;
		private System.Windows.Forms.TabControl oMainTabs;
		private System.Windows.Forms.Label label1;

        private FolderBrowserDialog folderBrowserDialog1;
        private CheckBox IDH_DOPO;
        private ToolStripStatusLabel ItemInfo;
        private Button btnHelp;
        private System.Windows.Forms.ToolStripStatusLabel LblVersion;
        private Button IDH_CONGEN;
        private Button IDH_ORIGLU2;
        private TextBox IDH_ORIGIN2;
        private Label label110;
        private Button IDH_REF;
        private TextBox IDH_ADJWGT;
        private TextBox IDH_WGTDED;
        private Label label114;
        private Label label113;
        private GroupBox groupBox1;
        private TextBox IDH_VALDED;
        private Label label115;
        private TextBox IDH_ORDTOT;
        private Label label119;
        private TextBox IDH_ORDCOS;
        private TextBox IDH_ORDQTY;
        private Label label120;
        private Label label121;
        private TextBox IDH_TIPTOT;
        private Label label116;
        private TextBox IDH_TIPCOS;
        private TextBox IDH_TIPWEI;
        private Label label117;
        private Label label118;
        private ComboBox IDH_UOM;
        private ComboBox IDH_PUOM;
        private ComboBox IDH_PURUOM;
        private CheckBox IDH_ISTRL;
        private CheckBox IDH_BOOKIN;
        private TextBox IDH_BOKSTA;
        private GroupBox SO_GRP;
        private GroupBox PO_GRP;
        private RadioButton IDH_USEAU;
        private RadioButton IDH_USERE;
        private TabPage oDeductions;
        private DataGridView oDeduction;
        private Label label122;
        private Label label123;
        private TextBox IDH_DEDVAL;
        private TextBox IDH_DEDWEI;
        private idh.wr1.grid.Deductions oDeductionGrid;
        private TextBox IDH_CUST;
#endregion

        private Button IDH_DISPCF;
        private TextBox IDH_DISPNM;
        private TextBox IDH_DISPCD;
        private Label label124;
        private Label label128;
        private TextBox IDH_ADDEX;
        private Label label127;
        private TextBox IDH_ADDCOS;
        private Label label126;
        private Label label125;
        private TextBox IDH_DISPAD;
        private TextBox IDH_EXPLDW;
        private Label label129;
	}
}
