﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace com.idh.wr1.form 
{
    public partial class WR1Help : Form
    {
        public WR1Help()
        {
            InitializeComponent();
        }

        public void setHelpFile( string sFile )
        {
            Viewer.Url = new Uri(sFile);
        }
    }
}
