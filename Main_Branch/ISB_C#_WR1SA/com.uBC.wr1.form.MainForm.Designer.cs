﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2008/12/16
 * Time: 06:00 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Collections;
using System.Windows.Forms;

using com.idh.bridge;
using com.idh.bridge.utils;
using com.idh.bridge.form;
using com.idh.bridge.lookups;
using com.idh.bridge.reports;
using com.idh.dbObjects.User;
using com.idh.bridge.error;

using com.idh.win.controls.SBO;

namespace com.uBC.wr1.form
{
	partial class MainForm
	{
        ///// <summary>
        ///// Designer variable used to keep track of non-visual components.
        ///// </summary>
        ///// 
        //private bool mbDoBP = false;
        //private System.ComponentModel.IContainer components = null;
        //private object moLastEnter = null;
        //private object moLastLeave = null;
        
        //private string[] soIgnoreControls = {"IDH_ADDCO","IDH_ADDCH"};

		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            DBOGridControl control1 = new DBOGridControl();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            DBOGridControl control2 = new DBOGridControl();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            DBOGridControl control3 = new DBOGridControl();
            this.label1 = new System.Windows.Forms.Label();
            this.IDH_JOBTTP = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_VEHREG = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WRORD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WRROW = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_VEH = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DRIVER = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TRLReg = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TRLNM = new com.idh.win.controls.SBO.SBOTextBox();
            this.oMainTabs = new System.Windows.Forms.TabControl();
            this.oTPWeighing = new System.Windows.Forms.TabPage();
            this.IDH_NEWWEI = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_TRWTE2 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TRWTE1 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_BPWGT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label131 = new System.Windows.Forms.Label();
            this.IDH_USEAU = new System.Windows.Forms.RadioButton();
            this.IDH_USERE = new System.Windows.Forms.RadioButton();
            this.PO_GRP = new System.Windows.Forms.GroupBox();
            this.IDH_PCOCUR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CCOCUR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DCOCUR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TIPCOS = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_ORDCOS = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_PRCOST = new com.idh.win.controls.SBO.SBOComboBox();
            this.label128 = new System.Windows.Forms.Label();
            this.IDH_ADDEX = new com.idh.win.controls.SBO.SBOTextBox();
            this.label127 = new System.Windows.Forms.Label();
            this.IDH_ADDCOS = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PUOM = new com.idh.win.controls.SBO.SBOComboBox();
            this.label118 = new System.Windows.Forms.Label();
            this.IDH_PURUOM = new com.idh.win.controls.SBO.SBOComboBox();
            this.label105 = new System.Windows.Forms.Label();
            this.IDH_ORDTOT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.IDH_PURWGT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ORDQTY = new com.idh.win.controls.SBO.SBOTextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.IDH_PURTOT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DOPO = new System.Windows.Forms.CheckBox();
            this.label121 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.IDH_TIPTOT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TAXO = new com.idh.win.controls.SBO.SBOTextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.IDH_PSTAT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TIPWEI = new com.idh.win.controls.SBO.SBOTextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.IDH_TOTCOS = new com.idh.win.controls.SBO.SBOTextBox();
            this.SO_GRP = new System.Windows.Forms.GroupBox();
            this.IDH_DCGCUR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSCHG = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_UOM = new com.idh.win.controls.SBO.SBOComboBox();
            this.label90 = new System.Windows.Forms.Label();
            this.IDH_VALDED = new com.idh.win.controls.SBO.SBOTextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.IDH_CUSTOT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DOORD = new System.Windows.Forms.CheckBox();
            this.IDH_DSCPRC = new com.idh.win.controls.SBO.SBOTextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.IDH_ADDCHR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DOARIP = new System.Windows.Forms.CheckBox();
            this.IDH_SUBTOT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DOARI = new System.Windows.Forms.CheckBox();
            this.IDH_TAX = new com.idh.win.controls.SBO.SBOTextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.IDH_NOVAT = new System.Windows.Forms.CheckBox();
            this.IDH_CASHMT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TOTAL = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_FOC = new System.Windows.Forms.CheckBox();
            this.label82 = new System.Windows.Forms.Label();
            this.IDH_RSTAT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.IDH_DISCNT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.IDH_CUSWEI = new com.idh.win.controls.SBO.SBOTextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.IDH_AINV = new System.Windows.Forms.CheckBox();
            this.label87 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.IDH_ALTITM = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_EXPLDW = new com.idh.win.controls.SBO.SBOTextBox();
            this.label126 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.IDH_ISTRL = new System.Windows.Forms.CheckBox();
            this.IDH_DISPAD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DISPCF = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_DISPNM = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DISPCD = new com.idh.win.controls.SBO.SBOTextBox();
            this.label124 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.IDH_ITMGRN = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ITMGRC = new com.idh.win.controls.SBO.SBOTextBox();
            this.Lbl1000007 = new System.Windows.Forms.Label();
            this.IDH_ITMCOD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DESC = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WASCL1 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WASMAT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_RORIGI = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ITCF = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_WASCF = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_ORLK = new com.idh.win.controls.SBO.SBOButton();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.IDH_PRONM = new com.idh.win.controls.SBO.SBOTextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.IDH_PROCD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PROCF = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_ADJWGT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WGTDED = new com.idh.win.controls.SBO.SBOTextBox();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.IDH_TAR2 = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_BUF2 = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_ACC2 = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_TAR1 = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_BUF1 = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_ACC1 = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_ACCB = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_TMR = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_REF = new com.idh.win.controls.SBO.SBOButton();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.IDH_CUSCM = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_RDWGT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WDT2 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SER2 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WEI2 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WDT1 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SER1 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WEI1 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TRLTar = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TARWEI = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WDTB = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SERB = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WEIB = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WEIG = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WEIBRG = new com.idh.win.controls.SBO.SBOComboBox();
            this.oTPCarrier = new System.Windows.Forms.TabPage();
            this.IDH_LICREG = new com.idh.win.controls.SBO.SBOTextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.IDH_PHONE = new com.idh.win.controls.SBO.SBOTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.IDH_ZIP = new com.idh.win.controls.SBO.SBOTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.IDH_CITY = new com.idh.win.controls.SBO.SBOTextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.IDH_BLOCK = new com.idh.win.controls.SBO.SBOTextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.IDH_WASAL = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_ADDRES = new com.idh.win.controls.SBO.SBOTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.IDH_STREET = new com.idh.win.controls.SBO.SBOTextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.IDH_CARREF = new com.idh.win.controls.SBO.SBOTextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.IDH_CONTNM = new com.idh.win.controls.SBO.SBOTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.oTPCustomer = new System.Windows.Forms.TabPage();
            this.label136 = new System.Windows.Forms.Label();
            this.IDH_FTORIG = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ORIGLU2 = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_ORIGIN2 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.IDH_SLCNO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PCD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SteId = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ROUTL = new com.idh.win.controls.SBO.SBOButton();
            this.label42 = new System.Windows.Forms.Label();
            this.IDH_SEQ = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ROUTE = new com.idh.win.controls.SBO.SBOTextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.IDH_CNA = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_CUSAL = new com.idh.win.controls.SBO.SBOButton();
            this.label40 = new System.Windows.Forms.Label();
            this.IDH_CUSLIC = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITETL = new com.idh.win.controls.SBO.SBOTextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.IDH_CUSPHO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSPOS = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_COUNTY = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSCIT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSBLO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSSTR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSADD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSCON = new com.idh.win.controls.SBO.SBOTextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.oTPProducer = new System.Windows.Forms.TabPage();
            this.IDH_PROAL = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_ORIGLU = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_PRDL = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_ORIGIN = new com.idh.win.controls.SBO.SBOTextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.IDH_PRDPHO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRDPOS = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRDCIT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.IDH_PRDBLO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRDSTR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRDADD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRDCRF = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRDCON = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WNAM = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WPRODU = new com.idh.win.controls.SBO.SBOTextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.oTPSite = new System.Windows.Forms.TabPage();
            this.IDH_SITAL = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_SITL = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_SITPHO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITPOS = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITCIT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.IDH_SITBLO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITSTR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITADD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITCRF = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITCON = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DISNAM = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DISSIT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.oDetails = new System.Windows.Forms.TabPage();
            this.oDORows = new com.idh.win.controls.WR1Grid();
            this.oAdditional = new System.Windows.Forms.TabPage();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.IDH_TADDCH = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TADDCO = new com.idh.win.controls.SBO.SBOTextBox();
            this.label132 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.IDH_ADDCHV = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ADDCOV = new com.idh.win.controls.SBO.SBOTextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.IDH_ADDCH = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ADDCO = new com.idh.win.controls.SBO.SBOTextBox();
            this.oAddGrid = new com.idh.wr1.grid.AdditionalExpenses();
            this.oDeductions = new System.Windows.Forms.TabPage();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.IDH_DEDVAL = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DEDWEI = new com.idh.win.controls.SBO.SBOTextBox();
            this.oDeductionGrid = new com.idh.wr1.grid.Deductions();
            this.btnSummary = new com.idh.win.controls.SBO.SBOButton();
            this.oPNavigation = new System.Windows.Forms.Panel();
            this.btFind = new com.idh.win.controls.SBO.SBOButton();
            this.btAdd = new com.idh.win.controls.SBO.SBOButton();
            this.btLast = new com.idh.win.controls.SBO.SBOButton();
            this.btNext = new com.idh.win.controls.SBO.SBOButton();
            this.btPrevious = new com.idh.win.controls.SBO.SBOButton();
            this.btFirst = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_SPECIN = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUST = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSTNM = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSCRF = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ONCS = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_USER = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CARNAM = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CARRIE = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITREF = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_HAZCN = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WASTTN = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_BOOKDT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ROW = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_BOOREF = new com.idh.win.controls.SBO.SBOTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.IDH_EXTWEI = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CONTNR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SEALNR = new com.idh.win.controls.SBO.SBOTextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.IDH_BOOKTM = new com.idh.win.controls.SBO.SBOTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.IDH_REGL = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_WOCF = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_CUSL = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_CSCF = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_CARL = new com.idh.win.controls.SBO.SBOButton();
            this.btn1 = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_BRANCH = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_TZONE = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_OBLED = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_AVDOC = new System.Windows.Forms.CheckBox();
            this.IDH_AVCONV = new System.Windows.Forms.CheckBox();
            this.IDH_CON = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_DOC = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_OS = new com.idh.win.controls.SBO.SBOButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.prg1 = new System.Windows.Forms.ToolStripProgressBar();
            this.LblVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.ItemInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.stl01 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblLastError = new System.Windows.Forms.ToolStripStatusLabel();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnHelp = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_CONGEN = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_BOOKIN = new System.Windows.Forms.CheckBox();
            this.IDH_BOKSTA = new com.idh.win.controls.SBO.SBOTextBox();
            this.uBC_TRNCd = new com.idh.win.controls.SBO.SBOComboBox();
            this.label130 = new System.Windows.Forms.Label();
            this.IDH_Amend = new com.idh.win.controls.SBO.SBOButton();
            this.IDH_STATUS = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_WRROW2 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WOCFL = new com.idh.win.controls.SBO.SBOButton();
            this.oMainTabs.SuspendLayout();
            this.oTPWeighing.SuspendLayout();
            this.PO_GRP.SuspendLayout();
            this.SO_GRP.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.oTPCarrier.SuspendLayout();
            this.oTPCustomer.SuspendLayout();
            this.oTPProducer.SuspendLayout();
            this.oTPSite.SuspendLayout();
            this.oDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oDORows)).BeginInit();
            this.oAdditional.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oAddGrid)).BeginInit();
            this.oDeductions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oDeductionGrid)).BeginInit();
            this.oPNavigation.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Disposal Type";
            // 
            // IDH_JOBTTP
            // 
            this.IDH_JOBTTP.BackColor = System.Drawing.Color.Transparent;
            this.IDH_JOBTTP.BlockPaint = false;
            this.IDH_JOBTTP.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_JOBTTP.CornerRadius = 2;
            this.IDH_JOBTTP.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_JOBTTP.DisplayDescription = false;
            this.IDH_JOBTTP.DropDownHight = 100;
            this.IDH_JOBTTP.DroppedDown = false;
            this.IDH_JOBTTP.EnabledColor = System.Drawing.Color.White;
            this.IDH_JOBTTP.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_JOBTTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_JOBTTP.HideDropDown = false;
            this.IDH_JOBTTP.Location = new System.Drawing.Point(110, 5);
            this.IDH_JOBTTP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_JOBTTP.Multiline = true;
            this.IDH_JOBTTP.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_JOBTTP.Name = "IDH_JOBTTP";
            this.IDH_JOBTTP.SelectedIndex = -1;
            this.IDH_JOBTTP.SelectedItem = null;
            this.IDH_JOBTTP.Size = new System.Drawing.Size(155, 14);
            this.IDH_JOBTTP.TabIndex = 0;
            this.IDH_JOBTTP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_JOBTTP.TextBoxReadOnly = true;
            this.IDH_JOBTTP.TextValue = "";
            // 
            // IDH_VEHREG
            // 
            this.IDH_VEHREG.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_VEHREG.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_VEHREG.BackColor = System.Drawing.Color.Transparent;
            this.IDH_VEHREG.BlockPaint = false;
            this.IDH_VEHREG.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_VEHREG.CornerRadius = 2;
            this.IDH_VEHREG.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_VEHREG.EnabledColor = System.Drawing.Color.White;
            this.IDH_VEHREG.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_VEHREG.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_VEHREG.Location = new System.Drawing.Point(110, 20);
            this.IDH_VEHREG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_VEHREG.Multiline = true;
            this.IDH_VEHREG.Name = "IDH_VEHREG";
            this.IDH_VEHREG.PasswordChar = '\0';
            this.IDH_VEHREG.ReadOnly = false;
            this.IDH_VEHREG.Size = new System.Drawing.Size(155, 14);
            this.IDH_VEHREG.TabIndex = 1;
            this.IDH_VEHREG.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_VEHREG.TextValue = "";
            this.IDH_VEHREG.WordWrap = true;
            // 
            // IDH_WRORD
            // 
            this.IDH_WRORD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WRORD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WRORD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WRORD.BlockPaint = false;
            this.IDH_WRORD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WRORD.CornerRadius = 2;
            this.IDH_WRORD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WRORD.EnabledColor = System.Drawing.Color.White;
            this.IDH_WRORD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WRORD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WRORD.Location = new System.Drawing.Point(110, 35);
            this.IDH_WRORD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WRORD.Multiline = true;
            this.IDH_WRORD.Name = "IDH_WRORD";
            this.IDH_WRORD.PasswordChar = '\0';
            this.IDH_WRORD.ReadOnly = false;
            this.IDH_WRORD.Size = new System.Drawing.Size(40, 14);
            this.IDH_WRORD.TabIndex = 162;
            this.IDH_WRORD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WRORD.TextValue = "";
            this.IDH_WRORD.WordWrap = true;
            // 
            // IDH_WRROW
            // 
            this.IDH_WRROW.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WRROW.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WRROW.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WRROW.BlockPaint = false;
            this.IDH_WRROW.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WRROW.CornerRadius = 2;
            this.IDH_WRROW.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WRROW.EnabledColor = System.Drawing.Color.White;
            this.IDH_WRROW.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WRROW.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WRROW.Location = new System.Drawing.Point(183, 35);
            this.IDH_WRROW.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WRROW.Multiline = true;
            this.IDH_WRROW.Name = "IDH_WRROW";
            this.IDH_WRROW.PasswordChar = '\0';
            this.IDH_WRROW.ReadOnly = false;
            this.IDH_WRROW.Size = new System.Drawing.Size(40, 14);
            this.IDH_WRROW.TabIndex = 163;
            this.IDH_WRROW.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WRROW.TextValue = "";
            this.IDH_WRROW.WordWrap = true;
            this.IDH_WRROW.Click += new System.EventHandler(this.IDH_WRROW_Click);
            // 
            // IDH_VEH
            // 
            this.IDH_VEH.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_VEH.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_VEH.BackColor = System.Drawing.Color.Transparent;
            this.IDH_VEH.BlockPaint = false;
            this.IDH_VEH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_VEH.CornerRadius = 2;
            this.IDH_VEH.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_VEH.EnabledColor = System.Drawing.Color.White;
            this.IDH_VEH.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_VEH.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_VEH.Location = new System.Drawing.Point(110, 50);
            this.IDH_VEH.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_VEH.Multiline = true;
            this.IDH_VEH.Name = "IDH_VEH";
            this.IDH_VEH.PasswordChar = '\0';
            this.IDH_VEH.ReadOnly = false;
            this.IDH_VEH.Size = new System.Drawing.Size(155, 14);
            this.IDH_VEH.TabIndex = 165;
            this.IDH_VEH.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_VEH.TextValue = "";
            this.IDH_VEH.WordWrap = true;
            // 
            // IDH_DRIVER
            // 
            this.IDH_DRIVER.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DRIVER.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DRIVER.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DRIVER.BlockPaint = false;
            this.IDH_DRIVER.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DRIVER.CornerRadius = 2;
            this.IDH_DRIVER.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DRIVER.EnabledColor = System.Drawing.Color.White;
            this.IDH_DRIVER.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DRIVER.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DRIVER.Location = new System.Drawing.Point(110, 65);
            this.IDH_DRIVER.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DRIVER.Multiline = true;
            this.IDH_DRIVER.Name = "IDH_DRIVER";
            this.IDH_DRIVER.PasswordChar = '\0';
            this.IDH_DRIVER.ReadOnly = false;
            this.IDH_DRIVER.Size = new System.Drawing.Size(155, 14);
            this.IDH_DRIVER.TabIndex = 166;
            this.IDH_DRIVER.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DRIVER.TextValue = "";
            this.IDH_DRIVER.WordWrap = true;
            // 
            // IDH_TRLReg
            // 
            this.IDH_TRLReg.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TRLReg.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TRLReg.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TRLReg.BlockPaint = false;
            this.IDH_TRLReg.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TRLReg.CornerRadius = 2;
            this.IDH_TRLReg.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TRLReg.EnabledColor = System.Drawing.Color.White;
            this.IDH_TRLReg.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TRLReg.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TRLReg.Location = new System.Drawing.Point(110, 80);
            this.IDH_TRLReg.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TRLReg.Multiline = true;
            this.IDH_TRLReg.Name = "IDH_TRLReg";
            this.IDH_TRLReg.PasswordChar = '\0';
            this.IDH_TRLReg.ReadOnly = false;
            this.IDH_TRLReg.Size = new System.Drawing.Size(155, 14);
            this.IDH_TRLReg.TabIndex = 167;
            this.IDH_TRLReg.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TRLReg.TextValue = "";
            this.IDH_TRLReg.WordWrap = true;
            // 
            // IDH_TRLNM
            // 
            this.IDH_TRLNM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TRLNM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TRLNM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TRLNM.BlockPaint = false;
            this.IDH_TRLNM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TRLNM.CornerRadius = 2;
            this.IDH_TRLNM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TRLNM.EnabledColor = System.Drawing.Color.White;
            this.IDH_TRLNM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TRLNM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TRLNM.Location = new System.Drawing.Point(110, 95);
            this.IDH_TRLNM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TRLNM.Multiline = true;
            this.IDH_TRLNM.Name = "IDH_TRLNM";
            this.IDH_TRLNM.PasswordChar = '\0';
            this.IDH_TRLNM.ReadOnly = false;
            this.IDH_TRLNM.Size = new System.Drawing.Size(155, 14);
            this.IDH_TRLNM.TabIndex = 168;
            this.IDH_TRLNM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TRLNM.TextValue = "";
            this.IDH_TRLNM.WordWrap = true;
            // 
            // oMainTabs
            // 
            this.oMainTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.oMainTabs.Controls.Add(this.oTPWeighing);
            this.oMainTabs.Controls.Add(this.oTPCarrier);
            this.oMainTabs.Controls.Add(this.oTPCustomer);
            this.oMainTabs.Controls.Add(this.oTPProducer);
            this.oMainTabs.Controls.Add(this.oTPSite);
            this.oMainTabs.Controls.Add(this.oDetails);
            this.oMainTabs.Controls.Add(this.oAdditional);
            this.oMainTabs.Controls.Add(this.oDeductions);
            this.oMainTabs.Location = new System.Drawing.Point(5, 188);
            this.oMainTabs.Name = "oMainTabs";
            this.oMainTabs.SelectedIndex = 0;
            this.oMainTabs.Size = new System.Drawing.Size(830, 332);
            this.oMainTabs.TabIndex = 4;
            // 
            // oTPWeighing
            // 
            this.oTPWeighing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPWeighing.Controls.Add(this.IDH_NEWWEI);
            this.oTPWeighing.Controls.Add(this.IDH_TRWTE2);
            this.oTPWeighing.Controls.Add(this.IDH_TRWTE1);
            this.oTPWeighing.Controls.Add(this.IDH_BPWGT);
            this.oTPWeighing.Controls.Add(this.label131);
            this.oTPWeighing.Controls.Add(this.IDH_USEAU);
            this.oTPWeighing.Controls.Add(this.IDH_USERE);
            this.oTPWeighing.Controls.Add(this.PO_GRP);
            this.oTPWeighing.Controls.Add(this.SO_GRP);
            this.oTPWeighing.Controls.Add(this.groupBox1);
            this.oTPWeighing.Controls.Add(this.IDH_ADJWGT);
            this.oTPWeighing.Controls.Add(this.IDH_WGTDED);
            this.oTPWeighing.Controls.Add(this.label114);
            this.oTPWeighing.Controls.Add(this.label113);
            this.oTPWeighing.Controls.Add(this.pictureBox1);
            this.oTPWeighing.Controls.Add(this.IDH_TAR2);
            this.oTPWeighing.Controls.Add(this.IDH_BUF2);
            this.oTPWeighing.Controls.Add(this.IDH_ACC2);
            this.oTPWeighing.Controls.Add(this.IDH_TAR1);
            this.oTPWeighing.Controls.Add(this.IDH_BUF1);
            this.oTPWeighing.Controls.Add(this.IDH_ACC1);
            this.oTPWeighing.Controls.Add(this.IDH_ACCB);
            this.oTPWeighing.Controls.Add(this.IDH_TMR);
            this.oTPWeighing.Controls.Add(this.IDH_REF);
            this.oTPWeighing.Controls.Add(this.label102);
            this.oTPWeighing.Controls.Add(this.label101);
            this.oTPWeighing.Controls.Add(this.label100);
            this.oTPWeighing.Controls.Add(this.label99);
            this.oTPWeighing.Controls.Add(this.label98);
            this.oTPWeighing.Controls.Add(this.label97);
            this.oTPWeighing.Controls.Add(this.label96);
            this.oTPWeighing.Controls.Add(this.label95);
            this.oTPWeighing.Controls.Add(this.label94);
            this.oTPWeighing.Controls.Add(this.IDH_CUSCM);
            this.oTPWeighing.Controls.Add(this.IDH_RDWGT);
            this.oTPWeighing.Controls.Add(this.IDH_WDT2);
            this.oTPWeighing.Controls.Add(this.IDH_SER2);
            this.oTPWeighing.Controls.Add(this.IDH_WEI2);
            this.oTPWeighing.Controls.Add(this.IDH_WDT1);
            this.oTPWeighing.Controls.Add(this.IDH_SER1);
            this.oTPWeighing.Controls.Add(this.IDH_WEI1);
            this.oTPWeighing.Controls.Add(this.IDH_TRLTar);
            this.oTPWeighing.Controls.Add(this.IDH_TARWEI);
            this.oTPWeighing.Controls.Add(this.IDH_WDTB);
            this.oTPWeighing.Controls.Add(this.IDH_SERB);
            this.oTPWeighing.Controls.Add(this.IDH_WEIB);
            this.oTPWeighing.Controls.Add(this.IDH_WEIG);
            this.oTPWeighing.Controls.Add(this.IDH_WEIBRG);
            this.oTPWeighing.Location = new System.Drawing.Point(4, 21);
            this.oTPWeighing.Name = "oTPWeighing";
            this.oTPWeighing.Padding = new System.Windows.Forms.Padding(3);
            this.oTPWeighing.Size = new System.Drawing.Size(822, 307);
            this.oTPWeighing.TabIndex = 0;
            this.oTPWeighing.Text = "Weighing";
            // 
            // IDH_NEWWEI
            // 
            this.IDH_NEWWEI.BlockPaint = false;
            this.IDH_NEWWEI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            this.IDH_NEWWEI.Caption = "New Weigh";
            this.IDH_NEWWEI.CornerRadius = 2;
            this.IDH_NEWWEI.DefaultButton = false;
            this.IDH_NEWWEI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_NEWWEI.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_NEWWEI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_NEWWEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_NEWWEI.Image = null;
            this.IDH_NEWWEI.Location = new System.Drawing.Point(270, 285);
            this.IDH_NEWWEI.Name = "IDH_NEWWEI";
            this.IDH_NEWWEI.Pressed = false;
            this.IDH_NEWWEI.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_NEWWEI.Size = new System.Drawing.Size(109, 14);
            this.IDH_NEWWEI.TabIndex = 1013;
            this.IDH_NEWWEI.Click += new System.EventHandler(this.IDH_NEWWEI_Click);
            // 
            // IDH_TRWTE2
            // 
            this.IDH_TRWTE2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TRWTE2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TRWTE2.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TRWTE2.BlockPaint = false;
            this.IDH_TRWTE2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TRWTE2.CornerRadius = 2;
            this.IDH_TRWTE2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TRWTE2.Enabled = false;
            this.IDH_TRWTE2.EnabledColor = System.Drawing.Color.White;
            this.IDH_TRWTE2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TRWTE2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TRWTE2.Location = new System.Drawing.Point(211, 165);
            this.IDH_TRWTE2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TRWTE2.Multiline = true;
            this.IDH_TRWTE2.Name = "IDH_TRWTE2";
            this.IDH_TRWTE2.PasswordChar = '\0';
            this.IDH_TRWTE2.ReadOnly = false;
            this.IDH_TRWTE2.Size = new System.Drawing.Size(54, 14);
            this.IDH_TRWTE2.TabIndex = 1012;
            this.IDH_TRWTE2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TRWTE2.TextValue = "";
            this.IDH_TRWTE2.WordWrap = true;
            // 
            // IDH_TRWTE1
            // 
            this.IDH_TRWTE1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TRWTE1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TRWTE1.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TRWTE1.BlockPaint = false;
            this.IDH_TRWTE1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TRWTE1.CornerRadius = 2;
            this.IDH_TRWTE1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TRWTE1.Enabled = false;
            this.IDH_TRWTE1.EnabledColor = System.Drawing.Color.White;
            this.IDH_TRWTE1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TRWTE1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TRWTE1.Location = new System.Drawing.Point(211, 150);
            this.IDH_TRWTE1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TRWTE1.Multiline = true;
            this.IDH_TRWTE1.Name = "IDH_TRWTE1";
            this.IDH_TRWTE1.PasswordChar = '\0';
            this.IDH_TRWTE1.ReadOnly = false;
            this.IDH_TRWTE1.Size = new System.Drawing.Size(54, 14);
            this.IDH_TRWTE1.TabIndex = 1011;
            this.IDH_TRWTE1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TRWTE1.TextValue = "";
            this.IDH_TRWTE1.WordWrap = true;
            // 
            // IDH_BPWGT
            // 
            this.IDH_BPWGT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_BPWGT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_BPWGT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_BPWGT.BlockPaint = false;
            this.IDH_BPWGT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BPWGT.CornerRadius = 2;
            this.IDH_BPWGT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BPWGT.EnabledColor = System.Drawing.Color.White;
            this.IDH_BPWGT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BPWGT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_BPWGT.Location = new System.Drawing.Point(101, 270);
            this.IDH_BPWGT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_BPWGT.Multiline = true;
            this.IDH_BPWGT.Name = "IDH_BPWGT";
            this.IDH_BPWGT.PasswordChar = '\0';
            this.IDH_BPWGT.ReadOnly = false;
            this.IDH_BPWGT.Size = new System.Drawing.Size(164, 14);
            this.IDH_BPWGT.TabIndex = 1010;
            this.IDH_BPWGT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BPWGT.TextValue = "";
            this.IDH_BPWGT.WordWrap = true;
            // 
            // label131
            // 
            this.label131.Location = new System.Drawing.Point(5, 270);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(90, 15);
            this.label131.TabIndex = 1009;
            this.label131.Text = "BP Weight";
            // 
            // IDH_USEAU
            // 
            this.IDH_USEAU.AutoSize = true;
            this.IDH_USEAU.Location = new System.Drawing.Point(214, 224);
            this.IDH_USEAU.Name = "IDH_USEAU";
            this.IDH_USEAU.Size = new System.Drawing.Size(40, 16);
            this.IDH_USEAU.TabIndex = 1008;
            this.IDH_USEAU.Text = "Use";
            this.IDH_USEAU.UseVisualStyleBackColor = true;
            this.IDH_USEAU.CheckedChanged += new System.EventHandler(this.IDH_USEAU_CheckedChanged);
            // 
            // IDH_USERE
            // 
            this.IDH_USERE.AutoSize = true;
            this.IDH_USERE.Checked = true;
            this.IDH_USERE.Location = new System.Drawing.Point(214, 210);
            this.IDH_USERE.Name = "IDH_USERE";
            this.IDH_USERE.Size = new System.Drawing.Size(40, 16);
            this.IDH_USERE.TabIndex = 1007;
            this.IDH_USERE.TabStop = true;
            this.IDH_USERE.Text = "Use";
            this.IDH_USERE.UseVisualStyleBackColor = true;
            this.IDH_USERE.CheckedChanged += new System.EventHandler(this.IDH_USERE_CheckedChanged);
            // 
            // PO_GRP
            // 
            this.PO_GRP.Controls.Add(this.IDH_PCOCUR);
            this.PO_GRP.Controls.Add(this.IDH_CCOCUR);
            this.PO_GRP.Controls.Add(this.IDH_DCOCUR);
            this.PO_GRP.Controls.Add(this.IDH_TIPCOS);
            this.PO_GRP.Controls.Add(this.IDH_ORDCOS);
            this.PO_GRP.Controls.Add(this.IDH_PRCOST);
            this.PO_GRP.Controls.Add(this.label128);
            this.PO_GRP.Controls.Add(this.IDH_ADDEX);
            this.PO_GRP.Controls.Add(this.label127);
            this.PO_GRP.Controls.Add(this.IDH_ADDCOS);
            this.PO_GRP.Controls.Add(this.IDH_PUOM);
            this.PO_GRP.Controls.Add(this.label118);
            this.PO_GRP.Controls.Add(this.IDH_PURUOM);
            this.PO_GRP.Controls.Add(this.label105);
            this.PO_GRP.Controls.Add(this.IDH_ORDTOT);
            this.PO_GRP.Controls.Add(this.label104);
            this.PO_GRP.Controls.Add(this.label119);
            this.PO_GRP.Controls.Add(this.IDH_PURWGT);
            this.PO_GRP.Controls.Add(this.IDH_ORDQTY);
            this.PO_GRP.Controls.Add(this.label103);
            this.PO_GRP.Controls.Add(this.label120);
            this.PO_GRP.Controls.Add(this.IDH_PURTOT);
            this.PO_GRP.Controls.Add(this.IDH_DOPO);
            this.PO_GRP.Controls.Add(this.label121);
            this.PO_GRP.Controls.Add(this.label106);
            this.PO_GRP.Controls.Add(this.IDH_TIPTOT);
            this.PO_GRP.Controls.Add(this.IDH_TAXO);
            this.PO_GRP.Controls.Add(this.label116);
            this.PO_GRP.Controls.Add(this.label107);
            this.PO_GRP.Controls.Add(this.IDH_PSTAT);
            this.PO_GRP.Controls.Add(this.IDH_TIPWEI);
            this.PO_GRP.Controls.Add(this.label108);
            this.PO_GRP.Controls.Add(this.label117);
            this.PO_GRP.Controls.Add(this.IDH_TOTCOS);
            this.PO_GRP.Enabled = false;
            this.PO_GRP.Location = new System.Drawing.Point(383, 73);
            this.PO_GRP.Name = "PO_GRP";
            this.PO_GRP.Size = new System.Drawing.Size(439, 107);
            this.PO_GRP.TabIndex = 1006;
            this.PO_GRP.TabStop = false;
            // 
            // IDH_PCOCUR
            // 
            this.IDH_PCOCUR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PCOCUR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PCOCUR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PCOCUR.BlockPaint = false;
            this.IDH_PCOCUR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PCOCUR.CornerRadius = 2;
            this.IDH_PCOCUR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PCOCUR.Enabled = false;
            this.IDH_PCOCUR.EnabledColor = System.Drawing.Color.White;
            this.IDH_PCOCUR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PCOCUR.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PCOCUR.Location = new System.Drawing.Point(345, 40);
            this.IDH_PCOCUR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PCOCUR.Multiline = true;
            this.IDH_PCOCUR.Name = "IDH_PCOCUR";
            this.IDH_PCOCUR.PasswordChar = '\0';
            this.IDH_PCOCUR.ReadOnly = false;
            this.IDH_PCOCUR.Size = new System.Drawing.Size(23, 14);
            this.IDH_PCOCUR.TabIndex = 818;
            this.IDH_PCOCUR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PCOCUR.TextValue = "";
            this.IDH_PCOCUR.WordWrap = true;
            // 
            // IDH_CCOCUR
            // 
            this.IDH_CCOCUR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CCOCUR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CCOCUR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CCOCUR.BlockPaint = false;
            this.IDH_CCOCUR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CCOCUR.CornerRadius = 2;
            this.IDH_CCOCUR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CCOCUR.Enabled = false;
            this.IDH_CCOCUR.EnabledColor = System.Drawing.Color.White;
            this.IDH_CCOCUR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CCOCUR.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CCOCUR.Location = new System.Drawing.Point(345, 25);
            this.IDH_CCOCUR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CCOCUR.Multiline = true;
            this.IDH_CCOCUR.Name = "IDH_CCOCUR";
            this.IDH_CCOCUR.PasswordChar = '\0';
            this.IDH_CCOCUR.ReadOnly = false;
            this.IDH_CCOCUR.Size = new System.Drawing.Size(23, 14);
            this.IDH_CCOCUR.TabIndex = 817;
            this.IDH_CCOCUR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CCOCUR.TextValue = "";
            this.IDH_CCOCUR.WordWrap = true;
            // 
            // IDH_DCOCUR
            // 
            this.IDH_DCOCUR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DCOCUR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DCOCUR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DCOCUR.BlockPaint = false;
            this.IDH_DCOCUR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DCOCUR.CornerRadius = 2;
            this.IDH_DCOCUR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DCOCUR.Enabled = false;
            this.IDH_DCOCUR.EnabledColor = System.Drawing.Color.White;
            this.IDH_DCOCUR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DCOCUR.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DCOCUR.Location = new System.Drawing.Point(345, 10);
            this.IDH_DCOCUR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DCOCUR.Multiline = true;
            this.IDH_DCOCUR.Name = "IDH_DCOCUR";
            this.IDH_DCOCUR.PasswordChar = '\0';
            this.IDH_DCOCUR.ReadOnly = false;
            this.IDH_DCOCUR.Size = new System.Drawing.Size(23, 14);
            this.IDH_DCOCUR.TabIndex = 816;
            this.IDH_DCOCUR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DCOCUR.TextValue = "";
            this.IDH_DCOCUR.WordWrap = true;
            // 
            // IDH_TIPCOS
            // 
            this.IDH_TIPCOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TIPCOS.BlockPaint = false;
            this.IDH_TIPCOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TIPCOS.CornerRadius = 2;
            this.IDH_TIPCOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TIPCOS.DisplayDescription = false;
            this.IDH_TIPCOS.DropDownHight = 100;
            this.IDH_TIPCOS.DroppedDown = false;
            this.IDH_TIPCOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_TIPCOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TIPCOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TIPCOS.HideDropDown = true;
            this.IDH_TIPCOS.Location = new System.Drawing.Point(214, 10);
            this.IDH_TIPCOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TIPCOS.Multiline = true;
            this.IDH_TIPCOS.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_TIPCOS.Name = "IDH_TIPCOS";
            this.IDH_TIPCOS.SelectedIndex = -1;
            this.IDH_TIPCOS.SelectedItem = null;
            this.IDH_TIPCOS.Size = new System.Drawing.Size(52, 14);
            this.IDH_TIPCOS.TabIndex = 815;
            this.IDH_TIPCOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TIPCOS.TextBoxReadOnly = false;
            this.IDH_TIPCOS.TextValue = "";
            // 
            // IDH_ORDCOS
            // 
            this.IDH_ORDCOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ORDCOS.BlockPaint = false;
            this.IDH_ORDCOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORDCOS.CornerRadius = 2;
            this.IDH_ORDCOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORDCOS.DisplayDescription = false;
            this.IDH_ORDCOS.DropDownHight = 100;
            this.IDH_ORDCOS.DroppedDown = false;
            this.IDH_ORDCOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_ORDCOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORDCOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ORDCOS.HideDropDown = true;
            this.IDH_ORDCOS.Location = new System.Drawing.Point(214, 25);
            this.IDH_ORDCOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ORDCOS.Multiline = true;
            this.IDH_ORDCOS.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_ORDCOS.Name = "IDH_ORDCOS";
            this.IDH_ORDCOS.SelectedIndex = -1;
            this.IDH_ORDCOS.SelectedItem = null;
            this.IDH_ORDCOS.Size = new System.Drawing.Size(52, 14);
            this.IDH_ORDCOS.TabIndex = 814;
            this.IDH_ORDCOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ORDCOS.TextBoxReadOnly = false;
            this.IDH_ORDCOS.TextValue = "";
            // 
            // IDH_PRCOST
            // 
            this.IDH_PRCOST.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRCOST.BlockPaint = false;
            this.IDH_PRCOST.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRCOST.CornerRadius = 2;
            this.IDH_PRCOST.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRCOST.DisplayDescription = false;
            this.IDH_PRCOST.DropDownHight = 100;
            this.IDH_PRCOST.DroppedDown = false;
            this.IDH_PRCOST.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRCOST.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRCOST.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PRCOST.HideDropDown = true;
            this.IDH_PRCOST.Location = new System.Drawing.Point(214, 40);
            this.IDH_PRCOST.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRCOST.Multiline = true;
            this.IDH_PRCOST.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_PRCOST.Name = "IDH_PRCOST";
            this.IDH_PRCOST.SelectedIndex = -1;
            this.IDH_PRCOST.SelectedItem = null;
            this.IDH_PRCOST.Size = new System.Drawing.Size(52, 14);
            this.IDH_PRCOST.TabIndex = 813;
            this.IDH_PRCOST.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRCOST.TextBoxReadOnly = false;
            this.IDH_PRCOST.TextValue = "";
            // 
            // label128
            // 
            this.label128.Location = new System.Drawing.Point(139, 70);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(70, 15);
            this.label128.TabIndex = 811;
            this.label128.Text = "Add. Expenses";
            // 
            // IDH_ADDEX
            // 
            this.IDH_ADDEX.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ADDEX.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ADDEX.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADDEX.BlockPaint = false;
            this.IDH_ADDEX.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADDEX.CornerRadius = 2;
            this.IDH_ADDEX.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADDEX.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADDEX.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADDEX.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ADDEX.Location = new System.Drawing.Point(214, 70);
            this.IDH_ADDEX.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ADDEX.Multiline = true;
            this.IDH_ADDEX.Name = "IDH_ADDEX";
            this.IDH_ADDEX.PasswordChar = '\0';
            this.IDH_ADDEX.ReadOnly = false;
            this.IDH_ADDEX.Size = new System.Drawing.Size(52, 14);
            this.IDH_ADDEX.TabIndex = 812;
            this.IDH_ADDEX.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ADDEX.TextValue = "";
            this.IDH_ADDEX.WordWrap = true;
            // 
            // label127
            // 
            this.label127.Location = new System.Drawing.Point(275, 55);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(70, 15);
            this.label127.TabIndex = 809;
            this.label127.Text = "Additional Cost";
            // 
            // IDH_ADDCOS
            // 
            this.IDH_ADDCOS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ADDCOS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ADDCOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADDCOS.BlockPaint = false;
            this.IDH_ADDCOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADDCOS.CornerRadius = 2;
            this.IDH_ADDCOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADDCOS.Enabled = false;
            this.IDH_ADDCOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADDCOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADDCOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ADDCOS.Location = new System.Drawing.Point(369, 55);
            this.IDH_ADDCOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ADDCOS.Multiline = true;
            this.IDH_ADDCOS.Name = "IDH_ADDCOS";
            this.IDH_ADDCOS.PasswordChar = '\0';
            this.IDH_ADDCOS.ReadOnly = false;
            this.IDH_ADDCOS.Size = new System.Drawing.Size(68, 14);
            this.IDH_ADDCOS.TabIndex = 810;
            this.IDH_ADDCOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ADDCOS.TextValue = "";
            this.IDH_ADDCOS.WordWrap = true;
            // 
            // IDH_PUOM
            // 
            this.IDH_PUOM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PUOM.BlockPaint = false;
            this.IDH_PUOM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PUOM.CornerRadius = 2;
            this.IDH_PUOM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PUOM.DisplayDescription = true;
            this.IDH_PUOM.DropDownHight = 100;
            this.IDH_PUOM.DroppedDown = false;
            this.IDH_PUOM.EnabledColor = System.Drawing.Color.White;
            this.IDH_PUOM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PUOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PUOM.HideDropDown = false;
            this.IDH_PUOM.Location = new System.Drawing.Point(108, 10);
            this.IDH_PUOM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PUOM.Multiline = true;
            this.IDH_PUOM.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_PUOM.Name = "IDH_PUOM";
            this.IDH_PUOM.SelectedIndex = -1;
            this.IDH_PUOM.SelectedItem = null;
            this.IDH_PUOM.Size = new System.Drawing.Size(54, 14);
            this.IDH_PUOM.TabIndex = 808;
            this.IDH_PUOM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PUOM.TextBoxReadOnly = true;
            this.IDH_PUOM.TextValue = "";
            // 
            // label118
            // 
            this.label118.Location = new System.Drawing.Point(3, 10);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(62, 15);
            this.label118.TabIndex = 109;
            this.label118.Text = "Disposal Qty";
            // 
            // IDH_PURUOM
            // 
            this.IDH_PURUOM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PURUOM.BlockPaint = false;
            this.IDH_PURUOM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PURUOM.CornerRadius = 2;
            this.IDH_PURUOM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PURUOM.DisplayDescription = true;
            this.IDH_PURUOM.DropDownHight = 100;
            this.IDH_PURUOM.DroppedDown = false;
            this.IDH_PURUOM.EnabledColor = System.Drawing.Color.White;
            this.IDH_PURUOM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PURUOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PURUOM.HideDropDown = false;
            this.IDH_PURUOM.Location = new System.Drawing.Point(108, 40);
            this.IDH_PURUOM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PURUOM.Multiline = true;
            this.IDH_PURUOM.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_PURUOM.Name = "IDH_PURUOM";
            this.IDH_PURUOM.SelectedIndex = -1;
            this.IDH_PURUOM.SelectedItem = null;
            this.IDH_PURUOM.Size = new System.Drawing.Size(54, 14);
            this.IDH_PURUOM.TabIndex = 807;
            this.IDH_PURUOM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PURUOM.TextBoxReadOnly = true;
            this.IDH_PURUOM.TextValue = "";
            // 
            // label105
            // 
            this.label105.Location = new System.Drawing.Point(3, 40);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(62, 15);
            this.label105.TabIndex = 96;
            this.label105.Text = "Purchase Qty";
            // 
            // IDH_ORDTOT
            // 
            this.IDH_ORDTOT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ORDTOT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ORDTOT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ORDTOT.BlockPaint = false;
            this.IDH_ORDTOT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORDTOT.CornerRadius = 2;
            this.IDH_ORDTOT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORDTOT.Enabled = false;
            this.IDH_ORDTOT.EnabledColor = System.Drawing.Color.White;
            this.IDH_ORDTOT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORDTOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ORDTOT.Location = new System.Drawing.Point(369, 25);
            this.IDH_ORDTOT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ORDTOT.Multiline = true;
            this.IDH_ORDTOT.Name = "IDH_ORDTOT";
            this.IDH_ORDTOT.PasswordChar = '\0';
            this.IDH_ORDTOT.ReadOnly = false;
            this.IDH_ORDTOT.Size = new System.Drawing.Size(68, 14);
            this.IDH_ORDTOT.TabIndex = 115;
            this.IDH_ORDTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ORDTOT.TextValue = "";
            this.IDH_ORDTOT.WordWrap = true;
            // 
            // label104
            // 
            this.label104.Location = new System.Drawing.Point(164, 40);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(44, 15);
            this.label104.TabIndex = 92;
            this.label104.Text = "Cost";
            // 
            // label119
            // 
            this.label119.Location = new System.Drawing.Point(275, 25);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(70, 15);
            this.label119.TabIndex = 114;
            this.label119.Text = "Order Cost";
            // 
            // IDH_PURWGT
            // 
            this.IDH_PURWGT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PURWGT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PURWGT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PURWGT.BlockPaint = false;
            this.IDH_PURWGT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PURWGT.CornerRadius = 2;
            this.IDH_PURWGT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PURWGT.EnabledColor = System.Drawing.Color.White;
            this.IDH_PURWGT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PURWGT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PURWGT.Location = new System.Drawing.Point(66, 40);
            this.IDH_PURWGT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PURWGT.Multiline = true;
            this.IDH_PURWGT.Name = "IDH_PURWGT";
            this.IDH_PURWGT.PasswordChar = '\0';
            this.IDH_PURWGT.ReadOnly = false;
            this.IDH_PURWGT.Size = new System.Drawing.Size(41, 14);
            this.IDH_PURWGT.TabIndex = 90;
            this.IDH_PURWGT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PURWGT.TextValue = "";
            this.IDH_PURWGT.WordWrap = true;
            // 
            // IDH_ORDQTY
            // 
            this.IDH_ORDQTY.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ORDQTY.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ORDQTY.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ORDQTY.BlockPaint = false;
            this.IDH_ORDQTY.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORDQTY.CornerRadius = 2;
            this.IDH_ORDQTY.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORDQTY.EnabledColor = System.Drawing.Color.White;
            this.IDH_ORDQTY.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORDQTY.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ORDQTY.Location = new System.Drawing.Point(66, 25);
            this.IDH_ORDQTY.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ORDQTY.Multiline = true;
            this.IDH_ORDQTY.Name = "IDH_ORDQTY";
            this.IDH_ORDQTY.PasswordChar = '\0';
            this.IDH_ORDQTY.ReadOnly = false;
            this.IDH_ORDQTY.Size = new System.Drawing.Size(41, 14);
            this.IDH_ORDQTY.TabIndex = 110;
            this.IDH_ORDQTY.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ORDQTY.TextValue = "";
            this.IDH_ORDQTY.WordWrap = true;
            // 
            // label103
            // 
            this.label103.Location = new System.Drawing.Point(275, 40);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(70, 15);
            this.label103.TabIndex = 94;
            this.label103.Text = "Purchase Cost";
            // 
            // label120
            // 
            this.label120.Location = new System.Drawing.Point(137, 25);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(76, 15);
            this.label120.TabIndex = 112;
            this.label120.Text = "Cost ( per Unit )";
            // 
            // IDH_PURTOT
            // 
            this.IDH_PURTOT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PURTOT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PURTOT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PURTOT.BlockPaint = false;
            this.IDH_PURTOT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PURTOT.CornerRadius = 2;
            this.IDH_PURTOT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PURTOT.Enabled = false;
            this.IDH_PURTOT.EnabledColor = System.Drawing.Color.White;
            this.IDH_PURTOT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PURTOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PURTOT.Location = new System.Drawing.Point(369, 40);
            this.IDH_PURTOT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PURTOT.Multiline = true;
            this.IDH_PURTOT.Name = "IDH_PURTOT";
            this.IDH_PURTOT.PasswordChar = '\0';
            this.IDH_PURTOT.ReadOnly = false;
            this.IDH_PURTOT.Size = new System.Drawing.Size(68, 14);
            this.IDH_PURTOT.TabIndex = 95;
            this.IDH_PURTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PURTOT.TextValue = "";
            this.IDH_PURTOT.WordWrap = true;
            // 
            // IDH_DOPO
            // 
            this.IDH_DOPO.AutoSize = true;
            this.IDH_DOPO.Location = new System.Drawing.Point(3, 80);
            this.IDH_DOPO.Name = "IDH_DOPO";
            this.IDH_DOPO.Size = new System.Drawing.Size(118, 16);
            this.IDH_DOPO.TabIndex = 803;
            this.IDH_DOPO.Text = "Create Purchase Order";
            this.IDH_DOPO.UseVisualStyleBackColor = true;
            this.IDH_DOPO.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // label121
            // 
            this.label121.Location = new System.Drawing.Point(3, 25);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(62, 15);
            this.label121.TabIndex = 116;
            this.label121.Text = "Order Qty";
            // 
            // label106
            // 
            this.label106.Location = new System.Drawing.Point(275, 70);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(70, 15);
            this.label106.TabIndex = 97;
            this.label106.Text = "Tax";
            // 
            // IDH_TIPTOT
            // 
            this.IDH_TIPTOT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TIPTOT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TIPTOT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TIPTOT.BlockPaint = false;
            this.IDH_TIPTOT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TIPTOT.CornerRadius = 2;
            this.IDH_TIPTOT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TIPTOT.Enabled = false;
            this.IDH_TIPTOT.EnabledColor = System.Drawing.Color.White;
            this.IDH_TIPTOT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TIPTOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TIPTOT.Location = new System.Drawing.Point(369, 10);
            this.IDH_TIPTOT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TIPTOT.Multiline = true;
            this.IDH_TIPTOT.Name = "IDH_TIPTOT";
            this.IDH_TIPTOT.PasswordChar = '\0';
            this.IDH_TIPTOT.ReadOnly = false;
            this.IDH_TIPTOT.Size = new System.Drawing.Size(68, 14);
            this.IDH_TIPTOT.TabIndex = 108;
            this.IDH_TIPTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TIPTOT.TextValue = "";
            this.IDH_TIPTOT.WordWrap = true;
            // 
            // IDH_TAXO
            // 
            this.IDH_TAXO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TAXO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TAXO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TAXO.BlockPaint = false;
            this.IDH_TAXO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TAXO.CornerRadius = 2;
            this.IDH_TAXO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TAXO.Enabled = false;
            this.IDH_TAXO.EnabledColor = System.Drawing.Color.White;
            this.IDH_TAXO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TAXO.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TAXO.Location = new System.Drawing.Point(369, 70);
            this.IDH_TAXO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TAXO.Multiline = true;
            this.IDH_TAXO.Name = "IDH_TAXO";
            this.IDH_TAXO.PasswordChar = '\0';
            this.IDH_TAXO.ReadOnly = false;
            this.IDH_TAXO.Size = new System.Drawing.Size(68, 14);
            this.IDH_TAXO.TabIndex = 98;
            this.IDH_TAXO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TAXO.TextValue = "";
            this.IDH_TAXO.WordWrap = true;
            // 
            // label116
            // 
            this.label116.Location = new System.Drawing.Point(275, 10);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(70, 15);
            this.label116.TabIndex = 107;
            this.label116.Text = "Disposal Cost";
            // 
            // label107
            // 
            this.label107.Location = new System.Drawing.Point(164, 85);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(50, 16);
            this.label107.TabIndex = 100;
            this.label107.Text = "Status";
            // 
            // IDH_PSTAT
            // 
            this.IDH_PSTAT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PSTAT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PSTAT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PSTAT.BlockPaint = false;
            this.IDH_PSTAT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PSTAT.CornerRadius = 2;
            this.IDH_PSTAT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PSTAT.Enabled = false;
            this.IDH_PSTAT.EnabledColor = System.Drawing.Color.White;
            this.IDH_PSTAT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PSTAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PSTAT.Location = new System.Drawing.Point(214, 85);
            this.IDH_PSTAT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PSTAT.Multiline = true;
            this.IDH_PSTAT.Name = "IDH_PSTAT";
            this.IDH_PSTAT.PasswordChar = '\0';
            this.IDH_PSTAT.ReadOnly = false;
            this.IDH_PSTAT.Size = new System.Drawing.Size(52, 14);
            this.IDH_PSTAT.TabIndex = 99;
            this.IDH_PSTAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PSTAT.TextValue = "";
            this.IDH_PSTAT.WordWrap = true;
            // 
            // IDH_TIPWEI
            // 
            this.IDH_TIPWEI.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TIPWEI.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TIPWEI.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TIPWEI.BlockPaint = false;
            this.IDH_TIPWEI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TIPWEI.CornerRadius = 2;
            this.IDH_TIPWEI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TIPWEI.EnabledColor = System.Drawing.Color.White;
            this.IDH_TIPWEI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TIPWEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TIPWEI.Location = new System.Drawing.Point(66, 10);
            this.IDH_TIPWEI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TIPWEI.Multiline = true;
            this.IDH_TIPWEI.Name = "IDH_TIPWEI";
            this.IDH_TIPWEI.PasswordChar = '\0';
            this.IDH_TIPWEI.ReadOnly = false;
            this.IDH_TIPWEI.Size = new System.Drawing.Size(41, 14);
            this.IDH_TIPWEI.TabIndex = 103;
            this.IDH_TIPWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TIPWEI.TextValue = "";
            this.IDH_TIPWEI.WordWrap = true;
            // 
            // label108
            // 
            this.label108.Location = new System.Drawing.Point(275, 85);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(70, 15);
            this.label108.TabIndex = 101;
            this.label108.Text = "Total Cost";
            // 
            // label117
            // 
            this.label117.Location = new System.Drawing.Point(164, 10);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(44, 15);
            this.label117.TabIndex = 105;
            this.label117.Text = "Cost";
            // 
            // IDH_TOTCOS
            // 
            this.IDH_TOTCOS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TOTCOS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TOTCOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TOTCOS.BlockPaint = false;
            this.IDH_TOTCOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TOTCOS.CornerRadius = 2;
            this.IDH_TOTCOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TOTCOS.Enabled = false;
            this.IDH_TOTCOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_TOTCOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TOTCOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TOTCOS.Location = new System.Drawing.Point(369, 85);
            this.IDH_TOTCOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TOTCOS.Multiline = true;
            this.IDH_TOTCOS.Name = "IDH_TOTCOS";
            this.IDH_TOTCOS.PasswordChar = '\0';
            this.IDH_TOTCOS.ReadOnly = false;
            this.IDH_TOTCOS.Size = new System.Drawing.Size(68, 14);
            this.IDH_TOTCOS.TabIndex = 102;
            this.IDH_TOTCOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TOTCOS.TextValue = "";
            this.IDH_TOTCOS.WordWrap = true;
            // 
            // SO_GRP
            // 
            this.SO_GRP.Controls.Add(this.IDH_DCGCUR);
            this.SO_GRP.Controls.Add(this.IDH_CUSCHG);
            this.SO_GRP.Controls.Add(this.IDH_UOM);
            this.SO_GRP.Controls.Add(this.label90);
            this.SO_GRP.Controls.Add(this.IDH_VALDED);
            this.SO_GRP.Controls.Add(this.label93);
            this.SO_GRP.Controls.Add(this.label115);
            this.SO_GRP.Controls.Add(this.label92);
            this.SO_GRP.Controls.Add(this.label91);
            this.SO_GRP.Controls.Add(this.IDH_CUSTOT);
            this.SO_GRP.Controls.Add(this.IDH_DOORD);
            this.SO_GRP.Controls.Add(this.IDH_DSCPRC);
            this.SO_GRP.Controls.Add(this.label89);
            this.SO_GRP.Controls.Add(this.IDH_ADDCHR);
            this.SO_GRP.Controls.Add(this.IDH_DOARIP);
            this.SO_GRP.Controls.Add(this.IDH_SUBTOT);
            this.SO_GRP.Controls.Add(this.IDH_DOARI);
            this.SO_GRP.Controls.Add(this.IDH_TAX);
            this.SO_GRP.Controls.Add(this.label88);
            this.SO_GRP.Controls.Add(this.IDH_NOVAT);
            this.SO_GRP.Controls.Add(this.IDH_CASHMT);
            this.SO_GRP.Controls.Add(this.IDH_TOTAL);
            this.SO_GRP.Controls.Add(this.IDH_FOC);
            this.SO_GRP.Controls.Add(this.label82);
            this.SO_GRP.Controls.Add(this.IDH_RSTAT);
            this.SO_GRP.Controls.Add(this.label83);
            this.SO_GRP.Controls.Add(this.IDH_DISCNT);
            this.SO_GRP.Controls.Add(this.label84);
            this.SO_GRP.Controls.Add(this.IDH_CUSWEI);
            this.SO_GRP.Controls.Add(this.label85);
            this.SO_GRP.Controls.Add(this.label86);
            this.SO_GRP.Controls.Add(this.IDH_AINV);
            this.SO_GRP.Controls.Add(this.label87);
            this.SO_GRP.Location = new System.Drawing.Point(383, 182);
            this.SO_GRP.Margin = new System.Windows.Forms.Padding(2);
            this.SO_GRP.Name = "SO_GRP";
            this.SO_GRP.Padding = new System.Windows.Forms.Padding(2);
            this.SO_GRP.Size = new System.Drawing.Size(439, 122);
            this.SO_GRP.TabIndex = 1006;
            this.SO_GRP.TabStop = false;
            // 
            // IDH_DCGCUR
            // 
            this.IDH_DCGCUR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DCGCUR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DCGCUR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DCGCUR.BlockPaint = false;
            this.IDH_DCGCUR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DCGCUR.CornerRadius = 2;
            this.IDH_DCGCUR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DCGCUR.Enabled = false;
            this.IDH_DCGCUR.EnabledColor = System.Drawing.Color.White;
            this.IDH_DCGCUR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DCGCUR.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DCGCUR.Location = new System.Drawing.Point(345, 10);
            this.IDH_DCGCUR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DCGCUR.Multiline = true;
            this.IDH_DCGCUR.Name = "IDH_DCGCUR";
            this.IDH_DCGCUR.PasswordChar = '\0';
            this.IDH_DCGCUR.ReadOnly = false;
            this.IDH_DCGCUR.Size = new System.Drawing.Size(23, 14);
            this.IDH_DCGCUR.TabIndex = 819;
            this.IDH_DCGCUR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DCGCUR.TextValue = "";
            this.IDH_DCGCUR.WordWrap = true;
            // 
            // IDH_CUSCHG
            // 
            this.IDH_CUSCHG.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSCHG.BlockPaint = false;
            this.IDH_CUSCHG.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSCHG.CornerRadius = 2;
            this.IDH_CUSCHG.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSCHG.DisplayDescription = false;
            this.IDH_CUSCHG.DropDownHight = 100;
            this.IDH_CUSCHG.DroppedDown = false;
            this.IDH_CUSCHG.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSCHG.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSCHG.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSCHG.HideDropDown = true;
            this.IDH_CUSCHG.Location = new System.Drawing.Point(215, 10);
            this.IDH_CUSCHG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSCHG.Multiline = true;
            this.IDH_CUSCHG.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_CUSCHG.Name = "IDH_CUSCHG";
            this.IDH_CUSCHG.SelectedIndex = -1;
            this.IDH_CUSCHG.SelectedItem = null;
            this.IDH_CUSCHG.Size = new System.Drawing.Size(52, 14);
            this.IDH_CUSCHG.TabIndex = 807;
            this.IDH_CUSCHG.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSCHG.TextBoxReadOnly = false;
            this.IDH_CUSCHG.TextValue = "";
            // 
            // IDH_UOM
            // 
            this.IDH_UOM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_UOM.BlockPaint = false;
            this.IDH_UOM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_UOM.CornerRadius = 2;
            this.IDH_UOM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_UOM.DisplayDescription = true;
            this.IDH_UOM.DropDownHight = 100;
            this.IDH_UOM.DroppedDown = false;
            this.IDH_UOM.EnabledColor = System.Drawing.Color.White;
            this.IDH_UOM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_UOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_UOM.HideDropDown = false;
            this.IDH_UOM.Location = new System.Drawing.Point(109, 10);
            this.IDH_UOM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_UOM.Multiline = true;
            this.IDH_UOM.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_UOM.Name = "IDH_UOM";
            this.IDH_UOM.SelectedIndex = -1;
            this.IDH_UOM.SelectedItem = null;
            this.IDH_UOM.Size = new System.Drawing.Size(54, 14);
            this.IDH_UOM.TabIndex = 806;
            this.IDH_UOM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_UOM.TextBoxReadOnly = true;
            this.IDH_UOM.TextValue = "";
            // 
            // label90
            // 
            this.label90.Location = new System.Drawing.Point(4, 10);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(62, 14);
            this.label90.TabIndex = 89;
            this.label90.Text = "Cust. Qty";
            // 
            // IDH_VALDED
            // 
            this.IDH_VALDED.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_VALDED.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_VALDED.BackColor = System.Drawing.Color.Transparent;
            this.IDH_VALDED.BlockPaint = false;
            this.IDH_VALDED.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_VALDED.CornerRadius = 2;
            this.IDH_VALDED.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_VALDED.Enabled = false;
            this.IDH_VALDED.EnabledColor = System.Drawing.Color.White;
            this.IDH_VALDED.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_VALDED.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_VALDED.Location = new System.Drawing.Point(369, 85);
            this.IDH_VALDED.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_VALDED.Multiline = true;
            this.IDH_VALDED.Name = "IDH_VALDED";
            this.IDH_VALDED.PasswordChar = '\0';
            this.IDH_VALDED.ReadOnly = false;
            this.IDH_VALDED.Size = new System.Drawing.Size(67, 14);
            this.IDH_VALDED.TabIndex = 157;
            this.IDH_VALDED.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_VALDED.TextValue = "";
            this.IDH_VALDED.WordWrap = true;
            // 
            // label93
            // 
            this.label93.Location = new System.Drawing.Point(132, 70);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(49, 14);
            this.label93.TabIndex = 92;
            this.label93.Text = "Pay Mth.";
            // 
            // label115
            // 
            this.label115.Location = new System.Drawing.Point(277, 85);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(70, 15);
            this.label115.TabIndex = 156;
            this.label115.Text = "Deduction";
            // 
            // label92
            // 
            this.label92.Location = new System.Drawing.Point(132, 55);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(49, 13);
            this.label92.TabIndex = 91;
            this.label92.Text = "Status";
            // 
            // label91
            // 
            this.label91.Location = new System.Drawing.Point(165, 25);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(49, 15);
            this.label91.TabIndex = 25;
            this.label91.Text = "Discount";
            // 
            // IDH_CUSTOT
            // 
            this.IDH_CUSTOT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSTOT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSTOT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSTOT.BlockPaint = false;
            this.IDH_CUSTOT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSTOT.CornerRadius = 2;
            this.IDH_CUSTOT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSTOT.Enabled = false;
            this.IDH_CUSTOT.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSTOT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSTOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSTOT.Location = new System.Drawing.Point(369, 10);
            this.IDH_CUSTOT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSTOT.Multiline = true;
            this.IDH_CUSTOT.Name = "IDH_CUSTOT";
            this.IDH_CUSTOT.PasswordChar = '\0';
            this.IDH_CUSTOT.ReadOnly = false;
            this.IDH_CUSTOT.Size = new System.Drawing.Size(67, 14);
            this.IDH_CUSTOT.TabIndex = 147;
            this.IDH_CUSTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSTOT.TextValue = "";
            this.IDH_CUSTOT.WordWrap = true;
            // 
            // IDH_DOORD
            // 
            this.IDH_DOORD.Location = new System.Drawing.Point(4, 40);
            this.IDH_DOORD.Name = "IDH_DOORD";
            this.IDH_DOORD.Size = new System.Drawing.Size(82, 14);
            this.IDH_DOORD.TabIndex = 800;
            this.IDH_DOORD.Text = "Account";
            this.IDH_DOORD.UseVisualStyleBackColor = true;
            this.IDH_DOORD.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // IDH_DSCPRC
            // 
            this.IDH_DSCPRC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DSCPRC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DSCPRC.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DSCPRC.BlockPaint = false;
            this.IDH_DSCPRC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DSCPRC.CornerRadius = 2;
            this.IDH_DSCPRC.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DSCPRC.EnabledColor = System.Drawing.Color.White;
            this.IDH_DSCPRC.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DSCPRC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DSCPRC.Location = new System.Drawing.Point(369, 25);
            this.IDH_DSCPRC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DSCPRC.Multiline = true;
            this.IDH_DSCPRC.Name = "IDH_DSCPRC";
            this.IDH_DSCPRC.PasswordChar = '\0';
            this.IDH_DSCPRC.ReadOnly = false;
            this.IDH_DSCPRC.Size = new System.Drawing.Size(67, 14);
            this.IDH_DSCPRC.TabIndex = 149;
            this.IDH_DSCPRC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DSCPRC.TextValue = "";
            this.IDH_DSCPRC.WordWrap = true;
            // 
            // label89
            // 
            this.label89.Location = new System.Drawing.Point(164, 10);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(44, 15);
            this.label89.TabIndex = 18;
            this.label89.Text = "Charge";
            // 
            // IDH_ADDCHR
            // 
            this.IDH_ADDCHR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ADDCHR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ADDCHR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADDCHR.BlockPaint = false;
            this.IDH_ADDCHR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADDCHR.CornerRadius = 2;
            this.IDH_ADDCHR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADDCHR.Enabled = false;
            this.IDH_ADDCHR.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADDCHR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADDCHR.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ADDCHR.Location = new System.Drawing.Point(369, 55);
            this.IDH_ADDCHR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ADDCHR.Multiline = true;
            this.IDH_ADDCHR.Name = "IDH_ADDCHR";
            this.IDH_ADDCHR.PasswordChar = '\0';
            this.IDH_ADDCHR.ReadOnly = false;
            this.IDH_ADDCHR.Size = new System.Drawing.Size(67, 14);
            this.IDH_ADDCHR.TabIndex = 150;
            this.IDH_ADDCHR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ADDCHR.TextValue = "";
            this.IDH_ADDCHR.WordWrap = true;
            // 
            // IDH_DOARIP
            // 
            this.IDH_DOARIP.Enabled = false;
            this.IDH_DOARIP.Location = new System.Drawing.Point(4, 70);
            this.IDH_DOARIP.Name = "IDH_DOARIP";
            this.IDH_DOARIP.Size = new System.Drawing.Size(133, 14);
            this.IDH_DOARIP.TabIndex = 802;
            this.IDH_DOARIP.Text = "AR Invoice + Payment";
            this.IDH_DOARIP.UseVisualStyleBackColor = true;
            this.IDH_DOARIP.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // IDH_SUBTOT
            // 
            this.IDH_SUBTOT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SUBTOT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SUBTOT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SUBTOT.BlockPaint = false;
            this.IDH_SUBTOT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SUBTOT.CornerRadius = 2;
            this.IDH_SUBTOT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SUBTOT.Enabled = false;
            this.IDH_SUBTOT.EnabledColor = System.Drawing.Color.White;
            this.IDH_SUBTOT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SUBTOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SUBTOT.Location = new System.Drawing.Point(369, 40);
            this.IDH_SUBTOT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SUBTOT.Multiline = true;
            this.IDH_SUBTOT.Name = "IDH_SUBTOT";
            this.IDH_SUBTOT.PasswordChar = '\0';
            this.IDH_SUBTOT.ReadOnly = false;
            this.IDH_SUBTOT.Size = new System.Drawing.Size(67, 14);
            this.IDH_SUBTOT.TabIndex = 151;
            this.IDH_SUBTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SUBTOT.TextValue = "";
            this.IDH_SUBTOT.WordWrap = true;
            // 
            // IDH_DOARI
            // 
            this.IDH_DOARI.Location = new System.Drawing.Point(4, 55);
            this.IDH_DOARI.Name = "IDH_DOARI";
            this.IDH_DOARI.Size = new System.Drawing.Size(80, 14);
            this.IDH_DOARI.TabIndex = 801;
            this.IDH_DOARI.Text = "Cash";
            this.IDH_DOARI.UseVisualStyleBackColor = true;
            this.IDH_DOARI.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // IDH_TAX
            // 
            this.IDH_TAX.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TAX.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TAX.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TAX.BlockPaint = false;
            this.IDH_TAX.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TAX.CornerRadius = 2;
            this.IDH_TAX.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TAX.Enabled = false;
            this.IDH_TAX.EnabledColor = System.Drawing.Color.White;
            this.IDH_TAX.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TAX.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TAX.Location = new System.Drawing.Point(369, 70);
            this.IDH_TAX.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TAX.Multiline = true;
            this.IDH_TAX.Name = "IDH_TAX";
            this.IDH_TAX.PasswordChar = '\0';
            this.IDH_TAX.ReadOnly = false;
            this.IDH_TAX.Size = new System.Drawing.Size(67, 14);
            this.IDH_TAX.TabIndex = 153;
            this.IDH_TAX.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TAX.TextValue = "";
            this.IDH_TAX.WordWrap = true;
            // 
            // label88
            // 
            this.label88.Location = new System.Drawing.Point(251, 25);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(18, 23);
            this.label88.TabIndex = 27;
            this.label88.Text = "%";
            // 
            // IDH_NOVAT
            // 
            this.IDH_NOVAT.Location = new System.Drawing.Point(154, 100);
            this.IDH_NOVAT.Name = "IDH_NOVAT";
            this.IDH_NOVAT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.IDH_NOVAT.Size = new System.Drawing.Size(113, 14);
            this.IDH_NOVAT.TabIndex = 805;
            this.IDH_NOVAT.Text = "Don\'t Charge VAT  ";
            this.IDH_NOVAT.UseVisualStyleBackColor = true;
            this.IDH_NOVAT.CheckedChanged += new System.EventHandler(this.IDH_NOVATCheckedChanged);
            // 
            // IDH_CASHMT
            // 
            this.IDH_CASHMT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CASHMT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CASHMT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CASHMT.BlockPaint = false;
            this.IDH_CASHMT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CASHMT.CornerRadius = 2;
            this.IDH_CASHMT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CASHMT.Enabled = false;
            this.IDH_CASHMT.EnabledColor = System.Drawing.Color.White;
            this.IDH_CASHMT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CASHMT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CASHMT.Location = new System.Drawing.Point(182, 70);
            this.IDH_CASHMT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CASHMT.Multiline = true;
            this.IDH_CASHMT.Name = "IDH_CASHMT";
            this.IDH_CASHMT.PasswordChar = '\0';
            this.IDH_CASHMT.ReadOnly = false;
            this.IDH_CASHMT.Size = new System.Drawing.Size(51, 14);
            this.IDH_CASHMT.TabIndex = 154;
            this.IDH_CASHMT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CASHMT.TextValue = "";
            this.IDH_CASHMT.WordWrap = true;
            // 
            // IDH_TOTAL
            // 
            this.IDH_TOTAL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TOTAL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TOTAL.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TOTAL.BlockPaint = false;
            this.IDH_TOTAL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TOTAL.CornerRadius = 2;
            this.IDH_TOTAL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TOTAL.Enabled = false;
            this.IDH_TOTAL.EnabledColor = System.Drawing.Color.White;
            this.IDH_TOTAL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TOTAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TOTAL.Location = new System.Drawing.Point(369, 100);
            this.IDH_TOTAL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TOTAL.Multiline = true;
            this.IDH_TOTAL.Name = "IDH_TOTAL";
            this.IDH_TOTAL.PasswordChar = '\0';
            this.IDH_TOTAL.ReadOnly = false;
            this.IDH_TOTAL.Size = new System.Drawing.Size(67, 14);
            this.IDH_TOTAL.TabIndex = 155;
            this.IDH_TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TOTAL.TextValue = "";
            this.IDH_TOTAL.WordWrap = true;
            // 
            // IDH_FOC
            // 
            this.IDH_FOC.Location = new System.Drawing.Point(4, 100);
            this.IDH_FOC.Name = "IDH_FOC";
            this.IDH_FOC.Size = new System.Drawing.Size(74, 14);
            this.IDH_FOC.TabIndex = 804;
            this.IDH_FOC.Text = "FOC";
            this.IDH_FOC.UseVisualStyleBackColor = true;
            this.IDH_FOC.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // label82
            // 
            this.label82.Location = new System.Drawing.Point(277, 10);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(70, 15);
            this.label82.TabIndex = 20;
            this.label82.Text = "Cust. Charge";
            // 
            // IDH_RSTAT
            // 
            this.IDH_RSTAT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_RSTAT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_RSTAT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_RSTAT.BlockPaint = false;
            this.IDH_RSTAT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_RSTAT.CornerRadius = 2;
            this.IDH_RSTAT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_RSTAT.Enabled = false;
            this.IDH_RSTAT.EnabledColor = System.Drawing.Color.White;
            this.IDH_RSTAT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_RSTAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_RSTAT.Location = new System.Drawing.Point(182, 55);
            this.IDH_RSTAT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_RSTAT.Multiline = true;
            this.IDH_RSTAT.Name = "IDH_RSTAT";
            this.IDH_RSTAT.PasswordChar = '\0';
            this.IDH_RSTAT.ReadOnly = false;
            this.IDH_RSTAT.Size = new System.Drawing.Size(51, 14);
            this.IDH_RSTAT.TabIndex = 152;
            this.IDH_RSTAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_RSTAT.TextValue = "";
            this.IDH_RSTAT.WordWrap = true;
            // 
            // label83
            // 
            this.label83.Location = new System.Drawing.Point(277, 25);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(70, 15);
            this.label83.TabIndex = 28;
            this.label83.Text = "Amount";
            // 
            // IDH_DISCNT
            // 
            this.IDH_DISCNT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DISCNT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DISCNT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DISCNT.BlockPaint = false;
            this.IDH_DISCNT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DISCNT.CornerRadius = 2;
            this.IDH_DISCNT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DISCNT.EnabledColor = System.Drawing.Color.White;
            this.IDH_DISCNT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DISCNT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DISCNT.Location = new System.Drawing.Point(215, 25);
            this.IDH_DISCNT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DISCNT.Multiline = true;
            this.IDH_DISCNT.Name = "IDH_DISCNT";
            this.IDH_DISCNT.PasswordChar = '\0';
            this.IDH_DISCNT.ReadOnly = false;
            this.IDH_DISCNT.Size = new System.Drawing.Size(31, 14);
            this.IDH_DISCNT.TabIndex = 148;
            this.IDH_DISCNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DISCNT.TextValue = "";
            this.IDH_DISCNT.WordWrap = true;
            // 
            // label84
            // 
            this.label84.Location = new System.Drawing.Point(277, 55);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(70, 15);
            this.label84.TabIndex = 30;
            this.label84.Text = "Add. Expenses";
            // 
            // IDH_CUSWEI
            // 
            this.IDH_CUSWEI.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSWEI.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSWEI.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSWEI.BlockPaint = false;
            this.IDH_CUSWEI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSWEI.CornerRadius = 2;
            this.IDH_CUSWEI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSWEI.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSWEI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSWEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSWEI.Location = new System.Drawing.Point(67, 10);
            this.IDH_CUSWEI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSWEI.Multiline = true;
            this.IDH_CUSWEI.Name = "IDH_CUSWEI";
            this.IDH_CUSWEI.PasswordChar = '\0';
            this.IDH_CUSWEI.ReadOnly = false;
            this.IDH_CUSWEI.Size = new System.Drawing.Size(41, 14);
            this.IDH_CUSWEI.TabIndex = 142;
            this.IDH_CUSWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSWEI.TextValue = "";
            this.IDH_CUSWEI.WordWrap = true;
            // 
            // label85
            // 
            this.label85.Location = new System.Drawing.Point(277, 40);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(70, 15);
            this.label85.TabIndex = 32;
            this.label85.Text = "Order Subtotal";
            // 
            // label86
            // 
            this.label86.Location = new System.Drawing.Point(277, 70);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(70, 15);
            this.label86.TabIndex = 35;
            this.label86.Text = "Tax";
            // 
            // IDH_AINV
            // 
            this.IDH_AINV.Enabled = false;
            this.IDH_AINV.Location = new System.Drawing.Point(4, 85);
            this.IDH_AINV.Name = "IDH_AINV";
            this.IDH_AINV.Size = new System.Drawing.Size(105, 14);
            this.IDH_AINV.TabIndex = 803;
            this.IDH_AINV.Text = "Already Invoiced";
            this.IDH_AINV.UseVisualStyleBackColor = true;
            this.IDH_AINV.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // label87
            // 
            this.label87.Location = new System.Drawing.Point(277, 100);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(70, 15);
            this.label87.TabIndex = 38;
            this.label87.Text = "Total Charge";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.IDH_ALTITM);
            this.groupBox1.Controls.Add(this.IDH_EXPLDW);
            this.groupBox1.Controls.Add(this.label126);
            this.groupBox1.Controls.Add(this.label129);
            this.groupBox1.Controls.Add(this.label125);
            this.groupBox1.Controls.Add(this.IDH_ISTRL);
            this.groupBox1.Controls.Add(this.IDH_DISPAD);
            this.groupBox1.Controls.Add(this.IDH_DISPCF);
            this.groupBox1.Controls.Add(this.IDH_DISPNM);
            this.groupBox1.Controls.Add(this.IDH_DISPCD);
            this.groupBox1.Controls.Add(this.label124);
            this.groupBox1.Controls.Add(this.label109);
            this.groupBox1.Controls.Add(this.IDH_ITMGRN);
            this.groupBox1.Controls.Add(this.IDH_ITMGRC);
            this.groupBox1.Controls.Add(this.Lbl1000007);
            this.groupBox1.Controls.Add(this.IDH_ITMCOD);
            this.groupBox1.Controls.Add(this.IDH_DESC);
            this.groupBox1.Controls.Add(this.IDH_WASCL1);
            this.groupBox1.Controls.Add(this.IDH_WASMAT);
            this.groupBox1.Controls.Add(this.IDH_RORIGI);
            this.groupBox1.Controls.Add(this.IDH_ITCF);
            this.groupBox1.Controls.Add(this.IDH_WASCF);
            this.groupBox1.Controls.Add(this.IDH_ORLK);
            this.groupBox1.Controls.Add(this.label76);
            this.groupBox1.Controls.Add(this.label77);
            this.groupBox1.Controls.Add(this.label78);
            this.groupBox1.Controls.Add(this.IDH_PRONM);
            this.groupBox1.Controls.Add(this.label79);
            this.groupBox1.Controls.Add(this.label80);
            this.groupBox1.Controls.Add(this.label81);
            this.groupBox1.Controls.Add(this.IDH_PROCD);
            this.groupBox1.Controls.Add(this.IDH_PROCF);
            this.groupBox1.Location = new System.Drawing.Point(0, -3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(826, 75);
            this.groupBox1.TabIndex = 810;
            this.groupBox1.TabStop = false;
            // 
            // IDH_ALTITM
            // 
            this.IDH_ALTITM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ALTITM.BlockPaint = false;
            this.IDH_ALTITM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ALTITM.CornerRadius = 2;
            this.IDH_ALTITM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ALTITM.DisplayDescription = true;
            this.IDH_ALTITM.DropDownHight = 100;
            this.IDH_ALTITM.DroppedDown = false;
            this.IDH_ALTITM.EnabledColor = System.Drawing.Color.White;
            this.IDH_ALTITM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ALTITM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ALTITM.HideDropDown = false;
            this.IDH_ALTITM.Location = new System.Drawing.Point(636, 25);
            this.IDH_ALTITM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ALTITM.Multiline = true;
            this.IDH_ALTITM.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_ALTITM.Name = "IDH_ALTITM";
            this.IDH_ALTITM.SelectedIndex = -1;
            this.IDH_ALTITM.SelectedItem = null;
            this.IDH_ALTITM.Size = new System.Drawing.Size(183, 14);
            this.IDH_ALTITM.TabIndex = 1011;
            this.IDH_ALTITM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ALTITM.TextBoxReadOnly = true;
            this.IDH_ALTITM.TextValue = "";
            // 
            // IDH_EXPLDW
            // 
            this.IDH_EXPLDW.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_EXPLDW.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_EXPLDW.BackColor = System.Drawing.Color.Transparent;
            this.IDH_EXPLDW.BlockPaint = false;
            this.IDH_EXPLDW.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_EXPLDW.CornerRadius = 2;
            this.IDH_EXPLDW.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_EXPLDW.Enabled = false;
            this.IDH_EXPLDW.EnabledColor = System.Drawing.Color.White;
            this.IDH_EXPLDW.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_EXPLDW.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_EXPLDW.Location = new System.Drawing.Point(100, 55);
            this.IDH_EXPLDW.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_EXPLDW.Multiline = true;
            this.IDH_EXPLDW.Name = "IDH_EXPLDW";
            this.IDH_EXPLDW.PasswordChar = '\0';
            this.IDH_EXPLDW.ReadOnly = false;
            this.IDH_EXPLDW.Size = new System.Drawing.Size(54, 14);
            this.IDH_EXPLDW.TabIndex = 1010;
            this.IDH_EXPLDW.TabStop = false;
            this.IDH_EXPLDW.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_EXPLDW.TextValue = "";
            this.IDH_EXPLDW.WordWrap = true;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(540, 55);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(58, 12);
            this.label126.TabIndex = 148;
            this.label126.Text = "Site Address";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(5, 55);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(97, 12);
            this.label129.TabIndex = 1009;
            this.label129.Text = "Expected Load Weight";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(283, 55);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(30, 12);
            this.label125.TabIndex = 147;
            this.label125.Text = "Name";
            // 
            // IDH_ISTRL
            // 
            this.IDH_ISTRL.AutoSize = true;
            this.IDH_ISTRL.Location = new System.Drawing.Point(193, 55);
            this.IDH_ISTRL.Name = "IDH_ISTRL";
            this.IDH_ISTRL.Size = new System.Drawing.Size(63, 16);
            this.IDH_ISTRL.TabIndex = 811;
            this.IDH_ISTRL.Text = "Trail Load";
            this.IDH_ISTRL.UseVisualStyleBackColor = true;
            // 
            // IDH_DISPAD
            // 
            this.IDH_DISPAD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DISPAD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DISPAD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DISPAD.BlockPaint = false;
            this.IDH_DISPAD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DISPAD.CornerRadius = 2;
            this.IDH_DISPAD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DISPAD.EnabledColor = System.Drawing.Color.White;
            this.IDH_DISPAD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DISPAD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DISPAD.Location = new System.Drawing.Point(636, 55);
            this.IDH_DISPAD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DISPAD.Multiline = true;
            this.IDH_DISPAD.Name = "IDH_DISPAD";
            this.IDH_DISPAD.PasswordChar = '\0';
            this.IDH_DISPAD.ReadOnly = false;
            this.IDH_DISPAD.Size = new System.Drawing.Size(183, 14);
            this.IDH_DISPAD.TabIndex = 146;
            this.IDH_DISPAD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DISPAD.TextValue = "";
            this.IDH_DISPAD.WordWrap = true;
            // 
            // IDH_DISPCF
            // 
            this.IDH_DISPCF.BlockPaint = false;
            this.IDH_DISPCF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DISPCF.Caption = "";
            this.IDH_DISPCF.CornerRadius = 2;
            this.IDH_DISPCF.DefaultButton = false;
            this.IDH_DISPCF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DISPCF.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_DISPCF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DISPCF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DISPCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_DISPCF.Image")));
            this.IDH_DISPCF.Location = new System.Drawing.Point(514, 40);
            this.IDH_DISPCF.Name = "IDH_DISPCF";
            this.IDH_DISPCF.Pressed = false;
            this.IDH_DISPCF.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_DISPCF.Size = new System.Drawing.Size(14, 14);
            this.IDH_DISPCF.TabIndex = 145;
            // 
            // IDH_DISPNM
            // 
            this.IDH_DISPNM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DISPNM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DISPNM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DISPNM.BlockPaint = false;
            this.IDH_DISPNM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DISPNM.CornerRadius = 2;
            this.IDH_DISPNM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DISPNM.EnabledColor = System.Drawing.Color.White;
            this.IDH_DISPNM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DISPNM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DISPNM.Location = new System.Drawing.Point(359, 55);
            this.IDH_DISPNM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DISPNM.Multiline = true;
            this.IDH_DISPNM.Name = "IDH_DISPNM";
            this.IDH_DISPNM.PasswordChar = '\0';
            this.IDH_DISPNM.ReadOnly = false;
            this.IDH_DISPNM.Size = new System.Drawing.Size(155, 14);
            this.IDH_DISPNM.TabIndex = 144;
            this.IDH_DISPNM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DISPNM.TextValue = "";
            this.IDH_DISPNM.WordWrap = true;
            // 
            // IDH_DISPCD
            // 
            this.IDH_DISPCD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DISPCD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DISPCD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DISPCD.BlockPaint = false;
            this.IDH_DISPCD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DISPCD.CornerRadius = 2;
            this.IDH_DISPCD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DISPCD.EnabledColor = System.Drawing.Color.White;
            this.IDH_DISPCD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DISPCD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DISPCD.Location = new System.Drawing.Point(359, 40);
            this.IDH_DISPCD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DISPCD.Multiline = true;
            this.IDH_DISPCD.Name = "IDH_DISPCD";
            this.IDH_DISPCD.PasswordChar = '\0';
            this.IDH_DISPCD.ReadOnly = false;
            this.IDH_DISPCD.Size = new System.Drawing.Size(155, 14);
            this.IDH_DISPCD.TabIndex = 143;
            this.IDH_DISPCD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DISPCD.TextValue = "";
            this.IDH_DISPCD.WordWrap = true;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(283, 40);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(59, 12);
            this.label124.TabIndex = 142;
            this.label124.Text = "Disposal Site";
            // 
            // label109
            // 
            this.label109.Location = new System.Drawing.Point(283, 25);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(71, 15);
            this.label109.TabIndex = 104;
            this.label109.Text = "Name";
            // 
            // IDH_ITMGRN
            // 
            this.IDH_ITMGRN.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ITMGRN.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ITMGRN.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ITMGRN.BlockPaint = false;
            this.IDH_ITMGRN.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ITMGRN.CornerRadius = 2;
            this.IDH_ITMGRN.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ITMGRN.Enabled = false;
            this.IDH_ITMGRN.EnabledColor = System.Drawing.Color.White;
            this.IDH_ITMGRN.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ITMGRN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ITMGRN.Location = new System.Drawing.Point(156, 10);
            this.IDH_ITMGRN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ITMGRN.Multiline = true;
            this.IDH_ITMGRN.Name = "IDH_ITMGRN";
            this.IDH_ITMGRN.PasswordChar = '\0';
            this.IDH_ITMGRN.ReadOnly = false;
            this.IDH_ITMGRN.Size = new System.Drawing.Size(100, 14);
            this.IDH_ITMGRN.TabIndex = 136;
            this.IDH_ITMGRN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ITMGRN.TextValue = "";
            this.IDH_ITMGRN.WordWrap = true;
            // 
            // IDH_ITMGRC
            // 
            this.IDH_ITMGRC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ITMGRC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ITMGRC.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ITMGRC.BlockPaint = false;
            this.IDH_ITMGRC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ITMGRC.CornerRadius = 2;
            this.IDH_ITMGRC.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ITMGRC.Enabled = false;
            this.IDH_ITMGRC.EnabledColor = System.Drawing.Color.White;
            this.IDH_ITMGRC.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ITMGRC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ITMGRC.Location = new System.Drawing.Point(100, 10);
            this.IDH_ITMGRC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ITMGRC.Multiline = true;
            this.IDH_ITMGRC.Name = "IDH_ITMGRC";
            this.IDH_ITMGRC.PasswordChar = '\0';
            this.IDH_ITMGRC.ReadOnly = false;
            this.IDH_ITMGRC.Size = new System.Drawing.Size(55, 14);
            this.IDH_ITMGRC.TabIndex = 135;
            this.IDH_ITMGRC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ITMGRC.TextValue = "";
            this.IDH_ITMGRC.WordWrap = true;
            // 
            // Lbl1000007
            // 
            this.Lbl1000007.Location = new System.Drawing.Point(283, 10);
            this.Lbl1000007.Name = "Lbl1000007";
            this.Lbl1000007.Size = new System.Drawing.Size(71, 15);
            this.Lbl1000007.TabIndex = 103;
            this.Lbl1000007.Text = "Buying From";
            // 
            // IDH_ITMCOD
            // 
            this.IDH_ITMCOD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ITMCOD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ITMCOD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ITMCOD.BlockPaint = false;
            this.IDH_ITMCOD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ITMCOD.CornerRadius = 2;
            this.IDH_ITMCOD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ITMCOD.EnabledColor = System.Drawing.Color.White;
            this.IDH_ITMCOD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ITMCOD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ITMCOD.Location = new System.Drawing.Point(100, 25);
            this.IDH_ITMCOD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ITMCOD.Multiline = true;
            this.IDH_ITMCOD.Name = "IDH_ITMCOD";
            this.IDH_ITMCOD.PasswordChar = '\0';
            this.IDH_ITMCOD.ReadOnly = false;
            this.IDH_ITMCOD.Size = new System.Drawing.Size(156, 14);
            this.IDH_ITMCOD.TabIndex = 5;
            this.IDH_ITMCOD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ITMCOD.TextValue = "";
            this.IDH_ITMCOD.WordWrap = true;
            // 
            // IDH_DESC
            // 
            this.IDH_DESC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DESC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DESC.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DESC.BlockPaint = false;
            this.IDH_DESC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DESC.CornerRadius = 2;
            this.IDH_DESC.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DESC.EnabledColor = System.Drawing.Color.White;
            this.IDH_DESC.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DESC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DESC.Location = new System.Drawing.Point(100, 40);
            this.IDH_DESC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DESC.Multiline = true;
            this.IDH_DESC.Name = "IDH_DESC";
            this.IDH_DESC.PasswordChar = '\0';
            this.IDH_DESC.ReadOnly = false;
            this.IDH_DESC.Size = new System.Drawing.Size(156, 14);
            this.IDH_DESC.TabIndex = 138;
            this.IDH_DESC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DESC.TextValue = "";
            this.IDH_DESC.WordWrap = true;
            // 
            // IDH_WASCL1
            // 
            this.IDH_WASCL1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WASCL1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WASCL1.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WASCL1.BlockPaint = false;
            this.IDH_WASCL1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WASCL1.CornerRadius = 2;
            this.IDH_WASCL1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WASCL1.EnabledColor = System.Drawing.Color.White;
            this.IDH_WASCL1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WASCL1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WASCL1.Location = new System.Drawing.Point(636, 10);
            this.IDH_WASCL1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WASCL1.Multiline = true;
            this.IDH_WASCL1.Name = "IDH_WASCL1";
            this.IDH_WASCL1.PasswordChar = '\0';
            this.IDH_WASCL1.ReadOnly = false;
            this.IDH_WASCL1.Size = new System.Drawing.Size(79, 14);
            this.IDH_WASCL1.TabIndex = 6;
            this.IDH_WASCL1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WASCL1.TextValue = "";
            this.IDH_WASCL1.WordWrap = true;
            // 
            // IDH_WASMAT
            // 
            this.IDH_WASMAT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WASMAT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WASMAT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WASMAT.BlockPaint = false;
            this.IDH_WASMAT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WASMAT.CornerRadius = 2;
            this.IDH_WASMAT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WASMAT.EnabledColor = System.Drawing.Color.White;
            this.IDH_WASMAT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WASMAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WASMAT.Location = new System.Drawing.Point(716, 10);
            this.IDH_WASMAT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WASMAT.Multiline = true;
            this.IDH_WASMAT.Name = "IDH_WASMAT";
            this.IDH_WASMAT.PasswordChar = '\0';
            this.IDH_WASMAT.ReadOnly = false;
            this.IDH_WASMAT.Size = new System.Drawing.Size(87, 14);
            this.IDH_WASMAT.TabIndex = 140;
            this.IDH_WASMAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WASMAT.TextValue = "";
            this.IDH_WASMAT.WordWrap = true;
            // 
            // IDH_RORIGI
            // 
            this.IDH_RORIGI.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_RORIGI.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_RORIGI.BackColor = System.Drawing.Color.Transparent;
            this.IDH_RORIGI.BlockPaint = false;
            this.IDH_RORIGI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_RORIGI.CornerRadius = 2;
            this.IDH_RORIGI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_RORIGI.EnabledColor = System.Drawing.Color.White;
            this.IDH_RORIGI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_RORIGI.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_RORIGI.Location = new System.Drawing.Point(636, 40);
            this.IDH_RORIGI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_RORIGI.Multiline = true;
            this.IDH_RORIGI.Name = "IDH_RORIGI";
            this.IDH_RORIGI.PasswordChar = '\0';
            this.IDH_RORIGI.ReadOnly = false;
            this.IDH_RORIGI.Size = new System.Drawing.Size(167, 14);
            this.IDH_RORIGI.TabIndex = 7;
            this.IDH_RORIGI.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_RORIGI.TextValue = "";
            this.IDH_RORIGI.WordWrap = true;
            // 
            // IDH_ITCF
            // 
            this.IDH_ITCF.BlockPaint = false;
            this.IDH_ITCF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ITCF.Caption = "";
            this.IDH_ITCF.CornerRadius = 2;
            this.IDH_ITCF.DefaultButton = false;
            this.IDH_ITCF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ITCF.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_ITCF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ITCF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ITCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ITCF.Image")));
            this.IDH_ITCF.Location = new System.Drawing.Point(256, 25);
            this.IDH_ITCF.Name = "IDH_ITCF";
            this.IDH_ITCF.Pressed = false;
            this.IDH_ITCF.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_ITCF.Size = new System.Drawing.Size(14, 14);
            this.IDH_ITCF.TabIndex = 137;
            this.IDH_ITCF.Click += new System.EventHandler(this.IDH_ITCFClick);
            // 
            // IDH_WASCF
            // 
            this.IDH_WASCF.BlockPaint = false;
            this.IDH_WASCF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WASCF.Caption = "";
            this.IDH_WASCF.CornerRadius = 2;
            this.IDH_WASCF.DefaultButton = false;
            this.IDH_WASCF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WASCF.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_WASCF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WASCF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WASCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_WASCF.Image")));
            this.IDH_WASCF.Location = new System.Drawing.Point(805, 10);
            this.IDH_WASCF.Name = "IDH_WASCF";
            this.IDH_WASCF.Pressed = false;
            this.IDH_WASCF.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_WASCF.Size = new System.Drawing.Size(14, 14);
            this.IDH_WASCF.TabIndex = 139;
            this.IDH_WASCF.Click += new System.EventHandler(this.IDH_WASCFClick);
            // 
            // IDH_ORLK
            // 
            this.IDH_ORLK.BlockPaint = false;
            this.IDH_ORLK.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORLK.Caption = "";
            this.IDH_ORLK.CornerRadius = 2;
            this.IDH_ORLK.DefaultButton = false;
            this.IDH_ORLK.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORLK.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_ORLK.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORLK.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ORLK.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ORLK.Image")));
            this.IDH_ORLK.Location = new System.Drawing.Point(805, 40);
            this.IDH_ORLK.Name = "IDH_ORLK";
            this.IDH_ORLK.Pressed = false;
            this.IDH_ORLK.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_ORLK.Size = new System.Drawing.Size(14, 14);
            this.IDH_ORLK.TabIndex = 141;
            this.IDH_ORLK.Click += new System.EventHandler(this.IDH_ORLKClick);
            // 
            // label76
            // 
            this.label76.Location = new System.Drawing.Point(5, 10);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(90, 15);
            this.label76.TabIndex = 69;
            this.label76.Text = "Container Group";
            // 
            // label77
            // 
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(5, 25);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(90, 15);
            this.label77.TabIndex = 70;
            this.label77.Text = "Container Code";
            // 
            // label78
            // 
            this.label78.Location = new System.Drawing.Point(5, 40);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(90, 15);
            this.label78.TabIndex = 71;
            this.label78.Text = "Description";
            // 
            // IDH_PRONM
            // 
            this.IDH_PRONM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PRONM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PRONM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRONM.BlockPaint = false;
            this.IDH_PRONM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRONM.CornerRadius = 2;
            this.IDH_PRONM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRONM.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRONM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRONM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PRONM.Location = new System.Drawing.Point(359, 25);
            this.IDH_PRONM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRONM.Multiline = true;
            this.IDH_PRONM.Name = "IDH_PRONM";
            this.IDH_PRONM.PasswordChar = '\0';
            this.IDH_PRONM.ReadOnly = false;
            this.IDH_PRONM.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRONM.TabIndex = 65;
            this.IDH_PRONM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRONM.TextValue = "";
            this.IDH_PRONM.WordWrap = true;
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(540, 10);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(76, 15);
            this.label79.TabIndex = 72;
            this.label79.Text = "Waste Material";
            // 
            // label80
            // 
            this.label80.Location = new System.Drawing.Point(540, 25);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(94, 15);
            this.label80.TabIndex = 73;
            this.label80.Text = "Alternative Desc.";
            // 
            // label81
            // 
            this.label81.Location = new System.Drawing.Point(540, 40);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(76, 15);
            this.label81.TabIndex = 74;
            this.label81.Text = "Origin Waste";
            // 
            // IDH_PROCD
            // 
            this.IDH_PROCD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PROCD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PROCD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PROCD.BlockPaint = false;
            this.IDH_PROCD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PROCD.CornerRadius = 2;
            this.IDH_PROCD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PROCD.EnabledColor = System.Drawing.Color.White;
            this.IDH_PROCD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PROCD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PROCD.Location = new System.Drawing.Point(359, 10);
            this.IDH_PROCD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PROCD.Multiline = true;
            this.IDH_PROCD.Name = "IDH_PROCD";
            this.IDH_PROCD.PasswordChar = '\0';
            this.IDH_PROCD.ReadOnly = false;
            this.IDH_PROCD.Size = new System.Drawing.Size(155, 14);
            this.IDH_PROCD.TabIndex = 14;
            this.IDH_PROCD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PROCD.TextValue = "";
            this.IDH_PROCD.WordWrap = true;
            // 
            // IDH_PROCF
            // 
            this.IDH_PROCF.BlockPaint = false;
            this.IDH_PROCF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PROCF.Caption = "";
            this.IDH_PROCF.CornerRadius = 2;
            this.IDH_PROCF.DefaultButton = false;
            this.IDH_PROCF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PROCF.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_PROCF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PROCF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PROCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_PROCF.Image")));
            this.IDH_PROCF.Location = new System.Drawing.Point(514, 10);
            this.IDH_PROCF.Name = "IDH_PROCF";
            this.IDH_PROCF.Pressed = false;
            this.IDH_PROCF.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_PROCF.Size = new System.Drawing.Size(14, 14);
            this.IDH_PROCF.TabIndex = 64;
            this.IDH_PROCF.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_ADJWGT
            // 
            this.IDH_ADJWGT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ADJWGT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ADJWGT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADJWGT.BlockPaint = false;
            this.IDH_ADJWGT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADJWGT.CornerRadius = 2;
            this.IDH_ADJWGT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADJWGT.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADJWGT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADJWGT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ADJWGT.Location = new System.Drawing.Point(101, 255);
            this.IDH_ADJWGT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ADJWGT.Multiline = true;
            this.IDH_ADJWGT.Name = "IDH_ADJWGT";
            this.IDH_ADJWGT.PasswordChar = '\0';
            this.IDH_ADJWGT.ReadOnly = false;
            this.IDH_ADJWGT.Size = new System.Drawing.Size(164, 14);
            this.IDH_ADJWGT.TabIndex = 809;
            this.IDH_ADJWGT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ADJWGT.TextValue = "";
            this.IDH_ADJWGT.WordWrap = true;
            // 
            // IDH_WGTDED
            // 
            this.IDH_WGTDED.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WGTDED.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WGTDED.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WGTDED.BlockPaint = false;
            this.IDH_WGTDED.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WGTDED.CornerRadius = 2;
            this.IDH_WGTDED.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WGTDED.EnabledColor = System.Drawing.Color.White;
            this.IDH_WGTDED.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WGTDED.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WGTDED.Location = new System.Drawing.Point(101, 240);
            this.IDH_WGTDED.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WGTDED.Multiline = true;
            this.IDH_WGTDED.Name = "IDH_WGTDED";
            this.IDH_WGTDED.PasswordChar = '\0';
            this.IDH_WGTDED.ReadOnly = false;
            this.IDH_WGTDED.Size = new System.Drawing.Size(164, 14);
            this.IDH_WGTDED.TabIndex = 808;
            this.IDH_WGTDED.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WGTDED.TextValue = "";
            this.IDH_WGTDED.WordWrap = true;
            // 
            // label114
            // 
            this.label114.Location = new System.Drawing.Point(4, 255);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(90, 15);
            this.label114.TabIndex = 807;
            this.label114.Text = "Adjusted Weight";
            // 
            // label113
            // 
            this.label113.Location = new System.Drawing.Point(4, 240);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(90, 15);
            this.label113.TabIndex = 806;
            this.label113.Text = "Weight Deduction (Kg)";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(286, 213);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(77, 69);
            this.pictureBox1.TabIndex = 115;
            this.pictureBox1.TabStop = false;
            // 
            // IDH_TAR2
            // 
            this.IDH_TAR2.BlockPaint = false;
            this.IDH_TAR2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TAR2.Caption = "Tare";
            this.IDH_TAR2.CornerRadius = 2;
            this.IDH_TAR2.DefaultButton = false;
            this.IDH_TAR2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TAR2.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_TAR2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TAR2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TAR2.Image = null;
            this.IDH_TAR2.Location = new System.Drawing.Point(342, 195);
            this.IDH_TAR2.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_TAR2.Name = "IDH_TAR2";
            this.IDH_TAR2.Pressed = false;
            this.IDH_TAR2.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_TAR2.Size = new System.Drawing.Size(37, 14);
            this.IDH_TAR2.TabIndex = 134;
            this.IDH_TAR2.Click += new System.EventHandler(this.IDH_Tare2Click);
            // 
            // IDH_BUF2
            // 
            this.IDH_BUF2.BlockPaint = false;
            this.IDH_BUF2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BUF2.Caption = "BU";
            this.IDH_BUF2.CornerRadius = 2;
            this.IDH_BUF2.DefaultButton = false;
            this.IDH_BUF2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BUF2.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_BUF2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BUF2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_BUF2.Image = null;
            this.IDH_BUF2.Location = new System.Drawing.Point(310, 195);
            this.IDH_BUF2.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_BUF2.Name = "IDH_BUF2";
            this.IDH_BUF2.Pressed = false;
            this.IDH_BUF2.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_BUF2.Size = new System.Drawing.Size(33, 14);
            this.IDH_BUF2.TabIndex = 133;
            // 
            // IDH_ACC2
            // 
            this.IDH_ACC2.BlockPaint = false;
            this.IDH_ACC2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ACC2.Caption = "Bridge";
            this.IDH_ACC2.CornerRadius = 2;
            this.IDH_ACC2.DefaultButton = false;
            this.IDH_ACC2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ACC2.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_ACC2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ACC2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ACC2.Image = null;
            this.IDH_ACC2.Location = new System.Drawing.Point(266, 195);
            this.IDH_ACC2.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_ACC2.Name = "IDH_ACC2";
            this.IDH_ACC2.Pressed = false;
            this.IDH_ACC2.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_ACC2.Size = new System.Drawing.Size(45, 14);
            this.IDH_ACC2.TabIndex = 500;
            this.IDH_ACC2.Click += new System.EventHandler(this.IDH_ACC2Click);
            // 
            // IDH_TAR1
            // 
            this.IDH_TAR1.BlockPaint = false;
            this.IDH_TAR1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TAR1.Caption = "Tare";
            this.IDH_TAR1.CornerRadius = 2;
            this.IDH_TAR1.DefaultButton = false;
            this.IDH_TAR1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TAR1.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_TAR1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TAR1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TAR1.Image = null;
            this.IDH_TAR1.Location = new System.Drawing.Point(342, 180);
            this.IDH_TAR1.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_TAR1.Name = "IDH_TAR1";
            this.IDH_TAR1.Pressed = false;
            this.IDH_TAR1.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_TAR1.Size = new System.Drawing.Size(37, 14);
            this.IDH_TAR1.TabIndex = 129;
            this.IDH_TAR1.Click += new System.EventHandler(this.IDH_Tare1Click);
            // 
            // IDH_BUF1
            // 
            this.IDH_BUF1.BlockPaint = false;
            this.IDH_BUF1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BUF1.Caption = "BU";
            this.IDH_BUF1.CornerRadius = 2;
            this.IDH_BUF1.DefaultButton = false;
            this.IDH_BUF1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BUF1.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_BUF1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BUF1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_BUF1.Image = null;
            this.IDH_BUF1.Location = new System.Drawing.Point(310, 180);
            this.IDH_BUF1.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_BUF1.Name = "IDH_BUF1";
            this.IDH_BUF1.Pressed = false;
            this.IDH_BUF1.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_BUF1.Size = new System.Drawing.Size(33, 14);
            this.IDH_BUF1.TabIndex = 128;
            // 
            // IDH_ACC1
            // 
            this.IDH_ACC1.BlockPaint = false;
            this.IDH_ACC1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ACC1.Caption = "Bridge";
            this.IDH_ACC1.CornerRadius = 2;
            this.IDH_ACC1.DefaultButton = false;
            this.IDH_ACC1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ACC1.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_ACC1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ACC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ACC1.Image = null;
            this.IDH_ACC1.Location = new System.Drawing.Point(266, 180);
            this.IDH_ACC1.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_ACC1.Name = "IDH_ACC1";
            this.IDH_ACC1.Pressed = false;
            this.IDH_ACC1.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_ACC1.Size = new System.Drawing.Size(45, 14);
            this.IDH_ACC1.TabIndex = 400;
            this.IDH_ACC1.Click += new System.EventHandler(this.IDH_ACC1Click);
            // 
            // IDH_ACCB
            // 
            this.IDH_ACCB.BlockPaint = false;
            this.IDH_ACCB.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ACCB.Caption = "Buffer";
            this.IDH_ACCB.CornerRadius = 2;
            this.IDH_ACCB.DefaultButton = false;
            this.IDH_ACCB.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ACCB.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_ACCB.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ACCB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ACCB.Image = null;
            this.IDH_ACCB.Location = new System.Drawing.Point(266, 135);
            this.IDH_ACCB.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_ACCB.Name = "IDH_ACCB";
            this.IDH_ACCB.Pressed = false;
            this.IDH_ACCB.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_ACCB.Size = new System.Drawing.Size(113, 14);
            this.IDH_ACCB.TabIndex = 121;
            this.IDH_ACCB.Click += new System.EventHandler(this.IDH_ACCBClick);
            // 
            // IDH_TMR
            // 
            this.IDH_TMR.BlockPaint = false;
            this.IDH_TMR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TMR.Caption = "Start Reading";
            this.IDH_TMR.CornerRadius = 2;
            this.IDH_TMR.DefaultButton = false;
            this.IDH_TMR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TMR.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_TMR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TMR.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TMR.Image = null;
            this.IDH_TMR.Location = new System.Drawing.Point(266, 115);
            this.IDH_TMR.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_TMR.Name = "IDH_TMR";
            this.IDH_TMR.Pressed = false;
            this.IDH_TMR.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_TMR.Size = new System.Drawing.Size(113, 19);
            this.IDH_TMR.TabIndex = 120;
            this.IDH_TMR.Click += new System.EventHandler(this.IDH_TMRClick);
            // 
            // IDH_REF
            // 
            this.IDH_REF.BlockPaint = false;
            this.IDH_REF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_REF.Caption = "Refresh";
            this.IDH_REF.CornerRadius = 2;
            this.IDH_REF.DefaultButton = false;
            this.IDH_REF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_REF.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_REF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_REF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_REF.Image = null;
            this.IDH_REF.Location = new System.Drawing.Point(266, 95);
            this.IDH_REF.Margin = new System.Windows.Forms.Padding(0);
            this.IDH_REF.Name = "IDH_REF";
            this.IDH_REF.Pressed = false;
            this.IDH_REF.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_REF.Size = new System.Drawing.Size(113, 19);
            this.IDH_REF.TabIndex = 119;
            this.IDH_REF.Click += new System.EventHandler(this.IDH_REFClick);
            // 
            // label102
            // 
            this.label102.Location = new System.Drawing.Point(4, 225);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(90, 15);
            this.label102.TabIndex = 101;
            this.label102.Text = "Units";
            // 
            // label101
            // 
            this.label101.Location = new System.Drawing.Point(4, 210);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(90, 15);
            this.label101.TabIndex = 100;
            this.label101.Text = "Net Weight (Kg)";
            // 
            // label100
            // 
            this.label100.Location = new System.Drawing.Point(4, 195);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(90, 15);
            this.label100.TabIndex = 99;
            this.label100.Text = "2nd Weigh (Kg)";
            // 
            // label99
            // 
            this.label99.Location = new System.Drawing.Point(4, 180);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(90, 15);
            this.label99.TabIndex = 98;
            this.label99.Text = "1st Weigh (Kg)";
            // 
            // label98
            // 
            this.label98.Location = new System.Drawing.Point(4, 165);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(90, 15);
            this.label98.TabIndex = 97;
            this.label98.Text = "2nd Tare Weight (kg)";
            // 
            // label97
            // 
            this.label97.Location = new System.Drawing.Point(4, 150);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(90, 15);
            this.label97.TabIndex = 96;
            this.label97.Text = "Vehicle Tare Weight";
            // 
            // label96
            // 
            this.label96.Location = new System.Drawing.Point(4, 135);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(90, 15);
            this.label96.TabIndex = 95;
            this.label96.Text = "Weight Buffer (Kg)";
            // 
            // label95
            // 
            this.label95.Location = new System.Drawing.Point(4, 95);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(90, 15);
            this.label95.TabIndex = 94;
            this.label95.Text = "Current Reading";
            // 
            // label94
            // 
            this.label94.Location = new System.Drawing.Point(4, 80);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(90, 15);
            this.label94.TabIndex = 93;
            this.label94.Text = "Weighbridge";
            // 
            // IDH_CUSCM
            // 
            this.IDH_CUSCM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSCM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSCM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSCM.BlockPaint = false;
            this.IDH_CUSCM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSCM.CornerRadius = 2;
            this.IDH_CUSCM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSCM.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSCM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSCM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSCM.Location = new System.Drawing.Point(101, 225);
            this.IDH_CUSCM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSCM.Multiline = true;
            this.IDH_CUSCM.Name = "IDH_CUSCM";
            this.IDH_CUSCM.PasswordChar = '\0';
            this.IDH_CUSCM.ReadOnly = false;
            this.IDH_CUSCM.Size = new System.Drawing.Size(109, 14);
            this.IDH_CUSCM.TabIndex = 9;
            this.IDH_CUSCM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSCM.TextValue = "";
            this.IDH_CUSCM.WordWrap = true;
            // 
            // IDH_RDWGT
            // 
            this.IDH_RDWGT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_RDWGT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_RDWGT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_RDWGT.BlockPaint = false;
            this.IDH_RDWGT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_RDWGT.CornerRadius = 2;
            this.IDH_RDWGT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_RDWGT.EnabledColor = System.Drawing.Color.White;
            this.IDH_RDWGT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_RDWGT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_RDWGT.Location = new System.Drawing.Point(101, 210);
            this.IDH_RDWGT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_RDWGT.Multiline = true;
            this.IDH_RDWGT.Name = "IDH_RDWGT";
            this.IDH_RDWGT.PasswordChar = '\0';
            this.IDH_RDWGT.ReadOnly = false;
            this.IDH_RDWGT.Size = new System.Drawing.Size(109, 14);
            this.IDH_RDWGT.TabIndex = 6;
            this.IDH_RDWGT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_RDWGT.TextValue = "";
            this.IDH_RDWGT.WordWrap = true;
            // 
            // IDH_WDT2
            // 
            this.IDH_WDT2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WDT2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WDT2.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WDT2.BlockPaint = false;
            this.IDH_WDT2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WDT2.CornerRadius = 2;
            this.IDH_WDT2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WDT2.Enabled = false;
            this.IDH_WDT2.EnabledColor = System.Drawing.Color.White;
            this.IDH_WDT2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WDT2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WDT2.Location = new System.Drawing.Point(211, 195);
            this.IDH_WDT2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WDT2.Multiline = true;
            this.IDH_WDT2.Name = "IDH_WDT2";
            this.IDH_WDT2.PasswordChar = '\0';
            this.IDH_WDT2.ReadOnly = false;
            this.IDH_WDT2.Size = new System.Drawing.Size(54, 14);
            this.IDH_WDT2.TabIndex = 131;
            this.IDH_WDT2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WDT2.TextValue = "";
            this.IDH_WDT2.WordWrap = true;
            // 
            // IDH_SER2
            // 
            this.IDH_SER2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SER2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SER2.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SER2.BlockPaint = false;
            this.IDH_SER2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SER2.CornerRadius = 2;
            this.IDH_SER2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SER2.Enabled = false;
            this.IDH_SER2.EnabledColor = System.Drawing.Color.White;
            this.IDH_SER2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SER2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SER2.Location = new System.Drawing.Point(156, 195);
            this.IDH_SER2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SER2.Multiline = true;
            this.IDH_SER2.Name = "IDH_SER2";
            this.IDH_SER2.PasswordChar = '\0';
            this.IDH_SER2.ReadOnly = false;
            this.IDH_SER2.Size = new System.Drawing.Size(54, 14);
            this.IDH_SER2.TabIndex = 130;
            this.IDH_SER2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SER2.TextValue = "";
            this.IDH_SER2.WordWrap = true;
            // 
            // IDH_WEI2
            // 
            this.IDH_WEI2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WEI2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WEI2.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WEI2.BlockPaint = false;
            this.IDH_WEI2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WEI2.CornerRadius = 2;
            this.IDH_WEI2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WEI2.EnabledColor = System.Drawing.Color.White;
            this.IDH_WEI2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WEI2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WEI2.Location = new System.Drawing.Point(101, 195);
            this.IDH_WEI2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WEI2.Multiline = true;
            this.IDH_WEI2.Name = "IDH_WEI2";
            this.IDH_WEI2.PasswordChar = '\0';
            this.IDH_WEI2.ReadOnly = false;
            this.IDH_WEI2.Size = new System.Drawing.Size(54, 14);
            this.IDH_WEI2.TabIndex = 5;
            this.IDH_WEI2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WEI2.TextValue = "";
            this.IDH_WEI2.WordWrap = true;
            // 
            // IDH_WDT1
            // 
            this.IDH_WDT1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WDT1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WDT1.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WDT1.BlockPaint = false;
            this.IDH_WDT1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WDT1.CornerRadius = 2;
            this.IDH_WDT1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WDT1.Enabled = false;
            this.IDH_WDT1.EnabledColor = System.Drawing.Color.White;
            this.IDH_WDT1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WDT1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WDT1.Location = new System.Drawing.Point(211, 180);
            this.IDH_WDT1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WDT1.Multiline = true;
            this.IDH_WDT1.Name = "IDH_WDT1";
            this.IDH_WDT1.PasswordChar = '\0';
            this.IDH_WDT1.ReadOnly = false;
            this.IDH_WDT1.Size = new System.Drawing.Size(54, 14);
            this.IDH_WDT1.TabIndex = 126;
            this.IDH_WDT1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WDT1.TextValue = "";
            this.IDH_WDT1.WordWrap = true;
            // 
            // IDH_SER1
            // 
            this.IDH_SER1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SER1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SER1.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SER1.BlockPaint = false;
            this.IDH_SER1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SER1.CornerRadius = 2;
            this.IDH_SER1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SER1.Enabled = false;
            this.IDH_SER1.EnabledColor = System.Drawing.Color.White;
            this.IDH_SER1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SER1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SER1.Location = new System.Drawing.Point(156, 180);
            this.IDH_SER1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SER1.Multiline = true;
            this.IDH_SER1.Name = "IDH_SER1";
            this.IDH_SER1.PasswordChar = '\0';
            this.IDH_SER1.ReadOnly = false;
            this.IDH_SER1.Size = new System.Drawing.Size(54, 14);
            this.IDH_SER1.TabIndex = 125;
            this.IDH_SER1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SER1.TextValue = "";
            this.IDH_SER1.WordWrap = true;
            // 
            // IDH_WEI1
            // 
            this.IDH_WEI1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WEI1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WEI1.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WEI1.BlockPaint = false;
            this.IDH_WEI1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WEI1.CornerRadius = 2;
            this.IDH_WEI1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WEI1.EnabledColor = System.Drawing.Color.White;
            this.IDH_WEI1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WEI1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WEI1.Location = new System.Drawing.Point(101, 180);
            this.IDH_WEI1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WEI1.Multiline = true;
            this.IDH_WEI1.Name = "IDH_WEI1";
            this.IDH_WEI1.PasswordChar = '\0';
            this.IDH_WEI1.ReadOnly = false;
            this.IDH_WEI1.Size = new System.Drawing.Size(54, 14);
            this.IDH_WEI1.TabIndex = 4;
            this.IDH_WEI1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WEI1.TextValue = "";
            this.IDH_WEI1.WordWrap = true;
            // 
            // IDH_TRLTar
            // 
            this.IDH_TRLTar.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TRLTar.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TRLTar.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TRLTar.BlockPaint = false;
            this.IDH_TRLTar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TRLTar.CornerRadius = 2;
            this.IDH_TRLTar.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TRLTar.EnabledColor = System.Drawing.Color.White;
            this.IDH_TRLTar.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TRLTar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TRLTar.Location = new System.Drawing.Point(101, 165);
            this.IDH_TRLTar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TRLTar.Multiline = true;
            this.IDH_TRLTar.Name = "IDH_TRLTar";
            this.IDH_TRLTar.PasswordChar = '\0';
            this.IDH_TRLTar.ReadOnly = false;
            this.IDH_TRLTar.Size = new System.Drawing.Size(109, 14);
            this.IDH_TRLTar.TabIndex = 124;
            this.IDH_TRLTar.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TRLTar.TextValue = "0.00";
            this.IDH_TRLTar.WordWrap = true;
            // 
            // IDH_TARWEI
            // 
            this.IDH_TARWEI.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TARWEI.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TARWEI.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TARWEI.BlockPaint = false;
            this.IDH_TARWEI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TARWEI.CornerRadius = 2;
            this.IDH_TARWEI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TARWEI.EnabledColor = System.Drawing.Color.White;
            this.IDH_TARWEI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TARWEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TARWEI.Location = new System.Drawing.Point(101, 150);
            this.IDH_TARWEI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TARWEI.Multiline = true;
            this.IDH_TARWEI.Name = "IDH_TARWEI";
            this.IDH_TARWEI.PasswordChar = '\0';
            this.IDH_TARWEI.ReadOnly = false;
            this.IDH_TARWEI.Size = new System.Drawing.Size(109, 14);
            this.IDH_TARWEI.TabIndex = 123;
            this.IDH_TARWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TARWEI.TextValue = "0.00";
            this.IDH_TARWEI.WordWrap = true;
            // 
            // IDH_WDTB
            // 
            this.IDH_WDTB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WDTB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WDTB.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WDTB.BlockPaint = false;
            this.IDH_WDTB.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WDTB.CornerRadius = 2;
            this.IDH_WDTB.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WDTB.Enabled = false;
            this.IDH_WDTB.EnabledColor = System.Drawing.Color.White;
            this.IDH_WDTB.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WDTB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WDTB.Location = new System.Drawing.Point(211, 135);
            this.IDH_WDTB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WDTB.Multiline = true;
            this.IDH_WDTB.Name = "IDH_WDTB";
            this.IDH_WDTB.PasswordChar = '\0';
            this.IDH_WDTB.ReadOnly = false;
            this.IDH_WDTB.Size = new System.Drawing.Size(54, 14);
            this.IDH_WDTB.TabIndex = 4;
            this.IDH_WDTB.TabStop = false;
            this.IDH_WDTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_WDTB.TextValue = "";
            this.IDH_WDTB.WordWrap = true;
            // 
            // IDH_SERB
            // 
            this.IDH_SERB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SERB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SERB.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SERB.BlockPaint = false;
            this.IDH_SERB.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SERB.CornerRadius = 2;
            this.IDH_SERB.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SERB.Enabled = false;
            this.IDH_SERB.EnabledColor = System.Drawing.Color.White;
            this.IDH_SERB.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SERB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SERB.Location = new System.Drawing.Point(156, 135);
            this.IDH_SERB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SERB.Multiline = true;
            this.IDH_SERB.Name = "IDH_SERB";
            this.IDH_SERB.PasswordChar = '\0';
            this.IDH_SERB.ReadOnly = false;
            this.IDH_SERB.Size = new System.Drawing.Size(54, 14);
            this.IDH_SERB.TabIndex = 3;
            this.IDH_SERB.TabStop = false;
            this.IDH_SERB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_SERB.TextValue = "";
            this.IDH_SERB.WordWrap = true;
            // 
            // IDH_WEIB
            // 
            this.IDH_WEIB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WEIB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WEIB.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WEIB.BlockPaint = false;
            this.IDH_WEIB.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WEIB.CornerRadius = 2;
            this.IDH_WEIB.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WEIB.Enabled = false;
            this.IDH_WEIB.EnabledColor = System.Drawing.Color.White;
            this.IDH_WEIB.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WEIB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WEIB.Location = new System.Drawing.Point(101, 135);
            this.IDH_WEIB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WEIB.Multiline = true;
            this.IDH_WEIB.Name = "IDH_WEIB";
            this.IDH_WEIB.PasswordChar = '\0';
            this.IDH_WEIB.ReadOnly = false;
            this.IDH_WEIB.Size = new System.Drawing.Size(54, 14);
            this.IDH_WEIB.TabIndex = 2;
            this.IDH_WEIB.TabStop = false;
            this.IDH_WEIB.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WEIB.TextValue = "";
            this.IDH_WEIB.WordWrap = true;
            // 
            // IDH_WEIG
            // 
            this.IDH_WEIG.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WEIG.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WEIG.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WEIG.BlockPaint = false;
            this.IDH_WEIG.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WEIG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_WEIG.CornerRadius = 2;
            this.IDH_WEIG.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WEIG.Enabled = false;
            this.IDH_WEIG.EnabledColor = System.Drawing.Color.White;
            this.IDH_WEIG.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WEIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WEIG.Location = new System.Drawing.Point(101, 95);
            this.IDH_WEIG.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_WEIG.Multiline = true;
            this.IDH_WEIG.Name = "IDH_WEIG";
            this.IDH_WEIG.PasswordChar = '\0';
            this.IDH_WEIG.ReadOnly = false;
            this.IDH_WEIG.Size = new System.Drawing.Size(164, 39);
            this.IDH_WEIG.TabIndex = 1;
            this.IDH_WEIG.TabStop = false;
            this.IDH_WEIG.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.IDH_WEIG.TextValue = "Waiting";
            this.IDH_WEIG.WordWrap = true;
            // 
            // IDH_WEIBRG
            // 
            this.IDH_WEIBRG.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WEIBRG.BlockPaint = false;
            this.IDH_WEIBRG.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WEIBRG.CornerRadius = 2;
            this.IDH_WEIBRG.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WEIBRG.DisplayDescription = true;
            this.IDH_WEIBRG.DropDownHight = 100;
            this.IDH_WEIBRG.DroppedDown = false;
            this.IDH_WEIBRG.EnabledColor = System.Drawing.Color.White;
            this.IDH_WEIBRG.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WEIBRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WEIBRG.HideDropDown = false;
            this.IDH_WEIBRG.Location = new System.Drawing.Point(101, 80);
            this.IDH_WEIBRG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WEIBRG.Multiline = true;
            this.IDH_WEIBRG.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_WEIBRG.Name = "IDH_WEIBRG";
            this.IDH_WEIBRG.SelectedIndex = -1;
            this.IDH_WEIBRG.SelectedItem = null;
            this.IDH_WEIBRG.Size = new System.Drawing.Size(164, 14);
            this.IDH_WEIBRG.TabIndex = 118;
            this.IDH_WEIBRG.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WEIBRG.TextBoxReadOnly = true;
            this.IDH_WEIBRG.TextValue = "";
            // 
            // oTPCarrier
            // 
            this.oTPCarrier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPCarrier.Controls.Add(this.IDH_LICREG);
            this.oTPCarrier.Controls.Add(this.label54);
            this.oTPCarrier.Controls.Add(this.IDH_PHONE);
            this.oTPCarrier.Controls.Add(this.label53);
            this.oTPCarrier.Controls.Add(this.IDH_ZIP);
            this.oTPCarrier.Controls.Add(this.label52);
            this.oTPCarrier.Controls.Add(this.IDH_CITY);
            this.oTPCarrier.Controls.Add(this.label51);
            this.oTPCarrier.Controls.Add(this.IDH_BLOCK);
            this.oTPCarrier.Controls.Add(this.label50);
            this.oTPCarrier.Controls.Add(this.IDH_WASAL);
            this.oTPCarrier.Controls.Add(this.IDH_ADDRES);
            this.oTPCarrier.Controls.Add(this.label49);
            this.oTPCarrier.Controls.Add(this.IDH_STREET);
            this.oTPCarrier.Controls.Add(this.label48);
            this.oTPCarrier.Controls.Add(this.IDH_CARREF);
            this.oTPCarrier.Controls.Add(this.label47);
            this.oTPCarrier.Controls.Add(this.IDH_CONTNM);
            this.oTPCarrier.Controls.Add(this.label46);
            this.oTPCarrier.Location = new System.Drawing.Point(4, 21);
            this.oTPCarrier.Name = "oTPCarrier";
            this.oTPCarrier.Padding = new System.Windows.Forms.Padding(3);
            this.oTPCarrier.Size = new System.Drawing.Size(822, 307);
            this.oTPCarrier.TabIndex = 1;
            this.oTPCarrier.Text = "Carrier";
            // 
            // IDH_LICREG
            // 
            this.IDH_LICREG.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_LICREG.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_LICREG.BackColor = System.Drawing.Color.Transparent;
            this.IDH_LICREG.BlockPaint = false;
            this.IDH_LICREG.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_LICREG.CornerRadius = 2;
            this.IDH_LICREG.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_LICREG.EnabledColor = System.Drawing.Color.White;
            this.IDH_LICREG.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_LICREG.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_LICREG.Location = new System.Drawing.Point(130, 130);
            this.IDH_LICREG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_LICREG.Multiline = true;
            this.IDH_LICREG.Name = "IDH_LICREG";
            this.IDH_LICREG.PasswordChar = '\0';
            this.IDH_LICREG.ReadOnly = false;
            this.IDH_LICREG.Size = new System.Drawing.Size(155, 14);
            this.IDH_LICREG.TabIndex = 8;
            this.IDH_LICREG.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_LICREG.TextValue = "";
            this.IDH_LICREG.WordWrap = true;
            // 
            // label54
            // 
            this.label54.Location = new System.Drawing.Point(4, 135);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(100, 17);
            this.label54.TabIndex = 107;
            this.label54.Text = "Waste Lic. Reg. No.";
            // 
            // IDH_PHONE
            // 
            this.IDH_PHONE.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PHONE.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PHONE.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PHONE.BlockPaint = false;
            this.IDH_PHONE.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PHONE.CornerRadius = 2;
            this.IDH_PHONE.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PHONE.EnabledColor = System.Drawing.Color.White;
            this.IDH_PHONE.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PHONE.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PHONE.Location = new System.Drawing.Point(130, 115);
            this.IDH_PHONE.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PHONE.Multiline = true;
            this.IDH_PHONE.Name = "IDH_PHONE";
            this.IDH_PHONE.PasswordChar = '\0';
            this.IDH_PHONE.ReadOnly = false;
            this.IDH_PHONE.Size = new System.Drawing.Size(155, 14);
            this.IDH_PHONE.TabIndex = 7;
            this.IDH_PHONE.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PHONE.TextValue = "";
            this.IDH_PHONE.WordWrap = true;
            // 
            // label53
            // 
            this.label53.Location = new System.Drawing.Point(4, 115);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(100, 17);
            this.label53.TabIndex = 105;
            this.label53.Text = "Phone No.";
            // 
            // IDH_ZIP
            // 
            this.IDH_ZIP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ZIP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ZIP.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ZIP.BlockPaint = false;
            this.IDH_ZIP.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ZIP.CornerRadius = 2;
            this.IDH_ZIP.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ZIP.EnabledColor = System.Drawing.Color.White;
            this.IDH_ZIP.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ZIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ZIP.Location = new System.Drawing.Point(130, 100);
            this.IDH_ZIP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ZIP.Multiline = true;
            this.IDH_ZIP.Name = "IDH_ZIP";
            this.IDH_ZIP.PasswordChar = '\0';
            this.IDH_ZIP.ReadOnly = false;
            this.IDH_ZIP.Size = new System.Drawing.Size(155, 14);
            this.IDH_ZIP.TabIndex = 6;
            this.IDH_ZIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ZIP.TextValue = "";
            this.IDH_ZIP.WordWrap = true;
            // 
            // label52
            // 
            this.label52.Location = new System.Drawing.Point(4, 100);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(100, 17);
            this.label52.TabIndex = 103;
            this.label52.Text = "Post Code";
            // 
            // IDH_CITY
            // 
            this.IDH_CITY.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CITY.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CITY.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CITY.BlockPaint = false;
            this.IDH_CITY.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CITY.CornerRadius = 2;
            this.IDH_CITY.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CITY.EnabledColor = System.Drawing.Color.White;
            this.IDH_CITY.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CITY.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CITY.Location = new System.Drawing.Point(130, 85);
            this.IDH_CITY.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CITY.Multiline = true;
            this.IDH_CITY.Name = "IDH_CITY";
            this.IDH_CITY.PasswordChar = '\0';
            this.IDH_CITY.ReadOnly = false;
            this.IDH_CITY.Size = new System.Drawing.Size(155, 14);
            this.IDH_CITY.TabIndex = 5;
            this.IDH_CITY.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CITY.TextValue = "";
            this.IDH_CITY.WordWrap = true;
            // 
            // label51
            // 
            this.label51.Location = new System.Drawing.Point(4, 85);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(100, 17);
            this.label51.TabIndex = 101;
            this.label51.Text = "City";
            // 
            // IDH_BLOCK
            // 
            this.IDH_BLOCK.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_BLOCK.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_BLOCK.BackColor = System.Drawing.Color.Transparent;
            this.IDH_BLOCK.BlockPaint = false;
            this.IDH_BLOCK.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BLOCK.CornerRadius = 2;
            this.IDH_BLOCK.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BLOCK.EnabledColor = System.Drawing.Color.White;
            this.IDH_BLOCK.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BLOCK.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_BLOCK.Location = new System.Drawing.Point(130, 70);
            this.IDH_BLOCK.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_BLOCK.Multiline = true;
            this.IDH_BLOCK.Name = "IDH_BLOCK";
            this.IDH_BLOCK.PasswordChar = '\0';
            this.IDH_BLOCK.ReadOnly = false;
            this.IDH_BLOCK.Size = new System.Drawing.Size(155, 14);
            this.IDH_BLOCK.TabIndex = 4;
            this.IDH_BLOCK.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BLOCK.TextValue = "";
            this.IDH_BLOCK.WordWrap = true;
            // 
            // label50
            // 
            this.label50.Location = new System.Drawing.Point(4, 70);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(100, 17);
            this.label50.TabIndex = 99;
            this.label50.Text = "Block";
            // 
            // IDH_WASAL
            // 
            this.IDH_WASAL.BlockPaint = false;
            this.IDH_WASAL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WASAL.Caption = "";
            this.IDH_WASAL.CornerRadius = 2;
            this.IDH_WASAL.DefaultButton = false;
            this.IDH_WASAL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WASAL.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_WASAL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WASAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WASAL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_WASAL.Image")));
            this.IDH_WASAL.Location = new System.Drawing.Point(285, 40);
            this.IDH_WASAL.Name = "IDH_WASAL";
            this.IDH_WASAL.Pressed = false;
            this.IDH_WASAL.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_WASAL.Size = new System.Drawing.Size(14, 14);
            this.IDH_WASAL.TabIndex = 98;
            this.IDH_WASAL.Click += new System.EventHandler(this.AddressSearchClick);
            // 
            // IDH_ADDRES
            // 
            this.IDH_ADDRES.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ADDRES.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ADDRES.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADDRES.BlockPaint = false;
            this.IDH_ADDRES.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADDRES.CornerRadius = 2;
            this.IDH_ADDRES.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADDRES.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADDRES.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADDRES.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ADDRES.Location = new System.Drawing.Point(130, 40);
            this.IDH_ADDRES.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ADDRES.Multiline = true;
            this.IDH_ADDRES.Name = "IDH_ADDRES";
            this.IDH_ADDRES.PasswordChar = '\0';
            this.IDH_ADDRES.ReadOnly = false;
            this.IDH_ADDRES.Size = new System.Drawing.Size(155, 14);
            this.IDH_ADDRES.TabIndex = 2;
            this.IDH_ADDRES.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ADDRES.TextValue = "";
            this.IDH_ADDRES.WordWrap = true;
            // 
            // label49
            // 
            this.label49.Location = new System.Drawing.Point(4, 40);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(100, 14);
            this.label49.TabIndex = 96;
            this.label49.Text = "Address";
            // 
            // IDH_STREET
            // 
            this.IDH_STREET.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_STREET.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_STREET.BackColor = System.Drawing.Color.Transparent;
            this.IDH_STREET.BlockPaint = false;
            this.IDH_STREET.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_STREET.CornerRadius = 2;
            this.IDH_STREET.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_STREET.EnabledColor = System.Drawing.Color.White;
            this.IDH_STREET.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_STREET.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_STREET.Location = new System.Drawing.Point(130, 55);
            this.IDH_STREET.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_STREET.Multiline = true;
            this.IDH_STREET.Name = "IDH_STREET";
            this.IDH_STREET.PasswordChar = '\0';
            this.IDH_STREET.ReadOnly = false;
            this.IDH_STREET.Size = new System.Drawing.Size(155, 14);
            this.IDH_STREET.TabIndex = 3;
            this.IDH_STREET.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_STREET.TextValue = "";
            this.IDH_STREET.WordWrap = true;
            // 
            // label48
            // 
            this.label48.Location = new System.Drawing.Point(4, 55);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(100, 17);
            this.label48.TabIndex = 86;
            this.label48.Text = "Street";
            // 
            // IDH_CARREF
            // 
            this.IDH_CARREF.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CARREF.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CARREF.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CARREF.BlockPaint = false;
            this.IDH_CARREF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CARREF.CornerRadius = 2;
            this.IDH_CARREF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CARREF.EnabledColor = System.Drawing.Color.White;
            this.IDH_CARREF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CARREF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CARREF.Location = new System.Drawing.Point(130, 25);
            this.IDH_CARREF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CARREF.Multiline = true;
            this.IDH_CARREF.Name = "IDH_CARREF";
            this.IDH_CARREF.PasswordChar = '\0';
            this.IDH_CARREF.ReadOnly = false;
            this.IDH_CARREF.Size = new System.Drawing.Size(155, 14);
            this.IDH_CARREF.TabIndex = 1;
            this.IDH_CARREF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CARREF.TextValue = "";
            this.IDH_CARREF.WordWrap = true;
            // 
            // label47
            // 
            this.label47.Location = new System.Drawing.Point(4, 25);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(123, 18);
            this.label47.TabIndex = 2;
            this.label47.Text = "Waste Carrier Ref. No.";
            // 
            // IDH_CONTNM
            // 
            this.IDH_CONTNM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CONTNM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CONTNM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CONTNM.BlockPaint = false;
            this.IDH_CONTNM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CONTNM.CornerRadius = 2;
            this.IDH_CONTNM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CONTNM.EnabledColor = System.Drawing.Color.White;
            this.IDH_CONTNM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CONTNM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CONTNM.Location = new System.Drawing.Point(130, 10);
            this.IDH_CONTNM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CONTNM.Multiline = true;
            this.IDH_CONTNM.Name = "IDH_CONTNM";
            this.IDH_CONTNM.PasswordChar = '\0';
            this.IDH_CONTNM.ReadOnly = false;
            this.IDH_CONTNM.Size = new System.Drawing.Size(155, 14);
            this.IDH_CONTNM.TabIndex = 0;
            this.IDH_CONTNM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CONTNM.TextValue = "";
            this.IDH_CONTNM.WordWrap = true;
            // 
            // label46
            // 
            this.label46.Location = new System.Drawing.Point(4, 10);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(100, 18);
            this.label46.TabIndex = 0;
            this.label46.Text = "Contact Person";
            // 
            // oTPCustomer
            // 
            this.oTPCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPCustomer.Controls.Add(this.label136);
            this.oTPCustomer.Controls.Add(this.IDH_FTORIG);
            this.oTPCustomer.Controls.Add(this.IDH_ORIGLU2);
            this.oTPCustomer.Controls.Add(this.IDH_ORIGIN2);
            this.oTPCustomer.Controls.Add(this.label110);
            this.oTPCustomer.Controls.Add(this.label43);
            this.oTPCustomer.Controls.Add(this.label44);
            this.oTPCustomer.Controls.Add(this.label45);
            this.oTPCustomer.Controls.Add(this.IDH_SLCNO);
            this.oTPCustomer.Controls.Add(this.IDH_PCD);
            this.oTPCustomer.Controls.Add(this.IDH_SteId);
            this.oTPCustomer.Controls.Add(this.IDH_ROUTL);
            this.oTPCustomer.Controls.Add(this.label42);
            this.oTPCustomer.Controls.Add(this.IDH_SEQ);
            this.oTPCustomer.Controls.Add(this.IDH_ROUTE);
            this.oTPCustomer.Controls.Add(this.label41);
            this.oTPCustomer.Controls.Add(this.IDH_CNA);
            this.oTPCustomer.Controls.Add(this.IDH_CUSAL);
            this.oTPCustomer.Controls.Add(this.label40);
            this.oTPCustomer.Controls.Add(this.IDH_CUSLIC);
            this.oTPCustomer.Controls.Add(this.IDH_SITETL);
            this.oTPCustomer.Controls.Add(this.label39);
            this.oTPCustomer.Controls.Add(this.IDH_CUSPHO);
            this.oTPCustomer.Controls.Add(this.IDH_CUSPOS);
            this.oTPCustomer.Controls.Add(this.IDH_COUNTY);
            this.oTPCustomer.Controls.Add(this.IDH_CUSCIT);
            this.oTPCustomer.Controls.Add(this.IDH_CUSBLO);
            this.oTPCustomer.Controls.Add(this.IDH_CUSSTR);
            this.oTPCustomer.Controls.Add(this.IDH_CUSADD);
            this.oTPCustomer.Controls.Add(this.IDH_CUSCON);
            this.oTPCustomer.Controls.Add(this.label38);
            this.oTPCustomer.Controls.Add(this.label37);
            this.oTPCustomer.Controls.Add(this.label36);
            this.oTPCustomer.Controls.Add(this.label35);
            this.oTPCustomer.Controls.Add(this.label34);
            this.oTPCustomer.Controls.Add(this.label33);
            this.oTPCustomer.Controls.Add(this.label32);
            this.oTPCustomer.Controls.Add(this.label31);
            this.oTPCustomer.Location = new System.Drawing.Point(4, 21);
            this.oTPCustomer.Name = "oTPCustomer";
            this.oTPCustomer.Size = new System.Drawing.Size(822, 307);
            this.oTPCustomer.TabIndex = 2;
            this.oTPCustomer.Text = "Customer";
            // 
            // label136
            // 
            this.label136.Location = new System.Drawing.Point(4, 225);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(100, 15);
            this.label136.TabIndex = 119;
            this.label136.Text = "Free Text Origin";
            // 
            // IDH_FTORIG
            // 
            this.IDH_FTORIG.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_FTORIG.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_FTORIG.BackColor = System.Drawing.Color.Transparent;
            this.IDH_FTORIG.BlockPaint = false;
            this.IDH_FTORIG.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_FTORIG.CornerRadius = 2;
            this.IDH_FTORIG.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_FTORIG.Enabled = false;
            this.IDH_FTORIG.EnabledColor = System.Drawing.Color.White;
            this.IDH_FTORIG.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_FTORIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_FTORIG.Location = new System.Drawing.Point(130, 225);
            this.IDH_FTORIG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_FTORIG.Multiline = true;
            this.IDH_FTORIG.Name = "IDH_FTORIG";
            this.IDH_FTORIG.PasswordChar = '\0';
            this.IDH_FTORIG.ReadOnly = false;
            this.IDH_FTORIG.Size = new System.Drawing.Size(531, 14);
            this.IDH_FTORIG.TabIndex = 118;
            this.IDH_FTORIG.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_FTORIG.TextValue = "";
            this.IDH_FTORIG.WordWrap = true;
            // 
            // IDH_ORIGLU2
            // 
            this.IDH_ORIGLU2.BlockPaint = false;
            this.IDH_ORIGLU2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORIGLU2.Caption = "";
            this.IDH_ORIGLU2.CornerRadius = 2;
            this.IDH_ORIGLU2.DefaultButton = false;
            this.IDH_ORIGLU2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORIGLU2.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_ORIGLU2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORIGLU2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ORIGLU2.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ORIGLU2.Image")));
            this.IDH_ORIGLU2.Location = new System.Drawing.Point(285, 190);
            this.IDH_ORIGLU2.Name = "IDH_ORIGLU2";
            this.IDH_ORIGLU2.Pressed = false;
            this.IDH_ORIGLU2.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_ORIGLU2.Size = new System.Drawing.Size(14, 14);
            this.IDH_ORIGLU2.TabIndex = 117;
            this.IDH_ORIGLU2.Click += new System.EventHandler(this.IDH_ORLKClick);
            // 
            // IDH_ORIGIN2
            // 
            this.IDH_ORIGIN2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ORIGIN2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ORIGIN2.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ORIGIN2.BlockPaint = false;
            this.IDH_ORIGIN2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORIGIN2.CornerRadius = 2;
            this.IDH_ORIGIN2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORIGIN2.EnabledColor = System.Drawing.Color.White;
            this.IDH_ORIGIN2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORIGIN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ORIGIN2.Location = new System.Drawing.Point(130, 190);
            this.IDH_ORIGIN2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ORIGIN2.Multiline = true;
            this.IDH_ORIGIN2.Name = "IDH_ORIGIN2";
            this.IDH_ORIGIN2.PasswordChar = '\0';
            this.IDH_ORIGIN2.ReadOnly = false;
            this.IDH_ORIGIN2.Size = new System.Drawing.Size(155, 14);
            this.IDH_ORIGIN2.TabIndex = 115;
            this.IDH_ORIGIN2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ORIGIN2.TextValue = "";
            this.IDH_ORIGIN2.WordWrap = true;
            // 
            // label110
            // 
            this.label110.Location = new System.Drawing.Point(4, 190);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(100, 15);
            this.label110.TabIndex = 116;
            this.label110.Text = "Origin";
            // 
            // label43
            // 
            this.label43.Location = new System.Drawing.Point(309, 190);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(100, 14);
            this.label43.TabIndex = 107;
            this.label43.Text = "Site Licence";
            // 
            // label44
            // 
            this.label44.Location = new System.Drawing.Point(309, 175);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(95, 14);
            this.label44.TabIndex = 106;
            this.label44.Text = "Premesis Code";
            // 
            // label45
            // 
            this.label45.Location = new System.Drawing.Point(309, 160);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(108, 14);
            this.label45.TabIndex = 105;
            this.label45.Text = "Site Id";
            // 
            // IDH_SLCNO
            // 
            this.IDH_SLCNO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SLCNO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SLCNO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SLCNO.BlockPaint = false;
            this.IDH_SLCNO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SLCNO.CornerRadius = 2;
            this.IDH_SLCNO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SLCNO.Enabled = false;
            this.IDH_SLCNO.EnabledColor = System.Drawing.Color.White;
            this.IDH_SLCNO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SLCNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SLCNO.Location = new System.Drawing.Point(417, 190);
            this.IDH_SLCNO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SLCNO.Multiline = true;
            this.IDH_SLCNO.Name = "IDH_SLCNO";
            this.IDH_SLCNO.PasswordChar = '\0';
            this.IDH_SLCNO.ReadOnly = false;
            this.IDH_SLCNO.Size = new System.Drawing.Size(244, 14);
            this.IDH_SLCNO.TabIndex = 14;
            this.IDH_SLCNO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SLCNO.TextValue = "";
            this.IDH_SLCNO.WordWrap = true;
            // 
            // IDH_PCD
            // 
            this.IDH_PCD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PCD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PCD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PCD.BlockPaint = false;
            this.IDH_PCD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PCD.CornerRadius = 2;
            this.IDH_PCD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PCD.Enabled = false;
            this.IDH_PCD.EnabledColor = System.Drawing.Color.White;
            this.IDH_PCD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PCD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PCD.Location = new System.Drawing.Point(417, 175);
            this.IDH_PCD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PCD.Multiline = true;
            this.IDH_PCD.Name = "IDH_PCD";
            this.IDH_PCD.PasswordChar = '\0';
            this.IDH_PCD.ReadOnly = false;
            this.IDH_PCD.Size = new System.Drawing.Size(244, 14);
            this.IDH_PCD.TabIndex = 12;
            this.IDH_PCD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PCD.TextValue = "";
            this.IDH_PCD.WordWrap = true;
            // 
            // IDH_SteId
            // 
            this.IDH_SteId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SteId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SteId.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SteId.BlockPaint = false;
            this.IDH_SteId.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SteId.CornerRadius = 2;
            this.IDH_SteId.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SteId.Enabled = false;
            this.IDH_SteId.EnabledColor = System.Drawing.Color.White;
            this.IDH_SteId.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SteId.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SteId.Location = new System.Drawing.Point(417, 160);
            this.IDH_SteId.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SteId.Multiline = true;
            this.IDH_SteId.Name = "IDH_SteId";
            this.IDH_SteId.PasswordChar = '\0';
            this.IDH_SteId.ReadOnly = false;
            this.IDH_SteId.Size = new System.Drawing.Size(244, 14);
            this.IDH_SteId.TabIndex = 10;
            this.IDH_SteId.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SteId.TextValue = "";
            this.IDH_SteId.WordWrap = true;
            // 
            // IDH_ROUTL
            // 
            this.IDH_ROUTL.BlockPaint = false;
            this.IDH_ROUTL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ROUTL.Caption = "";
            this.IDH_ROUTL.CornerRadius = 2;
            this.IDH_ROUTL.DefaultButton = false;
            this.IDH_ROUTL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ROUTL.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_ROUTL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ROUTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ROUTL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ROUTL.Image")));
            this.IDH_ROUTL.Location = new System.Drawing.Point(285, 160);
            this.IDH_ROUTL.Name = "IDH_ROUTL";
            this.IDH_ROUTL.Pressed = false;
            this.IDH_ROUTL.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_ROUTL.Size = new System.Drawing.Size(14, 14);
            this.IDH_ROUTL.TabIndex = 101;
            // 
            // label42
            // 
            this.label42.Location = new System.Drawing.Point(4, 175);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(100, 15);
            this.label42.TabIndex = 100;
            this.label42.Text = "Sequence";
            // 
            // IDH_SEQ
            // 
            this.IDH_SEQ.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SEQ.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SEQ.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SEQ.BlockPaint = false;
            this.IDH_SEQ.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SEQ.CornerRadius = 2;
            this.IDH_SEQ.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SEQ.Enabled = false;
            this.IDH_SEQ.EnabledColor = System.Drawing.Color.White;
            this.IDH_SEQ.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SEQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SEQ.Location = new System.Drawing.Point(130, 175);
            this.IDH_SEQ.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SEQ.Multiline = true;
            this.IDH_SEQ.Name = "IDH_SEQ";
            this.IDH_SEQ.PasswordChar = '\0';
            this.IDH_SEQ.ReadOnly = false;
            this.IDH_SEQ.Size = new System.Drawing.Size(155, 14);
            this.IDH_SEQ.TabIndex = 13;
            this.IDH_SEQ.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SEQ.TextValue = "";
            this.IDH_SEQ.WordWrap = true;
            // 
            // IDH_ROUTE
            // 
            this.IDH_ROUTE.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ROUTE.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ROUTE.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ROUTE.BlockPaint = false;
            this.IDH_ROUTE.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ROUTE.CornerRadius = 2;
            this.IDH_ROUTE.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ROUTE.Enabled = false;
            this.IDH_ROUTE.EnabledColor = System.Drawing.Color.White;
            this.IDH_ROUTE.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ROUTE.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ROUTE.Location = new System.Drawing.Point(130, 160);
            this.IDH_ROUTE.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ROUTE.Multiline = true;
            this.IDH_ROUTE.Name = "IDH_ROUTE";
            this.IDH_ROUTE.PasswordChar = '\0';
            this.IDH_ROUTE.ReadOnly = false;
            this.IDH_ROUTE.Size = new System.Drawing.Size(155, 14);
            this.IDH_ROUTE.TabIndex = 11;
            this.IDH_ROUTE.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ROUTE.TextValue = "";
            this.IDH_ROUTE.WordWrap = true;
            // 
            // label41
            // 
            this.label41.Location = new System.Drawing.Point(4, 160);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(100, 15);
            this.label41.TabIndex = 97;
            this.label41.Text = "Route";
            // 
            // IDH_CNA
            // 
            this.IDH_CNA.BlockPaint = false;
            this.IDH_CNA.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CNA.Caption = "Create &New Address";
            this.IDH_CNA.CornerRadius = 2;
            this.IDH_CNA.DefaultButton = false;
            this.IDH_CNA.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CNA.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_CNA.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CNA.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CNA.Image = null;
            this.IDH_CNA.Location = new System.Drawing.Point(417, 31);
            this.IDH_CNA.Name = "IDH_CNA";
            this.IDH_CNA.Pressed = false;
            this.IDH_CNA.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_CNA.Size = new System.Drawing.Size(135, 14);
            this.IDH_CNA.TabIndex = 96;
            this.IDH_CNA.Click += new System.EventHandler(this.IDH_CNA_Click);
            // 
            // IDH_CUSAL
            // 
            this.IDH_CUSAL.BlockPaint = false;
            this.IDH_CUSAL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSAL.Caption = "";
            this.IDH_CUSAL.CornerRadius = 2;
            this.IDH_CUSAL.DefaultButton = false;
            this.IDH_CUSAL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSAL.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_CUSAL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSAL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_CUSAL.Image")));
            this.IDH_CUSAL.Location = new System.Drawing.Point(285, 25);
            this.IDH_CUSAL.Name = "IDH_CUSAL";
            this.IDH_CUSAL.Pressed = false;
            this.IDH_CUSAL.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_CUSAL.Size = new System.Drawing.Size(14, 14);
            this.IDH_CUSAL.TabIndex = 95;
            this.IDH_CUSAL.Click += new System.EventHandler(this.AddressSearchClick);
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(4, 145);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(106, 15);
            this.label40.TabIndex = 94;
            this.label40.Text = "Waste Lic. Reg. No.";
            // 
            // IDH_CUSLIC
            // 
            this.IDH_CUSLIC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSLIC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSLIC.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSLIC.BlockPaint = false;
            this.IDH_CUSLIC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSLIC.CornerRadius = 2;
            this.IDH_CUSLIC.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSLIC.Enabled = false;
            this.IDH_CUSLIC.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSLIC.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSLIC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSLIC.Location = new System.Drawing.Point(130, 145);
            this.IDH_CUSLIC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSLIC.Multiline = true;
            this.IDH_CUSLIC.Name = "IDH_CUSLIC";
            this.IDH_CUSLIC.PasswordChar = '\0';
            this.IDH_CUSLIC.ReadOnly = false;
            this.IDH_CUSLIC.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSLIC.TabIndex = 9;
            this.IDH_CUSLIC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSLIC.TextValue = "";
            this.IDH_CUSLIC.WordWrap = true;
            // 
            // IDH_SITETL
            // 
            this.IDH_SITETL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SITETL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SITETL.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITETL.BlockPaint = false;
            this.IDH_SITETL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITETL.CornerRadius = 2;
            this.IDH_SITETL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITETL.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITETL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITETL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SITETL.Location = new System.Drawing.Point(130, 130);
            this.IDH_SITETL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITETL.Multiline = true;
            this.IDH_SITETL.Name = "IDH_SITETL";
            this.IDH_SITETL.PasswordChar = '\0';
            this.IDH_SITETL.ReadOnly = false;
            this.IDH_SITETL.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITETL.TabIndex = 8;
            this.IDH_SITETL.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITETL.TextValue = "";
            this.IDH_SITETL.WordWrap = true;
            // 
            // label39
            // 
            this.label39.Location = new System.Drawing.Point(4, 130);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(100, 15);
            this.label39.TabIndex = 91;
            this.label39.Text = "Site Phone No.";
            // 
            // IDH_CUSPHO
            // 
            this.IDH_CUSPHO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSPHO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSPHO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSPHO.BlockPaint = false;
            this.IDH_CUSPHO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSPHO.CornerRadius = 2;
            this.IDH_CUSPHO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSPHO.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSPHO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSPHO.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSPHO.Location = new System.Drawing.Point(130, 115);
            this.IDH_CUSPHO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSPHO.Multiline = true;
            this.IDH_CUSPHO.Name = "IDH_CUSPHO";
            this.IDH_CUSPHO.PasswordChar = '\0';
            this.IDH_CUSPHO.ReadOnly = false;
            this.IDH_CUSPHO.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSPHO.TabIndex = 7;
            this.IDH_CUSPHO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSPHO.TextValue = "";
            this.IDH_CUSPHO.WordWrap = true;
            // 
            // IDH_CUSPOS
            // 
            this.IDH_CUSPOS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSPOS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSPOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSPOS.BlockPaint = false;
            this.IDH_CUSPOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSPOS.CornerRadius = 2;
            this.IDH_CUSPOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSPOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSPOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSPOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSPOS.Location = new System.Drawing.Point(130, 100);
            this.IDH_CUSPOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSPOS.Multiline = true;
            this.IDH_CUSPOS.Name = "IDH_CUSPOS";
            this.IDH_CUSPOS.PasswordChar = '\0';
            this.IDH_CUSPOS.ReadOnly = false;
            this.IDH_CUSPOS.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSPOS.TabIndex = 6;
            this.IDH_CUSPOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSPOS.TextValue = "";
            this.IDH_CUSPOS.WordWrap = true;
            // 
            // IDH_COUNTY
            // 
            this.IDH_COUNTY.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_COUNTY.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_COUNTY.BackColor = System.Drawing.Color.Transparent;
            this.IDH_COUNTY.BlockPaint = false;
            this.IDH_COUNTY.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_COUNTY.CornerRadius = 2;
            this.IDH_COUNTY.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_COUNTY.EnabledColor = System.Drawing.Color.White;
            this.IDH_COUNTY.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_COUNTY.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_COUNTY.Location = new System.Drawing.Point(130, 85);
            this.IDH_COUNTY.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_COUNTY.Multiline = true;
            this.IDH_COUNTY.Name = "IDH_COUNTY";
            this.IDH_COUNTY.PasswordChar = '\0';
            this.IDH_COUNTY.ReadOnly = false;
            this.IDH_COUNTY.Size = new System.Drawing.Size(155, 14);
            this.IDH_COUNTY.TabIndex = 5;
            this.IDH_COUNTY.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_COUNTY.TextValue = "";
            this.IDH_COUNTY.WordWrap = true;
            // 
            // IDH_CUSCIT
            // 
            this.IDH_CUSCIT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSCIT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSCIT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSCIT.BlockPaint = false;
            this.IDH_CUSCIT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSCIT.CornerRadius = 2;
            this.IDH_CUSCIT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSCIT.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSCIT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSCIT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSCIT.Location = new System.Drawing.Point(130, 70);
            this.IDH_CUSCIT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSCIT.Multiline = true;
            this.IDH_CUSCIT.Name = "IDH_CUSCIT";
            this.IDH_CUSCIT.PasswordChar = '\0';
            this.IDH_CUSCIT.ReadOnly = false;
            this.IDH_CUSCIT.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSCIT.TabIndex = 4;
            this.IDH_CUSCIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSCIT.TextValue = "";
            this.IDH_CUSCIT.WordWrap = true;
            // 
            // IDH_CUSBLO
            // 
            this.IDH_CUSBLO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSBLO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSBLO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSBLO.BlockPaint = false;
            this.IDH_CUSBLO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSBLO.CornerRadius = 2;
            this.IDH_CUSBLO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSBLO.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSBLO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSBLO.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSBLO.Location = new System.Drawing.Point(130, 55);
            this.IDH_CUSBLO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSBLO.Multiline = true;
            this.IDH_CUSBLO.Name = "IDH_CUSBLO";
            this.IDH_CUSBLO.PasswordChar = '\0';
            this.IDH_CUSBLO.ReadOnly = false;
            this.IDH_CUSBLO.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSBLO.TabIndex = 3;
            this.IDH_CUSBLO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSBLO.TextValue = "";
            this.IDH_CUSBLO.WordWrap = true;
            // 
            // IDH_CUSSTR
            // 
            this.IDH_CUSSTR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSSTR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSSTR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSSTR.BlockPaint = false;
            this.IDH_CUSSTR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSSTR.CornerRadius = 2;
            this.IDH_CUSSTR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSSTR.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSSTR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSSTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSSTR.Location = new System.Drawing.Point(130, 40);
            this.IDH_CUSSTR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSSTR.Multiline = true;
            this.IDH_CUSSTR.Name = "IDH_CUSSTR";
            this.IDH_CUSSTR.PasswordChar = '\0';
            this.IDH_CUSSTR.ReadOnly = false;
            this.IDH_CUSSTR.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSSTR.TabIndex = 2;
            this.IDH_CUSSTR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSSTR.TextValue = "";
            this.IDH_CUSSTR.WordWrap = true;
            // 
            // IDH_CUSADD
            // 
            this.IDH_CUSADD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSADD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSADD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSADD.BlockPaint = false;
            this.IDH_CUSADD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSADD.CornerRadius = 2;
            this.IDH_CUSADD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSADD.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSADD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSADD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSADD.Location = new System.Drawing.Point(130, 25);
            this.IDH_CUSADD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSADD.Multiline = true;
            this.IDH_CUSADD.Name = "IDH_CUSADD";
            this.IDH_CUSADD.PasswordChar = '\0';
            this.IDH_CUSADD.ReadOnly = false;
            this.IDH_CUSADD.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSADD.TabIndex = 1;
            this.IDH_CUSADD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSADD.TextValue = "";
            this.IDH_CUSADD.WordWrap = true;
            // 
            // IDH_CUSCON
            // 
            this.IDH_CUSCON.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSCON.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSCON.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSCON.BlockPaint = false;
            this.IDH_CUSCON.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSCON.CornerRadius = 2;
            this.IDH_CUSCON.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSCON.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSCON.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSCON.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSCON.Location = new System.Drawing.Point(130, 10);
            this.IDH_CUSCON.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSCON.Multiline = true;
            this.IDH_CUSCON.Name = "IDH_CUSCON";
            this.IDH_CUSCON.PasswordChar = '\0';
            this.IDH_CUSCON.ReadOnly = false;
            this.IDH_CUSCON.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSCON.TabIndex = 0;
            this.IDH_CUSCON.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSCON.TextValue = "";
            this.IDH_CUSCON.WordWrap = true;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(4, 115);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(100, 15);
            this.label38.TabIndex = 82;
            this.label38.Text = "Phone No.";
            // 
            // label37
            // 
            this.label37.Location = new System.Drawing.Point(4, 100);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(100, 15);
            this.label37.TabIndex = 81;
            this.label37.Text = "Post Code";
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(4, 85);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(100, 15);
            this.label36.TabIndex = 80;
            this.label36.Text = "County";
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(4, 70);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(100, 17);
            this.label35.TabIndex = 79;
            this.label35.Text = "City";
            // 
            // label34
            // 
            this.label34.Location = new System.Drawing.Point(4, 55);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(100, 23);
            this.label34.TabIndex = 78;
            this.label34.Text = "Block";
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(4, 40);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(100, 23);
            this.label33.TabIndex = 77;
            this.label33.Text = "Street";
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(4, 25);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(100, 17);
            this.label32.TabIndex = 76;
            this.label32.Text = "Address";
            // 
            // label31
            // 
            this.label31.Location = new System.Drawing.Point(4, 10);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(100, 18);
            this.label31.TabIndex = 75;
            this.label31.Text = "Contact Person";
            // 
            // oTPProducer
            // 
            this.oTPProducer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPProducer.Controls.Add(this.IDH_PROAL);
            this.oTPProducer.Controls.Add(this.IDH_ORIGLU);
            this.oTPProducer.Controls.Add(this.IDH_PRDL);
            this.oTPProducer.Controls.Add(this.IDH_ORIGIN);
            this.oTPProducer.Controls.Add(this.label65);
            this.oTPProducer.Controls.Add(this.IDH_PRDPHO);
            this.oTPProducer.Controls.Add(this.IDH_PRDPOS);
            this.oTPProducer.Controls.Add(this.IDH_PRDCIT);
            this.oTPProducer.Controls.Add(this.label62);
            this.oTPProducer.Controls.Add(this.label63);
            this.oTPProducer.Controls.Add(this.label64);
            this.oTPProducer.Controls.Add(this.IDH_PRDBLO);
            this.oTPProducer.Controls.Add(this.IDH_PRDSTR);
            this.oTPProducer.Controls.Add(this.IDH_PRDADD);
            this.oTPProducer.Controls.Add(this.IDH_PRDCRF);
            this.oTPProducer.Controls.Add(this.IDH_PRDCON);
            this.oTPProducer.Controls.Add(this.IDH_WNAM);
            this.oTPProducer.Controls.Add(this.IDH_WPRODU);
            this.oTPProducer.Controls.Add(this.label55);
            this.oTPProducer.Controls.Add(this.label56);
            this.oTPProducer.Controls.Add(this.label57);
            this.oTPProducer.Controls.Add(this.label58);
            this.oTPProducer.Controls.Add(this.label59);
            this.oTPProducer.Controls.Add(this.label60);
            this.oTPProducer.Controls.Add(this.label61);
            this.oTPProducer.Location = new System.Drawing.Point(4, 21);
            this.oTPProducer.Name = "oTPProducer";
            this.oTPProducer.Size = new System.Drawing.Size(822, 307);
            this.oTPProducer.TabIndex = 3;
            this.oTPProducer.Text = "Producer";
            // 
            // IDH_PROAL
            // 
            this.IDH_PROAL.BlockPaint = false;
            this.IDH_PROAL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PROAL.Caption = "";
            this.IDH_PROAL.CornerRadius = 2;
            this.IDH_PROAL.DefaultButton = false;
            this.IDH_PROAL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PROAL.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_PROAL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PROAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PROAL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_PROAL.Image")));
            this.IDH_PROAL.Location = new System.Drawing.Point(287, 70);
            this.IDH_PROAL.Name = "IDH_PROAL";
            this.IDH_PROAL.Pressed = false;
            this.IDH_PROAL.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_PROAL.Size = new System.Drawing.Size(14, 14);
            this.IDH_PROAL.TabIndex = 115;
            this.IDH_PROAL.Click += new System.EventHandler(this.AddressSearchClick);
            // 
            // IDH_ORIGLU
            // 
            this.IDH_ORIGLU.BlockPaint = false;
            this.IDH_ORIGLU.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORIGLU.Caption = "";
            this.IDH_ORIGLU.CornerRadius = 2;
            this.IDH_ORIGLU.DefaultButton = false;
            this.IDH_ORIGLU.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORIGLU.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_ORIGLU.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORIGLU.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ORIGLU.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ORIGLU.Image")));
            this.IDH_ORIGLU.Location = new System.Drawing.Point(287, 160);
            this.IDH_ORIGLU.Name = "IDH_ORIGLU";
            this.IDH_ORIGLU.Pressed = false;
            this.IDH_ORIGLU.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_ORIGLU.Size = new System.Drawing.Size(14, 14);
            this.IDH_ORIGLU.TabIndex = 114;
            this.IDH_ORIGLU.Click += new System.EventHandler(this.IDH_ORLKClick);
            // 
            // IDH_PRDL
            // 
            this.IDH_PRDL.BlockPaint = false;
            this.IDH_PRDL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDL.Caption = "";
            this.IDH_PRDL.CornerRadius = 2;
            this.IDH_PRDL.DefaultButton = false;
            this.IDH_PRDL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDL.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_PRDL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PRDL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_PRDL.Image")));
            this.IDH_PRDL.Location = new System.Drawing.Point(287, 10);
            this.IDH_PRDL.Name = "IDH_PRDL";
            this.IDH_PRDL.Pressed = false;
            this.IDH_PRDL.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_PRDL.Size = new System.Drawing.Size(14, 14);
            this.IDH_PRDL.TabIndex = 113;
            this.IDH_PRDL.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_ORIGIN
            // 
            this.IDH_ORIGIN.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ORIGIN.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ORIGIN.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ORIGIN.BlockPaint = false;
            this.IDH_ORIGIN.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORIGIN.CornerRadius = 2;
            this.IDH_ORIGIN.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORIGIN.EnabledColor = System.Drawing.Color.White;
            this.IDH_ORIGIN.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORIGIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ORIGIN.Location = new System.Drawing.Point(132, 160);
            this.IDH_ORIGIN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ORIGIN.Multiline = true;
            this.IDH_ORIGIN.Name = "IDH_ORIGIN";
            this.IDH_ORIGIN.PasswordChar = '\0';
            this.IDH_ORIGIN.ReadOnly = false;
            this.IDH_ORIGIN.Size = new System.Drawing.Size(155, 14);
            this.IDH_ORIGIN.TabIndex = 10;
            this.IDH_ORIGIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ORIGIN.TextValue = "";
            this.IDH_ORIGIN.WordWrap = true;
            // 
            // label65
            // 
            this.label65.Location = new System.Drawing.Point(4, 165);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(100, 23);
            this.label65.TabIndex = 111;
            this.label65.Text = "Origin";
            // 
            // IDH_PRDPHO
            // 
            this.IDH_PRDPHO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PRDPHO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PRDPHO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDPHO.BlockPaint = false;
            this.IDH_PRDPHO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDPHO.CornerRadius = 2;
            this.IDH_PRDPHO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDPHO.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDPHO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDPHO.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PRDPHO.Location = new System.Drawing.Point(132, 145);
            this.IDH_PRDPHO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDPHO.Multiline = true;
            this.IDH_PRDPHO.Name = "IDH_PRDPHO";
            this.IDH_PRDPHO.PasswordChar = '\0';
            this.IDH_PRDPHO.ReadOnly = false;
            this.IDH_PRDPHO.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDPHO.TabIndex = 9;
            this.IDH_PRDPHO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDPHO.TextValue = "";
            this.IDH_PRDPHO.WordWrap = true;
            // 
            // IDH_PRDPOS
            // 
            this.IDH_PRDPOS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PRDPOS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PRDPOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDPOS.BlockPaint = false;
            this.IDH_PRDPOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDPOS.CornerRadius = 2;
            this.IDH_PRDPOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDPOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDPOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDPOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PRDPOS.Location = new System.Drawing.Point(132, 130);
            this.IDH_PRDPOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDPOS.Multiline = true;
            this.IDH_PRDPOS.Name = "IDH_PRDPOS";
            this.IDH_PRDPOS.PasswordChar = '\0';
            this.IDH_PRDPOS.ReadOnly = false;
            this.IDH_PRDPOS.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDPOS.TabIndex = 8;
            this.IDH_PRDPOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDPOS.TextValue = "";
            this.IDH_PRDPOS.WordWrap = true;
            // 
            // IDH_PRDCIT
            // 
            this.IDH_PRDCIT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PRDCIT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PRDCIT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDCIT.BlockPaint = false;
            this.IDH_PRDCIT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDCIT.CornerRadius = 2;
            this.IDH_PRDCIT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDCIT.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDCIT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDCIT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PRDCIT.Location = new System.Drawing.Point(132, 115);
            this.IDH_PRDCIT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDCIT.Multiline = true;
            this.IDH_PRDCIT.Name = "IDH_PRDCIT";
            this.IDH_PRDCIT.PasswordChar = '\0';
            this.IDH_PRDCIT.ReadOnly = false;
            this.IDH_PRDCIT.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDCIT.TabIndex = 7;
            this.IDH_PRDCIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDCIT.TextValue = "";
            this.IDH_PRDCIT.WordWrap = true;
            // 
            // label62
            // 
            this.label62.Location = new System.Drawing.Point(4, 150);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(100, 14);
            this.label62.TabIndex = 107;
            this.label62.Text = "Phone No.";
            // 
            // label63
            // 
            this.label63.Location = new System.Drawing.Point(4, 130);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(100, 23);
            this.label63.TabIndex = 106;
            this.label63.Text = "Post Code";
            // 
            // label64
            // 
            this.label64.Location = new System.Drawing.Point(4, 115);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(100, 14);
            this.label64.TabIndex = 105;
            this.label64.Text = "City";
            // 
            // IDH_PRDBLO
            // 
            this.IDH_PRDBLO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PRDBLO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PRDBLO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDBLO.BlockPaint = false;
            this.IDH_PRDBLO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDBLO.CornerRadius = 2;
            this.IDH_PRDBLO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDBLO.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDBLO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDBLO.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PRDBLO.Location = new System.Drawing.Point(132, 100);
            this.IDH_PRDBLO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDBLO.Multiline = true;
            this.IDH_PRDBLO.Name = "IDH_PRDBLO";
            this.IDH_PRDBLO.PasswordChar = '\0';
            this.IDH_PRDBLO.ReadOnly = false;
            this.IDH_PRDBLO.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDBLO.TabIndex = 6;
            this.IDH_PRDBLO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDBLO.TextValue = "";
            this.IDH_PRDBLO.WordWrap = true;
            // 
            // IDH_PRDSTR
            // 
            this.IDH_PRDSTR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PRDSTR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PRDSTR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDSTR.BlockPaint = false;
            this.IDH_PRDSTR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDSTR.CornerRadius = 2;
            this.IDH_PRDSTR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDSTR.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDSTR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDSTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PRDSTR.Location = new System.Drawing.Point(132, 85);
            this.IDH_PRDSTR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDSTR.Multiline = true;
            this.IDH_PRDSTR.Name = "IDH_PRDSTR";
            this.IDH_PRDSTR.PasswordChar = '\0';
            this.IDH_PRDSTR.ReadOnly = false;
            this.IDH_PRDSTR.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDSTR.TabIndex = 5;
            this.IDH_PRDSTR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDSTR.TextValue = "";
            this.IDH_PRDSTR.WordWrap = true;
            // 
            // IDH_PRDADD
            // 
            this.IDH_PRDADD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PRDADD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PRDADD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDADD.BlockPaint = false;
            this.IDH_PRDADD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDADD.CornerRadius = 2;
            this.IDH_PRDADD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDADD.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDADD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDADD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PRDADD.Location = new System.Drawing.Point(132, 70);
            this.IDH_PRDADD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDADD.Multiline = true;
            this.IDH_PRDADD.Name = "IDH_PRDADD";
            this.IDH_PRDADD.PasswordChar = '\0';
            this.IDH_PRDADD.ReadOnly = false;
            this.IDH_PRDADD.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDADD.TabIndex = 4;
            this.IDH_PRDADD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDADD.TextValue = "";
            this.IDH_PRDADD.WordWrap = true;
            // 
            // IDH_PRDCRF
            // 
            this.IDH_PRDCRF.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PRDCRF.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PRDCRF.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDCRF.BlockPaint = false;
            this.IDH_PRDCRF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDCRF.CornerRadius = 2;
            this.IDH_PRDCRF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDCRF.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDCRF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDCRF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PRDCRF.Location = new System.Drawing.Point(132, 55);
            this.IDH_PRDCRF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDCRF.Multiline = true;
            this.IDH_PRDCRF.Name = "IDH_PRDCRF";
            this.IDH_PRDCRF.PasswordChar = '\0';
            this.IDH_PRDCRF.ReadOnly = false;
            this.IDH_PRDCRF.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDCRF.TabIndex = 3;
            this.IDH_PRDCRF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDCRF.TextValue = "";
            this.IDH_PRDCRF.WordWrap = true;
            // 
            // IDH_PRDCON
            // 
            this.IDH_PRDCON.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_PRDCON.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_PRDCON.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDCON.BlockPaint = false;
            this.IDH_PRDCON.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDCON.CornerRadius = 2;
            this.IDH_PRDCON.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDCON.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDCON.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDCON.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_PRDCON.Location = new System.Drawing.Point(132, 40);
            this.IDH_PRDCON.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDCON.Multiline = true;
            this.IDH_PRDCON.Name = "IDH_PRDCON";
            this.IDH_PRDCON.PasswordChar = '\0';
            this.IDH_PRDCON.ReadOnly = false;
            this.IDH_PRDCON.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDCON.TabIndex = 2;
            this.IDH_PRDCON.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDCON.TextValue = "";
            this.IDH_PRDCON.WordWrap = true;
            // 
            // IDH_WNAM
            // 
            this.IDH_WNAM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WNAM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WNAM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WNAM.BlockPaint = false;
            this.IDH_WNAM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WNAM.CornerRadius = 2;
            this.IDH_WNAM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WNAM.EnabledColor = System.Drawing.Color.White;
            this.IDH_WNAM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WNAM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WNAM.Location = new System.Drawing.Point(132, 25);
            this.IDH_WNAM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WNAM.Multiline = true;
            this.IDH_WNAM.Name = "IDH_WNAM";
            this.IDH_WNAM.PasswordChar = '\0';
            this.IDH_WNAM.ReadOnly = false;
            this.IDH_WNAM.Size = new System.Drawing.Size(155, 14);
            this.IDH_WNAM.TabIndex = 1;
            this.IDH_WNAM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WNAM.TextValue = "";
            this.IDH_WNAM.WordWrap = true;
            // 
            // IDH_WPRODU
            // 
            this.IDH_WPRODU.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WPRODU.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WPRODU.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WPRODU.BlockPaint = false;
            this.IDH_WPRODU.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WPRODU.CornerRadius = 2;
            this.IDH_WPRODU.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WPRODU.EnabledColor = System.Drawing.Color.White;
            this.IDH_WPRODU.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WPRODU.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WPRODU.Location = new System.Drawing.Point(132, 10);
            this.IDH_WPRODU.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WPRODU.Multiline = true;
            this.IDH_WPRODU.Name = "IDH_WPRODU";
            this.IDH_WPRODU.PasswordChar = '\0';
            this.IDH_WPRODU.ReadOnly = false;
            this.IDH_WPRODU.Size = new System.Drawing.Size(155, 14);
            this.IDH_WPRODU.TabIndex = 0;
            this.IDH_WPRODU.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WPRODU.TextValue = "";
            this.IDH_WPRODU.WordWrap = true;
            // 
            // label55
            // 
            this.label55.Location = new System.Drawing.Point(4, 100);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(100, 14);
            this.label55.TabIndex = 97;
            this.label55.Text = "Block";
            // 
            // label56
            // 
            this.label56.Location = new System.Drawing.Point(4, 85);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(100, 14);
            this.label56.TabIndex = 96;
            this.label56.Text = "Street";
            // 
            // label57
            // 
            this.label57.Location = new System.Drawing.Point(4, 70);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(100, 14);
            this.label57.TabIndex = 95;
            this.label57.Text = "Address";
            // 
            // label58
            // 
            this.label58.Location = new System.Drawing.Point(4, 55);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(100, 14);
            this.label58.TabIndex = 94;
            this.label58.Text = "Producer Ref. No.";
            // 
            // label59
            // 
            this.label59.Location = new System.Drawing.Point(4, 40);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(100, 23);
            this.label59.TabIndex = 93;
            this.label59.Text = "Contact Person";
            // 
            // label60
            // 
            this.label60.Location = new System.Drawing.Point(4, 25);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(100, 23);
            this.label60.TabIndex = 92;
            this.label60.Text = "Name";
            // 
            // label61
            // 
            this.label61.Location = new System.Drawing.Point(4, 10);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(100, 18);
            this.label61.TabIndex = 91;
            this.label61.Text = "Producer";
            // 
            // oTPSite
            // 
            this.oTPSite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPSite.Controls.Add(this.IDH_SITAL);
            this.oTPSite.Controls.Add(this.IDH_SITL);
            this.oTPSite.Controls.Add(this.IDH_SITPHO);
            this.oTPSite.Controls.Add(this.IDH_SITPOS);
            this.oTPSite.Controls.Add(this.IDH_SITCIT);
            this.oTPSite.Controls.Add(this.label66);
            this.oTPSite.Controls.Add(this.label67);
            this.oTPSite.Controls.Add(this.label68);
            this.oTPSite.Controls.Add(this.IDH_SITBLO);
            this.oTPSite.Controls.Add(this.IDH_SITSTR);
            this.oTPSite.Controls.Add(this.IDH_SITADD);
            this.oTPSite.Controls.Add(this.IDH_SITCRF);
            this.oTPSite.Controls.Add(this.IDH_SITCON);
            this.oTPSite.Controls.Add(this.IDH_DISNAM);
            this.oTPSite.Controls.Add(this.IDH_DISSIT);
            this.oTPSite.Controls.Add(this.label69);
            this.oTPSite.Controls.Add(this.label70);
            this.oTPSite.Controls.Add(this.label71);
            this.oTPSite.Controls.Add(this.label72);
            this.oTPSite.Controls.Add(this.label73);
            this.oTPSite.Controls.Add(this.label74);
            this.oTPSite.Controls.Add(this.label75);
            this.oTPSite.Location = new System.Drawing.Point(4, 21);
            this.oTPSite.Name = "oTPSite";
            this.oTPSite.Size = new System.Drawing.Size(822, 307);
            this.oTPSite.TabIndex = 4;
            this.oTPSite.Text = "Disposal Site";
            // 
            // IDH_SITAL
            // 
            this.IDH_SITAL.BlockPaint = false;
            this.IDH_SITAL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITAL.Caption = "";
            this.IDH_SITAL.CornerRadius = 2;
            this.IDH_SITAL.DefaultButton = false;
            this.IDH_SITAL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITAL.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_SITAL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SITAL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_SITAL.Image")));
            this.IDH_SITAL.Location = new System.Drawing.Point(286, 70);
            this.IDH_SITAL.Name = "IDH_SITAL";
            this.IDH_SITAL.Pressed = false;
            this.IDH_SITAL.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_SITAL.Size = new System.Drawing.Size(14, 14);
            this.IDH_SITAL.TabIndex = 137;
            this.IDH_SITAL.Click += new System.EventHandler(this.AddressSearchClick);
            // 
            // IDH_SITL
            // 
            this.IDH_SITL.BlockPaint = false;
            this.IDH_SITL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITL.Caption = "";
            this.IDH_SITL.CornerRadius = 2;
            this.IDH_SITL.DefaultButton = false;
            this.IDH_SITL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITL.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_SITL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SITL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_SITL.Image")));
            this.IDH_SITL.Location = new System.Drawing.Point(286, 10);
            this.IDH_SITL.Name = "IDH_SITL";
            this.IDH_SITL.Pressed = false;
            this.IDH_SITL.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_SITL.Size = new System.Drawing.Size(14, 14);
            this.IDH_SITL.TabIndex = 136;
            this.IDH_SITL.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_SITPHO
            // 
            this.IDH_SITPHO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SITPHO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SITPHO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITPHO.BlockPaint = false;
            this.IDH_SITPHO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITPHO.CornerRadius = 2;
            this.IDH_SITPHO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITPHO.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITPHO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITPHO.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SITPHO.Location = new System.Drawing.Point(131, 145);
            this.IDH_SITPHO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITPHO.Multiline = true;
            this.IDH_SITPHO.Name = "IDH_SITPHO";
            this.IDH_SITPHO.PasswordChar = '\0';
            this.IDH_SITPHO.ReadOnly = false;
            this.IDH_SITPHO.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITPHO.TabIndex = 135;
            this.IDH_SITPHO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITPHO.TextValue = "";
            this.IDH_SITPHO.WordWrap = true;
            // 
            // IDH_SITPOS
            // 
            this.IDH_SITPOS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SITPOS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SITPOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITPOS.BlockPaint = false;
            this.IDH_SITPOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITPOS.CornerRadius = 2;
            this.IDH_SITPOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITPOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITPOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITPOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SITPOS.Location = new System.Drawing.Point(131, 130);
            this.IDH_SITPOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITPOS.Multiline = true;
            this.IDH_SITPOS.Name = "IDH_SITPOS";
            this.IDH_SITPOS.PasswordChar = '\0';
            this.IDH_SITPOS.ReadOnly = false;
            this.IDH_SITPOS.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITPOS.TabIndex = 134;
            this.IDH_SITPOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITPOS.TextValue = "";
            this.IDH_SITPOS.WordWrap = true;
            // 
            // IDH_SITCIT
            // 
            this.IDH_SITCIT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SITCIT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SITCIT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITCIT.BlockPaint = false;
            this.IDH_SITCIT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITCIT.CornerRadius = 2;
            this.IDH_SITCIT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITCIT.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITCIT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITCIT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SITCIT.Location = new System.Drawing.Point(131, 115);
            this.IDH_SITCIT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITCIT.Multiline = true;
            this.IDH_SITCIT.Name = "IDH_SITCIT";
            this.IDH_SITCIT.PasswordChar = '\0';
            this.IDH_SITCIT.ReadOnly = false;
            this.IDH_SITCIT.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITCIT.TabIndex = 133;
            this.IDH_SITCIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITCIT.TextValue = "";
            this.IDH_SITCIT.WordWrap = true;
            // 
            // label66
            // 
            this.label66.Location = new System.Drawing.Point(4, 150);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(100, 14);
            this.label66.TabIndex = 132;
            this.label66.Text = "Phone No.";
            // 
            // label67
            // 
            this.label67.Location = new System.Drawing.Point(4, 130);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(100, 14);
            this.label67.TabIndex = 131;
            this.label67.Text = "Post Code";
            // 
            // label68
            // 
            this.label68.Location = new System.Drawing.Point(4, 115);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(100, 14);
            this.label68.TabIndex = 130;
            this.label68.Text = "City";
            // 
            // IDH_SITBLO
            // 
            this.IDH_SITBLO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SITBLO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SITBLO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITBLO.BlockPaint = false;
            this.IDH_SITBLO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITBLO.CornerRadius = 2;
            this.IDH_SITBLO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITBLO.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITBLO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITBLO.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SITBLO.Location = new System.Drawing.Point(131, 100);
            this.IDH_SITBLO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITBLO.Multiline = true;
            this.IDH_SITBLO.Name = "IDH_SITBLO";
            this.IDH_SITBLO.PasswordChar = '\0';
            this.IDH_SITBLO.ReadOnly = false;
            this.IDH_SITBLO.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITBLO.TabIndex = 129;
            this.IDH_SITBLO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITBLO.TextValue = "";
            this.IDH_SITBLO.WordWrap = true;
            // 
            // IDH_SITSTR
            // 
            this.IDH_SITSTR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SITSTR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SITSTR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITSTR.BlockPaint = false;
            this.IDH_SITSTR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITSTR.CornerRadius = 2;
            this.IDH_SITSTR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITSTR.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITSTR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITSTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SITSTR.Location = new System.Drawing.Point(131, 85);
            this.IDH_SITSTR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITSTR.Multiline = true;
            this.IDH_SITSTR.Name = "IDH_SITSTR";
            this.IDH_SITSTR.PasswordChar = '\0';
            this.IDH_SITSTR.ReadOnly = false;
            this.IDH_SITSTR.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITSTR.TabIndex = 128;
            this.IDH_SITSTR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITSTR.TextValue = "";
            this.IDH_SITSTR.WordWrap = true;
            // 
            // IDH_SITADD
            // 
            this.IDH_SITADD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SITADD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SITADD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITADD.BlockPaint = false;
            this.IDH_SITADD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITADD.CornerRadius = 2;
            this.IDH_SITADD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITADD.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITADD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITADD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SITADD.Location = new System.Drawing.Point(131, 70);
            this.IDH_SITADD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITADD.Multiline = true;
            this.IDH_SITADD.Name = "IDH_SITADD";
            this.IDH_SITADD.PasswordChar = '\0';
            this.IDH_SITADD.ReadOnly = false;
            this.IDH_SITADD.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITADD.TabIndex = 127;
            this.IDH_SITADD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITADD.TextValue = "";
            this.IDH_SITADD.WordWrap = true;
            // 
            // IDH_SITCRF
            // 
            this.IDH_SITCRF.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SITCRF.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SITCRF.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITCRF.BlockPaint = false;
            this.IDH_SITCRF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITCRF.CornerRadius = 2;
            this.IDH_SITCRF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITCRF.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITCRF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITCRF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SITCRF.Location = new System.Drawing.Point(131, 55);
            this.IDH_SITCRF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITCRF.Multiline = true;
            this.IDH_SITCRF.Name = "IDH_SITCRF";
            this.IDH_SITCRF.PasswordChar = '\0';
            this.IDH_SITCRF.ReadOnly = false;
            this.IDH_SITCRF.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITCRF.TabIndex = 126;
            this.IDH_SITCRF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITCRF.TextValue = "";
            this.IDH_SITCRF.WordWrap = true;
            // 
            // IDH_SITCON
            // 
            this.IDH_SITCON.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SITCON.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SITCON.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITCON.BlockPaint = false;
            this.IDH_SITCON.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITCON.CornerRadius = 2;
            this.IDH_SITCON.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITCON.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITCON.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITCON.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SITCON.Location = new System.Drawing.Point(131, 40);
            this.IDH_SITCON.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITCON.Multiline = true;
            this.IDH_SITCON.Name = "IDH_SITCON";
            this.IDH_SITCON.PasswordChar = '\0';
            this.IDH_SITCON.ReadOnly = false;
            this.IDH_SITCON.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITCON.TabIndex = 125;
            this.IDH_SITCON.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITCON.TextValue = "";
            this.IDH_SITCON.WordWrap = true;
            // 
            // IDH_DISNAM
            // 
            this.IDH_DISNAM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DISNAM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DISNAM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DISNAM.BlockPaint = false;
            this.IDH_DISNAM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DISNAM.CornerRadius = 2;
            this.IDH_DISNAM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DISNAM.EnabledColor = System.Drawing.Color.White;
            this.IDH_DISNAM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DISNAM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DISNAM.Location = new System.Drawing.Point(131, 25);
            this.IDH_DISNAM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DISNAM.Multiline = true;
            this.IDH_DISNAM.Name = "IDH_DISNAM";
            this.IDH_DISNAM.PasswordChar = '\0';
            this.IDH_DISNAM.ReadOnly = false;
            this.IDH_DISNAM.Size = new System.Drawing.Size(155, 14);
            this.IDH_DISNAM.TabIndex = 124;
            this.IDH_DISNAM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DISNAM.TextValue = "";
            this.IDH_DISNAM.WordWrap = true;
            // 
            // IDH_DISSIT
            // 
            this.IDH_DISSIT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DISSIT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DISSIT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DISSIT.BlockPaint = false;
            this.IDH_DISSIT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DISSIT.CornerRadius = 2;
            this.IDH_DISSIT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DISSIT.EnabledColor = System.Drawing.Color.White;
            this.IDH_DISSIT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DISSIT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DISSIT.Location = new System.Drawing.Point(131, 10);
            this.IDH_DISSIT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DISSIT.Multiline = true;
            this.IDH_DISSIT.Name = "IDH_DISSIT";
            this.IDH_DISSIT.PasswordChar = '\0';
            this.IDH_DISSIT.ReadOnly = false;
            this.IDH_DISSIT.Size = new System.Drawing.Size(155, 14);
            this.IDH_DISSIT.TabIndex = 123;
            this.IDH_DISSIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DISSIT.TextValue = "";
            this.IDH_DISSIT.WordWrap = true;
            // 
            // label69
            // 
            this.label69.Location = new System.Drawing.Point(4, 101);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(100, 14);
            this.label69.TabIndex = 122;
            this.label69.Text = "Block";
            // 
            // label70
            // 
            this.label70.Location = new System.Drawing.Point(4, 85);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(100, 14);
            this.label70.TabIndex = 121;
            this.label70.Text = "Street";
            // 
            // label71
            // 
            this.label71.Location = new System.Drawing.Point(4, 70);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(100, 14);
            this.label71.TabIndex = 120;
            this.label71.Text = "Address";
            // 
            // label72
            // 
            this.label72.Location = new System.Drawing.Point(4, 55);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(100, 14);
            this.label72.TabIndex = 119;
            this.label72.Text = "Disp. Rec. Site. Ref. No.";
            // 
            // label73
            // 
            this.label73.Location = new System.Drawing.Point(4, 40);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(100, 14);
            this.label73.TabIndex = 118;
            this.label73.Text = "Contact Person";
            // 
            // label74
            // 
            this.label74.Location = new System.Drawing.Point(4, 25);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(100, 14);
            this.label74.TabIndex = 117;
            this.label74.Text = "Name";
            // 
            // label75
            // 
            this.label75.Location = new System.Drawing.Point(4, 10);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(100, 18);
            this.label75.TabIndex = 116;
            this.label75.Text = "Disposal Site";
            // 
            // oDetails
            // 
            this.oDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oDetails.Controls.Add(this.oDORows);
            this.oDetails.Location = new System.Drawing.Point(4, 21);
            this.oDetails.Name = "oDetails";
            this.oDetails.Size = new System.Drawing.Size(822, 307);
            this.oDetails.TabIndex = 0;
            this.oDetails.Text = "Details";
            // 
            // oDORows
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Beige;
            this.oDORows.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.oDORows.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.oDORows.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.oDORows.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.oDORows.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oDORows.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oDORows.GrayReadOnly = false;
            control1.CurrentMaxRows = 2000;
            control1.DBObject = null;
            control1.DoAsCol = true;
            control1.DoFilter = true;
            control1.SoftLimit = -1;
            this.oDORows.GridControl = control1;
            this.oDORows.Location = new System.Drawing.Point(4, 4);
            this.oDORows.Name = "oDORows";
            this.oDORows.RowTemplate.Height = 16;
            this.oDORows.Size = new System.Drawing.Size(813, 300);
            this.oDORows.TabIndex = 0;
            // 
            // oAdditional
            // 
            this.oAdditional.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oAdditional.Controls.Add(this.label134);
            this.oAdditional.Controls.Add(this.label135);
            this.oAdditional.Controls.Add(this.IDH_TADDCH);
            this.oAdditional.Controls.Add(this.IDH_TADDCO);
            this.oAdditional.Controls.Add(this.label132);
            this.oAdditional.Controls.Add(this.label133);
            this.oAdditional.Controls.Add(this.IDH_ADDCHV);
            this.oAdditional.Controls.Add(this.IDH_ADDCOV);
            this.oAdditional.Controls.Add(this.label112);
            this.oAdditional.Controls.Add(this.label111);
            this.oAdditional.Controls.Add(this.IDH_ADDCH);
            this.oAdditional.Controls.Add(this.IDH_ADDCO);
            this.oAdditional.Controls.Add(this.oAddGrid);
            this.oAdditional.Location = new System.Drawing.Point(4, 21);
            this.oAdditional.Name = "oAdditional";
            this.oAdditional.Padding = new System.Windows.Forms.Padding(3);
            this.oAdditional.Size = new System.Drawing.Size(822, 307);
            this.oAdditional.TabIndex = 5;
            this.oAdditional.Text = "Additional";
            // 
            // label134
            // 
            this.label134.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label134.Location = new System.Drawing.Point(671, 291);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(100, 16);
            this.label134.TabIndex = 12;
            this.label134.Text = "Total Additional Charge";
            // 
            // label135
            // 
            this.label135.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label135.Location = new System.Drawing.Point(490, 291);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(100, 15);
            this.label135.TabIndex = 11;
            this.label135.Text = "Total Additional Cost";
            // 
            // IDH_TADDCH
            // 
            this.IDH_TADDCH.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_TADDCH.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TADDCH.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TADDCH.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TADDCH.BlockPaint = false;
            this.IDH_TADDCH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TADDCH.CornerRadius = 2;
            this.IDH_TADDCH.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TADDCH.EnabledColor = System.Drawing.Color.White;
            this.IDH_TADDCH.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TADDCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TADDCH.Location = new System.Drawing.Point(771, 291);
            this.IDH_TADDCH.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TADDCH.Multiline = true;
            this.IDH_TADDCH.Name = "IDH_TADDCH";
            this.IDH_TADDCH.PasswordChar = '\0';
            this.IDH_TADDCH.ReadOnly = true;
            this.IDH_TADDCH.Size = new System.Drawing.Size(50, 14);
            this.IDH_TADDCH.TabIndex = 10;
            this.IDH_TADDCH.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TADDCH.TextValue = "";
            this.IDH_TADDCH.WordWrap = true;
            // 
            // IDH_TADDCO
            // 
            this.IDH_TADDCO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_TADDCO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_TADDCO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_TADDCO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TADDCO.BlockPaint = false;
            this.IDH_TADDCO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TADDCO.CornerRadius = 2;
            this.IDH_TADDCO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TADDCO.EnabledColor = System.Drawing.Color.White;
            this.IDH_TADDCO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TADDCO.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TADDCO.Location = new System.Drawing.Point(590, 291);
            this.IDH_TADDCO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TADDCO.Multiline = true;
            this.IDH_TADDCO.Name = "IDH_TADDCO";
            this.IDH_TADDCO.PasswordChar = '\0';
            this.IDH_TADDCO.ReadOnly = true;
            this.IDH_TADDCO.Size = new System.Drawing.Size(50, 14);
            this.IDH_TADDCO.TabIndex = 9;
            this.IDH_TADDCO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TADDCO.TextValue = "";
            this.IDH_TADDCO.WordWrap = true;
            // 
            // label132
            // 
            this.label132.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label132.Location = new System.Drawing.Point(671, 272);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(100, 16);
            this.label132.TabIndex = 8;
            this.label132.Text = "Additional Charge VAT";
            // 
            // label133
            // 
            this.label133.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label133.Location = new System.Drawing.Point(490, 272);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(100, 15);
            this.label133.TabIndex = 7;
            this.label133.Text = "Additional Cost VAT";
            // 
            // IDH_ADDCHV
            // 
            this.IDH_ADDCHV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_ADDCHV.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ADDCHV.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ADDCHV.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADDCHV.BlockPaint = false;
            this.IDH_ADDCHV.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADDCHV.CornerRadius = 2;
            this.IDH_ADDCHV.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADDCHV.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADDCHV.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADDCHV.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ADDCHV.Location = new System.Drawing.Point(771, 272);
            this.IDH_ADDCHV.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ADDCHV.Multiline = true;
            this.IDH_ADDCHV.Name = "IDH_ADDCHV";
            this.IDH_ADDCHV.PasswordChar = '\0';
            this.IDH_ADDCHV.ReadOnly = true;
            this.IDH_ADDCHV.Size = new System.Drawing.Size(50, 14);
            this.IDH_ADDCHV.TabIndex = 6;
            this.IDH_ADDCHV.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ADDCHV.TextValue = "";
            this.IDH_ADDCHV.WordWrap = true;
            // 
            // IDH_ADDCOV
            // 
            this.IDH_ADDCOV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_ADDCOV.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ADDCOV.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ADDCOV.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADDCOV.BlockPaint = false;
            this.IDH_ADDCOV.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADDCOV.CornerRadius = 2;
            this.IDH_ADDCOV.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADDCOV.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADDCOV.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADDCOV.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ADDCOV.Location = new System.Drawing.Point(590, 272);
            this.IDH_ADDCOV.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ADDCOV.Multiline = true;
            this.IDH_ADDCOV.Name = "IDH_ADDCOV";
            this.IDH_ADDCOV.PasswordChar = '\0';
            this.IDH_ADDCOV.ReadOnly = true;
            this.IDH_ADDCOV.Size = new System.Drawing.Size(50, 14);
            this.IDH_ADDCOV.TabIndex = 5;
            this.IDH_ADDCOV.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ADDCOV.TextValue = "";
            this.IDH_ADDCOV.WordWrap = true;
            // 
            // label112
            // 
            this.label112.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label112.Location = new System.Drawing.Point(671, 257);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(100, 16);
            this.label112.TabIndex = 4;
            this.label112.Text = "Additional Charge";
            // 
            // label111
            // 
            this.label111.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label111.Location = new System.Drawing.Point(490, 257);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(100, 15);
            this.label111.TabIndex = 3;
            this.label111.Text = "Additional Cost";
            // 
            // IDH_ADDCH
            // 
            this.IDH_ADDCH.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_ADDCH.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ADDCH.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ADDCH.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADDCH.BlockPaint = false;
            this.IDH_ADDCH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADDCH.CornerRadius = 2;
            this.IDH_ADDCH.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADDCH.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADDCH.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADDCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ADDCH.Location = new System.Drawing.Point(771, 257);
            this.IDH_ADDCH.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ADDCH.Multiline = true;
            this.IDH_ADDCH.Name = "IDH_ADDCH";
            this.IDH_ADDCH.PasswordChar = '\0';
            this.IDH_ADDCH.ReadOnly = true;
            this.IDH_ADDCH.Size = new System.Drawing.Size(50, 14);
            this.IDH_ADDCH.TabIndex = 2;
            this.IDH_ADDCH.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ADDCH.TextValue = "";
            this.IDH_ADDCH.WordWrap = true;
            // 
            // IDH_ADDCO
            // 
            this.IDH_ADDCO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_ADDCO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ADDCO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ADDCO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADDCO.BlockPaint = false;
            this.IDH_ADDCO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADDCO.CornerRadius = 2;
            this.IDH_ADDCO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADDCO.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADDCO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADDCO.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ADDCO.Location = new System.Drawing.Point(590, 257);
            this.IDH_ADDCO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ADDCO.Multiline = true;
            this.IDH_ADDCO.Name = "IDH_ADDCO";
            this.IDH_ADDCO.PasswordChar = '\0';
            this.IDH_ADDCO.ReadOnly = true;
            this.IDH_ADDCO.Size = new System.Drawing.Size(50, 14);
            this.IDH_ADDCO.TabIndex = 1;
            this.IDH_ADDCO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ADDCO.TextValue = "";
            this.IDH_ADDCO.WordWrap = true;
            // 
            // oAddGrid
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Beige;
            this.oAddGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.oAddGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.oAddGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.oAddGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.oAddGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.oAddGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oAddGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oAddGrid.GrayReadOnly = false;
            control2.CurrentMaxRows = 2000;
            control2.DBObject = null;
            control2.DoAsCol = true;
            control2.DoFilter = true;
            control2.SoftLimit = -1;
            this.oAddGrid.GridControl = control2;
            this.oAddGrid.Location = new System.Drawing.Point(0, 0);
            this.oAddGrid.Name = "oAddGrid";
            this.oAddGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.oAddGrid.Size = new System.Drawing.Size(821, 254);
            this.oAddGrid.TabIndex = 0;
            // 
            // oDeductions
            // 
            this.oDeductions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oDeductions.Controls.Add(this.label122);
            this.oDeductions.Controls.Add(this.label123);
            this.oDeductions.Controls.Add(this.IDH_DEDVAL);
            this.oDeductions.Controls.Add(this.IDH_DEDWEI);
            this.oDeductions.Controls.Add(this.oDeductionGrid);
            this.oDeductions.Location = new System.Drawing.Point(4, 21);
            this.oDeductions.Name = "oDeductions";
            this.oDeductions.Size = new System.Drawing.Size(822, 307);
            this.oDeductions.TabIndex = 6;
            this.oDeductions.Text = "Deductions";
            // 
            // label122
            // 
            this.label122.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label122.Location = new System.Drawing.Point(647, 285);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(119, 16);
            this.label122.TabIndex = 8;
            this.label122.Text = "Total Value Deduction";
            // 
            // label123
            // 
            this.label123.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label123.Location = new System.Drawing.Point(645, 270);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(121, 15);
            this.label123.TabIndex = 7;
            this.label123.Text = "Total Weight Deduction (kg)";
            // 
            // IDH_DEDVAL
            // 
            this.IDH_DEDVAL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_DEDVAL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DEDVAL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DEDVAL.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DEDVAL.BlockPaint = false;
            this.IDH_DEDVAL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DEDVAL.CornerRadius = 2;
            this.IDH_DEDVAL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DEDVAL.EnabledColor = System.Drawing.Color.White;
            this.IDH_DEDVAL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DEDVAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DEDVAL.Location = new System.Drawing.Point(766, 285);
            this.IDH_DEDVAL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DEDVAL.Multiline = true;
            this.IDH_DEDVAL.Name = "IDH_DEDVAL";
            this.IDH_DEDVAL.PasswordChar = '\0';
            this.IDH_DEDVAL.ReadOnly = true;
            this.IDH_DEDVAL.Size = new System.Drawing.Size(50, 14);
            this.IDH_DEDVAL.TabIndex = 6;
            this.IDH_DEDVAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DEDVAL.TextValue = "";
            this.IDH_DEDVAL.WordWrap = true;
            // 
            // IDH_DEDWEI
            // 
            this.IDH_DEDWEI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_DEDWEI.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_DEDWEI.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_DEDWEI.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DEDWEI.BlockPaint = false;
            this.IDH_DEDWEI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DEDWEI.CornerRadius = 2;
            this.IDH_DEDWEI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DEDWEI.EnabledColor = System.Drawing.Color.White;
            this.IDH_DEDWEI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DEDWEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DEDWEI.Location = new System.Drawing.Point(766, 270);
            this.IDH_DEDWEI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DEDWEI.Multiline = true;
            this.IDH_DEDWEI.Name = "IDH_DEDWEI";
            this.IDH_DEDWEI.PasswordChar = '\0';
            this.IDH_DEDWEI.ReadOnly = true;
            this.IDH_DEDWEI.Size = new System.Drawing.Size(50, 14);
            this.IDH_DEDWEI.TabIndex = 5;
            this.IDH_DEDWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DEDWEI.TextValue = "";
            this.IDH_DEDWEI.WordWrap = true;
            // 
            // oDeductionGrid
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Beige;
            this.oDeductionGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.oDeductionGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.oDeductionGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.oDeductionGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.oDeductionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oDeductionGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oDeductionGrid.GrayReadOnly = false;
            control3.CurrentMaxRows = 2000;
            control3.DBObject = null;
            control3.DoAsCol = true;
            control3.DoFilter = true;
            control3.SoftLimit = -1;
            this.oDeductionGrid.GridControl = control3;
            this.oDeductionGrid.Location = new System.Drawing.Point(0, 0);
            this.oDeductionGrid.Name = "oDeductionGrid";
            this.oDeductionGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.oDeductionGrid.Size = new System.Drawing.Size(821, 260);
            this.oDeductionGrid.TabIndex = 0;
            // 
            // btnSummary
            // 
            this.btnSummary.BlockPaint = false;
            this.btnSummary.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            this.btnSummary.Caption = "Summary";
            this.btnSummary.CornerRadius = 2;
            this.btnSummary.DefaultButton = false;
            this.btnSummary.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btnSummary.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btnSummary.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btnSummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSummary.Image = null;
            this.btnSummary.Location = new System.Drawing.Point(749, 552);
            this.btnSummary.Name = "btnSummary";
            this.btnSummary.Pressed = false;
            this.btnSummary.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btnSummary.Size = new System.Drawing.Size(50, 19);
            this.btnSummary.TabIndex = 820;
            this.btnSummary.Click += new System.EventHandler(this.btnSummary_Click);
            // 
            // oPNavigation
            // 
            this.oPNavigation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.oPNavigation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.oPNavigation.CausesValidation = false;
            this.oPNavigation.Controls.Add(this.btFind);
            this.oPNavigation.Controls.Add(this.btAdd);
            this.oPNavigation.Controls.Add(this.btLast);
            this.oPNavigation.Controls.Add(this.btNext);
            this.oPNavigation.Controls.Add(this.btPrevious);
            this.oPNavigation.Controls.Add(this.btFirst);
            this.oPNavigation.Location = new System.Drawing.Point(616, 522);
            this.oPNavigation.Name = "oPNavigation";
            this.oPNavigation.Size = new System.Drawing.Size(183, 24);
            this.oPNavigation.TabIndex = 76;
            // 
            // btFind
            // 
            this.btFind.BlockPaint = false;
            this.btFind.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.btFind.Caption = "F";
            this.btFind.CausesValidation = false;
            this.btFind.CornerRadius = 2;
            this.btFind.DefaultButton = false;
            this.btFind.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btFind.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btFind.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btFind.Image = null;
            this.btFind.Location = new System.Drawing.Point(4, 2);
            this.btFind.Margin = new System.Windows.Forms.Padding(0);
            this.btFind.Name = "btFind";
            this.btFind.Pressed = false;
            this.btFind.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btFind.Size = new System.Drawing.Size(28, 18);
            this.btFind.TabIndex = 904;
            this.btFind.Click += new System.EventHandler(this.BtFindClick);
            // 
            // btAdd
            // 
            this.btAdd.BlockPaint = false;
            this.btAdd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.btAdd.Caption = "*";
            this.btAdd.CausesValidation = false;
            this.btAdd.CornerRadius = 2;
            this.btAdd.DefaultButton = false;
            this.btAdd.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btAdd.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btAdd.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd.Image = null;
            this.btAdd.Location = new System.Drawing.Point(33, 2);
            this.btAdd.Margin = new System.Windows.Forms.Padding(0);
            this.btAdd.Name = "btAdd";
            this.btAdd.Pressed = false;
            this.btAdd.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btAdd.Size = new System.Drawing.Size(28, 18);
            this.btAdd.TabIndex = 905;
            this.btAdd.Click += new System.EventHandler(this.BtAddClick);
            // 
            // btLast
            // 
            this.btLast.BlockPaint = false;
            this.btLast.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.btLast.Caption = ">>";
            this.btLast.CausesValidation = false;
            this.btLast.CornerRadius = 2;
            this.btLast.DefaultButton = false;
            this.btLast.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btLast.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btLast.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btLast.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLast.Image = null;
            this.btLast.Location = new System.Drawing.Point(149, 2);
            this.btLast.Margin = new System.Windows.Forms.Padding(0);
            this.btLast.Name = "btLast";
            this.btLast.Pressed = false;
            this.btLast.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btLast.Size = new System.Drawing.Size(28, 18);
            this.btLast.TabIndex = 910;
            this.btLast.Click += new System.EventHandler(this.BtLastClick);
            // 
            // btNext
            // 
            this.btNext.BlockPaint = false;
            this.btNext.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.btNext.Caption = ">";
            this.btNext.CausesValidation = false;
            this.btNext.CornerRadius = 2;
            this.btNext.DefaultButton = false;
            this.btNext.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btNext.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btNext.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btNext.Image = null;
            this.btNext.Location = new System.Drawing.Point(120, 2);
            this.btNext.Margin = new System.Windows.Forms.Padding(0);
            this.btNext.Name = "btNext";
            this.btNext.Pressed = false;
            this.btNext.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btNext.Size = new System.Drawing.Size(28, 18);
            this.btNext.TabIndex = 908;
            this.btNext.Click += new System.EventHandler(this.BtNextClick);
            // 
            // btPrevious
            // 
            this.btPrevious.BlockPaint = false;
            this.btPrevious.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.btPrevious.Caption = "<";
            this.btPrevious.CausesValidation = false;
            this.btPrevious.CornerRadius = 2;
            this.btPrevious.DefaultButton = false;
            this.btPrevious.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btPrevious.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btPrevious.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPrevious.Image = null;
            this.btPrevious.Location = new System.Drawing.Point(91, 2);
            this.btPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.btPrevious.Name = "btPrevious";
            this.btPrevious.Pressed = false;
            this.btPrevious.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btPrevious.Size = new System.Drawing.Size(28, 18);
            this.btPrevious.TabIndex = 907;
            this.btPrevious.Click += new System.EventHandler(this.BtPreviousClick);
            // 
            // btFirst
            // 
            this.btFirst.BlockPaint = false;
            this.btFirst.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.btFirst.Caption = "<<";
            this.btFirst.CausesValidation = false;
            this.btFirst.CornerRadius = 2;
            this.btFirst.DefaultButton = false;
            this.btFirst.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btFirst.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btFirst.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btFirst.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btFirst.Image = null;
            this.btFirst.Location = new System.Drawing.Point(62, 2);
            this.btFirst.Margin = new System.Windows.Forms.Padding(0);
            this.btFirst.Name = "btFirst";
            this.btFirst.Pressed = false;
            this.btFirst.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btFirst.Size = new System.Drawing.Size(28, 18);
            this.btFirst.TabIndex = 906;
            this.btFirst.Click += new System.EventHandler(this.BtFirstClick);
            // 
            // IDH_SPECIN
            // 
            this.IDH_SPECIN.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SPECIN.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SPECIN.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SPECIN.BlockPaint = false;
            this.IDH_SPECIN.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SPECIN.CornerRadius = 2;
            this.IDH_SPECIN.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SPECIN.EnabledColor = System.Drawing.Color.White;
            this.IDH_SPECIN.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SPECIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SPECIN.Location = new System.Drawing.Point(110, 125);
            this.IDH_SPECIN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SPECIN.Multiline = true;
            this.IDH_SPECIN.Name = "IDH_SPECIN";
            this.IDH_SPECIN.PasswordChar = '\0';
            this.IDH_SPECIN.ReadOnly = false;
            this.IDH_SPECIN.Size = new System.Drawing.Size(431, 55);
            this.IDH_SPECIN.TabIndex = 169;
            this.IDH_SPECIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SPECIN.TextValue = "";
            this.IDH_SPECIN.WordWrap = true;
            // 
            // IDH_CUST
            // 
            this.IDH_CUST.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUST.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUST.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUST.BlockPaint = false;
            this.IDH_CUST.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUST.CornerRadius = 2;
            this.IDH_CUST.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUST.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUST.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUST.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUST.Location = new System.Drawing.Point(386, 5);
            this.IDH_CUST.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUST.Multiline = true;
            this.IDH_CUST.Name = "IDH_CUST";
            this.IDH_CUST.PasswordChar = '\0';
            this.IDH_CUST.ReadOnly = false;
            this.IDH_CUST.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUST.TabIndex = 2;
            this.IDH_CUST.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUST.TextValue = "";
            this.IDH_CUST.WordWrap = true;
            // 
            // IDH_CUSTNM
            // 
            this.IDH_CUSTNM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSTNM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSTNM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSTNM.BlockPaint = false;
            this.IDH_CUSTNM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSTNM.CornerRadius = 2;
            this.IDH_CUSTNM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSTNM.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSTNM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSTNM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSTNM.Location = new System.Drawing.Point(386, 20);
            this.IDH_CUSTNM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSTNM.Multiline = true;
            this.IDH_CUSTNM.Name = "IDH_CUSTNM";
            this.IDH_CUSTNM.PasswordChar = '\0';
            this.IDH_CUSTNM.ReadOnly = false;
            this.IDH_CUSTNM.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSTNM.TabIndex = 180;
            this.IDH_CUSTNM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSTNM.TextValue = "";
            this.IDH_CUSTNM.WordWrap = true;
            // 
            // IDH_CUSCRF
            // 
            this.IDH_CUSCRF.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CUSCRF.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CUSCRF.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSCRF.BlockPaint = false;
            this.IDH_CUSCRF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSCRF.CornerRadius = 2;
            this.IDH_CUSCRF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSCRF.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSCRF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSCRF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSCRF.Location = new System.Drawing.Point(386, 35);
            this.IDH_CUSCRF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSCRF.Multiline = true;
            this.IDH_CUSCRF.Name = "IDH_CUSCRF";
            this.IDH_CUSCRF.PasswordChar = '\0';
            this.IDH_CUSCRF.ReadOnly = false;
            this.IDH_CUSCRF.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSCRF.TabIndex = 181;
            this.IDH_CUSCRF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSCRF.TextValue = "";
            this.IDH_CUSCRF.WordWrap = true;
            this.IDH_CUSCRF.Validated += new System.EventHandler(this.IDH_WRROW_Click);
            // 
            // IDH_ONCS
            // 
            this.IDH_ONCS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ONCS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ONCS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ONCS.BlockPaint = false;
            this.IDH_ONCS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ONCS.CornerRadius = 2;
            this.IDH_ONCS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ONCS.EnabledColor = System.Drawing.Color.White;
            this.IDH_ONCS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ONCS.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ONCS.Location = new System.Drawing.Point(386, 50);
            this.IDH_ONCS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ONCS.Multiline = true;
            this.IDH_ONCS.Name = "IDH_ONCS";
            this.IDH_ONCS.PasswordChar = '\0';
            this.IDH_ONCS.ReadOnly = false;
            this.IDH_ONCS.Size = new System.Drawing.Size(155, 14);
            this.IDH_ONCS.TabIndex = 182;
            this.IDH_ONCS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ONCS.TextValue = "";
            this.IDH_ONCS.WordWrap = true;
            // 
            // IDH_USER
            // 
            this.IDH_USER.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_USER.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_USER.BackColor = System.Drawing.Color.Transparent;
            this.IDH_USER.BlockPaint = false;
            this.IDH_USER.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_USER.CornerRadius = 2;
            this.IDH_USER.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_USER.EnabledColor = System.Drawing.Color.White;
            this.IDH_USER.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_USER.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_USER.Location = new System.Drawing.Point(386, 95);
            this.IDH_USER.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_USER.Multiline = true;
            this.IDH_USER.Name = "IDH_USER";
            this.IDH_USER.PasswordChar = '\0';
            this.IDH_USER.ReadOnly = false;
            this.IDH_USER.Size = new System.Drawing.Size(155, 14);
            this.IDH_USER.TabIndex = 186;
            this.IDH_USER.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_USER.TextValue = "";
            this.IDH_USER.WordWrap = true;
            // 
            // IDH_CARNAM
            // 
            this.IDH_CARNAM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CARNAM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CARNAM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CARNAM.BlockPaint = false;
            this.IDH_CARNAM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CARNAM.CornerRadius = 2;
            this.IDH_CARNAM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CARNAM.EnabledColor = System.Drawing.Color.White;
            this.IDH_CARNAM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CARNAM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CARNAM.Location = new System.Drawing.Point(386, 80);
            this.IDH_CARNAM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CARNAM.Multiline = true;
            this.IDH_CARNAM.Name = "IDH_CARNAM";
            this.IDH_CARNAM.PasswordChar = '\0';
            this.IDH_CARNAM.ReadOnly = false;
            this.IDH_CARNAM.Size = new System.Drawing.Size(155, 14);
            this.IDH_CARNAM.TabIndex = 186;
            this.IDH_CARNAM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CARNAM.TextValue = "";
            this.IDH_CARNAM.WordWrap = true;
            // 
            // IDH_CARRIE
            // 
            this.IDH_CARRIE.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CARRIE.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CARRIE.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CARRIE.BlockPaint = false;
            this.IDH_CARRIE.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CARRIE.CornerRadius = 2;
            this.IDH_CARRIE.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CARRIE.EnabledColor = System.Drawing.Color.White;
            this.IDH_CARRIE.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CARRIE.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CARRIE.Location = new System.Drawing.Point(386, 65);
            this.IDH_CARRIE.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CARRIE.Multiline = true;
            this.IDH_CARRIE.Name = "IDH_CARRIE";
            this.IDH_CARRIE.PasswordChar = '\0';
            this.IDH_CARRIE.ReadOnly = false;
            this.IDH_CARRIE.Size = new System.Drawing.Size(155, 14);
            this.IDH_CARRIE.TabIndex = 3;
            this.IDH_CARRIE.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CARRIE.TextValue = "";
            this.IDH_CARRIE.WordWrap = true;
            // 
            // IDH_SITREF
            // 
            this.IDH_SITREF.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SITREF.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SITREF.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITREF.BlockPaint = false;
            this.IDH_SITREF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITREF.CornerRadius = 2;
            this.IDH_SITREF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITREF.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITREF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITREF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SITREF.Location = new System.Drawing.Point(675, 125);
            this.IDH_SITREF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITREF.Multiline = true;
            this.IDH_SITREF.Name = "IDH_SITREF";
            this.IDH_SITREF.PasswordChar = '\0';
            this.IDH_SITREF.ReadOnly = false;
            this.IDH_SITREF.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITREF.TabIndex = 12;
            this.IDH_SITREF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITREF.TextValue = "";
            this.IDH_SITREF.WordWrap = true;
            // 
            // IDH_HAZCN
            // 
            this.IDH_HAZCN.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_HAZCN.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_HAZCN.BackColor = System.Drawing.Color.Transparent;
            this.IDH_HAZCN.BlockPaint = false;
            this.IDH_HAZCN.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_HAZCN.CornerRadius = 2;
            this.IDH_HAZCN.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_HAZCN.Enabled = false;
            this.IDH_HAZCN.EnabledColor = System.Drawing.Color.White;
            this.IDH_HAZCN.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_HAZCN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_HAZCN.Location = new System.Drawing.Point(675, 110);
            this.IDH_HAZCN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_HAZCN.Multiline = true;
            this.IDH_HAZCN.Name = "IDH_HAZCN";
            this.IDH_HAZCN.PasswordChar = '\0';
            this.IDH_HAZCN.ReadOnly = false;
            this.IDH_HAZCN.Size = new System.Drawing.Size(107, 14);
            this.IDH_HAZCN.TabIndex = 11;
            this.IDH_HAZCN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_HAZCN.TextValue = "";
            this.IDH_HAZCN.WordWrap = true;
            // 
            // IDH_WASTTN
            // 
            this.IDH_WASTTN.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WASTTN.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WASTTN.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WASTTN.BlockPaint = false;
            this.IDH_WASTTN.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WASTTN.CornerRadius = 2;
            this.IDH_WASTTN.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WASTTN.EnabledColor = System.Drawing.Color.White;
            this.IDH_WASTTN.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WASTTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WASTTN.Location = new System.Drawing.Point(675, 95);
            this.IDH_WASTTN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WASTTN.Multiline = true;
            this.IDH_WASTTN.Name = "IDH_WASTTN";
            this.IDH_WASTTN.PasswordChar = '\0';
            this.IDH_WASTTN.ReadOnly = false;
            this.IDH_WASTTN.Size = new System.Drawing.Size(155, 14);
            this.IDH_WASTTN.TabIndex = 10;
            this.IDH_WASTTN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WASTTN.TextValue = "";
            this.IDH_WASTTN.WordWrap = true;
            // 
            // IDH_BOOKDT
            // 
            this.IDH_BOOKDT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_BOOKDT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_BOOKDT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_BOOKDT.BlockPaint = false;
            this.IDH_BOOKDT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BOOKDT.CornerRadius = 2;
            this.IDH_BOOKDT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BOOKDT.EnabledColor = System.Drawing.Color.White;
            this.IDH_BOOKDT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BOOKDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_BOOKDT.Location = new System.Drawing.Point(675, 35);
            this.IDH_BOOKDT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_BOOKDT.Multiline = true;
            this.IDH_BOOKDT.Name = "IDH_BOOKDT";
            this.IDH_BOOKDT.PasswordChar = '\0';
            this.IDH_BOOKDT.ReadOnly = false;
            this.IDH_BOOKDT.Size = new System.Drawing.Size(70, 14);
            this.IDH_BOOKDT.TabIndex = 16;
            this.IDH_BOOKDT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BOOKDT.TextValue = "";
            this.IDH_BOOKDT.WordWrap = true;
            // 
            // IDH_ROW
            // 
            this.IDH_ROW.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_ROW.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_ROW.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ROW.BlockPaint = false;
            this.IDH_ROW.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ROW.CornerRadius = 2;
            this.IDH_ROW.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ROW.Enabled = false;
            this.IDH_ROW.EnabledColor = System.Drawing.Color.White;
            this.IDH_ROW.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ROW.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_ROW.Location = new System.Drawing.Point(675, 20);
            this.IDH_ROW.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ROW.Multiline = true;
            this.IDH_ROW.Name = "IDH_ROW";
            this.IDH_ROW.PasswordChar = '\0';
            this.IDH_ROW.ReadOnly = false;
            this.IDH_ROW.Size = new System.Drawing.Size(155, 14);
            this.IDH_ROW.TabIndex = 189;
            this.IDH_ROW.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ROW.TextValue = "";
            this.IDH_ROW.WordWrap = true;
            // 
            // IDH_BOOREF
            // 
            this.IDH_BOOREF.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_BOOREF.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_BOOREF.BackColor = System.Drawing.Color.Transparent;
            this.IDH_BOOREF.BlockPaint = false;
            this.IDH_BOOREF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BOOREF.CornerRadius = 2;
            this.IDH_BOOREF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BOOREF.Enabled = false;
            this.IDH_BOOREF.EnabledColor = System.Drawing.Color.White;
            this.IDH_BOOREF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BOOREF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_BOOREF.Location = new System.Drawing.Point(675, 5);
            this.IDH_BOOREF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_BOOREF.Multiline = true;
            this.IDH_BOOREF.Name = "IDH_BOOREF";
            this.IDH_BOOREF.PasswordChar = '\0';
            this.IDH_BOOREF.ReadOnly = false;
            this.IDH_BOOREF.Size = new System.Drawing.Size(155, 14);
            this.IDH_BOOREF.TabIndex = 188;
            this.IDH_BOOREF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BOOREF.TextValue = "";
            this.IDH_BOOREF.WordWrap = false;
            this.IDH_BOOREF.Validated += new System.EventHandler(this.IDH_WRROW_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 15);
            this.label2.TabIndex = 27;
            this.label2.Text = "Vehicle Reg. No.";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(5, 35);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 15);
            this.label3.TabIndex = 28;
            this.label3.Text = "Linked Waste Order";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(156, 35);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 15);
            this.label4.TabIndex = 29;
            this.label4.Text = "Row";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(5, 50);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 15);
            this.label5.TabIndex = 30;
            this.label5.Text = "Vehicle";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(5, 65);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 15);
            this.label6.TabIndex = 31;
            this.label6.Text = "Driver";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(5, 80);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 15);
            this.label7.TabIndex = 32;
            this.label7.Text = "2nd Tare Id";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(4, 95);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 15);
            this.label8.TabIndex = 33;
            this.label8.Text = "2nd Tare Name";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(5, 125);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 15);
            this.label9.TabIndex = 34;
            this.label9.Text = "Delivery Instruction";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(296, 5);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 15);
            this.label10.TabIndex = 35;
            this.label10.Text = "Customer";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(296, 20);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 15);
            this.label11.TabIndex = 36;
            this.label11.Text = "Name";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(296, 35);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 15);
            this.label12.TabIndex = 37;
            this.label12.Text = "Customer Ref. No.";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(296, 50);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 15);
            this.label13.TabIndex = 38;
            this.label13.Text = "Compl. Scheme";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(296, 65);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 15);
            this.label14.TabIndex = 39;
            this.label14.Text = "Waste Carrier";
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(296, 80);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 15);
            this.label15.TabIndex = 40;
            this.label15.Text = "Name";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(296, 95);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 15);
            this.label16.TabIndex = 41;
            this.label16.Text = "User";
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(296, 110);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 15);
            this.label17.TabIndex = 42;
            this.label17.Text = "Branch";
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(563, 5);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(91, 15);
            this.label18.TabIndex = 43;
            this.label18.Text = "WB Ticket No.";
            this.label18.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.label19_HelpRequested);
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(563, 20);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(91, 15);
            this.label19.TabIndex = 44;
            this.label19.Text = "Row No.";
            this.label19.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.label19_HelpRequested);
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(563, 35);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 15);
            this.label20.TabIndex = 45;
            this.label20.Text = "Booking Date";
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(563, 50);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(91, 15);
            this.label21.TabIndex = 46;
            this.label21.Text = "Status";
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(563, 80);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(91, 15);
            this.label22.TabIndex = 47;
            this.label22.Text = "Tip Zone";
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(564, 95);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(91, 15);
            this.label23.TabIndex = 48;
            this.label23.Text = "Waste Trnsf. No.";
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(563, 110);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(91, 15);
            this.label24.TabIndex = 49;
            this.label24.Text = "Consign. Note No.";
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(563, 125);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(91, 15);
            this.label25.TabIndex = 50;
            this.label25.Text = "Ste Ref. No.";
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(563, 140);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(91, 15);
            this.label26.TabIndex = 51;
            this.label26.Text = "Extr Weighbridge No.";
            // 
            // label27
            // 
            this.label27.Location = new System.Drawing.Point(563, 155);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(101, 15);
            this.label27.TabIndex = 52;
            this.label27.Text = "Container Number";
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(563, 170);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(110, 15);
            this.label28.TabIndex = 53;
            this.label28.Text = "Seal Number/Pentane";
            // 
            // IDH_EXTWEI
            // 
            this.IDH_EXTWEI.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_EXTWEI.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_EXTWEI.BackColor = System.Drawing.Color.Transparent;
            this.IDH_EXTWEI.BlockPaint = false;
            this.IDH_EXTWEI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_EXTWEI.CornerRadius = 2;
            this.IDH_EXTWEI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_EXTWEI.EnabledColor = System.Drawing.Color.White;
            this.IDH_EXTWEI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_EXTWEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_EXTWEI.Location = new System.Drawing.Point(675, 140);
            this.IDH_EXTWEI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_EXTWEI.Multiline = true;
            this.IDH_EXTWEI.Name = "IDH_EXTWEI";
            this.IDH_EXTWEI.PasswordChar = '\0';
            this.IDH_EXTWEI.ReadOnly = false;
            this.IDH_EXTWEI.Size = new System.Drawing.Size(155, 14);
            this.IDH_EXTWEI.TabIndex = 13;
            this.IDH_EXTWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_EXTWEI.TextValue = "";
            this.IDH_EXTWEI.WordWrap = true;
            // 
            // IDH_CONTNR
            // 
            this.IDH_CONTNR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_CONTNR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_CONTNR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CONTNR.BlockPaint = false;
            this.IDH_CONTNR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CONTNR.CornerRadius = 2;
            this.IDH_CONTNR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CONTNR.EnabledColor = System.Drawing.Color.White;
            this.IDH_CONTNR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CONTNR.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CONTNR.Location = new System.Drawing.Point(675, 155);
            this.IDH_CONTNR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CONTNR.Multiline = true;
            this.IDH_CONTNR.Name = "IDH_CONTNR";
            this.IDH_CONTNR.PasswordChar = '\0';
            this.IDH_CONTNR.ReadOnly = false;
            this.IDH_CONTNR.Size = new System.Drawing.Size(155, 14);
            this.IDH_CONTNR.TabIndex = 14;
            this.IDH_CONTNR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CONTNR.TextValue = "";
            this.IDH_CONTNR.WordWrap = true;
            // 
            // IDH_SEALNR
            // 
            this.IDH_SEALNR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_SEALNR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_SEALNR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SEALNR.BlockPaint = false;
            this.IDH_SEALNR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SEALNR.CornerRadius = 2;
            this.IDH_SEALNR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SEALNR.EnabledColor = System.Drawing.Color.White;
            this.IDH_SEALNR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SEALNR.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_SEALNR.Location = new System.Drawing.Point(675, 170);
            this.IDH_SEALNR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SEALNR.Multiline = true;
            this.IDH_SEALNR.Name = "IDH_SEALNR";
            this.IDH_SEALNR.PasswordChar = '\0';
            this.IDH_SEALNR.ReadOnly = false;
            this.IDH_SEALNR.Size = new System.Drawing.Size(155, 14);
            this.IDH_SEALNR.TabIndex = 15;
            this.IDH_SEALNR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SEALNR.TextValue = "";
            this.IDH_SEALNR.WordWrap = true;
            // 
            // label29
            // 
            this.label29.Location = new System.Drawing.Point(563, 185);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(91, 15);
            this.label29.TabIndex = 36;
            this.label29.Text = "Obligated";
            // 
            // IDH_BOOKTM
            // 
            this.IDH_BOOKTM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_BOOKTM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_BOOKTM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_BOOKTM.BlockPaint = false;
            this.IDH_BOOKTM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BOOKTM.CornerRadius = 2;
            this.IDH_BOOKTM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BOOKTM.EnabledColor = System.Drawing.Color.White;
            this.IDH_BOOKTM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BOOKTM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_BOOKTM.Location = new System.Drawing.Point(775, 35);
            this.IDH_BOOKTM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_BOOKTM.Multiline = true;
            this.IDH_BOOKTM.Name = "IDH_BOOKTM";
            this.IDH_BOOKTM.PasswordChar = '\0';
            this.IDH_BOOKTM.ReadOnly = false;
            this.IDH_BOOKTM.Size = new System.Drawing.Size(55, 14);
            this.IDH_BOOKTM.TabIndex = 190;
            this.IDH_BOOKTM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BOOKTM.TextValue = "";
            this.IDH_BOOKTM.WordWrap = true;
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(745, 35);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(30, 15);
            this.label30.TabIndex = 59;
            this.label30.Text = "Time";
            // 
            // IDH_REGL
            // 
            this.IDH_REGL.BlockPaint = false;
            this.IDH_REGL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_REGL.Caption = "";
            this.IDH_REGL.CornerRadius = 2;
            this.IDH_REGL.DefaultButton = false;
            this.IDH_REGL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_REGL.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_REGL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_REGL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_REGL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_REGL.Image")));
            this.IDH_REGL.Location = new System.Drawing.Point(265, 20);
            this.IDH_REGL.Name = "IDH_REGL";
            this.IDH_REGL.Pressed = false;
            this.IDH_REGL.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_REGL.Size = new System.Drawing.Size(14, 14);
            this.IDH_REGL.TabIndex = 161;
            this.IDH_REGL.Click += new System.EventHandler(this.IDH_REGLButton1Click);
            // 
            // IDH_WOCF
            // 
            this.IDH_WOCF.BlockPaint = false;
            this.IDH_WOCF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WOCF.Caption = "";
            this.IDH_WOCF.CornerRadius = 2;
            this.IDH_WOCF.DefaultButton = false;
            this.IDH_WOCF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WOCF.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_WOCF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WOCF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WOCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_WOCF.Image")));
            this.IDH_WOCF.Location = new System.Drawing.Point(265, 35);
            this.IDH_WOCF.Name = "IDH_WOCF";
            this.IDH_WOCF.Pressed = false;
            this.IDH_WOCF.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_WOCF.Size = new System.Drawing.Size(14, 14);
            this.IDH_WOCF.TabIndex = 164;
            this.IDH_WOCF.Click += new System.EventHandler(this.LinkWOClick);
            // 
            // IDH_CUSL
            // 
            this.IDH_CUSL.BlockPaint = false;
            this.IDH_CUSL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSL.Caption = "";
            this.IDH_CUSL.CornerRadius = 2;
            this.IDH_CUSL.DefaultButton = false;
            this.IDH_CUSL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSL.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_CUSL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CUSL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_CUSL.Image")));
            this.IDH_CUSL.Location = new System.Drawing.Point(541, 6);
            this.IDH_CUSL.Name = "IDH_CUSL";
            this.IDH_CUSL.Pressed = false;
            this.IDH_CUSL.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_CUSL.Size = new System.Drawing.Size(14, 14);
            this.IDH_CUSL.TabIndex = 179;
            this.IDH_CUSL.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_CSCF
            // 
            this.IDH_CSCF.BlockPaint = false;
            this.IDH_CSCF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CSCF.Caption = "";
            this.IDH_CSCF.CornerRadius = 2;
            this.IDH_CSCF.DefaultButton = false;
            this.IDH_CSCF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CSCF.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_CSCF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CSCF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CSCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_CSCF.Image")));
            this.IDH_CSCF.Location = new System.Drawing.Point(541, 50);
            this.IDH_CSCF.Name = "IDH_CSCF";
            this.IDH_CSCF.Pressed = false;
            this.IDH_CSCF.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_CSCF.Size = new System.Drawing.Size(14, 14);
            this.IDH_CSCF.TabIndex = 184;
            this.IDH_CSCF.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_CARL
            // 
            this.IDH_CARL.BlockPaint = false;
            this.IDH_CARL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CARL.Caption = "";
            this.IDH_CARL.CornerRadius = 2;
            this.IDH_CARL.DefaultButton = false;
            this.IDH_CARL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CARL.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_CARL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CARL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CARL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_CARL.Image")));
            this.IDH_CARL.Location = new System.Drawing.Point(541, 65);
            this.IDH_CARL.Name = "IDH_CARL";
            this.IDH_CARL.Pressed = false;
            this.IDH_CARL.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_CARL.Size = new System.Drawing.Size(14, 14);
            this.IDH_CARL.TabIndex = 185;
            this.IDH_CARL.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // btn1
            // 
            this.btn1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn1.BlockPaint = false;
            this.btn1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.btn1.Caption = "Find";
            this.btn1.CornerRadius = 2;
            this.btn1.DefaultButton = false;
            this.btn1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btn1.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btn1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Image = null;
            this.btn1.Location = new System.Drawing.Point(6, 524);
            this.btn1.Margin = new System.Windows.Forms.Padding(1);
            this.btn1.Name = "btn1";
            this.btn1.Pressed = false;
            this.btn1.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btn1.Size = new System.Drawing.Size(75, 19);
            this.btn1.TabIndex = 1000;
            this.btn1.Click += new System.EventHandler(this.Btn1Click);
            // 
            // IDH_BRANCH
            // 
            this.IDH_BRANCH.BackColor = System.Drawing.Color.Transparent;
            this.IDH_BRANCH.BlockPaint = false;
            this.IDH_BRANCH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BRANCH.CornerRadius = 2;
            this.IDH_BRANCH.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BRANCH.DisplayDescription = true;
            this.IDH_BRANCH.DropDownHight = 100;
            this.IDH_BRANCH.DroppedDown = false;
            this.IDH_BRANCH.Enabled = false;
            this.IDH_BRANCH.EnabledColor = System.Drawing.Color.White;
            this.IDH_BRANCH.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BRANCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_BRANCH.HideDropDown = false;
            this.IDH_BRANCH.Location = new System.Drawing.Point(386, 110);
            this.IDH_BRANCH.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_BRANCH.Multiline = true;
            this.IDH_BRANCH.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_BRANCH.Name = "IDH_BRANCH";
            this.IDH_BRANCH.SelectedIndex = -1;
            this.IDH_BRANCH.SelectedItem = null;
            this.IDH_BRANCH.Size = new System.Drawing.Size(155, 14);
            this.IDH_BRANCH.TabIndex = 187;
            this.IDH_BRANCH.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BRANCH.TextBoxReadOnly = true;
            this.IDH_BRANCH.TextValue = "";
            // 
            // IDH_TZONE
            // 
            this.IDH_TZONE.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TZONE.BlockPaint = false;
            this.IDH_TZONE.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TZONE.CornerRadius = 2;
            this.IDH_TZONE.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TZONE.DisplayDescription = true;
            this.IDH_TZONE.DropDownHight = 100;
            this.IDH_TZONE.DroppedDown = false;
            this.IDH_TZONE.EnabledColor = System.Drawing.Color.White;
            this.IDH_TZONE.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TZONE.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_TZONE.HideDropDown = false;
            this.IDH_TZONE.Location = new System.Drawing.Point(675, 80);
            this.IDH_TZONE.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TZONE.Multiline = true;
            this.IDH_TZONE.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_TZONE.Name = "IDH_TZONE";
            this.IDH_TZONE.SelectedIndex = -1;
            this.IDH_TZONE.SelectedItem = null;
            this.IDH_TZONE.Size = new System.Drawing.Size(155, 14);
            this.IDH_TZONE.TabIndex = 192;
            this.IDH_TZONE.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TZONE.TextBoxReadOnly = true;
            this.IDH_TZONE.TextValue = "";
            // 
            // IDH_OBLED
            // 
            this.IDH_OBLED.BackColor = System.Drawing.Color.Transparent;
            this.IDH_OBLED.BlockPaint = false;
            this.IDH_OBLED.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_OBLED.CornerRadius = 2;
            this.IDH_OBLED.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_OBLED.DisplayDescription = false;
            this.IDH_OBLED.DropDownHight = 100;
            this.IDH_OBLED.DroppedDown = false;
            this.IDH_OBLED.Enabled = false;
            this.IDH_OBLED.EnabledColor = System.Drawing.Color.White;
            this.IDH_OBLED.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_OBLED.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_OBLED.HideDropDown = false;
            this.IDH_OBLED.Location = new System.Drawing.Point(675, 185);
            this.IDH_OBLED.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_OBLED.Multiline = true;
            this.IDH_OBLED.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_OBLED.Name = "IDH_OBLED";
            this.IDH_OBLED.SelectedIndex = -1;
            this.IDH_OBLED.SelectedItem = null;
            this.IDH_OBLED.Size = new System.Drawing.Size(155, 14);
            this.IDH_OBLED.TabIndex = 28;
            this.IDH_OBLED.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_OBLED.TextBoxReadOnly = true;
            this.IDH_OBLED.TextValue = "";
            // 
            // IDH_AVDOC
            // 
            this.IDH_AVDOC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IDH_AVDOC.Location = new System.Drawing.Point(193, 529);
            this.IDH_AVDOC.Name = "IDH_AVDOC";
            this.IDH_AVDOC.Size = new System.Drawing.Size(90, 14);
            this.IDH_AVDOC.TabIndex = 901;
            this.IDH_AVDOC.Text = "Auto Print DOC";
            this.IDH_AVDOC.UseVisualStyleBackColor = true;
            // 
            // IDH_AVCONV
            // 
            this.IDH_AVCONV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IDH_AVCONV.Location = new System.Drawing.Point(89, 529);
            this.IDH_AVCONV.Name = "IDH_AVCONV";
            this.IDH_AVCONV.Size = new System.Drawing.Size(100, 14);
            this.IDH_AVCONV.TabIndex = 900;
            this.IDH_AVCONV.Text = "Auto Print WB Tkt";
            this.IDH_AVCONV.UseVisualStyleBackColor = true;
            // 
            // IDH_CON
            // 
            this.IDH_CON.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_CON.BlockPaint = false;
            this.IDH_CON.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CON.Caption = "Conv Note";
            this.IDH_CON.CornerRadius = 2;
            this.IDH_CON.DefaultButton = false;
            this.IDH_CON.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CON.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_CON.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CON.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_CON.Image = null;
            this.IDH_CON.Location = new System.Drawing.Point(497, 524);
            this.IDH_CON.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_CON.Name = "IDH_CON";
            this.IDH_CON.Pressed = false;
            this.IDH_CON.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_CON.Size = new System.Drawing.Size(55, 19);
            this.IDH_CON.TabIndex = 902;
            this.IDH_CON.Click += new System.EventHandler(this.IDH_CON_Click);
            // 
            // IDH_DOC
            // 
            this.IDH_DOC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_DOC.BlockPaint = false;
            this.IDH_DOC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DOC.Caption = "DOC";
            this.IDH_DOC.CornerRadius = 2;
            this.IDH_DOC.DefaultButton = false;
            this.IDH_DOC.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DOC.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_DOC.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DOC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_DOC.Image = null;
            this.IDH_DOC.Location = new System.Drawing.Point(554, 524);
            this.IDH_DOC.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_DOC.Name = "IDH_DOC";
            this.IDH_DOC.Pressed = false;
            this.IDH_DOC.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_DOC.Size = new System.Drawing.Size(55, 19);
            this.IDH_DOC.TabIndex = 903;
            this.IDH_DOC.Click += new System.EventHandler(this.IDH_DOC_Click);
            // 
            // IDH_OS
            // 
            this.IDH_OS.BlockPaint = false;
            this.IDH_OS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_OS.Caption = "";
            this.IDH_OS.CornerRadius = 2;
            this.IDH_OS.DefaultButton = false;
            this.IDH_OS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_OS.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_OS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_OS.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_OS.Image = ((System.Drawing.Image)(resources.GetObject("IDH_OS.Image")));
            this.IDH_OS.Location = new System.Drawing.Point(279, 20);
            this.IDH_OS.Margin = new System.Windows.Forms.Padding(0);
            this.IDH_OS.Name = "IDH_OS";
            this.IDH_OS.Pressed = false;
            this.IDH_OS.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_OS.Size = new System.Drawing.Size(14, 14);
            this.IDH_OS.TabIndex = 160;
            this.IDH_OS.Click += new System.EventHandler(this.IDH_OSClick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prg1,
            this.LblVersion,
            this.ItemInfo,
            this.stl01,
            this.lblLastError});
            this.statusStrip1.Location = new System.Drawing.Point(0, 548);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(838, 24);
            this.statusStrip1.TabIndex = 105;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.StatusStrip1ItemClicked);
            // 
            // prg1
            // 
            this.prg1.Name = "prg1";
            this.prg1.Size = new System.Drawing.Size(100, 18);
            // 
            // LblVersion
            // 
            this.LblVersion.Name = "LblVersion";
            this.LblVersion.Size = new System.Drawing.Size(106, 19);
            this.LblVersion.Text = " Version db: 577 - 0.0001";
            // 
            // ItemInfo
            // 
            this.ItemInfo.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.ItemInfo.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.ItemInfo.Name = "ItemInfo";
            this.ItemInfo.Size = new System.Drawing.Size(44, 19);
            this.ItemInfo.Text = "ItemInfo";
            // 
            // stl01
            // 
            this.stl01.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.stl01.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.stl01.Name = "stl01";
            this.stl01.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.stl01.Size = new System.Drawing.Size(48, 19);
            this.stl01.Text = "Welcome";
            this.stl01.Click += new System.EventHandler(this.Stl01Click);
            // 
            // lblLastError
            // 
            this.lblLastError.BackColor = System.Drawing.Color.Linen;
            this.lblLastError.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblLastError.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter;
            this.lblLastError.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lblLastError.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(64, 19);
            this.lblLastError.Text = "Get LastError";
            this.lblLastError.Visible = false;
            // 
            // btnHelp
            // 
            this.btnHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHelp.BlockPaint = false;
            this.btnHelp.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.btnHelp.Caption = "?";
            this.btnHelp.CornerRadius = 2;
            this.btnHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHelp.DefaultButton = false;
            this.btnHelp.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btnHelp.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btnHelp.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btnHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHelp.Image = null;
            this.btnHelp.Location = new System.Drawing.Point(803, 524);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Pressed = false;
            this.btnHelp.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btnHelp.Size = new System.Drawing.Size(27, 19);
            this.btnHelp.TabIndex = 1001;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // IDH_CONGEN
            // 
            this.IDH_CONGEN.BlockPaint = false;
            this.IDH_CONGEN.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CONGEN.Caption = "GEN";
            this.IDH_CONGEN.CornerRadius = 2;
            this.IDH_CONGEN.DefaultButton = false;
            this.IDH_CONGEN.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CONGEN.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_CONGEN.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CONGEN.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F);
            this.IDH_CONGEN.Image = null;
            this.IDH_CONGEN.Location = new System.Drawing.Point(784, 110);
            this.IDH_CONGEN.Margin = new System.Windows.Forms.Padding(0);
            this.IDH_CONGEN.Name = "IDH_CONGEN";
            this.IDH_CONGEN.Pressed = false;
            this.IDH_CONGEN.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_CONGEN.Size = new System.Drawing.Size(46, 14);
            this.IDH_CONGEN.TabIndex = 1003;
            this.IDH_CONGEN.Click += new System.EventHandler(this.IDH_CONGEN_Click);
            // 
            // IDH_BOOKIN
            // 
            this.IDH_BOOKIN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IDH_BOOKIN.AutoSize = true;
            this.IDH_BOOKIN.Location = new System.Drawing.Point(289, 529);
            this.IDH_BOOKIN.Name = "IDH_BOOKIN";
            this.IDH_BOOKIN.Size = new System.Drawing.Size(110, 16);
            this.IDH_BOOKIN.TabIndex = 1004;
            this.IDH_BOOKIN.Text = "Do Stock Movement";
            this.IDH_BOOKIN.UseVisualStyleBackColor = true;
            this.IDH_BOOKIN.CheckedChanged += new System.EventHandler(this.IDH_BOOKIN_CheckedChanged);
            // 
            // IDH_BOKSTA
            // 
            this.IDH_BOKSTA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IDH_BOKSTA.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_BOKSTA.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_BOKSTA.BackColor = System.Drawing.Color.Transparent;
            this.IDH_BOKSTA.BlockPaint = false;
            this.IDH_BOKSTA.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BOKSTA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_BOKSTA.CornerRadius = 2;
            this.IDH_BOKSTA.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BOKSTA.Enabled = false;
            this.IDH_BOKSTA.EnabledColor = System.Drawing.Color.White;
            this.IDH_BOKSTA.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BOKSTA.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_BOKSTA.Location = new System.Drawing.Point(406, 528);
            this.IDH_BOKSTA.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_BOKSTA.Multiline = true;
            this.IDH_BOKSTA.Name = "IDH_BOKSTA";
            this.IDH_BOKSTA.PasswordChar = '\0';
            this.IDH_BOKSTA.ReadOnly = false;
            this.IDH_BOKSTA.Size = new System.Drawing.Size(14, 14);
            this.IDH_BOKSTA.TabIndex = 1005;
            this.IDH_BOKSTA.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BOKSTA.TextValue = "";
            this.IDH_BOKSTA.WordWrap = true;
            // 
            // uBC_TRNCd
            // 
            this.uBC_TRNCd.BackColor = System.Drawing.Color.Transparent;
            this.uBC_TRNCd.BlockPaint = false;
            this.uBC_TRNCd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.uBC_TRNCd.CornerRadius = 2;
            this.uBC_TRNCd.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBC_TRNCd.DisplayDescription = true;
            this.uBC_TRNCd.DropDownHight = 100;
            this.uBC_TRNCd.DroppedDown = false;
            this.uBC_TRNCd.EnabledColor = System.Drawing.Color.White;
            this.uBC_TRNCd.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBC_TRNCd.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBC_TRNCd.HideDropDown = false;
            this.uBC_TRNCd.Location = new System.Drawing.Point(675, 65);
            this.uBC_TRNCd.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uBC_TRNCd.Multiline = true;
            this.uBC_TRNCd.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.uBC_TRNCd.Name = "uBC_TRNCd";
            this.uBC_TRNCd.SelectedIndex = -1;
            this.uBC_TRNCd.SelectedItem = null;
            this.uBC_TRNCd.Size = new System.Drawing.Size(155, 14);
            this.uBC_TRNCd.TabIndex = 1006;
            this.uBC_TRNCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.uBC_TRNCd.TextBoxReadOnly = true;
            this.uBC_TRNCd.TextValue = "";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(563, 65);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(77, 12);
            this.label130.TabIndex = 1007;
            this.label130.Text = "Transaction Code";
            // 
            // IDH_Amend
            // 
            this.IDH_Amend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_Amend.BlockPaint = false;
            this.IDH_Amend.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_Amend.Caption = "Amend...";
            this.IDH_Amend.CornerRadius = 2;
            this.IDH_Amend.DefaultButton = false;
            this.IDH_Amend.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_Amend.Enabled = false;
            this.IDH_Amend.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_Amend.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_Amend.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_Amend.Image = null;
            this.IDH_Amend.Location = new System.Drawing.Point(440, 524);
            this.IDH_Amend.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_Amend.Name = "IDH_Amend";
            this.IDH_Amend.Pressed = false;
            this.IDH_Amend.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_Amend.Size = new System.Drawing.Size(55, 19);
            this.IDH_Amend.TabIndex = 1008;
            this.IDH_Amend.Click += new System.EventHandler(this.IDH_Amend_Click);
            // 
            // IDH_STATUS
            // 
            this.IDH_STATUS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_STATUS.BlockPaint = false;
            this.IDH_STATUS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_STATUS.CornerRadius = 2;
            this.IDH_STATUS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_STATUS.DisplayDescription = false;
            this.IDH_STATUS.DropDownHight = 100;
            this.IDH_STATUS.DroppedDown = false;
            this.IDH_STATUS.Enabled = false;
            this.IDH_STATUS.EnabledColor = System.Drawing.Color.White;
            this.IDH_STATUS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_STATUS.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_STATUS.HideDropDown = false;
            this.IDH_STATUS.Location = new System.Drawing.Point(675, 50);
            this.IDH_STATUS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_STATUS.Multiline = true;
            this.IDH_STATUS.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.IDH_STATUS.Name = "IDH_STATUS";
            this.IDH_STATUS.SelectedIndex = -1;
            this.IDH_STATUS.SelectedItem = null;
            this.IDH_STATUS.Size = new System.Drawing.Size(155, 14);
            this.IDH_STATUS.TabIndex = 1009;
            this.IDH_STATUS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_STATUS.TextBoxReadOnly = true;
            this.IDH_STATUS.TextValue = "";
            // 
            // IDH_WRROW2
            // 
            this.IDH_WRROW2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.IDH_WRROW2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.IDH_WRROW2.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WRROW2.BlockPaint = false;
            this.IDH_WRROW2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WRROW2.CornerRadius = 2;
            this.IDH_WRROW2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WRROW2.EnabledColor = System.Drawing.Color.White;
            this.IDH_WRROW2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WRROW2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WRROW2.Location = new System.Drawing.Point(224, 35);
            this.IDH_WRROW2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WRROW2.Multiline = true;
            this.IDH_WRROW2.Name = "IDH_WRROW2";
            this.IDH_WRROW2.PasswordChar = '\0';
            this.IDH_WRROW2.ReadOnly = false;
            this.IDH_WRROW2.Size = new System.Drawing.Size(40, 14);
            this.IDH_WRROW2.TabIndex = 1010;
            this.IDH_WRROW2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WRROW2.TextValue = "";
            this.IDH_WRROW2.WordWrap = true;
            // 
            // IDH_WOCFL
            // 
            this.IDH_WOCFL.BlockPaint = false;
            this.IDH_WOCFL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WOCFL.Caption = "";
            this.IDH_WOCFL.CornerRadius = 2;
            this.IDH_WOCFL.DefaultButton = false;
            this.IDH_WOCFL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WOCFL.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.IDH_WOCFL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WOCFL.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WOCFL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_WOCFL.Image")));
            this.IDH_WOCFL.Location = new System.Drawing.Point(279, 35);
            this.IDH_WOCFL.Margin = new System.Windows.Forms.Padding(0);
            this.IDH_WOCFL.Name = "IDH_WOCFL";
            this.IDH_WOCFL.Pressed = false;
            this.IDH_WOCFL.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.IDH_WOCFL.Size = new System.Drawing.Size(14, 14);
            this.IDH_WOCFL.TabIndex = 1011;
            this.IDH_WOCFL.Click += new System.EventHandler(this.LinkWOLClick);
            this.IDH_WOCFL.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(838, 572);
            this.Controls.Add(this.IDH_WOCFL);
            this.Controls.Add(this.IDH_WRROW2);
            this.Controls.Add(this.IDH_STATUS);
            this.Controls.Add(this.btnSummary);
            this.Controls.Add(this.IDH_Amend);
            this.Controls.Add(this.label130);
            this.Controls.Add(this.uBC_TRNCd);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.IDH_BOKSTA);
            this.Controls.Add(this.IDH_CONGEN);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.IDH_BOOKIN);
            this.Controls.Add(this.IDH_OBLED);
            this.Controls.Add(this.IDH_AVDOC);
            this.Controls.Add(this.IDH_TZONE);
            this.Controls.Add(this.IDH_AVCONV);
            this.Controls.Add(this.IDH_BRANCH);
            this.Controls.Add(this.IDH_CON);
            this.Controls.Add(this.IDH_DOC);
            this.Controls.Add(this.IDH_CARL);
            this.Controls.Add(this.oPNavigation);
            this.Controls.Add(this.IDH_CSCF);
            this.Controls.Add(this.IDH_CUSL);
            this.Controls.Add(this.IDH_WOCF);
            this.Controls.Add(this.IDH_OS);
            this.Controls.Add(this.IDH_REGL);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.IDH_BOOKTM);
            this.Controls.Add(this.IDH_SEALNR);
            this.Controls.Add(this.IDH_CONTNR);
            this.Controls.Add(this.IDH_EXTWEI);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.IDH_SITREF);
            this.Controls.Add(this.IDH_HAZCN);
            this.Controls.Add(this.IDH_WASTTN);
            this.Controls.Add(this.IDH_BOOKDT);
            this.Controls.Add(this.IDH_ROW);
            this.Controls.Add(this.IDH_BOOREF);
            this.Controls.Add(this.IDH_USER);
            this.Controls.Add(this.IDH_CARNAM);
            this.Controls.Add(this.IDH_CARRIE);
            this.Controls.Add(this.IDH_ONCS);
            this.Controls.Add(this.IDH_CUSCRF);
            this.Controls.Add(this.IDH_CUSTNM);
            this.Controls.Add(this.IDH_CUST);
            this.Controls.Add(this.IDH_SPECIN);
            this.Controls.Add(this.oMainTabs);
            this.Controls.Add(this.IDH_TRLNM);
            this.Controls.Add(this.IDH_TRLReg);
            this.Controls.Add(this.IDH_DRIVER);
            this.Controls.Add(this.IDH_VEH);
            this.Controls.Add(this.IDH_WRROW);
            this.Controls.Add(this.IDH_WRORD);
            this.Controls.Add(this.IDH_VEHREG);
            this.Controls.Add(this.IDH_JOBTTP);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Disposal Order";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFormFormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.oMainTabs.ResumeLayout(false);
            this.oTPWeighing.ResumeLayout(false);
            this.oTPWeighing.PerformLayout();
            this.PO_GRP.ResumeLayout(false);
            this.PO_GRP.PerformLayout();
            this.SO_GRP.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.oTPCarrier.ResumeLayout(false);
            this.oTPCustomer.ResumeLayout(false);
            this.oTPProducer.ResumeLayout(false);
            this.oTPSite.ResumeLayout(false);
            this.oDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.oDORows)).EndInit();
            this.oAdditional.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.oAddGrid)).EndInit();
            this.oDeductions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.oDeductionGrid)).EndInit();
            this.oPNavigation.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
		private SBOTextBox IDH_ADDCO;
		private SBOTextBox IDH_ADDCH;
		private System.Windows.Forms.Label label111;
		private System.Windows.Forms.Label label112;
		//private com.idh.win.controls.WR1Grid oAddGrid;
		private idh.wr1.grid.AdditionalExpenses oAddGrid;
		private System.Windows.Forms.TabPage oAdditional;
		private System.Windows.Forms.ToolStripStatusLabel lblLastError;
#region variables
        private SBOTextBox IDH_PROCD;
		private SBOButton IDH_PROCF;
		private SBOTextBox IDH_PRONM;
		private System.Windows.Forms.Label label105;
		private System.Windows.Forms.Label label104;
        private SBOTextBox IDH_PURWGT;
		private System.Windows.Forms.Label label103;
        private SBOTextBox IDH_PURTOT;
		private System.Windows.Forms.Label label106;
		private SBOTextBox IDH_TAXO;
		private System.Windows.Forms.Label label107;
		private SBOTextBox IDH_PSTAT;
		private System.Windows.Forms.Label label108;
		private SBOTextBox IDH_TOTCOS;
		private System.Windows.Forms.Label Lbl1000007;
        private System.Windows.Forms.Label label109;
		private System.Windows.Forms.ToolStripProgressBar prg1;
		private System.Windows.Forms.ToolStripStatusLabel stl01;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.Panel oPNavigation;
		private SBOButton IDH_TMR;
		private SBOButton IDH_OS;
		private System.Windows.Forms.CheckBox IDH_NOVAT;
		private SBOButton IDH_REGL;
		private SBOButton IDH_WOCF;
		private SBOButton IDH_CUSL;
		private SBOButton IDH_CSCF;
		private SBOButton IDH_CARL;
		private SBOButton IDH_ORLK;
		private SBOButton IDH_WASCF;
		private SBOButton IDH_ITCF;
		private SBOButton IDH_DOC;
		private SBOButton IDH_CON;
		private System.Windows.Forms.CheckBox IDH_AVCONV;
		private System.Windows.Forms.CheckBox IDH_AVDOC;
		private SBOButton btAdd;
		private SBOButton IDH_WASAL;
		private SBOTextBox IDH_ADDRES;
		private SBOTextBox IDH_STREET;
		private SBOTextBox IDH_CARREF;
		private System.Windows.Forms.CheckBox IDH_DOORD;
		private System.Windows.Forms.CheckBox IDH_DOARI;
		private System.Windows.Forms.CheckBox IDH_DOARIP;
		private System.Windows.Forms.CheckBox IDH_AINV;
		private System.Windows.Forms.CheckBox IDH_FOC;
		private SBOTextBox IDH_CUSTOT;
		private SBOTextBox IDH_RORIGI;
		private SBOTextBox IDH_WASMAT;
		private SBOTextBox IDH_WASCL1;
		private SBOTextBox IDH_DESC;
		private SBOTextBox IDH_ITMCOD;
		private SBOTextBox IDH_ITMGRC;
		private SBOTextBox IDH_ITMGRN;
		private SBOTextBox IDH_CUSCM;
		private SBOTextBox IDH_RDWGT;
		private SBOTextBox IDH_WDT2;
		private SBOTextBox IDH_SER2;
		private SBOTextBox IDH_WEI2;
		private SBOTextBox IDH_CASHMT;
		private SBOTextBox IDH_RSTAT;
		private SBOTextBox IDH_DISCNT;
        private SBOTextBox IDH_CUSWEI;
		private SBOTextBox IDH_TOTAL;
		private SBOTextBox IDH_TAX;
		private SBOTextBox IDH_SUBTOT;
		private SBOTextBox IDH_ADDCHR;
        private SBOTextBox IDH_DSCPRC;
		private SBOButton IDH_ACCB;
		private SBOButton IDH_ACC1;
		private SBOButton IDH_BUF1;
		private SBOButton IDH_TAR1;
		private SBOButton IDH_ACC2;
		private SBOButton IDH_BUF2;
		private SBOButton IDH_TAR2;
		private System.Windows.Forms.PictureBox pictureBox1;
		private SBOTextBox IDH_WDT1;
		private SBOTextBox IDH_SER1;
		private SBOTextBox IDH_WEI1;
		private SBOTextBox IDH_TRLTar;
		private SBOTextBox IDH_TARWEI;
		private SBOTextBox IDH_WDTB;
		private SBOTextBox IDH_SERB;
		private SBOTextBox IDH_WEIB;
		private SBOTextBox IDH_WEIG;
		private SBOComboBox IDH_WEIBRG;
		private System.Windows.Forms.Label label88;
		private System.Windows.Forms.Label label89;
		private System.Windows.Forms.Label label90;
		private System.Windows.Forms.Label label91;
		private System.Windows.Forms.Label label92;
		private System.Windows.Forms.Label label93;
		private System.Windows.Forms.Label label94;
		private System.Windows.Forms.Label label95;
		private System.Windows.Forms.Label label96;
		private System.Windows.Forms.Label label97;
		private System.Windows.Forms.Label label98;
		private System.Windows.Forms.Label label99;
		private System.Windows.Forms.Label label100;
		private System.Windows.Forms.Label label101;
		private System.Windows.Forms.Label label102;
		private System.Windows.Forms.Label label76;
		private System.Windows.Forms.Label label77;
		private System.Windows.Forms.Label label78;
		private System.Windows.Forms.Label label79;
		private System.Windows.Forms.Label label80;
		private System.Windows.Forms.Label label81;
		private System.Windows.Forms.Label label82;
		private System.Windows.Forms.Label label83;
		private System.Windows.Forms.Label label84;
		private System.Windows.Forms.Label label85;
		private System.Windows.Forms.Label label86;
		private System.Windows.Forms.Label label87;
		private SBOButton IDH_SITAL;
		private SBOButton IDH_SITL;
		private SBOTextBox IDH_SITPHO;
		private SBOTextBox IDH_SITPOS;
		private SBOTextBox IDH_SITCIT;
		private SBOTextBox IDH_SITBLO;
		private SBOTextBox IDH_SITSTR;
		private SBOTextBox IDH_SITADD;
		private SBOTextBox IDH_SITCRF;
		private SBOTextBox IDH_SITCON;
		private SBOTextBox IDH_DISNAM;
		private SBOTextBox IDH_DISSIT;
		private System.Windows.Forms.Label label75;
		private System.Windows.Forms.Label label74;
		private System.Windows.Forms.Label label73;
		private System.Windows.Forms.Label label72;
		private System.Windows.Forms.Label label71;
		private System.Windows.Forms.Label label70;
		private System.Windows.Forms.Label label69;
		private System.Windows.Forms.Label label68;
		private System.Windows.Forms.Label label67;
		private System.Windows.Forms.Label label66;
		private System.Windows.Forms.Label label64;
		private System.Windows.Forms.Label label63;
		private System.Windows.Forms.Label label62;
		private SBOTextBox IDH_PRDCIT;
		private SBOTextBox IDH_PRDPOS;
		private SBOTextBox IDH_PRDPHO;
		private System.Windows.Forms.Label label65;
		private SBOTextBox IDH_ORIGIN;
		private SBOButton IDH_PRDL;
		private SBOButton IDH_ORIGLU;
		private SBOButton IDH_PROAL;
		private SBOTextBox IDH_PRDBLO;
		private SBOTextBox IDH_PRDSTR;
		private SBOTextBox IDH_PRDADD;
		private SBOTextBox IDH_PRDCRF;
		private SBOTextBox IDH_PRDCON;
		private SBOTextBox IDH_WNAM;
		private SBOTextBox IDH_WPRODU;
		private System.Windows.Forms.Label label61;
		private System.Windows.Forms.Label label60;
		private System.Windows.Forms.Label label59;
		private System.Windows.Forms.Label label58;
		private System.Windows.Forms.Label label57;
		private System.Windows.Forms.Label label56;
		private System.Windows.Forms.Label label55;
		private SBOTextBox IDH_CONTNM;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.Label label49;
		private System.Windows.Forms.Label label50;
		private SBOTextBox IDH_BLOCK;
		private System.Windows.Forms.Label label51;
		private SBOTextBox IDH_CITY;
		private System.Windows.Forms.Label label52;
		private SBOTextBox IDH_ZIP;
		private System.Windows.Forms.Label label53;
		private SBOTextBox IDH_PHONE;
		private System.Windows.Forms.Label label54;
		private SBOTextBox IDH_LICREG;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.Label label41;
		private SBOTextBox IDH_ROUTE;
		private SBOTextBox IDH_SEQ;
		private System.Windows.Forms.Label label42;
		private SBOButton IDH_ROUTL;
		private SBOTextBox IDH_SteId;
		private SBOTextBox IDH_PCD;
		private SBOTextBox IDH_SLCNO;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.Label label43;
		private SBOButton IDH_CUSAL;
		private SBOButton IDH_CNA;
		private SBOComboBox IDH_OBLED;
		private SBOTextBox IDH_EXTWEI;
		private SBOTextBox IDH_BOOREF;
		private SBOTextBox IDH_TRLReg;
		private SBOButton btFirst;
		private SBOButton btPrevious;
		private SBOButton btNext;
		private SBOButton btLast;
		private SBOButton btFind;
		private System.Windows.Forms.Label label39;
		private SBOTextBox IDH_SITETL;
		private SBOTextBox IDH_CUSLIC;
		private System.Windows.Forms.Label label40;
		private SBOTextBox IDH_CUSPHO;
		private SBOTextBox IDH_CUSPOS;
		private SBOTextBox IDH_COUNTY;
		private SBOTextBox IDH_CUSCIT;
		private SBOTextBox IDH_CUSBLO;
		private SBOTextBox IDH_CUSSTR;
		private SBOTextBox IDH_CUSADD;
		private SBOTextBox IDH_CUSCON;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.Label label31;
		private SBOTextBox IDH_CUSCRF;
		private SBOButton btn1;
		private SBOComboBox IDH_JOBTTP;
		private SBOTextBox IDH_VEH;
		private SBOTextBox IDH_DRIVER;
		private SBOTextBox IDH_TRLNM;
        private SBOTextBox IDH_SPECIN;
		private SBOTextBox IDH_CUSTNM;
		private SBOTextBox IDH_ONCS;
		private SBOComboBox IDH_BRANCH;
		private SBOTextBox IDH_USER;
		private SBOTextBox IDH_CARNAM;
		private SBOTextBox IDH_CARRIE;
		private SBOTextBox IDH_SITREF;
		private SBOTextBox IDH_HAZCN;
		private SBOTextBox IDH_WASTTN;
        private SBOComboBox IDH_TZONE;
		private SBOTextBox IDH_BOOKDT;
		private SBOTextBox IDH_ROW;
		private SBOTextBox IDH_CONTNR;
		private SBOTextBox IDH_SEALNR;
		private SBOTextBox IDH_BOOKTM;
		private SBOTextBox IDH_WRROW;
		private SBOTextBox IDH_WRORD;
		private SBOTextBox IDH_VEHREG;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TabPage oTPSite;
		private System.Windows.Forms.TabPage oTPProducer;
		private System.Windows.Forms.TabPage oTPCustomer;
		private System.Windows.Forms.TabPage oTPCarrier;
		private System.Windows.Forms.TabPage oTPWeighing;
		private System.Windows.Forms.TabControl oMainTabs;
		private System.Windows.Forms.Label label1;

        private FolderBrowserDialog folderBrowserDialog1;
        private CheckBox IDH_DOPO;
        private ToolStripStatusLabel ItemInfo;
        private SBOButton btnHelp;
        private System.Windows.Forms.ToolStripStatusLabel LblVersion;
        private SBOButton IDH_CONGEN;
        private SBOButton IDH_ORIGLU2;
        private SBOTextBox IDH_ORIGIN2;
        private Label label110;
        private SBOButton IDH_REF;
        private SBOTextBox IDH_ADJWGT;
        private SBOTextBox IDH_WGTDED;
        private Label label114;
        private Label label113;
        private GroupBox groupBox1;
        private SBOTextBox IDH_VALDED;
        private Label label115;
        private SBOTextBox IDH_ORDTOT;
        private Label label119;
        private SBOTextBox IDH_ORDQTY;
        private Label label120;
        private Label label121;
        private SBOTextBox IDH_TIPTOT;
        private Label label116;
        private SBOTextBox IDH_TIPWEI;
        private Label label117;
        private Label label118;
        private SBOComboBox IDH_UOM;
        private SBOComboBox IDH_PUOM;
        private SBOComboBox IDH_PURUOM;
        private CheckBox IDH_ISTRL;
        private CheckBox IDH_BOOKIN;
        private SBOTextBox IDH_BOKSTA;
        private GroupBox SO_GRP;
        private GroupBox PO_GRP;
        private RadioButton IDH_USEAU;
        private RadioButton IDH_USERE;
        private TabPage oDeductions;
        //private DataGridView oDeduction;
        private Label label122;
        private Label label123;
        private SBOTextBox IDH_DEDVAL;
        private SBOTextBox IDH_DEDWEI;
        private idh.wr1.grid.Deductions oDeductionGrid;
        private SBOTextBox IDH_CUST;
#endregion

        private SBOButton IDH_DISPCF;
        private SBOTextBox IDH_DISPNM;
        private SBOTextBox IDH_DISPCD;
        private Label label124;
        private Label label128;
        private SBOTextBox IDH_ADDEX;
        private Label label127;
        private SBOTextBox IDH_ADDCOS;
        private Label label126;
        private Label label125;
        private SBOTextBox IDH_DISPAD;
        private SBOTextBox IDH_EXPLDW;
        private Label label129;
        private SBOTextBox IDH_BPWGT;
        private Label label131;
        private SBOComboBox uBC_TRNCd;
        private Label label130;
        private SBOComboBox IDH_CUSCHG;
        private SBOComboBox IDH_PRCOST;
        private SBOComboBox IDH_ORDCOS;
        private SBOComboBox IDH_TIPCOS;
        private SBOComboBox IDH_ALTITM;
        private Label label134;
        private Label label135;
        private SBOTextBox IDH_TADDCH;
        private SBOTextBox IDH_TADDCO;
        private Label label132;
        private Label label133;
        private SBOTextBox IDH_ADDCHV;
        private SBOTextBox IDH_ADDCOV;
        private SBOTextBox IDH_DCOCUR;
        private SBOTextBox IDH_PCOCUR;
        private SBOTextBox IDH_CCOCUR;
        private SBOTextBox IDH_DCGCUR;
        private SBOButton IDH_Amend;
        private SBOTextBox IDH_TRWTE2;
        private SBOTextBox IDH_TRWTE1;
        private SBOButton btnSummary;
        private SBOButton IDH_NEWWEI;
        private TabPage oDetails;
        private idh.win.controls.WR1Grid oDORows;
        private SBOComboBox IDH_STATUS;
        private SBOTextBox IDH_WRROW2;
        private Label label136;
        private SBOTextBox IDH_FTORIG;
        private SBOButton IDH_WOCFL;
	}
}
