﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using com.idh.controls;
using com.idh.win;
using com.idh.win.controls;

using com.idh.dbObjects.User;
using com.idh.bridge.action;

namespace com.uBC.wr1.form {
    public partial class TaxSummary : AppForm {

        public string HTML {
            set {
                wbBrowser.DocumentText = value;
            }
        }

        public TaxSummary() {
            InitializeComponent();
        }

        private void wbBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e) {

        }

        private void btnClose_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
