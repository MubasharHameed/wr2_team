﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2008/12/16
 * Time: 06:00 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;
using com.idh.bridge;

namespace WR1SA
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args) {

            //Arguments -config config file
            //          exampe -config Config.xml

            if (args != null && args.Length > 0) {
                int iIndex = Array.IndexOf(args, "-config");
                if (iIndex != -1) {
                    try {
                        com.idh.bridge.lookups.Base.CONFIGFILE = args[iIndex + 1];
                    } catch (Exception e) { }
                }
            }

            com.isb.core.controller.Loader.BOAssembly = "ISB_Shared.dll";
            com.isb.core.controller.Loader.BONamespace = "com.idh.dbObjects.User";

            com.isb.core.controller.Loader.FormsAssembly = "ISB_Forms.dll";
            com.isb.core.controller.Loader.FormsNamespace = "WR1_Forms.idh.forms";

            com.isb.core.controller.Loader.SearchFormAssembly = "ISB_Search.dll";
            com.isb.core.controller.Loader.SearchFormNamespace = "WR1_Search.idh.forms.search";

            com.isb.core.controller.Loader.AdminFormAssembly = "ISB_AdminForms.dll";
            com.isb.core.controller.Loader.AdminFormNamespace = "WR1_Admin.idh.forms.admin";

            com.isb.core.controller.Loader.SBOFormAssembly = "ISB_SBOForms.dll";
            com.isb.core.controller.Loader.SBOFormNamespace = "WR1_SBOForms.idh.forms.SBO";

            com.isb.core.controller.Loader.ManagerFormAssembly = "ISB_Managers.dll";
            com.isb.core.controller.Loader.ManagerFormNamespace = "WR1_Managers.idh.forms.manager";

            com.isb.core.controller.Loader.ReportFormAssembly = "ISB_Reports.dll";
            com.isb.core.controller.Loader.ReportFormNamespace = "WR1_Reports.idh.report";

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

            ////string sAppName = "WR";
            ////string sISV = "IDH";
            ////string sDesc = "WR1 Standalone";
            ////string sBaseVersion = "753";
            ////double dRelCount = 753;
            ////string sLicSupport = "0844 800 9881";
            ////int iDebugLevel = 0;
            ////string sConfigTable = "IDH_WRCONFIG";
            ////string sDCode = "258284";
            ////bool bDoStepBackRCount = false;
            //com.idh.bridge.resources.Messages oMessages = new com.idh.bridge.Messages();

            //com.idh.win.AppForm oMainForm = new com.idh.wr1.form.MainForm(
            //    ReleaseInfo.AppName,
            //    ReleaseInfo.ISV,
            //    "WR1 Standalone",
            //    ReleaseInfo.BaseVersion,
            //    ReleaseInfo.RelCount,
            //    ReleaseInfo.LicSupport,
            //    ReleaseInfo.DebugLevel,
            //    ReleaseInfo.ConfigTable,
            //    ReleaseInfo.DCode,
            //    false,oMessages
            //);
            //if (oMainForm != null && oMainForm.Started) {
            //    oMainForm.doPostInitializeComponent();
            //    Application.Run(oMainForm);
            //    if (DataHandler.INSTANCE != null)
            //        DataHandler.INSTANCE.doClose();
            //    Application.Exit();
            //}

            com.uBC.sa.Main oMainForm= new com.uBC.sa.Main();

            if (oMainForm != null) {
            //    oMainForm.doPostInitializeComponent();
                Application.Run(oMainForm);
                if (DataHandler.INSTANCE != null)
                    DataHandler.INSTANCE.doClose();
                Application.Exit();
            }
        }
	}
}
