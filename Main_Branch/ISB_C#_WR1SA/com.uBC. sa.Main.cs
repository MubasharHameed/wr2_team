﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;

using com.idh.win.forms;
using com.idh.bridge;
using com.idh.wr1;
using com.uBC.toolControl;
using com.idh.bridge.resources;
using com.idh.dbObjects.User;
using com.idh.bridge.lookups;
using com.idh.dbObjects.User.Shared;
using System.Globalization;
using com.idh.utils;
using System.Diagnostics;

namespace com.uBC.sa {
    public partial class Main : SBOForm {
        //private com.idh.win.AppForm moMainForm;
        protected bool mbShowPreSelect;
        protected string msStartupModule;

        protected MdiClient moMDIClient;
        protected com.idh.bridge.resources.Messages moMessage;
        public com.idh.bridge.resources.Messages Message {
            get { return moMessage; }
            set { moMessage = value; }
        }

        public Main() {
            InitializeComponent();
            doReadConfig();
            moPreSelect.TopForm = this;
            msISV = "IDH";
            msAOName = "WR";
        }

        protected override void doReadConfig() {
            base.doReadConfig();

            string sVal;
            sVal = ConfigReader.getParameter("ShowPreSelect");
            if (sVal == null || sVal.Equals("FALSE", StringComparison.OrdinalIgnoreCase))
                mbShowPreSelect = false;
            else
                mbShowPreSelect = true;

            msStartupModule = ConfigReader.getParameter("StartupModule");
        }


        private void mdiBackgroundPaint(object sender, PaintEventArgs e) {
            if (moMDIClient == null) return;

            e.Graphics.Clip = new System.Drawing.Region(moMDIClient.ClientRectangle);

            int iTop = moMDIClient.Height - 80;
            int iLeft = moMDIClient.Width - 200;

            e.Graphics.DrawString(ReleaseInfo.Desc, this.Font, Brushes.LightGray, iLeft, iTop);
            e.Graphics.DrawString("Release Count: " + ReleaseInfo.RelCount, this.Font, Brushes.LightGray, iLeft, iTop + (1 * 15));
            e.Graphics.DrawString("Support Number: " + ReleaseInfo.LicSupport, this.Font, Brushes.LightGray, iLeft, iTop + (2 * 15));
            e.Graphics.DrawString("Debug Level: " + ReleaseInfo.DebugLevel, this.Font, Brushes.LightGray, iLeft, iTop + (3 * 15));
        }

        private void doShowInfo() {
            //MDILabel1.Text = ReleaseInfo.Desc;
            //MDILabel2.Text = "Release Count: " + ReleaseInfo.RelCount;
            //MDILabel3.Text = "Support Number: " + ReleaseInfo.LicSupport;
            //MDILabel4.Text = "Debug Level: " + ReleaseInfo.DebugLevel;
        }

        private bool doShowWelcome() {
            //DataHandler.setAddOnName(msAOName);

            if (gsOther == null || gsOther.Length == 0 || gsOther.IndexOf("DNL") == -1) {
                //Welcome oWelcome = new Welcome("Louis Viljoen");
                Welcome oWelcome = new Welcome(gsUserName);
                //oWelcome.MdiParent = this;
                oWelcome.UseSBO = true;

                oWelcome.DBServer = gsDatabaseServer;
                oWelcome.CompanyDB = gsDatabaseName;
                oWelcome.DatabaseType = gsDatabaseType;
                oWelcome.DatabaseUsername = gsDatabaseUsername;
                oWelcome.DatabasePassword = gsDatabasePassword;
                oWelcome.LicenseServer = gsLicenseServer;
                oWelcome.UseTrusted = gbUseTrusted;

                DialogResult oResult = oWelcome.ShowDialog();
                if (oResult != DialogResult.OK) {
                    if (DataHandler.INSTANCE != null)
                        DataHandler.INSTANCE.doClose();
                             mbStarted = false;
                    Close();
                    return false;
                } else {
                    DataHandler.INSTANCE.User = oWelcome.Username;
                    gsUserName = oWelcome.Username;
                    doConnectDataHandler(false);
                }
            } else {
                doConnectDataHandler(false);
                mbStarted = true;
            }
            DataHandler.INSTANCE.setNewReleaseCount(ReleaseInfo.RelCount);
            DataHandler.INSTANCE.setNewVersion(ReleaseInfo.SBOVersion);

            Config.XMLROOTTAG = "WR1SA";
            Config oConfig = new Config("[@IDH_WRCONFIG]");
            
            moMessage = new com.idh.bridge.Messages();

            if (Translation.getInstance().EnableTranslation) {
                doTranslateThisForm();
            }

            //Set the overriding WRConfig values from the config file
            Hashtable hValues = ConfigReader.getTagParameters("/" + Config.XMLROOTTAG + "/WRConfig");
            if (hValues != null && hValues.Count > 0) {
                foreach (string sKey in hValues.Keys) {
                    oConfig.setOverridingParameterValue(sKey, (string)hValues[sKey]);
                }
            }

            IDH_DISPROW.Handler_CheckNumberRequest = new IDH_DISPROW.et_ExternalTriggerWithReturn(com.uBC.forms.PopUps.doRequestCheckNumber);
            doCacheObjects();

            return true;
        }

        private void doCacheObjects() {
            int iLoop = 100;
            Stopwatch oStopWatch;

            double dNormal = 0;
            double dCache = 0;
            Prices oPrice;

            //oStopWatch = Stopwatch.StartNew();
            ////Console.Out.WriteLine("Normal Duration Start: (" + iLoop + ") " + oStopWatch.Elapsed.TotalMilliseconds);
            //for (int i = 0; i < iLoop; i++) {
            //    oPrice = oCIP.doGetJobChargePrices("DO", "Outgoing", "106", "ArticHSC", "ZODIAC", 0, "t", "ECO0007", "Han", "UNIT 1", DateTime.Now, "1", "jhsdu");
            //}
            //oStopWatch.Stop();
            //dNormal = oStopWatch.Elapsed.TotalMilliseconds;

            //Console.Out.WriteLine("Normal Duration End: (" + iLoop + ") " + dNormal);

            IDH_CSITPR oCIP = new IDH_CSITPR();
            oCIP.doCache();
            //if (oCIP.getData(IDH_CSITPR._EnDate + " >= '" + DateTime.Now.AddYears(-1) + '\'', null) > 0) {
            //    oCIP.doCacheData();

                //oStopWatch.Restart();
                ////Console.Out.WriteLine("Cached Duration Start: (" + iLoop + ") " + oStopWatch.Elapsed.TotalMilliseconds);
                //for (int i = 0; i < iLoop; i++) {
                //    oPrice = oCIP.doGetJobChargePrices("DO", "Outgoing", "106", "ArticHSC", "ZODIAC", 0, "t", "ECO0007", "Han", "UNIT 1", DateTime.Now, "1", "jhsdu");
                //}
                //oStopWatch.Stop();
                //dCache = oStopWatch.Elapsed.TotalMilliseconds;
                ////Console.Out.WriteLine("Cached Duration End: (" + iLoop + ") " + dCache);
                
                //string sRes = "Loop(" + iLoop +") Normal(" + dNormal +") Cache(" + dCache + ")";
                //Console.Out.WriteLine(sRes);
            //}

            IDH_SUITPR oSIP = new IDH_SUITPR();
            oSIP.doCache();
            //if (oSIP.getData(IDH_SUITPR._EnDate + " >= '" + DateTime.Now.AddYears(-1) + '\'', null) > 0) {
            //    oSIP.doCacheData();
            //}

           
            //if (oCIP.getData() > 0) {
            //    oCIP.doCacheData();
                //DataTable oTable = oCIP.CacheData;
                //if ( oTable != null ) {
                //    Console.Out.WriteLine("Count: " + oTable.Rows.Count);
                //    Console.Out.WriteLine("Data: " + oTable);

                //    //"UnitDisply = U_UOM  " +
                //    //" And(U_CustCd = 'Han' Or U_CustCd = '') " +
                //    //" And U_StDate <= convert(DATETIME, '2016-11-09', 126) " +
                //    //" And U_EnDate >= convert(DATETIME, '2016-11-09', 126) " +
                //    //" And(U_ItemCd = 'DISPOSAL' Or U_ItemCd = '' Or U_ItemCd Is Null) " +
                //    //" And(U_JobTp = 'Incoming' OR U_JobTp = '' Or U_JobTp Is Null) " +
                //    //" And(U_WasteCd = 'ECO0020' OR U_WasteCd = '' Or U_WasteCd Is Null) " +
                //    //" And(U_StAddr = '' Or U_StAddr Is Null) " +
                //    //" And(U_ZpCd = '' Or U_ZpCd Is Null) " +
                //    //" And(U_BP2CD = 'Han' OR U_BP2CD = '' Or U_BP2CD Is Null) " +
                //    //" And(U_WR1ORD = 'DO' OR U_WR1ORD = '' OR U_WR1ORD IS Null) " +
                //    //" And(U_Branch = '1' OR U_Branch = '' OR U_Branch IS Null) " +
                //    //" And( " +
                //    //" (U_FrmBsWe <= 3500000000 And U_ToBsWe >= 3500000000)  " +
                //    //"   Or((Upper(U_ChrgCal) = 'OFFSET') " +
                //    //"   AND(U_FrmBsWe <= 3500000000 And U_ToBsWe <= 3500000000)) " +
                //    //" ) " +
                //    //" And(U_UOM = 't' OR U_UOM = '' OR U_UOM IS Null)";


                //    string sWhere = 
                //        //"UnitDisply = U_UOM  And " +
                //        " (U_CustCd = 'Han' Or U_CustCd = '') " +
                //        " And U_StDate <= convert(DATETIME, '2016-11-09', 126) " +
                //        " And U_EnDate >= convert(DATETIME, '2016-11-09', 126) " +
                //        " And(U_ItemCd = 'DISPOSAL' Or U_ItemCd = '' Or U_ItemCd Is Null) " +
                //        " And(U_JobTp = 'Incoming' OR U_JobTp = '' Or U_JobTp Is Null) " +
                //        " And(U_WasteCd = 'ECO0020' OR U_WasteCd = '' Or U_WasteCd Is Null) " +
                //        " And(U_StAddr = '' Or U_StAddr Is Null) " +
                //        " And(U_ZpCd = '' Or U_ZpCd Is Null) " +
                //        " And(U_BP2CD = 'Han' OR U_BP2CD = '' Or U_BP2CD Is Null) " +
                //        " And(U_WR1ORD = 'DO' OR U_WR1ORD = '' OR U_WR1ORD IS Null) " +
                //        " And(U_Branch = '1' OR U_Branch = '' OR U_Branch IS Null) " +
                //        " And( " +
                //        " (U_FrmBsWe <= 3500000000 And U_ToBsWe >= 3500000000)  " +
                //        "   Or((Upper(U_ChrgCal) = 'OFFSET') " +
                //        "   AND(U_FrmBsWe <= 3500000000 And U_ToBsWe <= 3500000000)) " +
                //        " ) " +
                //        " And(U_UOM = 't' OR U_UOM = '' OR U_UOM IS Null)";

                //    string sWhere1 = string.Format(" (U_CustCd = 'Han' Or U_CustCd = '') " +
                //        " And U_StDate <= '{0}' " +
                //        " And U_EnDate >= '{1}' " +
                //        " And(U_ItemCd = 'DISPOSAL' Or U_ItemCd = '' Or U_ItemCd Is Null) " +
                //        " And(U_JobTp = 'Incoming' OR U_JobTp = '' Or U_JobTp Is Null) " +
                //        " And(U_WasteCd = 'ECO0020' OR U_WasteCd = '' Or U_WasteCd Is Null) " +
                //        " And(U_StAddr = '' Or U_StAddr Is Null) " +
                //        " And(U_ZpCd = '' Or U_ZpCd Is Null) " +
                //        " And(U_BP2CD = 'Han' OR U_BP2CD = '' Or U_BP2CD Is Null) " +
                //        " And(U_WR1ORD = 'DO' OR U_WR1ORD = '' OR U_WR1ORD IS Null) " +
                //        " And(U_Branch = '1' OR U_Branch = '' OR U_Branch IS Null) " +
                //        " And( " +
                //        " (U_FrmBsWe <= 3500000000 And U_ToBsWe >= 3500000000)  " +
                //        "   Or((U_ChrgCal = 'OFFSET') " +
                //        "   AND(U_FrmBsWe <= 3500000000 And U_ToBsWe <= 3500000000)) " +
                //        " ) " +
                //        " And(U_UOM = 't' OR U_UOM = '' OR U_UOM IS Null)",
                //        DateTime.Now.ToString(DateTimeFormatInfo.InvariantInfo),
                //        DateTime.Now.ToString(DateTimeFormatInfo.InvariantInfo)
                //        );

                //    //sWhere = IDH_CSITPR._CustCd + " = 'Han'";
                //    sWhere = Formatter.doFormatWhereForDataTableSelect(sWhere);
                //    DataRow[] oRows = oTable.Select(sWhere);
                //}
            //}
        }

        private void disposalOrderToolStripMenuItem_Click(object sender, EventArgs e) {
            //com.idh.bridge.resources.Messages oMessages = new com.idh.bridge.Messages();
            doOpenDO();
        }

        private void doOpenDO() {
            try {
                com.idh.win.AppForm oMainForm = new com.uBC.wr1.form.MainForm(
                    ReleaseInfo.AppName,
                    ReleaseInfo.ISV,
                    "WR1 Standalone",
                    ReleaseInfo.BaseVersion,
                    ReleaseInfo.RelCount,
                    ReleaseInfo.LicSupport,
                    ReleaseInfo.DebugLevel,
                    ReleaseInfo.ConfigTable,
                    ReleaseInfo.DCode,
                    false, moMessage
                );

                //oMainForm.Visible = false;

                oMainForm.MdiParent = this;
                oMainForm.Show();
                if (oMainForm != null && oMainForm.Started) {
                    oMainForm.doPostInitializeComponent();
                }
                //oMainForm.Visible = true;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
            }
        }

        private void Main_Load(object sender, EventArgs e) {
            // Loop through all of the form's controls looking
            // for the control of type MdiClient.
            foreach (Control ctl in this.Controls) {
                try {
                    // Attempt to cast the control to type MdiClient.
                    moMDIClient = (MdiClient)ctl;

                    // Set the BackColor of the MdiClient control.
                    moMDIClient.BackColor = this.BackColor;
                    moMDIClient.Paint += this.mdiBackgroundPaint;
                    moMDIClient.Resize += new System.EventHandler(this.Main_Resize);
                    //moMDIClient.Location = new Point(0, 110);
                    //this.SetBounds(ctlMDI.Left, ctlMDI.Top + 100, ctlMDI.Width, ctlMDI.Height - 100);
                } catch (InvalidCastException) {
                    // Catch and ignore the error if casting failed.
                }
            }
        }

        private void Main_Shown(object sender, EventArgs e) {
            if (doShowWelcome()) {
                if (mbShowPreSelect) {
                    moPreSelectMenuItem.Checked = true;
                    if (poQuickTool != null)
                        poQuickTool.Visible = true;

                    moPreSelect.doFillFirstCombos();
                    this.Height += poQuickTool.Height;
                }

                if (msStartupModule.Equals("DO", StringComparison.CurrentCultureIgnoreCase)) {
                    doOpenDO();
                }
            }
        }

        private void moMainToolStrip_MouseDoubleClick(object sender, MouseEventArgs e) {
            //if (moMainToolStrip.Height > 5)
            //    moMainToolStrip.Height = 5;
            //else
            //    moMainToolStrip.Height = 100;
        }

        private void moPreSelectMenuItem_Click(object sender, EventArgs e) {
            if (moPreSelectMenuItem.Checked == true) {
                moPreSelectMenuItem.Checked = false;
                poQuickTool.Visible = false;
            } else {
                moPreSelectMenuItem.Checked = true;
                if ( poQuickTool != null )
                    poQuickTool.Visible = true;

                moPreSelect.doFillFirstCombos();
            }
        }

        private void moPreSelect_CloseButtonClick(object sender, EventArgs e) {
            moPreSelectMenuItem.Checked = false;
            poQuickTool.Visible = false;
        }

        private void Main_Resize(object sender, EventArgs e) {
            moMDIClient.Invalidate();
        }

        private void configValuesToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!DataHandler.INSTANCE.IsConnected) {
                //doWriteLineToLog("Not connected to the Database.");
                return;
            }

            com.idh.wr1.form.InfoForm oForm = new com.idh.wr1.form.InfoForm();
            
            ArrayList oDiff = new ArrayList();
            IDH_WRCONFIG oConfig = new IDH_WRCONFIG();
            string sCat = "";
            if (oConfig.getData(null, IDH_WRCONFIG._Cat) > 0) {
                while (oConfig.next()) {
                    if (sCat != oConfig.U_Cat) {
                        //doWriteLineToLog("\n=====" + oConfig.U_Cat + "========\n");
                        oForm.doWriteLine("\n=====" + oConfig.U_Cat + "========\n", Color.Black);
                        sCat = oConfig.U_Cat;
                    }

                    try {
                        string sValue = Config.Parameter(oConfig.Code, true);
                        string sLine = oConfig.Code + ": " + oConfig.U_Val + " -> (" + sValue + ") [" + oConfig.U_Desc + ']';
                        if (sValue == null) {
                            sLine = "\t! " + sLine;
                            oForm.doWriteLine(sLine, Color.Red);
                        } else if (oConfig.U_Val == sValue ) {
                            sLine = "\t* " + sLine;
                            oForm.doWriteLine(sLine, Color.Green);
                        } else {
                            sLine = "\t> " + sLine;
                            oDiff.Add(sLine);
                            oForm.doWriteLine(sLine, Color.OrangeRed);
                        }
                        //doWriteLineToLog(sLine);
                        
                    } catch (Exception ex) {
                        System.Console.Out.WriteLine("Exception: " + ex.ToString());
                    }
                }

                if (oDiff.Count > 0) {
                    //doWriteLineToLog("\n\n!! CHANGED VALUES - BEGIN !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    oForm.doWriteLine("\n\n!! CHANGED VALUES - BEGIN !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",Color.OrangeRed);
                    foreach (string sLine in oDiff)
                        //doWriteLineToLog(sLine);
                        oForm.doWriteLine(sLine, Color.OrangeRed);
                    //doWriteLineToLog("!! CHANGED VALUES - END !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
                    oForm.doWriteLine("!! CHANGED VALUES - END !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n", Color.OrangeRed);
                }
            }

            oForm.ShowDialog();
        }

        private void moTools_Click(object sender, EventArgs e) {

        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e) {
            if (DataHandler.INSTANCE != null) {
                DataHandler.INSTANCE.doInfo("=============================================================================================");
                DataHandler.INSTANCE.doInfo("==== Thank you for using " + this.ToString());
                DataHandler.INSTANCE.doInfo("=============================================================================================");
            }
        }

        //private void moPreSelect_OkButtonClick(object sender, EventArgs e) {
            //com.idh.win.AppForm oMainForm = new com.uBC.wr1.form.MainForm(
            //    ReleaseInfo.AppName,
            //    ReleaseInfo.ISV,
            //    "WR1 Standalone",
            //    ReleaseInfo.BaseVersion,
            //    ReleaseInfo.RelCount,
            //    ReleaseInfo.LicSupport,
            //    ReleaseInfo.DebugLevel,
            //    ReleaseInfo.ConfigTable,
            //    ReleaseInfo.DCode,
            //    false, moMessage
            //);
            //oMainForm.MdiParent = this;
            //oMainForm.doPostInitializeComponent();
            //oMainForm.Show();

            //if (oMainForm != null && oMainForm.Started) {
            //    oMainForm.doPostInitializeComponent();
            //    if (DataHandler.INSTANCE != null)
            //        DataHandler.INSTANCE.doClose();
            //}

        //}

    }
}
