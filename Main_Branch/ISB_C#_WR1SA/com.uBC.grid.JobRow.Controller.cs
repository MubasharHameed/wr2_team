﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.bridge.data;
using com.idh.controls;
using com.idh.dbObjects.User.Shared;
using com.idh.dbObjects;

namespace com.uBC.grid.JobRow {
    class Controller : DBOGridControl {
        private com.idh.win.controls.WR1Grid moWORGrid;
        //private DBBase moOrderRows;

        public Controller(DBBase oOrderRows) {
            DBObject = oOrderRows;

            doSetAdditionalGridOptions();
        }

        public Controller(DBBase oOrderRows, com.idh.win.controls.WR1Grid oWORGrid) {
            moWORGrid = oWORGrid;
            //moJobRows = oJobRows;
            DBObject = oOrderRows;

            doSetAdditionalGridOptions();
            moWORGrid.doApplyControl();
        }

        private void doSetAdditionalGridOptions() {
            DBOGridControl oGridControl = this;
            if ( moWORGrid != null ) {
                oGridControl = moWORGrid.GridControl;
                ////bool bCanEdit = false;

                oGridControl.DBObject = DBObject;
            }
            oGridControl.DBObject.AutoAddEmptyLine = false;

            //oGridControl.setRequiredFilter("");
            oGridControl.doAddListField(OrderRow._JobNr, "Header", false, -1, ListFields.LISTTYPE_IGNORE, null);
            oGridControl.doAddListField(OrderRow._JobTp, "Job Type", false, -1, ListFields.LISTTYPE_IGNORE, null);
            oGridControl.doAddListField(OrderRow._Code, "Row", false, -1, ListFields.LISTTYPE_IGNORE, null);
            oGridControl.doAddListField(OrderRow._CustCd, "Customer", false, -1, ListFields.LISTTYPE_IGNORE, null);
            oGridControl.doAddListField(OrderRow._WasCd, "Waste Code", false, -1, ListFields.LISTTYPE_IGNORE, null);
            oGridControl.doAddListField(OrderRow._WasDsc, "Waste Description", false, -1, ListFields.LISTTYPE_IGNORE, null);
            oGridControl.doAddListField(OrderRow._Lorry, "Registration", false, -1, ListFields.LISTTYPE_IGNORE, null);
            oGridControl.doAddListField(OrderRow._CarrCd, "Haulier", false, -1, ListFields.LISTTYPE_IGNORE, null);
            oGridControl.doAddListField(OrderRow._RDate, "Requested Date", false, -1, ListFields.LISTTYPE_IGNORE, null);
            oGridControl.doAddListField(OrderRow._TrnCode, "Transaction Code", false, -1, ListFields.LISTTYPE_IGNORE, null);

            oGridControl.doAddListField(OrderRow._Status, "Status", false, -1, ListFields.LISTTYPE_IGNORE, null);
            oGridControl.doAddListField(OrderRow._PStat, "PO Status", false, -1, ListFields.LISTTYPE_IGNORE, null);
            //oGridControl.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single;
        }
    }
}
