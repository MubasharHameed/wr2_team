﻿namespace com.idh.wr1.form {
    partial class DOAmend {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uBCPay = new com.idh.win.controls.SBO.SBOButton();
            this.uBCAll = new com.idh.win.controls.SBO.SBOButton();
            this.btn1 = new com.idh.win.controls.SBO.SBOButton();
            this.btn2 = new com.idh.win.controls.SBO.SBOButton();
            this.uBCDOR = new com.idh.win.controls.SBO.SBOTextBox();
            this.uBCREASN = new com.idh.win.controls.SBO.SBOComboBox();
            this.uBCCQNUMLbl = new System.Windows.Forms.Label();
            this.uBCCQNUM = new com.idh.win.controls.SBO.SBOTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "DO Row";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(117, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Reason";
            // 
            // uBCPay
            // 
            this.uBCPay.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            this.uBCPay.Caption = "Redo Payment";
            this.uBCPay.CornerRadius = 2;
            this.uBCPay.DefaultButton = false;
            this.uBCPay.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBCPay.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.uBCPay.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBCPay.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBCPay.Image = null;
            this.uBCPay.Location = new System.Drawing.Point(17, 32);
            this.uBCPay.Name = "uBCPay";
            this.uBCPay.Pressed = false;
            this.uBCPay.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.uBCPay.Size = new System.Drawing.Size(94, 19);
            this.uBCPay.TabIndex = 2;
            this.uBCPay.Click += new System.EventHandler(this.uBCPay_Click);
            // 
            // uBCAll
            // 
            this.uBCAll.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            this.uBCAll.Caption = "Amend Values";
            this.uBCAll.CornerRadius = 2;
            this.uBCAll.DefaultButton = false;
            this.uBCAll.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBCAll.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.uBCAll.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBCAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBCAll.Image = null;
            this.uBCAll.Location = new System.Drawing.Point(17, 53);
            this.uBCAll.Name = "uBCAll";
            this.uBCAll.Pressed = false;
            this.uBCAll.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.uBCAll.Size = new System.Drawing.Size(94, 19);
            this.uBCAll.TabIndex = 3;
            this.uBCAll.Click += new System.EventHandler(this.uBCAll_Click);
            // 
            // btn1
            // 
            this.btn1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            this.btn1.Caption = "OK";
            this.btn1.CornerRadius = 2;
            this.btn1.DefaultButton = false;
            this.btn1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btn1.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btn1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Image = null;
            this.btn1.Location = new System.Drawing.Point(17, 95);
            this.btn1.Name = "btn1";
            this.btn1.Pressed = false;
            this.btn1.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btn1.Size = new System.Drawing.Size(75, 19);
            this.btn1.TabIndex = 4;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(156)))), ((int)(((byte)(156)))));
            this.btn2.Caption = "Cancel";
            this.btn2.CornerRadius = 2;
            this.btn2.DefaultButton = false;
            this.btn2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btn2.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btn2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Image = null;
            this.btn2.Location = new System.Drawing.Point(95, 95);
            this.btn2.Name = "btn2";
            this.btn2.Pressed = false;
            this.btn2.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btn2.Size = new System.Drawing.Size(75, 19);
            this.btn2.TabIndex = 5;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // uBCDOR
            // 
            this.uBCDOR.BackColor = System.Drawing.Color.Transparent;
            this.uBCDOR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.uBCDOR.CornerRadius = 2;
            this.uBCDOR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBCDOR.Enabled = false;
            this.uBCDOR.EnabledColor = System.Drawing.Color.White;
            this.uBCDOR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBCDOR.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBCDOR.Location = new System.Drawing.Point(66, 7);
            this.uBCDOR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uBCDOR.Multiline = true;
            this.uBCDOR.Name = "uBCDOR";
            this.uBCDOR.PasswordChar = '\0';
            this.uBCDOR.ReadOnly = false;
            this.uBCDOR.Size = new System.Drawing.Size(94, 14);
            this.uBCDOR.TabIndex = 6;
            this.uBCDOR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.uBCDOR.TextValue = "";
            // 
            // uBCREASN
            // 
            this.uBCREASN.BackColor = System.Drawing.Color.Transparent;
            this.uBCREASN.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.uBCREASN.CornerRadius = 2;
            this.uBCREASN.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBCREASN.Enabled = false;
            this.uBCREASN.EnabledColor = System.Drawing.Color.White;
            this.uBCREASN.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBCREASN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBCREASN.Location = new System.Drawing.Point(209, 57);
            this.uBCREASN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uBCREASN.Multiline = true;
            this.uBCREASN.Name = "uBCREASN";
            //this.uBCREASN.PasswordChar = '\0';
            //this.uBCREASN.ReadOnly = false;
            this.uBCREASN.Size = new System.Drawing.Size(288, 14);
            this.uBCREASN.TabIndex = 7;
            this.uBCREASN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.uBCREASN.TextValue = "";
            // 
            // label3
            // 
            this.uBCCQNUMLbl.AutoSize = true;
            this.uBCCQNUMLbl.Location = new System.Drawing.Point(117, 38);
            this.uBCCQNUMLbl.Name = "uBCCQNUMLbl";
            this.uBCCQNUMLbl.Size = new System.Drawing.Size(83, 13);
            this.uBCCQNUMLbl.TabIndex = 8;
            this.uBCCQNUMLbl.Text = "";
            // 
            // uBCCQNUM
            // 
            this.uBCCQNUM.BackColor = System.Drawing.Color.Transparent;
            this.uBCCQNUM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.uBCCQNUM.CornerRadius = 2;
            this.uBCCQNUM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBCCQNUM.EnabledColor = System.Drawing.Color.White;
            this.uBCCQNUM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBCCQNUM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBCCQNUM.Location = new System.Drawing.Point(209, 38);
            this.uBCCQNUM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uBCCQNUM.Multiline = true;
            this.uBCCQNUM.Name = "uBCCQNUM";
            this.uBCCQNUM.PasswordChar = '\0';
            this.uBCCQNUM.ReadOnly = false;
            this.uBCCQNUM.Size = new System.Drawing.Size(66, 14);
            this.uBCCQNUM.TabIndex = 9;
            this.uBCCQNUM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.uBCCQNUM.TextValue = "";
            // 
            // DOAmend
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 125);
            this.Controls.Add(this.uBCCQNUM);
            this.Controls.Add(this.uBCCQNUMLbl);
            this.Controls.Add(this.uBCREASN);
            this.Controls.Add(this.uBCDOR);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.uBCAll);
            this.Controls.Add(this.uBCPay);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DOAmend";
            this.Text = "Amend DO Row";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private win.controls.SBO.SBOButton uBCPay;
        private win.controls.SBO.SBOButton uBCAll;
        private win.controls.SBO.SBOButton btn1;
        private win.controls.SBO.SBOButton btn2;
        private win.controls.SBO.SBOTextBox uBCDOR;
        private win.controls.SBO.SBOComboBox uBCREASN;
        private System.Windows.Forms.Label uBCCQNUMLbl;
        private win.controls.SBO.SBOTextBox uBCCQNUM;
    }
}