﻿namespace com.uBC.toolControl {
    partial class PreSelect{
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.uBCShwPrev = new System.Windows.Forms.CheckBox();
            this.uBCVehRgCF = new System.Windows.Forms.Button();
            this.uBCWasteCF = new System.Windows.Forms.Button();
            this.uBCBPCF = new System.Windows.Forms.Button();
            this.btnClose = new com.idh.win.controls.SBO.SBOButton();
            this.btnOk = new com.idh.win.controls.SBO.SBOButton();
            this.uBCTranCD = new com.idh.win.controls.SBO.SBOComboBox();
            this.uBCBPNM = new com.idh.win.controls.SBO.SBOTextBox();
            this.uBCBPCD = new com.idh.win.controls.SBO.SBOTextBox();
            this.uBCWasteNM = new com.idh.win.controls.SBO.SBOTextBox();
            this.uBCWasteCD = new com.idh.win.controls.SBO.SBOTextBox();
            this.uBCVehReg = new com.idh.win.controls.SBO.SBOTextBox();
            this.uBCWR1O = new com.idh.win.controls.SBO.SBOComboBox();
            this.sboClear = new com.idh.win.controls.SBO.SBOButton();
            this.moWORGrid = new com.uBC.grid.JobRowGrid();
            ((System.ComponentModel.ISupportInitialize)(this.moWORGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(5, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "WR1 Order Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(5, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Vehicle Registration";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(5, 65);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Choose Material";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(5, 36);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Choose BP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(5, 50);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Choose Transaction Code";
            // 
            // uBCShwPrev
            // 
            this.uBCShwPrev.AutoSize = true;
            this.uBCShwPrev.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uBCShwPrev.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.uBCShwPrev.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.uBCShwPrev.Location = new System.Drawing.Point(218, 87);
            this.uBCShwPrev.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.uBCShwPrev.Name = "uBCShwPrev";
            this.uBCShwPrev.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.uBCShwPrev.Size = new System.Drawing.Size(101, 17);
            this.uBCShwPrev.TabIndex = 14;
            this.uBCShwPrev.Text = "Previous Orders";
            this.uBCShwPrev.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.uBCShwPrev.UseVisualStyleBackColor = true;
            this.uBCShwPrev.CheckedChanged += new System.EventHandler(this.uBCShwPrev_CheckedChanged);
            // 
            // uBCVehRgCF
            // 
            this.uBCVehRgCF.Image = global::com.idh.wr1.Properties.Resources.CFL;
            this.uBCVehRgCF.Location = new System.Drawing.Point(373, 20);
            this.uBCVehRgCF.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.uBCVehRgCF.Name = "uBCVehRgCF";
            this.uBCVehRgCF.Size = new System.Drawing.Size(14, 14);
            this.uBCVehRgCF.TabIndex = 15;
            this.uBCVehRgCF.UseVisualStyleBackColor = true;
            this.uBCVehRgCF.Click += new System.EventHandler(this.uBCVehRgCF_Click);
            // 
            // uBCWasteCF
            // 
            this.uBCWasteCF.Image = global::com.idh.wr1.Properties.Resources.CFL;
            this.uBCWasteCF.Location = new System.Drawing.Point(373, 65);
            this.uBCWasteCF.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.uBCWasteCF.Name = "uBCWasteCF";
            this.uBCWasteCF.Size = new System.Drawing.Size(14, 14);
            this.uBCWasteCF.TabIndex = 16;
            this.uBCWasteCF.UseVisualStyleBackColor = true;
            this.uBCWasteCF.Click += new System.EventHandler(this.uBCWasteCF_Click);
            // 
            // uBCBPCF
            // 
            this.uBCBPCF.Image = global::com.idh.wr1.Properties.Resources.CFL;
            this.uBCBPCF.Location = new System.Drawing.Point(373, 36);
            this.uBCBPCF.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.uBCBPCF.Name = "uBCBPCF";
            this.uBCBPCF.Size = new System.Drawing.Size(14, 14);
            this.uBCBPCF.TabIndex = 17;
            this.uBCBPCF.UseVisualStyleBackColor = true;
            this.uBCBPCF.Click += new System.EventHandler(this.uBCBPCF_Click);
            // 
            // btnClose
            // 
            this.btnClose.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.btnClose.Caption = "Close";
            this.btnClose.CornerRadius = 2;
            this.btnClose.DefaultButton = false;
            this.btnClose.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btnClose.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btnClose.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = null;
            this.btnClose.Location = new System.Drawing.Point(64, 84);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Pressed = false;
            this.btnClose.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btnClose.Size = new System.Drawing.Size(50, 19);
            this.btnClose.TabIndex = 13;
            // 
            // btnOk
            // 
            this.btnOk.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.btnOk.Caption = "Ok";
            this.btnOk.CornerRadius = 2;
            this.btnOk.DefaultButton = false;
            this.btnOk.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.btnOk.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.btnOk.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Image = null;
            this.btnOk.Location = new System.Drawing.Point(8, 84);
            this.btnOk.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Pressed = false;
            this.btnOk.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.btnOk.Size = new System.Drawing.Size(50, 19);
            this.btnOk.TabIndex = 12;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // uBCTranCD
            // 
            this.uBCTranCD.BackColor = System.Drawing.Color.Transparent;
            this.uBCTranCD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.uBCTranCD.CornerRadius = 2;
            this.uBCTranCD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBCTranCD.DisplayDescription = true;
            this.uBCTranCD.DropDownHight = 100;
            this.uBCTranCD.DroppedDown = false;
            this.uBCTranCD.EnabledColor = System.Drawing.Color.White;
            this.uBCTranCD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBCTranCD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBCTranCD.HideDropDown = false;
            this.uBCTranCD.Location = new System.Drawing.Point(134, 50);
            this.uBCTranCD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uBCTranCD.Multiline = true;
            this.uBCTranCD.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.uBCTranCD.Name = "uBCTranCD";
            this.uBCTranCD.SelectedIndex = -1;
            this.uBCTranCD.SelectedItem = null;
            this.uBCTranCD.Size = new System.Drawing.Size(235, 14);
            this.uBCTranCD.TabIndex = 11;
            this.uBCTranCD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.uBCTranCD.TextBoxReadOnly = true;
            this.uBCTranCD.TextValue = "";
            this.uBCTranCD.ValueChanged += new com.idh.win.controls.LPVComboBox.ValueChangedEventHandler(this.uBCTranCD_ValueChanged);
            // 
            // uBCBPNM
            // 
            this.uBCBPNM.BackColor = System.Drawing.Color.Transparent;
            this.uBCBPNM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.uBCBPNM.CornerRadius = 2;
            this.uBCBPNM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBCBPNM.EnabledColor = System.Drawing.Color.White;
            this.uBCBPNM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBCBPNM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBCBPNM.Location = new System.Drawing.Point(218, 35);
            this.uBCBPNM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uBCBPNM.Multiline = true;
            this.uBCBPNM.Name = "uBCBPNM";
            this.uBCBPNM.PasswordChar = '\0';
            this.uBCBPNM.ReadOnly = false;
            this.uBCBPNM.Size = new System.Drawing.Size(151, 14);
            this.uBCBPNM.TabIndex = 10;
            this.uBCBPNM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.uBCBPNM.TextValue = "";
            this.uBCBPNM.TextChanged += new System.EventHandler(this.All_TextChanged);
            this.uBCBPNM.Validated += new System.EventHandler(this.uBCBPNM_Validate);
            // 
            // uBCBPCD
            // 
            this.uBCBPCD.BackColor = System.Drawing.Color.Transparent;
            this.uBCBPCD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.uBCBPCD.CornerRadius = 2;
            this.uBCBPCD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBCBPCD.EnabledColor = System.Drawing.Color.White;
            this.uBCBPCD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBCBPCD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBCBPCD.Location = new System.Drawing.Point(134, 35);
            this.uBCBPCD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uBCBPCD.Multiline = true;
            this.uBCBPCD.Name = "uBCBPCD";
            this.uBCBPCD.PasswordChar = '\0';
            this.uBCBPCD.ReadOnly = false;
            this.uBCBPCD.Size = new System.Drawing.Size(80, 14);
            this.uBCBPCD.TabIndex = 9;
            this.uBCBPCD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.uBCBPCD.TextValue = "";
            this.uBCBPCD.TextChanged += new System.EventHandler(this.All_TextChanged);
            this.uBCBPCD.Validated += new System.EventHandler(this.uBCBPCD_Validate);
            // 
            // uBCWasteNM
            // 
            this.uBCWasteNM.BackColor = System.Drawing.Color.Transparent;
            this.uBCWasteNM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.uBCWasteNM.CornerRadius = 2;
            this.uBCWasteNM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBCWasteNM.EnabledColor = System.Drawing.Color.White;
            this.uBCWasteNM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBCWasteNM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBCWasteNM.Location = new System.Drawing.Point(218, 65);
            this.uBCWasteNM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uBCWasteNM.Multiline = true;
            this.uBCWasteNM.Name = "uBCWasteNM";
            this.uBCWasteNM.PasswordChar = '\0';
            this.uBCWasteNM.ReadOnly = false;
            this.uBCWasteNM.Size = new System.Drawing.Size(151, 14);
            this.uBCWasteNM.TabIndex = 8;
            this.uBCWasteNM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.uBCWasteNM.TextValue = "";
            this.uBCWasteNM.TextChanged += new System.EventHandler(this.All_TextChanged);
            this.uBCWasteNM.Validated += new System.EventHandler(this.uBCWasteNM_Validate);
            // 
            // uBCWasteCD
            // 
            this.uBCWasteCD.BackColor = System.Drawing.Color.Transparent;
            this.uBCWasteCD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.uBCWasteCD.CornerRadius = 2;
            this.uBCWasteCD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBCWasteCD.EnabledColor = System.Drawing.Color.White;
            this.uBCWasteCD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBCWasteCD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBCWasteCD.Location = new System.Drawing.Point(134, 65);
            this.uBCWasteCD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uBCWasteCD.Multiline = true;
            this.uBCWasteCD.Name = "uBCWasteCD";
            this.uBCWasteCD.PasswordChar = '\0';
            this.uBCWasteCD.ReadOnly = false;
            this.uBCWasteCD.Size = new System.Drawing.Size(80, 14);
            this.uBCWasteCD.TabIndex = 7;
            this.uBCWasteCD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.uBCWasteCD.TextValue = "";
            this.uBCWasteCD.TextChanged += new System.EventHandler(this.All_TextChanged);
            this.uBCWasteCD.Validated += new System.EventHandler(this.uBCWasteCD_Validate);
            // 
            // uBCVehReg
            // 
            this.uBCVehReg.BackColor = System.Drawing.Color.Transparent;
            this.uBCVehReg.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.uBCVehReg.CornerRadius = 2;
            this.uBCVehReg.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBCVehReg.EnabledColor = System.Drawing.Color.White;
            this.uBCVehReg.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBCVehReg.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBCVehReg.Location = new System.Drawing.Point(134, 20);
            this.uBCVehReg.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uBCVehReg.Multiline = true;
            this.uBCVehReg.Name = "uBCVehReg";
            this.uBCVehReg.PasswordChar = '\0';
            this.uBCVehReg.ReadOnly = false;
            this.uBCVehReg.Size = new System.Drawing.Size(235, 14);
            this.uBCVehReg.TabIndex = 3;
            this.uBCVehReg.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.uBCVehReg.TextValue = "";
            this.uBCVehReg.TextChanged += new System.EventHandler(this.All_TextChanged);
            this.uBCVehReg.Validated += new System.EventHandler(this.uBCVehReg_Validate);
            // 
            // uBCWR1O
            // 
            this.uBCWR1O.BackColor = System.Drawing.Color.Transparent;
            this.uBCWR1O.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.uBCWR1O.CornerRadius = 2;
            this.uBCWR1O.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.uBCWR1O.DisplayDescription = false;
            this.uBCWR1O.DropDownHight = 100;
            this.uBCWR1O.DroppedDown = false;
            this.uBCWR1O.EnabledColor = System.Drawing.Color.White;
            this.uBCWR1O.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.uBCWR1O.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uBCWR1O.HideDropDown = false;
            this.uBCWR1O.Location = new System.Drawing.Point(134, 5);
            this.uBCWR1O.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uBCWR1O.Multiline = true;
            this.uBCWR1O.MultiSelect = System.Windows.Forms.SelectionMode.One;
            this.uBCWR1O.Name = "uBCWR1O";
            this.uBCWR1O.SelectedIndex = -1;
            this.uBCWR1O.SelectedItem = null;
            this.uBCWR1O.Size = new System.Drawing.Size(80, 14);
            this.uBCWR1O.TabIndex = 1;
            this.uBCWR1O.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.uBCWR1O.TextBoxReadOnly = true;
            this.uBCWR1O.TextValue = "";
            // 
            // sboClear
            // 
            this.sboClear.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboClear.Caption = "Clear";
            this.sboClear.CornerRadius = 2;
            this.sboClear.DefaultButton = false;
            this.sboClear.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboClear.EnabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(218)))), ((int)(((byte)(144)))));
            this.sboClear.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sboClear.Image = null;
            this.sboClear.Location = new System.Drawing.Point(134, 84);
            this.sboClear.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.sboClear.Name = "sboClear";
            this.sboClear.Pressed = false;
            this.sboClear.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(200)))), ((int)(((byte)(69)))));
            this.sboClear.Size = new System.Drawing.Size(50, 19);
            this.sboClear.TabIndex = 18;
            this.sboClear.Click += new System.EventHandler(this.sboClear_Click);
            // 
            // moWORGrid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Beige;
            this.moWORGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.moWORGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.moWORGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.moWORGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.moWORGrid.ColumnHeadersHeight = 18;
            this.moWORGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moWORGrid.GrayReadOnly = false;
            this.moWORGrid.Location = new System.Drawing.Point(404, 0);
            this.moWORGrid.Name = "moWORGrid";
            this.moWORGrid.Size = new System.Drawing.Size(525, 110);
            this.moWORGrid.TabIndex = 19;
            this.moWORGrid.Visible = false;
            this.moWORGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.moWORGrid_PreviewGridCellDoubleClick);
            // 
            // PreSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.moWORGrid);
            this.Controls.Add(this.sboClear);
            this.Controls.Add(this.uBCBPCF);
            this.Controls.Add(this.uBCWasteCF);
            this.Controls.Add(this.uBCVehRgCF);
            this.Controls.Add(this.uBCShwPrev);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.uBCTranCD);
            this.Controls.Add(this.uBCBPNM);
            this.Controls.Add(this.uBCBPCD);
            this.Controls.Add(this.uBCWasteNM);
            this.Controls.Add(this.uBCWasteCD);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.uBCVehReg);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.uBCWR1O);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "PreSelect";
            this.Size = new System.Drawing.Size(932, 107);
            this.VisibleChanged += new System.EventHandler(this.PreSelect_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.moWORGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private idh.win.controls.SBO.SBOComboBox uBCWR1O;
        private System.Windows.Forms.Label label2;
        private idh.win.controls.SBO.SBOTextBox uBCVehReg;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private idh.win.controls.SBO.SBOTextBox uBCWasteCD;
        private idh.win.controls.SBO.SBOTextBox uBCWasteNM;
        private idh.win.controls.SBO.SBOTextBox uBCBPCD;
        private idh.win.controls.SBO.SBOTextBox uBCBPNM;
        private idh.win.controls.SBO.SBOComboBox uBCTranCD;
        private idh.win.controls.SBO.SBOButton btnOk;
        private idh.win.controls.SBO.SBOButton btnClose;
        private System.Windows.Forms.CheckBox uBCShwPrev;
        private System.Windows.Forms.Button uBCVehRgCF;
        private System.Windows.Forms.Button uBCWasteCF;
        private System.Windows.Forms.Button uBCBPCF;
        private idh.win.controls.SBO.SBOButton sboClear;
        private com.uBC.grid.JobRowGrid moWORGrid;
    }
}
