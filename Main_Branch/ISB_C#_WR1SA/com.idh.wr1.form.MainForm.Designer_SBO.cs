﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2008/12/16
 * Time: 06:00 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Collections;
using System.Windows.Forms;

using com.idh.bridge;
using com.idh.bridge.utils;
using com.idh.bridge.form;
using com.idh.bridge.lookups;
using com.idh.bridge.reports;
using com.idh.dbObjects.User;
using com.idh.bridge.error;

using com.idh.win.controls.SBO;

namespace com.idh.wr1.form
{
	partial class MainForm
	{
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.IDH_JOBTTP = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_VEHREG = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WRORD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WRROW = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_VEH = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DRIVER = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TRLReg = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TRLNM = new com.idh.win.controls.SBO.SBOTextBox();
            this.oMainTabs = new System.Windows.Forms.TabControl();
            this.oTPWeighing = new System.Windows.Forms.TabPage();
            this.IDH_USEAU = new System.Windows.Forms.RadioButton();
            this.IDH_USERE = new System.Windows.Forms.RadioButton();
            this.PO_GRP = new System.Windows.Forms.GroupBox();
            this.IDH_PUOM = new com.idh.win.controls.SBO.SBOComboBox();
            this.label118 = new System.Windows.Forms.Label();
            this.IDH_PURUOM = new com.idh.win.controls.SBO.SBOComboBox();
            this.label105 = new System.Windows.Forms.Label();
            this.IDH_ORDTOT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.IDH_PURWGT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ORDCOS = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRCOST = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ORDQTY = new com.idh.win.controls.SBO.SBOTextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.IDH_PURTOT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DOPO = new System.Windows.Forms.CheckBox();
            this.label121 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.IDH_TIPTOT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TAXO = new com.idh.win.controls.SBO.SBOTextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.IDH_TIPCOS = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PSTAT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TIPWEI = new com.idh.win.controls.SBO.SBOTextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.IDH_TOTCOS = new com.idh.win.controls.SBO.SBOTextBox();
            this.SO_GRP = new System.Windows.Forms.GroupBox();
            this.IDH_UOM = new com.idh.win.controls.SBO.SBOComboBox();
            this.label90 = new System.Windows.Forms.Label();
            this.IDH_VALDED = new com.idh.win.controls.SBO.SBOTextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.IDH_CUSTOT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DOORD = new System.Windows.Forms.CheckBox();
            this.IDH_DSCPRC = new com.idh.win.controls.SBO.SBOTextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.IDH_ADDEX = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DOARIP = new System.Windows.Forms.CheckBox();
            this.IDH_SUBTOT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DOARI = new System.Windows.Forms.CheckBox();
            this.IDH_TAX = new com.idh.win.controls.SBO.SBOTextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.IDH_NOVAT = new System.Windows.Forms.CheckBox();
            this.IDH_CASHMT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TOTAL = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_FOC = new System.Windows.Forms.CheckBox();
            this.label82 = new System.Windows.Forms.Label();
            this.IDH_RSTAT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.IDH_DISCNT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.IDH_CUSWEI = new com.idh.win.controls.SBO.SBOTextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.IDH_CUSCHG = new com.idh.win.controls.SBO.SBOTextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.IDH_AINV = new System.Windows.Forms.CheckBox();
            this.label87 = new System.Windows.Forms.Label();
            this.IDH_ISTRL = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label109 = new System.Windows.Forms.Label();
            this.IDH_ITMGRN = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ITMGRC = new com.idh.win.controls.SBO.SBOTextBox();
            this.Lbl1000007 = new System.Windows.Forms.Label();
            this.IDH_ITMCOD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DESC = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WASCL1 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WASMAT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_RORIGI = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ITCF = new System.Windows.Forms.Button();
            this.IDH_WASCF = new System.Windows.Forms.Button();
            this.IDH_ORLK = new System.Windows.Forms.Button();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.IDH_PRONM = new com.idh.win.controls.SBO.SBOTextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.IDH_PROCD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PROCF = new System.Windows.Forms.Button();
            this.IDH_ADJWGT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WGTDED = new com.idh.win.controls.SBO.SBOTextBox();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.IDH_TAR2 = new System.Windows.Forms.Button();
            this.IDH_BUF2 = new System.Windows.Forms.Button();
            this.IDH_ACC2 = new System.Windows.Forms.Button();
            this.IDH_TAR1 = new System.Windows.Forms.Button();
            this.IDH_BUF1 = new System.Windows.Forms.Button();
            this.IDH_ACC1 = new System.Windows.Forms.Button();
            this.IDH_ACCB = new System.Windows.Forms.Button();
            this.IDH_TMR = new System.Windows.Forms.Button();
            this.IDH_REF = new System.Windows.Forms.Button();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.IDH_CUSCM = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_RDWGT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WDT2 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SER2 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WEI2 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WDT1 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SER1 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WEI1 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TRLTar = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TARWEI = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WDTB = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SERB = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WEIB = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WEIG = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WEIBRG = new com.idh.win.controls.SBO.SBOComboBox();
            this.oTPCarrier = new System.Windows.Forms.TabPage();
            this.IDH_LICREG = new com.idh.win.controls.SBO.SBOTextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.IDH_PHONE = new com.idh.win.controls.SBO.SBOTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.IDH_ZIP = new com.idh.win.controls.SBO.SBOTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.IDH_CITY = new com.idh.win.controls.SBO.SBOTextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.IDH_BLOCK = new com.idh.win.controls.SBO.SBOTextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.IDH_WASAL = new System.Windows.Forms.Button();
            this.IDH_ADDRES = new com.idh.win.controls.SBO.SBOTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.IDH_STREET = new com.idh.win.controls.SBO.SBOTextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.IDH_CARREF = new com.idh.win.controls.SBO.SBOTextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.IDH_CONTNM = new com.idh.win.controls.SBO.SBOTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.oTPCustomer = new System.Windows.Forms.TabPage();
            this.IDH_ORIGLU2 = new System.Windows.Forms.Button();
            this.IDH_ORIGIN2 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.IDH_SLCNO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PCD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SteId = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ROUTL = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.IDH_SEQ = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ROUTE = new com.idh.win.controls.SBO.SBOTextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.IDH_CNA = new System.Windows.Forms.Button();
            this.IDH_CUSAL = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.IDH_CUSLIC = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITETL = new com.idh.win.controls.SBO.SBOTextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.IDH_CUSPHO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSPOS = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_COUNTY = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSCIT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSBLO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSSTR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSADD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSCON = new com.idh.win.controls.SBO.SBOTextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.oTPProducer = new System.Windows.Forms.TabPage();
            this.IDH_PROAL = new System.Windows.Forms.Button();
            this.IDH_ORIGLU = new System.Windows.Forms.Button();
            this.IDH_PRDL = new System.Windows.Forms.Button();
            this.IDH_ORIGIN = new com.idh.win.controls.SBO.SBOTextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.IDH_PRDPHO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRDPOS = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRDCIT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.IDH_PRDBLO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRDSTR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRDADD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRDCRF = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_PRDCON = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WNAM = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WPRODU = new com.idh.win.controls.SBO.SBOTextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.oTPSite = new System.Windows.Forms.TabPage();
            this.IDH_SITAL = new System.Windows.Forms.Button();
            this.IDH_SITL = new System.Windows.Forms.Button();
            this.IDH_SITPHO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITPOS = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITCIT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.IDH_SITBLO = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITSTR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITADD = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITCRF = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITCON = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DISNAM = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DISSIT = new com.idh.win.controls.SBO.SBOTextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.oAdditional = new System.Windows.Forms.TabPage();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.IDH_ADDCH = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ADDCO = new com.idh.win.controls.SBO.SBOTextBox();
            this.oAddGrid = new com.idh.wr1.grid.AdditionalExpenses();
            this.oDeductions = new System.Windows.Forms.TabPage();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.IDH_DEDVAL = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_DEDWEI = new com.idh.win.controls.SBO.SBOTextBox();
            this.oDeductionGrid = new com.idh.wr1.grid.Deductions();
            this.oPNavigation = new System.Windows.Forms.Panel();
            this.btFind = new System.Windows.Forms.Button();
            this.btAdd = new System.Windows.Forms.Button();
            this.btLast = new System.Windows.Forms.Button();
            this.btNext = new System.Windows.Forms.Button();
            this.btPrevious = new System.Windows.Forms.Button();
            this.btFirst = new System.Windows.Forms.Button();
            this.IDH_SPECIN = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUST = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSTNM = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CUSCRF = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ONCS = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_USER = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CARNAM = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CARRIE = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SITREF = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_HAZCN = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_WASTTN = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_STATUS = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_BOOKDT = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_ROW = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_BOOREF = new com.idh.win.controls.SBO.SBOTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.IDH_EXTWEI = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_CONTNR = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_SEALNR = new com.idh.win.controls.SBO.SBOTextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.IDH_BOOKTM = new com.idh.win.controls.SBO.SBOTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.IDH_REGL = new System.Windows.Forms.Button();
            this.IDH_WOCF = new System.Windows.Forms.Button();
            this.IDH_CUSL = new System.Windows.Forms.Button();
            this.IDH_CSCF = new System.Windows.Forms.Button();
            this.IDH_CARL = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.IDH_BRANCH = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_TZONE = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_OBLED = new com.idh.win.controls.SBO.SBOComboBox();
            this.IDH_AVDOC = new System.Windows.Forms.CheckBox();
            this.IDH_AVCONV = new System.Windows.Forms.CheckBox();
            this.IDH_CON = new System.Windows.Forms.Button();
            this.IDH_DOC = new System.Windows.Forms.Button();
            this.IDH_OS = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.prg1 = new System.Windows.Forms.ToolStripProgressBar();
            this.LblVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.ItemInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.stl01 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblLastError = new System.Windows.Forms.ToolStripStatusLabel();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnHelp = new System.Windows.Forms.Button();
            this.IDH_CONGEN = new System.Windows.Forms.Button();
            this.IDH_BOOKIN = new System.Windows.Forms.CheckBox();
            this.IDH_BOKSTA = new com.idh.win.controls.SBO.SBOTextBox();
            this.oMainTabs.SuspendLayout();
            this.oTPWeighing.SuspendLayout();
            this.PO_GRP.SuspendLayout();
            this.SO_GRP.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.oTPCarrier.SuspendLayout();
            this.oTPCustomer.SuspendLayout();
            this.oTPProducer.SuspendLayout();
            this.oTPSite.SuspendLayout();
            this.oAdditional.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oAddGrid)).BeginInit();
            this.oDeductions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oDeductionGrid)).BeginInit();
            this.oPNavigation.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Disposal Type";
            // 
            // IDH_JOBTTP
            // 
            this.IDH_JOBTTP.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_JOBTTP.CornerRadius = 2;
            this.IDH_JOBTTP.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_JOBTTP.DropDownHight = 100;
            this.IDH_JOBTTP.EnabledColor = System.Drawing.Color.White;
            this.IDH_JOBTTP.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_JOBTTP.Location = new System.Drawing.Point(110, 5);
            this.IDH_JOBTTP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_JOBTTP.Multiline = false;
            this.IDH_JOBTTP.Name = "IDH_JOBTTP";
            this.IDH_JOBTTP.SelectedIndex = -1;
            this.IDH_JOBTTP.SelectedItem = null;
            this.IDH_JOBTTP.Size = new System.Drawing.Size(155, 14);
            this.IDH_JOBTTP.TabIndex = 0;
            this.IDH_JOBTTP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_JOBTTP.TextBoxReadOnly = true;
            this.IDH_JOBTTP.TextValue = "";
            this.IDH_JOBTTP.SelectedIndexChanged += new System.EventHandler(this.All_ComboChanged);
            // 
            // IDH_VEHREG
            // 
            this.IDH_VEHREG.BackColor = System.Drawing.Color.Transparent;
            this.IDH_VEHREG.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_VEHREG.CornerRadius = 2;
            this.IDH_VEHREG.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_VEHREG.EnabledColor = System.Drawing.Color.White;
            this.IDH_VEHREG.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_VEHREG.Location = new System.Drawing.Point(110, 20);
            this.IDH_VEHREG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_VEHREG.Multiline = false;
            this.IDH_VEHREG.Name = "IDH_VEHREG";
            this.IDH_VEHREG.ReadOnly = false;
            this.IDH_VEHREG.Size = new System.Drawing.Size(138, 14);
            this.IDH_VEHREG.TabIndex = 1;
            this.IDH_VEHREG.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_VEHREG.TextValue = "";
            // 
            // IDH_WRORD
            // 
            this.IDH_WRORD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WRORD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WRORD.CornerRadius = 2;
            this.IDH_WRORD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WRORD.EnabledColor = System.Drawing.Color.White;
            this.IDH_WRORD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WRORD.Location = new System.Drawing.Point(110, 35);
            this.IDH_WRORD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WRORD.Multiline = false;
            this.IDH_WRORD.Name = "IDH_WRORD";
            this.IDH_WRORD.ReadOnly = false;
            this.IDH_WRORD.Size = new System.Drawing.Size(55, 14);
            this.IDH_WRORD.TabIndex = 162;
            this.IDH_WRORD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WRORD.TextValue = "";
            // 
            // IDH_WRROW
            // 
            this.IDH_WRROW.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WRROW.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WRROW.CornerRadius = 2;
            this.IDH_WRROW.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WRROW.EnabledColor = System.Drawing.Color.White;
            this.IDH_WRROW.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WRROW.Location = new System.Drawing.Point(210, 35);
            this.IDH_WRROW.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WRROW.Multiline = false;
            this.IDH_WRROW.Name = "IDH_WRROW";
            this.IDH_WRROW.ReadOnly = false;
            this.IDH_WRROW.Size = new System.Drawing.Size(55, 14);
            this.IDH_WRROW.TabIndex = 163;
            this.IDH_WRROW.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WRROW.TextValue = "";
            this.IDH_WRROW.Click += new System.EventHandler(this.IDH_WRROW_Click);
            // 
            // IDH_VEH
            // 
            this.IDH_VEH.BackColor = System.Drawing.Color.Transparent;
            this.IDH_VEH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_VEH.CornerRadius = 2;
            this.IDH_VEH.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_VEH.EnabledColor = System.Drawing.Color.White;
            this.IDH_VEH.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_VEH.Location = new System.Drawing.Point(110, 50);
            this.IDH_VEH.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_VEH.Multiline = false;
            this.IDH_VEH.Name = "IDH_VEH";
            this.IDH_VEH.ReadOnly = false;
            this.IDH_VEH.Size = new System.Drawing.Size(155, 14);
            this.IDH_VEH.TabIndex = 165;
            this.IDH_VEH.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_VEH.TextValue = "";
            // 
            // IDH_DRIVER
            // 
            this.IDH_DRIVER.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DRIVER.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DRIVER.CornerRadius = 2;
            this.IDH_DRIVER.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DRIVER.EnabledColor = System.Drawing.Color.White;
            this.IDH_DRIVER.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DRIVER.Location = new System.Drawing.Point(110, 65);
            this.IDH_DRIVER.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DRIVER.Multiline = false;
            this.IDH_DRIVER.Name = "IDH_DRIVER";
            this.IDH_DRIVER.ReadOnly = false;
            this.IDH_DRIVER.Size = new System.Drawing.Size(155, 14);
            this.IDH_DRIVER.TabIndex = 166;
            this.IDH_DRIVER.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DRIVER.TextValue = "";
            // 
            // IDH_TRLReg
            // 
            this.IDH_TRLReg.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TRLReg.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TRLReg.CornerRadius = 2;
            this.IDH_TRLReg.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TRLReg.EnabledColor = System.Drawing.Color.White;
            this.IDH_TRLReg.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TRLReg.Location = new System.Drawing.Point(110, 80);
            this.IDH_TRLReg.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TRLReg.Multiline = false;
            this.IDH_TRLReg.Name = "IDH_TRLReg";
            this.IDH_TRLReg.ReadOnly = false;
            this.IDH_TRLReg.Size = new System.Drawing.Size(155, 14);
            this.IDH_TRLReg.TabIndex = 167;
            this.IDH_TRLReg.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TRLReg.TextValue = "";
            // 
            // IDH_TRLNM
            // 
            this.IDH_TRLNM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TRLNM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TRLNM.CornerRadius = 2;
            this.IDH_TRLNM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TRLNM.EnabledColor = System.Drawing.Color.White;
            this.IDH_TRLNM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TRLNM.Location = new System.Drawing.Point(110, 95);
            this.IDH_TRLNM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TRLNM.Multiline = false;
            this.IDH_TRLNM.Name = "IDH_TRLNM";
            this.IDH_TRLNM.ReadOnly = false;
            this.IDH_TRLNM.Size = new System.Drawing.Size(155, 14);
            this.IDH_TRLNM.TabIndex = 168;
            this.IDH_TRLNM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TRLNM.TextValue = "";
            // 
            // oMainTabs
            // 
            this.oMainTabs.Controls.Add(this.oTPWeighing);
            this.oMainTabs.Controls.Add(this.oTPCarrier);
            this.oMainTabs.Controls.Add(this.oTPCustomer);
            this.oMainTabs.Controls.Add(this.oTPProducer);
            this.oMainTabs.Controls.Add(this.oTPSite);
            this.oMainTabs.Controls.Add(this.oAdditional);
            this.oMainTabs.Controls.Add(this.oDeductions);
            this.oMainTabs.Location = new System.Drawing.Point(5, 183);
            this.oMainTabs.Name = "oMainTabs";
            this.oMainTabs.SelectedIndex = 0;
            this.oMainTabs.Size = new System.Drawing.Size(830, 315);
            this.oMainTabs.TabIndex = 4;
            // 
            // oTPWeighing
            // 
            this.oTPWeighing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPWeighing.Controls.Add(this.IDH_USEAU);
            this.oTPWeighing.Controls.Add(this.IDH_USERE);
            this.oTPWeighing.Controls.Add(this.PO_GRP);
            this.oTPWeighing.Controls.Add(this.SO_GRP);
            this.oTPWeighing.Controls.Add(this.IDH_ISTRL);
            this.oTPWeighing.Controls.Add(this.groupBox1);
            this.oTPWeighing.Controls.Add(this.IDH_ADJWGT);
            this.oTPWeighing.Controls.Add(this.IDH_WGTDED);
            this.oTPWeighing.Controls.Add(this.label114);
            this.oTPWeighing.Controls.Add(this.label113);
            this.oTPWeighing.Controls.Add(this.pictureBox1);
            this.oTPWeighing.Controls.Add(this.IDH_TAR2);
            this.oTPWeighing.Controls.Add(this.IDH_BUF2);
            this.oTPWeighing.Controls.Add(this.IDH_ACC2);
            this.oTPWeighing.Controls.Add(this.IDH_TAR1);
            this.oTPWeighing.Controls.Add(this.IDH_BUF1);
            this.oTPWeighing.Controls.Add(this.IDH_ACC1);
            this.oTPWeighing.Controls.Add(this.IDH_ACCB);
            this.oTPWeighing.Controls.Add(this.IDH_TMR);
            this.oTPWeighing.Controls.Add(this.IDH_REF);
            this.oTPWeighing.Controls.Add(this.label102);
            this.oTPWeighing.Controls.Add(this.label101);
            this.oTPWeighing.Controls.Add(this.label100);
            this.oTPWeighing.Controls.Add(this.label99);
            this.oTPWeighing.Controls.Add(this.label98);
            this.oTPWeighing.Controls.Add(this.label97);
            this.oTPWeighing.Controls.Add(this.label96);
            this.oTPWeighing.Controls.Add(this.label95);
            this.oTPWeighing.Controls.Add(this.label94);
            this.oTPWeighing.Controls.Add(this.IDH_CUSCM);
            this.oTPWeighing.Controls.Add(this.IDH_RDWGT);
            this.oTPWeighing.Controls.Add(this.IDH_WDT2);
            this.oTPWeighing.Controls.Add(this.IDH_SER2);
            this.oTPWeighing.Controls.Add(this.IDH_WEI2);
            this.oTPWeighing.Controls.Add(this.IDH_WDT1);
            this.oTPWeighing.Controls.Add(this.IDH_SER1);
            this.oTPWeighing.Controls.Add(this.IDH_WEI1);
            this.oTPWeighing.Controls.Add(this.IDH_TRLTar);
            this.oTPWeighing.Controls.Add(this.IDH_TARWEI);
            this.oTPWeighing.Controls.Add(this.IDH_WDTB);
            this.oTPWeighing.Controls.Add(this.IDH_SERB);
            this.oTPWeighing.Controls.Add(this.IDH_WEIB);
            this.oTPWeighing.Controls.Add(this.IDH_WEIG);
            this.oTPWeighing.Controls.Add(this.IDH_WEIBRG);
            this.oTPWeighing.Location = new System.Drawing.Point(4, 21);
            this.oTPWeighing.Name = "oTPWeighing";
            this.oTPWeighing.Padding = new System.Windows.Forms.Padding(3);
            this.oTPWeighing.Size = new System.Drawing.Size(822, 290);
            this.oTPWeighing.TabIndex = 0;
            this.oTPWeighing.Text = "Weighing";
            // 
            // IDH_USEAU
            // 
            this.IDH_USEAU.AutoSize = true;
            this.IDH_USEAU.Location = new System.Drawing.Point(159, 231);
            this.IDH_USEAU.Name = "IDH_USEAU";
            this.IDH_USEAU.Size = new System.Drawing.Size(40, 16);
            this.IDH_USEAU.TabIndex = 1008;
            this.IDH_USEAU.Text = "Use";
            this.IDH_USEAU.UseVisualStyleBackColor = true;
            this.IDH_USEAU.CheckedChanged += new System.EventHandler(this.IDH_USEAU_CheckedChanged);
            // 
            // IDH_USERE
            // 
            this.IDH_USERE.AutoSize = true;
            this.IDH_USERE.Checked = true;
            this.IDH_USERE.Location = new System.Drawing.Point(159, 215);
            this.IDH_USERE.Name = "IDH_USERE";
            this.IDH_USERE.Size = new System.Drawing.Size(40, 16);
            this.IDH_USERE.TabIndex = 1007;
            this.IDH_USERE.TabStop = true;
            this.IDH_USERE.Text = "Use";
            this.IDH_USERE.UseVisualStyleBackColor = true;
            this.IDH_USERE.CheckedChanged += new System.EventHandler(this.IDH_USERE_CheckedChanged);
            // 
            // PO_GRP
            // 
            this.PO_GRP.Controls.Add(this.IDH_PUOM);
            this.PO_GRP.Controls.Add(this.label118);
            this.PO_GRP.Controls.Add(this.IDH_PURUOM);
            this.PO_GRP.Controls.Add(this.label105);
            this.PO_GRP.Controls.Add(this.IDH_ORDTOT);
            this.PO_GRP.Controls.Add(this.label104);
            this.PO_GRP.Controls.Add(this.label119);
            this.PO_GRP.Controls.Add(this.IDH_PURWGT);
            this.PO_GRP.Controls.Add(this.IDH_ORDCOS);
            this.PO_GRP.Controls.Add(this.IDH_PRCOST);
            this.PO_GRP.Controls.Add(this.IDH_ORDQTY);
            this.PO_GRP.Controls.Add(this.label103);
            this.PO_GRP.Controls.Add(this.label120);
            this.PO_GRP.Controls.Add(this.IDH_PURTOT);
            this.PO_GRP.Controls.Add(this.IDH_DOPO);
            this.PO_GRP.Controls.Add(this.label121);
            this.PO_GRP.Controls.Add(this.label106);
            this.PO_GRP.Controls.Add(this.IDH_TIPTOT);
            this.PO_GRP.Controls.Add(this.IDH_TAXO);
            this.PO_GRP.Controls.Add(this.label116);
            this.PO_GRP.Controls.Add(this.label107);
            this.PO_GRP.Controls.Add(this.IDH_TIPCOS);
            this.PO_GRP.Controls.Add(this.IDH_PSTAT);
            this.PO_GRP.Controls.Add(this.IDH_TIPWEI);
            this.PO_GRP.Controls.Add(this.label108);
            this.PO_GRP.Controls.Add(this.label117);
            this.PO_GRP.Controls.Add(this.IDH_TOTCOS);
            this.PO_GRP.Enabled = false;
            this.PO_GRP.Location = new System.Drawing.Point(383, 66);
            this.PO_GRP.Name = "PO_GRP";
            this.PO_GRP.Size = new System.Drawing.Size(439, 89);
            this.PO_GRP.TabIndex = 1006;
            this.PO_GRP.TabStop = false;
            // 
            // IDH_PUOM
            // 
            this.IDH_PUOM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PUOM.CornerRadius = 2;
            this.IDH_PUOM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PUOM.DropDownHight = 100;
            this.IDH_PUOM.EnabledColor = System.Drawing.Color.White;
            this.IDH_PUOM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PUOM.Location = new System.Drawing.Point(108, 9);
            this.IDH_PUOM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PUOM.Multiline = false;
            this.IDH_PUOM.Name = "IDH_PUOM";
            this.IDH_PUOM.SelectedIndex = -1;
            this.IDH_PUOM.SelectedItem = null;
            this.IDH_PUOM.Size = new System.Drawing.Size(54, 14);
            this.IDH_PUOM.TabIndex = 808;
            this.IDH_PUOM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PUOM.TextBoxReadOnly = true;
            this.IDH_PUOM.TextValue = "";
            this.IDH_PUOM.SelectedIndexChanged += new System.EventHandler(this.All_ComboChanged);
            // 
            // label118
            // 
            this.label118.Location = new System.Drawing.Point(3, 12);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(62, 15);
            this.label118.TabIndex = 109;
            this.label118.Text = "Disposal Qty";
            // 
            // IDH_PURUOM
            // 
            this.IDH_PURUOM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PURUOM.CornerRadius = 2;
            this.IDH_PURUOM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PURUOM.DropDownHight = 100;
            this.IDH_PURUOM.EnabledColor = System.Drawing.Color.White;
            this.IDH_PURUOM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PURUOM.Location = new System.Drawing.Point(108, 40);
            this.IDH_PURUOM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PURUOM.Multiline = false;
            this.IDH_PURUOM.Name = "IDH_PURUOM";
            this.IDH_PURUOM.SelectedIndex = -1;
            this.IDH_PURUOM.SelectedItem = null;
            this.IDH_PURUOM.Size = new System.Drawing.Size(54, 14);
            this.IDH_PURUOM.TabIndex = 807;
            this.IDH_PURUOM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PURUOM.TextBoxReadOnly = true;
            this.IDH_PURUOM.TextValue = "";
            this.IDH_PURUOM.SelectedIndexChanged += new System.EventHandler(this.All_ComboChanged);
            // 
            // label105
            // 
            this.label105.Location = new System.Drawing.Point(3, 40);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(62, 15);
            this.label105.TabIndex = 96;
            this.label105.Text = "Purchase Qty";
            // 
            // IDH_ORDTOT
            // 
            this.IDH_ORDTOT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ORDTOT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORDTOT.CornerRadius = 2;
            this.IDH_ORDTOT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORDTOT.Enabled = false;
            this.IDH_ORDTOT.EnabledColor = System.Drawing.Color.White;
            this.IDH_ORDTOT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORDTOT.Location = new System.Drawing.Point(368, 25);
            this.IDH_ORDTOT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ORDTOT.Multiline = false;
            this.IDH_ORDTOT.Name = "IDH_ORDTOT";
            this.IDH_ORDTOT.ReadOnly = false;
            this.IDH_ORDTOT.Size = new System.Drawing.Size(68, 14);
            this.IDH_ORDTOT.TabIndex = 115;
            this.IDH_ORDTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_ORDTOT.TextValue = "";
            // 
            // label104
            // 
            this.label104.Location = new System.Drawing.Point(164, 40);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(44, 15);
            this.label104.TabIndex = 92;
            this.label104.Text = "Cost";
            // 
            // label119
            // 
            this.label119.Location = new System.Drawing.Point(280, 25);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(70, 15);
            this.label119.TabIndex = 114;
            this.label119.Text = "Order Cost";
            // 
            // IDH_PURWGT
            // 
            this.IDH_PURWGT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PURWGT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PURWGT.CornerRadius = 2;
            this.IDH_PURWGT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PURWGT.EnabledColor = System.Drawing.Color.White;
            this.IDH_PURWGT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PURWGT.Location = new System.Drawing.Point(66, 40);
            this.IDH_PURWGT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PURWGT.Multiline = false;
            this.IDH_PURWGT.Name = "IDH_PURWGT";
            this.IDH_PURWGT.ReadOnly = false;
            this.IDH_PURWGT.Size = new System.Drawing.Size(41, 14);
            this.IDH_PURWGT.TabIndex = 90;
            this.IDH_PURWGT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_PURWGT.TextValue = "";
            // 
            // IDH_ORDCOS
            // 
            this.IDH_ORDCOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ORDCOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORDCOS.CornerRadius = 2;
            this.IDH_ORDCOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORDCOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_ORDCOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORDCOS.Location = new System.Drawing.Point(214, 25);
            this.IDH_ORDCOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ORDCOS.Multiline = false;
            this.IDH_ORDCOS.Name = "IDH_ORDCOS";
            this.IDH_ORDCOS.ReadOnly = false;
            this.IDH_ORDCOS.Size = new System.Drawing.Size(52, 14);
            this.IDH_ORDCOS.TabIndex = 113;
            this.IDH_ORDCOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_ORDCOS.TextValue = "";
            // 
            // IDH_PRCOST
            // 
            this.IDH_PRCOST.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRCOST.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRCOST.CornerRadius = 2;
            this.IDH_PRCOST.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRCOST.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRCOST.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRCOST.Location = new System.Drawing.Point(214, 40);
            this.IDH_PRCOST.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRCOST.Multiline = false;
            this.IDH_PRCOST.Name = "IDH_PRCOST";
            this.IDH_PRCOST.ReadOnly = false;
            this.IDH_PRCOST.Size = new System.Drawing.Size(52, 14);
            this.IDH_PRCOST.TabIndex = 93;
            this.IDH_PRCOST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_PRCOST.TextValue = "";
            // 
            // IDH_ORDQTY
            // 
            this.IDH_ORDQTY.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ORDQTY.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORDQTY.CornerRadius = 2;
            this.IDH_ORDQTY.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORDQTY.EnabledColor = System.Drawing.Color.White;
            this.IDH_ORDQTY.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORDQTY.Location = new System.Drawing.Point(66, 25);
            this.IDH_ORDQTY.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ORDQTY.Multiline = false;
            this.IDH_ORDQTY.Name = "IDH_ORDQTY";
            this.IDH_ORDQTY.ReadOnly = false;
            this.IDH_ORDQTY.Size = new System.Drawing.Size(41, 14);
            this.IDH_ORDQTY.TabIndex = 110;
            this.IDH_ORDQTY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_ORDQTY.TextValue = "";
            // 
            // label103
            // 
            this.label103.Location = new System.Drawing.Point(280, 40);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(70, 15);
            this.label103.TabIndex = 94;
            this.label103.Text = "Purchase Cost";
            // 
            // label120
            // 
            this.label120.Location = new System.Drawing.Point(137, 25);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(76, 15);
            this.label120.TabIndex = 112;
            this.label120.Text = "Cost ( per Unit )";
            // 
            // IDH_PURTOT
            // 
            this.IDH_PURTOT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PURTOT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PURTOT.CornerRadius = 2;
            this.IDH_PURTOT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PURTOT.Enabled = false;
            this.IDH_PURTOT.EnabledColor = System.Drawing.Color.White;
            this.IDH_PURTOT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PURTOT.Location = new System.Drawing.Point(368, 40);
            this.IDH_PURTOT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PURTOT.Multiline = false;
            this.IDH_PURTOT.Name = "IDH_PURTOT";
            this.IDH_PURTOT.ReadOnly = false;
            this.IDH_PURTOT.Size = new System.Drawing.Size(68, 14);
            this.IDH_PURTOT.TabIndex = 95;
            this.IDH_PURTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_PURTOT.TextValue = "";
            // 
            // IDH_DOPO
            // 
            this.IDH_DOPO.AutoSize = true;
            this.IDH_DOPO.Location = new System.Drawing.Point(3, 70);
            this.IDH_DOPO.Name = "IDH_DOPO";
            this.IDH_DOPO.Size = new System.Drawing.Size(118, 16);
            this.IDH_DOPO.TabIndex = 803;
            this.IDH_DOPO.Text = "Create Purchase Order";
            this.IDH_DOPO.UseVisualStyleBackColor = true;
            this.IDH_DOPO.Visible = false;
            this.IDH_DOPO.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // label121
            // 
            this.label121.Location = new System.Drawing.Point(3, 25);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(62, 15);
            this.label121.TabIndex = 116;
            this.label121.Text = "Order Qty";
            // 
            // label106
            // 
            this.label106.Location = new System.Drawing.Point(279, 55);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(70, 15);
            this.label106.TabIndex = 97;
            this.label106.Text = "Tax";
            // 
            // IDH_TIPTOT
            // 
            this.IDH_TIPTOT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TIPTOT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TIPTOT.CornerRadius = 2;
            this.IDH_TIPTOT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TIPTOT.Enabled = false;
            this.IDH_TIPTOT.EnabledColor = System.Drawing.Color.White;
            this.IDH_TIPTOT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TIPTOT.Location = new System.Drawing.Point(368, 10);
            this.IDH_TIPTOT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TIPTOT.Multiline = false;
            this.IDH_TIPTOT.Name = "IDH_TIPTOT";
            this.IDH_TIPTOT.ReadOnly = false;
            this.IDH_TIPTOT.Size = new System.Drawing.Size(68, 14);
            this.IDH_TIPTOT.TabIndex = 108;
            this.IDH_TIPTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_TIPTOT.TextValue = "";
            // 
            // IDH_TAXO
            // 
            this.IDH_TAXO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TAXO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TAXO.CornerRadius = 2;
            this.IDH_TAXO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TAXO.Enabled = false;
            this.IDH_TAXO.EnabledColor = System.Drawing.Color.White;
            this.IDH_TAXO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TAXO.Location = new System.Drawing.Point(368, 55);
            this.IDH_TAXO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TAXO.Multiline = false;
            this.IDH_TAXO.Name = "IDH_TAXO";
            this.IDH_TAXO.ReadOnly = false;
            this.IDH_TAXO.Size = new System.Drawing.Size(68, 14);
            this.IDH_TAXO.TabIndex = 98;
            this.IDH_TAXO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_TAXO.TextValue = "";
            // 
            // label116
            // 
            this.label116.Location = new System.Drawing.Point(280, 12);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(70, 15);
            this.label116.TabIndex = 107;
            this.label116.Text = "Disposal Cost";
            // 
            // label107
            // 
            this.label107.Location = new System.Drawing.Point(164, 70);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(50, 16);
            this.label107.TabIndex = 100;
            this.label107.Text = "Status";
            // 
            // IDH_TIPCOS
            // 
            this.IDH_TIPCOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TIPCOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TIPCOS.CornerRadius = 2;
            this.IDH_TIPCOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TIPCOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_TIPCOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TIPCOS.Location = new System.Drawing.Point(214, 10);
            this.IDH_TIPCOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TIPCOS.Multiline = false;
            this.IDH_TIPCOS.Name = "IDH_TIPCOS";
            this.IDH_TIPCOS.ReadOnly = false;
            this.IDH_TIPCOS.Size = new System.Drawing.Size(52, 14);
            this.IDH_TIPCOS.TabIndex = 106;
            this.IDH_TIPCOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_TIPCOS.TextValue = "";
            // 
            // IDH_PSTAT
            // 
            this.IDH_PSTAT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PSTAT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PSTAT.CornerRadius = 2;
            this.IDH_PSTAT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PSTAT.Enabled = false;
            this.IDH_PSTAT.EnabledColor = System.Drawing.Color.White;
            this.IDH_PSTAT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PSTAT.Location = new System.Drawing.Point(214, 70);
            this.IDH_PSTAT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PSTAT.Multiline = false;
            this.IDH_PSTAT.Name = "IDH_PSTAT";
            this.IDH_PSTAT.ReadOnly = false;
            this.IDH_PSTAT.Size = new System.Drawing.Size(53, 14);
            this.IDH_PSTAT.TabIndex = 99;
            this.IDH_PSTAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PSTAT.TextValue = "";
            // 
            // IDH_TIPWEI
            // 
            this.IDH_TIPWEI.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TIPWEI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TIPWEI.CornerRadius = 2;
            this.IDH_TIPWEI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TIPWEI.EnabledColor = System.Drawing.Color.White;
            this.IDH_TIPWEI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TIPWEI.Location = new System.Drawing.Point(66, 9);
            this.IDH_TIPWEI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TIPWEI.Multiline = false;
            this.IDH_TIPWEI.Name = "IDH_TIPWEI";
            this.IDH_TIPWEI.ReadOnly = false;
            this.IDH_TIPWEI.Size = new System.Drawing.Size(41, 14);
            this.IDH_TIPWEI.TabIndex = 103;
            this.IDH_TIPWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_TIPWEI.TextValue = "";
            // 
            // label108
            // 
            this.label108.Location = new System.Drawing.Point(280, 70);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(70, 15);
            this.label108.TabIndex = 101;
            this.label108.Text = "Total Cost";
            // 
            // label117
            // 
            this.label117.Location = new System.Drawing.Point(164, 10);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(44, 15);
            this.label117.TabIndex = 105;
            this.label117.Text = "Cost";
            // 
            // IDH_TOTCOS
            // 
            this.IDH_TOTCOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TOTCOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TOTCOS.CornerRadius = 2;
            this.IDH_TOTCOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TOTCOS.Enabled = false;
            this.IDH_TOTCOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_TOTCOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TOTCOS.Location = new System.Drawing.Point(368, 70);
            this.IDH_TOTCOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TOTCOS.Multiline = false;
            this.IDH_TOTCOS.Name = "IDH_TOTCOS";
            this.IDH_TOTCOS.ReadOnly = false;
            this.IDH_TOTCOS.Size = new System.Drawing.Size(68, 14);
            this.IDH_TOTCOS.TabIndex = 102;
            this.IDH_TOTCOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_TOTCOS.TextValue = "";
            // 
            // SO_GRP
            // 
            this.SO_GRP.Controls.Add(this.IDH_UOM);
            this.SO_GRP.Controls.Add(this.label90);
            this.SO_GRP.Controls.Add(this.IDH_VALDED);
            this.SO_GRP.Controls.Add(this.label93);
            this.SO_GRP.Controls.Add(this.label115);
            this.SO_GRP.Controls.Add(this.label92);
            this.SO_GRP.Controls.Add(this.label91);
            this.SO_GRP.Controls.Add(this.IDH_CUSTOT);
            this.SO_GRP.Controls.Add(this.IDH_DOORD);
            this.SO_GRP.Controls.Add(this.IDH_DSCPRC);
            this.SO_GRP.Controls.Add(this.label89);
            this.SO_GRP.Controls.Add(this.IDH_ADDEX);
            this.SO_GRP.Controls.Add(this.IDH_DOARIP);
            this.SO_GRP.Controls.Add(this.IDH_SUBTOT);
            this.SO_GRP.Controls.Add(this.IDH_DOARI);
            this.SO_GRP.Controls.Add(this.IDH_TAX);
            this.SO_GRP.Controls.Add(this.label88);
            this.SO_GRP.Controls.Add(this.IDH_NOVAT);
            this.SO_GRP.Controls.Add(this.IDH_CASHMT);
            this.SO_GRP.Controls.Add(this.IDH_TOTAL);
            this.SO_GRP.Controls.Add(this.IDH_FOC);
            this.SO_GRP.Controls.Add(this.label82);
            this.SO_GRP.Controls.Add(this.IDH_RSTAT);
            this.SO_GRP.Controls.Add(this.label83);
            this.SO_GRP.Controls.Add(this.IDH_DISCNT);
            this.SO_GRP.Controls.Add(this.label84);
            this.SO_GRP.Controls.Add(this.IDH_CUSWEI);
            this.SO_GRP.Controls.Add(this.label85);
            this.SO_GRP.Controls.Add(this.IDH_CUSCHG);
            this.SO_GRP.Controls.Add(this.label86);
            this.SO_GRP.Controls.Add(this.IDH_AINV);
            this.SO_GRP.Controls.Add(this.label87);
            this.SO_GRP.Location = new System.Drawing.Point(383, 156);
            this.SO_GRP.Margin = new System.Windows.Forms.Padding(2);
            this.SO_GRP.Name = "SO_GRP";
            this.SO_GRP.Padding = new System.Windows.Forms.Padding(2);
            this.SO_GRP.Size = new System.Drawing.Size(439, 121);
            this.SO_GRP.TabIndex = 1006;
            this.SO_GRP.TabStop = false;
            // 
            // IDH_UOM
            // 
            this.IDH_UOM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_UOM.CornerRadius = 2;
            this.IDH_UOM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_UOM.DropDownHight = 100;
            this.IDH_UOM.EnabledColor = System.Drawing.Color.White;
            this.IDH_UOM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_UOM.Location = new System.Drawing.Point(109, 10);
            this.IDH_UOM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_UOM.Multiline = false;
            this.IDH_UOM.Name = "IDH_UOM";
            this.IDH_UOM.SelectedIndex = -1;
            this.IDH_UOM.SelectedItem = null;
            this.IDH_UOM.Size = new System.Drawing.Size(54, 14);
            this.IDH_UOM.TabIndex = 806;
            this.IDH_UOM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_UOM.TextBoxReadOnly = true;
            this.IDH_UOM.TextValue = "";
            this.IDH_UOM.SelectedIndexChanged += new System.EventHandler(this.All_ComboChanged);
            // 
            // label90
            // 
            this.label90.Location = new System.Drawing.Point(4, 10);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(62, 15);
            this.label90.TabIndex = 89;
            this.label90.Text = "Cust. Qty";
            // 
            // IDH_VALDED
            // 
            this.IDH_VALDED.BackColor = System.Drawing.Color.Transparent;
            this.IDH_VALDED.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_VALDED.CornerRadius = 2;
            this.IDH_VALDED.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_VALDED.Enabled = false;
            this.IDH_VALDED.EnabledColor = System.Drawing.Color.White;
            this.IDH_VALDED.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_VALDED.Location = new System.Drawing.Point(369, 85);
            this.IDH_VALDED.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_VALDED.Multiline = false;
            this.IDH_VALDED.Name = "IDH_VALDED";
            this.IDH_VALDED.ReadOnly = false;
            this.IDH_VALDED.Size = new System.Drawing.Size(67, 14);
            this.IDH_VALDED.TabIndex = 157;
            this.IDH_VALDED.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_VALDED.TextValue = "";
            // 
            // label93
            // 
            this.label93.Location = new System.Drawing.Point(165, 85);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(49, 14);
            this.label93.TabIndex = 92;
            this.label93.Text = "Pay Mth.";
            // 
            // label115
            // 
            this.label115.Location = new System.Drawing.Point(281, 85);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(70, 15);
            this.label115.TabIndex = 156;
            this.label115.Text = "Deduction";
            // 
            // label92
            // 
            this.label92.Location = new System.Drawing.Point(165, 70);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(49, 13);
            this.label92.TabIndex = 91;
            this.label92.Text = "Status";
            // 
            // label91
            // 
            this.label91.Location = new System.Drawing.Point(165, 25);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(49, 15);
            this.label91.TabIndex = 25;
            this.label91.Text = "Discount";
            // 
            // IDH_CUSTOT
            // 
            this.IDH_CUSTOT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSTOT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSTOT.CornerRadius = 2;
            this.IDH_CUSTOT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSTOT.Enabled = false;
            this.IDH_CUSTOT.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSTOT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSTOT.Location = new System.Drawing.Point(369, 10);
            this.IDH_CUSTOT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSTOT.Multiline = false;
            this.IDH_CUSTOT.Name = "IDH_CUSTOT";
            this.IDH_CUSTOT.ReadOnly = false;
            this.IDH_CUSTOT.Size = new System.Drawing.Size(67, 14);
            this.IDH_CUSTOT.TabIndex = 147;
            this.IDH_CUSTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_CUSTOT.TextValue = "";
            // 
            // IDH_DOORD
            // 
            this.IDH_DOORD.Location = new System.Drawing.Point(4, 30);
            this.IDH_DOORD.Name = "IDH_DOORD";
            this.IDH_DOORD.Size = new System.Drawing.Size(82, 14);
            this.IDH_DOORD.TabIndex = 800;
            this.IDH_DOORD.Text = "Account";
            this.IDH_DOORD.UseVisualStyleBackColor = true;
            this.IDH_DOORD.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // IDH_DSCPRC
            // 
            this.IDH_DSCPRC.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DSCPRC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DSCPRC.CornerRadius = 2;
            this.IDH_DSCPRC.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DSCPRC.EnabledColor = System.Drawing.Color.White;
            this.IDH_DSCPRC.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DSCPRC.Location = new System.Drawing.Point(369, 25);
            this.IDH_DSCPRC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DSCPRC.Multiline = false;
            this.IDH_DSCPRC.Name = "IDH_DSCPRC";
            this.IDH_DSCPRC.ReadOnly = false;
            this.IDH_DSCPRC.Size = new System.Drawing.Size(67, 14);
            this.IDH_DSCPRC.TabIndex = 149;
            this.IDH_DSCPRC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_DSCPRC.TextValue = "";
            // 
            // label89
            // 
            this.label89.Location = new System.Drawing.Point(164, 10);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(44, 15);
            this.label89.TabIndex = 18;
            this.label89.Text = "Charge";
            // 
            // IDH_ADDEX
            // 
            this.IDH_ADDEX.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADDEX.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADDEX.CornerRadius = 2;
            this.IDH_ADDEX.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADDEX.Enabled = false;
            this.IDH_ADDEX.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADDEX.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADDEX.Location = new System.Drawing.Point(369, 70);
            this.IDH_ADDEX.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ADDEX.Multiline = false;
            this.IDH_ADDEX.Name = "IDH_ADDEX";
            this.IDH_ADDEX.ReadOnly = false;
            this.IDH_ADDEX.Size = new System.Drawing.Size(67, 14);
            this.IDH_ADDEX.TabIndex = 150;
            this.IDH_ADDEX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_ADDEX.TextValue = "";
            // 
            // IDH_DOARIP
            // 
            this.IDH_DOARIP.Enabled = false;
            this.IDH_DOARIP.Location = new System.Drawing.Point(4, 60);
            this.IDH_DOARIP.Name = "IDH_DOARIP";
            this.IDH_DOARIP.Size = new System.Drawing.Size(133, 14);
            this.IDH_DOARIP.TabIndex = 802;
            this.IDH_DOARIP.Text = "AR Invoice + Payment";
            this.IDH_DOARIP.UseVisualStyleBackColor = true;
            this.IDH_DOARIP.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // IDH_SUBTOT
            // 
            this.IDH_SUBTOT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SUBTOT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SUBTOT.CornerRadius = 2;
            this.IDH_SUBTOT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SUBTOT.Enabled = false;
            this.IDH_SUBTOT.EnabledColor = System.Drawing.Color.White;
            this.IDH_SUBTOT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SUBTOT.Location = new System.Drawing.Point(369, 40);
            this.IDH_SUBTOT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SUBTOT.Multiline = false;
            this.IDH_SUBTOT.Name = "IDH_SUBTOT";
            this.IDH_SUBTOT.ReadOnly = false;
            this.IDH_SUBTOT.Size = new System.Drawing.Size(67, 14);
            this.IDH_SUBTOT.TabIndex = 151;
            this.IDH_SUBTOT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_SUBTOT.TextValue = "";
            // 
            // IDH_DOARI
            // 
            this.IDH_DOARI.Location = new System.Drawing.Point(4, 45);
            this.IDH_DOARI.Name = "IDH_DOARI";
            this.IDH_DOARI.Size = new System.Drawing.Size(80, 14);
            this.IDH_DOARI.TabIndex = 801;
            this.IDH_DOARI.Text = "Cash";
            this.IDH_DOARI.UseVisualStyleBackColor = true;
            this.IDH_DOARI.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // IDH_TAX
            // 
            this.IDH_TAX.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TAX.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TAX.CornerRadius = 2;
            this.IDH_TAX.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TAX.Enabled = false;
            this.IDH_TAX.EnabledColor = System.Drawing.Color.White;
            this.IDH_TAX.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TAX.Location = new System.Drawing.Point(369, 55);
            this.IDH_TAX.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TAX.Multiline = false;
            this.IDH_TAX.Name = "IDH_TAX";
            this.IDH_TAX.ReadOnly = false;
            this.IDH_TAX.Size = new System.Drawing.Size(67, 14);
            this.IDH_TAX.TabIndex = 153;
            this.IDH_TAX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_TAX.TextValue = "";
            // 
            // label88
            // 
            this.label88.Location = new System.Drawing.Point(251, 25);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(18, 23);
            this.label88.TabIndex = 27;
            this.label88.Text = "%";
            // 
            // IDH_NOVAT
            // 
            this.IDH_NOVAT.Location = new System.Drawing.Point(154, 55);
            this.IDH_NOVAT.Name = "IDH_NOVAT";
            this.IDH_NOVAT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.IDH_NOVAT.Size = new System.Drawing.Size(113, 14);
            this.IDH_NOVAT.TabIndex = 805;
            this.IDH_NOVAT.Text = "Don\'t Charge VAT  ";
            this.IDH_NOVAT.UseVisualStyleBackColor = true;
            this.IDH_NOVAT.CheckedChanged += new System.EventHandler(this.IDH_NOVATCheckedChanged);
            // 
            // IDH_CASHMT
            // 
            this.IDH_CASHMT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CASHMT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CASHMT.CornerRadius = 2;
            this.IDH_CASHMT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CASHMT.Enabled = false;
            this.IDH_CASHMT.EnabledColor = System.Drawing.Color.White;
            this.IDH_CASHMT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CASHMT.Location = new System.Drawing.Point(215, 85);
            this.IDH_CASHMT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CASHMT.Multiline = false;
            this.IDH_CASHMT.Name = "IDH_CASHMT";
            this.IDH_CASHMT.ReadOnly = false;
            this.IDH_CASHMT.Size = new System.Drawing.Size(51, 14);
            this.IDH_CASHMT.TabIndex = 154;
            this.IDH_CASHMT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CASHMT.TextValue = "";
            // 
            // IDH_TOTAL
            // 
            this.IDH_TOTAL.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TOTAL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TOTAL.CornerRadius = 2;
            this.IDH_TOTAL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TOTAL.Enabled = false;
            this.IDH_TOTAL.EnabledColor = System.Drawing.Color.White;
            this.IDH_TOTAL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TOTAL.Location = new System.Drawing.Point(369, 100);
            this.IDH_TOTAL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TOTAL.Multiline = false;
            this.IDH_TOTAL.Name = "IDH_TOTAL";
            this.IDH_TOTAL.ReadOnly = false;
            this.IDH_TOTAL.Size = new System.Drawing.Size(67, 14);
            this.IDH_TOTAL.TabIndex = 155;
            this.IDH_TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_TOTAL.TextValue = "";
            // 
            // IDH_FOC
            // 
            this.IDH_FOC.Location = new System.Drawing.Point(4, 90);
            this.IDH_FOC.Name = "IDH_FOC";
            this.IDH_FOC.Size = new System.Drawing.Size(74, 14);
            this.IDH_FOC.TabIndex = 804;
            this.IDH_FOC.Text = "FOC";
            this.IDH_FOC.UseVisualStyleBackColor = true;
            this.IDH_FOC.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // label82
            // 
            this.label82.Location = new System.Drawing.Point(281, 10);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(70, 15);
            this.label82.TabIndex = 20;
            this.label82.Text = "Cust. Charge";
            // 
            // IDH_RSTAT
            // 
            this.IDH_RSTAT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_RSTAT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_RSTAT.CornerRadius = 2;
            this.IDH_RSTAT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_RSTAT.Enabled = false;
            this.IDH_RSTAT.EnabledColor = System.Drawing.Color.White;
            this.IDH_RSTAT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_RSTAT.Location = new System.Drawing.Point(215, 70);
            this.IDH_RSTAT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_RSTAT.Multiline = false;
            this.IDH_RSTAT.Name = "IDH_RSTAT";
            this.IDH_RSTAT.ReadOnly = false;
            this.IDH_RSTAT.Size = new System.Drawing.Size(51, 14);
            this.IDH_RSTAT.TabIndex = 152;
            this.IDH_RSTAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_RSTAT.TextValue = "";
            // 
            // label83
            // 
            this.label83.Location = new System.Drawing.Point(281, 25);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(70, 15);
            this.label83.TabIndex = 28;
            this.label83.Text = "Amount";
            // 
            // IDH_DISCNT
            // 
            this.IDH_DISCNT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DISCNT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DISCNT.CornerRadius = 2;
            this.IDH_DISCNT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DISCNT.EnabledColor = System.Drawing.Color.White;
            this.IDH_DISCNT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DISCNT.Location = new System.Drawing.Point(214, 25);
            this.IDH_DISCNT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DISCNT.Multiline = false;
            this.IDH_DISCNT.Name = "IDH_DISCNT";
            this.IDH_DISCNT.ReadOnly = false;
            this.IDH_DISCNT.Size = new System.Drawing.Size(31, 14);
            this.IDH_DISCNT.TabIndex = 148;
            this.IDH_DISCNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_DISCNT.TextValue = "";
            // 
            // label84
            // 
            this.label84.Location = new System.Drawing.Point(281, 70);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(70, 15);
            this.label84.TabIndex = 30;
            this.label84.Text = "Add. Expenses";
            // 
            // IDH_CUSWEI
            // 
            this.IDH_CUSWEI.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSWEI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSWEI.CornerRadius = 2;
            this.IDH_CUSWEI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSWEI.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSWEI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSWEI.Location = new System.Drawing.Point(67, 10);
            this.IDH_CUSWEI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSWEI.Multiline = false;
            this.IDH_CUSWEI.Name = "IDH_CUSWEI";
            this.IDH_CUSWEI.ReadOnly = false;
            this.IDH_CUSWEI.Size = new System.Drawing.Size(41, 14);
            this.IDH_CUSWEI.TabIndex = 142;
            this.IDH_CUSWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_CUSWEI.TextValue = "";
            // 
            // label85
            // 
            this.label85.Location = new System.Drawing.Point(281, 40);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(70, 15);
            this.label85.TabIndex = 32;
            this.label85.Text = "Order Subtotal";
            // 
            // IDH_CUSCHG
            // 
            this.IDH_CUSCHG.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSCHG.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSCHG.CornerRadius = 2;
            this.IDH_CUSCHG.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSCHG.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSCHG.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSCHG.Location = new System.Drawing.Point(214, 10);
            this.IDH_CUSCHG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSCHG.Multiline = false;
            this.IDH_CUSCHG.Name = "IDH_CUSCHG";
            this.IDH_CUSCHG.ReadOnly = false;
            this.IDH_CUSCHG.Size = new System.Drawing.Size(52, 14);
            this.IDH_CUSCHG.TabIndex = 146;
            this.IDH_CUSCHG.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_CUSCHG.TextValue = "";
            // 
            // label86
            // 
            this.label86.Location = new System.Drawing.Point(281, 55);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(70, 15);
            this.label86.TabIndex = 35;
            this.label86.Text = "Tax";
            // 
            // IDH_AINV
            // 
            this.IDH_AINV.Enabled = false;
            this.IDH_AINV.Location = new System.Drawing.Point(4, 75);
            this.IDH_AINV.Name = "IDH_AINV";
            this.IDH_AINV.Size = new System.Drawing.Size(105, 14);
            this.IDH_AINV.TabIndex = 803;
            this.IDH_AINV.Text = "Already Invoiced";
            this.IDH_AINV.UseVisualStyleBackColor = true;
            this.IDH_AINV.CheckedChanged += new System.EventHandler(this.IDH_ACCCheckedChanged);
            // 
            // label87
            // 
            this.label87.Location = new System.Drawing.Point(281, 100);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(70, 15);
            this.label87.TabIndex = 38;
            this.label87.Text = "Total Charge";
            // 
            // IDH_ISTRL
            // 
            this.IDH_ISTRL.AutoSize = true;
            this.IDH_ISTRL.Location = new System.Drawing.Point(105, 70);
            this.IDH_ISTRL.Name = "IDH_ISTRL";
            this.IDH_ISTRL.Size = new System.Drawing.Size(63, 16);
            this.IDH_ISTRL.TabIndex = 811;
            this.IDH_ISTRL.Text = "Trail Load";
            this.IDH_ISTRL.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label109);
            this.groupBox1.Controls.Add(this.IDH_ITMGRN);
            this.groupBox1.Controls.Add(this.IDH_ITMGRC);
            this.groupBox1.Controls.Add(this.Lbl1000007);
            this.groupBox1.Controls.Add(this.IDH_ITMCOD);
            this.groupBox1.Controls.Add(this.IDH_DESC);
            this.groupBox1.Controls.Add(this.IDH_WASCL1);
            this.groupBox1.Controls.Add(this.IDH_WASMAT);
            this.groupBox1.Controls.Add(this.IDH_RORIGI);
            this.groupBox1.Controls.Add(this.IDH_ITCF);
            this.groupBox1.Controls.Add(this.IDH_WASCF);
            this.groupBox1.Controls.Add(this.IDH_ORLK);
            this.groupBox1.Controls.Add(this.label76);
            this.groupBox1.Controls.Add(this.label77);
            this.groupBox1.Controls.Add(this.label78);
            this.groupBox1.Controls.Add(this.IDH_PRONM);
            this.groupBox1.Controls.Add(this.label79);
            this.groupBox1.Controls.Add(this.label80);
            this.groupBox1.Controls.Add(this.label81);
            this.groupBox1.Controls.Add(this.IDH_PROCD);
            this.groupBox1.Controls.Add(this.IDH_PROCF);
            this.groupBox1.Location = new System.Drawing.Point(0, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(826, 62);
            this.groupBox1.TabIndex = 810;
            this.groupBox1.TabStop = false;
            // 
            // label109
            // 
            this.label109.Location = new System.Drawing.Point(283, 25);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(90, 15);
            this.label109.TabIndex = 104;
            this.label109.Text = "Name";
            // 
            // IDH_ITMGRN
            // 
            this.IDH_ITMGRN.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ITMGRN.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ITMGRN.CornerRadius = 2;
            this.IDH_ITMGRN.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ITMGRN.Enabled = false;
            this.IDH_ITMGRN.EnabledColor = System.Drawing.Color.White;
            this.IDH_ITMGRN.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ITMGRN.Location = new System.Drawing.Point(156, 10);
            this.IDH_ITMGRN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ITMGRN.Multiline = false;
            this.IDH_ITMGRN.Name = "IDH_ITMGRN";
            this.IDH_ITMGRN.ReadOnly = false;
            this.IDH_ITMGRN.Size = new System.Drawing.Size(100, 14);
            this.IDH_ITMGRN.TabIndex = 136;
            this.IDH_ITMGRN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ITMGRN.TextValue = "";
            // 
            // IDH_ITMGRC
            // 
            this.IDH_ITMGRC.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ITMGRC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ITMGRC.CornerRadius = 2;
            this.IDH_ITMGRC.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ITMGRC.Enabled = false;
            this.IDH_ITMGRC.EnabledColor = System.Drawing.Color.White;
            this.IDH_ITMGRC.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ITMGRC.Location = new System.Drawing.Point(100, 10);
            this.IDH_ITMGRC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ITMGRC.Multiline = false;
            this.IDH_ITMGRC.Name = "IDH_ITMGRC";
            this.IDH_ITMGRC.ReadOnly = false;
            this.IDH_ITMGRC.Size = new System.Drawing.Size(55, 14);
            this.IDH_ITMGRC.TabIndex = 135;
            this.IDH_ITMGRC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ITMGRC.TextValue = "";
            // 
            // Lbl1000007
            // 
            this.Lbl1000007.Location = new System.Drawing.Point(283, 10);
            this.Lbl1000007.Name = "Lbl1000007";
            this.Lbl1000007.Size = new System.Drawing.Size(90, 15);
            this.Lbl1000007.TabIndex = 103;
            this.Lbl1000007.Text = "Buying From";
            // 
            // IDH_ITMCOD
            // 
            this.IDH_ITMCOD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ITMCOD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ITMCOD.CornerRadius = 2;
            this.IDH_ITMCOD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ITMCOD.EnabledColor = System.Drawing.Color.White;
            this.IDH_ITMCOD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ITMCOD.Location = new System.Drawing.Point(100, 25);
            this.IDH_ITMCOD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ITMCOD.Multiline = false;
            this.IDH_ITMCOD.Name = "IDH_ITMCOD";
            this.IDH_ITMCOD.ReadOnly = false;
            this.IDH_ITMCOD.Size = new System.Drawing.Size(156, 14);
            this.IDH_ITMCOD.TabIndex = 5;
            this.IDH_ITMCOD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ITMCOD.TextValue = "";
            // 
            // IDH_DESC
            // 
            this.IDH_DESC.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DESC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DESC.CornerRadius = 2;
            this.IDH_DESC.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DESC.EnabledColor = System.Drawing.Color.White;
            this.IDH_DESC.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DESC.Location = new System.Drawing.Point(100, 40);
            this.IDH_DESC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DESC.Multiline = false;
            this.IDH_DESC.Name = "IDH_DESC";
            this.IDH_DESC.ReadOnly = false;
            this.IDH_DESC.Size = new System.Drawing.Size(156, 14);
            this.IDH_DESC.TabIndex = 138;
            this.IDH_DESC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DESC.TextValue = "";
            // 
            // IDH_WASCL1
            // 
            this.IDH_WASCL1.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WASCL1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WASCL1.CornerRadius = 2;
            this.IDH_WASCL1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WASCL1.EnabledColor = System.Drawing.Color.White;
            this.IDH_WASCL1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WASCL1.Location = new System.Drawing.Point(666, 10);
            this.IDH_WASCL1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WASCL1.Multiline = false;
            this.IDH_WASCL1.Name = "IDH_WASCL1";
            this.IDH_WASCL1.ReadOnly = false;
            this.IDH_WASCL1.Size = new System.Drawing.Size(138, 14);
            this.IDH_WASCL1.TabIndex = 6;
            this.IDH_WASCL1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WASCL1.TextValue = "";
            // 
            // IDH_WASMAT
            // 
            this.IDH_WASMAT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WASMAT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WASMAT.CornerRadius = 2;
            this.IDH_WASMAT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WASMAT.EnabledColor = System.Drawing.Color.White;
            this.IDH_WASMAT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WASMAT.Location = new System.Drawing.Point(666, 25);
            this.IDH_WASMAT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WASMAT.Multiline = false;
            this.IDH_WASMAT.Name = "IDH_WASMAT";
            this.IDH_WASMAT.ReadOnly = false;
            this.IDH_WASMAT.Size = new System.Drawing.Size(153, 14);
            this.IDH_WASMAT.TabIndex = 140;
            this.IDH_WASMAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WASMAT.TextValue = "";
            // 
            // IDH_RORIGI
            // 
            this.IDH_RORIGI.BackColor = System.Drawing.Color.Transparent;
            this.IDH_RORIGI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_RORIGI.CornerRadius = 2;
            this.IDH_RORIGI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_RORIGI.EnabledColor = System.Drawing.Color.White;
            this.IDH_RORIGI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_RORIGI.Location = new System.Drawing.Point(666, 40);
            this.IDH_RORIGI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_RORIGI.Multiline = false;
            this.IDH_RORIGI.Name = "IDH_RORIGI";
            this.IDH_RORIGI.ReadOnly = false;
            this.IDH_RORIGI.Size = new System.Drawing.Size(138, 14);
            this.IDH_RORIGI.TabIndex = 7;
            this.IDH_RORIGI.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_RORIGI.TextValue = "";
            // 
            // IDH_ITCF
            // 
            this.IDH_ITCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ITCF.Image")));
            this.IDH_ITCF.Location = new System.Drawing.Point(256, 25);
            this.IDH_ITCF.Name = "IDH_ITCF";
            this.IDH_ITCF.Size = new System.Drawing.Size(14, 14);
            this.IDH_ITCF.TabIndex = 137;
            this.IDH_ITCF.UseVisualStyleBackColor = true;
            this.IDH_ITCF.Click += new System.EventHandler(this.IDH_ITCFClick);
            // 
            // IDH_WASCF
            // 
            this.IDH_WASCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_WASCF.Image")));
            this.IDH_WASCF.Location = new System.Drawing.Point(804, 9);
            this.IDH_WASCF.Name = "IDH_WASCF";
            this.IDH_WASCF.Size = new System.Drawing.Size(14, 14);
            this.IDH_WASCF.TabIndex = 139;
            this.IDH_WASCF.UseVisualStyleBackColor = true;
            this.IDH_WASCF.Click += new System.EventHandler(this.IDH_WASCFClick);
            // 
            // IDH_ORLK
            // 
            this.IDH_ORLK.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ORLK.Image")));
            this.IDH_ORLK.Location = new System.Drawing.Point(804, 40);
            this.IDH_ORLK.Name = "IDH_ORLK";
            this.IDH_ORLK.Size = new System.Drawing.Size(14, 14);
            this.IDH_ORLK.TabIndex = 141;
            this.IDH_ORLK.UseVisualStyleBackColor = true;
            this.IDH_ORLK.Click += new System.EventHandler(this.IDH_ORLKClick);
            // 
            // label76
            // 
            this.label76.Location = new System.Drawing.Point(5, 10);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(90, 15);
            this.label76.TabIndex = 69;
            this.label76.Text = "Container Group";
            // 
            // label77
            // 
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(5, 25);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(90, 15);
            this.label77.TabIndex = 70;
            this.label77.Text = "Container Code";
            // 
            // label78
            // 
            this.label78.Location = new System.Drawing.Point(5, 40);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(90, 15);
            this.label78.TabIndex = 71;
            this.label78.Text = "Description";
            // 
            // IDH_PRONM
            // 
            this.IDH_PRONM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRONM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRONM.CornerRadius = 2;
            this.IDH_PRONM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRONM.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRONM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRONM.Location = new System.Drawing.Point(377, 25);
            this.IDH_PRONM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRONM.Multiline = false;
            this.IDH_PRONM.Name = "IDH_PRONM";
            this.IDH_PRONM.ReadOnly = false;
            this.IDH_PRONM.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRONM.TabIndex = 65;
            this.IDH_PRONM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRONM.TextValue = "";
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(555, 10);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(100, 15);
            this.label79.TabIndex = 72;
            this.label79.Text = "Waste Code";
            // 
            // label80
            // 
            this.label80.Location = new System.Drawing.Point(555, 25);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(100, 15);
            this.label80.TabIndex = 73;
            this.label80.Text = "Waste Material";
            // 
            // label81
            // 
            this.label81.Location = new System.Drawing.Point(555, 40);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(100, 15);
            this.label81.TabIndex = 74;
            this.label81.Text = "Origin Waste";
            // 
            // IDH_PROCD
            // 
            this.IDH_PROCD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PROCD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PROCD.CornerRadius = 2;
            this.IDH_PROCD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PROCD.EnabledColor = System.Drawing.Color.White;
            this.IDH_PROCD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PROCD.Location = new System.Drawing.Point(377, 10);
            this.IDH_PROCD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PROCD.Multiline = false;
            this.IDH_PROCD.Name = "IDH_PROCD";
            this.IDH_PROCD.ReadOnly = false;
            this.IDH_PROCD.Size = new System.Drawing.Size(155, 14);
            this.IDH_PROCD.TabIndex = 14;
            this.IDH_PROCD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PROCD.TextValue = "";
            // 
            // IDH_PROCF
            // 
            this.IDH_PROCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_PROCF.Image")));
            this.IDH_PROCF.Location = new System.Drawing.Point(532, 10);
            this.IDH_PROCF.Name = "IDH_PROCF";
            this.IDH_PROCF.Size = new System.Drawing.Size(14, 14);
            this.IDH_PROCF.TabIndex = 64;
            this.IDH_PROCF.UseVisualStyleBackColor = true;
            this.IDH_PROCF.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_ADJWGT
            // 
            this.IDH_ADJWGT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADJWGT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADJWGT.CornerRadius = 2;
            this.IDH_ADJWGT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADJWGT.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADJWGT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADJWGT.Location = new System.Drawing.Point(101, 260);
            this.IDH_ADJWGT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ADJWGT.Multiline = false;
            this.IDH_ADJWGT.Name = "IDH_ADJWGT";
            this.IDH_ADJWGT.ReadOnly = false;
            this.IDH_ADJWGT.Size = new System.Drawing.Size(54, 14);
            this.IDH_ADJWGT.TabIndex = 809;
            this.IDH_ADJWGT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_ADJWGT.TextValue = "";
            // 
            // IDH_WGTDED
            // 
            this.IDH_WGTDED.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WGTDED.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WGTDED.CornerRadius = 2;
            this.IDH_WGTDED.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WGTDED.EnabledColor = System.Drawing.Color.White;
            this.IDH_WGTDED.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WGTDED.Location = new System.Drawing.Point(101, 245);
            this.IDH_WGTDED.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WGTDED.Multiline = false;
            this.IDH_WGTDED.Name = "IDH_WGTDED";
            this.IDH_WGTDED.ReadOnly = false;
            this.IDH_WGTDED.Size = new System.Drawing.Size(54, 14);
            this.IDH_WGTDED.TabIndex = 808;
            this.IDH_WGTDED.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_WGTDED.TextValue = "";
            // 
            // label114
            // 
            this.label114.Location = new System.Drawing.Point(4, 260);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(90, 15);
            this.label114.TabIndex = 807;
            this.label114.Text = "Adjusted Weight";
            // 
            // label113
            // 
            this.label113.Location = new System.Drawing.Point(4, 245);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(90, 15);
            this.label113.TabIndex = 806;
            this.label113.Text = "Weight Deduction (Kg)";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(265, 217);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(77, 69);
            this.pictureBox1.TabIndex = 115;
            this.pictureBox1.TabStop = false;
            // 
            // IDH_TAR2
            // 
            this.IDH_TAR2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_TAR2.Location = new System.Drawing.Point(342, 200);
            this.IDH_TAR2.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_TAR2.Name = "IDH_TAR2";
            this.IDH_TAR2.Size = new System.Drawing.Size(37, 15);
            this.IDH_TAR2.TabIndex = 134;
            this.IDH_TAR2.Text = "Tare";
            this.IDH_TAR2.UseVisualStyleBackColor = false;
            this.IDH_TAR2.Click += new System.EventHandler(this.IDH_Tare2Click);
            // 
            // IDH_BUF2
            // 
            this.IDH_BUF2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_BUF2.Location = new System.Drawing.Point(310, 200);
            this.IDH_BUF2.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_BUF2.Name = "IDH_BUF2";
            this.IDH_BUF2.Size = new System.Drawing.Size(33, 15);
            this.IDH_BUF2.TabIndex = 133;
            this.IDH_BUF2.Text = "BU";
            this.IDH_BUF2.UseVisualStyleBackColor = false;
            // 
            // IDH_ACC2
            // 
            this.IDH_ACC2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_ACC2.Location = new System.Drawing.Point(266, 200);
            this.IDH_ACC2.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_ACC2.Name = "IDH_ACC2";
            this.IDH_ACC2.Size = new System.Drawing.Size(45, 15);
            this.IDH_ACC2.TabIndex = 8;
            this.IDH_ACC2.Text = "Bridge";
            this.IDH_ACC2.UseVisualStyleBackColor = false;
            this.IDH_ACC2.Click += new System.EventHandler(this.IDH_ACC2Click);
            // 
            // IDH_TAR1
            // 
            this.IDH_TAR1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_TAR1.Location = new System.Drawing.Point(342, 185);
            this.IDH_TAR1.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_TAR1.Name = "IDH_TAR1";
            this.IDH_TAR1.Size = new System.Drawing.Size(37, 15);
            this.IDH_TAR1.TabIndex = 129;
            this.IDH_TAR1.Text = "Tare";
            this.IDH_TAR1.UseVisualStyleBackColor = false;
            this.IDH_TAR1.Click += new System.EventHandler(this.IDH_Tare1Click);
            // 
            // IDH_BUF1
            // 
            this.IDH_BUF1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_BUF1.Location = new System.Drawing.Point(310, 186);
            this.IDH_BUF1.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_BUF1.Name = "IDH_BUF1";
            this.IDH_BUF1.Size = new System.Drawing.Size(33, 15);
            this.IDH_BUF1.TabIndex = 128;
            this.IDH_BUF1.Text = "BU";
            this.IDH_BUF1.UseVisualStyleBackColor = false;
            // 
            // IDH_ACC1
            // 
            this.IDH_ACC1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_ACC1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.IDH_ACC1.FlatAppearance.BorderSize = 0;
            this.IDH_ACC1.Location = new System.Drawing.Point(266, 185);
            this.IDH_ACC1.Margin = new System.Windows.Forms.Padding(0);
            this.IDH_ACC1.Name = "IDH_ACC1";
            this.IDH_ACC1.Size = new System.Drawing.Size(45, 15);
            this.IDH_ACC1.TabIndex = 4;
            this.IDH_ACC1.Text = "Bridge";
            this.IDH_ACC1.UseVisualStyleBackColor = false;
            this.IDH_ACC1.Click += new System.EventHandler(this.IDH_ACC1Click);
            // 
            // IDH_ACCB
            // 
            this.IDH_ACCB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_ACCB.Location = new System.Drawing.Point(266, 143);
            this.IDH_ACCB.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_ACCB.Name = "IDH_ACCB";
            this.IDH_ACCB.Size = new System.Drawing.Size(113, 20);
            this.IDH_ACCB.TabIndex = 121;
            this.IDH_ACCB.Text = "buffer";
            this.IDH_ACCB.UseVisualStyleBackColor = false;
            this.IDH_ACCB.Click += new System.EventHandler(this.IDH_ACCBClick);
            // 
            // IDH_TMR
            // 
            this.IDH_TMR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_TMR.Location = new System.Drawing.Point(266, 124);
            this.IDH_TMR.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_TMR.Name = "IDH_TMR";
            this.IDH_TMR.Size = new System.Drawing.Size(113, 20);
            this.IDH_TMR.TabIndex = 120;
            this.IDH_TMR.Text = "Start Reading";
            this.IDH_TMR.UseVisualStyleBackColor = false;
            this.IDH_TMR.Click += new System.EventHandler(this.IDH_TMRClick);
            // 
            // IDH_REF
            // 
            this.IDH_REF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_REF.Location = new System.Drawing.Point(266, 105);
            this.IDH_REF.Margin = new System.Windows.Forms.Padding(0);
            this.IDH_REF.Name = "IDH_REF";
            this.IDH_REF.Size = new System.Drawing.Size(113, 20);
            this.IDH_REF.TabIndex = 119;
            this.IDH_REF.Text = "Read";
            this.IDH_REF.UseVisualStyleBackColor = false;
            this.IDH_REF.Click += new System.EventHandler(this.IDH_REFClick);
            // 
            // label102
            // 
            this.label102.Location = new System.Drawing.Point(4, 230);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(90, 15);
            this.label102.TabIndex = 101;
            this.label102.Text = "Units";
            // 
            // label101
            // 
            this.label101.Location = new System.Drawing.Point(4, 215);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(90, 15);
            this.label101.TabIndex = 100;
            this.label101.Text = "Net Weight (Kg)";
            // 
            // label100
            // 
            this.label100.Location = new System.Drawing.Point(4, 200);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(90, 15);
            this.label100.TabIndex = 99;
            this.label100.Text = "2nd Weigh (Kg)";
            // 
            // label99
            // 
            this.label99.Location = new System.Drawing.Point(4, 185);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(90, 15);
            this.label99.TabIndex = 98;
            this.label99.Text = "1st Weigh (Kg)";
            // 
            // label98
            // 
            this.label98.Location = new System.Drawing.Point(4, 170);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(90, 15);
            this.label98.TabIndex = 97;
            this.label98.Text = "2nd Tare Weight (kg)";
            // 
            // label97
            // 
            this.label97.Location = new System.Drawing.Point(4, 155);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(90, 15);
            this.label97.TabIndex = 96;
            this.label97.Text = "Vehicle Tare Weight";
            // 
            // label96
            // 
            this.label96.Location = new System.Drawing.Point(4, 140);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(90, 15);
            this.label96.TabIndex = 95;
            this.label96.Text = "Weight Buffer (Kg)";
            // 
            // label95
            // 
            this.label95.Location = new System.Drawing.Point(4, 117);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(90, 15);
            this.label95.TabIndex = 94;
            this.label95.Text = "Current Reading";
            // 
            // label94
            // 
            this.label94.Location = new System.Drawing.Point(4, 90);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(90, 15);
            this.label94.TabIndex = 93;
            this.label94.Text = "Weighbridge";
            // 
            // IDH_CUSCM
            // 
            this.IDH_CUSCM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSCM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSCM.CornerRadius = 2;
            this.IDH_CUSCM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSCM.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSCM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSCM.Location = new System.Drawing.Point(101, 230);
            this.IDH_CUSCM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSCM.Multiline = false;
            this.IDH_CUSCM.Name = "IDH_CUSCM";
            this.IDH_CUSCM.ReadOnly = false;
            this.IDH_CUSCM.Size = new System.Drawing.Size(54, 14);
            this.IDH_CUSCM.TabIndex = 9;
            this.IDH_CUSCM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_CUSCM.TextValue = "";
            // 
            // IDH_RDWGT
            // 
            this.IDH_RDWGT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_RDWGT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_RDWGT.CornerRadius = 2;
            this.IDH_RDWGT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_RDWGT.EnabledColor = System.Drawing.Color.White;
            this.IDH_RDWGT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_RDWGT.Location = new System.Drawing.Point(101, 215);
            this.IDH_RDWGT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_RDWGT.Multiline = false;
            this.IDH_RDWGT.Name = "IDH_RDWGT";
            this.IDH_RDWGT.ReadOnly = false;
            this.IDH_RDWGT.Size = new System.Drawing.Size(54, 14);
            this.IDH_RDWGT.TabIndex = 5;
            this.IDH_RDWGT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_RDWGT.TextValue = "";
            // 
            // IDH_WDT2
            // 
            this.IDH_WDT2.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WDT2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WDT2.CornerRadius = 2;
            this.IDH_WDT2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WDT2.Enabled = false;
            this.IDH_WDT2.EnabledColor = System.Drawing.Color.White;
            this.IDH_WDT2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WDT2.Location = new System.Drawing.Point(211, 200);
            this.IDH_WDT2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WDT2.Multiline = false;
            this.IDH_WDT2.Name = "IDH_WDT2";
            this.IDH_WDT2.ReadOnly = false;
            this.IDH_WDT2.Size = new System.Drawing.Size(54, 14);
            this.IDH_WDT2.TabIndex = 131;
            this.IDH_WDT2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WDT2.TextValue = "";
            // 
            // IDH_SER2
            // 
            this.IDH_SER2.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SER2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SER2.CornerRadius = 2;
            this.IDH_SER2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SER2.Enabled = false;
            this.IDH_SER2.EnabledColor = System.Drawing.Color.White;
            this.IDH_SER2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SER2.Location = new System.Drawing.Point(156, 200);
            this.IDH_SER2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SER2.Multiline = false;
            this.IDH_SER2.Name = "IDH_SER2";
            this.IDH_SER2.ReadOnly = false;
            this.IDH_SER2.Size = new System.Drawing.Size(54, 14);
            this.IDH_SER2.TabIndex = 130;
            this.IDH_SER2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SER2.TextValue = "";
            // 
            // IDH_WEI2
            // 
            this.IDH_WEI2.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WEI2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WEI2.CornerRadius = 2;
            this.IDH_WEI2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WEI2.EnabledColor = System.Drawing.Color.White;
            this.IDH_WEI2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WEI2.Location = new System.Drawing.Point(101, 200);
            this.IDH_WEI2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WEI2.Multiline = false;
            this.IDH_WEI2.Name = "IDH_WEI2";
            this.IDH_WEI2.ReadOnly = false;
            this.IDH_WEI2.Size = new System.Drawing.Size(54, 14);
            this.IDH_WEI2.TabIndex = 8;
            this.IDH_WEI2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_WEI2.TextValue = "";
            // 
            // IDH_WDT1
            // 
            this.IDH_WDT1.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WDT1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WDT1.CornerRadius = 2;
            this.IDH_WDT1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WDT1.Enabled = false;
            this.IDH_WDT1.EnabledColor = System.Drawing.Color.White;
            this.IDH_WDT1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WDT1.Location = new System.Drawing.Point(211, 185);
            this.IDH_WDT1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WDT1.Multiline = false;
            this.IDH_WDT1.Name = "IDH_WDT1";
            this.IDH_WDT1.ReadOnly = false;
            this.IDH_WDT1.Size = new System.Drawing.Size(54, 14);
            this.IDH_WDT1.TabIndex = 126;
            this.IDH_WDT1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WDT1.TextValue = "";
            // 
            // IDH_SER1
            // 
            this.IDH_SER1.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SER1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SER1.CornerRadius = 2;
            this.IDH_SER1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SER1.Enabled = false;
            this.IDH_SER1.EnabledColor = System.Drawing.Color.White;
            this.IDH_SER1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SER1.Location = new System.Drawing.Point(156, 185);
            this.IDH_SER1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SER1.Multiline = false;
            this.IDH_SER1.Name = "IDH_SER1";
            this.IDH_SER1.ReadOnly = false;
            this.IDH_SER1.Size = new System.Drawing.Size(54, 14);
            this.IDH_SER1.TabIndex = 125;
            this.IDH_SER1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SER1.TextValue = "";
            // 
            // IDH_WEI1
            // 
            this.IDH_WEI1.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WEI1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WEI1.CornerRadius = 2;
            this.IDH_WEI1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WEI1.EnabledColor = System.Drawing.Color.White;
            this.IDH_WEI1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WEI1.Location = new System.Drawing.Point(101, 185);
            this.IDH_WEI1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WEI1.Multiline = false;
            this.IDH_WEI1.Name = "IDH_WEI1";
            this.IDH_WEI1.ReadOnly = false;
            this.IDH_WEI1.Size = new System.Drawing.Size(54, 14);
            this.IDH_WEI1.TabIndex = 4;
            this.IDH_WEI1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_WEI1.TextValue = "";
            // 
            // IDH_TRLTar
            // 
            this.IDH_TRLTar.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TRLTar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TRLTar.CornerRadius = 2;
            this.IDH_TRLTar.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TRLTar.EnabledColor = System.Drawing.Color.White;
            this.IDH_TRLTar.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TRLTar.Location = new System.Drawing.Point(101, 170);
            this.IDH_TRLTar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TRLTar.Multiline = false;
            this.IDH_TRLTar.Name = "IDH_TRLTar";
            this.IDH_TRLTar.ReadOnly = false;
            this.IDH_TRLTar.Size = new System.Drawing.Size(54, 14);
            this.IDH_TRLTar.TabIndex = 124;
            this.IDH_TRLTar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_TRLTar.TextValue = "0.00";
            // 
            // IDH_TARWEI
            // 
            this.IDH_TARWEI.BackColor = System.Drawing.Color.Transparent;
            this.IDH_TARWEI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TARWEI.CornerRadius = 2;
            this.IDH_TARWEI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TARWEI.EnabledColor = System.Drawing.Color.White;
            this.IDH_TARWEI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TARWEI.Location = new System.Drawing.Point(101, 155);
            this.IDH_TARWEI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TARWEI.Multiline = false;
            this.IDH_TARWEI.Name = "IDH_TARWEI";
            this.IDH_TARWEI.ReadOnly = false;
            this.IDH_TARWEI.Size = new System.Drawing.Size(54, 14);
            this.IDH_TARWEI.TabIndex = 123;
            this.IDH_TARWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_TARWEI.TextValue = "0.00";
            // 
            // IDH_WDTB
            // 
            this.IDH_WDTB.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WDTB.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WDTB.CornerRadius = 2;
            this.IDH_WDTB.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WDTB.Enabled = false;
            this.IDH_WDTB.EnabledColor = System.Drawing.Color.White;
            this.IDH_WDTB.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WDTB.Location = new System.Drawing.Point(211, 140);
            this.IDH_WDTB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WDTB.Multiline = false;
            this.IDH_WDTB.Name = "IDH_WDTB";
            this.IDH_WDTB.ReadOnly = false;
            this.IDH_WDTB.Size = new System.Drawing.Size(54, 14);
            this.IDH_WDTB.TabIndex = 4;
            this.IDH_WDTB.TabStop = false;
            this.IDH_WDTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_WDTB.TextValue = "";
            // 
            // IDH_SERB
            // 
            this.IDH_SERB.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SERB.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SERB.CornerRadius = 2;
            this.IDH_SERB.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SERB.Enabled = false;
            this.IDH_SERB.EnabledColor = System.Drawing.Color.White;
            this.IDH_SERB.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SERB.Location = new System.Drawing.Point(156, 140);
            this.IDH_SERB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SERB.Multiline = false;
            this.IDH_SERB.Name = "IDH_SERB";
            this.IDH_SERB.ReadOnly = false;
            this.IDH_SERB.Size = new System.Drawing.Size(54, 14);
            this.IDH_SERB.TabIndex = 3;
            this.IDH_SERB.TabStop = false;
            this.IDH_SERB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_SERB.TextValue = "";
            // 
            // IDH_WEIB
            // 
            this.IDH_WEIB.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WEIB.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WEIB.CornerRadius = 2;
            this.IDH_WEIB.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WEIB.Enabled = false;
            this.IDH_WEIB.EnabledColor = System.Drawing.Color.White;
            this.IDH_WEIB.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WEIB.Location = new System.Drawing.Point(101, 140);
            this.IDH_WEIB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WEIB.Multiline = false;
            this.IDH_WEIB.Name = "IDH_WEIB";
            this.IDH_WEIB.ReadOnly = false;
            this.IDH_WEIB.Size = new System.Drawing.Size(54, 14);
            this.IDH_WEIB.TabIndex = 2;
            this.IDH_WEIB.TabStop = false;
            this.IDH_WEIB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.IDH_WEIB.TextValue = "";
            // 
            // IDH_WEIG
            // 
            this.IDH_WEIG.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WEIG.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WEIG.CornerRadius = 2;
            this.IDH_WEIG.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.IDH_WEIG.Enabled = false;
            this.IDH_WEIG.EnabledColor = System.Drawing.Color.White;
            this.IDH_WEIG.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WEIG.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDH_WEIG.Location = new System.Drawing.Point(101, 105);
            this.IDH_WEIG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WEIG.Multiline = true;
            this.IDH_WEIG.Name = "IDH_WEIG";
            this.IDH_WEIG.ReadOnly = false;
            this.IDH_WEIG.Size = new System.Drawing.Size(164, 34);
            this.IDH_WEIG.TabIndex = 1;
            this.IDH_WEIG.TabStop = false;
            this.IDH_WEIG.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.IDH_WEIG.TextValue = "Waiting";
            // 
            // IDH_WEIBRG
            // 
            this.IDH_WEIBRG.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WEIBRG.CornerRadius = 2;
            this.IDH_WEIBRG.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WEIBRG.DropDownHight = 100;
            this.IDH_WEIBRG.EnabledColor = System.Drawing.Color.White;
            this.IDH_WEIBRG.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WEIBRG.Location = new System.Drawing.Point(101, 90);
            this.IDH_WEIBRG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WEIBRG.Multiline = false;
            this.IDH_WEIBRG.Name = "IDH_WEIBRG";
            this.IDH_WEIBRG.SelectedIndex = -1;
            this.IDH_WEIBRG.SelectedItem = null;
            this.IDH_WEIBRG.Size = new System.Drawing.Size(164, 14);
            this.IDH_WEIBRG.TabIndex = 118;
            this.IDH_WEIBRG.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WEIBRG.TextBoxReadOnly = true;
            this.IDH_WEIBRG.TextValue = "";
            this.IDH_WEIBRG.SelectedIndexChanged += new System.EventHandler(this.IDH_WEIBRGSelectedIndexChanged);
            // 
            // oTPCarrier
            // 
            this.oTPCarrier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPCarrier.Controls.Add(this.IDH_LICREG);
            this.oTPCarrier.Controls.Add(this.label54);
            this.oTPCarrier.Controls.Add(this.IDH_PHONE);
            this.oTPCarrier.Controls.Add(this.label53);
            this.oTPCarrier.Controls.Add(this.IDH_ZIP);
            this.oTPCarrier.Controls.Add(this.label52);
            this.oTPCarrier.Controls.Add(this.IDH_CITY);
            this.oTPCarrier.Controls.Add(this.label51);
            this.oTPCarrier.Controls.Add(this.IDH_BLOCK);
            this.oTPCarrier.Controls.Add(this.label50);
            this.oTPCarrier.Controls.Add(this.IDH_WASAL);
            this.oTPCarrier.Controls.Add(this.IDH_ADDRES);
            this.oTPCarrier.Controls.Add(this.label49);
            this.oTPCarrier.Controls.Add(this.IDH_STREET);
            this.oTPCarrier.Controls.Add(this.label48);
            this.oTPCarrier.Controls.Add(this.IDH_CARREF);
            this.oTPCarrier.Controls.Add(this.label47);
            this.oTPCarrier.Controls.Add(this.IDH_CONTNM);
            this.oTPCarrier.Controls.Add(this.label46);
            this.oTPCarrier.Location = new System.Drawing.Point(4, 21);
            this.oTPCarrier.Name = "oTPCarrier";
            this.oTPCarrier.Padding = new System.Windows.Forms.Padding(3);
            this.oTPCarrier.Size = new System.Drawing.Size(822, 290);
            this.oTPCarrier.TabIndex = 1;
            this.oTPCarrier.Text = "Carrier";
            // 
            // IDH_LICREG
            // 
            this.IDH_LICREG.BackColor = System.Drawing.Color.Transparent;
            this.IDH_LICREG.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_LICREG.CornerRadius = 2;
            this.IDH_LICREG.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_LICREG.EnabledColor = System.Drawing.Color.White;
            this.IDH_LICREG.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_LICREG.Location = new System.Drawing.Point(130, 130);
            this.IDH_LICREG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_LICREG.Multiline = false;
            this.IDH_LICREG.Name = "IDH_LICREG";
            this.IDH_LICREG.ReadOnly = false;
            this.IDH_LICREG.Size = new System.Drawing.Size(155, 14);
            this.IDH_LICREG.TabIndex = 8;
            this.IDH_LICREG.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_LICREG.TextValue = "";
            // 
            // label54
            // 
            this.label54.Location = new System.Drawing.Point(4, 130);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(100, 14);
            this.label54.TabIndex = 107;
            this.label54.Text = "Waste Lic. Reg. No.";
            // 
            // IDH_PHONE
            // 
            this.IDH_PHONE.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PHONE.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PHONE.CornerRadius = 2;
            this.IDH_PHONE.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PHONE.EnabledColor = System.Drawing.Color.White;
            this.IDH_PHONE.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PHONE.Location = new System.Drawing.Point(130, 115);
            this.IDH_PHONE.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PHONE.Multiline = false;
            this.IDH_PHONE.Name = "IDH_PHONE";
            this.IDH_PHONE.ReadOnly = false;
            this.IDH_PHONE.Size = new System.Drawing.Size(155, 14);
            this.IDH_PHONE.TabIndex = 7;
            this.IDH_PHONE.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PHONE.TextValue = "";
            // 
            // label53
            // 
            this.label53.Location = new System.Drawing.Point(4, 115);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(100, 14);
            this.label53.TabIndex = 105;
            this.label53.Text = "Phone No.";
            // 
            // IDH_ZIP
            // 
            this.IDH_ZIP.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ZIP.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ZIP.CornerRadius = 2;
            this.IDH_ZIP.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ZIP.EnabledColor = System.Drawing.Color.White;
            this.IDH_ZIP.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ZIP.Location = new System.Drawing.Point(130, 100);
            this.IDH_ZIP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ZIP.Multiline = false;
            this.IDH_ZIP.Name = "IDH_ZIP";
            this.IDH_ZIP.ReadOnly = false;
            this.IDH_ZIP.Size = new System.Drawing.Size(155, 14);
            this.IDH_ZIP.TabIndex = 6;
            this.IDH_ZIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ZIP.TextValue = "";
            // 
            // label52
            // 
            this.label52.Location = new System.Drawing.Point(4, 100);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(100, 14);
            this.label52.TabIndex = 103;
            this.label52.Text = "Post Code";
            // 
            // IDH_CITY
            // 
            this.IDH_CITY.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CITY.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CITY.CornerRadius = 2;
            this.IDH_CITY.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CITY.EnabledColor = System.Drawing.Color.White;
            this.IDH_CITY.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CITY.Location = new System.Drawing.Point(130, 85);
            this.IDH_CITY.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CITY.Multiline = false;
            this.IDH_CITY.Name = "IDH_CITY";
            this.IDH_CITY.ReadOnly = false;
            this.IDH_CITY.Size = new System.Drawing.Size(155, 14);
            this.IDH_CITY.TabIndex = 5;
            this.IDH_CITY.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CITY.TextValue = "";
            // 
            // label51
            // 
            this.label51.Location = new System.Drawing.Point(4, 85);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(100, 14);
            this.label51.TabIndex = 101;
            this.label51.Text = "City";
            // 
            // IDH_BLOCK
            // 
            this.IDH_BLOCK.BackColor = System.Drawing.Color.Transparent;
            this.IDH_BLOCK.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BLOCK.CornerRadius = 2;
            this.IDH_BLOCK.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BLOCK.EnabledColor = System.Drawing.Color.White;
            this.IDH_BLOCK.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BLOCK.Location = new System.Drawing.Point(130, 70);
            this.IDH_BLOCK.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_BLOCK.Multiline = false;
            this.IDH_BLOCK.Name = "IDH_BLOCK";
            this.IDH_BLOCK.ReadOnly = false;
            this.IDH_BLOCK.Size = new System.Drawing.Size(155, 14);
            this.IDH_BLOCK.TabIndex = 4;
            this.IDH_BLOCK.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BLOCK.TextValue = "";
            // 
            // label50
            // 
            this.label50.Location = new System.Drawing.Point(4, 70);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(100, 14);
            this.label50.TabIndex = 99;
            this.label50.Text = "Block";
            // 
            // IDH_WASAL
            // 
            this.IDH_WASAL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_WASAL.Image")));
            this.IDH_WASAL.Location = new System.Drawing.Point(285, 40);
            this.IDH_WASAL.Name = "IDH_WASAL";
            this.IDH_WASAL.Size = new System.Drawing.Size(14, 14);
            this.IDH_WASAL.TabIndex = 98;
            this.IDH_WASAL.UseVisualStyleBackColor = true;
            this.IDH_WASAL.Click += new System.EventHandler(this.AddressSearchClick);
            // 
            // IDH_ADDRES
            // 
            this.IDH_ADDRES.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADDRES.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADDRES.CornerRadius = 2;
            this.IDH_ADDRES.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADDRES.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADDRES.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADDRES.Location = new System.Drawing.Point(130, 40);
            this.IDH_ADDRES.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ADDRES.Multiline = false;
            this.IDH_ADDRES.Name = "IDH_ADDRES";
            this.IDH_ADDRES.ReadOnly = false;
            this.IDH_ADDRES.Size = new System.Drawing.Size(155, 14);
            this.IDH_ADDRES.TabIndex = 2;
            this.IDH_ADDRES.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ADDRES.TextValue = "";
            // 
            // label49
            // 
            this.label49.Location = new System.Drawing.Point(4, 40);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(100, 14);
            this.label49.TabIndex = 96;
            this.label49.Text = "Address";
            // 
            // IDH_STREET
            // 
            this.IDH_STREET.BackColor = System.Drawing.Color.Transparent;
            this.IDH_STREET.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_STREET.CornerRadius = 2;
            this.IDH_STREET.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_STREET.EnabledColor = System.Drawing.Color.White;
            this.IDH_STREET.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_STREET.Location = new System.Drawing.Point(130, 55);
            this.IDH_STREET.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_STREET.Multiline = false;
            this.IDH_STREET.Name = "IDH_STREET";
            this.IDH_STREET.ReadOnly = false;
            this.IDH_STREET.Size = new System.Drawing.Size(155, 14);
            this.IDH_STREET.TabIndex = 3;
            this.IDH_STREET.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_STREET.TextValue = "";
            // 
            // label48
            // 
            this.label48.Location = new System.Drawing.Point(4, 55);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(100, 14);
            this.label48.TabIndex = 86;
            this.label48.Text = "Street";
            // 
            // IDH_CARREF
            // 
            this.IDH_CARREF.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CARREF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CARREF.CornerRadius = 2;
            this.IDH_CARREF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CARREF.EnabledColor = System.Drawing.Color.White;
            this.IDH_CARREF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CARREF.Location = new System.Drawing.Point(130, 25);
            this.IDH_CARREF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CARREF.Multiline = false;
            this.IDH_CARREF.Name = "IDH_CARREF";
            this.IDH_CARREF.ReadOnly = false;
            this.IDH_CARREF.Size = new System.Drawing.Size(155, 14);
            this.IDH_CARREF.TabIndex = 1;
            this.IDH_CARREF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CARREF.TextValue = "";
            // 
            // label47
            // 
            this.label47.Location = new System.Drawing.Point(4, 25);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(123, 14);
            this.label47.TabIndex = 2;
            this.label47.Text = "Waste Carrier Ref. No.";
            // 
            // IDH_CONTNM
            // 
            this.IDH_CONTNM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CONTNM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CONTNM.CornerRadius = 2;
            this.IDH_CONTNM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CONTNM.EnabledColor = System.Drawing.Color.White;
            this.IDH_CONTNM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CONTNM.Location = new System.Drawing.Point(130, 10);
            this.IDH_CONTNM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CONTNM.Multiline = false;
            this.IDH_CONTNM.Name = "IDH_CONTNM";
            this.IDH_CONTNM.ReadOnly = false;
            this.IDH_CONTNM.Size = new System.Drawing.Size(155, 14);
            this.IDH_CONTNM.TabIndex = 0;
            this.IDH_CONTNM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CONTNM.TextValue = "";
            // 
            // label46
            // 
            this.label46.Location = new System.Drawing.Point(4, 10);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(100, 14);
            this.label46.TabIndex = 0;
            this.label46.Text = "Contact Person";
            // 
            // oTPCustomer
            // 
            this.oTPCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPCustomer.Controls.Add(this.IDH_ORIGLU2);
            this.oTPCustomer.Controls.Add(this.IDH_ORIGIN2);
            this.oTPCustomer.Controls.Add(this.label110);
            this.oTPCustomer.Controls.Add(this.label43);
            this.oTPCustomer.Controls.Add(this.label44);
            this.oTPCustomer.Controls.Add(this.label45);
            this.oTPCustomer.Controls.Add(this.IDH_SLCNO);
            this.oTPCustomer.Controls.Add(this.IDH_PCD);
            this.oTPCustomer.Controls.Add(this.IDH_SteId);
            this.oTPCustomer.Controls.Add(this.IDH_ROUTL);
            this.oTPCustomer.Controls.Add(this.label42);
            this.oTPCustomer.Controls.Add(this.IDH_SEQ);
            this.oTPCustomer.Controls.Add(this.IDH_ROUTE);
            this.oTPCustomer.Controls.Add(this.label41);
            this.oTPCustomer.Controls.Add(this.IDH_CNA);
            this.oTPCustomer.Controls.Add(this.IDH_CUSAL);
            this.oTPCustomer.Controls.Add(this.label40);
            this.oTPCustomer.Controls.Add(this.IDH_CUSLIC);
            this.oTPCustomer.Controls.Add(this.IDH_SITETL);
            this.oTPCustomer.Controls.Add(this.label39);
            this.oTPCustomer.Controls.Add(this.IDH_CUSPHO);
            this.oTPCustomer.Controls.Add(this.IDH_CUSPOS);
            this.oTPCustomer.Controls.Add(this.IDH_COUNTY);
            this.oTPCustomer.Controls.Add(this.IDH_CUSCIT);
            this.oTPCustomer.Controls.Add(this.IDH_CUSBLO);
            this.oTPCustomer.Controls.Add(this.IDH_CUSSTR);
            this.oTPCustomer.Controls.Add(this.IDH_CUSADD);
            this.oTPCustomer.Controls.Add(this.IDH_CUSCON);
            this.oTPCustomer.Controls.Add(this.label38);
            this.oTPCustomer.Controls.Add(this.label37);
            this.oTPCustomer.Controls.Add(this.label36);
            this.oTPCustomer.Controls.Add(this.label35);
            this.oTPCustomer.Controls.Add(this.label34);
            this.oTPCustomer.Controls.Add(this.label33);
            this.oTPCustomer.Controls.Add(this.label32);
            this.oTPCustomer.Controls.Add(this.label31);
            this.oTPCustomer.Location = new System.Drawing.Point(4, 21);
            this.oTPCustomer.Name = "oTPCustomer";
            this.oTPCustomer.Size = new System.Drawing.Size(822, 290);
            this.oTPCustomer.TabIndex = 2;
            this.oTPCustomer.Text = "Customer";
            // 
            // IDH_ORIGLU2
            // 
            this.IDH_ORIGLU2.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ORIGLU2.Image")));
            this.IDH_ORIGLU2.Location = new System.Drawing.Point(285, 190);
            this.IDH_ORIGLU2.Name = "IDH_ORIGLU2";
            this.IDH_ORIGLU2.Size = new System.Drawing.Size(14, 14);
            this.IDH_ORIGLU2.TabIndex = 117;
            this.IDH_ORIGLU2.UseVisualStyleBackColor = true;
            this.IDH_ORIGLU2.Click += new System.EventHandler(this.IDH_ORLKClick);
            // 
            // IDH_ORIGIN2
            // 
            this.IDH_ORIGIN2.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ORIGIN2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORIGIN2.CornerRadius = 2;
            this.IDH_ORIGIN2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORIGIN2.EnabledColor = System.Drawing.Color.White;
            this.IDH_ORIGIN2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORIGIN2.Location = new System.Drawing.Point(130, 190);
            this.IDH_ORIGIN2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ORIGIN2.Multiline = false;
            this.IDH_ORIGIN2.Name = "IDH_ORIGIN2";
            this.IDH_ORIGIN2.ReadOnly = false;
            this.IDH_ORIGIN2.Size = new System.Drawing.Size(155, 14);
            this.IDH_ORIGIN2.TabIndex = 115;
            this.IDH_ORIGIN2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ORIGIN2.TextValue = "";
            // 
            // label110
            // 
            this.label110.Location = new System.Drawing.Point(4, 190);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(100, 14);
            this.label110.TabIndex = 116;
            this.label110.Text = "Origin";
            // 
            // label43
            // 
            this.label43.Location = new System.Drawing.Point(309, 190);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(100, 14);
            this.label43.TabIndex = 107;
            this.label43.Text = "Sequence";
            // 
            // label44
            // 
            this.label44.Location = new System.Drawing.Point(309, 175);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(100, 14);
            this.label44.TabIndex = 106;
            this.label44.Text = "Route";
            // 
            // label45
            // 
            this.label45.Location = new System.Drawing.Point(309, 160);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(100, 14);
            this.label45.TabIndex = 105;
            this.label45.Text = "Site Id";
            // 
            // IDH_SLCNO
            // 
            this.IDH_SLCNO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SLCNO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SLCNO.CornerRadius = 2;
            this.IDH_SLCNO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SLCNO.Enabled = false;
            this.IDH_SLCNO.EnabledColor = System.Drawing.Color.White;
            this.IDH_SLCNO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SLCNO.Location = new System.Drawing.Point(417, 190);
            this.IDH_SLCNO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SLCNO.Multiline = false;
            this.IDH_SLCNO.Name = "IDH_SLCNO";
            this.IDH_SLCNO.ReadOnly = false;
            this.IDH_SLCNO.Size = new System.Drawing.Size(244, 14);
            this.IDH_SLCNO.TabIndex = 14;
            this.IDH_SLCNO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SLCNO.TextValue = "";
            // 
            // IDH_PCD
            // 
            this.IDH_PCD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PCD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PCD.CornerRadius = 2;
            this.IDH_PCD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PCD.Enabled = false;
            this.IDH_PCD.EnabledColor = System.Drawing.Color.White;
            this.IDH_PCD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PCD.Location = new System.Drawing.Point(417, 175);
            this.IDH_PCD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PCD.Multiline = false;
            this.IDH_PCD.Name = "IDH_PCD";
            this.IDH_PCD.ReadOnly = false;
            this.IDH_PCD.Size = new System.Drawing.Size(244, 14);
            this.IDH_PCD.TabIndex = 12;
            this.IDH_PCD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PCD.TextValue = "";
            // 
            // IDH_SteId
            // 
            this.IDH_SteId.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SteId.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SteId.CornerRadius = 2;
            this.IDH_SteId.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SteId.Enabled = false;
            this.IDH_SteId.EnabledColor = System.Drawing.Color.White;
            this.IDH_SteId.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SteId.Location = new System.Drawing.Point(417, 160);
            this.IDH_SteId.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SteId.Multiline = false;
            this.IDH_SteId.Name = "IDH_SteId";
            this.IDH_SteId.ReadOnly = false;
            this.IDH_SteId.Size = new System.Drawing.Size(244, 14);
            this.IDH_SteId.TabIndex = 10;
            this.IDH_SteId.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SteId.TextValue = "";
            // 
            // IDH_ROUTL
            // 
            this.IDH_ROUTL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ROUTL.Image")));
            this.IDH_ROUTL.Location = new System.Drawing.Point(285, 160);
            this.IDH_ROUTL.Name = "IDH_ROUTL";
            this.IDH_ROUTL.Size = new System.Drawing.Size(14, 14);
            this.IDH_ROUTL.TabIndex = 101;
            this.IDH_ROUTL.UseVisualStyleBackColor = true;
            // 
            // label42
            // 
            this.label42.Location = new System.Drawing.Point(4, 175);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(100, 14);
            this.label42.TabIndex = 100;
            this.label42.Text = "Sequence";
            // 
            // IDH_SEQ
            // 
            this.IDH_SEQ.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SEQ.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SEQ.CornerRadius = 2;
            this.IDH_SEQ.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SEQ.Enabled = false;
            this.IDH_SEQ.EnabledColor = System.Drawing.Color.White;
            this.IDH_SEQ.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SEQ.Location = new System.Drawing.Point(130, 175);
            this.IDH_SEQ.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SEQ.Multiline = false;
            this.IDH_SEQ.Name = "IDH_SEQ";
            this.IDH_SEQ.ReadOnly = false;
            this.IDH_SEQ.Size = new System.Drawing.Size(155, 14);
            this.IDH_SEQ.TabIndex = 13;
            this.IDH_SEQ.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SEQ.TextValue = "";
            // 
            // IDH_ROUTE
            // 
            this.IDH_ROUTE.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ROUTE.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ROUTE.CornerRadius = 2;
            this.IDH_ROUTE.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ROUTE.Enabled = false;
            this.IDH_ROUTE.EnabledColor = System.Drawing.Color.White;
            this.IDH_ROUTE.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ROUTE.Location = new System.Drawing.Point(130, 160);
            this.IDH_ROUTE.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ROUTE.Multiline = false;
            this.IDH_ROUTE.Name = "IDH_ROUTE";
            this.IDH_ROUTE.ReadOnly = false;
            this.IDH_ROUTE.Size = new System.Drawing.Size(155, 14);
            this.IDH_ROUTE.TabIndex = 11;
            this.IDH_ROUTE.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ROUTE.TextValue = "";
            // 
            // label41
            // 
            this.label41.Location = new System.Drawing.Point(4, 160);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(100, 14);
            this.label41.TabIndex = 97;
            this.label41.Text = "Route";
            // 
            // IDH_CNA
            // 
            this.IDH_CNA.Location = new System.Drawing.Point(417, 35);
            this.IDH_CNA.Name = "IDH_CNA";
            this.IDH_CNA.Size = new System.Drawing.Size(135, 23);
            this.IDH_CNA.TabIndex = 96;
            this.IDH_CNA.Text = "Create &New Address";
            this.IDH_CNA.UseVisualStyleBackColor = true;
            // 
            // IDH_CUSAL
            // 
            this.IDH_CUSAL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_CUSAL.Image")));
            this.IDH_CUSAL.Location = new System.Drawing.Point(285, 25);
            this.IDH_CUSAL.Name = "IDH_CUSAL";
            this.IDH_CUSAL.Size = new System.Drawing.Size(14, 14);
            this.IDH_CUSAL.TabIndex = 95;
            this.IDH_CUSAL.UseVisualStyleBackColor = true;
            this.IDH_CUSAL.Click += new System.EventHandler(this.AddressSearchClick);
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(4, 145);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(100, 14);
            this.label40.TabIndex = 94;
            this.label40.Text = "Waste Lic. Reg. No.";
            // 
            // IDH_CUSLIC
            // 
            this.IDH_CUSLIC.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSLIC.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSLIC.CornerRadius = 2;
            this.IDH_CUSLIC.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSLIC.Enabled = false;
            this.IDH_CUSLIC.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSLIC.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSLIC.Location = new System.Drawing.Point(130, 145);
            this.IDH_CUSLIC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSLIC.Multiline = false;
            this.IDH_CUSLIC.Name = "IDH_CUSLIC";
            this.IDH_CUSLIC.ReadOnly = false;
            this.IDH_CUSLIC.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSLIC.TabIndex = 9;
            this.IDH_CUSLIC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSLIC.TextValue = "";
            // 
            // IDH_SITETL
            // 
            this.IDH_SITETL.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITETL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITETL.CornerRadius = 2;
            this.IDH_SITETL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITETL.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITETL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITETL.Location = new System.Drawing.Point(130, 130);
            this.IDH_SITETL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITETL.Multiline = false;
            this.IDH_SITETL.Name = "IDH_SITETL";
            this.IDH_SITETL.ReadOnly = false;
            this.IDH_SITETL.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITETL.TabIndex = 8;
            this.IDH_SITETL.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITETL.TextValue = "";
            // 
            // label39
            // 
            this.label39.Location = new System.Drawing.Point(4, 130);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(100, 14);
            this.label39.TabIndex = 91;
            this.label39.Text = "Site Phone No.";
            // 
            // IDH_CUSPHO
            // 
            this.IDH_CUSPHO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSPHO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSPHO.CornerRadius = 2;
            this.IDH_CUSPHO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSPHO.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSPHO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSPHO.Location = new System.Drawing.Point(130, 115);
            this.IDH_CUSPHO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSPHO.Multiline = false;
            this.IDH_CUSPHO.Name = "IDH_CUSPHO";
            this.IDH_CUSPHO.ReadOnly = false;
            this.IDH_CUSPHO.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSPHO.TabIndex = 7;
            this.IDH_CUSPHO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSPHO.TextValue = "";
            // 
            // IDH_CUSPOS
            // 
            this.IDH_CUSPOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSPOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSPOS.CornerRadius = 2;
            this.IDH_CUSPOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSPOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSPOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSPOS.Location = new System.Drawing.Point(130, 100);
            this.IDH_CUSPOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSPOS.Multiline = false;
            this.IDH_CUSPOS.Name = "IDH_CUSPOS";
            this.IDH_CUSPOS.ReadOnly = false;
            this.IDH_CUSPOS.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSPOS.TabIndex = 6;
            this.IDH_CUSPOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSPOS.TextValue = "";
            // 
            // IDH_COUNTY
            // 
            this.IDH_COUNTY.BackColor = System.Drawing.Color.Transparent;
            this.IDH_COUNTY.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_COUNTY.CornerRadius = 2;
            this.IDH_COUNTY.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_COUNTY.EnabledColor = System.Drawing.Color.White;
            this.IDH_COUNTY.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_COUNTY.Location = new System.Drawing.Point(130, 85);
            this.IDH_COUNTY.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_COUNTY.Multiline = false;
            this.IDH_COUNTY.Name = "IDH_COUNTY";
            this.IDH_COUNTY.ReadOnly = false;
            this.IDH_COUNTY.Size = new System.Drawing.Size(155, 14);
            this.IDH_COUNTY.TabIndex = 5;
            this.IDH_COUNTY.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_COUNTY.TextValue = "";
            // 
            // IDH_CUSCIT
            // 
            this.IDH_CUSCIT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSCIT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSCIT.CornerRadius = 2;
            this.IDH_CUSCIT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSCIT.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSCIT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSCIT.Location = new System.Drawing.Point(130, 70);
            this.IDH_CUSCIT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSCIT.Multiline = false;
            this.IDH_CUSCIT.Name = "IDH_CUSCIT";
            this.IDH_CUSCIT.ReadOnly = false;
            this.IDH_CUSCIT.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSCIT.TabIndex = 4;
            this.IDH_CUSCIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSCIT.TextValue = "";
            // 
            // IDH_CUSBLO
            // 
            this.IDH_CUSBLO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSBLO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSBLO.CornerRadius = 2;
            this.IDH_CUSBLO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSBLO.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSBLO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSBLO.Location = new System.Drawing.Point(130, 55);
            this.IDH_CUSBLO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSBLO.Multiline = false;
            this.IDH_CUSBLO.Name = "IDH_CUSBLO";
            this.IDH_CUSBLO.ReadOnly = false;
            this.IDH_CUSBLO.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSBLO.TabIndex = 3;
            this.IDH_CUSBLO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSBLO.TextValue = "";
            // 
            // IDH_CUSSTR
            // 
            this.IDH_CUSSTR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSSTR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSSTR.CornerRadius = 2;
            this.IDH_CUSSTR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSSTR.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSSTR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSSTR.Location = new System.Drawing.Point(130, 40);
            this.IDH_CUSSTR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSSTR.Multiline = false;
            this.IDH_CUSSTR.Name = "IDH_CUSSTR";
            this.IDH_CUSSTR.ReadOnly = false;
            this.IDH_CUSSTR.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSSTR.TabIndex = 2;
            this.IDH_CUSSTR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSSTR.TextValue = "";
            // 
            // IDH_CUSADD
            // 
            this.IDH_CUSADD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSADD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSADD.CornerRadius = 2;
            this.IDH_CUSADD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSADD.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSADD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSADD.Location = new System.Drawing.Point(130, 25);
            this.IDH_CUSADD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSADD.Multiline = false;
            this.IDH_CUSADD.Name = "IDH_CUSADD";
            this.IDH_CUSADD.ReadOnly = false;
            this.IDH_CUSADD.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSADD.TabIndex = 1;
            this.IDH_CUSADD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSADD.TextValue = "";
            // 
            // IDH_CUSCON
            // 
            this.IDH_CUSCON.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSCON.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSCON.CornerRadius = 2;
            this.IDH_CUSCON.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSCON.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSCON.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSCON.Location = new System.Drawing.Point(130, 10);
            this.IDH_CUSCON.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSCON.Multiline = false;
            this.IDH_CUSCON.Name = "IDH_CUSCON";
            this.IDH_CUSCON.ReadOnly = false;
            this.IDH_CUSCON.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSCON.TabIndex = 0;
            this.IDH_CUSCON.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSCON.TextValue = "";
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(4, 115);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(100, 14);
            this.label38.TabIndex = 82;
            this.label38.Text = "Phone No.";
            // 
            // label37
            // 
            this.label37.Location = new System.Drawing.Point(4, 100);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(100, 14);
            this.label37.TabIndex = 81;
            this.label37.Text = "Post Code";
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(4, 85);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(100, 14);
            this.label36.TabIndex = 80;
            this.label36.Text = "County";
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(4, 70);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(100, 14);
            this.label35.TabIndex = 79;
            this.label35.Text = "City";
            // 
            // label34
            // 
            this.label34.Location = new System.Drawing.Point(4, 55);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(100, 14);
            this.label34.TabIndex = 78;
            this.label34.Text = "Block";
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(4, 40);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(100, 14);
            this.label33.TabIndex = 77;
            this.label33.Text = "Street";
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(4, 26);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(100, 14);
            this.label32.TabIndex = 76;
            this.label32.Text = "Address";
            // 
            // label31
            // 
            this.label31.Location = new System.Drawing.Point(4, 10);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(100, 14);
            this.label31.TabIndex = 75;
            this.label31.Text = "Contact Person";
            // 
            // oTPProducer
            // 
            this.oTPProducer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPProducer.Controls.Add(this.IDH_PROAL);
            this.oTPProducer.Controls.Add(this.IDH_ORIGLU);
            this.oTPProducer.Controls.Add(this.IDH_PRDL);
            this.oTPProducer.Controls.Add(this.IDH_ORIGIN);
            this.oTPProducer.Controls.Add(this.label65);
            this.oTPProducer.Controls.Add(this.IDH_PRDPHO);
            this.oTPProducer.Controls.Add(this.IDH_PRDPOS);
            this.oTPProducer.Controls.Add(this.IDH_PRDCIT);
            this.oTPProducer.Controls.Add(this.label62);
            this.oTPProducer.Controls.Add(this.label63);
            this.oTPProducer.Controls.Add(this.label64);
            this.oTPProducer.Controls.Add(this.IDH_PRDBLO);
            this.oTPProducer.Controls.Add(this.IDH_PRDSTR);
            this.oTPProducer.Controls.Add(this.IDH_PRDADD);
            this.oTPProducer.Controls.Add(this.IDH_PRDCRF);
            this.oTPProducer.Controls.Add(this.IDH_PRDCON);
            this.oTPProducer.Controls.Add(this.IDH_WNAM);
            this.oTPProducer.Controls.Add(this.IDH_WPRODU);
            this.oTPProducer.Controls.Add(this.label55);
            this.oTPProducer.Controls.Add(this.label56);
            this.oTPProducer.Controls.Add(this.label57);
            this.oTPProducer.Controls.Add(this.label58);
            this.oTPProducer.Controls.Add(this.label59);
            this.oTPProducer.Controls.Add(this.label60);
            this.oTPProducer.Controls.Add(this.label61);
            this.oTPProducer.Location = new System.Drawing.Point(4, 21);
            this.oTPProducer.Name = "oTPProducer";
            this.oTPProducer.Size = new System.Drawing.Size(822, 290);
            this.oTPProducer.TabIndex = 3;
            this.oTPProducer.Text = "Producer";
            // 
            // IDH_PROAL
            // 
            this.IDH_PROAL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_PROAL.Image")));
            this.IDH_PROAL.Location = new System.Drawing.Point(285, 70);
            this.IDH_PROAL.Name = "IDH_PROAL";
            this.IDH_PROAL.Size = new System.Drawing.Size(14, 14);
            this.IDH_PROAL.TabIndex = 115;
            this.IDH_PROAL.UseVisualStyleBackColor = true;
            this.IDH_PROAL.Click += new System.EventHandler(this.AddressSearchClick);
            // 
            // IDH_ORIGLU
            // 
            this.IDH_ORIGLU.Image = ((System.Drawing.Image)(resources.GetObject("IDH_ORIGLU.Image")));
            this.IDH_ORIGLU.Location = new System.Drawing.Point(285, 160);
            this.IDH_ORIGLU.Name = "IDH_ORIGLU";
            this.IDH_ORIGLU.Size = new System.Drawing.Size(14, 14);
            this.IDH_ORIGLU.TabIndex = 114;
            this.IDH_ORIGLU.UseVisualStyleBackColor = true;
            this.IDH_ORIGLU.Click += new System.EventHandler(this.IDH_ORLKClick);
            // 
            // IDH_PRDL
            // 
            this.IDH_PRDL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_PRDL.Image")));
            this.IDH_PRDL.Location = new System.Drawing.Point(285, 10);
            this.IDH_PRDL.Name = "IDH_PRDL";
            this.IDH_PRDL.Size = new System.Drawing.Size(14, 14);
            this.IDH_PRDL.TabIndex = 113;
            this.IDH_PRDL.UseVisualStyleBackColor = true;
            this.IDH_PRDL.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_ORIGIN
            // 
            this.IDH_ORIGIN.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ORIGIN.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ORIGIN.CornerRadius = 2;
            this.IDH_ORIGIN.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ORIGIN.EnabledColor = System.Drawing.Color.White;
            this.IDH_ORIGIN.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ORIGIN.Location = new System.Drawing.Point(130, 160);
            this.IDH_ORIGIN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ORIGIN.Multiline = false;
            this.IDH_ORIGIN.Name = "IDH_ORIGIN";
            this.IDH_ORIGIN.ReadOnly = false;
            this.IDH_ORIGIN.Size = new System.Drawing.Size(155, 14);
            this.IDH_ORIGIN.TabIndex = 10;
            this.IDH_ORIGIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ORIGIN.TextValue = "";
            // 
            // label65
            // 
            this.label65.Location = new System.Drawing.Point(4, 160);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(100, 14);
            this.label65.TabIndex = 111;
            this.label65.Text = "Origin";
            // 
            // IDH_PRDPHO
            // 
            this.IDH_PRDPHO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDPHO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDPHO.CornerRadius = 2;
            this.IDH_PRDPHO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDPHO.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDPHO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDPHO.Location = new System.Drawing.Point(130, 145);
            this.IDH_PRDPHO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDPHO.Multiline = false;
            this.IDH_PRDPHO.Name = "IDH_PRDPHO";
            this.IDH_PRDPHO.ReadOnly = false;
            this.IDH_PRDPHO.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDPHO.TabIndex = 9;
            this.IDH_PRDPHO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDPHO.TextValue = "";
            // 
            // IDH_PRDPOS
            // 
            this.IDH_PRDPOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDPOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDPOS.CornerRadius = 2;
            this.IDH_PRDPOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDPOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDPOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDPOS.Location = new System.Drawing.Point(130, 130);
            this.IDH_PRDPOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDPOS.Multiline = false;
            this.IDH_PRDPOS.Name = "IDH_PRDPOS";
            this.IDH_PRDPOS.ReadOnly = false;
            this.IDH_PRDPOS.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDPOS.TabIndex = 8;
            this.IDH_PRDPOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDPOS.TextValue = "";
            // 
            // IDH_PRDCIT
            // 
            this.IDH_PRDCIT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDCIT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDCIT.CornerRadius = 2;
            this.IDH_PRDCIT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDCIT.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDCIT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDCIT.Location = new System.Drawing.Point(130, 115);
            this.IDH_PRDCIT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDCIT.Multiline = false;
            this.IDH_PRDCIT.Name = "IDH_PRDCIT";
            this.IDH_PRDCIT.ReadOnly = false;
            this.IDH_PRDCIT.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDCIT.TabIndex = 7;
            this.IDH_PRDCIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDCIT.TextValue = "";
            // 
            // label62
            // 
            this.label62.Location = new System.Drawing.Point(4, 145);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(100, 14);
            this.label62.TabIndex = 107;
            this.label62.Text = "Phone No.";
            // 
            // label63
            // 
            this.label63.Location = new System.Drawing.Point(4, 130);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(100, 14);
            this.label63.TabIndex = 106;
            this.label63.Text = "Post Code";
            // 
            // label64
            // 
            this.label64.Location = new System.Drawing.Point(4, 115);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(100, 14);
            this.label64.TabIndex = 105;
            this.label64.Text = "City";
            // 
            // IDH_PRDBLO
            // 
            this.IDH_PRDBLO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDBLO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDBLO.CornerRadius = 2;
            this.IDH_PRDBLO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDBLO.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDBLO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDBLO.Location = new System.Drawing.Point(130, 100);
            this.IDH_PRDBLO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDBLO.Multiline = false;
            this.IDH_PRDBLO.Name = "IDH_PRDBLO";
            this.IDH_PRDBLO.ReadOnly = false;
            this.IDH_PRDBLO.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDBLO.TabIndex = 6;
            this.IDH_PRDBLO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDBLO.TextValue = "";
            // 
            // IDH_PRDSTR
            // 
            this.IDH_PRDSTR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDSTR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDSTR.CornerRadius = 2;
            this.IDH_PRDSTR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDSTR.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDSTR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDSTR.Location = new System.Drawing.Point(130, 85);
            this.IDH_PRDSTR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDSTR.Multiline = false;
            this.IDH_PRDSTR.Name = "IDH_PRDSTR";
            this.IDH_PRDSTR.ReadOnly = false;
            this.IDH_PRDSTR.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDSTR.TabIndex = 5;
            this.IDH_PRDSTR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDSTR.TextValue = "";
            // 
            // IDH_PRDADD
            // 
            this.IDH_PRDADD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDADD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDADD.CornerRadius = 2;
            this.IDH_PRDADD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDADD.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDADD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDADD.Location = new System.Drawing.Point(130, 70);
            this.IDH_PRDADD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDADD.Multiline = false;
            this.IDH_PRDADD.Name = "IDH_PRDADD";
            this.IDH_PRDADD.ReadOnly = false;
            this.IDH_PRDADD.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDADD.TabIndex = 4;
            this.IDH_PRDADD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDADD.TextValue = "";
            // 
            // IDH_PRDCRF
            // 
            this.IDH_PRDCRF.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDCRF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDCRF.CornerRadius = 2;
            this.IDH_PRDCRF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDCRF.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDCRF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDCRF.Location = new System.Drawing.Point(130, 55);
            this.IDH_PRDCRF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDCRF.Multiline = false;
            this.IDH_PRDCRF.Name = "IDH_PRDCRF";
            this.IDH_PRDCRF.ReadOnly = false;
            this.IDH_PRDCRF.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDCRF.TabIndex = 3;
            this.IDH_PRDCRF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDCRF.TextValue = "";
            // 
            // IDH_PRDCON
            // 
            this.IDH_PRDCON.BackColor = System.Drawing.Color.Transparent;
            this.IDH_PRDCON.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_PRDCON.CornerRadius = 2;
            this.IDH_PRDCON.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_PRDCON.EnabledColor = System.Drawing.Color.White;
            this.IDH_PRDCON.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_PRDCON.Location = new System.Drawing.Point(130, 40);
            this.IDH_PRDCON.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_PRDCON.Multiline = false;
            this.IDH_PRDCON.Name = "IDH_PRDCON";
            this.IDH_PRDCON.ReadOnly = false;
            this.IDH_PRDCON.Size = new System.Drawing.Size(155, 14);
            this.IDH_PRDCON.TabIndex = 2;
            this.IDH_PRDCON.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_PRDCON.TextValue = "";
            // 
            // IDH_WNAM
            // 
            this.IDH_WNAM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WNAM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WNAM.CornerRadius = 2;
            this.IDH_WNAM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WNAM.EnabledColor = System.Drawing.Color.White;
            this.IDH_WNAM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WNAM.Location = new System.Drawing.Point(130, 25);
            this.IDH_WNAM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WNAM.Multiline = false;
            this.IDH_WNAM.Name = "IDH_WNAM";
            this.IDH_WNAM.ReadOnly = false;
            this.IDH_WNAM.Size = new System.Drawing.Size(155, 14);
            this.IDH_WNAM.TabIndex = 1;
            this.IDH_WNAM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WNAM.TextValue = "";
            // 
            // IDH_WPRODU
            // 
            this.IDH_WPRODU.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WPRODU.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WPRODU.CornerRadius = 2;
            this.IDH_WPRODU.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WPRODU.EnabledColor = System.Drawing.Color.White;
            this.IDH_WPRODU.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WPRODU.Location = new System.Drawing.Point(130, 10);
            this.IDH_WPRODU.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WPRODU.Multiline = false;
            this.IDH_WPRODU.Name = "IDH_WPRODU";
            this.IDH_WPRODU.ReadOnly = false;
            this.IDH_WPRODU.Size = new System.Drawing.Size(155, 14);
            this.IDH_WPRODU.TabIndex = 0;
            this.IDH_WPRODU.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WPRODU.TextValue = "";
            // 
            // label55
            // 
            this.label55.Location = new System.Drawing.Point(4, 100);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(100, 14);
            this.label55.TabIndex = 97;
            this.label55.Text = "Block";
            // 
            // label56
            // 
            this.label56.Location = new System.Drawing.Point(4, 85);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(100, 14);
            this.label56.TabIndex = 96;
            this.label56.Text = "Street";
            // 
            // label57
            // 
            this.label57.Location = new System.Drawing.Point(4, 70);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(100, 14);
            this.label57.TabIndex = 95;
            this.label57.Text = "Address";
            // 
            // label58
            // 
            this.label58.Location = new System.Drawing.Point(4, 55);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(100, 14);
            this.label58.TabIndex = 94;
            this.label58.Text = "Producer Ref. No.";
            // 
            // label59
            // 
            this.label59.Location = new System.Drawing.Point(4, 40);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(100, 14);
            this.label59.TabIndex = 93;
            this.label59.Text = "Contact Person";
            // 
            // label60
            // 
            this.label60.Location = new System.Drawing.Point(4, 25);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(100, 14);
            this.label60.TabIndex = 92;
            this.label60.Text = "Name";
            // 
            // label61
            // 
            this.label61.Location = new System.Drawing.Point(4, 10);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(100, 14);
            this.label61.TabIndex = 91;
            this.label61.Text = "Producer";
            // 
            // oTPSite
            // 
            this.oTPSite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oTPSite.Controls.Add(this.IDH_SITAL);
            this.oTPSite.Controls.Add(this.IDH_SITL);
            this.oTPSite.Controls.Add(this.IDH_SITPHO);
            this.oTPSite.Controls.Add(this.IDH_SITPOS);
            this.oTPSite.Controls.Add(this.IDH_SITCIT);
            this.oTPSite.Controls.Add(this.label66);
            this.oTPSite.Controls.Add(this.label67);
            this.oTPSite.Controls.Add(this.label68);
            this.oTPSite.Controls.Add(this.IDH_SITBLO);
            this.oTPSite.Controls.Add(this.IDH_SITSTR);
            this.oTPSite.Controls.Add(this.IDH_SITADD);
            this.oTPSite.Controls.Add(this.IDH_SITCRF);
            this.oTPSite.Controls.Add(this.IDH_SITCON);
            this.oTPSite.Controls.Add(this.IDH_DISNAM);
            this.oTPSite.Controls.Add(this.IDH_DISSIT);
            this.oTPSite.Controls.Add(this.label69);
            this.oTPSite.Controls.Add(this.label70);
            this.oTPSite.Controls.Add(this.label71);
            this.oTPSite.Controls.Add(this.label72);
            this.oTPSite.Controls.Add(this.label73);
            this.oTPSite.Controls.Add(this.label74);
            this.oTPSite.Controls.Add(this.label75);
            this.oTPSite.Location = new System.Drawing.Point(4, 21);
            this.oTPSite.Name = "oTPSite";
            this.oTPSite.Size = new System.Drawing.Size(822, 290);
            this.oTPSite.TabIndex = 4;
            this.oTPSite.Text = "Disposal Site";
            // 
            // IDH_SITAL
            // 
            this.IDH_SITAL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_SITAL.Image")));
            this.IDH_SITAL.Location = new System.Drawing.Point(285, 70);
            this.IDH_SITAL.Name = "IDH_SITAL";
            this.IDH_SITAL.Size = new System.Drawing.Size(14, 14);
            this.IDH_SITAL.TabIndex = 137;
            this.IDH_SITAL.UseVisualStyleBackColor = true;
            this.IDH_SITAL.Click += new System.EventHandler(this.AddressSearchClick);
            // 
            // IDH_SITL
            // 
            this.IDH_SITL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_SITL.Image")));
            this.IDH_SITL.Location = new System.Drawing.Point(285, 10);
            this.IDH_SITL.Name = "IDH_SITL";
            this.IDH_SITL.Size = new System.Drawing.Size(14, 14);
            this.IDH_SITL.TabIndex = 136;
            this.IDH_SITL.UseVisualStyleBackColor = true;
            this.IDH_SITL.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_SITPHO
            // 
            this.IDH_SITPHO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITPHO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITPHO.CornerRadius = 2;
            this.IDH_SITPHO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITPHO.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITPHO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITPHO.Location = new System.Drawing.Point(130, 145);
            this.IDH_SITPHO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITPHO.Multiline = false;
            this.IDH_SITPHO.Name = "IDH_SITPHO";
            this.IDH_SITPHO.ReadOnly = false;
            this.IDH_SITPHO.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITPHO.TabIndex = 135;
            this.IDH_SITPHO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITPHO.TextValue = "";
            // 
            // IDH_SITPOS
            // 
            this.IDH_SITPOS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITPOS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITPOS.CornerRadius = 2;
            this.IDH_SITPOS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITPOS.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITPOS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITPOS.Location = new System.Drawing.Point(130, 130);
            this.IDH_SITPOS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITPOS.Multiline = false;
            this.IDH_SITPOS.Name = "IDH_SITPOS";
            this.IDH_SITPOS.ReadOnly = false;
            this.IDH_SITPOS.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITPOS.TabIndex = 134;
            this.IDH_SITPOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITPOS.TextValue = "";
            // 
            // IDH_SITCIT
            // 
            this.IDH_SITCIT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITCIT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITCIT.CornerRadius = 2;
            this.IDH_SITCIT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITCIT.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITCIT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITCIT.Location = new System.Drawing.Point(130, 115);
            this.IDH_SITCIT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITCIT.Multiline = false;
            this.IDH_SITCIT.Name = "IDH_SITCIT";
            this.IDH_SITCIT.ReadOnly = false;
            this.IDH_SITCIT.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITCIT.TabIndex = 133;
            this.IDH_SITCIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITCIT.TextValue = "";
            // 
            // label66
            // 
            this.label66.Location = new System.Drawing.Point(4, 145);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(100, 14);
            this.label66.TabIndex = 132;
            this.label66.Text = "Phone No.";
            // 
            // label67
            // 
            this.label67.Location = new System.Drawing.Point(4, 130);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(100, 14);
            this.label67.TabIndex = 131;
            this.label67.Text = "Post Code";
            // 
            // label68
            // 
            this.label68.Location = new System.Drawing.Point(4, 115);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(100, 14);
            this.label68.TabIndex = 130;
            this.label68.Text = "City";
            // 
            // IDH_SITBLO
            // 
            this.IDH_SITBLO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITBLO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITBLO.CornerRadius = 2;
            this.IDH_SITBLO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITBLO.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITBLO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITBLO.Location = new System.Drawing.Point(130, 100);
            this.IDH_SITBLO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITBLO.Multiline = false;
            this.IDH_SITBLO.Name = "IDH_SITBLO";
            this.IDH_SITBLO.ReadOnly = false;
            this.IDH_SITBLO.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITBLO.TabIndex = 129;
            this.IDH_SITBLO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITBLO.TextValue = "";
            // 
            // IDH_SITSTR
            // 
            this.IDH_SITSTR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITSTR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITSTR.CornerRadius = 2;
            this.IDH_SITSTR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITSTR.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITSTR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITSTR.Location = new System.Drawing.Point(130, 85);
            this.IDH_SITSTR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITSTR.Multiline = false;
            this.IDH_SITSTR.Name = "IDH_SITSTR";
            this.IDH_SITSTR.ReadOnly = false;
            this.IDH_SITSTR.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITSTR.TabIndex = 128;
            this.IDH_SITSTR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITSTR.TextValue = "";
            // 
            // IDH_SITADD
            // 
            this.IDH_SITADD.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITADD.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITADD.CornerRadius = 2;
            this.IDH_SITADD.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITADD.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITADD.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITADD.Location = new System.Drawing.Point(130, 70);
            this.IDH_SITADD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITADD.Multiline = false;
            this.IDH_SITADD.Name = "IDH_SITADD";
            this.IDH_SITADD.ReadOnly = false;
            this.IDH_SITADD.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITADD.TabIndex = 127;
            this.IDH_SITADD.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITADD.TextValue = "";
            // 
            // IDH_SITCRF
            // 
            this.IDH_SITCRF.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITCRF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITCRF.CornerRadius = 2;
            this.IDH_SITCRF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITCRF.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITCRF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITCRF.Location = new System.Drawing.Point(130, 55);
            this.IDH_SITCRF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITCRF.Multiline = false;
            this.IDH_SITCRF.Name = "IDH_SITCRF";
            this.IDH_SITCRF.ReadOnly = false;
            this.IDH_SITCRF.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITCRF.TabIndex = 126;
            this.IDH_SITCRF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITCRF.TextValue = "";
            // 
            // IDH_SITCON
            // 
            this.IDH_SITCON.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITCON.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITCON.CornerRadius = 2;
            this.IDH_SITCON.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITCON.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITCON.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITCON.Location = new System.Drawing.Point(130, 40);
            this.IDH_SITCON.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITCON.Multiline = false;
            this.IDH_SITCON.Name = "IDH_SITCON";
            this.IDH_SITCON.ReadOnly = false;
            this.IDH_SITCON.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITCON.TabIndex = 125;
            this.IDH_SITCON.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITCON.TextValue = "";
            // 
            // IDH_DISNAM
            // 
            this.IDH_DISNAM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DISNAM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DISNAM.CornerRadius = 2;
            this.IDH_DISNAM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DISNAM.EnabledColor = System.Drawing.Color.White;
            this.IDH_DISNAM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DISNAM.Location = new System.Drawing.Point(130, 25);
            this.IDH_DISNAM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DISNAM.Multiline = false;
            this.IDH_DISNAM.Name = "IDH_DISNAM";
            this.IDH_DISNAM.ReadOnly = false;
            this.IDH_DISNAM.Size = new System.Drawing.Size(155, 14);
            this.IDH_DISNAM.TabIndex = 124;
            this.IDH_DISNAM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DISNAM.TextValue = "";
            // 
            // IDH_DISSIT
            // 
            this.IDH_DISSIT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DISSIT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DISSIT.CornerRadius = 2;
            this.IDH_DISSIT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DISSIT.EnabledColor = System.Drawing.Color.White;
            this.IDH_DISSIT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DISSIT.Location = new System.Drawing.Point(130, 10);
            this.IDH_DISSIT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_DISSIT.Multiline = false;
            this.IDH_DISSIT.Name = "IDH_DISSIT";
            this.IDH_DISSIT.ReadOnly = false;
            this.IDH_DISSIT.Size = new System.Drawing.Size(155, 14);
            this.IDH_DISSIT.TabIndex = 123;
            this.IDH_DISSIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DISSIT.TextValue = "";
            // 
            // label69
            // 
            this.label69.Location = new System.Drawing.Point(4, 100);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(100, 14);
            this.label69.TabIndex = 122;
            this.label69.Text = "Block";
            // 
            // label70
            // 
            this.label70.Location = new System.Drawing.Point(4, 85);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(100, 14);
            this.label70.TabIndex = 121;
            this.label70.Text = "Street";
            // 
            // label71
            // 
            this.label71.Location = new System.Drawing.Point(4, 70);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(100, 14);
            this.label71.TabIndex = 120;
            this.label71.Text = "Address";
            // 
            // label72
            // 
            this.label72.Location = new System.Drawing.Point(4, 55);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(100, 14);
            this.label72.TabIndex = 119;
            this.label72.Text = "Producer Ref. No.";
            // 
            // label73
            // 
            this.label73.Location = new System.Drawing.Point(4, 40);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(100, 14);
            this.label73.TabIndex = 118;
            this.label73.Text = "Contact Person";
            // 
            // label74
            // 
            this.label74.Location = new System.Drawing.Point(4, 25);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(100, 14);
            this.label74.TabIndex = 117;
            this.label74.Text = "Name";
            // 
            // label75
            // 
            this.label75.Location = new System.Drawing.Point(4, 10);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(100, 14);
            this.label75.TabIndex = 116;
            this.label75.Text = "Disposal Site";
            // 
            // oAdditional
            // 
            this.oAdditional.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oAdditional.Controls.Add(this.label112);
            this.oAdditional.Controls.Add(this.label111);
            this.oAdditional.Controls.Add(this.IDH_ADDCH);
            this.oAdditional.Controls.Add(this.IDH_ADDCO);
            this.oAdditional.Controls.Add(this.oAddGrid);
            this.oAdditional.Location = new System.Drawing.Point(4, 21);
            this.oAdditional.Name = "oAdditional";
            this.oAdditional.Padding = new System.Windows.Forms.Padding(3);
            this.oAdditional.Size = new System.Drawing.Size(822, 290);
            this.oAdditional.TabIndex = 5;
            this.oAdditional.Text = "Additional";
            // 
            // label112
            // 
            this.label112.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label112.Location = new System.Drawing.Point(666, 315);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(100, 16);
            this.label112.TabIndex = 4;
            this.label112.Text = "Additional Charge";
            // 
            // label111
            // 
            this.label111.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label111.Location = new System.Drawing.Point(666, 296);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(100, 15);
            this.label111.TabIndex = 3;
            this.label111.Text = "Additional Cost";
            // 
            // IDH_ADDCH
            // 
            this.IDH_ADDCH.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_ADDCH.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADDCH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADDCH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ADDCH.CornerRadius = 2;
            this.IDH_ADDCH.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADDCH.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADDCH.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADDCH.Location = new System.Drawing.Point(766, 313);
            this.IDH_ADDCH.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ADDCH.Multiline = true;
            this.IDH_ADDCH.Name = "IDH_ADDCH";
            this.IDH_ADDCH.ReadOnly = true;
            this.IDH_ADDCH.Size = new System.Drawing.Size(50, 18);
            this.IDH_ADDCH.TabIndex = 2;
            this.IDH_ADDCH.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ADDCH.TextValue = "";
            // 
            // IDH_ADDCO
            // 
            this.IDH_ADDCO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_ADDCO.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ADDCO.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ADDCO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_ADDCO.CornerRadius = 2;
            this.IDH_ADDCO.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ADDCO.EnabledColor = System.Drawing.Color.White;
            this.IDH_ADDCO.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ADDCO.Location = new System.Drawing.Point(766, 293);
            this.IDH_ADDCO.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_ADDCO.Multiline = true;
            this.IDH_ADDCO.Name = "IDH_ADDCO";
            this.IDH_ADDCO.ReadOnly = true;
            this.IDH_ADDCO.Size = new System.Drawing.Size(50, 18);
            this.IDH_ADDCO.TabIndex = 1;
            this.IDH_ADDCO.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ADDCO.TextValue = "";
            // 
            // oAddGrid
            // 
            this.oAddGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.oAddGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.oAddGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oAddGrid.GrayReadOnly = false;
            this.oAddGrid.Location = new System.Drawing.Point(0, 0);
            this.oAddGrid.Name = "oAddGrid";
            this.oAddGrid.Size = new System.Drawing.Size(821, 223);
            this.oAddGrid.TabIndex = 0;
            // 
            // oDeductions
            // 
            this.oDeductions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.oDeductions.Controls.Add(this.label122);
            this.oDeductions.Controls.Add(this.label123);
            this.oDeductions.Controls.Add(this.IDH_DEDVAL);
            this.oDeductions.Controls.Add(this.IDH_DEDWEI);
            this.oDeductions.Controls.Add(this.oDeductionGrid);
            this.oDeductions.Location = new System.Drawing.Point(4, 21);
            this.oDeductions.Name = "oDeductions";
            this.oDeductions.Size = new System.Drawing.Size(822, 290);
            this.oDeductions.TabIndex = 6;
            this.oDeductions.Text = "Deductions";
            // 
            // label122
            // 
            this.label122.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label122.Location = new System.Drawing.Point(666, 315);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(100, 16);
            this.label122.TabIndex = 8;
            this.label122.Text = "Value Deduction";
            // 
            // label123
            // 
            this.label123.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label123.Location = new System.Drawing.Point(666, 296);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(100, 15);
            this.label123.TabIndex = 7;
            this.label123.Text = "Weight Deduction";
            // 
            // IDH_DEDVAL
            // 
            this.IDH_DEDVAL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_DEDVAL.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DEDVAL.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DEDVAL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DEDVAL.CornerRadius = 2;
            this.IDH_DEDVAL.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DEDVAL.EnabledColor = System.Drawing.Color.White;
            this.IDH_DEDVAL.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DEDVAL.Location = new System.Drawing.Point(766, 313);
            this.IDH_DEDVAL.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_DEDVAL.Multiline = true;
            this.IDH_DEDVAL.Name = "IDH_DEDVAL";
            this.IDH_DEDVAL.ReadOnly = true;
            this.IDH_DEDVAL.Size = new System.Drawing.Size(50, 18);
            this.IDH_DEDVAL.TabIndex = 6;
            this.IDH_DEDVAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DEDVAL.TextValue = "";
            // 
            // IDH_DEDWEI
            // 
            this.IDH_DEDWEI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_DEDWEI.BackColor = System.Drawing.Color.Transparent;
            this.IDH_DEDWEI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_DEDWEI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDH_DEDWEI.CornerRadius = 2;
            this.IDH_DEDWEI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_DEDWEI.EnabledColor = System.Drawing.Color.White;
            this.IDH_DEDWEI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_DEDWEI.Location = new System.Drawing.Point(766, 293);
            this.IDH_DEDWEI.Margin = new System.Windows.Forms.Padding(2);
            this.IDH_DEDWEI.Multiline = true;
            this.IDH_DEDWEI.Name = "IDH_DEDWEI";
            this.IDH_DEDWEI.ReadOnly = true;
            this.IDH_DEDWEI.Size = new System.Drawing.Size(50, 18);
            this.IDH_DEDWEI.TabIndex = 5;
            this.IDH_DEDWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_DEDWEI.TextValue = "";
            // 
            // oDeductionGrid
            // 
            this.oDeductionGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.oDeductionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oDeductionGrid.GrayReadOnly = false;
            this.oDeductionGrid.Location = new System.Drawing.Point(0, 0);
            this.oDeductionGrid.Name = "oDeductionGrid";
            this.oDeductionGrid.Size = new System.Drawing.Size(821, 211);
            this.oDeductionGrid.TabIndex = 0;
            // 
            // oPNavigation
            // 
            this.oPNavigation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.oPNavigation.CausesValidation = false;
            this.oPNavigation.Controls.Add(this.btFind);
            this.oPNavigation.Controls.Add(this.btAdd);
            this.oPNavigation.Controls.Add(this.btLast);
            this.oPNavigation.Controls.Add(this.btNext);
            this.oPNavigation.Controls.Add(this.btPrevious);
            this.oPNavigation.Controls.Add(this.btFirst);
            this.oPNavigation.Location = new System.Drawing.Point(590, 500);
            this.oPNavigation.Name = "oPNavigation";
            this.oPNavigation.Size = new System.Drawing.Size(205, 26);
            this.oPNavigation.TabIndex = 76;
            // 
            // btFind
            // 
            this.btFind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(235)))));
            this.btFind.CausesValidation = false;
            this.btFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btFind.Location = new System.Drawing.Point(3, 3);
            this.btFind.Margin = new System.Windows.Forms.Padding(0);
            this.btFind.Name = "btFind";
            this.btFind.Size = new System.Drawing.Size(28, 18);
            this.btFind.TabIndex = 904;
            this.btFind.Text = "F";
            this.btFind.UseVisualStyleBackColor = false;
            this.btFind.Click += new System.EventHandler(this.BtFindClick);
            // 
            // btAdd
            // 
            this.btAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(235)))));
            this.btAdd.CausesValidation = false;
            this.btAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd.Location = new System.Drawing.Point(36, 3);
            this.btAdd.Margin = new System.Windows.Forms.Padding(0);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(28, 18);
            this.btAdd.TabIndex = 905;
            this.btAdd.Text = "*";
            this.btAdd.UseVisualStyleBackColor = false;
            this.btAdd.Click += new System.EventHandler(this.BtAddClick);
            // 
            // btLast
            // 
            this.btLast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(235)))));
            this.btLast.CausesValidation = false;
            this.btLast.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLast.Location = new System.Drawing.Point(168, 3);
            this.btLast.Margin = new System.Windows.Forms.Padding(0);
            this.btLast.Name = "btLast";
            this.btLast.Size = new System.Drawing.Size(28, 18);
            this.btLast.TabIndex = 910;
            this.btLast.Text = ">>";
            this.btLast.UseVisualStyleBackColor = false;
            this.btLast.Click += new System.EventHandler(this.BtLastClick);
            // 
            // btNext
            // 
            this.btNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(235)))));
            this.btNext.CausesValidation = false;
            this.btNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btNext.Location = new System.Drawing.Point(135, 3);
            this.btNext.Margin = new System.Windows.Forms.Padding(0);
            this.btNext.Name = "btNext";
            this.btNext.Size = new System.Drawing.Size(28, 18);
            this.btNext.TabIndex = 908;
            this.btNext.Text = ">";
            this.btNext.UseVisualStyleBackColor = false;
            this.btNext.Click += new System.EventHandler(this.BtNextClick);
            // 
            // btPrevious
            // 
            this.btPrevious.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(235)))));
            this.btPrevious.CausesValidation = false;
            this.btPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPrevious.Location = new System.Drawing.Point(102, 3);
            this.btPrevious.Margin = new System.Windows.Forms.Padding(0);
            this.btPrevious.Name = "btPrevious";
            this.btPrevious.Size = new System.Drawing.Size(28, 18);
            this.btPrevious.TabIndex = 907;
            this.btPrevious.Text = "<";
            this.btPrevious.UseVisualStyleBackColor = false;
            this.btPrevious.Click += new System.EventHandler(this.BtPreviousClick);
            // 
            // btFirst
            // 
            this.btFirst.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(235)))));
            this.btFirst.CausesValidation = false;
            this.btFirst.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btFirst.Location = new System.Drawing.Point(69, 3);
            this.btFirst.Margin = new System.Windows.Forms.Padding(0);
            this.btFirst.Name = "btFirst";
            this.btFirst.Size = new System.Drawing.Size(28, 18);
            this.btFirst.TabIndex = 906;
            this.btFirst.Text = "<<";
            this.btFirst.UseVisualStyleBackColor = false;
            this.btFirst.Click += new System.EventHandler(this.BtFirstClick);
            // 
            // IDH_SPECIN
            // 
            this.IDH_SPECIN.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SPECIN.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SPECIN.CornerRadius = 2;
            this.IDH_SPECIN.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SPECIN.EnabledColor = System.Drawing.Color.White;
            this.IDH_SPECIN.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SPECIN.Location = new System.Drawing.Point(110, 126);
            this.IDH_SPECIN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SPECIN.Multiline = true;
            this.IDH_SPECIN.Name = "IDH_SPECIN";
            this.IDH_SPECIN.ReadOnly = false;
            this.IDH_SPECIN.Size = new System.Drawing.Size(431, 55);
            this.IDH_SPECIN.TabIndex = 169;
            this.IDH_SPECIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SPECIN.TextValue = "";
            // 
            // IDH_CUST
            // 
            this.IDH_CUST.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUST.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUST.CornerRadius = 2;
            this.IDH_CUST.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUST.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUST.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUST.Location = new System.Drawing.Point(386, 5);
            this.IDH_CUST.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUST.Multiline = false;
            this.IDH_CUST.Name = "IDH_CUST";
            this.IDH_CUST.ReadOnly = false;
            this.IDH_CUST.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUST.TabIndex = 2;
            this.IDH_CUST.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUST.TextValue = "Customer";
            // 
            // IDH_CUSTNM
            // 
            this.IDH_CUSTNM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSTNM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSTNM.CornerRadius = 2;
            this.IDH_CUSTNM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSTNM.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSTNM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSTNM.Location = new System.Drawing.Point(386, 20);
            this.IDH_CUSTNM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSTNM.Multiline = false;
            this.IDH_CUSTNM.Name = "IDH_CUSTNM";
            this.IDH_CUSTNM.ReadOnly = false;
            this.IDH_CUSTNM.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSTNM.TabIndex = 180;
            this.IDH_CUSTNM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSTNM.TextValue = "";
            // 
            // IDH_CUSCRF
            // 
            this.IDH_CUSCRF.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CUSCRF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CUSCRF.CornerRadius = 2;
            this.IDH_CUSCRF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CUSCRF.EnabledColor = System.Drawing.Color.White;
            this.IDH_CUSCRF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CUSCRF.Location = new System.Drawing.Point(386, 35);
            this.IDH_CUSCRF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CUSCRF.Multiline = false;
            this.IDH_CUSCRF.Name = "IDH_CUSCRF";
            this.IDH_CUSCRF.ReadOnly = false;
            this.IDH_CUSCRF.Size = new System.Drawing.Size(155, 14);
            this.IDH_CUSCRF.TabIndex = 181;
            this.IDH_CUSCRF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CUSCRF.TextValue = "";
            this.IDH_CUSCRF.Validated += new System.EventHandler(this.IDH_WRROW_Click);
            // 
            // IDH_ONCS
            // 
            this.IDH_ONCS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ONCS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ONCS.CornerRadius = 2;
            this.IDH_ONCS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ONCS.EnabledColor = System.Drawing.Color.White;
            this.IDH_ONCS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ONCS.Location = new System.Drawing.Point(386, 50);
            this.IDH_ONCS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ONCS.Multiline = false;
            this.IDH_ONCS.Name = "IDH_ONCS";
            this.IDH_ONCS.ReadOnly = false;
            this.IDH_ONCS.Size = new System.Drawing.Size(155, 14);
            this.IDH_ONCS.TabIndex = 182;
            this.IDH_ONCS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ONCS.TextValue = "";
            // 
            // IDH_USER
            // 
            this.IDH_USER.BackColor = System.Drawing.Color.Transparent;
            this.IDH_USER.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_USER.CornerRadius = 2;
            this.IDH_USER.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_USER.EnabledColor = System.Drawing.Color.White;
            this.IDH_USER.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_USER.Location = new System.Drawing.Point(386, 95);
            this.IDH_USER.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_USER.Multiline = false;
            this.IDH_USER.Name = "IDH_USER";
            this.IDH_USER.ReadOnly = false;
            this.IDH_USER.Size = new System.Drawing.Size(155, 14);
            this.IDH_USER.TabIndex = 186;
            this.IDH_USER.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_USER.TextValue = "";
            // 
            // IDH_CARNAM
            // 
            this.IDH_CARNAM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CARNAM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CARNAM.CornerRadius = 2;
            this.IDH_CARNAM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CARNAM.EnabledColor = System.Drawing.Color.White;
            this.IDH_CARNAM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CARNAM.Location = new System.Drawing.Point(386, 80);
            this.IDH_CARNAM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CARNAM.Multiline = false;
            this.IDH_CARNAM.Name = "IDH_CARNAM";
            this.IDH_CARNAM.ReadOnly = false;
            this.IDH_CARNAM.Size = new System.Drawing.Size(155, 14);
            this.IDH_CARNAM.TabIndex = 186;
            this.IDH_CARNAM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CARNAM.TextValue = "";
            // 
            // IDH_CARRIE
            // 
            this.IDH_CARRIE.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CARRIE.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CARRIE.CornerRadius = 2;
            this.IDH_CARRIE.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CARRIE.EnabledColor = System.Drawing.Color.White;
            this.IDH_CARRIE.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CARRIE.Location = new System.Drawing.Point(386, 65);
            this.IDH_CARRIE.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CARRIE.Multiline = false;
            this.IDH_CARRIE.Name = "IDH_CARRIE";
            this.IDH_CARRIE.ReadOnly = false;
            this.IDH_CARRIE.Size = new System.Drawing.Size(155, 14);
            this.IDH_CARRIE.TabIndex = 3;
            this.IDH_CARRIE.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CARRIE.TextValue = "";
            // 
            // IDH_SITREF
            // 
            this.IDH_SITREF.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SITREF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SITREF.CornerRadius = 2;
            this.IDH_SITREF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SITREF.EnabledColor = System.Drawing.Color.White;
            this.IDH_SITREF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SITREF.Location = new System.Drawing.Point(675, 110);
            this.IDH_SITREF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SITREF.Multiline = false;
            this.IDH_SITREF.Name = "IDH_SITREF";
            this.IDH_SITREF.ReadOnly = false;
            this.IDH_SITREF.Size = new System.Drawing.Size(155, 14);
            this.IDH_SITREF.TabIndex = 12;
            this.IDH_SITREF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SITREF.TextValue = "";
            // 
            // IDH_HAZCN
            // 
            this.IDH_HAZCN.BackColor = System.Drawing.Color.Transparent;
            this.IDH_HAZCN.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_HAZCN.CornerRadius = 2;
            this.IDH_HAZCN.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_HAZCN.Enabled = false;
            this.IDH_HAZCN.EnabledColor = System.Drawing.Color.White;
            this.IDH_HAZCN.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_HAZCN.Location = new System.Drawing.Point(675, 95);
            this.IDH_HAZCN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_HAZCN.Multiline = false;
            this.IDH_HAZCN.Name = "IDH_HAZCN";
            this.IDH_HAZCN.ReadOnly = false;
            this.IDH_HAZCN.Size = new System.Drawing.Size(107, 14);
            this.IDH_HAZCN.TabIndex = 11;
            this.IDH_HAZCN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_HAZCN.TextValue = "";
            // 
            // IDH_WASTTN
            // 
            this.IDH_WASTTN.BackColor = System.Drawing.Color.Transparent;
            this.IDH_WASTTN.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_WASTTN.CornerRadius = 2;
            this.IDH_WASTTN.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_WASTTN.EnabledColor = System.Drawing.Color.White;
            this.IDH_WASTTN.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_WASTTN.Location = new System.Drawing.Point(675, 80);
            this.IDH_WASTTN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_WASTTN.Multiline = false;
            this.IDH_WASTTN.Name = "IDH_WASTTN";
            this.IDH_WASTTN.ReadOnly = false;
            this.IDH_WASTTN.Size = new System.Drawing.Size(155, 14);
            this.IDH_WASTTN.TabIndex = 10;
            this.IDH_WASTTN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_WASTTN.TextValue = "";
            // 
            // IDH_STATUS
            // 
            this.IDH_STATUS.BackColor = System.Drawing.Color.Transparent;
            this.IDH_STATUS.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_STATUS.CornerRadius = 2;
            this.IDH_STATUS.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_STATUS.Enabled = false;
            this.IDH_STATUS.EnabledColor = System.Drawing.Color.White;
            this.IDH_STATUS.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_STATUS.Location = new System.Drawing.Point(675, 50);
            this.IDH_STATUS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_STATUS.Multiline = false;
            this.IDH_STATUS.Name = "IDH_STATUS";
            this.IDH_STATUS.ReadOnly = false;
            this.IDH_STATUS.Size = new System.Drawing.Size(155, 14);
            this.IDH_STATUS.TabIndex = 191;
            this.IDH_STATUS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_STATUS.TextValue = "";
            // 
            // IDH_BOOKDT
            // 
            this.IDH_BOOKDT.BackColor = System.Drawing.Color.Transparent;
            this.IDH_BOOKDT.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BOOKDT.CornerRadius = 2;
            this.IDH_BOOKDT.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BOOKDT.EnabledColor = System.Drawing.Color.White;
            this.IDH_BOOKDT.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BOOKDT.Location = new System.Drawing.Point(675, 35);
            this.IDH_BOOKDT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_BOOKDT.Multiline = false;
            this.IDH_BOOKDT.Name = "IDH_BOOKDT";
            this.IDH_BOOKDT.ReadOnly = false;
            this.IDH_BOOKDT.Size = new System.Drawing.Size(70, 14);
            this.IDH_BOOKDT.TabIndex = 16;
            this.IDH_BOOKDT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BOOKDT.TextValue = "";
            // 
            // IDH_ROW
            // 
            this.IDH_ROW.BackColor = System.Drawing.Color.Transparent;
            this.IDH_ROW.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_ROW.CornerRadius = 2;
            this.IDH_ROW.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_ROW.Enabled = false;
            this.IDH_ROW.EnabledColor = System.Drawing.Color.White;
            this.IDH_ROW.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_ROW.Location = new System.Drawing.Point(675, 20);
            this.IDH_ROW.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_ROW.Multiline = false;
            this.IDH_ROW.Name = "IDH_ROW";
            this.IDH_ROW.ReadOnly = false;
            this.IDH_ROW.Size = new System.Drawing.Size(155, 14);
            this.IDH_ROW.TabIndex = 189;
            this.IDH_ROW.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_ROW.TextValue = "";
            // 
            // IDH_BOOREF
            // 
            this.IDH_BOOREF.BackColor = System.Drawing.Color.Transparent;
            this.IDH_BOOREF.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BOOREF.CornerRadius = 2;
            this.IDH_BOOREF.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BOOREF.Enabled = false;
            this.IDH_BOOREF.EnabledColor = System.Drawing.Color.White;
            this.IDH_BOOREF.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BOOREF.Location = new System.Drawing.Point(675, 5);
            this.IDH_BOOREF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_BOOREF.Multiline = false;
            this.IDH_BOOREF.Name = "IDH_BOOREF";
            this.IDH_BOOREF.ReadOnly = false;
            this.IDH_BOOREF.Size = new System.Drawing.Size(155, 14);
            this.IDH_BOOREF.TabIndex = 188;
            this.IDH_BOOREF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BOOREF.TextValue = "";
            this.IDH_BOOREF.Validated += new System.EventHandler(this.IDH_WRROW_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 14);
            this.label2.TabIndex = 27;
            this.label2.Text = "Vehicle Reg. No.";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(5, 35);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 14);
            this.label3.TabIndex = 28;
            this.label3.Text = "Linked Waste Order";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(169, 35);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 14);
            this.label4.TabIndex = 29;
            this.label4.Text = "Row";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(5, 50);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 14);
            this.label5.TabIndex = 30;
            this.label5.Text = "Vehicle";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(5, 65);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 14);
            this.label6.TabIndex = 31;
            this.label6.Text = "Driver";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(5, 80);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 14);
            this.label7.TabIndex = 32;
            this.label7.Text = "2nd Tare Id";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(4, 96);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 14);
            this.label8.TabIndex = 33;
            this.label8.Text = "2nd Tare Name";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(9, 128);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 15);
            this.label9.TabIndex = 34;
            this.label9.Text = "Delivery Instruction";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(292, 5);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 14);
            this.label10.TabIndex = 35;
            this.label10.Text = "Customer";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(292, 20);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 14);
            this.label11.TabIndex = 36;
            this.label11.Text = "Name";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(292, 35);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 14);
            this.label12.TabIndex = 37;
            this.label12.Text = "Customer Ref. No.";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(292, 50);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 14);
            this.label13.TabIndex = 38;
            this.label13.Text = "Compl. Scheme";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(292, 65);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 14);
            this.label14.TabIndex = 39;
            this.label14.Text = "Waste Carrier";
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(292, 80);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 14);
            this.label15.TabIndex = 40;
            this.label15.Text = "Name";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(292, 95);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 14);
            this.label16.TabIndex = 41;
            this.label16.Text = "User";
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(292, 110);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 14);
            this.label17.TabIndex = 42;
            this.label17.Text = "Branch";
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(563, 7);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(91, 14);
            this.label18.TabIndex = 43;
            this.label18.Text = "WB Ticket No.";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(563, 20);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(91, 14);
            this.label19.TabIndex = 44;
            this.label19.Text = "Row No.";
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(563, 35);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 14);
            this.label20.TabIndex = 45;
            this.label20.Text = "Booking Date";
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(563, 50);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(91, 14);
            this.label21.TabIndex = 46;
            this.label21.Text = "Status";
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(563, 65);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(91, 14);
            this.label22.TabIndex = 47;
            this.label22.Text = "CHECKED BY";
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(564, 80);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(91, 14);
            this.label23.TabIndex = 48;
            this.label23.Text = "WTN";
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(563, 95);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(91, 14);
            this.label24.TabIndex = 49;
            this.label24.Text = "HWCN";
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(563, 110);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(91, 14);
            this.label25.TabIndex = 50;
            this.label25.Text = "Site Ref";
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(563, 125);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(91, 14);
            this.label26.TabIndex = 51;
            this.label26.Text = "External WB";
            // 
            // label27
            // 
            this.label27.Location = new System.Drawing.Point(563, 140);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(101, 14);
            this.label27.TabIndex = 52;
            this.label27.Text = "Container Number";
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(563, 155);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(110, 14);
            this.label28.TabIndex = 53;
            this.label28.Text = "Seal Number/Pentane";
            // 
            // IDH_EXTWEI
            // 
            this.IDH_EXTWEI.BackColor = System.Drawing.Color.Transparent;
            this.IDH_EXTWEI.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_EXTWEI.CornerRadius = 2;
            this.IDH_EXTWEI.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_EXTWEI.EnabledColor = System.Drawing.Color.White;
            this.IDH_EXTWEI.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_EXTWEI.Location = new System.Drawing.Point(675, 125);
            this.IDH_EXTWEI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_EXTWEI.Multiline = false;
            this.IDH_EXTWEI.Name = "IDH_EXTWEI";
            this.IDH_EXTWEI.ReadOnly = false;
            this.IDH_EXTWEI.Size = new System.Drawing.Size(155, 14);
            this.IDH_EXTWEI.TabIndex = 13;
            this.IDH_EXTWEI.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_EXTWEI.TextValue = "";
            // 
            // IDH_CONTNR
            // 
            this.IDH_CONTNR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_CONTNR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_CONTNR.CornerRadius = 2;
            this.IDH_CONTNR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_CONTNR.EnabledColor = System.Drawing.Color.White;
            this.IDH_CONTNR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_CONTNR.Location = new System.Drawing.Point(675, 140);
            this.IDH_CONTNR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_CONTNR.Multiline = false;
            this.IDH_CONTNR.Name = "IDH_CONTNR";
            this.IDH_CONTNR.ReadOnly = false;
            this.IDH_CONTNR.Size = new System.Drawing.Size(155, 14);
            this.IDH_CONTNR.TabIndex = 14;
            this.IDH_CONTNR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_CONTNR.TextValue = "";
            // 
            // IDH_SEALNR
            // 
            this.IDH_SEALNR.BackColor = System.Drawing.Color.Transparent;
            this.IDH_SEALNR.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_SEALNR.CornerRadius = 2;
            this.IDH_SEALNR.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_SEALNR.EnabledColor = System.Drawing.Color.White;
            this.IDH_SEALNR.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_SEALNR.Location = new System.Drawing.Point(675, 155);
            this.IDH_SEALNR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_SEALNR.Multiline = false;
            this.IDH_SEALNR.Name = "IDH_SEALNR";
            this.IDH_SEALNR.ReadOnly = false;
            this.IDH_SEALNR.Size = new System.Drawing.Size(155, 14);
            this.IDH_SEALNR.TabIndex = 15;
            this.IDH_SEALNR.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_SEALNR.TextValue = "";
            // 
            // label29
            // 
            this.label29.Location = new System.Drawing.Point(563, 170);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(91, 14);
            this.label29.TabIndex = 36;
            this.label29.Text = "Obligated";
            // 
            // IDH_BOOKTM
            // 
            this.IDH_BOOKTM.BackColor = System.Drawing.Color.Transparent;
            this.IDH_BOOKTM.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BOOKTM.CornerRadius = 2;
            this.IDH_BOOKTM.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BOOKTM.EnabledColor = System.Drawing.Color.White;
            this.IDH_BOOKTM.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BOOKTM.Location = new System.Drawing.Point(775, 35);
            this.IDH_BOOKTM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_BOOKTM.Multiline = false;
            this.IDH_BOOKTM.Name = "IDH_BOOKTM";
            this.IDH_BOOKTM.ReadOnly = false;
            this.IDH_BOOKTM.Size = new System.Drawing.Size(55, 14);
            this.IDH_BOOKTM.TabIndex = 190;
            this.IDH_BOOKTM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BOOKTM.TextValue = "";
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(745, 35);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(30, 14);
            this.label30.TabIndex = 59;
            this.label30.Text = "Time";
            // 
            // IDH_REGL
            // 
            this.IDH_REGL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_REGL.Image")));
            this.IDH_REGL.Location = new System.Drawing.Point(265, 20);
            this.IDH_REGL.Name = "IDH_REGL";
            this.IDH_REGL.Size = new System.Drawing.Size(14, 14);
            this.IDH_REGL.TabIndex = 161;
            this.IDH_REGL.UseVisualStyleBackColor = true;
            this.IDH_REGL.Click += new System.EventHandler(this.IDH_REGLButton1Click);
            // 
            // IDH_WOCF
            // 
            this.IDH_WOCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_WOCF.Image")));
            this.IDH_WOCF.Location = new System.Drawing.Point(265, 35);
            this.IDH_WOCF.Name = "IDH_WOCF";
            this.IDH_WOCF.Size = new System.Drawing.Size(14, 14);
            this.IDH_WOCF.TabIndex = 164;
            this.IDH_WOCF.UseVisualStyleBackColor = true;
            this.IDH_WOCF.Click += new System.EventHandler(this.LinkWOClick);
            // 
            // IDH_CUSL
            // 
            this.IDH_CUSL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_CUSL.Image")));
            this.IDH_CUSL.Location = new System.Drawing.Point(541, 5);
            this.IDH_CUSL.Name = "IDH_CUSL";
            this.IDH_CUSL.Size = new System.Drawing.Size(14, 14);
            this.IDH_CUSL.TabIndex = 179;
            this.IDH_CUSL.UseVisualStyleBackColor = true;
            this.IDH_CUSL.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_CSCF
            // 
            this.IDH_CSCF.Image = ((System.Drawing.Image)(resources.GetObject("IDH_CSCF.Image")));
            this.IDH_CSCF.Location = new System.Drawing.Point(541, 50);
            this.IDH_CSCF.Name = "IDH_CSCF";
            this.IDH_CSCF.Size = new System.Drawing.Size(14, 14);
            this.IDH_CSCF.TabIndex = 184;
            this.IDH_CSCF.UseVisualStyleBackColor = true;
            this.IDH_CSCF.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // IDH_CARL
            // 
            this.IDH_CARL.Image = ((System.Drawing.Image)(resources.GetObject("IDH_CARL.Image")));
            this.IDH_CARL.Location = new System.Drawing.Point(541, 65);
            this.IDH_CARL.Name = "IDH_CARL";
            this.IDH_CARL.Size = new System.Drawing.Size(14, 14);
            this.IDH_CARL.TabIndex = 185;
            this.IDH_CARL.UseVisualStyleBackColor = true;
            this.IDH_CARL.Click += new System.EventHandler(this.BPSearchClick);
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.btn1.Location = new System.Drawing.Point(12, 502);
            this.btn1.Margin = new System.Windows.Forms.Padding(1);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(75, 20);
            this.btn1.TabIndex = 1000;
            this.btn1.Text = "Find";
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.Btn1Click);
            // 
            // IDH_BRANCH
            // 
            this.IDH_BRANCH.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BRANCH.CornerRadius = 2;
            this.IDH_BRANCH.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BRANCH.DropDownHight = 100;
            this.IDH_BRANCH.EnabledColor = System.Drawing.Color.White;
            this.IDH_BRANCH.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BRANCH.Location = new System.Drawing.Point(386, 110);
            this.IDH_BRANCH.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_BRANCH.Multiline = false;
            this.IDH_BRANCH.Name = "IDH_BRANCH";
            this.IDH_BRANCH.SelectedIndex = -1;
            this.IDH_BRANCH.SelectedItem = null;
            this.IDH_BRANCH.Size = new System.Drawing.Size(155, 14);
            this.IDH_BRANCH.TabIndex = 187;
            this.IDH_BRANCH.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BRANCH.TextBoxReadOnly = true;
            this.IDH_BRANCH.TextValue = "";
            this.IDH_BRANCH.SelectedIndexChanged += new System.EventHandler(this.All_ComboChanged);
            // 
            // IDH_TZONE
            // 
            this.IDH_TZONE.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_TZONE.CornerRadius = 2;
            this.IDH_TZONE.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_TZONE.DropDownHight = 100;
            this.IDH_TZONE.EnabledColor = System.Drawing.Color.White;
            this.IDH_TZONE.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_TZONE.Location = new System.Drawing.Point(675, 65);
            this.IDH_TZONE.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_TZONE.Multiline = false;
            this.IDH_TZONE.Name = "IDH_TZONE";
            this.IDH_TZONE.SelectedIndex = -1;
            this.IDH_TZONE.SelectedItem = null;
            this.IDH_TZONE.Size = new System.Drawing.Size(155, 14);
            this.IDH_TZONE.TabIndex = 192;
            this.IDH_TZONE.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_TZONE.TextBoxReadOnly = true;
            this.IDH_TZONE.TextValue = "";
            this.IDH_TZONE.SelectedIndexChanged += new System.EventHandler(this.All_ComboChanged);
            // 
            // IDH_OBLED
            // 
            this.IDH_OBLED.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_OBLED.CornerRadius = 2;
            this.IDH_OBLED.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_OBLED.DropDownHight = 100;
            this.IDH_OBLED.EnabledColor = System.Drawing.Color.White;
            this.IDH_OBLED.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_OBLED.Location = new System.Drawing.Point(675, 170);
            this.IDH_OBLED.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_OBLED.Multiline = false;
            this.IDH_OBLED.Name = "IDH_OBLED";
            this.IDH_OBLED.SelectedIndex = -1;
            this.IDH_OBLED.SelectedItem = null;
            this.IDH_OBLED.Size = new System.Drawing.Size(155, 14);
            this.IDH_OBLED.TabIndex = 28;
            this.IDH_OBLED.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_OBLED.TextBoxReadOnly = true;
            this.IDH_OBLED.TextValue = "";
            this.IDH_OBLED.SelectedIndexChanged += new System.EventHandler(this.All_ComboChanged);
            // 
            // IDH_AVDOC
            // 
            this.IDH_AVDOC.Location = new System.Drawing.Point(220, 508);
            this.IDH_AVDOC.Name = "IDH_AVDOC";
            this.IDH_AVDOC.Size = new System.Drawing.Size(93, 14);
            this.IDH_AVDOC.TabIndex = 901;
            this.IDH_AVDOC.Text = "Auto Print DOC";
            this.IDH_AVDOC.UseVisualStyleBackColor = true;
            // 
            // IDH_AVCONV
            // 
            this.IDH_AVCONV.Location = new System.Drawing.Point(110, 508);
            this.IDH_AVCONV.Name = "IDH_AVCONV";
            this.IDH_AVCONV.Size = new System.Drawing.Size(104, 14);
            this.IDH_AVCONV.TabIndex = 900;
            this.IDH_AVCONV.Text = "Auto Print WB Tkt";
            this.IDH_AVCONV.UseVisualStyleBackColor = true;
            // 
            // IDH_CON
            // 
            this.IDH_CON.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_CON.Location = new System.Drawing.Point(454, 502);
            this.IDH_CON.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_CON.Name = "IDH_CON";
            this.IDH_CON.Size = new System.Drawing.Size(65, 20);
            this.IDH_CON.TabIndex = 902;
            this.IDH_CON.Text = "Conv Note";
            this.IDH_CON.UseVisualStyleBackColor = false;
            this.IDH_CON.Click += new System.EventHandler(this.IDH_CON_Click);
            // 
            // IDH_DOC
            // 
            this.IDH_DOC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_DOC.Location = new System.Drawing.Point(522, 502);
            this.IDH_DOC.Margin = new System.Windows.Forms.Padding(1);
            this.IDH_DOC.Name = "IDH_DOC";
            this.IDH_DOC.Size = new System.Drawing.Size(65, 20);
            this.IDH_DOC.TabIndex = 903;
            this.IDH_DOC.Text = "DOC";
            this.IDH_DOC.UseVisualStyleBackColor = false;
            this.IDH_DOC.Click += new System.EventHandler(this.IDH_DOC_Click);
            // 
            // IDH_OS
            // 
            this.IDH_OS.Image = ((System.Drawing.Image)(resources.GetObject("IDH_OS.Image")));
            this.IDH_OS.Location = new System.Drawing.Point(248, 19);
            this.IDH_OS.Margin = new System.Windows.Forms.Padding(0);
            this.IDH_OS.Name = "IDH_OS";
            this.IDH_OS.Size = new System.Drawing.Size(14, 14);
            this.IDH_OS.TabIndex = 160;
            this.IDH_OS.UseVisualStyleBackColor = true;
            this.IDH_OS.Click += new System.EventHandler(this.IDH_OSClick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prg1,
            this.LblVersion,
            this.ItemInfo,
            this.stl01,
            this.lblLastError});
            this.statusStrip1.Location = new System.Drawing.Point(0, 529);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(838, 24);
            this.statusStrip1.TabIndex = 105;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.StatusStrip1ItemClicked);
            // 
            // prg1
            // 
            this.prg1.Name = "prg1";
            this.prg1.Size = new System.Drawing.Size(100, 18);
            // 
            // LblVersion
            // 
            this.LblVersion.Name = "LblVersion";
            this.LblVersion.Size = new System.Drawing.Size(106, 19);
            this.LblVersion.Text = " Version db: 577 - 0.0001";
            // 
            // ItemInfo
            // 
            this.ItemInfo.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.ItemInfo.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.ItemInfo.Name = "ItemInfo";
            this.ItemInfo.Size = new System.Drawing.Size(44, 19);
            this.ItemInfo.Text = "ItemInfo";
            // 
            // stl01
            // 
            this.stl01.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.stl01.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.stl01.Name = "stl01";
            this.stl01.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.stl01.Size = new System.Drawing.Size(48, 19);
            this.stl01.Text = "Welcome";
            this.stl01.Click += new System.EventHandler(this.Stl01Click);
            // 
            // lblLastError
            // 
            this.lblLastError.BackColor = System.Drawing.Color.Linen;
            this.lblLastError.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblLastError.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter;
            this.lblLastError.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lblLastError.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(64, 19);
            this.lblLastError.Text = "Get LastError";
            this.lblLastError.Visible = false;
            // 
            // btnHelp
            // 
            this.btnHelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(230)))), ((int)(((byte)(200)))));
            this.btnHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHelp.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnHelp.Location = new System.Drawing.Point(800, 505);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(27, 19);
            this.btnHelp.TabIndex = 1001;
            this.btnHelp.Text = "?";
            this.btnHelp.UseVisualStyleBackColor = false;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // IDH_CONGEN
            // 
            this.IDH_CONGEN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(245)))), ((int)(((byte)(200)))));
            this.IDH_CONGEN.Location = new System.Drawing.Point(784, 95);
            this.IDH_CONGEN.Margin = new System.Windows.Forms.Padding(0);
            this.IDH_CONGEN.Name = "IDH_CONGEN";
            this.IDH_CONGEN.Size = new System.Drawing.Size(46, 14);
            this.IDH_CONGEN.TabIndex = 1003;
            this.IDH_CONGEN.Text = "GEN";
            this.IDH_CONGEN.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.IDH_CONGEN.UseVisualStyleBackColor = false;
            this.IDH_CONGEN.Click += new System.EventHandler(this.IDH_CONGEN_Click);
            // 
            // IDH_BOOKIN
            // 
            this.IDH_BOOKIN.AutoSize = true;
            this.IDH_BOOKIN.Location = new System.Drawing.Point(319, 508);
            this.IDH_BOOKIN.Name = "IDH_BOOKIN";
            this.IDH_BOOKIN.Size = new System.Drawing.Size(110, 16);
            this.IDH_BOOKIN.TabIndex = 1004;
            this.IDH_BOOKIN.Text = "Do Stock Movement";
            this.IDH_BOOKIN.UseVisualStyleBackColor = true;
            this.IDH_BOOKIN.CheckedChanged += new System.EventHandler(this.IDH_BOOKIN_CheckedChanged);
            // 
            // IDH_BOKSTA
            // 
            this.IDH_BOKSTA.BackColor = System.Drawing.Color.Transparent;
            this.IDH_BOKSTA.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.IDH_BOKSTA.CornerRadius = 2;
            this.IDH_BOKSTA.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.IDH_BOKSTA.Enabled = false;
            this.IDH_BOKSTA.EnabledColor = System.Drawing.Color.White;
            this.IDH_BOKSTA.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.IDH_BOKSTA.Location = new System.Drawing.Point(429, 508);
            this.IDH_BOKSTA.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IDH_BOKSTA.Multiline = false;
            this.IDH_BOKSTA.Name = "IDH_BOKSTA";
            this.IDH_BOKSTA.ReadOnly = false;
            this.IDH_BOKSTA.Size = new System.Drawing.Size(14, 14);
            this.IDH_BOKSTA.TabIndex = 1005;
            this.IDH_BOKSTA.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.IDH_BOKSTA.TextValue = "";
            // 
            // MainForm
            // 
            this.AcceptButton = this.btn1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(241)))), ((int)(((byte)(246)))));
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(838, 553);
            this.Controls.Add(this.IDH_BOKSTA);
            this.Controls.Add(this.IDH_BOOKIN);
            this.Controls.Add(this.IDH_CONGEN);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.oPNavigation);
            this.Controls.Add(this.IDH_DOC);
            this.Controls.Add(this.IDH_CON);
            this.Controls.Add(this.IDH_AVCONV);
            this.Controls.Add(this.IDH_AVDOC);
            this.Controls.Add(this.IDH_OBLED);
            this.Controls.Add(this.IDH_TZONE);
            this.Controls.Add(this.IDH_BRANCH);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.IDH_CARL);
            this.Controls.Add(this.IDH_CSCF);
            this.Controls.Add(this.IDH_CUSL);
            this.Controls.Add(this.IDH_WOCF);
            this.Controls.Add(this.IDH_OS);
            this.Controls.Add(this.IDH_REGL);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.IDH_BOOKTM);
            this.Controls.Add(this.IDH_SEALNR);
            this.Controls.Add(this.IDH_CONTNR);
            this.Controls.Add(this.IDH_EXTWEI);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.IDH_SITREF);
            this.Controls.Add(this.IDH_HAZCN);
            this.Controls.Add(this.IDH_WASTTN);
            this.Controls.Add(this.IDH_STATUS);
            this.Controls.Add(this.IDH_BOOKDT);
            this.Controls.Add(this.IDH_ROW);
            this.Controls.Add(this.IDH_BOOREF);
            this.Controls.Add(this.IDH_USER);
            this.Controls.Add(this.IDH_CARNAM);
            this.Controls.Add(this.IDH_CARRIE);
            this.Controls.Add(this.IDH_ONCS);
            this.Controls.Add(this.IDH_CUSCRF);
            this.Controls.Add(this.IDH_CUSTNM);
            this.Controls.Add(this.IDH_CUST);
            this.Controls.Add(this.IDH_SPECIN);
            this.Controls.Add(this.oMainTabs);
            this.Controls.Add(this.IDH_TRLNM);
            this.Controls.Add(this.IDH_TRLReg);
            this.Controls.Add(this.IDH_DRIVER);
            this.Controls.Add(this.IDH_VEH);
            this.Controls.Add(this.IDH_WRROW);
            this.Controls.Add(this.IDH_WRORD);
            this.Controls.Add(this.IDH_VEHREG);
            this.Controls.Add(this.IDH_JOBTTP);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Disposal Order";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFormFormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.oMainTabs.ResumeLayout(false);
            this.oTPWeighing.ResumeLayout(false);
            this.oTPWeighing.PerformLayout();
            this.PO_GRP.ResumeLayout(false);
            this.PO_GRP.PerformLayout();
            this.SO_GRP.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.oTPCarrier.ResumeLayout(false);
            this.oTPCustomer.ResumeLayout(false);
            this.oTPProducer.ResumeLayout(false);
            this.oTPSite.ResumeLayout(false);
            this.oAdditional.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.oAddGrid)).EndInit();
            this.oDeductions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.oDeductionGrid)).EndInit();
            this.oPNavigation.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

#region variables
        private SBOTextBox IDH_ADDCO;
        private SBOTextBox IDH_ADDCH;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        //private com.idh.win.controls.WR1Grid oAddGrid;
        private com.idh.wr1.grid.AdditionalExpenses oAddGrid;
        private System.Windows.Forms.TabPage oAdditional;
        private System.Windows.Forms.ToolStripStatusLabel lblLastError;
        private SBOTextBox IDH_PROCD;
		private System.Windows.Forms.Button IDH_PROCF;
		private SBOTextBox IDH_PRONM;
		private System.Windows.Forms.Label label105;
		private System.Windows.Forms.Label label104;
		private SBOTextBox IDH_PURWGT;
		private SBOTextBox IDH_PRCOST;
		private System.Windows.Forms.Label label103;
        private SBOTextBox IDH_PURTOT;
		private System.Windows.Forms.Label label106;
		private SBOTextBox IDH_TAXO;
		private System.Windows.Forms.Label label107;
		private SBOTextBox IDH_PSTAT;
		private System.Windows.Forms.Label label108;
		private SBOTextBox IDH_TOTCOS;
		private System.Windows.Forms.Label Lbl1000007;
        private System.Windows.Forms.Label label109;
		private System.Windows.Forms.ToolStripProgressBar prg1;
		private System.Windows.Forms.ToolStripStatusLabel stl01;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.Panel oPNavigation;
		private System.Windows.Forms.Button IDH_TMR;
		private System.Windows.Forms.Button IDH_OS;
		private System.Windows.Forms.CheckBox IDH_NOVAT;
		private System.Windows.Forms.Button IDH_REGL;
		private System.Windows.Forms.Button IDH_WOCF;
		private System.Windows.Forms.Button IDH_CUSL;
		private System.Windows.Forms.Button IDH_CSCF;
		private System.Windows.Forms.Button IDH_CARL;
		private System.Windows.Forms.Button IDH_ORLK;
		private System.Windows.Forms.Button IDH_WASCF;
		private System.Windows.Forms.Button IDH_ITCF;
		private System.Windows.Forms.Button IDH_DOC;
		private System.Windows.Forms.Button IDH_CON;
		private System.Windows.Forms.CheckBox IDH_AVCONV;
		private System.Windows.Forms.CheckBox IDH_AVDOC;
		private System.Windows.Forms.Button btAdd;
		private System.Windows.Forms.Button IDH_WASAL;
		private SBOTextBox IDH_ADDRES;
		private SBOTextBox IDH_STREET;
		private SBOTextBox IDH_CARREF;
		private System.Windows.Forms.CheckBox IDH_DOORD;
		private System.Windows.Forms.CheckBox IDH_DOARI;
		private System.Windows.Forms.CheckBox IDH_DOARIP;
		private System.Windows.Forms.CheckBox IDH_AINV;
		private System.Windows.Forms.CheckBox IDH_FOC;
		private SBOTextBox IDH_CUSTOT;
		private SBOTextBox IDH_RORIGI;
		private SBOTextBox IDH_WASMAT;
		private SBOTextBox IDH_WASCL1;
		private SBOTextBox IDH_DESC;
		private SBOTextBox IDH_ITMCOD;
		private SBOTextBox IDH_ITMGRC;
		private SBOTextBox IDH_ITMGRN;
		private SBOTextBox IDH_CUSCM;
		private SBOTextBox IDH_RDWGT;
		private SBOTextBox IDH_WDT2;
		private SBOTextBox IDH_SER2;
		private SBOTextBox IDH_WEI2;
		private SBOTextBox IDH_CASHMT;
		private SBOTextBox IDH_RSTAT;
		private SBOTextBox IDH_DISCNT;
        private SBOTextBox IDH_CUSWEI;
		private SBOTextBox IDH_CUSCHG;
		private SBOTextBox IDH_TOTAL;
		private SBOTextBox IDH_TAX;
		private SBOTextBox IDH_SUBTOT;
		private SBOTextBox IDH_ADDEX;
        private SBOTextBox IDH_DSCPRC;
		private System.Windows.Forms.Button IDH_ACCB;
		private System.Windows.Forms.Button IDH_ACC1;
		private System.Windows.Forms.Button IDH_BUF1;
		private System.Windows.Forms.Button IDH_TAR1;
		private System.Windows.Forms.Button IDH_ACC2;
		private System.Windows.Forms.Button IDH_BUF2;
		private System.Windows.Forms.Button IDH_TAR2;
		private System.Windows.Forms.PictureBox pictureBox1;
		private SBOTextBox IDH_WDT1;
		private SBOTextBox IDH_SER1;
		private SBOTextBox IDH_WEI1;
		private SBOTextBox IDH_TRLTar;
		private SBOTextBox IDH_TARWEI;
		private SBOTextBox IDH_WDTB;
		private SBOTextBox IDH_SERB;
		private SBOTextBox IDH_WEIB;
		private SBOTextBox IDH_WEIG;
        private SBOComboBox IDH_WEIBRG;
		private System.Windows.Forms.Label label88;
		private System.Windows.Forms.Label label89;
		private System.Windows.Forms.Label label90;
		private System.Windows.Forms.Label label91;
		private System.Windows.Forms.Label label92;
		private System.Windows.Forms.Label label93;
		private System.Windows.Forms.Label label94;
		private System.Windows.Forms.Label label95;
		private System.Windows.Forms.Label label96;
		private System.Windows.Forms.Label label97;
		private System.Windows.Forms.Label label98;
		private System.Windows.Forms.Label label99;
		private System.Windows.Forms.Label label100;
		private System.Windows.Forms.Label label101;
		private System.Windows.Forms.Label label102;
		private System.Windows.Forms.Label label76;
		private System.Windows.Forms.Label label77;
		private System.Windows.Forms.Label label78;
		private System.Windows.Forms.Label label79;
		private System.Windows.Forms.Label label80;
		private System.Windows.Forms.Label label81;
		private System.Windows.Forms.Label label82;
		private System.Windows.Forms.Label label83;
		private System.Windows.Forms.Label label84;
		private System.Windows.Forms.Label label85;
		private System.Windows.Forms.Label label86;
		private System.Windows.Forms.Label label87;
		private System.Windows.Forms.Button IDH_SITAL;
		private System.Windows.Forms.Button IDH_SITL;
		private SBOTextBox IDH_SITPHO;
		private SBOTextBox IDH_SITPOS;
		private SBOTextBox IDH_SITCIT;
		private SBOTextBox IDH_SITBLO;
		private SBOTextBox IDH_SITSTR;
		private SBOTextBox IDH_SITADD;
		private SBOTextBox IDH_SITCRF;
		private SBOTextBox IDH_SITCON;
		private SBOTextBox IDH_DISNAM;
		private SBOTextBox IDH_DISSIT;
		private System.Windows.Forms.Label label75;
		private System.Windows.Forms.Label label74;
		private System.Windows.Forms.Label label73;
		private System.Windows.Forms.Label label72;
		private System.Windows.Forms.Label label71;
		private System.Windows.Forms.Label label70;
		private System.Windows.Forms.Label label69;
		private System.Windows.Forms.Label label68;
		private System.Windows.Forms.Label label67;
		private System.Windows.Forms.Label label66;
		private System.Windows.Forms.Label label64;
		private System.Windows.Forms.Label label63;
		private System.Windows.Forms.Label label62;
		private SBOTextBox IDH_PRDCIT;
		private SBOTextBox IDH_PRDPOS;
		private SBOTextBox IDH_PRDPHO;
		private System.Windows.Forms.Label label65;
		private SBOTextBox IDH_ORIGIN;
		private System.Windows.Forms.Button IDH_PRDL;
		private System.Windows.Forms.Button IDH_ORIGLU;
		private System.Windows.Forms.Button IDH_PROAL;
		private SBOTextBox IDH_PRDBLO;
		private SBOTextBox IDH_PRDSTR;
		private SBOTextBox IDH_PRDADD;
		private SBOTextBox IDH_PRDCRF;
		private SBOTextBox IDH_PRDCON;
		private SBOTextBox IDH_WNAM;
		private SBOTextBox IDH_WPRODU;
		private System.Windows.Forms.Label label61;
		private System.Windows.Forms.Label label60;
		private System.Windows.Forms.Label label59;
		private System.Windows.Forms.Label label58;
		private System.Windows.Forms.Label label57;
		private System.Windows.Forms.Label label56;
		private System.Windows.Forms.Label label55;
		private SBOTextBox IDH_CONTNM;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.Label label49;
		private System.Windows.Forms.Label label50;
		private SBOTextBox IDH_BLOCK;
		private System.Windows.Forms.Label label51;
		private SBOTextBox IDH_CITY;
		private System.Windows.Forms.Label label52;
		private SBOTextBox IDH_ZIP;
		private System.Windows.Forms.Label label53;
		private SBOTextBox IDH_PHONE;
		private System.Windows.Forms.Label label54;
		private SBOTextBox IDH_LICREG;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.Label label41;
		private SBOTextBox IDH_ROUTE;
		private SBOTextBox IDH_SEQ;
		private System.Windows.Forms.Label label42;
		private System.Windows.Forms.Button IDH_ROUTL;
		private SBOTextBox IDH_SteId;
		private SBOTextBox IDH_PCD;
		private SBOTextBox IDH_SLCNO;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.Button IDH_CUSAL;
		private System.Windows.Forms.Button IDH_CNA;
        private SBOComboBox IDH_OBLED;
		private SBOTextBox IDH_EXTWEI;
		private SBOTextBox IDH_BOOREF;
		private SBOTextBox IDH_TRLReg;
		private System.Windows.Forms.Button btFirst;
		private System.Windows.Forms.Button btPrevious;
		private System.Windows.Forms.Button btNext;
		private System.Windows.Forms.Button btLast;
		private System.Windows.Forms.Button btFind;
		private System.Windows.Forms.Label label39;
		private SBOTextBox IDH_SITETL;
		private SBOTextBox IDH_CUSLIC;
		private System.Windows.Forms.Label label40;
		private SBOTextBox IDH_CUSPHO;
		private SBOTextBox IDH_CUSPOS;
		private SBOTextBox IDH_COUNTY;
		private SBOTextBox IDH_CUSCIT;
		private SBOTextBox IDH_CUSBLO;
		private SBOTextBox IDH_CUSSTR;
		private SBOTextBox IDH_CUSADD;
		private SBOTextBox IDH_CUSCON;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.Label label31;
		private SBOTextBox IDH_CUSCRF;
		private System.Windows.Forms.Button btn1;
        private SBOComboBox IDH_JOBTTP;
		private SBOTextBox IDH_VEH;
		private SBOTextBox IDH_DRIVER;
		private SBOTextBox IDH_TRLNM;
        private SBOTextBox IDH_SPECIN;
		private SBOTextBox IDH_CUSTNM;
		private SBOTextBox IDH_ONCS;
        private SBOComboBox IDH_BRANCH;
		private SBOTextBox IDH_USER;
		private SBOTextBox IDH_CARNAM;
		private SBOTextBox IDH_CARRIE;
		private SBOTextBox IDH_SITREF;
		private SBOTextBox IDH_HAZCN;
		private SBOTextBox IDH_WASTTN;
        private SBOComboBox IDH_TZONE;
		private SBOTextBox IDH_STATUS;
		private SBOTextBox IDH_BOOKDT;
		private SBOTextBox IDH_ROW;
		private SBOTextBox IDH_CONTNR;
		private SBOTextBox IDH_SEALNR;
		private SBOTextBox IDH_BOOKTM;
		private SBOTextBox IDH_WRROW;
		private SBOTextBox IDH_WRORD;
		private SBOTextBox IDH_VEHREG;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TabPage oTPSite;
		private System.Windows.Forms.TabPage oTPProducer;
		private System.Windows.Forms.TabPage oTPCustomer;
		private System.Windows.Forms.TabPage oTPCarrier;
		private System.Windows.Forms.TabPage oTPWeighing;
		private System.Windows.Forms.TabControl oMainTabs;
		private System.Windows.Forms.Label label1;

        private FolderBrowserDialog folderBrowserDialog1;
        private CheckBox IDH_DOPO;
        private ToolStripStatusLabel ItemInfo;
        private Button btnHelp;
        private System.Windows.Forms.ToolStripStatusLabel LblVersion;
        private Button IDH_CONGEN;
        private Button IDH_ORIGLU2;
        private SBOTextBox IDH_ORIGIN2;
        private Label label110;
        private Button IDH_REF;
        private SBOTextBox IDH_ADJWGT;
        private SBOTextBox IDH_WGTDED;
        private Label label114;
        private Label label113;
        private GroupBox groupBox1;
        private SBOTextBox IDH_VALDED;
        private Label label115;
        private SBOTextBox IDH_ORDTOT;
        private Label label119;
        private SBOTextBox IDH_ORDCOS;
        private SBOTextBox IDH_ORDQTY;
        private Label label120;
        private Label label121;
        private SBOTextBox IDH_TIPTOT;
        private Label label116;
        private SBOTextBox IDH_TIPCOS;
        private SBOTextBox IDH_TIPWEI;
        private Label label117;
        private Label label118;
        private SBOComboBox IDH_UOM;
        private SBOComboBox IDH_PUOM;
        private SBOComboBox IDH_PURUOM;
        private CheckBox IDH_ISTRL;
        private CheckBox IDH_BOOKIN;
        private SBOTextBox IDH_BOKSTA;
        private GroupBox SO_GRP;
        private GroupBox PO_GRP;
        private RadioButton IDH_USEAU;
        private RadioButton IDH_USERE;
        private TabPage oDeductions;
        private DataGridView oDeduction;
        private Label label122;
        private Label label123;
        private SBOTextBox IDH_DEDVAL;
        private SBOTextBox IDH_DEDWEI;
        private com.idh.wr1.grid.Deductions oDeductionGrid;
        private SBOTextBox IDH_CUST;
#endregion

        //private void doCheckToGenConNum()
        //{
        //    if (IDH_AVDOC.Checked)
        //    {
        //        string sWasteCode = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._WasCd);
        //        string sConNum = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._ConNum);

        //        double dWei1 = moDisposal.DisposalRows.getFormDFValueAsDouble(IDH_DISPROW._Wei1);
        //        double dWei2 = moDisposal.DisposalRows.getFormDFValueAsDouble(IDH_DISPROW._Wei2);
        //        double dRdWgt = moDisposal.DisposalRows.getFormDFValueAsDouble(IDH_DISPROW._RdWgt);
        //        double dAQt = moDisposal.DisposalRows.getFormDFValueAsDouble(IDH_DISPROW._AUOMQt);
        //        if (((dWei1 > 0 && dWei2 > 0) || dRdWgt != 0 || dAQt != 0) &&
        //            sWasteCode != null && sWasteCode.Length > 0 &&
        //            (sConNum == null || sConNum.Length == 0))
        //        {
        //            if (Config.INSTANCE.isHazardousItem(sWasteCode))
        //            {
        //                string sCardCode = moDisposal.getFormDFValueAsString(IDH_DISPORD._CardCd);
        //                string sCardName = moDisposal.getFormDFValueAsString(IDH_DISPORD._PCardNM);
        //                string sAddress = moDisposal.getFormDFValueAsString(IDH_DISPORD._Address);
        //                sConNum = com.idh.bridge.utils.General.doCreateConNumber(sCardCode, sCardName, sAddress);

        //                moDisposal.DisposalRows.setFormDFValue(IDH_DISPROW._ConNum, sConNum);
        //            }
        //        }
        //    }
        //}

        //void Btn1Click(object sender, EventArgs e)	{
        //    try {
        //        string sCaption = btn1.Text;
        //        if ( moDisposal.Mode == FormBridge.Mode_FIND ){
        //            string sCode = IDH_BOOREF.Text;
        //            if ( sCode.Length > 0 ) {
        //                if ( moDisposal.getByKey(sCode) ){
        //                    moDisposal.doUpdateFormData();
        //                    moDisposal.Mode = FormBridge.Mode_VIEW; 
        //                }
        //            }
        //        }
        //        else if ( moDisposal.Mode == FormBridge.Mode_UPDATE ){
        //            doUpdateDB();
	
        //            doCheckToGenConNum();
	
        //            if ( moDisposal.doUpDateData() ){
	
        //                if (IDH_AVCONV.Checked)
        //                    moDisposal.doConvReport();
        //                if (IDH_AVDOC.Checked)
        //                    moDisposal.doDocReport();
	
        //                string sDONr = moDisposal.getFormDFValueAsString(IDH_DISPORD._Code);
        //                moDisposal.getByKey(sDONr);
        //                moDisposal.Mode = FormBridge.Mode_OK;
        //            }
        //        }
        //        else if ( moDisposal.Mode == FormBridge.Mode_ADD ){
        //            doUpdateDB();
	
        //            doCheckToGenConNum();
	
        //            if ( moDisposal.doAddData() ){
        //                moDisposal.doUpdateFormData();
							
						
        //                if (IDH_AVCONV.Checked)
        //                    moDisposal.doConvReport();
        //                if (IDH_AVDOC.Checked)
        //                    moDisposal.doDocReport();
	
        //                moDisposal.Mode = FormBridge.Mode_FIND;
        //            }				
        //        }
        //        else if (moDisposal.Mode == FormBridge.Mode_OK || moDisposal.Mode == FormBridge.Mode_VIEW)
        //        {
        //            moDisposal.Mode = FormBridge.Mode_FIND;
        //        }
        //    }
        //    catch ( Exception ex ) {
        //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error doing the action.");
        //    }        
        //}	
		
        //void BtFindClick(object sender, EventArgs e){
        //    try {
        //        moDisposal.Mode = FormBridge.Mode_FIND; 
        //        btn1.Text = FormBridge.Mode_FIND;
        //        doLoadSupportData();
        //    }
        //    catch ( Exception ex ) {
        //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
        //    }        
        //}	
		
        //void BtAddClick(object sender, EventArgs e) {
        //    try {
        //        moDisposal.Mode = FormBridge.Mode_ADD;
        //        doLoadSupportData();
        //    }
        //    catch ( Exception ex ) {
        //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
        //    }        
        //}		
		
        //void BtFirstClick(object sender, EventArgs e) {
        //    try {
        //        if ( moDisposal.getByKeyFirst() ){
        //            doLoadSupportData();
        //        }
        //    }
        //    catch ( Exception ex ) {
        //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
        //    }        
        //}
		
        //void BtPreviousClick(object sender, EventArgs e) {
        //    try {
        //        if ( moDisposal.getByKeyPrevious() ){
        //            doLoadSupportData();
        //        }
        //    }
        //    catch ( Exception ex ) {
        //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
        //    }        
        //}
		
        //void BtNextClick(object sender, EventArgs e) {
        //    try {
        //        if ( moDisposal.getByKeyNext() ){
        //            doLoadSupportData();
        //        }
        //    }
        //    catch ( Exception ex ) {
        //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
        //    }        
        //}
		
        //void BtLastClick(object sender, EventArgs e) {
        //    try {
        //        if ( moDisposal.getByKeyLast() ){
        //            doLoadSupportData();
        //        }
        //    }
        //    catch ( Exception ex ) {
        //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
        //    }        
        //}

        //void doLoadSupportData() {
        //    doLoadAdditionalExpenses();
        //    doLoadDeductions();

        //    IDH_TARWEI.Text = "0.00";
        //    IDH_TRLTar.Text = "0.00";
        //}

        //private void doLoadAdditionalExpenses(){
        //    oAddGrid.doRefreshGridFromDBObject();
        //    moDisposal.DisposalRows.setUFValue("IDH_ADDCH",((IDH_DOADDEXP)oAddGrid.getGridControl().DBObject).ChargeTotal.ToString());
        //    moDisposal.DisposalRows.setUFValue("IDH_ADDCO",((IDH_DOADDEXP)oAddGrid.getGridControl().DBObject).CostTotal.ToString());
			
        //    //IDH_ADDCH.Text = ((IDH_DOADDEXP)oAddGrid.getGridControl().DBObject).ChargeTotal.ToString();
        //    //IDH_ADDCO.Text = ((IDH_DOADDEXP)oAddGrid.getGridControl().DBObject).CostTotal.ToString();
        //}

        //private void doLoadDeductions() {
        //    oDeductionGrid.doRefreshGridFromDBObject();
        //    moDisposal.DisposalRows.setUFValue("IDH_DEDWEI", ((IDH_DODEDUCT)oDeductionGrid.getGridControl().DBObject).WeightTotal.ToString());
        //    moDisposal.DisposalRows.setUFValue("IDH_DEDVAL", ((IDH_DODEDUCT)oDeductionGrid.getGridControl().DBObject).ValueTotal.ToString());
        //}
        
        //private void IDH_CONGEN_Click(object sender, EventArgs e)
        //{
        //    try {
        //        moDisposal.doGenerateConsignmentNo();
        //    }
        //    catch ( Exception ex ) {
        //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
        //    }        
        //}

//        public override void doPostInitializeComponent() {
//            try {
//                base.doPostInitializeComponent();

//                moDisposal.doInitialize();
//                moDisposal.doSetInitialFormState();
//                //moDisposal.Mode = FormBridge.Mode_FIND;
//                //moDisposal.doUpdateFormData();
//            } catch (Exception ex) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            } 
//        }

//        void MainFormLoad(object sender, EventArgs e) {
//            try {
//                moDisposal.doInitialize();
//                moDisposal.doSetInitialFormState();
//                //moDisposal.Mode = FormBridge.Mode_FIND;
//                //moDisposal.doUpdateFormData();
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }        
//        }
		
//        void IDH_REFClick(object sender, EventArgs e) {
//            try {
//                moDisposal.doReadWeight("CR");
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }        
//        }
		
//        void IDH_ACCBClick(object sender, EventArgs e)	{
//            try {
//                moDisposal.doReadWeight("ACCB");
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }        
//        }
		
//        void IDH_ACC1Click(object sender, EventArgs e)	{
//            try {
//                moDisposal.doReadWeight("ACC1");
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }        
//        }
		
//        void IDH_ACC2Click(object sender, EventArgs e) {
//            try {
//                moDisposal.doReadWeight("ACC2");
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }        
//        }
		
//        void IDH_TMRClick(object sender, EventArgs e) {
//            try {
//                //moDisposal.getRows().doReadWeight("CR");
//                if ( !moDisposal.TimerStarted ) {
//                    moDisposal.DoStartTimer();
//                    IDH_TMR.Text = "Stop Reading";
//                }
//                else {
//                    moDisposal.DoStopTimer();
//                    IDH_TMR.Text = "Start Reading";
//                }
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }        
//        }

//        void IDH_Tare1Click(object sender, EventArgs e) {
//            double dTar1 = Convert.ToDouble( IDH_TARWEI.Text );
//            double dTar2 = Convert.ToDouble( IDH_TRLTar.Text );

//            moDisposal.DisposalRows.doUseTareWei1(dTar1, dTar2);
//        }

//        void IDH_Tare2Click(object sender, EventArgs e) {
//            double dTar1 = Convert.ToDouble(IDH_TARWEI.Text);
//            double dTar2 = Convert.ToDouble(IDH_TRLTar.Text);

//            moDisposal.DisposalRows.doUseTareWei2(dTar1, dTar2);
//        }
		
//        void MainFormFormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e) {
//            try {
//                moDisposal.doClose();
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }        
//        }

//        /**
//         * Use this method to do Checks and Controls when the focus is lost from a control
//         */
//        private void All_Enter(object sender, EventArgs e)
//        {
//            try {
//                moLastEnter = sender;
//                if (sender != oPNavigation &&
//                     sender != btFind && sender != btAdd &&
//                     sender != btFirst && sender != btPrevious &&
//                     sender != btNext && sender != btLast )
//                {
//                    if (moLastLeave != null && moLastLeave != sender)
//                    {
//                        Control oLastLeave = (Control)moLastLeave;
//                        if (oLastLeave.Name == "IDH_VEHREG")
//                        {
//                            if (sender != IDH_WRORD && sender != IDH_WRROW &&
//                                sender != IDH_OS && sender != IDH_REGL && sender != IDH_WOCF )
//                            {
//                                if (!doRegCheck())
//                                    oLastLeave.Focus();
//                            }
//                        }
//                        else if (oLastLeave == IDH_CUST ||
//                            oLastLeave == IDH_CUSTNM ||
//                            oLastLeave == IDH_CARRIE ||
//                            oLastLeave == IDH_CARNAM ||
//                            oLastLeave == IDH_WPRODU ||
//                            oLastLeave == IDH_WNAM ||
//                            oLastLeave == IDH_DISSIT ||
//                            oLastLeave == IDH_DISNAM)
//                            {
//                            if ( !doBPCheck(oLastLeave) )
//                                oLastLeave.Focus();
//                        }
//                        else if (oLastLeave == IDH_WRORD ||
//                                  oLastLeave == IDH_WRROW)
//                        {
//                            if ( !doCheckLinkWO(oLastLeave) )
//                                oLastLeave.Focus();
//                        }
//                        else if (oLastLeave == IDH_ITMCOD ||
//                                 oLastLeave == IDH_DESC)
//                        {
//                            if (sender != IDH_ITCF)
//                            {
//                                if (!doCheckContainer(oLastLeave))
//                                    oLastLeave.Focus();
//                            }
//                        }
//                        else if (oLastLeave == IDH_WASCL1 ||
//                                oLastLeave == IDH_WASMAT)
//                        {
//                            if (sender != IDH_WASCF)
//                            {
//                                if (!doCheckWaste(oLastLeave))
//                                    oLastLeave.Focus();
//                            }
//                        }
//                        else if ( oLastLeave == IDH_ORIGIN ||
//                                  oLastLeave == IDH_ORIGIN2 ||
//                                  oLastLeave == IDH_RORIGI ) {
////	                    	if (sender != IDH_ORIGLU &&
////	                    	    sender != IDH_ORLK &&
////	                    	    sender != IDH_ORIGLU2 )
//                            if (sender != IDH_ORIGLU &&
//                                sender != IDH_ORLK ) {
	                    		
//                                string sCurrentValue = "";
//                                string sItemValue = oLastLeave.Text;

//                                sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._Origin);

//                                if (!sItemValue.Equals(sCurrentValue))	                    		
//                                    doOriginSearch();
//                            }
//                        }
//                        else if ( oLastLeave == IDH_DOORD || 
//                                  oLastLeave == IDH_DOARI ||
//                                  oLastLeave == IDH_AINV ||
//                                  oLastLeave == IDH_DOARIP ||
//                                  oLastLeave == IDH_FOC ||
//                                  oLastLeave == IDH_DOPO ) {
//                        }
//                        else 
//                            doUpdateDB();
//                    }
//                }
//                moLastLeave = null;
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }        
//        }

//        private void All_Leave(object sender, EventArgs e)
//        {
//            try {
//                if ( isEditableField( sender ) )
//                    moLastLeave = sender;
//                else
//                    moLastEnter = null;
	
//                if (sender == IDH_CUSCM)
//                    IDH_WASTTN.Focus();
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }        
//        }

//        private void All_TextChanged(object sender, EventArgs e)
//        {
//            try {
//                if (moDisposal.Mode == FormBridge.Mode_VIEW ||
//                    moDisposal.Mode == FormBridge.Mode_OK) {
//                    moLastLeave = sender;
        			
//                    string sName = ((Control)sender).Name;
//                    //if (!moDisposal.IsBusyUpdatingForm) {
//                        if (Array.IndexOf(soIgnoreControls, sName) == -1) {
//                            moDisposal.Mode = FormBridge.Mode_UPDATE; //doSetToUpdateMode();
//                            btn1.Text = FormBridge.Mode_UPDATE;
//                        }
//                    //}
//                }
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }        
//        }

//        private void IDH_WRROW_Click(object sender, EventArgs e)
//        {
//            try {
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }        
//        }

//        void All_Validated(object sender, EventArgs e) {
//            try {
//                //Control oControl = (Control)sender;
//                //if ((oControl is TextBox && ((TextBox)oControl).Modified) ||
//                //    !(oControl is TextBox))
//                //{
//                //    string sValue = moDisposal.getFormFieldValue(oControl);
//                //    if (!moDisposal.doUpdateDBFieldValue(oControl.Name, sValue))
//                //    {
//                //        moDisposal.DisposalRows.doUpdateDBFieldValue(oControl.Name, sValue);
//                //    }
	
//                //    moDisposal.doHandleFieldChange(oControl.Name);
//                //}
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }
//        }

//        /**
//         * Only update fields where the Edit form fields has been changed
//         */
//        private void doUpdateDB()
//        {
//            if ( moLastLeave == null )
//                return;
            
//            Control oControl = (Control)moLastLeave;
//            if (    (oControl is TextBox && ((TextBox)oControl).Modified) ||
//                !(oControl is TextBox))
//            {
//                moDisposal.setWasSetFromUser( oControl.Name );
            	
//                moDisposal.doUpdateDBFieldFromFormField(oControl);

//                moDisposal.doHandleFieldChange(oControl.Name);

//                if (oControl is TextBox)
//                {
//                    ((TextBox)oControl).Modified = false;
//                }
//            }

//            //Control oControl = (Control)moLastLeave;
//            //if ((oControl is TextBox && ((TextBox)oControl).Modified) ||
//            //    !(oControl is TextBox))
//            //{
//            //    string sValue = moDisposal.getFormFieldValue(oControl);
//            //    if (!moDisposal.doUpdateDBFieldValue(oControl.Name, sValue))
//            //    {
//            //        moDisposal.DisposalRows.doUpdateDBFieldValue(oControl.Name, sValue);
//            //    }

//            //    //Now update all the Form fields linked to this DBField
//            //    moDisposal.doHandleFieldChange(oControl.Name);
//            //}
//        }

//        //private void BPValidating(object sender, System.ComponentModel.CancelEventArgs e)
//        //{
//        //    Control oControl = (Control)sender;
//        //    string sFieldName = oControl.Name;

//        //    if (oControl == IDH_CUST ||
//        //        oControl == IDH_CUSTNM || 
//        //        oControl == IDH_CARRIE ||
//        //        oControl == IDH_CARNAM ||
//        //        oControl == IDH_WPRODU ||
//        //        oControl == IDH_WNAM ||
//        //        oControl == IDH_DISSIT ||
//        //        oControl == IDH_DISNAM )
//        //    {
//        //        string sCurrentValue = "";
//        //        string sItemValue = oControl.Text;

//        //        if ( oControl == IDH_CUST )
//        //            sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._CardCd);
//        //        else if (oControl == IDH_CUSTNM)
//        //            sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._CardNM);
//        //        else if (oControl == IDH_CARRIE)
//        //            sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._CCardCd);
//        //        else if (oControl == IDH_CARNAM)
//        //            sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._CCardNM);
//        //        else if (oControl == IDH_WPRODU)
//        //            sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._PCardCd);
//        //        else if (oControl == IDH_WNAM)
//        //            sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._PCardNM);
//        //        else if (oControl == IDH_DISSIT)
//        //            sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._SCardCd);
//        //        else if (oControl == IDH_DISNAM)
//        //            sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._SCardNM);

//        //        if (!sItemValue.Equals(sCurrentValue))
//        //        {
//        //            if (!doBPSearch(sFieldName, true))
//        //            {
//        //                e.Cancel = true;
//        //            }
//        //        }
//        //    }
//        //}
//        private bool doBPCheck(Control oControl)
//        {
//            string sFieldName = oControl.Name;

//            string sCurrentValue = "";
//            string sItemValue = oControl.Text;

//            if (oControl == IDH_CUST)
//                sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._CardCd);
//            else if (oControl == IDH_CUSTNM)
//                sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._CardNM);
//            else if (oControl == IDH_CARRIE)
//                sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._CCardCd);
//            else if (oControl == IDH_CARNAM)
//                sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._CCardNM);
//            else if (oControl == IDH_WPRODU)
//                sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._PCardCd);
//            else if (oControl == IDH_PROCF)
//                sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._PCardCd);
//            else if (oControl == IDH_WNAM)
//                sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._PCardNM);
//            else if (oControl == IDH_DISSIT)
//                sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._SCardCd);
//            else if (oControl == IDH_DISNAM)
//                sCurrentValue = moDisposal.getFormDFValueAsString(IDH_DISPORD._SCardNM);

//            if (!sItemValue.Equals(sCurrentValue))
//            {
//                if (!doBPSearch( oControl, true, false))
//                {
//                    return false;
//                }
//            }
//            return true;
//        }


//        //private void REGValidating(object sender, System.ComponentModel.CancelEventArgs e)
//        //{
//        //    string sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._TRLReg);
//        //    string sItemValue = IDH_VEHREG.Text;
//        //    if (!sItemValue.Equals(sCurrentValue))
//        //    {
//        //        if (!doVehicleSearch(false, true))
//        //            e.Cancel = true;
//        //    }
//        //}
//        private bool doRegCheck()
//        {
//            string sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._Lorry);
//            string sItemValue = IDH_VEHREG.Text;

//            if (!sItemValue.Equals(sCurrentValue))
//            {
//                if (!doVehicleSearch(false, true, false))
//                    return false;
//            }
//            return true;
//        }
		
//        //private void LinkWOValidating(object sender, System.ComponentModel.CancelEventArgs e) {
//        //    Control oControl = (Control)sender;
//        //    string sCurrentValue = "";
//        //    string sItemValue = oControl.Text;

//        //    if (oControl == IDH_WRORD)
//        //        sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._WROrd);
//        //    else if (oControl == IDH_WRROW)
//        //        sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._WRRow);

//        //    if (!sItemValue.Equals(sCurrentValue))
//        //    {
//        //        if (!doLinkWOSearch(true))
//        //            e.Cancel = false;
//        //    }
//        //}
//        private bool doCheckLinkWO(Control oControl)
//        {
//            string sCurrentValue = "";
//            string sItemValue = oControl.Text;

//            if (oControl == IDH_WRORD)
//                sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._WROrd);
//            else if (oControl == IDH_WRROW)
//                sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._WRRow);

//            if (!sItemValue.Equals(sCurrentValue))
//            {
//                if (!doLinkWOSearch(true, false))
//                    return false;
//            }
//            return true;
//        }
        
//        //private void ContainerValidating(object sender, System.ComponentModel.CancelEventArgs e) {
//        //    Control oControl = (Control)sender;
//        //    string sCurrentValue = "";
//        //    string sItemValue = oControl.Text;

//        //    if ( oControl == IDH_ITMCOD )
//        //        sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._ItemCd);
//        //    else if ( oControl == IDH_DESC )
//        //        sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._ItemDsc);

//        //    if (!sItemValue.Equals(sCurrentValue))
//        //    {
//        //        if (!doContainerSearch(true))
//        //            e.Cancel = false;
//        //    }
//        //}
//        private bool doCheckContainer( Control oControl )
//        {
//            string sCurrentValue = "";
//            string sItemValue = oControl.Text;

//            if (oControl == IDH_ITMCOD)
//                sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._ItemCd);
//            else if (oControl == IDH_DESC)
//                sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._ItemDsc);

//            if (!sItemValue.Equals(sCurrentValue))
//            {
//                if (!doContainerSearch(true, false))
//                    return false;
//            }
//            return true;
//        }
        
//        //private void WasteValidating(object sender, System.ComponentModel.CancelEventArgs e) {
//        //    Control oControl = (Control)sender;
//        //    string sCurrentValue = "";
//        //    string sItemValue = oControl.Text;

//        //    if (oControl == IDH_WASCL1)
//        //        sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._WasCd);
//        //    else if (oControl == IDH_WASMAT)
//        //        sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._WasDsc);

//        //    if (!sItemValue.Equals(sCurrentValue))
//        //    {
//        //        if (!doWasteSearch(true))
//        //            e.Cancel = false;
//        //    }
//        //}
//        private bool doCheckWaste(Control oControl)
//        {
//            string sCurrentValue = "";
//            string sItemValue = oControl.Text;

//            if (oControl == IDH_WASCL1)
//                sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._WasCd);
//            else if (oControl == IDH_WASMAT)
//                sCurrentValue = moDisposal.DisposalRows.getFormDFValueAsString(IDH_DISPROW._WasDsc);

//            if (!sItemValue.Equals(sCurrentValue))
//            {
//                if ( !Config.INSTANCE.getParameterAsBool("DOCWD", true ) && oControl == IDH_WASMAT ) {
//                    sItemValue = sItemValue.Trim();
//                    if ( sItemValue.Length == 0 ||
//                        ( !sItemValue.StartsWith("*") &&
//                         !sItemValue.EndsWith("*") ) ){
//                        moDisposal.DisposalRows.setFormDFValueFromUser(IDH_DISPROW._WasDsc, sItemValue);
//                        return true;
//                    }
//                }
//                if (!doWasteSearch(true, false)) {
//                    if (oControl == IDH_WASCL1)
//                        IDH_WASCL1.Text = sItemValue;
//                    else if (oControl == IDH_WASMAT)
//                        IDH_WASMAT.Text = sItemValue;
//                    return false;
//                }
//            }
//            return true;
//        }


//        void All_ComboChanged(object sender, EventArgs e) {
//            try {
//                //ComboBox oComboBox = (ComboBox)sender;
	
//                //string sValue = moDisposal.getFormFieldValue(oComboBox);
//                string sValue = moDisposal.getFormFieldValue((Control)sender);
//                if (!moDisposal.doUpdateDBFieldValueUsingFormFieldId(((Control)sender).Name, sValue))
//                {
//                    moDisposal.DisposalRows.doUpdateDBFieldValueUsingFormFieldId(((Control)sender).Name, sValue);
//                }
	
//                //if (moDisposal.Mode != FormBridge.Mode_FIND && moDisposal.Mode != FormBridge.Mode_STARTUP)
//                //if (oComboBox.Name.Equals("IDH_JOBTTP")) {
//                if ( ((Control)sender).Name.Equals("IDH_JOBTTP")) {
//                    if (mbDoBP) {
//                        //moDisposal.doHandleFieldChange(oComboBox.Name);
//                        //if (oComboBox.Name.Equals("IDH_JOBTTP")) {
//                            moDisposal.DisposalRows.doJobTypeChange();
//                        //} 
//                        mbDoBP = false;
//                    }
//                } else {
//                    //moDisposal.DisposalRows.doHandleFieldChange(oComboBox.Name);
//                    moDisposal.DisposalRows.doHandleFieldChange(((Control)sender).Name);
//                }

//                //if ( mbDoBP )
//                //{
//                //    //moDisposal.doHandleFieldChange(oComboBox.Name);
//                //    if (oComboBox.Name.Equals("IDH_JOBTTP")) {
//                //        moDisposal.DisposalRows.doJobTypeChange();
//                //    } else {
//                //        moDisposal.DisposalRows.doHandleFieldChange(oComboBox.Name);
//                //    }
//                //    mbDoBP = false;
//                //}
	
//                if (moDisposal.Mode == FormBridge.Mode_VIEW || 
//                    moDisposal.Mode == FormBridge.Mode_OK)
//                {
//                    moDisposal.Mode = FormBridge.Mode_UPDATE;
//                    btn1.Text = FormBridge.Mode_UPDATE;
//                }
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }	            
//        }

//        //private void IDH_JOBTTP_SelectionChangeCommitted(object sender, EventArgs e)
//        //{
//        //    try {
//        //        if (moDisposal.Mode != FormBridge.Mode_FIND && moDisposal.Mode != FormBridge.Mode_STARTUP)
//        //            mbDoBP = true;
//        //            //moDisposal.DisposalRows.doJobTypeChange();
//        //    }
//        //    catch ( Exception ex ) {
//        //        DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//        //    }	                
//        //}
		
//#region searches
//        ///*
//        // * Check for auto search fields
//        // */
//        //public bool doAutoSearches(string sFieldName, bool bFromFocus )
//        //{
//        //    if (sFieldName.Equals("IDH_CUST") ||
//        //        sFieldName.Equals("IDH_CUSTNM") || 
//        //        sFieldName.Equals("IDH_CARRIE") ||
//        //        sFieldName.Equals("IDH_CARNAM") ||
//        //        sFieldName.Equals("IDH_WPRODU") ||
//        //        sFieldName.Equals("IDH_WPNAM") ||
//        //        sFieldName.Equals("IDH_DISST") ||
//        //        sFieldName.Equals("IDH_DISNAM") )
//        //    {
//        //        return doBPSearch(sFieldName, bFromFocus );
//        //    }
//        //    //if (sFieldName.Equals("IDH_PRONM"))
//        //    //{
//        //    //    string sName = moDisposal.getItemValue("IDH_PRONM");
//        //    //    if (sName.StartsWith("*") || sName.EndsWith("*"))
//        //    //    {
//        //    //        moDisposal.setItemValue( "IDH_CUST", "");
//        //    //    }
//        //    //    doChooseProducer(ref oForm, true);
//        //    //}
//        //    //else if (sFieldName.Equals("IDH_ONCS"))
//        //    //{
//        //    //    string sCs = getItemValue(ref oForm, "IDH_ONCS");
//        //    //    if (sCs.StartsWith("*") || sCs.EndsWith("*"))
//        //    //    {
//        //    //        setItemValue(ref oForm, ref "IDH_ONCS", "");
//        //    //        doChooseComplianceScheme(ref oForm, true);
//        //    //    }
//        //    //}
//        //    //else if (sFieldName.Equals("IDH_WASMAT"))
//        //    //{
//        //    //    string sDesc = getItemValue(ref oForm, "IDH_WASMAT");
//        //    //    if (sDesc.StartsWith("*") || sDesc.EndsWith("*"))
//        //    //    {
//        //    //        setSharedData(ref oForm, "TP", "WAST");
//        //    //        setSharedData(ref oForm, "IDH_GRPCOD", idh.@const.Lookup.INSTANCE.doWasteMaterialGroup(ref goParent));
//        //    //        setSharedData(ref oForm, "IDH_CRDCD", getDFValue(ref oForm, "@IDH_DISPORD", "U_CardCd"));
//        //    //        setSharedData(ref oForm, "IDH_ITMNAM", sDesc);
//        //    //        setSharedData(ref oForm, "IDH_INVENT", "N");
//        //    //        goParent.doOpenModalForm("IDHWISRC", ref oForm);
//        //    //    }
//        //    //}
//        //    //else if (sFieldName.Equals("IDH_DESC"))
//        //    //{
//        //    //    string sDesc = getDFValue(ref oForm, msRowTable, "U_ItemDsc");
//        //    //    if (sDesc.StartsWith("*") || sDesc.EndsWith("*"))
//        //    //    {
//        //    //        setSharedData(ref oForm, "TRG", "PROD");
//        //    //        setSharedData(ref oForm, "IDH_ITMCOD", "");
//        //    //        setSharedData(ref oForm, "IDH_GRPCOD", idh.@const.Lookup.INSTANCE.getParamater("MDDDIG"));
//        //    //        setSharedData(ref oForm, "IDH_ITMNAM", sDesc);
//        //    //        goParent.doOpenModalForm("IDHISRC", ref oForm);
//        //    //    }
//        //    //}
//        //    //else if (sFieldName.Equals("IDH_VEHREG"))
//        //    //{
//        //    //    string sAct = getWFValue(ref oForm, "LastVehAct");
//        //    //    if (sAct == "DOSEARCH")
//        //    //    {
//        //    //        string sReg = getDFValue(ref oForm, msRowTable, "U_Lorry", true);
//        //    //        bool bDoSetReg = false;
//        //    //        if (sReg.StartsWith("!"))
//        //    //        {
//        //    //            setSharedData(ref oForm, "SHOWONSITE", "TRUE");
//        //    //            sReg = sReg.Substring(1);
//        //    //            bDoSetReg = true;
//        //    //        }
//        //    //        else
//        //    //        {
//        //    //            setSharedData(ref oForm, "SHOWACTIVITY", "FALSE");
//        //    //            setSharedData(ref oForm, "SHOWONSITE", "FALSE");
//        //    //        }
//        //    //        string sRegChanged = idh.forms.admin.VehicleMaster.doFixReg(sReg);
//        //    //        if (sRegChanged.Equals(sReg) == false || bDoSetReg == true)
//        //    //        {
//        //    //            setDFValue(ref oForm, msRowTable, "U_Lorry", sRegChanged, true);
//        //    //        }
//        //    //        if (idh.@const.Lookup.INSTANCE.getParamaterAsBool("DOVMSUC", true))
//        //    //        {
//        //    //            string sCustomer = null;
//        //    //            sCustomer = getDFValue(ref oForm, "@IDH_DISPORD", "U_CardCd", true);
//        //    //            setSharedData(ref oForm, "IDH_VEHCUS", sCustomer);
//        //    //        }
//        //    //        else
//        //    //        {
//        //    //            setSharedData(ref oForm, "IDH_VEHCUS", "");
//        //    //        }

//        //    //        string sJobTp = getDFValue(ref oForm, msRowTable, "U_JobTp", true);
//        //    //        setSharedData(ref oForm, "JOBTYPE", sJobTp);

//        //    //        getDFValue(ref oForm, msRowTable, "U_Lorry", true);
//        //    //        setSharedData(ref oForm, "IDH_VEHREG", sRegChanged);
//        //    //        setSharedData(ref oForm, "SHOWCREATE", "TRUE");
//        //    //        setSharedData(ref oForm, "IDH_WR1OT", "^DO");
//        //    //        setSharedData(ref oForm, "SILENT", "SHOWMULTI");
//        //    //        goParent.doOpenModalForm("IDHLOSCH", ref oForm);
//        //    //    }
//        //    //}
//        //    return true;
//        //}

//        void IDH_OSClick(object sender, EventArgs e) {
//            try {
//                if (!doVehicleSearch(true, false, false) && moLastLeave != null)
//                    ((Control)moLastLeave).Focus();
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }            	
//        }
		
//        void IDH_REGLButton1Click(object sender, EventArgs e) {
//            try {
//                if (!doVehicleSearch(false, false, false) && moLastLeave != null)
//                    ((Control)moLastLeave).Focus();
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }            	
//        }
		
//        public bool doVehicleSearch( bool bOnsite, bool bFromFocus, bool bIgnoreData ){
//            com.idh.wr1.form.search.Lorry oSearch = new com.idh.wr1.form.search.Lorry();
//            oSearch.Target = "LORRY";
//            if (bOnsite)
//                oSearch.doSetupControlOnsiteSearch();
//            else
//                oSearch.doSetupControlNormalSearch();
		    
////		   oSearch.Bridge.getWinControl("btCreate").Visible = true;		    
//            object oDialog = oSearch;
//            if (moDisposal.doSetVehicleSearchParams(ref oDialog, "LORRY", true, bFromFocus, bIgnoreData))
//            {
//                DialogResult oResult = oSearch.ShowDialog();
//                if (oResult == DialogResult.OK)
//                {
//                    moDisposal.doHandleLorryResult(ref oDialog, bFromFocus, false );
//                    return true;
//                }
//                else if (oResult == DialogResult.Ignore)
//                {
//                    moDisposal.doHandleLorryResult(ref oDialog, bFromFocus, true);
//                    return true;
//                }
//                else
//                    return false;
//            }
//            else
//                return true;
//        }

//        void LinkWOClick(object sender, EventArgs e) {
//            try {
//                if (!doLinkWOSearch(false, false) && moLastLeave != null)
//                    ((Control)moLastLeave).Focus();
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }	            
//        }

//        private bool doLinkWOSearch(bool bFromFocus, bool bIgnoreData )
//        {
//            com.idh.wr1.form.search.LinkWO oSearch = new com.idh.wr1.form.search.LinkWO();
//            oSearch.Target = "LINKWO";

//            string sOrd = IDH_BOOREF.Text;
//            string sRow = IDH_ROW.Text;
//            oSearch.doSetupControl(sOrd, sRow);

//            object oDialog = oSearch;
//            if (moDisposal.doSetLinkWOParams(ref oDialog, "LINKWO", true, bFromFocus, bIgnoreData))
//            {
//                DialogResult oResult = oSearch.ShowDialog();
//                if (oResult == DialogResult.OK)
//                {
//                    moDisposal.doHandleLinkWOResult(ref oDialog);
//                    return true;
//                }
//                else
//                    return false;
//            }
//            return false;
//        }

//        void BPSearchClick(object sender, EventArgs e)
//        {
//            try {
//                if (!doBPSearch( (Control)sender, false, false) && moLastLeave != null)
//                    ((Control)moLastLeave).Focus();
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }    
//        }

//        private bool doBPSearch(Control oControl, bool bFromFocus, bool bIgnoreData)
//        {
//            try {
				
//                string sIsCS = null;
				
//                string sTarget;
//                string sTitle;
//                if ( oControl == IDH_CUSL || oControl == IDH_CUST )
//                {
//                    sTarget = DisposalOrder.BP_CUSTOMER;
//                    sTitle = "Customer Search";
//                }
//                else if ( oControl == IDH_CUSTNM )
//                {
//                    sTarget = DisposalOrder.BP_CUSTOMERNAME;
//                    sTitle = "Customer Search";
//                }
//                else if ( oControl == IDH_CARL || oControl == IDH_CARRIE )
//                {
//                    sTarget = DisposalOrder.BP_CARRIER;
//                    sTitle = "Waste Carrier Search";
//                }
//                else if ( oControl == IDH_CARNAM )
//                {
//                    sTarget = DisposalOrder.BP_CARRIERNAME;
//                    sTitle = "Waste Carrier Search";
//                }
//                else if ( oControl == IDH_PRDL || oControl == IDH_WPRODU || oControl == IDH_PROCF )
//                {
//                    sTarget = DisposalOrder.BP_PRODUCER;
//                    sTitle = "Producer Search";
//                }
//                else if ( oControl == IDH_WNAM )
//                {
//                    sTarget = DisposalOrder.BP_PRODUCERNAME;
//                    sTitle = "Producer Search";
//                }
//                else if ( oControl == IDH_SITL || oControl == IDH_DISSIT )
//                {
//                    sTarget = DisposalOrder.BP_DISPOSALSITE;
//                    sTitle = "DisposalSite Search";
//                }
//                else if ( oControl == IDH_DISNAM )
//                {
//                    sTarget = DisposalOrder.BP_DISPOSALSITENAME;
//                    sTitle = "DisposalSite Search";
//                }
//                else if ( oControl == IDH_CSCF ) {
//                    sTarget = DisposalOrder.BP_COMPLIANCE;
//                    sTitle = "Compliance Scheme Search";
//                    sIsCS = "Y";
//                }
//                else {
//                    sTarget = DisposalOrder.BP_CUSTOMER;
//                    sTitle = "Business Partner Search";
//                }
				
//                com.idh.wr1.form.search.BP oSearch = new com.idh.wr1.form.search.BP(sIsCS);
//                object oDialog = oSearch;
//                //oSearch.doSetupControl(sIsCS);
	
//                oSearch.Text = sTitle;
	
//                if (moDisposal.doSetBPSearchParams(ref oDialog, sTarget, true, bFromFocus, bIgnoreData))
//                {
//                    if (oSearch.ShowDialog(this) == DialogResult.OK)
//                    {
//                        if ( !moDisposal.doHandleBPResult(ref oDialog, sTarget) ){
//                            if ( sTarget.Equals(DisposalOrder.BP_CUSTOMER) ||
//                                sTarget.Equals(DisposalOrder.BP_CUSTOMERNAME) ){
//                                IDH_CUST.Text = "";
//                                IDH_CUSTNM.Text = "";
//                            }
//                            else if ( sTarget.Equals(DisposalOrder.BP_CARRIER) ||
//                                sTarget.Equals(DisposalOrder.BP_CARRIERNAME) ){
//                                IDH_CARRIE.Text = "";
//                                IDH_CARNAM.Text = "";
//                            }
//                            else if ( sTarget.Equals(DisposalOrder.BP_PRODUCER) ||
//                                sTarget.Equals(DisposalOrder.BP_PRODUCERNAME) ){
//                                IDH_WPRODU.Text = "";
//                                IDH_WNAM.Text = "";
//                            }
//                            else if ( sTarget.Equals(DisposalOrder.BP_DISPOSALSITE) ||
//                                sTarget.Equals(DisposalOrder.BP_DISPOSALSITENAME) ){
//                                IDH_DISSIT.Text = "";
//                                IDH_DISNAM.Text = "";
//                            }
//                            return false;
//                        }
//                        else {
//                            if ( sTarget.Equals(DisposalOrder.BP_CUSTOMER) ){
//                                if ( moDisposal.Mode == FormBridge.Mode_ADD || moDisposal.Mode == FormBridge.Mode_UPDATE || moDisposal.Mode == FormBridge.Mode_OK ){
//                                    oAddGrid.doRefreshGridFromDBObject();
//                                    oDeductionGrid.doRefreshGridFromDBObject();
//                                }
//                                oAddGrid.Refresh();
//                                oDeductionGrid.Refresh();
//                            }
//                            return true;
//                        }
//                    }
//                    else
//                        return false;
//                }
//                else
//                {
//                    return true;
//                }
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//                return false;
//            }
//        }

//        void AddressSearchClick(object sender, EventArgs e)
//        {
//            try {
//                if (!doAddressSearch((Control)sender, false) && moLastLeave != null)
//                    ((Control)moLastLeave).Focus();
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//            }	            
//        }

//        private bool doAddressSearch( Control oControl, bool bFromFocus)
//        {
//            try {
//                com.idh.wr1.form.search.Address oSearch = new com.idh.wr1.form.search.Address();
//                //oSearch.doSetupControl();
	
//                object oDialog = oSearch;
//                string sTarget;
//                string sTitle;
//                if (oControl == IDH_CUSAL)
//                {
//                    sTarget = DisposalOrder.BP_CUSTOMER;
//                    sTitle = "Customer Search";
//                }
//                else if (oControl == IDH_WASAL)
//                {
//                    sTarget = DisposalOrder.BP_CARRIER;
//                    sTitle = "Waste Carrier Search";
//                }
//                else if (oControl == IDH_PROAL)
//                {
//                    sTarget = DisposalOrder.BP_PRODUCER;
//                    sTitle = "Producer Search";
//                }
//                else if (oControl == IDH_SITAL)
//                {
//                    sTarget = DisposalOrder.BP_DISPOSALSITE;
//                    sTitle = "DisposalSite Search";
//                }
//                else {
//                    sTarget = DisposalOrder.BP_CUSTOMER;
//                    sTitle = "Business Partner Search";
//                }
//                oSearch.Text = sTitle;
				
//                moDisposal.doSetAddressSearchParams( ref oDialog, sTarget, true );
				
//                if ( oSearch.ShowDialog(this) == DialogResult.OK ) {
//                    moDisposal.doHandleAddressResult( ref oDialog, sTarget );
//                    return false;
//                }
//                return true;
//            }
//            catch ( Exception ex ) {
//                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
//                return false;
//            }
//        }
		
//        void IDH_WASCFClick(object sender, EventArgs e)	{
//            if (!doWasteSearch(false, false) && moLastLeave != null)
//                ((Control)moLastLeave).Focus();
//        }

//        private bool doWasteSearch(bool bFromFocus, bool bIgnoreData)
//        {
//            com.idh.wr1.form.search.WasteItem oSearch = new com.idh.wr1.form.search.WasteItem();
//            //oSearch.doSetupControl();

//            object oDialog = oSearch;
//            moDisposal.doSetWasteItemSearchParams(ref oDialog, true, bFromFocus, bIgnoreData);

//            if (oSearch.ShowDialog(this) == DialogResult.OK)
//            {
//                moDisposal.doHandleWasteItemResult(ref oDialog);
//                if ( moDisposal.Mode == FormBridge.Mode_ADD || moDisposal.Mode == FormBridge.Mode_UPDATE || moDisposal.Mode == FormBridge.Mode_OK ){
//                    oAddGrid.doRefreshGridFromDBObject();
//                    oDeductionGrid.doRefreshGridFromDBObject();
//                }
//                return true;
//            }
//            else {
//                return false;
//            }
//        }
		
//        void IDH_ITCFClick(object sender, EventArgs e)	{
//            if (!doContainerSearch(false, false) && moLastLeave != null)
//                ((Control)moLastLeave).Focus();
//        }

//        private bool doContainerSearch(bool bFromFocus, bool bIgnoreData)
//        {
//            com.idh.wr1.form.search.WRItem oSearch = new com.idh.wr1.form.search.WRItem();
//            oSearch.doSetupControl();

//            object oDialog = oSearch;
//            moDisposal.doSetItemSearchParams(ref oDialog, true, bFromFocus, bIgnoreData);

//            if (oSearch.ShowDialog(this) == DialogResult.OK)
//            {
//                moDisposal.doHandleItemResult(ref oDialog);
//                return true;
//            }
//            else
//                return true;
//        }
		
//        void IDH_ORLKClick(object sender, EventArgs e)
//        {
//            if (!doOriginSearch() && moLastLeave != null)
//                ((Control)moLastLeave).Focus();
//        }	
	
//        private bool doOriginSearch(){
//            com.idh.wr1.form.search.Origin oSearch = new com.idh.wr1.form.search.Origin();
//            oSearch.doSetupControl();

//            object oDialog = oSearch;
//            moDisposal.doSetOriginSearchParams(ref oDialog, true);

//            if (oSearch.ShowDialog(this) == DialogResult.OK)
//            {
//                moDisposal.doHandleOriginResult(ref oDialog);
//                return true;
//            }
//            else
//                return false;
//        }
//#endregion
//        void IDH_NOVATCheckedChanged(object sender, EventArgs e)
//        {
//            moDisposal.DisposalRows.doSwitchVatGroups();
//        }
		
//        void IDH_ACCCheckedChanged(object sender, EventArgs e)
//        {
//            //moDisposal.DisposalRows.doSwitchAccountingCheckBox(((Control)sender).Name, false, true);

//            CheckBox oCheckBox = (CheckBox)sender;
//            if (oCheckBox.Checked)
//            {
//                moDisposal.DisposalRows.doCheckAccountingCheckBox(oCheckBox.Name, false);
//                moDisposal.DisposalRows.doSwitchAccountingValues(oCheckBox.Name);
//            }
//        }
		
//        void IDH_WEIBRGSelectedIndexChanged(object sender, EventArgs e)
//        {
//            string sID = moDisposal.getUFValue(((Control)sender).Name);
//            moDisposal.DisposalRows.doConfigWeighBridge( sID );
//            moDisposal.SetWeighBridgeNoSwitch(sID);
//        }

//        private void IDH_DOC_Click(object sender, EventArgs e)
//        {
//            moDisposal.doDocReport();
//        }

//        private void IDH_CON_Click(object sender, EventArgs e)
//        {
//            moDisposal.doConvReport();
//        }

//        private void MainForm_KeyDown(object sender, KeyEventArgs e) {
//            if (e.Control)
//            {
//                string sKey = e.KeyCode.ToString();
//                if (sKey == "L")
//                {
//                    if (IDH_REGL.Enabled)
//                        doVehicleSearch(false, false, e.Alt);

//                    e.Handled = true;
//                }
//                else
//                if (sKey == "O")
//                {
//                    if (IDH_OS.Enabled)
//                        doVehicleSearch(true, false, e.Alt);

//                    e.Handled = true;
//                }
//                else
//                if (sKey == "I")
//                {
//                    if (IDH_WOCF.Enabled)
//                        doLinkWOSearch(false, e.Alt);

//                    e.Handled = true;
//                }
//                else
//                if (sKey == "U")
//                {
//                    if (IDH_CUSL.Enabled)
//                        doBPSearch(IDH_CUST, false, e.Alt);

//                    e.Handled = true;
//                }
//                else
//                if (sKey == "R")
//                {
//                    if (IDH_CARL.Enabled)
//                        doBPSearch(IDH_CARRIE, false, e.Alt);

//                    e.Handled = true;
//                }
//                else
//                if (sKey == "P")
//                {
//                    if (IDH_PRDL.Enabled)
//                        doBPSearch(IDH_WPRODU, false, e.Alt);

//                    e.Handled = true;
//                }
//                else
//                if (sKey == "D")
//                {
//                    if (IDH_SITL.Enabled)
//                        doBPSearch(IDH_DISSIT, false, e.Alt);

//                    e.Handled = true;
//                }
//                else
//                if (sKey == "W")
//                {
//                    if (IDH_WASCF.Enabled)
//                        doWasteSearch(false, e.Alt);

//                    e.Handled = true;
//                }
//                else
//                if (sKey == "B")
//                {
//                    if (IDH_ITCF.Enabled)
//                        doContainerSearch(false, e.Alt);

//                    e.Handled = true;
//                }
//                else
//                if (sKey == "F"){
//                    moDisposal.Mode = FormBridge.Mode_FIND; 
//                    btn1.Text = FormBridge.Mode_FIND;
//                    e.Handled = true;
//                }
//                else
//                if (sKey == "A"){
//                    moDisposal.Mode = FormBridge.Mode_ADD;
//                    e.Handled = true;
//                }
//                else
//                if (sKey == "Down"){
//                    moDisposal.getByKeyFirst();
//                    e.Handled = true;
//                }
//                else
//                if (sKey == "Left"){
//                    moDisposal.getByKeyPrevious();
//                    e.Handled = true;
//                }
//                else
//                if (sKey == "Right"){
//                    moDisposal.getByKeyNext();
//                    e.Handled = true;
//                }
//                else
//                if (sKey == "Up"){
//                    moDisposal.getByKeyLast();
//                    e.Handled = true;
//                }
//                else
//                if (sKey == "1"){
//                    moDisposal.doReadWeight("ACC1");
//                    e.Handled = true;
//                }
//                else
//                if (sKey == "2") {
//                    moDisposal.doReadWeight("ACC2");
//                    e.Handled = true;
//                }
//            }
//            else
//            if ((int)e.KeyCode == 112)
//            {
//                WR1Help oHelp = new WR1Help();
//                string sFile = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\help.html"; ;
//                oHelp.setHelpFile(sFile);
//                oHelp.Show();

//                e.Handled = true;
//            }
//            /**
//             * Compliance Scheme - IDH_CSCF
//             * Carrier - IDH_CARL
//             * Origin - IDH_ORLK
//             */
//        }

//        private void btnHelp_Click(object sender, EventArgs e)
//        {
//            WR1Help oHelp = new WR1Help();
//            string sFile = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\help.html"; ;
//            oHelp.setHelpFile(sFile);
//            oHelp.Show();
//        }
		
//        void Stl01Click(object sender, EventArgs e)
//        {
//            doShowErrorWindow();
//        }
		
//        private void doShowErrorWindow(){
//            DataHandler oData = DataHandler.INSTANCE;
//            ErrorBuffer oErrs = oData.ErrorBuffer;
//            if ( oErrs != null && oErrs.Buffer.Count > 0 ) {
//                InfoForm oInfo = new InfoForm( oErrs );
//                oInfo.Show();
//            }
//        }
		
//        void ToolStripStatusLabel1Click(object sender, EventArgs e)
//        {
//            doShowErrorWindow();
//        }
		
//        void StatusStrip1ItemClicked(object sender, ToolStripItemClickedEventArgs e)
//        {
//            doShowErrorWindow();
//        }
	}
}
