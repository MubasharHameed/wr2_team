﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2010/02/06
 * Time: 06:24 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;

using com.idh.bridge.error;

namespace com.idh.wr1.form 
{
	/// <summary>
	/// Information Display
	/// </summary>
	public partial class InfoForm : Form
	{
		public static string sKeyDelimiter = " -> ";
		public ErrorBuffer moErrorBuffer = null;

        public InfoForm() {
            InitializeComponent();

        }

		public InfoForm(ErrorBuffer oErrors)
		{
			InitializeComponent();
			moErrorBuffer = oErrors;
			
			if ( moErrorBuffer != null && moErrorBuffer.Buffer.Count > 0 ) {
				ErrorMessage oErr = moErrorBuffer.getLastMessage();
				setInfo(oErr);
				
				// Fill the error combo box
				ArrayList oErrs = moErrorBuffer.Buffer;
				for ( int i = oErrs.Count -1; i >= 0; i-- ) {
					oErr = (ErrorMessage)oErrs[i];
					addError( oErr.Key, oErr.DisplayMessage );
				}
			}
		}
		
		public void setInfo(ErrorMessage oErr) {
			string sExtendedError = "Key: " + oErr.Key + '\n' +
									"User: " + oErr.User + '\n' +
    						  		"Class: " + oErr.Class + '\n' +
    						  		"Function: " + oErr.Function + '\n' +
									"Date: " + oErr.MessageDate + "\n\n" +
    						  		"Detail: \n" + oErr.DetailMessage;
			
			InfoTxt.Text = oErr.DisplayMessage + "\n\n" + sExtendedError;
		}


        public void doWriteLine(string sMessage, Color oColor) {
            int iStart = InfoTxt.TextLength;
            InfoTxt.AppendText(sMessage);
            InfoTxt.AppendText("\n");
            int iEnd = InfoTxt.TextLength;

            InfoTxt.Select(iStart, iEnd - iStart);

            if (oColor != Color.Empty)
                InfoTxt.SelectionColor = oColor;

            InfoTxt.SelectionLength = 0;
        }

    	void InfoFormDoubleClick(object sender, EventArgs e)
		{
			Close();
		}
		
		void InfoFormResizeEnd(object sender, EventArgs e)
		{
			InfoTxt.Width = ClientRectangle.Width;
			InfoTxt.Height = ClientRectangle.Height - toolStrip1.Height;
		}
		
		void ToolStripButton1Click(object sender, EventArgs e)
		{
			InfoTxt.WordWrap = !InfoTxt.WordWrap;
		}
		
		public void clearComboBox(){
			cbErrors.Items.Clear();
		}
		
		public void addError(string sKey, string sValue) {
			cbErrors.Items.Add( sKey + sKeyDelimiter + sValue );
		}
		
		void BtnSelectAllClick(object sender, EventArgs e)
		{
			InfoTxt.SelectAll();
		}
		
		void BtnCopyClick(object sender, EventArgs e)
		{
			InfoTxt.Copy();
		}
		
		void CbErrorsSelectedIndexChanged(object sender, EventArgs e)
		{
			string sValue = cbErrors.Text;
			string sKey = "";
			int iIndex = sValue.IndexOf(sKeyDelimiter);
			if ( iIndex != -1 ) {
				sKey = sValue.Substring(0, iIndex);
			
				
				ArrayList oErrs = moErrorBuffer.Buffer;
				ErrorMessage oErr;
				for ( int i = 0; i < oErrs.Count; i++ ) {
					oErr = (ErrorMessage)oErrs[i];
					if ( oErr.Key.Equals( sKey ) ){
						setInfo(oErr);
						break;
					}
				}
			}
		}
	}
}
