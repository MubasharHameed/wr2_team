﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

using com.idh.controls;
using com.idh.win;
using com.idh.win.controls;

using com.idh.bridge;

using SAPbobsCOM;

namespace com.idh.wr1 {
    public partial class Welcome : Form {
        public string Username {
            get { return IDH_UNAME.Text; }
        }

        public string ReturnInfo = "";
        public string DBServer = "";
        public string CompanyDB = "";
        public string DatabaseType = "";
        public string DatabaseUsername = "";
        public string DatabasePassword = "";
        public string DatabaseServer = "";
        public string DatabaseName = "";
        public string LicenseServer = "";
        public bool UseTrusted = true;

        public bool UseSBO = false;

        private int miUserRole = -1;

        private readonly BackgroundWorker moBackWorker = new BackgroundWorker();

        public int Userrole {
            get { return miUserRole; }
        }

        public Welcome(string sUsername) : base() {
            InitializeComponent();
            IDH_UNAME.Text = sUsername;

            moBackWorker.DoWork += Login;
            moBackWorker.RunWorkerCompleted += BwRunWorkerCompleted;

            Result.Text = "Welcome: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        }

        void doSetCompanyDefaults(SAPbobsCOM.Company oCompany) {
            oCompany.UseTrusted = UseTrusted; // true;
            oCompany.Server = DBServer;

            if ( LicenseServer != null && LicenseServer.Length > 0 )
                oCompany.LicenseServer = LicenseServer;

            oCompany.language = SAPbobsCOM.BoSuppLangs.ln_English;
            //oCompany.language = SAPbobsCOM.BoSuppLangs.ln_English;

            int iTypes = int.Parse(DatabaseType);
            //SAPbobsCOM.BoDataServerTypes oType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
            oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes)iTypes; //SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
            oCompany.DbUserName = DatabaseUsername;
            oCompany.DbPassword = DatabasePassword;
        }

        private bool mbLoginResult = false;
        private string msUsername;
        private string msPassword;
        private void IDH_OK_Click(object sender, EventArgs e) {
            ReturnInfo = "";

            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();

            progressBar1.Show();
            //progressBar1.Update();

            IDH_UNAME.Enabled = false;
            IDH_PASS.Enabled = false;
            IDH_OK.Enabled = false;
            IDH_CANCEL.Enabled = false;

            Result.Text = "Please wait while verifying your username and password.";
            Result.Update();

            msUsername = IDH_UNAME.Text;
            msPassword = IDH_PASS.Text;

            // start the long running task async
            moBackWorker.RunWorkerAsync();
            //doNoWorkerLogin();
        }

        private void doNoWorkerLogin() {
            doTheLogin();
            doHandleLoginResult();
        }

        private void doTheLogin() {
            if (UseSBO) {
                SAPbobsCOM.Company oCompany = null;
                try {
                    oCompany = new SAPbobsCOM.Company();
                    doSetCompanyDefaults(oCompany);

                    oCompany.CompanyDB = CompanyDB;
                    oCompany.UserName = msUsername;
                    oCompany.Password = msPassword;

                    long lResult;


                    //Connect to the database
                    lResult = oCompany.Connect();
                    if (lResult == 0) {
                        ReturnInfo = "Welcome " + msUsername;
                        mbLoginResult = true;
                        new DataHandler(true,oCompany);

                        DataHandler.INSTANCE.AppLogger.AppDescription = "WR1 Standalone";
                    } else {
                        ReturnInfo = "[" + msUsername  + "] - " + oCompany.GetLastErrorDescription();
                    }
                } catch (Exception ex) {
                    ReturnInfo = ex.ToString();
                } finally {
                }
            } else {
                string sConnectionString = "server=" + DatabaseServer;
                sConnectionString += ";uid=" + DatabaseUsername;
                sConnectionString += ";pwd=" + DatabasePassword;
                sConnectionString += ";database=" + DatabaseName;


                DataHandler oData = DataHandler.INSTANCE;
                if (oData == null)
                    oData = new DataHandler(true, sConnectionString, msUsername);

                DataRecords oResults = null;
                try {
                    string sQry = "SELECT U_IDHRlLvl FROM OUSR WHERE USER_CODE = '" + msUsername + "'";
                    oResults = oData.doBufferedSelectQuery("doGetCurrentUserRole", sQry);
                    if (oResults != null && oResults.RecordCount > 0) {
                        miUserRole = oResults.getValueAsInt(0);
                        ReturnInfo = "Welcome " + msUsername;
                        mbLoginResult = true;
                    } else {
                        IDH_OK.Text = "Retry";
                        ReturnInfo = "The User [" + msUsername + "] could not be found.";
                        mbLoginResult = false;
                    }
                } catch (Exception ex) {
                    ReturnInfo = ex.ToString();
                } finally {
                    DataHandler.INSTANCE.doReleaseDataRecords(ref oResults);
                }
            }
        }

        private void doHandleLoginResult(){
            try {
                Result.Text = ReturnInfo;

                if ( DataHandler.INSTANCE != null )
                    DataHandler.INSTANCE.doInfo("LOGON: " + DateTime.Now + " " + ReturnInfo);
                
                progressBar1.Hide();

                if (!mbLoginResult) {
                    IDH_OK.Text = "Retry";
                    IDH_UNAME.Enabled = true;
                    IDH_PASS.Enabled = true;
                    IDH_OK.Enabled = true;
                    IDH_CANCEL.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
                Application.DoEvents();

                if (mbLoginResult)
                    DialogResult = System.Windows.Forms.DialogResult.OK;
            } catch (Exception e) {
                Result.Text = e.ToString();
             }
        }

        private void BwRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            doHandleLoginResult();
            //Result.Text = ReturnInfo;
            //progressBar1.Hide();

            //if (!mbLoginResult) {
            //    IDH_OK.Text = "Retry";
            //    IDH_UNAME.Enabled = true;
            //    IDH_PASS.Enabled = true;
            //    IDH_OK.Enabled = true;
            //    IDH_CANCEL.Enabled = true;
            //}
            //Cursor.Current = Cursors.Default;
            //Application.DoEvents();

            //if (mbLoginResult)
            //    DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void Login(object sender, DoWorkEventArgs doWorkEventArgs) {
            doTheLogin();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        
        }
    }
}
