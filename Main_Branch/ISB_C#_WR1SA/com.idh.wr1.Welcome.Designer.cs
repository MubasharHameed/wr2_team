﻿using com.idh.controls;
using com.idh.win;
using com.idh.win.controls;

namespace com.idh.wr1 {
    public partial class Welcome {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Welcome));
            this.IDH_UNAME = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.IDH_PASS = new System.Windows.Forms.TextBox();
            this.IDH_OK = new System.Windows.Forms.Button();
            this.IDH_CANCEL = new System.Windows.Forms.Button();
            this.Result = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // IDH_UNAME
            // 
            this.IDH_UNAME.Location = new System.Drawing.Point(68, 283);
            this.IDH_UNAME.Name = "IDH_UNAME";
            this.IDH_UNAME.Size = new System.Drawing.Size(147, 28);
            this.IDH_UNAME.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 283);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 22);
            this.label1.TabIndex = 2;
            this.label1.Text = "Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 308);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 22);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // IDH_PASS
            // 
            this.IDH_PASS.Location = new System.Drawing.Point(68, 308);
            this.IDH_PASS.Multiline = true;
            this.IDH_PASS.Name = "IDH_PASS";
            this.IDH_PASS.PasswordChar = '*';
            this.IDH_PASS.Size = new System.Drawing.Size(147, 17);
            this.IDH_PASS.TabIndex = 4;
            // 
            // IDH_OK
            // 
            this.IDH_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_OK.Location = new System.Drawing.Point(68, 338);
            this.IDH_OK.Name = "IDH_OK";
            this.IDH_OK.Size = new System.Drawing.Size(72, 22);
            this.IDH_OK.TabIndex = 6;
            this.IDH_OK.Text = "OK";
            this.IDH_OK.UseVisualStyleBackColor = true;
            this.IDH_OK.Click += new System.EventHandler(this.IDH_OK_Click);
            // 
            // IDH_CANCEL
            // 
            this.IDH_CANCEL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IDH_CANCEL.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.IDH_CANCEL.Location = new System.Drawing.Point(146, 338);
            this.IDH_CANCEL.Name = "IDH_CANCEL";
            this.IDH_CANCEL.Size = new System.Drawing.Size(69, 22);
            this.IDH_CANCEL.TabIndex = 7;
            this.IDH_CANCEL.Text = "Cancel";
            this.IDH_CANCEL.UseVisualStyleBackColor = true;
            // 
            // Result
            // 
            this.Result.BackColor = System.Drawing.SystemColors.Control;
            this.Result.Location = new System.Drawing.Point(4, 241);
            this.Result.Multiline = true;
            this.Result.Name = "Result";
            this.Result.ReadOnly = true;
            this.Result.Size = new System.Drawing.Size(214, 36);
            this.Result.TabIndex = 8;
            this.Result.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Result.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(5, 228);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(214, 10);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 9;
            this.progressBar1.UseWaitCursor = true;
            this.progressBar1.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = global::com.idh.wr1.Properties.Resources.ISB_Stacked;
            this.pictureBox1.Location = new System.Drawing.Point(4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Size = new System.Drawing.Size(214, 220);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // Welcome
            // 
            this.AcceptButton = this.IDH_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.IDH_CANCEL;
            this.ClientSize = new System.Drawing.Size(248, 362);
            this.ControlBox = false;
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.Result);
            this.Controls.Add(this.IDH_CANCEL);
            this.Controls.Add(this.IDH_OK);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.IDH_PASS);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IDH_UNAME);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Welcome";
            this.Text = "Login";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox IDH_UNAME;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox IDH_PASS;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button IDH_OK;
        private System.Windows.Forms.Button IDH_CANCEL;
        private System.Windows.Forms.TextBox Result;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}