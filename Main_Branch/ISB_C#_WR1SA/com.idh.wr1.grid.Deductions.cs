﻿/*
 * Created by SharpDevelop.
 * User: Louis
 * Date: 2011/02/22
 * Time: 04:20 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using com.idh.win.controls;
using com.idh.bridge.grid;
using com.idh.dbObjects.User;
using System.Windows.Forms;
using com.idh.controls;

using com.idh.bridge.form;
using com.idh.bridge;
using com.idh.dbObjects.User.Shared;

namespace com.idh.wr1.grid {
	/// <summary>
    /// Description of com_idh_wr1_grid_Deductions.
	/// </summary>
	public partial class Deductions : WR1Grid {
        private idh.bridge.grid.Deductions moDeductionsGridControl;
		private DisposalOrder moDisposal;
		private DisposalRow moRow;
		private bool mbNewRowNeeded;
		
		private ContextMenu moPopUpMenu = new ContextMenu();
		
		public void doInitialize(DisposalOrder oOrder, DisposalRow oRow){
			moDisposal = oOrder;
			moRow = oRow;

			this.VirtualMode = true;
			this.GrayReadOnly = true;
            moDeductionsGridControl = new  idh.bridge.grid.Deductions(oRow.DBObject, this);

        	/*
        	 * Setting the events
        	 */
        	this.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridCellDoubleClick);
        	this.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridRowEnter);
			this.RowsAdded += new DataGridViewRowsAddedEventHandler(this.GridRowAdded);
        	this.NewRowNeeded += new DataGridViewRowEventHandler(this.GridNewRowNeeded);
	        this.CellValidating += new DataGridViewCellValidatingEventHandler(this.GridCellValidating);
        	this.CellValueNeeded += new DataGridViewCellValueEventHandler(this.GridCellValueNeeded);
        	this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GridKeyPress);
        	
        	this.MouseUp += new MouseEventHandler(this.GridMouseUp);
        	
    		moPopUpMenu.MenuItems.Add("Delete", new     EventHandler(Delete));

            IDH_DODEDUCT oDeduct = getDBObject();
            oDeduct.Handler_TotalWeightChanged = new IDH_DODEDUCT.ev_DBObject_Event(doHandleTotalWeightChanged);
            oDeduct.Handler_TotalValueChanged = new IDH_DODEDUCT.ev_DBObject_Event(doHandleTotalValueChanged);

            doPopulateUOM();
		}

        public void doPopulateUOM() {
            DataGridViewComboBoxColumn oCombo = getColumnCombobox(IDH_DODEDUCT._UOM);
            if (oCombo != null) {
                DataGridViewComboBoxCell.ObjectCollection oItems = oCombo.Items;
                oItems.Clear();

                DataRecords oRecords = null;
                string sValue = null;
                try {
                    oItems.Add("");

                    string sQry = "SELECT UnitDisply FROM OWGT";
                    oRecords = DataHandler.INSTANCE.doBufferedSelectQuery("doGetCurrentUserRole", sQry);
                    if (oRecords != null && oRecords.RecordCount > 0) {
                        for (int i = 0; i < oRecords.RecordCount; i++) {
                            oRecords.gotoRow(i);
                            sValue = oRecords.getValueAsStringNotNull("UnitDisply");
                            oItems.Add(sValue);
                        }
                    }
                } catch (Exception ex) {
                    throw ex;
                } finally {
                    DataHandler.INSTANCE.doReleaseDataRecords(ref oRecords);
                }
            }
        }

		public IDH_DODEDUCT getDBObject() {
            return (IDH_DODEDUCT)GridControl.DBObject;
		}
	
#region eventsection
        protected virtual void GridCellDoubleClick(object sender, DataGridViewCellEventArgs e) {
		}

        protected virtual void GridKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e) {
        }        

		protected virtual void GridRowEnter(object sender, DataGridViewCellEventArgs e) {
        }   
        
        private void GridNewRowNeeded(object sender, DataGridViewRowEventArgs e) {
        	mbNewRowNeeded = true;
    	}
        
        protected virtual void GridRowAdded(object sender, DataGridViewRowsAddedEventArgs e) {
			if ( mbNewRowNeeded ) {
				mbNewRowNeeded = false;
				GridControl.DBObject.doAddEmptyRow(true, true);
			}
		}
		
		private void MakeActive(object sender, EventArgs e) {
			object ss = sender;
		}
		
		private void Delete(object sender, EventArgs e) {
			DataGridViewSelectedRowCollection oRows = this.SelectedRows;
			if ( oRows != null && oRows.Count > 0 ) {
				foreach ( DataGridViewRow oRow in oRows ) {
					GridControl.DBObject.doMarkForDelete( oRow.Index );
					if ( moDisposal.Mode == FormBridge.Mode_VIEW || moDisposal.Mode == FormBridge.Mode_OK ) {
						moDisposal.Mode = FormBridge.Mode_UPDATE;
					}
				}
			}
			this.doRefreshGridFromDBObject();
		}
		
		private void Register(object sender, EventArgs e) {
			object ss = sender;
		}
		
		private void GridMouseUp(object sender, MouseEventArgs e) {
		    // Load context menu on right mouse click
		    DataGridView.HitTestInfo hitTestInfo;
		    if (e.Button == MouseButtons.Right) {
		        hitTestInfo = this.HitTest(e.X, e.Y);
		        if (hitTestInfo.Type == DataGridViewHitTestType.Cell) {
		        	this.ClearSelection();
		            this.SetSelectedRowCore( hitTestInfo.RowIndex, true );
		        	moPopUpMenu.Show(this, new System.Drawing.Point(e.X, e.Y));
		        }
		    }			
		}
		
    	private void GridCellValueNeeded(object sender, DataGridViewCellValueEventArgs e) {
			object oValue = e.Value;
			
			int iRow = e.RowIndex;
			int iCol = e.ColumnIndex;

            IDH_DODEDUCT oDB = (IDH_DODEDUCT)GridControl.DBObject;
			if ( iRow >= oDB.Count )
				e.Value = "";
			else {
				DataGridViewColumn oCol = this.Columns[iCol];
				string sFieldName = oCol.Name;
				string sValue;
				
				oDB.gotoRow(iRow);
				
				sValue = oDB.getValueAsString( sFieldName );		
                 idh.dbObjects.strct.DBFieldInfo oInfo = oDB.getFieldInfo(sFieldName);
                if (oInfo != null) {
                    SAPbobsCOM.BoFieldTypes oFormat = oInfo.Type;
                    SAPbobsCOM.BoFldSubTypes oSubFormat = oInfo.SubType;

                    string sFormatValue =  idh.utils.Formatter.doFormatForUI(sValue, oFormat, oSubFormat);
                    e.Value = sFormatValue;
                }				
			}
	    }

        protected override void OnCellValueChanged(DataGridViewCellEventArgs e) {
            int iRow = e.RowIndex;
            int iCol = e.ColumnIndex;

            IDH_DODEDUCT oDB = (IDH_DODEDUCT)GridControl.DBObject;

            DataGridViewColumn oCol = this.Columns[iCol];
            string sFieldName = oCol.Name;
            object oValue = CurrentCell.EditedFormattedValue;

            oDB.gotoRow(iRow);
            if (!doSearch(iRow, iCol)) {
                oDB.setValue(sFieldName, oValue);
            }

            if (sFieldName.Equals(IDH_DODEDUCT._ItmWgt) ||
                sFieldName.Equals(IDH_DODEDUCT._Qty) ||
                sFieldName.Equals(IDH_DODEDUCT._WgtDed) ||
                sFieldName.Equals(IDH_DODEDUCT._UOM) ||
                sFieldName.Equals(IDH_DODEDUCT._ValDed)) {

                moRow.DBObject.doCalculateTotalsWithOther();

                Refresh();
                moRow.doUpdateFormData();
            }

            if (moDisposal.Mode == FormBridge.Mode_VIEW || moDisposal.Mode == FormBridge.Mode_OK) {
                moDisposal.Mode = FormBridge.Mode_UPDATE;
            }
        }

		public void doHandleTotalWeightChanged( Deduction oDBObject ){
            moRow.setUFValue("IDH_DEDWEI", oDBObject.WeightTotal);
            //moRow.setFormDFValue(IDH_DISPROW._WgtDed, oDBObject.WeightTotal);
            //moRow.doItAll();
            moRow.doUpdateFormData();
        }

        public void doHandleTotalValueChanged(Deduction oDBObject) {
            //moRow.DBObject.doCalculateTotalsWithOther();

            moRow.setUFValue("IDH_DEDVAL", oDBObject.ValueTotal);

            // Must later just update the form value from the DB
            //moRow.setFormDFValue(IDH_DISPROW._ValDed, oDBObject.ValueTotal);

            moRow.doUpdateFormData();
        }

	    private void GridCellValidating(object sender, DataGridViewCellValidatingEventArgs e) {
	        Rows[e.RowIndex].ErrorText = "";
	
	        if (Rows[e.RowIndex].IsNewRow) { return; }
	    }	    
#endregion

#region search
        /** 
		 * Do the actual search return false if the item is not part of a search
		 */
		public override bool doSearch( int iRow, int iCol ) {
            DataGridViewColumn oCol = this.Columns[iCol];
            string sFieldName = oCol.Name;

            if ( sFieldName.Equals(IDH_DODEDUCT._ItemCd) || sFieldName.Equals(IDH_DODEDUCT._ItemDsc)) { //Item Search
                 idh.wr1.form.search.WasteItem oSearch = new  idh.wr1.form.search.WasteItem();
                oSearch.Text = "Deduction Item";
                oSearch.setFilterFieldValue("IDH_ITMCOD", "");
                oSearch.setFilterFieldValue("IDH_ITMNAM", "");
                oSearch.setFilterFieldValue( "IDH_GRPCOD", "" );
            	
                if (oSearch.ShowDialog(this) == DialogResult.OK) {
                    IDH_DODEDUCT oDB = (IDH_DODEDUCT)GridControl.DBObject;
            		
                    oDB.gotoRow(iRow);
                    oDB.U_ItemCd = oSearch.getReturnValue("ITEMCODE");
                    oDB.U_ItemDsc = oSearch.getReturnValue("ITEMNAME");

                    string sUOM = oSearch.getReturnValue("UOM");
                    oDB.U_UOM = sUOM;
                }
                return true;
            }
			return false;
		}
#endregion

	}
}
