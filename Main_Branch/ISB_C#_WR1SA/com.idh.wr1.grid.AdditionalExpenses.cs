﻿/*
 * Created by SharpDevelop.
 * User: Louis
 * Date: 2011/02/22
 * Time: 04:20 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using com.idh.win.controls;
using com.idh.bridge.grid;
using com.idh.dbObjects.User.Shared;
using System.Windows.Forms;
using com.idh.controls;

using com.idh.bridge.form;
using com.idh.bridge;

namespace com.idh.wr1.grid {
	/// <summary>
	/// Description of com_idh_wr1_grid_AdditionalExpenses.
	/// </summary>
	public partial class AdditionalExpenses : WR1Grid {
		private Additional moAdditionalGridControl;
        private DisposalOrder moDisposal;
        private DisposalRow moRow;
        private bool mbNewRowNeeded;
		
		private ContextMenu moPopUpMenu = new ContextMenu();
		
		public void doInitialize(DisposalOrder oOrder, DisposalRow oRow){
			moDisposal = oOrder;
			moRow = oRow;

			this.VirtualMode = true;
			this.GrayReadOnly = true;
			moAdditionalGridControl = new  idh.bridge.grid.Additional(oRow.DBObject, this);

        	/*
        	 * Setting the events
        	 */
        	this.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridCellDoubleClick);
        	this.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridRowEnter);
			this.RowsAdded += new DataGridViewRowsAddedEventHandler(this.GridRowAdded);
            this.NewRowNeeded += new DataGridViewRowEventHandler(this.GridNewRowNeeded);
	        this.CellValidating += new DataGridViewCellValidatingEventHandler(this.GridCellValidating);
        	this.CellValueNeeded += new DataGridViewCellValueEventHandler(this.GridCellValueNeeded);
        	this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GridKeyPress);
        	this.MouseUp += new MouseEventHandler(this.GridMouseUp);
        	
    		moPopUpMenu.MenuItems.Add("Delete", new     EventHandler(Delete));
            moPopUpMenu.MenuItems.Add("Duplicate", new EventHandler(Duplicate));

            AdditionalExp oAdditional = getDBObject();
            oAdditional.Handler_TotalCharge = new AdditionalExp.ev_DBObject_Event(doHandleTotalChargeChanged);
            oAdditional.Handler_TotalCost = new AdditionalExp.ev_DBObject_Event(doHandleTotalCostChanged);

            oAdditional.Handler_Charge = new AdditionalExp.ev_DBObject_Event(doHandleChargeChanged);
            oAdditional.Handler_Cost = new AdditionalExp.ev_DBObject_Event(doHandleCostChanged);

            oAdditional.Handler_TotalChargeVAT = new AdditionalExp.ev_DBObject_Event(doHandleTotalChargeVatChanged);
            oAdditional.Handler_TotalCostVAT = new AdditionalExp.ev_DBObject_Event(doHandleTotalCostVatChanged);

            oAdditional.Handler_AutoAddedCodesChanged = new AdditionalExp.ev_DBObject_Event(doHandleAutoAddedCodesChanged);

            //Populate the Comboboxes
            doPopulateUOM();
		}

        public void doPopulateUOM() {
            DataGridViewComboBoxColumn oCombo = getColumnCombobox(AdditionalExp._UOM);
            if (oCombo != null) {
                DataGridViewComboBoxCell.ObjectCollection oItems = oCombo.Items;
                oItems.Clear();

                DataRecords oRecords = null;
                string sValue = null;
                try {
                    oItems.Add("");

                    string sQry = "SELECT UnitDisply FROM OWGT";
                    oRecords = DataHandler.INSTANCE.doBufferedSelectQuery("doGetCurrentUserRole", sQry);
                    if (oRecords != null && oRecords.RecordCount > 0) {
                        for (int i = 0; i < oRecords.RecordCount; i++) {
                            oRecords.gotoRow(i);
                            sValue = oRecords.getValueAsStringNotNull("UnitDisply");
                            oItems.Add(sValue);
                        }
                    }
			    }
			    catch (Exception ex) {
				    throw ex;
			    }
			    finally {
				    DataHandler.INSTANCE.doReleaseDataRecords(ref oRecords);
			    }
            }
        }

		public AdditionalExp getDBObject() {
			return (AdditionalExp)GridControl.DBObject;
		}
		
#region eventsection
        protected virtual void GridCellDoubleClick(object sender, DataGridViewCellEventArgs e) {
		}

        protected virtual void GridKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e) {
        }        

		protected virtual void GridRowEnter(object sender, DataGridViewCellEventArgs e) {
        }   
        
        private void GridNewRowNeeded(object sender, DataGridViewRowEventArgs e) {
        	mbNewRowNeeded = true;
    	}
        
        protected virtual void GridRowAdded(object sender, DataGridViewRowsAddedEventArgs e) {
			if ( mbNewRowNeeded ) {
				mbNewRowNeeded = false;
				GridControl.DBObject.doAddEmptyRow(true, true);
			}
		}
		
		private void MakeActive(object sender, EventArgs e) {
			object ss = sender;
		}
		
		private void Delete(object sender, EventArgs e) {
			DataGridViewSelectedRowCollection oRows = this.SelectedRows;
			if ( oRows != null && oRows.Count > 0 ) {
				foreach ( DataGridViewRow oRow in oRows ) {
                    getDBObject().doMarkForDelete(oRow.Index);
					if ( moDisposal.Mode == FormBridge.Mode_VIEW || moDisposal.Mode == FormBridge.Mode_OK ) {
						moDisposal.Mode = FormBridge.Mode_UPDATE;
					}
				}
                moRow.DBObject.doCalculateTotalsWithOther();
			}
			this.doRefreshGridFromDBObject();
		}

        private void Duplicate(object sender, EventArgs e) {
			DataGridViewSelectedRowCollection oRows = this.SelectedRows;
			if ( oRows != null && oRows.Count > 0 ) {
				foreach ( DataGridViewRow oRow in oRows ) {
                    getDBObject().doDuplicate(oRow.Index);
					if ( moDisposal.Mode == FormBridge.Mode_VIEW || moDisposal.Mode == FormBridge.Mode_OK ) {
						moDisposal.Mode = FormBridge.Mode_UPDATE;
					}
				}
			}
			this.doRefreshGridFromDBObject();
		}

		private void Register(object sender, EventArgs e) {
			object ss = sender;
		}
		
		private void GridMouseUp(object sender, MouseEventArgs e) {
		    // Load context menu on right mouse click
		    DataGridView.HitTestInfo hitTestInfo;
		    if (e.Button == MouseButtons.Right) {
		        hitTestInfo = this.HitTest(e.X, e.Y);
		        if (hitTestInfo.Type == DataGridViewHitTestType.Cell) {
		        	this.ClearSelection();
		            this.SetSelectedRowCore( hitTestInfo.RowIndex, true );
		        	moPopUpMenu.Show(this, new System.Drawing.Point(e.X, e.Y));
		        }
		    }			
		}
		
    	private void GridCellValueNeeded(object sender, DataGridViewCellValueEventArgs e) {
			object oValue = e.Value;
			
			int iRow = e.RowIndex;
			int iCol = e.ColumnIndex;

            AdditionalExp oDB = (AdditionalExp)GridControl.DBObject;
			if ( iRow >= oDB.Count )
				e.Value = "";
			else {
				DataGridViewColumn oCol = this.Columns[iCol];
				string sFieldName = oCol.Name;
				string sValue;
				
				oDB.gotoRow(iRow);
				
				sValue = oDB.getValueAsString( sFieldName );		
                 idh.dbObjects.strct.DBFieldInfo oInfo = oDB.getFieldInfo(sFieldName);
                if (oInfo != null) {
                    if ( oCol.GetType() == typeof(DataGridViewCheckBoxColumn)) {
                        if (sValue == null || sValue.Length == 0) {
                            e.Value = false;
                        } else {
                            sValue = sValue.ToLower();
                            if ( sValue[0] == 'y' || sValue[0] == 't' || sValue[0] == '1' ) {
                                e.Value = true;
                            } else {
                                e.Value = false;
                            }
                        }
                    } else {
                        SAPbobsCOM.BoFieldTypes oFormat = oInfo.Type;
                        SAPbobsCOM.BoFldSubTypes oSubFormat = oInfo.SubType;

                        string sFormatValue =  idh.utils.Formatter.doFormatForUI(sValue, oFormat, oSubFormat);
                        e.Value = sFormatValue;
                    }
                }				
			}
	    }

        protected override void OnCellValueChanged(DataGridViewCellEventArgs e) {
            int iRow = e.RowIndex;
            int iCol = e.ColumnIndex;

            AdditionalExp oDB = (AdditionalExp)GridControl.DBObject;

            DataGridViewColumn oCol = this.Columns[iCol];
            string sFieldName = oCol.Name;
            object oValue = CurrentCell.EditedFormattedValue;

            oDB.gotoRow(iRow);
            if (!doSearch(iRow, iCol)) {
                oDB.setValue(sFieldName, oValue);
            }

            if (sFieldName.Equals(AdditionalExp._Quantity) ||
                sFieldName.Equals(AdditionalExp._ItmChrg) ||
                sFieldName.Equals(AdditionalExp._ItmCost) ||
                sFieldName.Equals(AdditionalExp._ItmChrgVat) ||
                sFieldName.Equals(AdditionalExp._ItmCostVat) ||
                sFieldName.Equals(AdditionalExp._ChrgVatGrp) ||
                sFieldName.Equals(AdditionalExp._CostVatGrp) ||
                sFieldName.Equals(AdditionalExp._ItemCd) ||
                sFieldName.Equals(AdditionalExp._CustCd) ||
                sFieldName.Equals(AdditionalExp._SuppCd)) {

                moRow.DBObject.doCalculateTotalsWithOther();

                Refresh();
            }

            if (moDisposal.Mode == FormBridge.Mode_VIEW || moDisposal.Mode == FormBridge.Mode_OK) {
                moDisposal.Mode = FormBridge.Mode_UPDATE;
            }
        }

        //public void doHandleTotalChargeChanged(IDH_DOADDEXP oDBObject) {
        public void doHandleTotalChargeChanged(com.idh.dbObjects.User.Shared.AdditionalExp oDBObject) {
            //moRow.setUFValue("IDH_ADDCH", oDBObject.ChargeTotal);

            // Must later just update the form value from the DB
            //moRow.setFormDFValue(IDH_DISPROW._TAddChrg, oDBObject.ChargeTotal);
            moRow.DBObject.U_TAddChrg = oDBObject.ChargeTotal;
            //moRow.DBObject.doCalculateTotalsWithOther();
        }

        //public void doHandleTotalCostChanged(IDH_DOADDEXP oDBObject) {
        public void doHandleTotalCostChanged(com.idh.dbObjects.User.Shared.AdditionalExp oDBObject) {
            //moRow.setUFValue("IDH_ADDCO", oDBObject.CostTotal);

            // Must later just update the form value from the DB
            //moRow.setFormDFValue(IDH_DISPROW._TAddCost, oDBObject.CostTotal);
            moRow.DBObject.U_TAddCost = oDBObject.CostTotal;
            //moRow.DBObject.doCalculateTotalsWithOther();
        }

        //public void doHandleChargeChanged(IDH_DOADDEXP oDBObject) {
        public void doHandleChargeChanged(com.idh.dbObjects.User.Shared.AdditionalExp oDBObject) {
            //moRow.setFormDFValue(IDH_DISPROW._AddCharge, oDBObject.Charge);
            moRow.DBObject.U_AddCharge = oDBObject.Charge;
            //if ( moRow.DBObject.BlockChangeNotif ) 
                moRow.DBObject.doCalculateChargeTotal();
        }

        //public void doHandleCostChanged(IDH_DOADDEXP oDBObject) {
        public void doHandleCostChanged(com.idh.dbObjects.User.Shared.AdditionalExp oDBObject) {
            //moRow.setFormDFValue(IDH_DISPROW._AddCost, oDBObject.Cost);
            moRow.DBObject.U_AddCost = oDBObject.Cost;
            //if (moRow.DBObject.BlockChangeNotif) 
                moRow.DBObject.doCalculateCostTotal();
        }

        //public void doHandleTotalChargeVatChanged(IDH_DOADDEXP oDBObject) {
        public void doHandleTotalChargeVatChanged(com.idh.dbObjects.User.Shared.AdditionalExp oDBObject) {
            //moRow.setFormDFValue(IDH_DISPROW._TAddChVat, oDBObject.ChargeTotalVAT);
            moRow.DBObject.U_TAddChVat = oDBObject.ChargeTotalVAT;
            //if (moRow.DBObject.BlockChangeNotif) 
                moRow.DBObject.doCalculateChargeVat(OrderRow.TOTAL_CALCULATE);
        }

        //public void doHandleTotalCostVatChanged(IDH_DOADDEXP oDBObject) {
        public void doHandleTotalCostVatChanged(com.idh.dbObjects.User.Shared.AdditionalExp oDBObject) {
            //moRow.setFormDFValue(IDH_DISPROW._TAddCsVat, oDBObject.CostTotalVAT);
            moRow.DBObject.U_TAddCsVat = oDBObject.CostTotalVAT;
            //if (moRow.DBObject.BlockChangeNotif) 
            moRow.DBObject.doCalculateCostVat(OrderRow.TOTAL_CALCULATE);
        }

        //public void doHandleAutoAddedCodesChanged(IDH_DOADDEXP oDBObject) {
        public void doHandleAutoAddedCodesChanged(com.idh.dbObjects.User.Shared.AdditionalExp oDBObject) {
            this.doRefreshGridFromDBObject();
        }
        
        private void GridCellValidating(object sender, DataGridViewCellValidatingEventArgs e) {
	        Rows[e.RowIndex].ErrorText = "";
	        if (Rows[e.RowIndex].IsNewRow) { return; }
	    }	    
#endregion

#region search
		/** 
		 * Do the actual search return false if the item is not part of a search
		 */
		public override bool doSearch( int iRow, int iCol ) {
            DataGridViewColumn oCol = this.Columns[iCol];
            string sFieldName = oCol.Name;

			if ( sFieldName.Equals(AdditionalExp._ItemCd ) ) { //Item Search
				 idh.wr1.form.search.WasteItem oSearch = new  idh.wr1.form.search.WasteItem();
				oSearch.Text = "Additional Expense Item";
				
				string sAddGrp =  idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup();
					
                oSearch.setFilterFieldValue("IDH_ITMCOD", "");
                oSearch.setFilterFieldValue("IDH_ITMNAM", "");
            	oSearch.setFilterFieldValue( "IDH_GRPCOD", sAddGrp );
            	
            	if (oSearch.ShowDialog(this) == DialogResult.OK) {
                    AdditionalExp oDB = (AdditionalExp)GridControl.DBObject;
            		
            		oDB.gotoRow(iRow);
                    oDB.U_UOM = oSearch.getReturnValue("UOM");
            		oDB.U_ItemCd = oSearch.getReturnValue("ITEMCODE");
            		oDB.U_ItemDsc = oSearch.getReturnValue("ITEMNAME");
		    	
			    	moRow.DBObject.doCalculateTotalsWithOther();
            	}
                return true;
			} else if ( sFieldName.Equals(AdditionalExp._SuppCd) ) { //BP Search
				 idh.wr1.form.search.BP oSearch = new  idh.wr1.form.search.BP(null);
				oSearch.Text = "Supplier Search";
				string sValue = (string)CurrentCell.EditedFormattedValue;
				
				oSearch.Target = "SUP";
				oSearch.DoSilent = true;
	
				oSearch.setFilterFieldValue( "IDH_CS", "N");
				oSearch.setFilterFieldValue( "IDH_TYPE", "S");
            	oSearch.setFilterFieldValue( "IDH_GROUP", "");
            	oSearch.setFilterFieldValue( "IDH_BRANCH", "");
            	oSearch.setFilterFieldValue( "IDH_BPCOD", sValue);
            	oSearch.setFilterFieldValue( "IDH_NAME", "");
            	
            	if (oSearch.ShowDialog(this) == DialogResult.OK){
                    AdditionalExp oDB = (AdditionalExp)GridControl.DBObject;
            		
            		oDB.gotoRow(iRow);
            		oDB.U_SuppCd = oSearch.getReturnValue("CARDCODE");
			    	oDB.U_SuppNm = oSearch.getReturnValue("CARDNAME");

			    	moRow.DBObject.doCalculateCostTotalWithOther();
   	
				}
                return true;
    	   	} 
			
			return false;
		}
#endregion

	}
}
