﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using com.idh.controls;
using com.idh.win;
using com.idh.win.controls;

using com.idh.dbObjects.User;
using com.idh.bridge.action;

namespace com.idh.wr1.form {
    public partial class DOAmend : AppForm {
        private IDH_DISPROW moDOR;
        public IDH_DISPROW DOR {
            get { return moDOR; }
        }

        private IDH_JOBSHD moWOR;
        public IDH_JOBSHD WOR {
            get { return moWOR; }
        }
        
        private char mcPaymentType = 'C';
        public char PaymentType {
            get { return mcPaymentType; }
            set { mcPaymentType = value; }
        }

        private string msAmendType = "NONE";
        public string AmendmentType {
            set { msAmendType = value; }
            get { return msAmendType; }
        }

        private string PayLabelText {
            set {
                uBCCQNUMLbl.Text = value;
            }
            get {
                return uBCCQNUMLbl.Text;
            }
        }

        //private string msDORCode;
        public string DORCode {
            get { return uBCDOR.Text; }
            set {
                moDOR = new IDH_DISPROW();
                //IDH_DISPROW.Handler_CheckNumberRequest = new IDH_DISPROW.et_ExternalTriggerWithReturn(com.uBC.forms.PopUps.doRequestCheckNumber);

                //moDOR.CalledFromGUI = true;
                if (!moDOR.getByKey(value)) {
                    value = "Not Found";
                    moDOR = null;
                } else {
                    if (moDOR.isLinkedToOrder()) {
                        moWOR = new IDH_JOBSHD();
                        if (!moWOR.getByKey(moDOR.U_WRRow))
                            moWOR = null;
                    } 
                }
                uBCDOR.Text = value;
            }
        }

        public const string AMENDTYPE_PAYMENT = "Payment";
        public const string AMENDTYPE_ALL = "All";

        public DOAmend() {
            InitializeComponent();
            uBCCQNUM.Visible = false;
            doFillReasonComboBox();
        }

        private void uBCPay_Click(object sender, EventArgs e) {
            uBCREASN.Enabled = false;
            uBCCQNUM.Enabled = true;
            
            //doCancelPayment
            msAmendType = AMENDTYPE_PAYMENT;
            PayLabelText = "Waiting for Repayment.";
        }

        private void doFillReasonComboBox() {
            com.uBC.utils.FillCombos.AmendmentReasonCombo(uBCREASN, "DOR Amended", "Select a Reason");
        }

        private void uBCAll_Click(object sender, EventArgs e) {
            uBCREASN.Enabled = true;

            msAmendType = AMENDTYPE_ALL;
            PayLabelText = ""; 
        }

        private void btn1_Click(object sender, EventArgs e) {
            if (moDOR != null) {
                if (msAmendType == AMENDTYPE_PAYMENT) {
                    int iChequeNumber = -1;
                    if (moWOR != null && !string.IsNullOrWhiteSpace(moDOR.U_TrnCode)) {
                        //check if there is a Linked WO 
                        if (MarketingDocs.doCancelOutgoingPayment(moWOR.U_ProCd, moWOR.U_PROINV, "AMD")) {
                            moWOR.doSupplierPaymentFromAPInvoice(true, iChequeNumber);
                        }
                    } else {
                        if (MarketingDocs.doCancelOutgoingPayment(moDOR.U_ProCd, moDOR.U_PROINV, "AMD")) {
                            moDOR.doSupplierPaymentFromAPInvoice(true, iChequeNumber);
                        }
                    }
                } else if (msAmendType == AMENDTYPE_ALL) {
                    string sReason = uBCREASN.Text;
                    if (moDOR.doReOpen(sReason == null || sReason.Length == 0 ? "DOR Amended." : sReason)) {
                        
                        bool bSkipMarkDocs = moDOR.DoSkipMarketingDocuments;
                        bool bSkipLinkUpdate = moDOR.DoSkipUpdateLinkedWO;
                        try {
                            moDOR.DoSkipMarketingDocuments = true;
                            moDOR.DoSkipUpdateLinkedWO = true;
                          
                            moDOR.doUpdateDataRow();
                        } catch (Exception ex) {
                            throw ex;
                        } finally {
                            moDOR.DoSkipMarketingDocuments = bSkipMarkDocs;
                            moDOR.DoSkipUpdateLinkedWO = bSkipLinkUpdate;
                        }

                        //Now do the linked WOR as well
                        if (moWOR != null && !string.IsNullOrWhiteSpace(moDOR.U_TrnCode)) {
                            if (moWOR.doReOpen(sReason == null || sReason.Length == 0 ? "DOR Amended." : sReason)) {
                                bSkipMarkDocs = moWOR.DoSkipMarketingDocuments;
                                try {
                                    moWOR.DoSkipMarketingDocuments = true;
                                    moWOR.doUpdateDataRow();
                                } catch (Exception ex) {
                                    throw ex;
                                } finally {
                                    moWOR.DoSkipMarketingDocuments = bSkipMarkDocs;
                                }
                            }
                        }
                    }
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Hide();
        }

        private void btn2_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
            this.Hide();
        }
    }
}
