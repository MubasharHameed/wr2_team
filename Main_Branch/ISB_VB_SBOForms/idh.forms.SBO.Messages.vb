﻿Imports System.Collections
Imports System.IO
Imports System

Namespace idh.forms.SBO
    Public Class Messages
        Inherits IDHAddOns.idh.forms.Base

        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "198", -1, Nothing)
            gsSystemMenuID = "1029"
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
        End Sub

        Public Overrides Sub doLoadForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)

        End Sub

        Public Overrides Sub doClose()
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sEvtItem As String = pVal.ItemUID
            'If pVal.Before_Action = False AndAlso sEvtItem.Equals("6") Then
            '    Dim oItem As SAPbouiCOM.Item = oForm.Items.Item(sEvtItem)
            '    Dim oMatrix As SAPbouiCOM.Matrix = oItem.Specific

            '    Dim oEdit As SAPbouiCOM.EditText = oMatrix.GetCellSpecific(pVal.ColUID, pVal.Row)
            '    Dim sValue As String = oEdit.Value
            '    If sValue.StartsWith("WR1APP-") Then
            '        setSharedData(oForm, "APPROVER", goParent.gsUserName)
            '        goParent.doOpenModalForm("50107", oForm)
            '    End If
            'End If
            Return False
        End Function

    End Class
End Namespace
