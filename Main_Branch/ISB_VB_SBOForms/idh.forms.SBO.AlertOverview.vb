
Imports System.Collections
Imports System.IO
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge
Imports com.idh.bridge.lookups

Namespace idh.forms.SBO
    Public Class AlertOverview
        Inherits IDHAddOns.idh.forms.Base

        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "198", -1, Nothing)
            'gsSystemMenuID = "1029"

            'doAddImagesToFix("IDH_WOHLK")
            'doAddImagesToFix("IDH_WORLK")
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            'doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            'doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            'doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            'doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
            'doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)

            'doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD)
            'doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE)
            'doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD)

        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
        End Sub

         
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sEvtItem As String = pVal.ItemUID
            Try
                If pVal.BeforeAction = False AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK AndAlso pVal.ItemUID = "5" Then
                    If (getDFValue(oForm, "OALR", "Subject", True) IsNot Nothing And getDFValue(oForm, "OALR", "Subject", True).ToString.IndexOf("WOQ ") > -1) Then
                        Dim aSubject() As String = getDFValue(oForm, "OALR", "Subject", True).ToString.Split(":")
                        aSubject = aSubject(1).Trim().Split(" ")
                        Dim sDocNum As String = aSubject(0).Trim()
                        If sDocNum.Trim <> "" Then
                            'Dim oOOForm As com.isb.forms.Enquiry.WOQuote = New com.isb.forms.Enquiry.WOQuote(Nothing, oForm.UniqueID, oForm)
                            'oOOForm.WOQID = sDocNum
                            'oOOForm.bLoadWOQ = True
                            'oOOForm.doOpenForm(oOOForm.IDHForm.gsType)
                        End If
                    ElseIf (getDFValue(oForm, "OALR", "Subject", True) IsNot Nothing And getDFValue(oForm, "OALR", "Subject", True).ToString.IndexOf("Lab Task:") > -1) Then
                        Dim aSubject() As String = getDFValue(oForm, "OALR", "Subject", True).ToString.Split(":")
                        aSubject = aSubject(1).Trim().Split(" ")
                        Dim sDocNum As String = aSubject(0).Trim()
                        If sDocNum.Trim <> "" Then
                            setSharedData(oForm, "LABTASK", sDocNum)
                            goParent.doOpenForm("IDHLABTODO", oForm, False)
                        End If
                    End If

                    ' Dim stemp As String = sSubject
                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXITME", {pVal.ItemUID, pVal.EventType})
            End Try
            Return True
        End Function
       
        Public Overrides Sub doClose()
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        End Sub

         

    End Class
End Namespace
