﻿'
' Created by SharpDevelop.
' User: Louis Viljoen
' Date: 16/04/2008
' Time: 11:12
'
Imports System.Collections
Imports System.IO

Namespace idh.forms.SBO
    Public Class Draft
        Inherits IDHAddOns.idh.forms.Base

        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "139", -1, Nothing)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_PRINT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_PRINT_DATA)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_PRINT_LAYOUT_KEY)
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                setWFValue(oForm, "ISDRAFT", False)
                Dim oItem As SAPbouiCOM.EditText = CType(oForm.Items.Item("81").Specific, SAPbouiCOM.EditText)
                Dim sStatus As String = oItem.Value
                If sStatus.Equals("Draft") Then
                    setWFValue(oForm, "ISDRAFT", True)
                End If
            Else
                If getWFValue(oForm, "ISDRAFT") Then
                    'Now Update the WoR And DOR's
                    Dim soFields As String() = {"U_SAORD", "U_TIPPO", "U_JOBPO", "U_ProPO", "U_ProGRPO"}
                    Dim soMarkDocs As String() = {"[ORDR]", "[OPOR]"}
                    Dim soWR1Table As String() = {"[@IDH_JOBSHD]", "[@IDH_DISPROW]"}
                    Dim sQry As String = ""

                    For Each sWR1Table As String In soWR1Table
                        For Each sMarkDoc As String In soMarkDocs
                            For Each sField As String In soFields
                                sQry = sQry & "UPDATE " & sWR1Table & " SET " & sWR1Table & "." & sField & " = " & sMarkDoc & ".DocEntry " & _
                                               " FROM " & sWR1Table & ", " & sMarkDoc & " " & _
                                              "  WHERE " & _
                                              "     " & sWR1Table & "." & sField & "  LIKE 'D-%'" & _
                                              "     AND SUBSTRING(" & sWR1Table & "." & sField & ",3,999) = " & sMarkDoc & ".draftKey;" & Chr(13)
                            Next
                        Next
                    Next

                    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                    Try
                        com.idh.bridge.DataHandler.INSTANCE.doUpdateQuery("doButtonID1", sQry)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing Query")
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", {""})
                    End Try
                End If
            End If

            'SELECT SUBSTRING(U_SONr,3,999) FROM [@IDH_WOADDEXP] WHERE U_SONr LIKE 'D-%';   
            'SELECT SUBSTRING(U_PONr,3,999) FROM [@IDH_WOADDEXP] WHERE U_PONr LIKE 'D-%';

        End Sub

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
