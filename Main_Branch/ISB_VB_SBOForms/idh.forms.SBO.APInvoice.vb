Imports System.Collections
Imports System.IO

Namespace idh.forms.SBO
    Public Class APInvoice
        Inherits IDHAddOns.idh.forms.Base

        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "141", -1, Nothing)
            gsSystemMenuID = "2308"
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)

            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE)

        End Sub

        Public Overrides Sub doLoadForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            'Try
            'Catch ex As Exception
            'End Try
        End Sub

        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            Return True
        End Function

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE Then
                If pVal.BeforeAction = False Then
                    Dim sFMode As String = getParentSharedData(oForm, "sFMode")
                    Dim sAPInvoiceNo As String = getParentSharedData(oForm, "APINVNO")

                    If sFMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE.ToString() AndAlso sAPInvoiceNo.Length > 0 Then
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE

                        Dim oItem As SAPbouiCOM.EditText
                        oItem = oForm.Items.Item("8").Specific
                        oItem.Value = sAPInvoiceNo

                        oForm.Items.Item("1").Click()
                        doClearParentSharedData(oForm)
                    End If
                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
