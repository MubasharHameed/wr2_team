Imports System.IO
Imports System
Imports IDHAddOns.idh.controls
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.data
Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User
Imports System.Collections
Imports com.idh.utils

Imports com.idh.bridge
Imports com.idh.dbObjects.numbers

Namespace idh.forms.SBO
    Public Class ItemMaster
        Inherits IDHAddOns.idh.forms.Base

        'Private miFormWidth As Integer = -1
        Private miSpaceWidth As Integer = 0
        Private miFolderWidth As Integer = 0

        Private msTabAnchor As String
        Private msRightTabPanelLine As String
        Private msBottomTabPanelLine As String
        Private msTopTabPanelLine As String
        Private msUnitPriceTextBox As String
        Private msUnitPriceLabel As String
        Private msRemarkFolder As String
        Private msInventoryCheckBox As String
        Private msSalesCheckBox As String
        Private msAssitCheckbox As String
        Private msPurchaseCheckBox As String
        Private msPriceListDropDown As String
        Private msPriceListLabel As String
        Private msOkButton As String
        Private msHeaderLinkTo As String
        Private msWasteGroupLinkTo As String
        Private miWasteProfileRecRightSpace As Integer = 50
        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "150", -1, Nothing)
            gsSystemMenuID = "3073"

            If IDHAddOns.idh.addon.Base.LAYOUTTYPE = FixedValues.SBOLayout2009PL5 Then
                msTabAnchor = "1320002069"
                'msTabAnchor = "163"
                msRightTabPanelLine = "103"
                msBottomTabPanelLine = "100"
                msTopTabPanelLine = "303"
                msUnitPriceTextBox = "107" '"34"
                msUnitPriceLabel = "106" '"52"
                msRemarkFolder = "9"
                msPriceListDropDown = "24"
                msPriceListLabel = "25"
                msOkButton = "1"

                msInventoryCheckBox = "14"
                msSalesCheckBox = "13"
                msPurchaseCheckBox = "12"
                msHeaderLinkTo = "106"
                msWasteGroupLinkTo = "6"
            Else
                msTabAnchor = "540002073"
                msRightTabPanelLine = "103"
                msBottomTabPanelLine = "100"
                msTopTabPanelLine = "303"
                msUnitPriceTextBox = "34"
                msUnitPriceLabel = "52"
                msRemarkFolder = "9"
                msPriceListDropDown = "24"
                msPriceListLabel = "25"
                msOkButton = "1"

                msInventoryCheckBox = "14"
                msSalesCheckBox = "13"
                msPurchaseCheckBox = "12"
                msAssitCheckbox = "42"

                msHeaderLinkTo = "24"
                msWasteGroupLinkTo = "6"
            End If
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
        End Sub

        Public Sub doAdjustFormSize(ByVal oForm As SAPbouiCOM.Form, ByVal oFolderItem As SAPbouiCOM.Item)
            Exit Sub
            'Temporary igonre the following Code

            'Dim oItem As SAPbouiCOM.Item
            'Dim iLeftStart As Integer

            ''Find a way to to Check if the Form has been resized
            'oItem = oFolderItem
            'Dim iTabLeftStart As Integer = oItem.Left
            ''If IDHAddOns.idh.addon.Base.LAYOUTTYPE = FixedValues.SBOLayout2009PL5 Then
            ''    miFolderWidth = 50 'oForm.Width / 12

            ''    oForm.Items.Item("163").Width = miFolderWidth
            ''    oForm.Items.Item("3").Width = miFolderWidth
            ''    oForm.Items.Item("4").Width = miFolderWidth
            ''    oForm.Items.Item("26").Width = miFolderWidth
            ''    oForm.Items.Item("27").Width = miFolderWidth
            ''    oForm.Items.Item("11").Width = miFolderWidth
            ''    oForm.Items.Item("9").Width = miFolderWidth
            ''    oForm.Items.Item("1320002069").Width = miFolderWidth
            ''Else
            ''    miFolderWidth = oItem.Width
            ''End If

            'Dim iTabSpace As Integer = oItem.Width + miFolderWidth + 20

            'oItem = oForm.Items.Item(msInventoryCheckBox)
            'Dim iHeadLeftStart As Integer = oItem.Left
            'Dim iHeadSpace As Integer = 100 + 5 + 80 + 5 + 70 + 5 + 170 + 5

            'Dim iWrkSpace As Integer
            'If iHeadLeftStart + iHeadSpace > iTabLeftStart + iTabSpace Then
            '    iWrkSpace = iHeadSpace
            '    iLeftStart = iHeadLeftStart
            'Else
            '    iWrkSpace = iTabSpace
            '    iLeftStart = iTabLeftStart
            'End If

            'If (iLeftStart + iWrkSpace) > oForm.Width Then
            '    miSpaceWidth = iWrkSpace
            '    'oForm.Width = iLeftStart + miSpaceWidth
            '    oForm.Resize(iLeftStart + miSpaceWidth, oForm.Height)
            'Else
            '    miSpaceWidth = 0
            '    'oForm.Width = oForm.Width + 0
            '    'oForm.Resize(oForm.Width + 0, oForm.Height)
            'End If
        End Sub

        Private Function doAddMainFolders(ByVal oForm As SAPbouiCOM.Form) As SAPbouiCOM.Item
            'Dim oFldr As SAPbouiCOM.Folder
            Dim oItm As SAPbouiCOM.Item
            Dim osItm As SAPbouiCOM.Item
            Dim oFolderItem As SAPbouiCOM.Item
            Dim oFolder As SAPbouiCOM.Folder

            oFolderItem = oForm.Items.Item(msTabAnchor) '(msTabAnchor)

            If IDHAddOns.idh.addon.Base.LAYOUTTYPE = FixedValues.SBOLayout2009PL5 Then
                miFolderWidth = 1 'oForm.Width / 12
                oForm.Items.Item("163").Width = miFolderWidth
                oForm.Items.Item("3").Width = miFolderWidth
                oForm.Items.Item("4").Width = miFolderWidth
                oForm.Items.Item("26").Width = miFolderWidth
                oForm.Items.Item("27").Width = miFolderWidth
                oForm.Items.Item("11").Width = miFolderWidth
                oForm.Items.Item("9").Width = miFolderWidth
                oForm.Items.Item("1320002069").Width = miFolderWidth
            Else
                miFolderWidth = oFolderItem.Width
            End If

            'Return oFolderItem

            oItm = oForm.Items.Add("IDH_WPTAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItm.Left = oFolderItem.Left + oFolderItem.Width
            oItm.Top = oFolderItem.Top
            oItm.Width = miFolderWidth
            oItm.Height = oFolderItem.Height
            oItm.Visible = True
            oItm.AffectsFormMode = False
            oItm.LinkTo = oFolderItem.LinkTo

            oFolder = CType(oItm.Specific, SAPbouiCOM.Folder)
            oFolder.Caption = getTranslatedWord("Waste Profile")
            oFolder.GroupWith(msTabAnchor)

            'oItm = oForm.Items.Add("IDH_UN01", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            'oItm.Left = oFolderItem.Left + miFolderWidth
            'oItm.Top = oFolderItem.Top
            'oItm.Width = miFolderWidth
            'oItm.Height = oFolderItem.Height
            'oItm.Visible = False
            'oItm.AffectsFormMode = False

            'oFolder = oItm.Specific
            'oFolder.Caption = getTranslatedWord("Classification")
            'oFolder.GroupWith(msRemarkFolder)

            osItm = oItm

            If IDHAddOns.idh.addon.Base.LAYOUTTYPE = FixedValues.SBOLayout2009PL5 Then
            Else
                Dim iExtraSpace As Integer
                oItm = oForm.Items.Item(msRightTabPanelLine)
                iExtraSpace = oForm.Width - oItm.Left - 25
                oItm.Left = oItm.Left + iExtraSpace 'miFolderWidth

                oItm = oForm.Items.Item(msBottomTabPanelLine)
                oItm.Width = oItm.Width + iExtraSpace 'miFolderWidth

                oItm = oForm.Items.Item(msTopTabPanelLine)
                oItm.Width = oItm.Width + iExtraSpace 'miFolderWidth
            End If

            'oItm = oForm.Items.Add("IDH_UN01", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            'oItm.Left = osItm.Left + miFolderWidth
            'oItm.Top = osItm.Top
            'oItm.Width = miFolderWidth
            'oItm.Height = osItm.Height
            'oItm.Visible = False
            'oItm.AffectsFormMode = False

            'oFolder = oItm.Specific
            'oFolder.Caption = getTranslatedWord("Classification")
            'oFolder.GroupWith(msRemarkFolder)

            ''oItm = oForm.Items.Add("IDH_WPTAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            ''oItm.Left = osItm.Left + osItm.Width
            ''oItm.Top = osItm.Top
            ''oItm.Width = osItm.Width
            ''oItm.Height = osItm.Height
            ''oItm.Visible = True
            ''oItm.AffectsFormMode = False

            ''oFolder = oItm.Specific
            ''oFolder.Caption = getTranslatedWord("Waste Profile")
            ''oFolder.GroupWith(msRemarkFolder)

            'osItm = oForm.Items.Item(msRightTabPanelLine)
            'osItm.Left = osItm.Left + oItm.Width

            'osItm = oForm.Items.Item(msBottomTabPanelLine)
            'osItm.Width = osItm.Width + oItm.Width

            'osItm = oForm.Items.Item(msTopTabPanelLine)
            'osItm.Width = osItm.Width + oItm.Width

            Return oFolderItem
        End Function

        ''Code from Joachim - To be removed once completed
        ''Only for the demo purpose
        Public Overrides Sub doLoadForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                Console.WriteLine("doLoadForm --> START ")
                Console.WriteLine("sICode = " + getParentSharedDataAsString(oForm, "WPIT_ITEMCODE"))
                Console.WriteLine("sIName = " + getParentSharedDataAsString(oForm, "WPIT_ITEMNAME"))
                Console.WriteLine("sWPItem = " + getParentSharedDataAsString(oForm, "WPITM"))

                doAddWF(oForm, "A_ACTIVE", False)

                doAddWF(oForm, "wfWPIT_ITEMCODE", False)
                doAddWF(oForm, "wfWPIT_ITEMNAME", False)
                doAddWF(oForm, "wfWPITM", False)
                doAddWF(oForm, "wfWPS_CUST", False)
                doAddWF(oForm, "wfWPS_CUSTNM", False)
                doAddWF(oForm, "wfWPS_ADDR", False)

                setWFValue(oForm, "wfWPIT_ITEMCODE", getParentSharedDataAsString(oForm, "WPIT_ITEMCODE"))
                setWFValue(oForm, "wfWPIT_ITEMNAME", getParentSharedDataAsString(oForm, "WPIT_ITEMNAME"))
                setWFValue(oForm, "wfWPITM", getParentSharedDataAsString(oForm, "WPITM"))
                setWFValue(oForm, "wfWPS_CUST", getParentSharedDataAsString(oForm, "WPS_CUST"))
                setWFValue(oForm, "wfWPS_CUSTNM", getParentSharedDataAsString(oForm, "WPS_CUSTNM"))
                setWFValue(oForm, "wfWPS_ADDR", getParentSharedDataAsString(oForm, "WPS_ADDR"))


                Dim oItem As SAPbouiCOM.Item
                'Dim owItem As SAPbouiCOM.Item
                'Dim oFolder As SAPbouiCOM.Folder
                Dim oFolderItem As SAPbouiCOM.Item

                oFolderItem = doAddMainFolders(oForm)
                doAdjustFormSize(oForm, oFolderItem)
                'oFolderItem = oForm.Items.Item(msTabAnchor)

                'owItem = oForm.Items.Add("IDH_UN01", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                'owItem.Left = oFolderItem.Left + miFolderWidth
                'owItem.Top = oFolderItem.Top
                'owItem.Width = miFolderWidth
                'owItem.Height = oFolderItem.Height
                'owItem.Visible = False
                'owItem.AffectsFormMode = False
                'oFolder = owItem.Specific
                'oFolder.Caption = getTranslatedWord("Classification")
                'oFolder.GroupWith(msRemarkFolder)

                'Dim iExtraSpace As Integer
                'owItem = oForm.Items.Item(msRightTabPanelLine)
                'iExtraSpace = oForm.Width - owItem.Left - 25
                'owItem.Left = owItem.Left + iExtraSpace 'miFolderWidth

                'owItem = oForm.Items.Item(msBottomTabPanelLine)
                'owItem.Width = owItem.Width + iExtraSpace 'miFolderWidth

                'owItem = oForm.Items.Item(msTopTabPanelLine)
                'owItem.Width = owItem.Width + iExtraSpace 'miFolderWidth

                Dim iTop As Integer = oFolderItem.Top + 25
                Dim oButton As SAPbouiCOM.Button

                'Added to the header
                oItem = oForm.Items.Item(msUnitPriceTextBox)
                doAddEdit(oForm, "IDH_WTITML", 0, oItem.Left, oItem.Top - 15, oItem.Width() - 15, -1, msHeaderLinkTo) '' "IDH_WTDIV")

                Dim oItemwt As SAPbouiCOM.Item
                oItemwt = doAddItem(oForm, "IDH_WTILNK", SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON, 0, oItem.Left - 20, oItem.Top - 15, 20, 13, "IDH_WTITML")
                CType(oItemwt.Specific, SAPbouiCOM.LinkedButton).LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items

                oItem = doAddItem(oForm, "IDH_ITMLUP", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 0, oItem.Left + oItem.Width() - 15, oItem.Top - 15, 14, 14, "IDH_WTITML")
                oButton = CType(oItem.Specific, SAPbouiCOM.Button)
                oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Image
                oButton.Image = IDHAddOns.idh.addon.Base.doFixImagePath("CFL.bmp")
                oItem = oForm.Items.Item(msUnitPriceLabel)
                doAddStatic(oForm, "S_WTITML", 0, oItem.Left, oItem.Top - 15, oItem.Width() - 10, -1, getTranslatedWord("Linked Item"), "IDH_WTITML")

                doAddDF(oForm, "OITM", "IDH_WTITML", "U_WTITMLN")

                'OnTime [#Ico000????] USA
                'START
                'Add field on form
                oItem = oForm.Items.Item(msPriceListDropDown)
                oItem = doAddItem(oForm, "IDH_WSTGRP", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX, 0, oItem.Left, oItem.Top + 15, oItem.Width, -1, "IDH_WTECAT")
                oItem.Visible = False
                oItem = oForm.Items.Item(msPriceListLabel)
                doAddStatic(oForm, "WG1", 0, oItem.Left, oItem.Top + 15, oItem.Width, -1, getTranslatedWord("Waste Group"), "IDH_WSTGRP").Visible = False
                oItem.DisplayDesc = True
                doFillWasteGroup(oForm)

                'Add field in header
                doAddDF(oForm, "OITM", "IDH_WSTGRP", "U_WSTGRP")

                doHeaderFields(oForm)
                'Adding Waste Profile Tab and Components
                doCreateWasteProfile(oForm)

                Try
                    doAddChemicalGrid(oForm)
                    doLoadChemicalsGrid(oForm)

                    'Add and Load Waste Group Grid
                    doAddWasteGroupGrid(oForm)
                    doLoadWasteGroupGrid(oForm)
                Catch ex As Exception
                End Try
                'END
                'OnTime [#Ico000????] USA

                oItem = oForm.Items.Item(msOkButton)
                oItem = doAddItem(oForm, "IDHITMPRPR", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 0, oItem.Left + 200, oItem.Top, oItem.Width, oItem.Height, "1")
                CType(oItem.Specific, SAPbouiCOM.Button).Caption = getTranslatedWord("CIP / SIP")

                oItem = doAddItem(oForm, "IDHLBANTMP", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 0, oItem.Left + oItem.Width + 30, oItem.Top, oItem.Width + 30, oItem.Height, "1")
                CType(oItem.Specific, SAPbouiCOM.Button).Caption = getTranslatedWord("Lab Analysis Codes")


                Console.WriteLine("doLoadForm --> END ")
                Console.WriteLine("sICode = " + getParentSharedDataAsString(oForm, "WPIT_ITEMCODE"))
                Console.WriteLine("sIName = " + getParentSharedDataAsString(oForm, "WPIT_ITEMNAME"))
                Console.WriteLine("sWPItem = " + getParentSharedDataAsString(oForm, "WPITM"))
                Try
                    oForm.Items.Item("44").Click()
                    oForm.Items.Item("7").Click()
                Catch ex As Exception
                End Try
                'doAdjustFormSize(oForm, oFolderItem)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString, "Error loading the form.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXELF", {Nothing})
                BubbleEvent = False
                Exit Sub
            End Try
        End Sub

        Private Sub doFillDocuments(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_WTDCUM", "[@" & goParent.goConfigTable & "]", "Code", "U_Desc", "Code like 'WDOC%'")
        End Sub

        Private Sub doFillENCategory(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_WTECAT", "[@IDH_WTENCT]", "Code", "U_Desc")
        End Sub

        Private Sub doFillENBased(ByVal oForm As SAPbouiCOM.Form, ByVal sItemID As String)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item(sItemID)
            oItem.DisplayDesc = True
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = CType(oItem.Specific(), SAPbouiCOM.ComboBox)

            Dim oValidValues As SAPbouiCOM.ValidValues
            oValidValues = oCombo.ValidValues
            doClearValidValues(oValidValues)

            oValidValues.Add("A", "Actual")
            oValidValues.Add("P", "Protocol")
        End Sub

        Private Sub doFillDisposalMethod(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_DCD", "[@IDH_WTDIRE]", "Code", "U_WTOD")
        End Sub

        Private Sub doFillWEEE(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_WEEECD", "[@IDH_WEEE]", "Code", "Name")
        End Sub

        Private Sub doFillWEEECL(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_WEEECL")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)
            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("", "None")
            oCombo.ValidValues.Add("Domestic", "Domestic")
            oCombo.ValidValues.Add("Commercial", "Commercial")
        End Sub

        Private Sub doFillWasteGroup(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_WSTGRP", "[@ISB_WASTEGRP]", "U_WstGpCd", "U_WstGpNm")
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sItemId As String = pVal.ItemUID

            Console.WriteLine("doItemEvent--> " + pVal.EventType.ToString())
            
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If sItemId = "39" Then
                        'Ontime 32756: WR1_NewDev_Planning_BOOKING IN STOCK - Always Display Linked Item On Waste materials
                        'remarking the hide link item. Should be displayed all the time
                        'doShowHideLinkItem(oForm)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_RESIZE Then
                If pVal.BeforeAction = False Then
                    Dim owItem As SAPbouiCOM.Item = oForm.Items.Item("INRECT")
                    owItem.Width = oForm.Width - miWasteProfileRecRightSpace
                    'If oForm.Width < miFormWidth Then
                    'miFormWidth = -1 'oForm.Width - miFolderWidth
                    'End If

                    'Setting Height & Width of WGP Grid 
                    Dim oWGItem As SAPbouiCOM.Item = oForm.Items.Item("WGPGRID")
                    oWGItem.Height = owItem.Height - 40
                    oWGItem.Width = owItem.Width - 30

                    'Setting Height & Width of Chemical Grid 
                    Dim oCGItem As SAPbouiCOM.Item = oForm.Items.Item("CHMGRID")
                    oCGItem.Height = CInt((owItem.Height / 2)) - 10
                    'oCGItem.Width = owItem.Width - 170

                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DRAW Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    doPrepareWasteProfileFields(oForm)
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_GOT_FOCUS _
                 Then
                'ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_GOT_FOCUS Then

                'sICode = getParentSharedDataAsString(oForm, "WPIT_ITEMCODE")
                'sIName = getParentSharedDataAsString(oForm, "WPIT_ITEMNAME")
                'sWPItem = getParentSharedDataAsString(oForm, "WPITM")
                Console.WriteLine("IN Block doItemEvent--> et_FORM_ACTIVATE " + pVal.EventType.ToString())
                Console.WriteLine("sICode = " + getParentSharedDataAsString(oForm, "WPIT_ITEMCODE"))
                Console.WriteLine("sIName = " + getParentSharedDataAsString(oForm, "WPIT_ITEMNAME"))
                Console.WriteLine("sWPItem = " + getParentSharedDataAsString(oForm, "WPITM"))

                If pVal.BeforeAction = False Then
                    doPrepareWasteProfileFields(oForm)
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                Dim oEdt As SAPbouiCOM.EditText
                'Dim oEdtSource As SAPbouiCOM.EditText

                If pVal.BeforeAction = False Then
                    oEdt = CType(oForm.Items.Item("IDH_TOTPER").Specific, SAPbouiCOM.EditText)
                    If sItemId = "IDH_LIQPER" Then
                        Return isValidPercentage(oForm, BubbleEvent, "IDH_LIQPER", "U_WCLiquidPerc", oEdt)
                    ElseIf sItemId = "IDH_SOLPER" Then
                        Return isValidPercentage(oForm, BubbleEvent, "IDH_SOLPER", "U_WCSolidPerc", oEdt)
                    ElseIf sItemId = "IDH_SLUPER" Then
                        Return isValidPercentage(oForm, BubbleEvent, "IDH_SLUPER", "U_WCSludgesPerc", oEdt)
                    ElseIf sItemId = "IDH_GASPER" Then
                        Return isValidPercentage(oForm, BubbleEvent, "IDH_GASPER", "U_WCGasPerc", oEdt)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                Dim oItem As SAPbouiCOM.Item
                oItem = oForm.Items.Item(sItemId)
                If pVal.BeforeAction = False Then
                    If sItemId = "IDHITMPRPR" Then
                        Dim sSupplier As String = CType(getDFValue(oForm, "OITM", "U_DispFacCd"), String) 'ility")
                        Dim sBPCd As String = ""
                        Dim sAddress As String = ""

                        Dim saWPFlagSplit As String() = CType(getWFValue(oForm, "IDH_WPFLAG"), String())
                        If Not saWPFlagSplit Is Nothing AndAlso saWPFlagSplit.Length > 0 Then
                            sBPCd = saWPFlagSplit(0)
                            sAddress = saWPFlagSplit(2)

                            Dim sICode As String = saWPFlagSplit(3)
                            'Dim sIDesc As String = saWPFlagSplit(4)
                            Dim sFlag As String = saWPFlagSplit(6)
                            Dim sNewICode As String = saWPFlagSplit(7)
                            Dim sCIPCPD As String = saWPFlagSplit(8)

                            'Dim sNewICode As String = getDFValue(oForm, "OITM", "ItemCode")
                            Dim sIDesc As String = CType(getDFValue(oForm, "OITM", "ItemName"), String)
                            If sFlag.Equals("BNEW") = False Then
                                If sCIPCPD = "N" Then
                                    Dim oCIP As IDH_CSITPR = New IDH_CSITPR()
                                    If sNewICode Is Nothing OrElse sNewICode.Length = 0 Then
                                        sNewICode = CType(getDFValue(oForm, "OITM", "ItemCode"), String)
                                    End If
                                    If sNewICode IsNot Nothing AndAlso sNewICode.Length > 0 _
                                       AndAlso sICode IsNot Nothing AndAlso sICode.Length > 0 Then
                                        oCIP.doCopyItemCIP(sICode, sNewICode, sIDesc, sBPCd, sAddress, sSupplier)
                                        saWPFlagSplit(8) = "Y"
                                    End If
                                End If
                                saWPFlagSplit(9) = sSupplier
                            End If
                        End If

                        Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.prices.ItemProfileCIPSIP = New WR1_FR2Forms.idh.forms.fr2.prices.ItemProfileCIPSIP(Nothing, oForm.UniqueID, Nothing)
                        oOOForm.ItemCode = CType(getDFValue(oForm, "OITM", "ItemCode"), String)
                        oOOForm.ItemDesc = CType(getDFValue(oForm, "OITM", "ItemName"), String)
                        oOOForm.ItemGroup = CType(getDFValue(oForm, "OITM", "ItmsGrpCod"), String)
                        oOOForm.Supplier = sSupplier
                        oOOForm.Customer = sBPCd
                        oOOForm.Address = sAddress

                        ''oOOForm.Customer = 
                        oOOForm.doShowModal(oForm)
                        ''
                    ElseIf sItemId = "IDHLBANTMP" Then
                        setSharedData(oForm, "ITEM_CODE", CType(getDFValue(oForm, "OITM", "ItemCode"), String))
                        goParent.doOpenModalForm("IDHITMLBAN", oForm)
                    ElseIf sItemId = "IDH_UN01" Then
                        oForm.PaneLevel = 100
                    ElseIf sItemId = "IDH_UNLUP" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            With oForm.DataSources.DBDataSources.Item("OITM")
                                setSharedData(oForm, "IDH_UNCODE", .GetValue("U_WTUNCD", .Offset).Trim())
                            End With
                            goParent.doOpenModalForm("IDH_WTUNSR", oForm)
                        End If
                    ElseIf sItemId = "IDH_EWCLUP" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            With oForm.DataSources.DBDataSources.Item("OITM")
                                'OnTime Ticket 25018 - EWC Code Search Bug
                                'Restructuring the screen with two search fields and two columns in the grid
                                'setSharedData(oForm, "IDH_ECCOD", .GetValue("U_EWC", .Offset).Trim())
                                setSharedData(oForm, "IDH_WTWC", .GetValue("U_EWC", .Offset).Trim())
                            End With
                            goParent.doOpenModalForm("IDH_WTECSR", oForm)
                        End If
                    ElseIf sItemId = "IDH_HAZLUP" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            With oForm.DataSources.DBDataSources.Item("OITM")
                                setSharedData(oForm, "IDH_HZCOD", .GetValue("U_HAZCD", .Offset).Trim())
                            End With
                            goParent.doOpenModalForm("IDH_WTHZSR", oForm)
                        End If
                    ElseIf sItemId = "IDH_ITMLUP" Then
                        setSharedData(oForm, "TP", "WAST")
                        setSharedData(oForm, "IDH_GRPCOD", Config.Parameter("IGR-WMS"))
                        Dim sItem As String
                        sItem = CType(getDFValue(oForm, "OITM", "U_WTITMLN"), String)
                        setSharedData(oForm, "IDH_ITMCOD", sItem)
                        setSharedData(oForm, "IDH_INVENT", "Y")
                        goParent.doOpenModalForm("IDHWISRC", oForm)
                    ElseIf sItemId = "IDH_SHNLKP" Then
                        Dim sShippingNm As String = CType(getDFValue(oForm, "OITM", "U_ShippingNm"), String)
                        Dim sPSNCode As String = ""
                        If sShippingNm IsNot Nothing AndAlso sShippingNm.Length > 0 Then
                            'get code for this shipping name
                            Dim oRec As SAPbobsCOM.Recordset = Nothing
                            oRec = goParent.goDB.doSelectQuery("SELECT Code FROM [@ISB_SHIPNM] WHERE CAST(U_ShipNm AS NVARCHAR(MAX)) = '" + sShippingNm + "'")
                            If oRec.RecordCount > 0 Then
                                sPSNCode = oRec.Fields.Item("Code").Value.ToString()
                            End If
                            DataHandler.INSTANCE.doReleaseRecordset(oRec)
                        End If
                        setSharedData(oForm, "SHIPNMCD", sPSNCode)
                        goParent.doOpenModalForm("SHIPNMGEN", oForm)
                    ElseIf sItemId = "DSPLKP" Then
                        setSharedData(oForm, "TRG", "DSPLKP")
                        setSharedData(oForm, "IDH_BPCOD", getItemValue(oForm, "IDH_DPFACD"))
                        setSharedData(oForm, "IDH_NAME", getItemValue(oForm, "IDH_DISPFA"))
                        setSharedData(oForm, "IDH_TYPE", "S")
                        goParent.doOpenModalForm("IDHCSRCH", oForm)
                        'KA -- START -- 20121025
                    ElseIf sItemId = "DFALKP" Then
                        setSharedData(oForm, "IDH_CUSCOD", getItemValue(oForm, "IDH_DPFACD"))
                        setSharedData(oForm, "IDH_ADRTYP", "S")
                        goParent.doOpenModalForm("IDHASRCH", oForm)
                        'KA -- END -- 20121025
                    ElseIf sItemId = "CNTLKP" Then
                        setSharedData(oForm, "ITMSGRPCOD", "113")
                        'temp comment, will fix after confirming from HH'setSharedData(oForm, "IDH_GRPCOD", "113")
                        goParent.doOpenModalForm("IDHISRC", oForm)
                    ElseIf sItemId = "IDH_EP1LUP" Then
                        setSharedData(oForm, "TRG", "LKP_EPACD1")
                        goParent.doOpenModalForm("EPASCR", oForm)
                    ElseIf sItemId = "IDH_EP2LUP" Then
                        setSharedData(oForm, "TRG", "LKP_EPACD2")
                        goParent.doOpenModalForm("EPASCR", oForm)
                    ElseIf sItemId = "IDH_EP3LUP" Then
                        setSharedData(oForm, "TRG", "LKP_EPACD3")
                        goParent.doOpenModalForm("EPASCR", oForm)
                    ElseIf sItemId = "IDH_EP4LUP" Then
                        setSharedData(oForm, "TRG", "LKP_EPACD4")
                        goParent.doOpenModalForm("EPASCR", oForm)
                    ElseIf sItemId = "IDH_EP5LUP" Then
                        setSharedData(oForm, "TRG", "LKP_EPACD5")
                        goParent.doOpenModalForm("EPASCR", oForm)
                    ElseIf sItemId = "IDH_EP6LUP" Then
                        setSharedData(oForm, "TRG", "LKP_EPACD6")
                        goParent.doOpenModalForm("EPASCR", oForm)
                    ElseIf sItemId = "14" Then
                        'Ontime 32756: WR1_NewDev_Planning_BOOKING IN STOCK - Always Display Linked Item On Waste materials
                        'remarking the hide link item. Should be displayed all the time
                        'doShowHideLinkItem(oForm)
                        ''ElseIf sItemId = "1" Then
                        ''    Dim sICode As String = getParentSharedData(oForm, "WPIT_ITEMCODE")
                        ''    Dim sIName As String = getParentSharedData(oForm, "WPIT_ITEMNAME")
                        ''    Dim sWPItem As String = getParentSharedData(oForm, "WPITM")
                        ''    If sWPItem = "TRUE" And sICode.Length > 0 And sIName.Length > 0 Then
                        ''        Dim sNewIC As String = getDFValue(oForm, "OITM", "ItemCode")
                        ''        Dim sNewIN As String = getDFValue(oForm, "OITM", "ItemName")

                        ''        'setParentSharedData(oForm, "WPIT_ITEMCODE", getDFValue(oForm, "OITM", "ItemCode"))
                        ''        'setParentSharedData(oForm, "WPIT_ITEMNAME", getDFValue(oForm, "OITM", "ItemName"))
                        ''        oForm.Close()
                        ''    End If
                    End If
                Else
                    If sItemId = "IDH_WPTAB" Then
                        oForm.PaneLevel = 201
                        oItem = oForm.Items.Item("IDH_LWGTAB")
                        Dim owItem As SAPbouiCOM.Folder
                        owItem = CType(oItem.Specific, SAPbouiCOM.Folder)
                        owItem.Select()
                    ElseIf sItemId = "IDH_LWGTAB" Then
                        oForm.PaneLevel = 201
                    ElseIf sItemId = "IDH_PHCTAB" Then
                        oForm.PaneLevel = 202
                    ElseIf sItemId = "IDH_WCMTAB" Then
                        oForm.PaneLevel = 203
                        If Not (oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or _
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            'oForm.Mode = SAPbouiCOM.BoFormMode.fm_EDIT_MODE) Then
                            'oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Or _
                            doLoadChemicalsGrid(oForm)
                        End If
                    ElseIf sItemId = "IDH_METTAB" Then
                        oForm.PaneLevel = 204
                    ElseIf sItemId = "IDH_RPTTAB" Then
                        oForm.PaneLevel = 205
                    ElseIf sItemId = "IDH_DOTTAB" Then
                        oForm.PaneLevel = 206
                    ElseIf sItemId = "IDH_WEETAB" Then
                        oForm.PaneLevel = 207
                    ElseIf sItemId = "IDH_MISTAB" Then
                        oForm.PaneLevel = 208
                    ElseIf sItemId = "IDH_WGPTAB" Then
                        oForm.PaneLevel = 209
                        If Not (oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or _
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_EDIT_MODE) Then
                            'oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Or _
                            doLoadWasteGroupGrid(oForm)
                        End If
                    End If
                End If
                'KA -- START -- 20121022
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = False Then
                    If pVal.CharPressed = 9 Then
                        If sItemId = "CHMGRID" Then
                            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "CHMGRID")
                            Dim sChmCode As String = CType(oGridN.doGetFieldValue("U_ChemicalCd"), String) 'U_ChemicalCd Chemical odes
                            If sChmCode = "*" Then
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                goParent.doOpenModalForm("CASTBLSCR", oForm)
                            End If
                        End If
                    End If
                End If
                'KA -- END -- 20121022
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_LOST_FOCUS Then
            End If
            'Return False
            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then

                    'Dim sWPFlag As String = String.Empty
                    'sWPFlag = getWFValue(oForm, "IDH_WPFLAG")
                    'Dim saWPFlagSplit As String() = sWPFlag.Split("#")
                    'If sWPFlag <> Nothing Then
                    Dim saWPFlagSplit As String() = CType(getWFValue(oForm, "IDH_WPFLAG"), String())
                    If Not saWPFlagSplit Is Nothing AndAlso saWPFlagSplit.Length > 0 Then
                        Dim sBPCd As String = saWPFlagSplit(0)
                        Dim sBPNm As String = saWPFlagSplit(1)
                        Dim sAddress As String = saWPFlagSplit(2)
                        Dim sICode As String = saWPFlagSplit(3)
                        Dim sIName As String = saWPFlagSplit(4)
                        Dim sWPItem As String = saWPFlagSplit(5)
                        Dim sWPItemStatus As String = saWPFlagSplit(6)
                        Dim sNewICode As String = saWPFlagSplit(7)
                        Dim sCIPCPD As String = saWPFlagSplit(8)

                        If sWPItem <> Nothing And (sWPItemStatus = "OLD" Or sWPItemStatus = "NEW") Then
                            Dim sNewIC As String

                            If sWPItemStatus = "Old" Then
                                Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("5")
                                If oItem.Enabled = True Then
                                    'oForm.Items.Item("5").Specific.Value = sNewICode
                                    setItemValue(oForm, "5", sNewICode)
                                End If
                                sNewIC = sNewICode
                            Else
                                sNewIC = CType(getDFValue(oForm, "OITM", "ItemCode"), String)
                            End If

                            Dim sNewIN As String = CType(getDFValue(oForm, "OITM", "ItemName"), String)

                            setParentSharedData(oForm, "WPIT_ITEMCODE", sNewIC)
                            setParentSharedData(oForm, "WPIT_ITEMNAME", sNewIN)

                            'sWPFlag = sBPCd + "#" + sBPNm + "#" + sAddress + "#" + sNewIC + "#" + sNewIN + "#" + sWPItem + "#" + "NEW" 'Set value for the newly added item code
                            'setWFValue(oForm, "IDH_WPFLAG", sWPFlag)

                            saWPFlagSplit(7) = sNewIC
                            'saWPFlagSplit(3) = sNewIC
                            saWPFlagSplit(4) = sNewIN
                            saWPFlagSplit(6) = "NEW"

                            'oForm.Close()
                        ElseIf sWPItem <> Nothing And sWPItemStatus = "BNEW" Then
                            'Dim sMsg As String = "Do you want to create the profile from Waste Profile Template?"
                            'Dim iSelection As Integer = goParent.doMessage(sMsg, 1, "Ok", "Cancel", Nothing, True)
                            Dim sNewIC As String = CType(getDFValue(oForm, "OITM", "ItemCode"), String)
                            Dim sNewIN As String = CType(getDFValue(oForm, "OITM", "ItemName"), String)

                            setParentSharedData(oForm, "WPIT_ITEMCODE", sNewIC)
                            setParentSharedData(oForm, "WPIT_ITEMNAME", sNewIN)

                            'sWPFlag = sBPCd + "#" + sBPNm + "#" + sAddress + "#" + sNewIC + "#" + sNewIN + "#" + sWPItem + "#" + "BNEW" 'Set value for the newly added item code
                            'setWFValue(oForm, "IDH_WPFLAG", sWPFlag)

                            saWPFlagSplit(7) = sNewIC
                            'saWPFlagSplit(3) = sNewIC
                            saWPFlagSplit(4) = sNewIN
                        End If

                        Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "CHMGRID")
                        oUpdateGrid.doProcessData()

                        Dim oWGPUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "WGPGRID")
                        oWGPUpdateGrid.doProcessData()
                    End If
                End If
            Else
                If oForm.Mode.Equals(SAPbouiCOM.BoFormMode.fm_FIND_MODE) = False Then
                    Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "CHMGRID")
                    If oUpdateGrid.doProcessData() = False Then
                        BubbleEvent = False
                    End If

                    Dim oWGPUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "WGPGRID")
                    If oWGPUpdateGrid.doProcessData() = False Then
                        BubbleEvent = False
                    End If

                End If

                If pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.ActionSuccess = True Then
                    'Dim sWPFlag As String = String.Empty
                    'sWPFlag = getWFValue(oForm, "IDH_WPFLAG")
                    'Dim saWPFlagSplit As String() = sWPFlag.Split("#")
                    'If sWPFlag <> Nothing Then
                    Dim saWPFlagSplit As String() = CType(getWFValue(oForm, "IDH_WPFLAG"), String())
                    If Not saWPFlagSplit Is Nothing AndAlso saWPFlagSplit.Length > 0 Then
                        Dim sBPCd As String = saWPFlagSplit(0)
                        Dim sBPNm As String = saWPFlagSplit(1)
                        Dim sAddress As String = saWPFlagSplit(2)
                        'sAddress = Formatter.doFormatStringForSQL(sAddress)

                        Dim sICode As String = saWPFlagSplit(3)
                        Dim sIName As String = saWPFlagSplit(4)
                        Dim sWPItem As String = saWPFlagSplit(5)
                        Dim sWPItemStatus As String = saWPFlagSplit(6)
                        Dim sNewICode As String = saWPFlagSplit(7)
                        Dim sCIPCPD As String = saWPFlagSplit(8)
                        Dim sSupplier As String = saWPFlagSplit(9)

                        If sSupplier.Length = 0 Then
                            sSupplier = CType(getDFValue(oForm, "OITM", "U_DispFacility"), String)
                            If sSupplier.Length = 0 Then
                                sSupplier = getItemValue(oForm, "IDH_DISPFA")
                            End If
                        End If

                        If sWPItem <> Nothing And sWPItemStatus = "NEW" Then
                            'Dim sNewIC As String = sICode
                            'Dim sNewIN As String = sIName

                            setParentSharedData(oForm, "WPIT_ITEMCODE", sNewICode)
                            setParentSharedData(oForm, "WPIT_ITEMNAME", sIName)
                            'sWPFlag = sNewIC + "#" + sNewIN + "#" + sWPItem + "#" + "NEW" 'Set value for the newly added item code
                            'setWFValue(oForm, "IDH_WPFLAG", sWPFlag)

                            'Insert New record for the Waste Profile Table
                            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "WPROS")
                            Dim sQry As String
                            ''sQry = "INSERT INTO [@ISB_WSTPROFILE]([Code], [Name], [U_ProfileNo] ,[U_Descp] ,[U_ValidFm] ,[U_ValidTo] ,[U_AddCd] ,[U_CardCd] ,[U_CardNm] ,[U_ItemCd] ,[U_ItemNm])" & _
                            ''        "VALUES('" + sWPCode + "', '" + sWPCode + "' ,'" + sWPCode + "' ,'" + sWPCode + "' ,'" + Date.Now + "' ,'" + Date.Now + "' ,'" + sAddress + "' ,'" + sBPCd + "' ,'" + sBPNm + "' ,'" + sICode + "' ,'" + sIName + "' )"
                            ''sQry = "INSERT INTO [@ISB_WSTPROFILE]([Code], [Name], [U_ProfileNo] ,[U_Descp] , [U_AddCd] ,[U_CardCd] ,[U_CardNm] ,[U_ItemCd] ,[U_ItemNm])" & _
                            ''        "VALUES('" + sWPCode + "', '" + sWPCode + "' ,'" + sWPCode + "' ,'" + sWPCode + "' ,'" + sAddress + "' ,'" + sBPCd + "' ,'" + sBPNm + "' ,'" + sICode + "' ,'" + sIName + "' )"


                            ''20150710: Had to split the delete and insert query becuase of changes in FWw to run doUpdateQuery 
                            ''Start from here: need to create ISB_WASTEPROFILE dbObject 
                            ''also replicate same in else of this block
                            ' ''Including the delete qry to avoid getting same Item Code in waste profile table that was previously deleted
                            'sQry = " DELETE [@ISB_WSTPROFILE] where U_AddCd = '" + sAddress + "' AND U_CardCd = '" + sBPCd + "' AND U_ItemCd = '" + sNewICode + "'; "
                            'goParent.goDB.doUpdateQuery(sQry)

                            'sQry = " INSERT INTO [@ISB_WSTPROFILE]([Code], [Name], [U_ProfileNo] ,[U_Descp] , [U_AddCd] ,[U_CardCd] ,[U_CardNm] ,[U_ItemCd] ,[U_ItemNm])" & _
                            '        "VALUES('" + oNumbers.CodeCode + "', '" + oNumbers.NameCode + "' ,'" + oNumbers.CodeCode + "' ,'" + oNumbers.CodeCode + "' ,'" + sAddress + "' ,'" + sBPCd + "' ,'" + sBPNm + "' ,'" + sNewICode + "' ,'" + sIName + "' ) "
                            'goParent.goDB.doUpdateQuery(sQry)

                            Dim oWP As ISB_WSTPROFILE = New ISB_WSTPROFILE
                            oWP.doAddEmptyRow()
                            oWP.Code = oNumbers.CodeCode
                            oWP.Name = oNumbers.NameCode
                            oWP.U_ProfileNo = oNumbers.CodeCode
                            oWP.U_Descp = oNumbers.CodeCode
                            oWP.U_AddCd = sAddress
                            oWP.U_CardCd = sBPCd
                            oWP.U_CardNm = sBPNm
                            oWP.U_ItemCd = sNewICode
                            oWP.U_ItemNm = sIName
                            oWP.doProcessData()


                            'START
                            'Now Copy Chemicals to the new item from [@ISB_CASTBL]
                            'CAS Table Sequence No.: CASSEQ
                            'Dim sCASSEQ As String = goParent.goDB.doNextNumber("CASSEQ").ToString()
                            sQry = "SELECT * FROM [@ISB_CASTBL] where U_ItemCd = '" + sICode + "'"
                            'run a query and do a loop to insert all records with New ItemCode
                            Dim oRecordset As SAPbobsCOM.Recordset = Nothing
                            oRecordset = goParent.goDB.doSelectQuery(sQry)
                            If oRecordset.RecordCount > 0 Then
                                Dim oCAS As ISB_CASTBL = New ISB_CASTBL()
                                oCAS.doCopyCASItems(sNewICode, sIName, oRecordset)
                            End If
                            DataHandler.INSTANCE.doReleaseRecordset(oRecordset)

                            doLoadChemicalsGrid(oForm)
                            'END

                            'KA -- START -- 20121022
                            'START
                            'Copy Waste Group to the new item from [@IDH_WGPCNTY]
                            'WGPCNTY Table Sequence No.: WGCNTSEQ
                            sQry = "SELECT * FROM [@IDH_WGPCNTY] where U_ItemCd = '" + sICode + "'"
                            'run a query and do a loop to insert all records with New ItemCode
                            Dim oRSWasteGpCounty As SAPbobsCOM.Recordset = Nothing
                            oRSWasteGpCounty = goParent.goDB.doSelectQuery(sQry)
                            If oRSWasteGpCounty.RecordCount > 0 Then
                                Dim oWGPCNTY As IDH_WGPCNTY = New IDH_WGPCNTY()
                                oWGPCNTY.doCopyWGPCNTYItems(sNewICode, sIName, oRSWasteGpCounty)
                            End If

                            DataHandler.INSTANCE.doReleaseObject(CType(oRSWasteGpCounty, Object))
                            DataHandler.INSTANCE.doReleaseRecordset(oRecordset)

                            doLoadChemicalsGrid(oForm)
                            'END
                            'KA -- END -- 20121022

                            'Now Copy the CIP and SIP as well
                            If sCIPCPD = "N" Then
                                Dim oCIP As IDH_CSITPR = New IDH_CSITPR()
                                oCIP.doCopyItemCIP(sICode, sNewICode, sIName, sBPCd, sAddress, sSupplier)
                            End If

                            'Before closing the form Update the Chemicals Grid
                            Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "CHMGRID")
                            oUpdateGrid.doProcessData()

                            Dim oWGPUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "WGPGRID")
                            oWGPUpdateGrid.doProcessData()

                            doClearSharedData(oForm)
                            doClearParentSharedData(oForm)
                            oForm.Close()
                        ElseIf sWPItem <> Nothing And sWPItemStatus = "BNEW" Then
                            'Dim sMsg As String = "Do you want to create the profile from Waste Profile Template?"
                            'Dim iSelection As Integer = goParent.doMessage(sMsg, 1, "Ok", "Cancel", Nothing, True)
                            'Dim sNewIC As String = sICode
                            'Dim sNewIN As String = sIName

                            setParentSharedData(oForm, "WPIT_ITEMCODE", sNewICode)
                            setParentSharedData(oForm, "WPIT_ITEMNAME", sIName)
                            'sWPFlag = sNewIC + "#" + sNewIN + "#" + sWPItem + "#" + "NEW" 'Set value for the newly added item code
                            'setWFValue(oForm, "IDH_WPFLAG", sWPFlag)

                            'Insert New record for the Waste Profile Table
                            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "WPROS")
                            'Dim sQry As String
                            ''sQry = "INSERT INTO [@ISB_WSTPROFILE]([Code], [Name], [U_ProfileNo] ,[U_Descp] ,[U_ValidFm] ,[U_ValidTo] ,[U_AddCd] ,[U_CardCd] ,[U_CardNm] ,[U_ItemCd] ,[U_ItemNm])" & _
                            ''        "VALUES('" + sWPCode + "', '" + sWPCode + "' ,'" + sWPCode + "' ,'" + sWPCode + "' ,'" + Date.Now + "' ,'" + Date.Now + "' ,'" + sAddress + "' ,'" + sBPCd + "' ,'" + sBPNm + "' ,'" + sICode + "' ,'" + sIName + "' )"
                            ''sQry = "INSERT INTO [@ISB_WSTPROFILE]([Code], [Name], [U_ProfileNo] ,[U_Descp] , [U_AddCd] ,[U_CardCd] ,[U_CardNm] ,[U_ItemCd] ,[U_ItemNm])" & _
                            ''        "VALUES('" + sWPCode + "', '" + sWPCode + "' ,'" + sWPCode + "' ,'" + sWPCode + "' ,'" + sAddress + "' ,'" + sBPCd + "' ,'" + sBPNm + "' ,'" + sICode + "' ,'" + sIName + "' )"

                            ''Including the delete qry to avoid getting same Item Code in waste profile table that was previously deleted
                            'sQry = "DELETE [@ISB_WSTPROFILE] where U_AddCd = '" + sAddress + "' AND U_CardCd = '" + sBPCd + "' AND U_ItemCd = '" + sNewICode + "';"
                            'sQry = "INSERT INTO [@ISB_WSTPROFILE]([Code], [Name], [U_ProfileNo] ,[U_Descp] , [U_AddCd] ,[U_CardCd] ,[U_CardNm] ,[U_ItemCd] ,[U_ItemNm])" & _
                            '        "VALUES('" + oNumbers.CodeCode + "', '" + oNumbers.NameCode + "' ,'" + oNumbers.CodeCode + "' ,'" + oNumbers.CodeCode + "' ,'" + sAddress + "' ,'" + sBPCd + "' ,'" + sBPNm + "' ,'" + sNewICode + "' ,'" + sIName + "' )"
                            'goParent.goDB.doUpdateQuery(sQry)

                            Dim oWP As ISB_WSTPROFILE = New ISB_WSTPROFILE
                            oWP.doAddEmptyRow()
                            oWP.Code = oNumbers.CodeCode
                            oWP.Name = oNumbers.NameCode
                            oWP.U_ProfileNo = oNumbers.CodeCode
                            oWP.U_Descp = oNumbers.CodeCode
                            oWP.U_AddCd = sAddress
                            oWP.U_CardCd = sBPCd
                            oWP.U_CardNm = sBPNm
                            oWP.U_ItemCd = sNewICode
                            oWP.U_ItemNm = sIName
                            oWP.doProcessData()

                            'Before closing the form Update the Chemicals Grid
                            Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "CHMGRID")
                            oUpdateGrid.doProcessData()

                            Dim oWGPUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "WGPGRID")
                            oWGPUpdateGrid.doProcessData()

                            doClearSharedData(oForm)
                            doClearParentSharedData(oForm)
                            oForm.Close()
                        End If
                    End If
                End If
            End If
            'MyBase.doButtonID1(oForm, pVal, BubbleEvent)
        End Sub

        Public Sub doPrepareWasteProfileFields(ByVal oForm As SAPbouiCOM.Form)

            Console.WriteLine("doPrepareWasteProfileFields --> START ")
            Console.WriteLine("sICode = " + getWFValue(oForm, "wfWPIT_ITEMCODE"))
            Console.WriteLine("sIName = " + getWFValue(oForm, "wfWPIT_ITEMNAME"))
            Console.WriteLine("sWPItem = " + getWFValue(oForm, "wfWPITM"))

            Dim bIsActive As Boolean = CType(getWFValue(oForm, "A_ACTIVE"), Boolean)
            If bIsActive = False Then
                'If pVal.BeforeAction = False Then
                setWFValue(oForm, "A_ACTIVE", True)

                Try
                    Dim oChk As SAPbouiCOM.CheckBox
                    Dim oEdit As SAPbouiCOM.EditText
                    Dim oCmb As SAPbouiCOM.ComboBox

                    Dim sBPCd As String = String.Empty
                    Dim sBPNm As String = String.Empty
                    Dim sAddress As String = String.Empty
                    Dim sICode As String = String.Empty
                    Dim sIName As String = String.Empty

                    Dim sWPItem As String = String.Empty
                    Dim sNewICode As String = ""
                    Dim sSupplier As String = CType(getDFValue(oForm, "OITM", "U_DispFacCd"), String) '"U_DispFacility")
                    Dim bWPReopen As Boolean = False

                    sICode = getWFValue(oForm, "wfWPIT_ITEMCODE") 'getParentSharedDataAsString(oForm, "WPIT_ITEMCODE")
                    sIName = getWFValue(oForm, "wfWPIT_ITEMNAME") 'getParentSharedDataAsString(oForm, "WPIT_ITEMNAME")
                    sWPItem = getWFValue(oForm, "wfWPITM") 'getParentSharedDataAsString(oForm, "WPITM")

                    If sWPItem Is Nothing Then
                        'Clear the global values
                        sBPCd = CType(IDHAddOns.idh.addon.Base.PARENT.getGlobalValue("WPS_CUST"), String)
                        sBPNm = CType(IDHAddOns.idh.addon.Base.PARENT.getGlobalValue("WPS_CUSTNM"), String)
                        sAddress = CType(IDHAddOns.idh.addon.Base.PARENT.getGlobalValue("WPS_ADDR"), String)

                        'setSharedData(oForm, "WPIT_ITEMCODE", sItemCode)
                        'setSharedData(oForm, "WPIT_ITEMNAME", sItemName)
                        sWPItem = CType(IDHAddOns.idh.addon.Base.PARENT.getGlobalValue("WPITM"), String)
                        bWPReopen = True
                    Else
                        sBPCd = getWFValue(oForm, "wfWPS_CUST") 'getParentSharedDataAsString(oForm, "WPS_CUST")
                        sBPNm = getWFValue(oForm, "wfWPS_CUSTNM") 'getParentSharedDataAsString(oForm, "WPS_CUSTNM")
                        sAddress = getWFValue(oForm, "wfWPS_ADDR") 'getParentSharedDataAsString(oForm, "WPS_ADDR")
                    End If


                    Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("5")

                    If sWPItem IsNot Nothing AndAlso sWPItem = "TRUE" Then
                        If sICode IsNot Nothing AndAlso sIName IsNot Nothing Then

                            'NOT SURE WHY THIS WAS SET IF IT GETS REPLACED BY ANOTHER VALUE WITHOUT A CONDITION
                            '??????????????????????????????
                            oEdit = CType(oForm.Items.Item("7").Specific, SAPbouiCOM.EditText)
                            oEdit.Value = sIName

                            oEdit = CType(oItem.Specific, SAPbouiCOM.EditText)
                            oEdit.Value = sICode

                            'sNewICode = Config.INSTANCE.doGetNextItemCode(sICode, Config.ParameterWithDefault("ITMDPPOS", "_P"))
                            'setItemValue(oForm, "5", sNewICode)

                            '?????????????????????????????

                            oForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

                            goParent.goApplication.ActivateMenuItem("1287")

                            'Un tick the WP Template Checkbox
                            oChk = CType(oForm.Items.Item("IDH_WPTEMP").Specific, SAPbouiCOM.CheckBox)
                            oChk.Checked = False

                            'Untick Approved checkbox
                            oChk = CType(oForm.Items.Item("IDH_APROVE").Specific, SAPbouiCOM.CheckBox)
                            oChk.Checked = False

                            'Selecte Approved user to logged in user
                            oCmb = CType(oForm.Items.Item("IDH_APUSR").Specific, SAPbouiCOM.ComboBox)
                            oCmb.Select(goParent.gsUserName, SAPbouiCOM.BoSearchKey.psk_ByDescription)

                            'Now load chemical grid with OLD Item Code
                            'oForm.PaneLevel = 203
                            doLoadChemicalsGrid(oForm, sICode)
                            doLoadWasteGroupGrid(oForm, sICode)

                            '#MA Start 02-05-2017 - if itemcode is written in config, then itemname should br preeceded by Itemname. If cardcode is written in config, then itemname should be preeceded by cardcode. If neither itemcode nor cardcode is written, then itemname should be preecded by itemcode.
                            If Config.ParameterWithDefault("ITMDPPOS", "_P").ToString().Length > 8 Then
                                If Config.ParameterWithDefault("ITMDPPOS", "_P").ToString().ToUpper().Substring(0, 8) = "ITEMCODE" Then
                                    sNewICode = Config.INSTANCE.doGetNextItemCode(sICode, Config.ParameterWithDefault("ITMDPPOS", "_P").ToString().Substring(8, Config.ParameterWithDefault("ITMDPPOS", "_P").ToString().Length - 8))
                                ElseIf Config.ParameterWithDefault("ITMDPPOS", "_P").ToString().ToUpper().Substring(0, 8) = "CARDCODE" Then
                                    sNewICode = Config.INSTANCE.doGetNextItemCode(getParentSharedData(oForm, "WPS_CUST"), Config.ParameterWithDefault("ITMDPPOS", "_P").ToString().Substring(8, Config.ParameterWithDefault("ITMDPPOS", "_P").ToString().Length - 8))
                                End If
                            ElseIf Config.ParameterWithDefault("ITMDPPOS", "_P").ToString().Length < 8 Then
                            sNewICode = Config.INSTANCE.doGetNextItemCode(sICode, Config.ParameterWithDefault("ITMDPPOS", "_P"))
                            End If
                            '#MA Start 02-05-2017

                            setItemValue(oForm, "5", sNewICode)
                            'oEdit.Value = sNewICode

                            'Dim sWPFlag As String = sBPCd + "#" + sBPNm + "#" + sAddress + "#" + sICode + "#" + sIName + "#" + sWPItem + "#" + "OLD"
                            'setWFValue(oForm, "IDH_WPFLAG", sWPFlag)
                            Dim saWPFlag As String() = {sBPCd, sBPNm, sAddress, sICode, sIName, sWPItem, "OLD", sNewICode, "N", sSupplier}
                            setWFValue(oForm, "IDH_WPFLAG", saWPFlag)

                            doSetFocus(oForm, "7")
                            oItem.Enabled = False
                        ElseIf bWPReopen Then
                            Dim saWPFlag As String() = {sBPCd, sBPNm, sAddress, sICode, sIName, sWPItem, "OLD", sNewICode, "N", sSupplier}
                            setWFValue(oForm, "IDH_WPFLAG", saWPFlag)

                            doSetFocus(oForm, "7")
                            oItem.Enabled = False
                        Else
                            'Dim sWPFlag As String = sBPCd + "#" + sBPNm + "#" + sAddress + "#" + sICode + "#" + sIName + "#" + sWPItem + "#" + "BNEW"
                            'setWFValue(oForm, "IDH_WPFLAG", sWPFlag)
                            Dim saWPFlag As String() = {sBPCd, sBPNm, sAddress, sICode, sIName, sWPItem, "BNEW", sICode, "N", sSupplier}
                            setWFValue(oForm, "IDH_WPFLAG", saWPFlag)

                            'user selected cancel on confirm message so open the form in add mode
                            goParent.goApplication.ActivateMenuItem("1282")

                            oItem.Enabled = True
                        End If
                    End If
                Catch ex As Exception
                End Try
                'End If
            End If
            Console.WriteLine("doPrepareWasteProfileFields --> END ")
            Console.WriteLine("sICode = " + getWFValue(oForm, "wfWPIT_ITEMCODE"))
            Console.WriteLine("sIName = " + getWFValue(oForm, "wfWPIT_ITEMNAME"))
            Console.WriteLine("sWPItem = " + getWFValue(oForm, "wfWPITM"))
        End Sub


        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "CHMGRID" Then
                        doSetChemicalGridLastLine(oForm, pVal)
                    ElseIf pVal.ItemUID = "WGPGRID" Then
                        doSetWasteGroupGridLastLine(oForm, pVal)
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "WGPGRID" Then


                    End If
                Else
                    If pVal.ItemUID = "WGPGRID" Then


                    End If
                End If
            End If

            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function

        'Private Sub doShowHideLinkItem(ByVal oForm As SAPbouiCOM.Form)
        '    Dim sGroup As String
        '    sGroup = getDFValue(oForm, "OITM", "ItmsGrpCod")
        '    If sGroup Is Nothing Then
        '        sGroup = "--"
        '    End If
        '    Dim sWasteGroup As String
        '    sWasteGroup = Config.Parameter( "IGR-WM")

        '    Dim sInvItem As String
        '    sInvItem = getDFValue(oForm, "OITM", "InvntItem")
        '    If Not sWasteGroup Is Nothing AndAlso _
        '        sWasteGroup.Length > 0 AndAlso _
        '        sWasteGroup.Equals(sGroup) AndAlso _
        '        sInvItem = "N" Then
        '        doSetItemPane(oForm, "S_WTITML", 0)
        '        doSetItemPane(oForm, "IDH_WTITML", 0)
        '        doSetItemPane(oForm, "IDH_ITMLUP", 0)
        '    Else
        '        doSetItemPane(oForm, "S_WTITML", 1000)
        '        doSetItemPane(oForm, "IDH_WTITML", 1000)
        '        doSetItemPane(oForm, "IDH_ITMLUP", 1000)

        '        Dim sVal As String
        '        sVal = getDFValue(oForm, "OITM", "U_WTITMLN")
        '        If Not sVal Is Nothing AndAlso sVal.Length > 0 Then
        '            setItemValue(oForm, "U_WTITMLN", "")
        '        End If
        '    End If
        'End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDH_WTUNSR" Then
                    setItemValue(oForm, "IDH_WTCD", getSharedDataAsString(oForm, "IDH_UNCODE"), False)
                    setItemValue(oForm, "IDH_WTCLS", getSharedDataAsString(oForm, "IDH_WTUCL"), False)
                    setItemValue(oForm, "IDH_WTTYP", getSharedDataAsString(oForm, "IDH_WTUMT"), False)
                    setItemValue(oForm, "IDH_WTSTE", getSharedDataAsString(oForm, "IDH_WTUST"), False)
                    setItemValue(oForm, "IDH_WTFLP", getSharedDataAsString(oForm, "IDH_WTUFP"), False)
                    setItemValue(oForm, "IDH_WTDNS", getSharedDataAsString(oForm, "IDH_WTUDE"), False)
                    setItemValue(oForm, "IDH_WTPH", getSharedDataAsString(oForm, "IDH_WTUPH"), False)
                    setItemValue(oForm, "IDH_WTGRP", getSharedDataAsString(oForm, "IDH_WTUPG"), False)
                ElseIf sModalFormType = "IDH_WTECSR" Then
                    'OnTime Ticket 25018 - EWC Code Search Bug
                    'Restructuring the screen with two search fields and two columns in the grid
                    'setItemValue(oForm, "IDH_WTEC", getSharedData(oForm, "IDH_ECCODO"), False)
                    'setItemValue(oForm, "IDH_WTEC", getSharedData(oForm, "IDH_WTWCO"), False)

                    'Making EWC Field as multi-select 
                    Dim sEWCCd As String = getItemValue(oForm, "IDH_WTEC") 'getFormDFValue(oForm, "U_EWC")
                    If sEWCCd IsNot Nothing AndAlso sEWCCd.Length > 0 Then
                        sEWCCd = sEWCCd + "," + getSharedDataAsString(oForm, "IDH_WTWCO")
                    Else
                        sEWCCd = getSharedDataAsString(oForm, "IDH_WTWCO")
                    End If
                    setItemValue(oForm, "IDH_WTEC", sEWCCd, False)
                ElseIf sModalFormType = "IDH_WTHZSR" Then
                    setItemValue(oForm, "IDH_HAZCD", getSharedDataAsString(oForm, "IDH_HZCODO"), False)
                ElseIf sModalFormType = "EPASCR" Then
                    Dim sTrg As String = getSharedDataAsString(oForm, "TRG")
                    setItemValue(oForm, sTrg, getSharedDataAsString(oForm, "IDH_EPACD"), False)
                    'setItemValue(oForm, "LKP_EPACD", getSharedData(oForm, "IDH_EPANM"), False)
                    Select Case sTrg
                        Case "LKP_EPACD1"
                            setItemValue(oForm, "LKP_EPANM1", getSharedDataAsString(oForm, "IDH_EPANM"), False)
                        Case "LKP_EPACD2"
                            setItemValue(oForm, "LKP_EPANM2", getSharedDataAsString(oForm, "IDH_EPANM"), False)
                        Case "LKP_EPACD3"
                            setItemValue(oForm, "LKP_EPANM3", getSharedDataAsString(oForm, "IDH_EPANM"), False)
                        Case "LKP_EPACD4"
                            setItemValue(oForm, "LKP_EPANM4", getSharedDataAsString(oForm, "IDH_EPANM"), False)
                        Case "LKP_EPACD5"
                            setItemValue(oForm, "LKP_EPANM5", getSharedDataAsString(oForm, "IDH_EPANM"), False)
                        Case "LKP_EPACD6"
                            setItemValue(oForm, "LKP_EPANM6", getSharedDataAsString(oForm, "IDH_EPANM"), False)
                    End Select

                ElseIf sModalFormType = "IDHWISRC" Then
                    setItemValue(oForm, "IDH_WTITML", getSharedDataAsString(oForm, "ITEMCODE"))
                    'ElseIf sModalFormType = "IDHSNSRCH" Then
                ElseIf sModalFormType = "SHIPNMGEN" Then
                    Dim sShpNm As String = getSharedDataAsString(oForm, "SHIPNM")
                    Dim sHazCls As String = getSharedDataAsString(oForm, "HAZCLS")
                    Dim sUNNANo As String = getSharedDataAsString(oForm, "UNNANO")
                    Dim sPkGp As String = getSharedDataAsString(oForm, "PKGGRP")

                    If sShpNm IsNot Nothing AndAlso sShpNm.Length > 0 Then
                        setItemValue(oForm, "IDH_SHPNM", getSharedDataAsString(oForm, "SHIPNM"))
                    Else
                        setItemValue(oForm, "IDH_SHPNM", "")
                    End If
                    If sHazCls IsNot Nothing AndAlso sHazCls.Length > 0 Then
                        setItemValue(oForm, "IDH_HAZCLS", sHazCls)
                    Else
                        setItemValue(oForm, "IDH_HAZCLS", "")
                    End If
                    If sUNNANo IsNot Nothing AndAlso sUNNANo.Length > 0 Then
                        setItemValue(oForm, "IDH_UNINA", sUNNANo)
                    Else
                        setItemValue(oForm, "IDH_UNINA", "")
                    End If
                    If sPkGp IsNot Nothing AndAlso sPkGp.Length > 0 Then
                        setItemValue(oForm, "IDH_PKGGRP", sPkGp)
                    Else
                        setItemValue(oForm, "IDH_PKGGRP", "")
                    End If
                ElseIf sModalFormType = "IDHISRC" Then
                    setItemValue(oForm, "IDH_CONTNR", getSharedDataAsString(oForm, "ITEMNAME"))
                ElseIf sModalFormType = "CASTBLSCR" Then
                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "CHMGRID")
                    Dim sChmCd As String = getSharedDataAsString(oForm, "IDH_CHMCD")
                    Dim sChmNm As String = getSharedDataAsString(oForm, "IDH_CHMNM")
                    oGridN.doSetFieldValue("U_ChemicalCd", sChmCd)
                    oGridN.doSetFieldValue("U_ChemicalNm", sChmNm)
                    'KA -- START -- 20121025
                ElseIf sModalFormType = "IDHCSRCH" Then
                    Dim sTrg As String = getSharedDataAsString(oForm, "TRG")
                    If sTrg = "DSPLKP" Then
                        Dim sCrdCd As String = getSharedDataAsString(oForm, "CARDCODE")
                        Dim sCrdNm As String = getSharedDataAsString(oForm, "CARDNAME")
                        setItemValue(oForm, "IDH_DPFACD", sCrdCd)
                        setItemValue(oForm, "IDH_DISPFA", sCrdNm)
                        'Set DF Address
                        Dim oData As ArrayList
                        oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCrdCd, "S")
                        If oData IsNot Nothing AndAlso oData.Count > 0 Then
                            setItemValue(oForm, "IDH_DFADD", CType(oData.Item(0), String))
                        Else
                            setItemValue(oForm, "IDH_DFADD", "")
                        End If
                    End If
                ElseIf sModalFormType = "IDHASRCH" Then
                    Dim sAddres As String = getSharedDataAsString(oForm, "ADDRESS")
                    setItemValue(oForm, "IDH_DFADD", sAddres)
                End If
                'KA -- END -- 20121025
                doClearSharedData(oForm)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Results - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.MenuUID = Config.NAV_FIND Then
                    oForm.Items.Item("163").Click()
                End If
            Else
                'Ontime 32756: WR1_NewDev_Planning_BOOKING IN STOCK - Always Display Linked Item On Waste materials
                'remarking the hide link item. Should be displayed all the time
                'doShowHideLinkItem(oForm)
                Try
                    If pVal.MenuUID = Config.NAV_FIND OrElse
                       pVal.MenuUID = Config.NAV_FIRST OrElse
                       pVal.MenuUID = Config.NAV_LAST OrElse
                       pVal.MenuUID = Config.NAV_NEXT OrElse
                       pVal.MenuUID = Config.NAV_PREV Then
                        doLoadChemicalsGrid(oForm)
                        doLoadWasteGroupGrid(oForm)
                    ElseIf pVal.MenuUID = Config.NAV_ADD Then
                        doLoadChemicalsGrid(oForm)
                        doLoadWasteGroupGrid(oForm)
                        setEnableItem(oForm, False, "IDH_WPTEMP")
                        setEnableItem(oForm, False, "IDH_WPUSR")
                    ElseIf pVal.MenuUID = Config.DUPLICATE Then
                        'Commented - Hayden asked that field greyed out (disables). Commented it so that it should not be greyed out
                        'setEnableItem(oForm, False, "IDH_WPTEMP")
                        setEnableItem(oForm, False, "IDH_WPUSR")
                    End If
                Catch ex As Exception

                End Try
                'Disable controls
                'Dim oItm As SAPbouiCOM.Item = oForm.Items.Item("IDH_DISPFA")
                'oItm.Enabled = False
            End If
            Return True
        End Function

        Public Overrides Sub doClose()
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)

            If miSpaceWidth > 0 Then
                oForm.Width = oForm.Width - miSpaceWidth
            End If
        End Sub

        Private Sub doHeaderFields(ByVal oForm As SAPbouiCOM.Form)
            Dim osItm As SAPbouiCOM.Item
            Dim iLeft As Integer = -1

            '' THE EXSTRA 2 COLUMNS
            '' FIRST = 100
            '' SECOND = 5 + 80 + 5 + 70
            ''THIRD = 5 + 120
            ''TOTAL = 100 | 5 + | 80 + 5 + 70 | +  5 + | 170 |
            'Adding controls to header
            If IDHAddOns.idh.addon.Base.LAYOUTTYPE = FixedValues.SBOLayout2009PL5 Then
                iLeft = 510 'oForm.Width - 200 - 100 - 5
                'osItm = oForm.Items.Item(msPurchaseCheckBox)
                'iLeft = osItm.Left - 200

                'If iLeft < 0 Then
                '    osItm = oForm.Items.Item(msSalesCheckBox)
                '    iLeft = osItm.Left - 200

                '    If iLeft < 0 Then
                '        osItm = oForm.Items.Item(msPurchaseCheckBox)
                '        iLeft = osItm.Left - 200
                '    End If
                'End If

                'If iLeft > 0 Then
                osItm = oForm.Items.Item(msPurchaseCheckBox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                osItm = oForm.Items.Item(msSalesCheckBox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                osItm = oForm.Items.Item(msInventoryCheckBox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                'End If

                iLeft = iLeft + 100 + 5
            Else
                iLeft = 510

                osItm = oForm.Items.Item(msAssitCheckbox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                osItm = oForm.Items.Item(msPurchaseCheckBox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                osItm = oForm.Items.Item(msSalesCheckBox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                osItm = oForm.Items.Item(msInventoryCheckBox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                iLeft = iLeft + 100 + 5
                'iLeft = osItm.Left + 100 + 5
            End If

            ''osItm = oForm.Items.Item("14")

            'iLeft = osItm.Left + 100 + 5 'osItm.Width  'To move it on second column
            doAddCheck(oForm, "IDH_WPTEMP", 0, iLeft, osItm.Top, 80, osItm.Height, getTranslatedWord("WP Template"), msHeaderLinkTo)
            doAddCombo(oForm, "IDH_WPUSR", 0, iLeft + 85, osItm.Top, 70, osItm.Height, msHeaderLinkTo)
            doFillCombo(oForm, "IDH_WPUSR", "OUSR", "User_Code", "U_Name") ', "", "", "", goParent.gsUserName)
            Dim sUsers As String = Config.INSTANCE.getParameterWithDefault("HIDWPTMP", "")
            If sUsers.Trim <> "" Then
                Dim aUsers() As String = (sUsers.ToUpper.Replace(" ", "")).Split(",")
                If Not aUsers.Contains(DataHandler.INSTANCE.User.ToUpper) Then
                    'setEnableItem(oForm, False, "IDH_WPTEMP")
                    'setEnableItem(oForm, False, "IDH_WPUSR")
                    Dim oitem As SAPbouiCOM.Item = oForm.Items.Item("IDH_WPTEMP")
                    oitem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, _
                            SAPbouiCOM.BoFormMode.fm_ADD_MODE + SAPbouiCOM.BoFormMode.fm_EDIT_MODE + _
                            SAPbouiCOM.BoFormMode.fm_OK_MODE + SAPbouiCOM.BoFormMode.fm_PRINT_MODE + _
                            SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + SAPbouiCOM.BoFormMode.fm_VIEW_MODE + SAPbouiCOM.BoFormMode.fm_FIND_MODE, _
                            SAPbouiCOM.BoModeVisualBehavior.mvb_False)

                    'oitem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, _
                    '       SAPbouiCOM.BoFormMode.fm_OK_MODE + _
                    '       SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + SAPbouiCOM.BoFormMode.fm_VIEW_MODE + SAPbouiCOM.BoFormMode.fm_FIND_MODE, _
                    '       SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                    'oitem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, _
                    '    SAPbouiCOM.BoFormMode.fm_ADD_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

                    oitem = oForm.Items.Item("IDH_WPUSR")

                    oitem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, _
                            SAPbouiCOM.BoFormMode.fm_ADD_MODE + SAPbouiCOM.BoFormMode.fm_EDIT_MODE + _
                            SAPbouiCOM.BoFormMode.fm_OK_MODE + SAPbouiCOM.BoFormMode.fm_PRINT_MODE + _
                            SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + SAPbouiCOM.BoFormMode.fm_VIEW_MODE + SAPbouiCOM.BoFormMode.fm_FIND_MODE, _
                            SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                    'oitem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, _
                    '       SAPbouiCOM.BoFormMode.fm_ADD_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                    'oitem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, _
                    '       SAPbouiCOM.BoFormMode.fm_EDIT_MODE + _
                    '       SAPbouiCOM.BoFormMode.fm_OK_MODE + SAPbouiCOM.BoFormMode.fm_PRINT_MODE + _
                    '       SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + SAPbouiCOM.BoFormMode.fm_VIEW_MODE + SAPbouiCOM.BoFormMode.fm_FIND_MODE, _
                    '       SAPbouiCOM.BoModeVisualBehavior.mvb_True)

                End If
                'For i As Int16 = 0 To aUsers.Length - 1
                '    If DataHandler.INSTANCE.User.ToLower().Equals(aUsers(i).Trim.ToLower) Then
                '    End If
                'Next
            End If
            doAddCheck(oForm, "IDH_APROVE", 0, iLeft, osItm.Top + (osItm.Height * 1) + 2, 80, osItm.Height, getTranslatedWord("Approved"), msHeaderLinkTo)
            doAddCombo(oForm, "IDH_APUSR", 0, iLeft + 85, osItm.Top + (osItm.Height * 1) + 2, 70, osItm.Height, msHeaderLinkTo)
            doFillCombo(oForm, "IDH_APUSR", "OUSR", "User_Code", "U_Name") ', "", "", "", goParent.gsUserName)

            doAddCheck(oForm, "IDH_LETOFI", 0, iLeft, osItm.Top + (osItm.Height * 2) + 4, 80, osItm.Height, getTranslatedWord("Letter on File"), msHeaderLinkTo)
            doAddCombo(oForm, "IDH_LTUSR", 0, iLeft + 85, osItm.Top + (osItm.Height * 2) + 4, 70, osItm.Height, msHeaderLinkTo)
            doFillCombo(oForm, "IDH_LTUSR", "OUSR", "User_Code", "U_Name") ', "", "", "", goParent.gsUserName)

            doAddCheck(oForm, "IDH_HAZORD", 0, iLeft, osItm.Top + (osItm.Height * 3) + 6, 80, osItm.Height, getTranslatedWord("Hazardous"), msHeaderLinkTo)
            doAddCheck(oForm, "IDH_SMPREQ", 0, iLeft, osItm.Top + (osItm.Height * 3) + 21, 100, osItm.Height, getTranslatedWord("Sample Required"), msHeaderLinkTo)

            iLeft = iLeft + 80 + 5 + 70 + 5 'To move it on third column
            doAddCheck(oForm, "IDHCWBB", 0, iLeft, osItm.Top, 170, osItm.Height, getTranslatedWord("Purchase Over Weighbridge"), msHeaderLinkTo)
            doAddCheck(oForm, "IDH_REB", 0, iLeft, osItm.Top + (osItm.Height * 1) + 2, 170, osItm.Height, getTranslatedWord("Rebate Item"), msHeaderLinkTo)
            doAddCheck(oForm, "IDH_CREB", 0, iLeft, osItm.Top + (osItm.Height * 2) + 4, 170, osItm.Height, getTranslatedWord("Carrier Rebate Item"), msHeaderLinkTo)

        End Sub

        'OnTime [#Ico000????] USA
        'START
        'Create Waste Profile Tab and fields
        Private Sub doCreateWasteProfile(ByVal oForm As SAPbouiCOM.Form)
            Dim oItm As SAPbouiCOM.Item
            'Dim osItm As SAPbouiCOM.Item
            Dim oFldr As SAPbouiCOM.Folder
            Dim iTop As Integer = -1
            Dim iWidth As Integer = -1
            Dim iHeight As Integer = -1
            Dim iFrame As Integer = 0

            'osItm = oForm.Items.Item("IDH_UN01")

            'oItm = oForm.Items.Add("IDH_WPTAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            'oItm.Left = osItm.Left + osItm.Width
            'oItm.Top = osItm.Top
            'oItm.Width = osItm.Width
            'oItm.Height = osItm.Height
            ''oItm.Visible = True
            'oItm.AffectsFormMode = False
            'oFldr = oItm.Specific
            'oFldr.Caption = getTranslatedWord("Waste Profile")
            'oFldr.GroupWith("9")

            'osItm = oForm.Items.Item("103")
            'osItm.Left = osItm.Left + oItm.Width

            'osItm = oForm.Items.Item("100")
            'osItm.Width = osItm.Width + oItm.Width

            'osItm = oForm.Items.Item("303")
            'osItm.Width = osItm.Width + oItm.Width

            'Get WRConfig key WPTABVIS to set Waste Profile tabs Visible T/F  (LWG,PHC,WCM,MET,RPT,DOT,WEE,WGP) 
            Dim sWPTABVIS As String = Config.ParameterWithDefault("WPTABVIS", "1,1,1,1,1,1,1,1")
            Dim sFlagArray() As String = sWPTABVIS.Split(","c)


            'Placing Inner Tabs
            oForm.DataSources.UserDataSources.Add("Fldr6DSKA", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
            '**Location of Waste Generation
            oItm = oForm.Items.Add("IDH_LWGTAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItm.Left = 20
            oItm.Width = 200
            oItm.Top = 160
            oItm.Height = 20
            'Set Visible based on WRConfig Key 
            oItm.Visible = If(sFlagArray(0).ToString() = "1", True, False)

            oItm.FromPane = 201
            oItm.ToPane = 299
            oItm.AffectsFormMode = False
            oFldr = CType(oItm.Specific, SAPbouiCOM.Folder)
            oFldr.Caption = getTranslatedWord("Location of Waste Generation")
            oFldr.DataBind.SetBound(True, "", "Fldr6DSKA")
            oFldr.Select()


            'Physical Characteristics
            oItm = oForm.Items.Add("IDH_PHCTAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItm.Left = 20 + 100
            oItm.Width = 200
            oItm.Top = 160
            oItm.Height = 20
            'Set Visible based on WRConfig Key 
            oItm.Visible = If(sFlagArray(1).ToString() = "1", True, False)

            oItm.FromPane = 201
            oItm.ToPane = 299
            oItm.AffectsFormMode = False
            oFldr = CType(oItm.Specific, SAPbouiCOM.Folder)
            oFldr.Caption = getTranslatedWord("Physical Characteristics")
            oFldr.DataBind.SetBound(True, "", "Fldr6DSKA")
            oFldr.GroupWith("IDH_LWGTAB")

            'Waste Composition
            oItm = oForm.Items.Add("IDH_WCMTAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItm.Left = 20 + 200
            oItm.Width = 200
            oItm.Top = 160
            oItm.Height = 20
            'Set Visible based on WRConfig Key 
            oItm.Visible = If(sFlagArray(2).ToString() = "1", True, False)

            oItm.FromPane = 201
            oItm.ToPane = 299
            oItm.AffectsFormMode = False
            oFldr = CType(oItm.Specific, SAPbouiCOM.Folder)
            oFldr.Caption = getTranslatedWord("Waste Composition")
            oFldr.DataBind.SetBound(True, "", "Fldr6DSKA")
            oFldr.GroupWith("IDH_PHCTAB")

            'Metals
            oItm = oForm.Items.Add("IDH_METTAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItm.Left = 20 + 300
            oItm.Width = 200
            oItm.Top = 160
            oItm.Height = 20
            'Set Visible based on WRConfig Key 
            oItm.Visible = If(sFlagArray(3).ToString() = "1", True, False)

            oItm.FromPane = 201
            oItm.ToPane = 299
            oItm.AffectsFormMode = False
            oFldr = CType(oItm.Specific, SAPbouiCOM.Folder)
            oFldr.Caption = getTranslatedWord("Metals")
            oFldr.DataBind.SetBound(True, "", "Fldr6DSKA")
            oFldr.GroupWith("IDH_WCMTAB")

            'Reporting Code
            oItm = oForm.Items.Add("IDH_RPTTAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItm.Left = 20 + 400
            oItm.Width = 200
            oItm.Top = 160
            oItm.Height = 20
            'Set Visible based on WRConfig Key 
            oItm.Visible = If(sFlagArray(4).ToString() = "1", True, False)

            oItm.FromPane = 201
            oItm.ToPane = 299
            oItm.AffectsFormMode = False
            oFldr = CType(oItm.Specific, SAPbouiCOM.Folder)
            oFldr.Caption = getTranslatedWord("Reporting Codes")
            oFldr.DataBind.SetBound(True, "", "Fldr6DSKA")
            oFldr.GroupWith("IDH_METTAB")

            'DOT / RCRA
            oItm = oForm.Items.Add("IDH_DOTTAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItm.Left = 20 + 500
            oItm.Width = 200
            oItm.Top = 160
            oItm.Height = 20
            'Set Visible based on WRConfig Key 
            oItm.Visible = If(sFlagArray(5).ToString() = "1", True, False)

            oItm.FromPane = 201
            oItm.ToPane = 299
            oItm.AffectsFormMode = False
            oFldr = CType(oItm.Specific, SAPbouiCOM.Folder)
            oFldr.Caption = getTranslatedWord("DOT RCRA")
            oFldr.DataBind.SetBound(True, "", "Fldr6DSKA")
            oFldr.GroupWith("IDH_RPTTAB")

            'WEEE
            oItm = oForm.Items.Add("IDH_WEETAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItm.Left = 20 + 600
            oItm.Width = 200
            oItm.Top = 160
            oItm.Height = 20
            'Set Visible based on WRConfig Key 
            oItm.Visible = If(sFlagArray(6).ToString() = "1", True, False)

            oItm.FromPane = 201
            oItm.ToPane = 299
            oItm.AffectsFormMode = False
            oFldr = CType(oItm.Specific, SAPbouiCOM.Folder)
            oFldr.Caption = getTranslatedWord("WEEE")
            oFldr.DataBind.SetBound(True, "", "Fldr6DSKA")
            oFldr.GroupWith("IDH_DOTTAB")

            ''Waste Group
            oItm = oForm.Items.Add("IDH_WGPTAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItm.Left = 20 + 700
            oItm.Width = 100
            oItm.Top = 160
            oItm.Height = 20
            'Set Visible based on WRConfig Key 
            oItm.Visible = If(sFlagArray(7).ToString() = "1", True, False)

            oItm.FromPane = 201
            oItm.ToPane = 299
            oItm.AffectsFormMode = False
            oFldr = CType(oItm.Specific, SAPbouiCOM.Folder)
            oFldr.Caption = getTranslatedWord("Waste Group")
            oFldr.DataBind.SetBound(True, "", "Fldr6DSKA")
            oFldr.GroupWith("IDH_WEETAB")

            ''Misc.
            'oItm = oForm.Items.Add("IDH_MISTAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            'oItm.Left = 20 + 700
            'oItm.Width = 100
            'oItm.Top = 160
            'oItm.Height = 20
            'oItm.FromPane = 201
            'oItm.ToPane = 299
            'oItm.Visible = False
            'oItm.AffectsFormMode = False
            'oFldr = oItm.Specific
            'oFldr.Caption = getTranslatedWord("Misc")
            'oFldr.DataBind.SetBound(True, "", "Fldr6DSKA")
            'oFldr.GroupWith("IDH_WGPTAB")

            'Add inner rectangle
            oItm = oForm.Items.Add("INRECT", SAPbouiCOM.BoFormItemTypes.it_RECTANGLE)
            oItm.Left = 20
            'oItm.Width = 840
            oItm.Width = oForm.Width - miWasteProfileRecRightSpace
            oItm.Top = 180
            oItm.Height = 270
            oItm.FromPane = 201
            oItm.ToPane = 299

            'Adding Controls on the General Tab
            oItm = oForm.Items.Item("180")
            Dim chk As SAPbouiCOM.CheckBox
            chk = CType(oItm.Specific, SAPbouiCOM.CheckBox)
            doAddCheck(oForm, "IDH_WTUDW", oItm.FromPane, oItm.Left, oItm.Top + 20, 200, -1, getTranslatedWord("Use Default Weight"), msWasteGroupLinkTo)

            doAddEdit(oForm, "IDH_ALW", oItm.FromPane, oItm.Left + 150 + 5, oItm.Top + 35, 120, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SALW", oItm.FromPane, oItm.Left, oItm.Top + 35, 150, -1, getTranslatedWord("Average Load Weight"), "IDH_ALW")

            doAddEdit(oForm, "IDH_WTDFW", oItm.FromPane, oItm.Left + 150 + 5, oItm.Top + 50, 120, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SWTDFW", oItm.FromPane, oItm.Left, oItm.Top + 50, 150, -1, getTranslatedWord("Default Weight"), "IDH_WTDFW")


            'KA -- START -- 20121022
            '** Pane 201 (IDH_LWGTAB)
            'doAddEdit(oForm, "IDH_DISPFA", 201, 155, 200, 140, -1)
            'doAddEdit(oForm, "IDH_DPFACD", 201, 320, 200, 140, -1) 'Added later to store Disposal Facility Code
            'doAddLookupButton(oForm, "DSPLKP", 201, 295, 200, "IDH_DISPFA")

            doAddEdit(oForm, "IDH_DPFACD", 201, 185, 200, 140, -1, msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_DISPFA", 201, 330, 200, 140, -1, msWasteGroupLinkTo).Enabled = False 'Added later to store Disposal Facility Code
            doAddLookupButton(oForm, "DSPLKP", 201, 470, 200, "IDH_DISPFA")

            doAddStatic(oForm, "WP1", 201, 30, 200, 150, -1, getTranslatedWord("Disposal Facility Code/Name"), "IDH_DISPFA")
            'KA -- END -- 20121022

            'KA -- START -- 20121025
            'Add Disposal Facility Address also
            doAddEdit(oForm, "IDH_DFADD", 201, 655, 200, 140, -1, msWasteGroupLinkTo)
            doAddLookupButton(oForm, "DFALKP", 201, 795, 200, "IDH_DFADD")
            doAddStatic(oForm, "DF1", 201, 500, 200, 150, -1, getTranslatedWord("Facility Address"), "IDH_DFADD")
            'KA -- END -- 20121025

            doAddOption(oForm, "IDH_OPPOY", 201, 285, 220, 40, -1, "Yes", "A", "ds_1", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_OPPON", 201, 325, 220, 40, -1, "No", "B", "", "IDH_OPPOY", msWasteGroupLinkTo)
            doAddStatic(oForm, "WP2", 201, 30, 220, 250, -1, getTranslatedWord("1. Material poisonous by inhalation?"), "IDH_OPPOY")

            doAddOption(oForm, "IDH_OPVDY", 201, 285, 240, 40, -1, "Yes", "A", "ds_2", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_OPVDN", 201, 325, 240, 40, -1, "No", "B", "", "IDH_OPVDY", msWasteGroupLinkTo)
            doAddStatic(oForm, "WP3", 201, 30, 240, 250, -1, getTranslatedWord("2. Is this waste stored in vented drums?"), "IDH_OPVDY")

            doAddOption(oForm, "IDH_OPWPY", 201, 285, 260, 40, -1, "Yes", "A", "ds_3", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_OPWPN", 201, 325, 260, 40, -1, "No", "B", "", "IDH_OPWPY", msWasteGroupLinkTo)
            doAddStatic(oForm, "WP4", 201, 30, 260, 250, -1, getTranslatedWord("3. Is this waste pumpable?"), "IDH_OPWPY")

            doAddOption(oForm, "IDH_OPPOLY", 201, 285, 280, 40, -1, "Yes", "A", "ds_4", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_OPPOLN", 201, 325, 280, 40, -1, "No", "B", "", "IDH_OPPOLY", msWasteGroupLinkTo)
            doAddStatic(oForm, "WP5", 201, 30, 280, 250, -1, getTranslatedWord("4. Is the waste polymerizable?"), "IDH_OPPOLY")

            'doAddOption2(oForm, "IDH_OPNEY", 201, 535, 300, 40, -1, "Yes", "A", "B", "OITM", "U_IsBenzeneWO", "", msWasteGroupLinkTo)
            'doAddOption2(oForm, "IDH_OPNEN", 201, 575, 300, 40, -1, "No", "B", "A", "", "", "IDH_OPNEY", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_OPNEY", 201, 535, 300, 40, -1, "Yes", "A", "ds_5", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_OPNEN", 201, 575, 300, 40, -1, "No", "B", "", "IDH_OPNEY", msWasteGroupLinkTo)
            doAddStatic(oForm, "WP6", 201, 30, 300, 500, -1, getTranslatedWord("5. Is waste subject to the National Emission Standards for Benzene Waste Operations (40 CFR 61 Subpart FF)?"), "IDH_OPNEY")

            doAddOption(oForm, "IDH_OPCPRY", 201, 535, 320, 40, -1, "Yes", "A", "ds_6", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_OPCPRN", 201, 575, 320, 40, -1, "No", "B", "", "IDH_OPCPRY", msWasteGroupLinkTo)
            doAddStatic(oForm, "WP8", 201, 30, 320, 500, -1, getTranslatedWord("6. Is this waste regulated as an ozone depleting substance (40 CPR part 82)?"), "IDH_OPCPRY")

            doAddOption(oForm, "IDH_OPMSZY", 201, 535, 340, 40, -1, "Yes", "A", "ds_7", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_OPMSZN", 201, 575, 340, 40, -1, "No", "B", "", "IDH_OPMSZY", msWasteGroupLinkTo)
            doAddStatic(oForm, "WP10", 201, 30, 340, 500, -1, getTranslatedWord("7. Does this waste contain scrap metal pieces greate than 2 inches in size?"), "IDH_OPMSZY")

            doAddOption(oForm, "IDH_OPCFRY", 201, 535, 360, 40, -1, "Yes", "A", "ds_8", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_OPCFRN", 201, 575, 360, 40, -1, "No", "B", "", "IDH_OPCFRY", msWasteGroupLinkTo)
            doAddStatic(oForm, "WP12", 201, 30, 360, 500, -1, getTranslatedWord("8. Is this waste regulated as a marine pollutant 49 CFR 171.8?"), "IDH_OPCFRY")

            '** Pane 202 (IDH_PHCTAB)
            'doAddStatic(oForm, "SBASEL2", 202, 25, 185, 120, -1, "Basel Code2", "IDH_BASEL")
            doAddEdit(oForm, "IDH_SPEHAN", 202, 175, 200, 140, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SPEHAN1", 202, 30, 200, 140, -1, getTranslatedWord("Special Handling Instructions"), "IDH_SPEHAN")

            doAddEdit(oForm, "IDH_COLOR", 202, 175, 220, 140, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "COLOR1", 202, 30, 220, 140, -1, getTranslatedWord("Color"), "IDH_COLOR")

            doAddOption(oForm, "IDH_OPODY", 202, 175, 239, 40, -1, "Yes", "A", "OPODY", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_OPODN", 202, 215, 239, 40, -1, "No", "B", "", "IDH_OPODY", msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_ODOR", 202, 305, 240, 140, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "ODOR1", 202, 260, 240, 40, -1, getTranslatedWord("Describe"), msWasteGroupLinkTo)
            doAddStatic(oForm, "ODOR2", 202, 30, 240, 140, -1, getTranslatedWord("Odor"), "IDH_ODOR")

            doAddOption(oForm, "IDH_OPODS", 202, 175, 259, 80, -1, "Single Phase", "A", "OPODS", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_OPODB", 202, 255, 259, 80, -1, "Bi-Layered", "B", "", "IDH_OPODS", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_OPODM", 202, 335, 259, 80, -1, "Multi-Layered", "C", "", "IDH_OPODS", msWasteGroupLinkTo)
            doAddStatic(oForm, "OPODS1", 202, 30, 260, 140, -1, getTranslatedWord("Layers"), "IDH_OPODS")

            doAddEdit(oForm, "IDH_SPGRAV", 202, 175, 280, 140, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SPGRAV1", 202, 30, 280, 140, -1, getTranslatedWord("Specific Gravity (Water=1.00)"), "IDH_SPGRAV")
            doAddEdit(oForm, "IDH_GRVRNG", 202, 365, 280, 140, -1, "IDH_OPODM")
            doAddStatic(oForm, "SPGRAV2", 202, 320, 280, 40, -1, getTranslatedWord("Range"), "IDH_GRVRNG")

            doAddOption(oForm, "IDH_PHYSS", 202, 175, 299, 80, -1, "Solid", "A", "PHYSS", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_PHYSL", 202, 255, 299, 80, -1, "Liquid", "B", "", "IDH_PHYSS", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_PHYSSS", 202, 335, 299, 80, -1, "Semi-Solid", "C", "", "IDH_PHYSL", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_PHYSO", 202, 415, 299, 80, -1, "Other", "D", "", "IDH_PHYSSS", msWasteGroupLinkTo)
            doAddStatic(oForm, "PSOTHR1", 202, 500, 300, 40, -1, getTranslatedWord("Describe"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_PSOTHR", 202, 545, 300, 140, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "PSOTHR2", 202, 30, 300, 140, -1, getTranslatedWord("Physical state at 70F"), "IDH_PSOTHR")

            doAddOption(oForm, "IDH_VISL", 202, 175, 319, 80, -1, "Low", "A", "VISL", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_VISM", 202, 255, 319, 80, -1, "Medium", "B", "", "IDH_VISL", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_VISH", 202, 335, 319, 80, -1, "High", "C", "", "IDH_VISM", msWasteGroupLinkTo)
            doAddStatic(oForm, "BTULB1", 202, 415, 320, 40, -1, getTranslatedWord("BTU/LB"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_BTULB", 202, 460, 320, 140, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "BTULB2", 202, 30, 320, 140, -1, getTranslatedWord("Viscosity"), "IDH_BTULB")

            doAddOption(oForm, "IDH_PHLVL0", 202, 175, 339, 80, -1, "<= 2", "A", "PHLVL0", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_PHLVL2", 202, 255, 339, 80, -1, "2 thru 6", "B", "", "IDH_PHLVL0", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_PHLVL6", 202, 335, 339, 80, -1, "6 thru 8", "C", "", "IDH_PHLVL0", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_PHLVL8", 202, 415, 339, 80, -1, "8 thru 12.5", "D", "", "IDH_PHLVL0", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_PHLV12", 202, 495, 339, 80, -1, ">= 12.5", "E", "", "IDH_PHLVL0", msWasteGroupLinkTo)
            doAddStatic(oForm, "PH1", 202, 580, 340, 60, -1, getTranslatedWord("Or Exactly"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_PHEXCT", 202, 645, 340, 140, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "PH2", 202, 30, 340, 140, -1, getTranslatedWord("pH"), "IDH_PHEXCT")

            doAddOption(oForm, "IDH_FLPN", 202, 175, 359, 80, -1, "None", "A", "FLPN", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_FLP140", 202, 255, 359, 80, -1, "< 140 F", "B", "", "IDH_FLPN", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_FLP199", 202, 335, 359, 80, -1, "140-199 F", "C", "", "IDH_FLPN", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_FLP200", 202, 415, 359, 80, -1, "> 200 F", "D", "", "IDH_FLPN", msWasteGroupLinkTo)
            doAddStatic(oForm, "FLPN1", 202, 500, 360, 60, -1, getTranslatedWord("Or Exactly"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_FLPNEX", 202, 560, 360, 140, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "FLPN2", 202, 30, 360, 140, -1, getTranslatedWord("Flash Point"), "IDH_FLPNEX")

            doAddOption(oForm, "IDH_FRQ1", 202, 175, 379, 80, -1, "One Time", "A", "FRQ1", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_FRQM", 202, 255, 379, 80, -1, "Monthly", "B", "", "IDH_FRQ1", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_FRQA", 202, 335, 379, 80, -1, "Annually", "C", "", "IDH_FRQ1", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_FRQO", 202, 415, 379, 80, -1, "Other", "D", "", "IDH_FRQ1", msWasteGroupLinkTo)

            doAddStatic(oForm, "FRQOTHDS1", 202, 500, 380, 60, -1, getTranslatedWord("Describe"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_FRQODS", 202, 560, 380, 140, -1, msWasteGroupLinkTo)

            doAddStatic(oForm, "FRQNCY1", 202, 705, 380, 60, -1, getTranslatedWord("Volume"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_FRQVOL", 202, 765, 380, 50, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "FRQNCY2", 202, 30, 380, 140, -1, getTranslatedWord("Frequency"), "IDH_FRQVOL")

            doAddEdit(oForm, "IDH_CONTNR", 202, 175, 400, 140, -1, msWasteGroupLinkTo)
            doAddLookupButton(oForm, "CNTLKP", 202, 315, 400, "IDH_CONTNR")
            doAddStatic(oForm, "CONTNR1", 202, 30, 400, 140, -1, getTranslatedWord("Containers"), "IDH_CONTNR")

            'Pane 203 Waste Composition
            doAddStatic(oForm, "CONCEN1", 203, 130, 190, 90, -1, getTranslatedWord("Concentrations"), msWasteGroupLinkTo)

            doAddCheck(oForm, "IDH_CHKLIQ", 203, 105, 209, 20, -1, "")
            doAddEdit(oForm, "IDH_LIQPER", 203, 130, 210, 40, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "LIQUID1", 203, 175, 210, 20, -1, "%", msWasteGroupLinkTo)
            doAddStatic(oForm, "LIQUID2", 203, 30, 210, 70, -1, getTranslatedWord("Liquids"), "IDH_LIQPER")

            doAddCheck(oForm, "IDH_CHKSOL", 203, 105, 229, 20, -1, "")
            doAddEdit(oForm, "IDH_SOLPER", 203, 130, 230, 40, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SOLID1", 203, 175, 230, 20, -1, "%", msWasteGroupLinkTo)
            doAddStatic(oForm, "SOLID2", 203, 30, 230, 70, -1, getTranslatedWord("Solids"), "IDH_SOLPER")

            doAddCheck(oForm, "IDH_CHKSLU", 203, 105, 249, 20, -1, "")
            doAddEdit(oForm, "IDH_SLUPER", 203, 130, 250, 40, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SLUDGE1", 203, 175, 250, 20, -1, "%", msWasteGroupLinkTo)
            doAddStatic(oForm, "SLUDGE2", 203, 30, 250, 70, -1, getTranslatedWord("Sludges"), "IDH_SLUPER")

            doAddCheck(oForm, "IDH_CHKGAS", 203, 105, 269, 20, -1, "")
            doAddEdit(oForm, "IDH_GASPER", 203, 130, 270, 40, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "GAS1", 203, 175, 270, 20, -1, "%", msWasteGroupLinkTo)
            doAddStatic(oForm, "GAS2", 203, 30, 270, 70, -1, getTranslatedWord("Gas"), "IDH_GASPER")

            doAddEdit(oForm, "IDH_TOTPER", 203, 130, 290, 40, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "TOT1", 203, 175, 290, 20, -1, "%", msWasteGroupLinkTo)
            doAddStatic(oForm, "TOT2", 203, 30, 290, 100, -1, getTranslatedWord("Total Should = 100%"), "IDH_TOTPER")

            doAddStatic(oForm, "CONTENT1", 203, 130, 310, 90, -1, getTranslatedWord("Check All Contents"), msWasteGroupLinkTo)

            doAddCheck(oForm, "IDH_CHKPCB", 203, 105, 329, 20, -1, "", msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_PCBPER", 203, 130, 330, 40, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "PCB1", 203, 30, 330, 70, -1, getTranslatedWord("PCB's"), "IDH_PCBPER")

            doAddCheck(oForm, "IDH_CHKCYN", 203, 105, 349, 20, -1, "", msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_CYNPER", 203, 130, 350, 40, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "CYN1", 203, 30, 350, 70, -1, getTranslatedWord("Cyanides"), "IDH_CYNPER")

            doAddCheck(oForm, "IDH_CHKSUL", 203, 105, 369, 20, -1, "", msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_SULPER", 203, 130, 370, 40, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SUL1", 203, 30, 370, 70, -1, getTranslatedWord("Sulfides"), "IDH_SULPER")

            doAddCheck(oForm, "IDH_CHKPHE", 203, 105, 389, 20, -1, "", msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_PHEPER", 203, 130, 390, 40, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "PHE1", 203, 30, 390, 70, -1, getTranslatedWord("Phenolics"), "IDH_PHEPER")

            doAddCheck(oForm, "IDH_CHKHAL", 203, 105, 409, 20, -1, "", msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_HALPER", 203, 130, 410, 40, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "HAL1", 203, 30, 410, 70, -1, getTranslatedWord("Halogen"), "IDH_HALPER")

            doAddCheck(oForm, "IDH_CHKDIO", 203, 105, 429, 20, -1, "", msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_DIOPER", 203, 130, 430, 40, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "DIO1", 203, 30, 430, 70, -1, getTranslatedWord("Dioxins"), "IDH_DIOPER")

            doAddOption(oForm, "IDH_ADINFY", 203, 455, 190, 40, -1, "Yes", "A", "ADINF1", "")
            doAddOption(oForm, "IDH_ADINFN", 203, 495, 190, 40, -1, "No", "B", "", "IDH_ADINFY", msWasteGroupLinkTo)
            doAddStatic(oForm, "ADINFO1", 203, 250, 190, 200, -1, getTranslatedWord("Additional Information (MSDS, TCLP, Etc)?"), "IDH_ADINF1")

            doAddStatic(oForm, "WPCOMM1", 203, 250, 210, 90, -1, getTranslatedWord("Comments"), msWasteGroupLinkTo)
            'Dim oExtTxt As SAPbouiCOM.EditText
            oItm = oForm.Items.Add("IDH_WPCOM", SAPbouiCOM.BoFormItemTypes.it_EXTEDIT)
            oItm.Left = 250
            oItm.Top = 230
            oItm.Width = 600
            oItm.Height = 60
            oItm.FromPane = 203
            oItm.ToPane = 203
            oItm.LinkTo = msWasteGroupLinkTo

            doAddStatic(oForm, "WPCHEMI1", 203, 250, 295, 90, -1, getTranslatedWord("Chemicals"), msWasteGroupLinkTo)
            'Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "CHEMGRID")
            'If oGridN Is Nothing Then
            '    Dim owItem As SAPbouiCOM.Item = oForm.Items.Item("21")
            '    oGridN = New UpdateGrid(Me, oForm, "CHEMGRID", 250, 315, 600, 130)
            '    oGridN.getSBOItem.FromPane = 203
            '    oGridN.getSBOItem.ToPane = 203
            'End If

            'Add table to grid
            'doSetFilterFields(oGridN)
            'doSetListFields(oGridN)
            'oGridN.doAddGridTable(New GridTable("@ISB_CASTBL", Nothing, "Code", True), True)

            'oGridN.doSetDoCount(True)
            'oGridN.doSetHistory("@ISB_CASTBL", "Code")

            'idh.controls.grid.TFSMovements.doAddToForm(Me, oForm, "MOVEGRID", 20, 155, 800, 350, 300, 399)
            'idh.controls.grid.WPChemicals.doAddToForm(Me, oForm, "CHMGRID", 250, 315, 600, 130, 203, 203)


            '** Pane 204 Metals
            doAddOption(oForm, "IDH_METN", 204, 30, 190, 100, -1, "None", "A", "METN1", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_METT", 204, 135, 190, 100, -1, "TCLP (MG/L)", "B", "", "IDH_METN", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_METTOT", 204, 240, 190, 100, -1, "Total (PPM)", "C", "", "IDH_METN", msWasteGroupLinkTo)

            doAddStatic(oForm, "CONCEN3", 204, 120, 210, 90, -1, getTranslatedWord("Concentration"), msWasteGroupLinkTo)
            doAddStatic(oForm, "CONCEN4", 204, 390, 210, 90, -1, getTranslatedWord("Concentration"), msWasteGroupLinkTo)

            doAddCheck(oForm, "IDH_CHKARS", 204, 30, 230, 80, -1, getTranslatedWord("Arsenic"), "IDH_ARSPER")
            doAddEdit(oForm, "IDH_ARSPER", 204, 120, 230, 40, -1, msWasteGroupLinkTo)
            doAddCheck(oForm, "IDH_CHKMER", 204, 300, 230, 80, -1, getTranslatedWord("Mercury"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_MERPER", 204, 390, 230, 40, -1, "IDH_CHKMER")

            doAddCheck(oForm, "IDH_CHKBAR", 204, 30, 250, 80, -1, getTranslatedWord("Barium"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_BARPER", 204, 120, 250, 40, -1, "IDH_CHKBAR")
            doAddCheck(oForm, "IDH_CHKNIK", 204, 300, 250, 80, -1, getTranslatedWord("Nickel"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_NIKPER", 204, 390, 250, 40, -1, "IDH_CHKNIK")

            doAddCheck(oForm, "IDH_CHKCAD", 204, 30, 270, 80, -1, getTranslatedWord("Cadmium"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_CADPER", 204, 120, 270, 40, -1, "IDH_CHKCAD")
            doAddCheck(oForm, "IDH_CHKSEL", 204, 300, 270, 80, -1, getTranslatedWord("Selenium"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_SELPER", 204, 390, 270, 40, -1, "IDH_CHKSEL")

            doAddCheck(oForm, "IDH_CHKCHR", 204, 30, 290, 80, -1, getTranslatedWord("Chromiun"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_CHRPER", 204, 120, 290, 40, -1, "IDH_CHKCHR")
            doAddCheck(oForm, "IDH_CHKSIL", 204, 300, 290, 80, -1, getTranslatedWord("Silver"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_SILPER", 204, 390, 290, 40, -1, "IDH_CHKSIL")

            doAddCheck(oForm, "IDH_CHKCOP", 204, 30, 310, 80, -1, getTranslatedWord("Copper"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_COPPER", 204, 120, 310, 40, -1, "IDH_CHKCOP")
            doAddCheck(oForm, "IDH_CHKZIN", 204, 300, 310, 80, -1, getTranslatedWord("Zinc"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_ZINPER", 204, 390, 310, 40, -1, "IDH_CHKZIN")

            doAddCheck(oForm, "IDH_CHKLED", 204, 30, 330, 80, -1, getTranslatedWord("Lead"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_LEDPER", 204, 120, 330, 40, -1, "IDH_CHKLED")
            doAddCheck(oForm, "IDH_CHKOMT", 204, 300, 330, 80, -1, getTranslatedWord("Other Metals"), msWasteGroupLinkTo)
            doAddEdit(oForm, "IDH_OMTPER", 204, 390, 330, 40, -1, "IDH_CHKOMT")

            '** Pane 205 Reporting Codes
            iTop = 190
            iFrame = 205
            doAddEdit(oForm, "IDH_BASEL", iFrame, 135, iTop, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SBASEL", iFrame, 30, iTop, 100, -1, getTranslatedWord("Basel Code"), "IDH_BASEL")

            doAddEdit(oForm, "IDH_OECD", iFrame, 135, iTop + 20, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SOECD", iFrame, 30, iTop + 20, 100, -1, getTranslatedWord("OECD Code"), "IDH_OECD")

            doAddEdit(oForm, "IDH_HAZCD", iFrame, 135, iTop + 40, 100, -1, msWasteGroupLinkTo)
            doAddLookupButton(oForm, "IDH_HAZLUP", iFrame, 235, iTop + 40, "IDH_HAZCD")
            doAddStatic(oForm, "SHAZ", iFrame, 30, iTop + 40, 100, -1, getTranslatedWord("Haz Code"), "IDH_HAZCD")

            doAddEdit(oForm, "IDH_WTEC", iFrame, 135, iTop + 60, 100, -1, msWasteGroupLinkTo)
            doAddLookupButton(oForm, "IDH_EWCLUP", iFrame, 235, iTop + 60, "IDH_WTEC")
            doAddStatic(oForm, "SWTE", iFrame, 30, iTop + 60, 100, -1, getTranslatedWord("EWC Code"), "IDH_WTEC")

            doAddEdit(oForm, "IDH_EWCE", iFrame, 135, iTop + 80, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SEWCE", iFrame, 30, iTop + 80, 100, -1, getTranslatedWord("EWC ExportCode"), "IDH_EWCE")

            doAddEdit(oForm, "IDH_EWCI", iFrame, 135, iTop + 100, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SEWCI", iFrame, 30, iTop + 100, 100, -1, getTranslatedWord("EWC Import Code"), "IDH_EWCI")

            doAddEdit(oForm, "IDH_NCE", iFrame, 135, iTop + 120, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SNCE", iFrame, 30, iTop + 120, 100, -1, getTranslatedWord("Ntnl code export"), "IDH_NCE")

            doAddEdit(oForm, "IDH_NCI", iFrame, 135, iTop + 140, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SNCI", iFrame, 30, iTop + 140, 100, -1, getTranslatedWord("Ntnl code import"), "IDH_NCI")

            doAddEdit(oForm, "IDH_NCO", iFrame, 135, iTop + 160, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SNCO", iFrame, 30, iTop + 160, 100, -1, getTranslatedWord("Other (specify)"), "IDH_NCO")

            doAddCombo(oForm, "IDH_WTDCUM", iFrame, 135, iTop + 180, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "WTDCUM", iFrame, 30, iTop + 180, 100, -1, getTranslatedWord("Documentation"), "IDH_WTDCUM")
            doFillDocuments(oForm)

            doAddEdit(oForm, "IDH_DRTCOD", iFrame, 135, iTop + 200, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "DRTCOD", iFrame, 30, iTop + 200, 100, -1, getTranslatedWord("Disp Route Code"), "IDH_DRTCOD")


            'Move to Right Hand side of form
            doAddEdit(oForm, "IDH_YCD", iFrame, 405, iTop, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SYCD", iFrame, 300, iTop, 100, -1, getTranslatedWord("Y-Code"), "IDH_YCD")

            doAddEdit(oForm, "IDH_HCDD", iFrame, 405, iTop + 20, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SHCDD", iFrame, 300, iTop + 20, 100, -1, getTranslatedWord("H-Code Domestic"), "IDH_HCDD")

            doAddEdit(oForm, "IDH_HCDF", iFrame, 405, iTop + 40, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SHCDF", iFrame, 300, iTop + 40, 100, -1, getTranslatedWord("H-Code Foreign"), "IDH_HCDF")

            doAddCombo(oForm, "IDH_DCD", iFrame, 405, iTop + 60, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SDCD", iFrame, 300, iTop + 60, 100, -1, getTranslatedWord("D-Code"), "IDH_DCD")
            doFillDisposalMethod(oForm)

            doAddEdit(oForm, "IDH_RCD", iFrame, 405, iTop + 80, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SRCD", iFrame, 300, iTop + 80, 100, -1, getTranslatedWord("R-Code"), "IDH_RCD")

            doAddCombo(oForm, "LKP_SCDT", iFrame, 405, iTop + 100, 300, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SSCDT", iFrame, 300, iTop + 100, 100, -1, getTranslatedWord("Source Code"), "LKP_SCDT")
            doFillCombo(oForm, "LKP_SCDT", "[@IDH_GCODE]", "U_GCode", "U_Desc")

            doAddCombo(oForm, "LKP_FCDT", iFrame, 405, iTop + 120, 300, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SFCDT", iFrame, 300, iTop + 120, 100, -1, getTranslatedWord("Form Code"), "LKP_FCDT")
            doFillCombo(oForm, "LKP_FCDT", "[@IDH_WCODE]", "U_WCode", "U_Desc")

            doAddCombo(oForm, "LKP_SYST", iFrame, 405, iTop + 140, 300, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SSYST", iFrame, 300, iTop + 140, 100, -1, getTranslatedWord("System Type"), "LKP_SYST")
            doFillCombo(oForm, "LKP_SYST", "[@IDH_HCode]", "U_HCode", "U_HDesc")

            'doAddCombo(oForm, "LKP_EPAWCD", iFrame, 405, iTop + 160, 300, -1)
            doAddStatic(oForm, "SEPAWCD", iFrame, 300, iTop + 160, 100, -1, getTranslatedWord("EPA Waste Code"), "LKP_EPACD1")
            'doFillCombo(oForm, "LKP_EPACD", "[@IDH_EPAWASTE]", "U_EPACode", "U_Desc")

            '6 fields for multi EPA Codes
            doAddEdit(oForm, "LKP_EPACD1", iFrame, 405, iTop + 160, 120, -1, msWasteGroupLinkTo)
            doAddLookupButton(oForm, "IDH_EP1LUP", iFrame, 525, iTop + 160, "LKP_EPACD1")

            doAddEdit(oForm, "LKP_EPACD2", iFrame, 405, iTop + 180, 120, -1, msWasteGroupLinkTo)
            doAddLookupButton(oForm, "IDH_EP2LUP", iFrame, 525, iTop + 180, "LKP_EPACD2")

            doAddEdit(oForm, "LKP_EPACD3", iFrame, 405, iTop + 200, 120, -1, msWasteGroupLinkTo)
            doAddLookupButton(oForm, "IDH_EP3LUP", iFrame, 525, iTop + 200, "LKP_EPACD3")

            doAddEdit(oForm, "LKP_EPACD4", iFrame, 565, iTop + 160, 120, -1, msWasteGroupLinkTo)
            doAddLookupButton(oForm, "IDH_EP4LUP", iFrame, 685, iTop + 160, "LKP_EPACD4")

            doAddEdit(oForm, "LKP_EPACD5", iFrame, 565, iTop + 180, 120, -1, msWasteGroupLinkTo)
            doAddLookupButton(oForm, "IDH_EP5LUP", iFrame, 685, iTop + 180, "LKP_EPACD5")

            doAddEdit(oForm, "LKP_EPACD6", iFrame, 565, iTop + 200, 120, -1, msWasteGroupLinkTo)
            doAddLookupButton(oForm, "IDH_EP6LUP", iFrame, 685, iTop + 200, "LKP_EPACD6")

            doAddEdit(oForm, "LKP_EPANM1", iFrame, 575, iTop + 200, 0, -1, msWasteGroupLinkTo)
            doAddEdit(oForm, "LKP_EPANM2", iFrame, 575, iTop + 200, 0, -1, msWasteGroupLinkTo)
            doAddEdit(oForm, "LKP_EPANM3", iFrame, 575, iTop + 200, 0, -1, msWasteGroupLinkTo)
            doAddEdit(oForm, "LKP_EPANM4", iFrame, 575, iTop + 200, 0, -1, msWasteGroupLinkTo)
            doAddEdit(oForm, "LKP_EPANM5", iFrame, 575, iTop + 200, 0, -1, msWasteGroupLinkTo)
            doAddEdit(oForm, "LKP_EPANM6", iFrame, 575, iTop + 200, 0, -1, msWasteGroupLinkTo)
            'master for 6 epa codes
            'doAddEdit(oForm, "LKP_EPACD", iFrame, 405, iTop + 220, 300, -1)

            '** Pane 206 Department Of Transportation (DOT/RCRA)
            iTop = 200
            iFrame = 206
            doAddEdit(oForm, "IDH_SHPNM", iFrame, 135, iTop, 400, -1, msWasteGroupLinkTo)
            doAddLookupButton(oForm, "IDH_SHNLKP", iFrame, 535, iTop, "IDH_SHPNM")
            doAddStatic(oForm, "SSHNLKP", iFrame, 30, iTop, 100, -1, getTranslatedWord("Shipping Name"), "IDH_SHPNM")

            doAddEdit(oForm, "IDH_HAZCLS", iFrame, 135, iTop + 20, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SHAZCLS", iFrame, 30, iTop + 20, 100, -1, getTranslatedWord("Hazard Class"), "IDH_HAZCLS")

            doAddEdit(oForm, "IDH_UNINA", iFrame, 135, iTop + 40, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SUNINA", iFrame, 30, iTop + 40, 100, -1, getTranslatedWord("UNINA Number"), "IDH_UNINA")

            doAddEdit(oForm, "IDH_PKGGRP", iFrame, 135, iTop + 60, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SPKGGRP", iFrame, 30, iTop + 60, 100, -1, getTranslatedWord("Packaging Group"), "IDH_PKGGRP")

            doAddEdit(oForm, "IDH_PROGEN", iFrame, 135, iTop + 80, 100, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SPROGEN", iFrame, 30, iTop + 80, 100, -1, getTranslatedWord("Process Generating"), "IDH_PROGEN")

            doAddOption(oForm, "IDH_NWW", iFrame, 30, iTop + 100, 100, -1, "NWW", "A", "NWW1", "", msWasteGroupLinkTo)
            doAddOption(oForm, "IDH_WW", iFrame, 135, iTop + 100, 100, -1, "WW", "B", "", "IDH_NWW", msWasteGroupLinkTo)

            '** Pane 207 WEEE Tab
            iTop = 200
            iFrame = 207
            doAddCombo(oForm, "IDH_WTECAT", iFrame, 235, iTop, 120, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SWTECAT", iFrame, 30, iTop, 200, -1, getTranslatedWord("EN Category"), "IDH_WTECAT")
            doFillENCategory(oForm)

            doAddCombo(oForm, "IDH_WTEIPA", iFrame, 235, iTop + 20, 120, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SWTEIPA", iFrame, 30, iTop + 20, 200, -1, getTranslatedWord("EN Incomming Based On"), "IDH_WTEIPA")
            doFillENBased(oForm, "IDH_WTEIPA")

            doAddCombo(oForm, "IDH_WTEOPA", iFrame, 235, iTop + 40, 120, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SWTEOPA", iFrame, 30, iTop + 40, 200, -1, getTranslatedWord("EN Outgoing Based On"), "IDH_WTEOPA")
            doFillENBased(oForm, "IDH_WTEOPA")

            doAddCombo(oForm, "IDH_WEEECD", iFrame, 235, iTop + 60, 120, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SWEEECD", iFrame, 30, iTop + 60, 200, -1, getTranslatedWord("WEEE Catagory"), "IDH_WEEECD")
            doFillWEEE(oForm)

            doAddCombo(oForm, "IDH_WEEECL", iFrame, 235, iTop + 80, 120, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SWEEECL", iFrame, 30, iTop + 80, 200, -1, getTranslatedWord("WEEE Classification"), "IDH_WEEECL")
            doFillWEEECL(oForm)

            doAddEdit(oForm, "IDH_WTEITU", iFrame, 235, iTop + 100, 120, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "S26", iFrame, 30, iTop + 100, 200, -1, getTranslatedWord("EN WEEE % Re-Use whole Appl."), "IDH_WTEITU")

            doAddEdit(oForm, "IDH_WTEOTC", iFrame, 235, iTop + 120, 120, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "S27", iFrame, 30, iTop + 120, 200, -1, getTranslatedWord("EN WEEE % Sent for Recovery"), "IDH_WTEOTC")

            doAddEdit(oForm, "IDH_WTEOTU", iFrame, 235, iTop + 140, 120, -1, "IDH_WTECAT")
            doAddStatic(oForm, "S28", iFrame, 30, iTop + 140, 200, -1, getTranslatedWord("EN WEEE % Sent for Re-use"), "IDH_WTEOTU")

            doAddEdit(oForm, "IDH_WTEOTW", iFrame, 235, iTop + 160, 120, -1, "IDH_WTECAT")
            doAddStatic(oForm, "S29", iFrame, 30, iTop + 160, 200, -1, getTranslatedWord("EN WEEE % Wastage"), "IDH_WTEOTW")

            '** Pane 208 Miscellenious Tab
            iTop = 200
            iFrame = 208

            doAddEdit(oForm, "IDH_WTCD", iFrame, 155, iTop, 120, -1, msWasteGroupLinkTo)
            doAddLookupButton(oForm, "IDH_UNLUP", iFrame, 275, iTop, "IDH_WTCD")
            doAddStatic(oForm, "SWTCD", iFrame, 30, iTop, 120, -1, getTranslatedWord("UN#"), "IDH_WTCD")

            doAddEdit(oForm, "IDH_WTDIV", iFrame, 155, iTop + 20, 120, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "S1", iFrame, 30, iTop + 20, 120, -1, getTranslatedWord("Division"), "IDH_WTDIV")

            doAddEdit(oForm, "IDH_PSN", iFrame, 155, iTop + 40, 120, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SPSN", iFrame, 30, iTop + 40, 120, -1, getTranslatedWord("Proper Shipping Name"), "IDH_PSN")

            doAddEdit(oForm, "IDH_MOT", iFrame, 155, iTop + 60, 120, -1, msWasteGroupLinkTo)
            doAddStatic(oForm, "SMOT", iFrame, 30, iTop + 60, 120, -1, getTranslatedWord("Mode of Transit"), "IDH_MOT")

            doAddEdit(oForm, "IDH_WTCLS", iFrame, 155, iTop + 80, 120, -1, "IDH_WTCD").Enabled = False
            doAddStatic(oForm, "S4", iFrame, 30, iTop + 80, 120, -1, getTranslatedWord("UN Class"), "IDH_WTCLS")

            doAddEdit(oForm, "IDH_WTTYP", iFrame, 155, iTop + 100, 120, -1, "IDH_WTCLS").Enabled = False
            doAddStatic(oForm, "S5", iFrame, 30, iTop + 100, 120, -1, getTranslatedWord("Material Type"), "IDH_WTTYP")

            doAddEdit(oForm, "IDH_WTSTE", iFrame, 155, iTop + 120, 120, -1, "IDH_WTTYP").Enabled = False
            doAddStatic(oForm, "S6", iFrame, 30, iTop + 120, 120, -1, getTranslatedWord("State"), "IDH_WTSTE")

            doAddEdit(oForm, "IDH_WTFLP", iFrame, 155, iTop + 140, 120, -1, "IDH_WTSTE").Enabled = False
            doAddStatic(oForm, "S7", iFrame, 30, iTop + 140, 120, -1, getTranslatedWord("Flashpoint"), "IDH_WTFLP")

            doAddEdit(oForm, "IDH_WTDNS", iFrame, 155, iTop + 160, 120, -1, "IDH_WTFLP").Enabled = False
            doAddStatic(oForm, "S8", iFrame, 30, iTop + 160, 120, -1, getTranslatedWord("Density"), "IDH_WTDNS")

            doAddEdit(oForm, "IDH_WTPH", iFrame, 155, iTop + 180, 120, -1, "IDH_WTDNS").Enabled = False
            doAddStatic(oForm, "S9", iFrame, 30, iTop + 180, 120, -1, getTranslatedWord("PH"), "IDH_WTPH")

            doAddEdit(oForm, "IDH_WTGRP", iFrame, 155, iTop + 200, 120, -1, "IDH_WTPH").Enabled = False
            doAddStatic(oForm, "S10", iFrame, 30, iTop + 200, 120, -1, getTranslatedWord("Pack Group"), "IDH_WTGRP")

            'WP Flag Field
            'doAddEdit(oForm, "IDH_WPFLAG", iFrame, 155, iTop + 200, 120, -1).Visible = False
            doAddWF(oForm, "IDH_WPFLAG", Nothing)

            'Adding Fields for the Waste Profile
            doAddWasteProfileDataFields(oForm)

        End Sub

        Public Function doAddOption2(ByVal oForm As SAPbouiCOM.Form, ByVal sUID As String, ByVal iPaneLevel As Integer, _
            ByVal iLeft As Integer, ByVal iTop As Integer, ByVal iWidth As Integer, ByVal iHeight As Integer, _
            ByVal sCaption As String, ByVal sValOn As String, ByVal sValOff As String, ByVal sDataSourceTable As String, ByVal sDataSourceField As String, Optional ByVal sGroupWith As String = "", Optional ByVal sLinkItem As String = "") As SAPbouiCOM.Item
            Dim oOption As SAPbouiCOM.OptionBtn
            Dim oItem As SAPbouiCOM.Item

            oItem = doAddItem(oForm, sUID, SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON, iPaneLevel, iLeft, iTop, iWidth, iHeight, sLinkItem)

            oOption = CType(oItem.Specific, SAPbouiCOM.OptionBtn)
            oOption.Caption = getTranslatedWord(sCaption)

            If String.IsNullOrEmpty(sValOn) = False Then
                oOption.ValOn = sValOn
            End If

            If String.IsNullOrEmpty(sValOff) = False Then
                oOption.ValOff = sValOff
            End If

            If String.IsNullOrEmpty(sGroupWith) = False Then
                oOption.GroupWith(sGroupWith)
            End If

            If String.IsNullOrEmpty(sDataSourceTable) = False AndAlso String.IsNullOrEmpty(sDataSourceField) = False Then
                doAddDF(oForm, sDataSourceTable, sUID, sDataSourceField)
            End If

            Return oItem
        End Function

        Public Function doAddOption(ByVal oForm As SAPbouiCOM.Form, ByVal sUID As String, ByVal iPaneLevel As Integer, _
            ByVal iLeft As Integer, ByVal iTop As Integer, ByVal iWidth As Integer, ByVal iHeight As Integer, _
            ByVal sCaption As String, ByVal sValOn As String, Optional ByVal sUDSource As String = "", Optional ByVal sGroupWith As String = "", Optional ByVal sLinkItem As String = "") As SAPbouiCOM.Item

            Dim oOption As SAPbouiCOM.OptionBtn
            Dim oItem As SAPbouiCOM.Item
            Dim oUDSource As SAPbouiCOM.UserDataSource

            oItem = oForm.Items.Add(sUID, SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
            oItem.Left = iLeft
            oItem.Top = iTop
            oItem.Width = iWidth
            If iHeight > 0 Then
                oItem.Height = iHeight
            End If
            oItem.FromPane = iPaneLevel
            oItem.ToPane = iPaneLevel
            oItem.LinkTo = sLinkItem
            oOption = CType(oItem.Specific, SAPbouiCOM.OptionBtn)
            oOption.Caption = getTranslatedWord(sCaption)

            'If sUDSource.Length > 0 Then
            '    oUDSource = oForm.DataSources.UserDataSources.Add(sUDSource, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
            '    oOption.DataBind.SetBound(True, "", sUDSource)
            'End If

            If sGroupWith.Length > 0 Then
                oOption.GroupWith(sGroupWith)
            End If
            oOption.ValOn = sValOn

            If sUDSource.Length > 0 Then
                oUDSource = oForm.DataSources.UserDataSources.Add(sUDSource, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                oOption.DataBind.SetBound(True, "", sUDSource)
            End If

            Return oItem
        End Function

        Public Sub doAddWasteProfileDataFields(ByVal oForm As SAPbouiCOM.Form)
            'Header
            doAddDF(oForm, "OITM", "IDH_WPTEMP", "U_WPTemplate")
            doAddDF(oForm, "OITM", "IDH_WPUSR", "U_WPTUser")
            doAddDF(oForm, "OITM", "IDH_APROVE", "U_Approved")
            doAddDF(oForm, "OITM", "IDH_APUSR", "U_AppUser")
            doAddDF(oForm, "OITM", "IDH_LETOFI", "U_LOFile")
            doAddDF(oForm, "OITM", "IDH_LTUSR", "U_LOFUser")

            doAddDF(oForm, "OITM", "IDH_HAZORD", "U_Hazordous")
            doAddDF(oForm, "OITM", "IDH_SMPREQ", "U_SAMPLEREQ")

            doAddDF(oForm, "OITM", "IDHCWBB", "U_IDHCWBB")
            doAddDF(oForm, "OITM", "IDH_REB", "U_IDHReb")
            doAddDF(oForm, "OITM", "IDH_CREB", "U_IDHCReb")

            'General Tab
            doAddDF(oForm, "OITM", "IDH_WTUDW", "U_WTUDW")
            doAddDF(oForm, "OITM", "IDH_ALW", "U_IDHALW")
            doAddDF(oForm, "OITM", "IDH_WTDFW", "U_WTDFW")

            'Location Tab
            doAddDF(oForm, "OITM", "IDH_DISPFA", "U_DispFacility")
            doAddDF(oForm, "OITM", "IDH_DPFACD", "U_DispFacCd") 'Added later to store Disposal Facility Code
            'KA -- START -- 20121025
            doAddDF(oForm, "OITM", "IDH_DFADD", "U_DFAddress")
            'KA -- END -- 20121025
            doAddDF(oForm, "OITM", "IDH_OPPOY", "U_IsPoisonous")
            doAddDF(oForm, "OITM", "IDH_OPPON", "U_IsPoisonous")
            doAddDF(oForm, "OITM", "IDH_OPVDY", "U_IsVentedDrums")
            doAddDF(oForm, "OITM", "IDH_OPVDN", "U_IsVentedDrums")
            doAddDF(oForm, "OITM", "IDH_OPWPY", "U_IsPumpable")
            doAddDF(oForm, "OITM", "IDH_OPWPN", "U_IsPumpable")
            doAddDF(oForm, "OITM", "IDH_OPPOLY", "U_IsPolymerizable")
            doAddDF(oForm, "OITM", "IDH_OPPOLN", "U_IsPolymerizable")
            doAddDF(oForm, "OITM", "IDH_OPNEY", "U_IsBenzeneWO")
            doAddDF(oForm, "OITM", "IDH_OPNEN", "U_IsBenzeneWO")
            doAddDF(oForm, "OITM", "IDH_OPCPRY", "U_IsOzone")
            doAddDF(oForm, "OITM", "IDH_OPCPRN", "U_IsOzone")
            doAddDF(oForm, "OITM", "IDH_OPMSZY", "U_IsScrap")
            doAddDF(oForm, "OITM", "IDH_OPMSZN", "U_IsScrap")
            doAddDF(oForm, "OITM", "IDH_OPCFRY", "U_IsMarinePolu")
            doAddDF(oForm, "OITM", "IDH_OPCFRN", "U_IsMarinePolu")

            'Physical Characteristics
            doAddDF(oForm, "OITM", "IDH_SPEHAN", "U_SpHandling")
            doAddDF(oForm, "OITM", "IDH_COLOR", "U_Color")
            doAddDF(oForm, "OITM", "IDH_OPODY", "U_Odor")
            doAddDF(oForm, "OITM", "IDH_OPODN", "U_Odor")
            doAddDF(oForm, "OITM", "IDH_ODOR", "U_OdorDesc")
            doAddDF(oForm, "OITM", "IDH_OPODS", "U_Layers")
            doAddDF(oForm, "OITM", "IDH_OPODB", "U_Layers")
            doAddDF(oForm, "OITM", "IDH_OPODM", "U_Layers")
            doAddDF(oForm, "OITM", "IDH_SPGRAV", "U_SpGravity")
            doAddDF(oForm, "OITM", "IDH_GRVRNG", "U_Range")
            doAddDF(oForm, "OITM", "IDH_PHYSS", "U_PhyState")
            doAddDF(oForm, "OITM", "IDH_PHYSL", "U_PhyState")
            doAddDF(oForm, "OITM", "IDH_PHYSSS", "U_PhyState")
            doAddDF(oForm, "OITM", "IDH_PHYSO", "U_PhyState")
            doAddDF(oForm, "OITM", "IDH_PSOTHR", "U_PhyStateDesc")
            doAddDF(oForm, "OITM", "IDH_VISL", "U_Viscosity")
            doAddDF(oForm, "OITM", "IDH_VISM", "U_Viscosity")
            doAddDF(oForm, "OITM", "IDH_VISH", "U_Viscosity")
            doAddDF(oForm, "OITM", "IDH_BTULB", "U_BTULB")
            doAddDF(oForm, "OITM", "IDH_PHLVL0", "U_PHLevel")
            doAddDF(oForm, "OITM", "IDH_PHLVL2", "U_PHLevel")
            doAddDF(oForm, "OITM", "IDH_PHLVL6", "U_PHLevel")
            doAddDF(oForm, "OITM", "IDH_PHLVL8", "U_PHLevel")
            doAddDF(oForm, "OITM", "IDH_PHLV12", "U_PHLevel")
            doAddDF(oForm, "OITM", "IDH_PHEXCT", "U_PHExact")
            doAddDF(oForm, "OITM", "IDH_FLPN", "U_FlashPoint")
            doAddDF(oForm, "OITM", "IDH_FLP140", "U_FlashPoint")
            doAddDF(oForm, "OITM", "IDH_FLP199", "U_FlashPoint")
            doAddDF(oForm, "OITM", "IDH_FLP200", "U_FlashPoint")
            doAddDF(oForm, "OITM", "IDH_FLPNEX", "U_FPExact")
            doAddDF(oForm, "OITM", "IDH_FRQ1", "U_Frequency")
            doAddDF(oForm, "OITM", "IDH_FRQM", "U_Frequency")
            doAddDF(oForm, "OITM", "IDH_FRQA", "U_Frequency")
            doAddDF(oForm, "OITM", "IDH_FRQO", "U_Frequency")
            doAddDF(oForm, "OITM", "IDH_FRQODS", "U_CntOther")
            doAddDF(oForm, "OITM", "IDH_FRQVOL", "U_Volume")
            doAddDF(oForm, "OITM", "IDH_CONTNR", "U_Containers")

            'Waste Composition
            doAddDF(oForm, "OITM", "IDH_CHKLIQ", "U_WCLiquid")
            doAddDF(oForm, "OITM", "IDH_LIQPER", "U_WCLiquidPerc")
            doAddDF(oForm, "OITM", "IDH_CHKSOL", "U_WCSolid")
            doAddDF(oForm, "OITM", "IDH_SOLPER", "U_WCSolidPerc")
            doAddDF(oForm, "OITM", "IDH_CHKSLU", "U_WCSludges")
            doAddDF(oForm, "OITM", "IDH_SLUPER", "U_WCSludgesPerc")
            doAddDF(oForm, "OITM", "IDH_CHKGAS", "U_WCGas")
            doAddDF(oForm, "OITM", "IDH_GASPER", "U_WCGasPerc")
            doAddDF(oForm, "OITM", "IDH_CHKPCB", "U_WCPCBs")
            doAddDF(oForm, "OITM", "IDH_PCBPER", "U_WCPCBsPerc")
            doAddDF(oForm, "OITM", "IDH_CHKCYN", "U_WCCyanides")
            doAddDF(oForm, "OITM", "IDH_CYNPER", "U_WCCyanidesPerc")
            doAddDF(oForm, "OITM", "IDH_CHKSUL", "U_WCSulfides")
            doAddDF(oForm, "OITM", "IDH_SULPER", "U_WCSulfidesPerc")
            doAddDF(oForm, "OITM", "IDH_CHKPHE", "U_WCPhenolics")
            doAddDF(oForm, "OITM", "IDH_PHEPER", "U_WCPhenolicsPerc")
            doAddDF(oForm, "OITM", "IDH_CHKHAL", "U_WCHalogen")
            doAddDF(oForm, "OITM", "IDH_HALPER", "U_WCHalogenPerc")
            doAddDF(oForm, "OITM", "IDH_CHKDIO", "U_WCDioxins")
            doAddDF(oForm, "OITM", "IDH_DIOPER", "U_WCDioxinsPerc")
            doAddDF(oForm, "OITM", "IDH_ADINFY", "U_WCAddInfo")
            doAddDF(oForm, "OITM", "IDH_ADINFN", "U_WCAddInfo")
            doAddDF(oForm, "OITM", "IDH_WPCOM", "U_WCWasteComm")
            '**** doAddDF(oForm, "OITM", "", "U_WCChemical")

            'Metals
            doAddDF(oForm, "OITM", "IDH_METN", "U_MetalAtr")
            doAddDF(oForm, "OITM", "IDH_METT", "U_MetalAtr")
            doAddDF(oForm, "OITM", "IDH_METTOT", "U_MetalAtr")
            doAddDF(oForm, "OITM", "IDH_CHKARS", "U_MArsenic")
            doAddDF(oForm, "OITM", "IDH_ARSPER", "U_MArsenicPerc")
            doAddDF(oForm, "OITM", "IDH_CHKBAR", "U_MBarium")
            doAddDF(oForm, "OITM", "IDH_BARPER", "U_MBariumPerc")
            doAddDF(oForm, "OITM", "IDH_CHKCAD", "U_MCadmium")
            doAddDF(oForm, "OITM", "IDH_CADPER", "U_MCadmiumPerc")
            doAddDF(oForm, "OITM", "IDH_CHKCHR", "U_MChromium")
            doAddDF(oForm, "OITM", "IDH_CHRPER", "U_MChromiumPerc")
            doAddDF(oForm, "OITM", "IDH_CHKCOP", "U_MCopper")
            doAddDF(oForm, "OITM", "IDH_COPPER", "U_MCopperPerc")
            doAddDF(oForm, "OITM", "IDH_CHKLED", "U_MLead")
            doAddDF(oForm, "OITM", "IDH_LEDPER", "U_MLeadPerc")
            doAddDF(oForm, "OITM", "IDH_CHKMER", "U_MMercury")
            doAddDF(oForm, "OITM", "IDH_MERPER", "U_MMercuryPerc")
            doAddDF(oForm, "OITM", "IDH_CHKNIK", "U_MNickel")
            doAddDF(oForm, "OITM", "IDH_NIKPER", "U_MNickelPerc")
            doAddDF(oForm, "OITM", "IDH_CHKSEL", "U_MSelenium")
            doAddDF(oForm, "OITM", "IDH_SELPER", "U_MSeleniumPerc")
            doAddDF(oForm, "OITM", "IDH_CHKSIL", "U_MSilver")
            doAddDF(oForm, "OITM", "IDH_SILPER", "U_MSilverPerc")
            doAddDF(oForm, "OITM", "IDH_CHKZIN", "U_MZinc")
            doAddDF(oForm, "OITM", "IDH_ZINPER", "U_MZincPerc")
            doAddDF(oForm, "OITM", "IDH_CHKOMT", "U_MOtherMat")
            doAddDF(oForm, "OITM", "IDH_OMTPER", "U_MOtherMatPerc")

            'Reporting Codes
            doAddDF(oForm, "OITM", "IDH_BASEL", "U_BASCODE")
            doAddDF(oForm, "OITM", "IDH_OECD", "U_OECDCODE")
            doAddDF(oForm, "OITM", "IDH_HAZCD", "U_HAZCD")
            doAddDF(oForm, "OITM", "IDH_WTEC", "U_EWC")
            doAddDF(oForm, "OITM", "IDH_EWCE", "U_EWCECODE")
            doAddDF(oForm, "OITM", "IDH_EWCI", "U_EWCICODE")
            doAddDF(oForm, "OITM", "IDH_NCE", "U_NCODEE")
            doAddDF(oForm, "OITM", "IDH_NCI", "U_NCODEI")
            doAddDF(oForm, "OITM", "IDH_NCO", "U_NCODEO")
            doAddDF(oForm, "OITM", "IDH_WTDCUM", "U_WTDOCUM")
            doAddDF(oForm, "OITM", "IDH_DRTCOD", "U_ROUTECD")

            doAddDF(oForm, "OITM", "IDH_YCD", "U_YCODE")
            doAddDF(oForm, "OITM", "IDH_HCDD", "U_HCODED")
            doAddDF(oForm, "OITM", "IDH_HCDF", "U_HCODEF")
            doAddDF(oForm, "OITM", "IDH_DCD", "U_DCODE")
            doAddDF(oForm, "OITM", "IDH_RCD", "U_RCODE")
            doAddDF(oForm, "OITM", "LKP_SCDT", "U_SourceCd")
            doAddDF(oForm, "OITM", "LKP_FCDT", "U_FormCd")
            doAddDF(oForm, "OITM", "LKP_SYST", "U_SysTTCd")

            '6times
            doAddDF(oForm, "OITM", "LKP_EPACD1", "U_EPAWstCd1")
            doAddDF(oForm, "OITM", "LKP_EPACD2", "U_EPAWstCd2")
            doAddDF(oForm, "OITM", "LKP_EPACD3", "U_EPAWstCd3")
            doAddDF(oForm, "OITM", "LKP_EPACD4", "U_EPAWstCd4")
            doAddDF(oForm, "OITM", "LKP_EPACD5", "U_EPAWstCd5")
            doAddDF(oForm, "OITM", "LKP_EPACD6", "U_EPAWstCd6")

            'EPA Name fields
            doAddDF(oForm, "OITM", "LKP_EPANM1", "U_EPAWstNm1")
            doAddDF(oForm, "OITM", "LKP_EPANM2", "U_EPAWstNm2")
            doAddDF(oForm, "OITM", "LKP_EPANM3", "U_EPAWstNm3")
            doAddDF(oForm, "OITM", "LKP_EPANM4", "U_EPAWstNm4")
            doAddDF(oForm, "OITM", "LKP_EPANM5", "U_EPAWstNm5")
            doAddDF(oForm, "OITM", "LKP_EPANM6", "U_EPAWstNm6")


            'Department of Transport
            doAddDF(oForm, "OITM", "IDH_SHPNM", "U_ShippingNm")

            doAddDF(oForm, "OITM", "IDH_HAZCLS", "U_HazClass")
            doAddDF(oForm, "OITM", "IDH_UNINA", "U_UNNANo")
            doAddDF(oForm, "OITM", "IDH_PKGGRP", "U_PackGrp")

            doAddDF(oForm, "OITM", "IDH_PROGEN", "U_ProcessGen")
            doAddDF(oForm, "OITM", "IDH_NWW", "U_NWW")
            doAddDF(oForm, "OITM", "IDH_WW", "U_NWW")

            'WEEE
            doAddDF(oForm, "OITM", "IDH_WTECAT", "U_WTENCAT")
            doAddDF(oForm, "OITM", "IDH_WTEIPA", "U_WTENIPA")
            doAddDF(oForm, "OITM", "IDH_WTEOPA", "U_WTENOPA")
            doAddDF(oForm, "OITM", "IDH_WEEECD", "U_IDHWEEECD")
            doAddDF(oForm, "OITM", "IDH_WEEECL", "U_IDHWEEECL")
            doAddDF(oForm, "OITM", "IDH_WTEITU", "U_WTENITU")
            doAddDF(oForm, "OITM", "IDH_WTEOTC", "U_WTENOTRC")
            doAddDF(oForm, "OITM", "IDH_WTEOTU", "U_WTENOTRU")
            doAddDF(oForm, "OITM", "IDH_WTEOTW", "U_WTENOWA")

            'Miscelleneous
            'doAddDF(oForm, "OITM", "IDH_WTEC", "U_EWC")    'Moved in the doAddWasteProfileFields()
            'doAddDF(oForm, "OITM", "IDH_HAZCD", "U_HAZCD") 'Moved in the doAddWasteProfileFields()
            doAddDF(oForm, "OITM", "IDH_WTCD", "U_WTUNCD")
            doAddDF(oForm, "OITM", "IDH_WTDIV", "U_WTDIV")
            doAddDF(oForm, "OITM", "IDH_PSN", "U_PSN")
            doAddDF(oForm, "OITM", "IDH_MOT", "U_MODETR")
            doAddDF(oForm, "OITM", "IDH_WTCLS", "U_WTUNCLS")
            doAddDF(oForm, "OITM", "IDH_WTTYP", "U_WTUNTYP")
            doAddDF(oForm, "OITM", "IDH_WTSTE", "U_WTUNSTE")
            doAddDF(oForm, "OITM", "IDH_WTFLP", "U_WTUNFLP")
            doAddDF(oForm, "OITM", "IDH_WTDNS", "U_WTUNDNS")
            doAddDF(oForm, "OITM", "IDH_WTPH", "U_WTUNPH")
            doAddDF(oForm, "OITM", "IDH_WTGRP", "U_WTUNGRP")

        End Sub

        Protected Overridable Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
        End Sub

        Protected Overridable Sub doSetWasteGroupFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
        End Sub

        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ChemicalCd", "Chemical Code", True, 20, Nothing, Nothing)
            oGridN.doAddListField("U_ChemicalNm", "Chemical Name", False, 300, Nothing, Nothing)
            oGridN.doAddListField("U_Quantity", "Quantity", True, 15, Nothing, Nothing)
            oGridN.doAddListField("U_MinQty", "Minimum Qty", True, 15, Nothing, Nothing)
            oGridN.doAddListField("U_MaxQty", "Maximum Qty", True, 15, Nothing, Nothing)
            oGridN.doAddListField("U_ItemCd", "Item Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ItemNm", "Item Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_IsPercentage", "Is Percentage", True, -1, "CHECKBOX", Nothing)

        End Sub

        'KA -- START -- 20121022
        Protected Overridable Sub doSetWasteGroupListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_WstGpCd", "Waste Group Code", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_CntyCd", "County Code", False, 0, "COMBOBOX", Nothing)

            'oGridN.doAddListField("U_VendorCd", "Vendor Code", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            'oGridN.doAddListField("U_VendorNm", "Vendor Name", False, 60, Nothing, Nothing)

            'oGridN.doAddListField("U_CostTrf", "Cost Tarif", True, 15, Nothing, Nothing)
            'oGridN.doAddListField("U_ChargeTrf", "Charge Tarif", True, 15, Nothing, Nothing)

            oGridN.doAddListField("U_ItemCd", "Item Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ItemNm", "Item Name", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("U_Include", "Include", True, -1, "CHECKBOX", Nothing)

        End Sub

        Public Function isValidPercentage(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean, ByVal sItemName As String, ByVal sDBField As String, ByVal oEdt As SAPbouiCOM.EditText) As Boolean
            With oForm.DataSources.DBDataSources.Item("OITM")

                Dim sTotPer As String = .GetValue(sDBField, .Offset)
                If sTotPer = "" Then sTotPer = "0"

                If Convert.ToInt16(sTotPer) > 100 Then
                    doWarnMess("Please enter value between 0-100", SAPbouiCOM.BoMessageTime.bmt_Short)
                    doSetFocus(oForm, sItemName)
                    BubbleEvent = False
                    Return False
                End If

                Dim iTotPer As Integer
                Dim sLiqPer As String = .GetValue("U_WCLiquidPerc", .Offset)
                Dim sSolPer As String = .GetValue("U_WCSolidPerc", .Offset)
                Dim sSluPer As String = .GetValue("U_WCSludgesPerc", .Offset)
                Dim sGasPer As String = .GetValue("U_WCGasPerc", .Offset)

                If sLiqPer = "" Then sLiqPer = "0"
                If sSolPer = "" Then sSolPer = "0"
                If sSluPer = "" Then sSluPer = "0"
                If sGasPer = "" Then sGasPer = "0"

                iTotPer = Convert.ToInt16(sLiqPer) + _
                            Convert.ToInt16(sSolPer) + _
                            Convert.ToInt16(sSluPer) + _
                            Convert.ToInt16(sGasPer)
                If iTotPer > 100 Then
                    doWarnMess("The value exceeds the limit to be 100% in total. ", SAPbouiCOM.BoMessageTime.bmt_Short)
                    doSetFocus(oForm, sItemName)
                    BubbleEvent = False
                    Return False
                Else
                    'setWFValue(oForm, "IDH_TOTPER", iTotPer.ToString())
                    If oForm.Items.Item("IDH_TOTPER").Visible = False Then
                        oForm.Items.Item("IDH_TOTPER").Visible = True
                        oEdt.Value = Conversions.ToString(iTotPer)
                    Else
                        oForm.Items.Item("IDH_TOTPER").Visible = False
                        oEdt.Value = Conversions.ToString(iTotPer)
                    End If
                End If
            End With
            Return True
        End Function

        Public Sub doAddChemicalGrid(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "CHMGRID")
            If oGridN Is Nothing Then
                Dim owItem As SAPbouiCOM.Item = oForm.Items.Item("21")
                'oGridN = New UpdateGrid(Me, oForm, "CHMGRID", 250, 315, 600, 130, 203, 203)
                oGridN = New UpdateGrid(Me, oForm, "CHMGRID", 250, 315, 375, 130, 203, 203)
                oGridN.getSBOItem.LinkTo = msWasteGroupLinkTo
                'oGridN.getSBOItem.FromPane = 203
                'oGridN.getSBOItem.ToPane = 203
                'oGridN.doSetSBOAutoReSize(True)
            End If
            doSetFilterFields(oGridN)
            doSetListFields(oGridN)

            oGridN.doAddGridTable(New GridTable("@ISB_CASTBL", Nothing, "Code", True, True), True)

            oGridN.doSetDoCount(True)
            oGridN.doSetHistory("@ISB_CASTBL", "Code")

        End Sub

        Public Sub doLoadChemicalsGrid(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "CHMGRID")
                Dim owItem As SAPbouiCOM.Item = oForm.Items.Item("21")
                Dim sItemCode As String = CType(getDFValue(oForm, "OITM", "ItemCode"), String)
                If sItemCode.Length = 0 OrElse sItemCode.StartsWith("*") OrElse sItemCode.EndsWith("*") Then
                    sItemCode = "-1"
                End If

                'oGrid.getSBOItem().Left = 250
                'oGrid.getSBOItem().Top = 315
                'oGrid.getSBOItem().Width = 600
                'oGrid.getSBOItem().Height = 130
                'oGrid.getSBOItem().FromPane = 203
                'oGrid.getSBOItem().ToPane = 203
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    oGrid.doEnabled(False)
                Else
                    oGrid.doEnabled(True)
                End If

                oGrid.setRequiredFilter("U_ItemCd = '" & sItemCode & "'")
                oGrid.doReloadData()

                'If Not (oGrid.hasChangedRows OrElse oGrid.hasAddedRows()) Then
                '    oGrid.setRequiredFilter("U_ItemCd = '" & sItemCode & "'")
                '    oGrid.doReloadData()
                'End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doLoadChemicalsGrid()")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDG", {com.idh.bridge.Translation.getTranslatedWord("Chemicals")})
            End Try

        End Sub

        Public Sub doLoadChemicalsGrid(ByVal oForm As SAPbouiCOM.Form, ByVal sNewItemCode As String)
            Try
                Dim oGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "CHMGRID")
                Dim owItem As SAPbouiCOM.Item = oForm.Items.Item("21")
                Dim sItemCode As String = sNewItemCode 'getDFValue(oForm, "OITM", "ItemCode")
                If sItemCode.Length = 0 OrElse sItemCode.StartsWith("*") OrElse sItemCode.EndsWith("*") Then
                    sItemCode = "-1"
                End If

                'oGrid.getSBOItem().Left = 250
                'oGrid.getSBOItem().Top = 315
                'oGrid.getSBOItem().Width = 600
                'oGrid.getSBOItem().Height = 130
                'oGrid.getSBOItem().FromPane = 203
                'oGrid.getSBOItem().ToPane = 203
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    oGrid.doEnabled(False)
                Else
                    oGrid.doEnabled(True)
                End If


                'Check if there is any change in the Grid
                If Not (oGrid.hasChangedRows OrElse oGrid.hasAddedRows()) Then
                    oGrid.setRequiredFilter("U_ItemCd = '" & sItemCode & "'")
                    oGrid.doReloadData()
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doLoadChemicalsGrid()")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDG", {com.idh.bridge.Translation.getTranslatedWord("Chemicals")})
            End Try
        End Sub

        'Waste Grouping Tab
        Public Sub doAddWasteGroupGrid(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "WGPGRID")
            If oGridN Is Nothing Then
                Dim owItem As SAPbouiCOM.Item = oForm.Items.Item("21")
                oGridN = New UpdateGrid(Me, oForm, "WGPGRID", 30, 200, 600, 230, 209, 209)
                'oGridN.getSBOItem.FromPane = 209
                'oGridN.getSBOItem.ToPane = 209
            End If
            doSetWasteGroupFilterFields(oGridN)
            doSetWasteGroupListFields(oGridN)

            oGridN.doAddGridTable(New GridTable("@IDH_WGPCNTY", Nothing, "Code", True, True), True)

            oGridN.doSetDoCount(True)
            oGridN.doSetHistory("@IDH_WGPCNTY", "Code")
        End Sub

        Public Sub doLoadWasteGroupGrid(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "WGPGRID")
                Dim owItem As SAPbouiCOM.Item = oForm.Items.Item("21")
                Dim sItemCode As String = CType(getDFValue(oForm, "OITM", "ItemCode"), String)
                If sItemCode.Length = 0 OrElse sItemCode.StartsWith("*") OrElse sItemCode.EndsWith("*") Then
                    sItemCode = "-1"
                End If

                'oGrid.getSBOItem().Left = 30 '250
                'oGrid.getSBOItem().Top = 200 '315
                'oGrid.getSBOItem().Width = 800
                'oGrid.getSBOItem().Height = 230
                'oGrid.getSBOItem().FromPane = 209
                'oGrid.getSBOItem().ToPane = 209

                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    oGrid.doEnabled(False)
                Else
                    oGrid.doEnabled(True)
                End If

                If Not (oGrid.hasChangedRows OrElse oGrid.hasAddedRows()) Then
                    oGrid.setRequiredFilter("U_ItemCd = '" & sItemCode & "'")
                    oGrid.doReloadData()

                    'Populate Combo's
                    doLoadWasteGpCombo(oForm)
                    '#MA 2017142 issue:263 start 2017214
                    'doLoadCountyCombo(oForm)
                    '#MA End 2017142 

                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doLoadWasteGroupGrid()")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDG", {com.idh.bridge.Translation.getTranslatedWord("WasteGroup")})
            End Try

        End Sub

        Public Sub doLoadWasteGroupGrid(ByVal oForm As SAPbouiCOM.Form, ByVal sNewItemCode As String)
            Try
                Dim oGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "WGPGRID")
                Dim owItem As SAPbouiCOM.Item = oForm.Items.Item("21")
                Dim sItemCode As String = sNewItemCode 'getDFValue(oForm, "OITM", "ItemCode")
                If sItemCode.Length = 0 OrElse sItemCode.StartsWith("*") OrElse sItemCode.EndsWith("*") Then
                    sItemCode = "-1"
                End If

                'oGrid.getSBOItem().Left = 250
                'oGrid.getSBOItem().Top = 315
                'oGrid.getSBOItem().Width = 600
                'oGrid.getSBOItem().Height = 130
                'oGrid.getSBOItem().FromPane = 209
                'oGrid.getSBOItem().ToPane = 209

                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    oGrid.doEnabled(False)
                Else
                    oGrid.doEnabled(True)
                End If

                If Not (oGrid.hasChangedRows OrElse oGrid.hasAddedRows()) Then
                    oGrid.setRequiredFilter("U_ItemCd = '" & sItemCode & "'")
                    oGrid.doReloadData()

                    'Populate Combo's
                    doLoadWasteGpCombo(oForm)
                    '#MA 2017142 issue:263 start 2017214
                    'doLoadCountyCombo(oForm)
                    '#MA End  2017214

                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doLoadWasteGroupGrid()")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDG", {com.idh.bridge.Translation.getTranslatedWord("WasteGroup")})
            End Try
        End Sub

        Private Sub doLoadChemicalCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "CHMGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_ChemicalCd")), SAPbouiCOM.ComboBoxColumn)
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doFillCombo(oForm, oCombo, "[@IDH_CHEM]", "U_ChemicalCd", "U_ChemicalNm", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Chemicals Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Chemicals")})
            End Try

        End Sub

        Private Sub doLoadWasteGpCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "WGPGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_WstGpCd")), SAPbouiCOM.ComboBoxColumn)
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doFillCombo(oForm, oCombo, "[@ISB_WASTEGRP]", "U_WstGpCd", "U_WstGpNm", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Waste Group Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Waste Group")})
            End Try

        End Sub

        Private Sub doLoadCountyCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "WGPGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                'If iWidth is 0 then type is editBox and if iWidth is -1 then type is Combobox. we are using 0 thats why it gives error.
                'Dim colType5 As String = oGridN.Columns.Item(4).Type.ToString()  
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_CntyCd")), SAPbouiCOM.ComboBoxColumn)
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doFillCombo(oForm, oCombo, "[@ISB_COUNTY]", "U_CountyCd", "U_CountyNm", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the County Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("County")})
            End Try

        End Sub

        Public Sub doSetChemicalGridLastLine(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "CHMGRID")
            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "CASSEQ")
            oGridN.doSetFieldValue("Code", pVal.Row, oNumbers.CodeCode, True)
            oGridN.doSetFieldValue("Name", pVal.Row, oNumbers.NameCode, True)

            Dim sItemCode As String = CType(getDFValue(oForm, "OITM", "ItemCode"), String)
            oGridN.doSetFieldValue("U_ItemCd", pVal.Row, sItemCode, True)

            Dim sItemName As String = CType(getDFValue(oForm, "OITM", "ItemName"), String)
            oGridN.doSetFieldValue("U_ItemNm", pVal.Row, sItemName, True)

            'Dim sChemicalCd As String = getDFValue(oForm, "OCRD", "U_IDHUOM")
            'oGridN.doSetFieldValue("U_UOM", pVal.Row, sBPUOM, True)

            'Dim sCardType As String = getDFValue(oForm, "OCRD", "CardType")
            'oGridN.doSetFieldValue("U_CardType", pVal.Row, sCardType, True)

            oGridN.doSetFieldValue("U_Quantity", pVal.Row, 0, True)
            oGridN.doSetFieldValue("U_MinQty", pVal.Row, 0, True)
            oGridN.doSetFieldValue("U_MaxQty", pVal.Row, 0, True)
            oGridN.doSetFieldValue("U_IsPercentage", pVal.Row, 0, True)
        End Sub

        Public Sub doSetWasteGroupGridLastLine(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "WGPGRID")
            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "WGCNTSEQ")
            oGridN.doSetFieldValue("Code", pVal.Row, oNumbers.CodeCode, True)
            oGridN.doSetFieldValue("Name", pVal.Row, oNumbers.NameCode, True)

            Dim sItemCode As String = CType(getDFValue(oForm, "OITM", "ItemCode"), String)
            oGridN.doSetFieldValue("U_ItemCd", pVal.Row, sItemCode, True)

            Dim sItemName As String = CType(getDFValue(oForm, "OITM", "ItemName"), String)
            oGridN.doSetFieldValue("U_ItemNm", pVal.Row, sItemName, True)

            oGridN.doSetFieldValue("U_WstGpCd", pVal.Row, "", True)
            oGridN.doSetFieldValue("U_CntyCd", pVal.Row, "", True)

            'oGridN.doSetFieldValue("U_VendorCd", pVal.Row, "", True)
            'oGridN.doSetFieldValue("U_VendorNm", pVal.Row, "", True)

            'oGridN.doSetFieldValue("U_CostTrf", pVal.Row, 0, True)
            'oGridN.doSetFieldValue("U_ChargeTrf", pVal.Row, 0, True)

            'oGridN.doSetFieldValue("U_Include", pVal.Row, 0, True)
        End Sub
        'KA -- END -- 20121022

    End Class
End Namespace

