'
' Created by SharpDevelop.
' User: Louis Viljoen
' User: Louis Viljoen
'
Imports System.Collections
Imports System.IO
Imports System

Imports com.idh.bridge.lookups
Imports com.idh.bridge
Imports SAPbouiCOM

Namespace idh.forms.SBO
    Public Class ServiceCall
        Inherits IDHAddOns.idh.forms.Base
        Private miSpaceWidth As Integer = 0
        Private miFolderWidth As Integer = 0
        
        Private msTabAnchor As String
        Private msRightTabPanelLine As String
        Private msBottomTabPanelLine As String
        Private msTopTabPanelLine As String
        Private msUnitPriceTextBox As String
        Private msUnitPriceLabel As String
        Private msRemarkFolder As String
        Private msInventoryCheckBox As String
        Private msSalesCheckBox As String
        Private msAssitCheckbox As String
        Private msPurchaseCheckBox As String
        Private msPriceListDropDown As String
        Private msPriceListLabel As String
        Private msOkButton As String
        Private msHeaderLinkTo As String
        Private msWasteGroupLinkTo As String
        Private miWasteProfileRecRightSpace As Integer = 50
        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "60110", -1, Nothing)

            gsSystemMenuID = "3073"

            If IDHAddOns.idh.addon.Base.LAYOUTTYPE = FixedValues.SBOLayout2009PL5 Then
                msTabAnchor = "234000005"
                msRightTabPanelLine = "103"
                msBottomTabPanelLine = "100"
                msTopTabPanelLine = "303"
                msUnitPriceTextBox = "107"
                msUnitPriceLabel = "106"
                msRemarkFolder = "9"
                msPriceListDropDown = "24"
                msPriceListLabel = "25"
                msOkButton = "1"

                msInventoryCheckBox = "14"
                msSalesCheckBox = "13"
                msPurchaseCheckBox = "12"
                msHeaderLinkTo = "106"
                msWasteGroupLinkTo = "6"
            Else
                msTabAnchor = "540002073"
                msRightTabPanelLine = "103"
                msBottomTabPanelLine = "100"
                msTopTabPanelLine = "303"
                msUnitPriceTextBox = "34"
                msUnitPriceLabel = "52"
                msRemarkFolder = "9"
                msPriceListDropDown = "24"
                msPriceListLabel = "25"
                msOkButton = "1"

                msInventoryCheckBox = "14"
                msSalesCheckBox = "13"
                msPurchaseCheckBox = "12"
                msAssitCheckbox = "42"

                msHeaderLinkTo = "24"
                msWasteGroupLinkTo = "6"
            End If
            doEnableHistory("OSCL")
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
        	doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
        End Sub
        Private Function doAddMainFolders(ByVal oForm As SAPbouiCOM.Form) As SAPbouiCOM.Item
            Dim oItm As SAPbouiCOM.Item
            Dim osItm As SAPbouiCOM.Item
            Dim oFolderItem As SAPbouiCOM.Item
            Dim oFolder As SAPbouiCOM.Folder

            oFolderItem = oForm.Items.Item(msTabAnchor) '(msTabAnchor)

            If IDHAddOns.idh.addon.Base.LAYOUTTYPE = FixedValues.SBOLayout2009PL5 Then
                miFolderWidth = 1
            Else
                miFolderWidth = oFolderItem.Width
            End If

            'Return oFolderItem

            oItm = oForm.Items.Add("IDH_RETAB", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItm.Left = oFolderItem.Left + oFolderItem.Width
            oItm.Top = oFolderItem.Top
            oItm.Width = miFolderWidth
            oItm.Height = oFolderItem.Height
            oItm.FromPane = 0
            oItm.ToPane = 0
            oItm.Visible = True
            oItm.AffectsFormMode = False
            oItm.LinkTo = oFolderItem.LinkTo

            oFolder = CType(oItm.Specific, SAPbouiCOM.Folder)
            oFolder.Pane = 20
            oFolder.Caption = getTranslatedWord("Requirements")
            oFolder.GroupWith(msTabAnchor)

            osItm = oItm

            If IDHAddOns.idh.addon.Base.LAYOUTTYPE = FixedValues.SBOLayout2009PL5 Then
            Else
                Dim iExtraSpace As Integer
                oItm = oForm.Items.Item(msRightTabPanelLine)
                iExtraSpace = oForm.Width - oItm.Left - 25
                oItm.Left = oItm.Left + iExtraSpace 'miFolderWidth

                oItm = oForm.Items.Item(msBottomTabPanelLine)
                oItm.Width = oItm.Width + iExtraSpace 'miFolderWidth

                oItm = oForm.Items.Item(msTopTabPanelLine)
                oItm.Width = oItm.Width + iExtraSpace 'miFolderWidth
            End If

            Return oFolderItem
        End Function

        Public Overrides Sub doLoadForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try

                Console.WriteLine("doLoadForm --> START ")
                Console.WriteLine("sICode = " + getParentSharedDataAsString(oForm, "WPIT_ITEMCODE"))
                Console.WriteLine("sIName = " + getParentSharedDataAsString(oForm, "WPIT_ITEMNAME"))
                Console.WriteLine("sWPItem = " + getParentSharedDataAsString(oForm, "WPITM"))

                doAddWF(oForm, "A_ACTIVE", False)

                doAddWF(oForm, "wfWPIT_ITEMCODE", False)
                doAddWF(oForm, "wfWPIT_ITEMNAME", False)
                doAddWF(oForm, "wfWPITM", False)
                doAddWF(oForm, "wfWPS_CUST", False)
                doAddWF(oForm, "wfWPS_CUSTNM", False)
                doAddWF(oForm, "wfWPS_ADDR", False)

                setWFValue(oForm, "wfWPIT_ITEMCODE", getParentSharedDataAsString(oForm, "WPIT_ITEMCODE"))
                setWFValue(oForm, "wfWPIT_ITEMNAME", getParentSharedDataAsString(oForm, "WPIT_ITEMNAME"))
                setWFValue(oForm, "wfWPITM", getParentSharedDataAsString(oForm, "WPITM"))
                setWFValue(oForm, "wfWPS_CUST", getParentSharedDataAsString(oForm, "WPS_CUST"))
                setWFValue(oForm, "wfWPS_CUSTNM", getParentSharedDataAsString(oForm, "WPS_CUSTNM"))
                setWFValue(oForm, "wfWPS_ADDR", getParentSharedDataAsString(oForm, "WPS_ADDR"))

                Dim oItem As SAPbouiCOM.Item
                Dim oFolderItem As SAPbouiCOM.Item

                oFolderItem = doAddMainFolders(oForm)

                Dim iTop As Integer = oFolderItem.Top + 25

                doCreateWasteProfile(oForm)

                Console.WriteLine("doLoadForm --> END ")
                Console.WriteLine("sICode = " + getParentSharedDataAsString(oForm, "WPIT_ITEMCODE"))
                Console.WriteLine("sIName = " + getParentSharedDataAsString(oForm, "WPIT_ITEMNAME"))
                Console.WriteLine("sWPItem = " + getParentSharedDataAsString(oForm, "WPITM"))

                Dim iFromPane As Integer
                Dim iToPane As Integer

                Dim iHeight As Integer = 15

                Dim iTopStatic As Integer
                Dim iLeftStatic1 As Integer
                Dim iWidthStatic1 As Integer
                Dim iLeftStatic2 As Integer
                Dim iWidthStatic2 As Integer
                Dim iLeftStatic3 As Integer
                Dim iWidthStatic3 As Integer

                Dim iTopEdit As Integer
                Dim iLeftEdit1 As Integer
                Dim iWidthEdit1 As Integer
                Dim iLeftEdit2 As Integer
                Dim iWidthEdit2 As Integer
                Dim iLeftEdit3 As Integer
                Dim iWidthEdit3 As Integer

                oItem = oForm.Items.Item("8")

                '************************************
                'The Header
                '************************************
                'The OSM Button
                oItem = doAddButton(oForm, "IDH_OSM", 0, oItem.Left + oItem.Width + 20, oItem.Top, oItem.Width, oItem.Height, getTranslatedWord("OSM"), "8")

                'The Sub Contractor Button
                oItem = doAddButton(oForm, "IDH_SUBCON", 0, oItem.Left + oItem.Width + 5, oItem.Top, oItem.Width, oItem.Height, getTranslatedWord("Subcon."), "8")

                ''Prices
                'doAddButton(oForm, "IDH_PRICES", 0, oItem.Left + oItem.Width + 5, oItem.Top, oItem.Width, oItem.Height, getTranslatedWord("Prices"), "8")


                'Validation - Combo
                oItem = oForm.Items.Item("42")
                iHeight = oItem.Height
                doAddDBCombo(oForm, "IDHVal", "OSCL", "U_IDHVal", oItem.FromPane, oItem.Left, oItem.Top + 15, oItem.Width, iHeight, "93")

                'Validation - Static
                oItem = oForm.Items.Item("43")
                doAddStatic(oForm, "IDHS000", 0, oItem.Left, oItem.Top + 15, oItem.Width, iHeight, getTranslatedWord("Validation"), "IDHVal")

                '#MA Start 30-03-2017
                oItem = oForm.Items.Item("42")
                doAddDBEdit(oForm, "IDHFlag", "OSCL", "U_Flag", oItem.FromPane, oItem.Left, oItem.Top + 30, oItem.Width, oItem.Height, "42")
                oItem = oForm.Items.Item("IDHS000")
                doAddStatic(oForm, "IDHEFlag", 0, oItem.Left, oItem.Top + 15, oItem.Width, iHeight, getTranslatedWord("Flag"), "IDHFlag")

                oItem = oForm.Items.Item("107")
                doAddDBEdit(oForm, "IDHQty", "OSCL", "U_Qty", oItem.FromPane, oItem.Left, oItem.Top + 15, oItem.Width, oItem.Height, "107")
                oItem = oForm.Items.Item("106")
                doAddStatic(oForm, "IDHEQty", 0, oItem.Left, oItem.Top + 15, oItem.Width, iHeight, getTranslatedWord("Quantity"), "IDHQty")
                '#MA End 30-03-2017

                '************************************
                'The General Tab
                '************************************                
                'Add the Static and Edit Controls
                oItem = oForm.Items.Item("101")
                oItem.Height = oItem.Height + 2
                'oItem.BackColor = &H0000FF

                oItem = oForm.Items.Item("100")
                oItem.Height = oItem.Height + 2
                'oItem.BackColor = &H0000FF

                oItem = oForm.Items.Item("99")
                oItem.Top = oItem.Top + 2
                'oItem.BackColor = &H0000FF

                oItem = oForm.Items.Item("94")

                iTopStatic = oItem.Top
                iLeftStatic1 = oItem.Left
                iWidthStatic1 = oItem.Width
                iFromPane = oItem.FromPane
                iToPane = oItem.ToPane

                oItem = oForm.Items.Item("149")
                iLeftStatic2 = oItem.Left - 31
                iWidthStatic2 = oItem.Width + 31

                oItem = oForm.Items.Item("155")
                iLeftStatic3 = oItem.Left
                iWidthStatic3 = oItem.Width

                oItem = oForm.Items.Item("93")
                iHeight = oItem.Height
                iTopEdit = oItem.Top
                iLeftEdit1 = oItem.Left
                iWidthEdit1 = oItem.Width

                oItem = oForm.Items.Item("147")
                iLeftEdit2 = oItem.Left
                iWidthEdit2 = iWidthEdit1 - 29

                oItem = oForm.Items.Item("150")
                iLeftEdit3 = oItem.Left
                iWidthEdit3 = iWidthEdit1 - 28

                iTopEdit = iTopEdit + iHeight + 1
                doAddDBEdit(oForm, "IDHJobNr", "OSCL", "U_IDHJobNr", iFromPane, iLeftEdit1, iTopEdit, iWidthEdit1, iHeight, "93")

                iTopStatic = iTopStatic + iHeight + 1
                oItem = oForm.Items.Item("94")
                doAddStatic(oForm, "IDHS01", iFromPane, iLeftStatic1, iTopStatic, iWidthStatic1, iHeight, getTranslatedWord("Job Number"), "IDHJobNr")

                'Job Date
                iTopEdit = iTopEdit + iHeight + 1
                doAddDBEdit(oForm, "IDHJDate", "OSCL", "U_IDHJDate", iFromPane, iLeftEdit1, iTopEdit, iWidthEdit1, iHeight, "93")

                iTopStatic = iTopStatic + iHeight + 1
                doAddStatic(oForm, "IDHS02", iFromPane, iLeftStatic1, iTopStatic, iWidthStatic1, iHeight, getTranslatedWord("Job Date"), "IDHJDate")

                'Site Address - Edit
                iTopEdit = iTopEdit + iHeight + 1
                doAddDBEdit(oForm, "IDHSAddr", "OSCL", "U_IDHSAddr", iFromPane, iLeftEdit1, iTopEdit, iWidthEdit1, iHeight, "93")
                doAddLookupButton(oForm, "IDHLSA", iFromPane, iLeftEdit1 + iWidthEdit1 + 1, iTopEdit, "IDHSAddr")

                'Site Contact - Edit
                doAddDBEdit(oForm, "IDHACN", "OSCL", "U_IDHACN", iFromPane, iLeftEdit2, iTopEdit, iWidthEdit2, iHeight, "147")

                'Site Tel - Edit
                doAddDBEdit(oForm, "IDHTel1", "OSCL", "U_IDHTel1", iFromPane, iLeftEdit3, iTopEdit, iWidthEdit3, iHeight, "153")

                'Site Address - Static
                iTopStatic = iTopStatic + iHeight + 1
                doAddStatic(oForm, "IDHS03", iFromPane, iLeftStatic1, iTopStatic, iWidthStatic1, iHeight, getTranslatedWord("Site Address"), "IDHSAddr")

                'Site Contact - Static
                doAddStatic(oForm, "IDHS101", iFromPane, iLeftStatic2, iTopStatic, iWidthStatic2, iHeight, getTranslatedWord("Site Contact"), "IDHACN")

                'Site Tel - Static
                doAddStatic(oForm, "IDHS201", iFromPane, iLeftStatic3, iTopStatic, iWidthStatic3, iHeight, getTranslatedWord("Site Tel"), "IDHTel1")

                'Site Id - Edit
                iTopEdit = iTopEdit + iHeight + 1
                doAddDBEdit(oForm, "IDHSteId", "OSCL", "U_IDHSteId", iFromPane, iLeftEdit1, iTopEdit, iWidthEdit1, iHeight, "93")

                'Sub Contract - Edit
                doAddDBEdit(oForm, "IDHSubC", "OSCL", "U_IDHSubC", iFromPane, iLeftEdit2, iTopEdit, iWidthEdit2, iHeight, "147")

                'Reason - Combo
                doAddDBCombo(oForm, "IDHReas", "OSCL", "U_IDHReas", iFromPane, iLeftEdit3, iTopEdit, iWidthEdit3, iHeight, "153")

                'Site Id - Static
                iTopStatic = iTopStatic + iHeight + 1
                doAddStatic(oForm, "IDHS002", iFromPane, iLeftStatic1, iTopStatic, iWidthStatic1, iHeight, getTranslatedWord("Site Id"), "IDHSteId")

                'Sub Contract - Static
                doAddStatic(oForm, "IDHS102", iFromPane, iLeftStatic2, iTopStatic, iWidthStatic2, iHeight, getTranslatedWord("Sub Contract"), "IDHSubC")

                'Reason - Static
                doAddStatic(oForm, "IDHS202", iFromPane, iLeftStatic3, iTopStatic, iWidthStatic3, iHeight, getTranslatedWord("Reason"), "IDHReas")

                doFillReasonCombo(oForm)
                doFillValidationCombo(oForm)
                oForm.Height = 420
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDLF", {Nothing})
                BubbleEvent = False
                Exit Sub
            End Try
        End Sub

        Public Sub doFillReasonCombo(ByVal oForm As SAPbouiCOM.Form)
			doFillCombo(oForm, "IDHReas", "[@IDH_SCREAS]", "Code", "Name")
		End Sub

		Public Sub doFillValidationCombo(ByVal oForm As SAPbouiCOM.Form)
			doFillCombo(oForm, "IDHVal", "[@IDH_SCVALID]", "Code", "Name")
		End Sub

		'** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
			If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                	If pVal.ItemUID = "IDHLSA" Then
                		Dim sCardCode As String = getDFValue(oForm, "OSCL", "customer")
                		If Not sCardCode Is Nothing AndAlso sCardCode.Length > 0 Then
	                        'setSharedData(oForm, "TRG", "SITEADDR")
	                        setSharedData(oForm, "IDH_CUSCOD", sCardCode)
	                        setSharedData(oForm, "IDH_ADRTYP", "S")
	                        goParent.doOpenModalForm("IDHASRCH", oForm)
                		End If
                        '#MA Start 2017-03-13
                    ElseIf pVal.ItemUID = "LKUPWASCD" Then
                        doChooseWasteType(oForm)
                    ElseIf pVal.ItemUID = "LKUPCONT" Then 'Container Choose from
                        doChooseContainer(oForm)
                    ElseIf pVal.ItemUID.Equals("LKUPDSPSUP") Then 'BP Choose from
                        doChooseCustomer(oForm)
                        '#MA End 2017-03-13
					ElseIf pVal.ItemUID = "IDH_SUBCON" Then
                		Dim sCardCode As String = getDFValue(oForm, "OSCL", "customer")
                		'Dim sAddr As String =  getDFValue(oForm, "OSCL", "U_IDHSAddr")
                		
                        If Not sCardCode Is Nothing AndAlso sCardCode.Length > 0 Then
                            setSharedData(oForm, "IDH_ZPCDTX", "")
                            setSharedData(oForm, "IDH_ITEM", "")
                            setSharedData(oForm, "IDH_WSCD", "")
                            setSharedData(oForm, "IDH_RTNG", "")

                            setSharedData(oForm, "IDH_CUSCOD", sCardCode)
                            goParent.doOpenModalForm("IDH_SUBBY", oForm)
                        End If
            		ElseIf pVal.ItemUID = "IDH_OSM" Then
                        Dim sCardCode As String = getDFValue(oForm, "OSCL", "customer")
                        If sCardCode Is Nothing Then
                        	sCardCode = ""
                        End If
                        
                        Dim sSiteAddress As String = getDFValue(oForm, "OSCL", "U_IDHSAddr")
                        If sSiteAddress Is Nothing Then
                        	sSiteAddress = ""
                        End If
                        
                        Dim sSiteId As String = getDFValue(oForm, "OSCL", "U_IDHSteId")
                        If sSiteId Is Nothing Then
                        	sSiteId = ""
                        End If
                        
                        Dim sReqDate As String = getDFValue(oForm, "OSCL", "U_IDHJDate")
                        If sReqDate Is Nothing Then
                        	sReqDate = ""
                        End If
                        
                        Dim sJobNumber As String = getDFValue(oForm, "OSCL", "U_IDHJobNr")
                        Dim sRowNumber As String = ""
                        If sJobNumber Is Nothing Then
                        	sJobNumber = ""
						Else
                        	If  sJobNumber.Length() > 0 Then
	                        	Dim iIndex As Integer = sJobNumber.IndexOf(".")	
	                        	
	                        	If iIndex > -1 Then
	                        		sRowNumber = sJobNumber.Substring(iIndex+1)
	                        		sJobNumber = sJobNumber.Substring(0, iIndex)
	                        	End If
	                        End If
                        End If
                        
                        setSharedData(oForm, "IDH_CUST", sCardCode )
                        setSharedData(oForm, "IDH_ADDR", sSiteAddress )
                        setSharedData(oForm, "IDH_ORDNO", sJobNumber )
                        setSharedData(oForm, "IDH_ROWNO", sRowNumber )
                        setSharedData(oForm, "IDH_STEID", sSiteId )
                        setSharedData(oForm, "IDH_REQSTF", sReqDate )
                        setSharedData(oForm, "IDH_REQSTT", sReqDate )
                        
                        Dim sOSM As String = Config.ParameterWithDefault("SCOSM", "OSM")
                        If sOSM.Equals("OSM") Then
                        	sOSM = "IDHJOBR"
                        ElseIf sOSM.Equals("OSM2") Then
                        	sOSM = "IDHJOBR2"
                        ElseIf sOSM.Equals("OSM3") Then
							sOSM = "IDHJOBR3"
                        ElseIf sOSM.Equals("OSM4") Then
                            sOSM = "IDHJOBR4"
                        ElseIf sOSM.Equals("OSM5") Then
                            sOSM = "IDHJOBR5"
                        ElseIf sOSM.Equals("OSM6") Then
                            sOSM = "IDHJOBR6"
                        ElseIf sOSM.Equals("OSM7") Then
                            sOSM = "IDHJOBR7"
                        ElseIf sOSM.Equals("OSM8") Then
                            sOSM = "IDHJOBR8"

                        End If
                        setSharedData(oForm, "FTYPE", "SEARCH" )
                        goParent.doOpenModalForm(sOSM, oForm)
                    ElseIf pVal.ItemUID = "IDH_RETAB" Then
                        oForm.PaneLevel = 20
                    ElseIf pVal.ItemUID = "IDH_PRICES" Then
                        If oForm.Mode = BoFormMode.fm_OK_MODE OrElse oForm.Mode = BoFormMode.fm_UPDATE_MODE Then
                            doLoadPrices(oForm)
                        Else

                    End If
                End If
            End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_RESIZE Then
                If pVal.BeforeAction = False Then
                    oForm.Freeze(True)
                    Dim oFolderItem As SAPbouiCOM.Item
                    oFolderItem = oForm.Items.Item(msTabAnchor)

                    oForm.Items.Item("IDHMAT").Top = oFolderItem.Top + 25
                    oForm.Items.Item("IDHEWASCOD").Top = oFolderItem.Top + 40
                    oForm.Items.Item("IDHWASCD").Top = oFolderItem.Top + 40
                    oForm.Items.Item("IDHEWSDSC").Top = oFolderItem.Top + 55
                    oForm.Items.Item("IDHWSDSC").Top = oFolderItem.Top + 55
                    oForm.Items.Item("IDHEWSDESC").Top = oFolderItem.Top + 70
                    oForm.Items.Item("IDHWSDESC").Top = oFolderItem.Top + 70
                    oForm.Items.Item("IDHECONT").Top = oFolderItem.Top + 85
                    oForm.Items.Item("IDHCONT").Top = oFolderItem.Top + 85
                    oForm.Items.Item("IDHBP").Top = oFolderItem.Top + 100
                    oForm.Items.Item("IDHEBPCOD").Top = oFolderItem.Top + 115
                    oForm.Items.Item("IDHBPCOD").Top = oFolderItem.Top + 115
                    oForm.Items.Item("IDHESUBCON").Top = oFolderItem.Top + 130
                    oForm.Items.Item("IDHSUBCON").Top = oFolderItem.Top + 130

                    oForm.Items.Item("IDHFRE").Top = oFolderItem.Top + 25
                    oForm.Items.Item("IDHEFREQ").Top = oFolderItem.Top + 40
                    oForm.Items.Item("IDHFREQ").Top = oFolderItem.Top + 40
                    oForm.Items.Item("IDHEDAYS").Top = oFolderItem.Top + 55
                    oForm.Items.Item("IDHDAYS").Top = oFolderItem.Top + 55
                    oForm.Items.Item("IDHEOLD").Top = oFolderItem.Top + 70
                    oForm.Items.Item("IDHOLD").Top = oFolderItem.Top + 70
                    oForm.Items.Item("IDHEREDT").Top = oFolderItem.Top + 85
                    oForm.Items.Item("IDHREQDT").Top = oFolderItem.Top + 85

                    oForm.Items.Item("IDHCSTPRC").Top = oFolderItem.Top + 25
                    oForm.Items.Item("IDHEDSPCO").Top = oFolderItem.Top + 40
                    oForm.Items.Item("IDHDSPCST").Top = oFolderItem.Top + 40
                    oForm.Items.Item("IDHEDSPUM").Top = oFolderItem.Top + 55
                    oForm.Items.Item("IDHDSPUOM").Top = oFolderItem.Top + 55
                    oForm.Items.Item("IDHEHOUCO").Top = oFolderItem.Top + 70
                    oForm.Items.Item("IDHHOUCOS").Top = oFolderItem.Top + 70
                    oForm.Items.Item("IDHEHOUUM").Top = oFolderItem.Top + 85
                    oForm.Items.Item("IDHHOUUOM").Top = oFolderItem.Top + 85

                    oForm.Items.Item("IDHADMIN").Top = oFolderItem.Top + 25
                    oForm.Items.Item("IDHECRTBY").Top = oFolderItem.Top + 40
                    oForm.Items.Item("IDHCRTBY").Top = oFolderItem.Top + 40
                    oForm.Items.Item("IDHECRSMWR").Top = oFolderItem.Top + 55
                    oForm.Items.Item("IDHCRSMWR").Top = oFolderItem.Top + 55
                    oForm.Items.Item("IDHEACCRES").Top = oFolderItem.Top + 70
                    oForm.Items.Item("IDHACCRES").Top = oFolderItem.Top + 70
                    oForm.Items.Item("IDHETMRES").Top = oFolderItem.Top + 85
                    oForm.Items.Item("IDHTMRES").Top = oFolderItem.Top + 85
                    oForm.Items.Item("IDHEORTYP").Top = oFolderItem.Top + 100
                    oForm.Items.Item("IDHORTYP").Top = oFolderItem.Top + 100
                    oForm.Items.Item("IDHEPONUM").Top = oFolderItem.Top + 115
                    oForm.Items.Item("IDHPONUM").Top = oFolderItem.Top + 115
                    oForm.Items.Item("IDHREB").Top = oFolderItem.Top + 130

                    oForm.Freeze(False)
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DRAW Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False AndAlso pVal.ItemUID = "49" Then
                    setItemValue(oForm, "IDHFlag", "Y", False)
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_GOT_FOCUS _
             Then

                Console.WriteLine("IN Block doItemEvent--> et_FORM_ACTIVATE " + pVal.EventType.ToString())
                Console.WriteLine("sICode = " + getParentSharedDataAsString(oForm, "WPIT_ITEMCODE"))
                Console.WriteLine("sIName = " + getParentSharedDataAsString(oForm, "WPIT_ITEMNAME"))
                Console.WriteLine("sWPItem = " + getParentSharedDataAsString(oForm, "WPITM"))

                If pVal.BeforeAction = False Then
                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
            	If sModalFormType = "IDHASRCH" Then
            		setItemValue(oForm, "IDHSteId", getSharedData(oForm, "STEID"))
            		setItemValue(oForm, "IDHSAddr", getSharedData(oForm, "ADDRESS"))
					setItemValue(oForm, "IDHTel1", getSharedData(oForm, "TEL1"))
					setItemValue(oForm, "IDHACN", getSharedData(oForm, "CONA"))
            		
                    doSetUpdate(oForm)
                Else If sModalFormType.Equals("IDH_SUBBY") Then
                	setItemValue(oForm, "IDHSubC", getSharedData(oForm, "IDH_CARR"))
                ElseIf sModalFormType.Equals("IDHJOBR") = True OrElse
                     sModalFormType.Equals("IDHJOBR2") = True OrElse
                     sModalFormType.Equals("IDHJOBR3") = True OrElse
                     sModalFormType.Equals("IDHJOBR4") = True OrElse
                     sModalFormType.Equals("IDHJOBR5") = True OrElse
                     sModalFormType.Equals("IDHJOBR6") = True OrElse
                     sModalFormType.Equals("IDHJOBR7") = True OrElse
                     sModalFormType.Equals("IDHJOBR8") = True Then
                    Dim sCJobNumber As String = getDFValue(oForm, "OSCL", "U_IDHJobNr")
                    Dim sOrdNum As String = getSharedData(oForm, "r.U_JobNr")
                    Dim sRowNum As String = getSharedData(oForm, "r.Code")
                    Dim sJobNumber As String = sOrdNum & "." & sRowNum

                    If Not sCJobNumber Is Nothing AndAlso
                     sCJobNumber.Length > 0 AndAlso
                     sCJobNumber.Equals(sJobNumber) = False Then
                        Dim saParams As String() = {sCJobNumber, sJobNumber}
                        If com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("SCRJBNR", saParams) = 2 Then
                            Exit Sub
                        End If
                    End If
                  		
                    Dim sAddress As String = getSharedData(oForm, "e.U_Address")
                    Dim sSiteId As String = getSharedData(oForm, "e.U_SteId")
                    Dim sSiteTel As String = getSharedData(oForm, "e.U_SiteTl")
                    Dim dReqDate As Date = getSharedData(oForm, "r.U_RDate")
                    Dim sReqDate As String = goParent.doDateToStr(dReqDate)
                    Dim sContact As String = getSharedData(oForm, "e.U_Contact")

                    setItemValue(oForm, "IDHJobNr", sJobNumber)
                    setItemValue(oForm, "IDHSAddr", sAddress)
                    setItemValue(oForm, "IDHSteId", sSiteId)
                    setItemValue(oForm, "IDHTel1", sSiteTel)
                    setItemValue(oForm, "IDHJDate", sReqDate)
                    setItemValue(oForm, "IDHACN", sContact)
                    '#MA STart 2017-03-13
                ElseIf sModalFormType = "IDHWISRC" Then
                    setItemValue(oForm, "IDHWASCD", getSharedData(oForm, "ITEMCODE"))
                    setItemValue(oForm, "IDHWSDSC", getSharedData(oForm, "ITEMNAME"))
                ElseIf sModalFormType = "IDHISRC" Then
                    If getSharedData(oForm, "TRG") = "CON" Then
                        If getSharedData(oForm, "ITEMCODE").ToString.Length = 0 Then 'To ignore invalid value
                            setItemValue(oForm, "IDHCONT", getSharedData(oForm, "ITEMCODE"))
                        Else
                            setItemValue(oForm, "IDHCONT", getSharedData(oForm, "ITEMCODE"))
                        End If
                    End If
                ElseIf sModalFormType = "IDHCSRCH" Then
                    Dim sTarget As String = getSharedData(oForm, "TRG")
                    Dim code As String = getSharedData(oForm, "CARDCODE")

                    If Not (code Is Nothing) AndAlso code.Length > 0 Then
                        If sTarget = "SUP" Then
                            setItemValue(oForm, "IDHBPCOD", code)
                        End If
                    End If
                    '#MA End 2017-03-13
                 End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {""})
            End Try
        End Sub
        '#MA Start 2017-03-13
        Protected Overridable Sub doChooseWasteType(ByVal oForm As SAPbouiCOM.Form)
            setSharedData(oForm, "TP", "WC")
            Dim itemcode As String = getItemValue(oForm, "IDHWASCD")
            Dim itemname As String = getItemValue(oForm, "IDHWSDSC")
            setSharedData(oForm, "IDH_ITMCOD", itemcode)
            setSharedData(oForm, "IDH_ITMNAM", itemname)
            setSharedData(oForm, "IDH_GRPCOD", Config.INSTANCE.doWasteMaterialGroup())
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            goParent.doOpenModalForm("IDHWISRC", oForm)
        End Sub

        Protected Overridable Sub doChooseContainer(ByVal oForm As SAPbouiCOM.Form)
            setSharedData(oForm, "TRG", "CON")
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            setSharedData(oForm, "IDH_GRPCOD", 108)

            goParent.doOpenModalForm("IDHISRC", oForm)
        End Sub
        
        Protected Overridable Sub doChooseCustomer(ByVal oForm As SAPbouiCOM.Form)
            setSharedData(oForm, "TRG", "SUP")
            setSharedData(oForm, "IDH_TYPE", "F-S")
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub
        '#MA End 2017-03-13
        Protected Sub doLoadPrices(ByVal oForm As SAPbouiCOM.Form)
            Dim db As SAPbouiCOM.DBDataSource
            db = oForm.DataSources.DBDataSources.Item("OSCL")
            Dim scallid As String = db.GetValue("callID", 0)
            Dim sDocNum As String = db.GetValue("DocNum", 0)
            Dim sStatus As String = db.GetValue("status", 0)
            Dim sSupplierr As String = db.GetValue("U_IDHSubC", 0)
            setSharedData(oForm, "CALLID", scallid)
            setSharedData(oForm, "SERVICEDOCNUM", sDocNum)
            setSharedData(oForm, "SERVICESTATUS", sStatus)
            setSharedData(oForm, "SERVICESupplier", sSupplierr)
            'setSharedData(oForm, "SILENT", "SHOWMULTI")

            If Not String.IsNullOrWhiteSpace(db.GetValue("U_FREQUENCY", 0).ToString()) AndAlso db.GetValue("U_FREQUENCY", 0).ToString().Trim().ToUpper() = "ADHOC" Then
                '--Adhoc------------
                goParent.doOpenModalForm("IDH_SCADHINF", oForm)
            ElseIf Not String.IsNullOrWhiteSpace(db.GetValue("U_FREQUENCY", 0).ToString()) AndAlso Not db.GetValue("U_FREQUENCY", 0).ToString().Trim().ToUpper() = "ADHOC" Then
                '---Scheduled-------------
                If Not String.IsNullOrWhiteSpace(db.GetValue("U_IDHSubC", 0).ToString()) Then
                    Dim sSupplier As String = com.idh.bridge.lookups.Config.INSTANCE.doGetBPName(db.GetValue("U_IDHSubC", 0))
                    If Not String.IsNullOrWhiteSpace(sSupplier) Then
                        goParent.doOpenModalForm("IDH_SCSCHJOB", oForm)
                    Else
                        com.idh.bridge.DataHandler.INSTANCE.doError("Invalid Supplier")
                    End If
                Else
                    com.idh.bridge.DataHandler.INSTANCE.doError("Supplier is missing")
                End If
            ElseIf String.IsNullOrWhiteSpace(db.GetValue("U_FREQUENCY", 0).ToString()) Then
                com.idh.bridge.DataHandler.INSTANCE.doError("AdHoc or Scheduled is not defined in Frequency")
            End If

            setSharedData(oForm, "CALLID", "")
            setSharedData(oForm, "SERVICEDOCNUM", "")
            setSharedData(oForm, "SERVICESTATUS", "")
            setSharedData(oForm, "SERVICESupplier", "")
        End Sub
        Public Overrides Sub doClose()
        End Sub

        Private Sub doHeaderFields(ByVal oForm As SAPbouiCOM.Form)
            Dim osItm As SAPbouiCOM.Item
            Dim iLeft As Integer = -1

            '' THE EXSTRA 2 COLUMNS
            '' FIRST = 100
            '' SECOND = 5 + 80 + 5 + 70
            ''THIRD = 5 + 120
            ''TOTAL = 100 | 5 + | 80 + 5 + 70 | +  5 + | 170 |
            'Adding controls to header
            If IDHAddOns.idh.addon.Base.LAYOUTTYPE = FixedValues.SBOLayout2009PL5 Then
                iLeft = 510 'oForm.Width - 200 - 100 - 5
                'osItm = oForm.Items.Item(msPurchaseCheckBox)
                'iLeft = osItm.Left - 200

                'If iLeft < 0 Then
                '    osItm = oForm.Items.Item(msSalesCheckBox)
                '    iLeft = osItm.Left - 200

                '    If iLeft < 0 Then
                '        osItm = oForm.Items.Item(msPurchaseCheckBox)
                '        iLeft = osItm.Left - 200
                '    End If
                'End If

                'If iLeft > 0 Then
                osItm = oForm.Items.Item(msPurchaseCheckBox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                osItm = oForm.Items.Item(msSalesCheckBox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                osItm = oForm.Items.Item(msInventoryCheckBox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                'End If

                iLeft = iLeft + 100 + 5
            Else
                iLeft = 510

                osItm = oForm.Items.Item(msAssitCheckbox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                osItm = oForm.Items.Item(msPurchaseCheckBox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                osItm = oForm.Items.Item(msSalesCheckBox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                osItm = oForm.Items.Item(msInventoryCheckBox)
                osItm.LinkTo = msHeaderLinkTo
                osItm.Left = iLeft

                iLeft = iLeft + 100 + 5
                'iLeft = osItm.Left + 100 + 5
            End If

            ''osItm = oForm.Items.Item("14")

            'iLeft = osItm.Left + 100 + 5 'osItm.Width  'To move it on second column
            doAddCheck(oForm, "IDH_WPTEMP", 0, iLeft, osItm.Top, 80, osItm.Height, getTranslatedWord("WP Template"), msHeaderLinkTo)
            doAddCombo(oForm, "IDH_WPUSR", 0, iLeft + 85, osItm.Top, 70, osItm.Height, msHeaderLinkTo)
            doFillCombo(oForm, "IDH_WPUSR", "OUSR", "User_Code", "U_Name") ', "", "", "", goParent.gsUserName)
            Dim sUsers As String = Config.INSTANCE.getParameterWithDefault("HIDWPTMP", "")
            If sUsers.Trim <> "" Then
                Dim aUsers() As String = (sUsers.ToUpper.Replace(" ", "")).Split(",")
                If Not aUsers.Contains(DataHandler.INSTANCE.User.ToUpper) Then
                    'setEnableItem(oForm, False, "IDH_WPTEMP")
                    'setEnableItem(oForm, False, "IDH_WPUSR")
                    Dim oitem As SAPbouiCOM.Item = oForm.Items.Item("IDH_WPTEMP")
                    oitem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable,
                            SAPbouiCOM.BoFormMode.fm_ADD_MODE + SAPbouiCOM.BoFormMode.fm_EDIT_MODE +
                            SAPbouiCOM.BoFormMode.fm_OK_MODE + SAPbouiCOM.BoFormMode.fm_PRINT_MODE +
                            SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + SAPbouiCOM.BoFormMode.fm_VIEW_MODE + SAPbouiCOM.BoFormMode.fm_FIND_MODE,
                            SAPbouiCOM.BoModeVisualBehavior.mvb_False)

                    'oitem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, _
                    '       SAPbouiCOM.BoFormMode.fm_OK_MODE + _
                    '       SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + SAPbouiCOM.BoFormMode.fm_VIEW_MODE + SAPbouiCOM.BoFormMode.fm_FIND_MODE, _
                    '       SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                    'oitem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, _
                    '    SAPbouiCOM.BoFormMode.fm_ADD_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

                    oitem = oForm.Items.Item("IDH_WPUSR")

                    oitem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable,
                            SAPbouiCOM.BoFormMode.fm_ADD_MODE + SAPbouiCOM.BoFormMode.fm_EDIT_MODE +
                            SAPbouiCOM.BoFormMode.fm_OK_MODE + SAPbouiCOM.BoFormMode.fm_PRINT_MODE +
                            SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + SAPbouiCOM.BoFormMode.fm_VIEW_MODE + SAPbouiCOM.BoFormMode.fm_FIND_MODE,
                            SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                    'oitem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, _
                    '       SAPbouiCOM.BoFormMode.fm_ADD_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                    'oitem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, _
                    '       SAPbouiCOM.BoFormMode.fm_EDIT_MODE + _
                    '       SAPbouiCOM.BoFormMode.fm_OK_MODE + SAPbouiCOM.BoFormMode.fm_PRINT_MODE + _
                    '       SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + SAPbouiCOM.BoFormMode.fm_VIEW_MODE + SAPbouiCOM.BoFormMode.fm_FIND_MODE, _
                    '       SAPbouiCOM.BoModeVisualBehavior.mvb_True)

                End If
                'For i As Int16 = 0 To aUsers.Length - 1
                '    If DataHandler.INSTANCE.User.ToLower().Equals(aUsers(i).Trim.ToLower) Then
                '    End If
                'Next
            End If
            doAddCheck(oForm, "IDH_APROVE", 0, iLeft, osItm.Top + (osItm.Height * 1) + 2, 80, osItm.Height, getTranslatedWord("Approved"), msHeaderLinkTo)
            doAddCombo(oForm, "IDH_APUSR", 0, iLeft + 85, osItm.Top + (osItm.Height * 1) + 2, 70, osItm.Height, msHeaderLinkTo)
            doFillCombo(oForm, "IDH_APUSR", "OUSR", "User_Code", "U_Name") ', "", "", "", goParent.gsUserName)

            doAddCheck(oForm, "IDH_LETOFI", 0, iLeft, osItm.Top + (osItm.Height * 2) + 4, 80, osItm.Height, getTranslatedWord("Letter on File"), msHeaderLinkTo)
            doAddCombo(oForm, "IDH_LTUSR", 0, iLeft + 85, osItm.Top + (osItm.Height * 2) + 4, 70, osItm.Height, msHeaderLinkTo)
            doFillCombo(oForm, "IDH_LTUSR", "OUSR", "User_Code", "U_Name") ', "", "", "", goParent.gsUserName)

            doAddCheck(oForm, "IDH_HAZORD", 0, iLeft, osItm.Top + (osItm.Height * 3) + 6, 80, osItm.Height, getTranslatedWord("Hazardous"), msHeaderLinkTo)
            doAddCheck(oForm, "IDH_SMPREQ", 0, iLeft, osItm.Top + (osItm.Height * 3) + 8, 100, osItm.Height, getTranslatedWord("Sample Required"), msHeaderLinkTo)

            iLeft = iLeft + 80 + 5 + 70 + 5 'To move it on third column
            doAddCheck(oForm, "IDHCWBB", 0, iLeft, osItm.Top, 170, osItm.Height, getTranslatedWord("Purchase Over Weighbridge"), msHeaderLinkTo)
            doAddCheck(oForm, "IDH_REB", 0, iLeft, osItm.Top + (osItm.Height * 1) + 2, 170, osItm.Height, getTranslatedWord("Rebate Item"), msHeaderLinkTo)
            doAddCheck(oForm, "IDH_CREB", 0, iLeft, osItm.Top + (osItm.Height * 2) + 4, 170, osItm.Height, getTranslatedWord("Carrier Rebate Item"), msHeaderLinkTo)

        End Sub

        Private Sub doCreateWasteProfile(ByVal oForm As SAPbouiCOM.Form)
            Dim iTop As Integer = -1
            Dim iWidth As Integer = -1
            Dim iHeight As Integer = -1
            Dim iFrame As Integer = 0

            Dim sWPTABVIS As String = Config.ParameterWithDefault("WPTABVIS", "1,1,1,1,1,1,1,1")
            Dim sFlagArray() As String = sWPTABVIS.Split(","c)

            Dim oFolderItem As SAPbouiCOM.Item
            oFolderItem = oForm.Items.Item(msTabAnchor)

            doAddStatic(oForm, "IDHMAT", 20, 11, oFolderItem.Top + 25, 90, 14, getTranslatedWord("Material / Container"), Nothing)
            doAddStatic(oForm, "IDHEWASCOD", 20, 11, oFolderItem.Top + 40, 90, 14, getTranslatedWord("Waste Code"), Nothing)
            doAddDBEdit(oForm, "IDHWASCD", "OSCL", "U_WasCd", 20, 107, oFolderItem.Top + 40, 80, 14, Nothing)
            'doAddLookupButton(oForm, "LKUPWASCD", 10, 187, 241, "IDHEWASCOD")
            doAddStatic(oForm, "IDHEWSDSC", 20, 11, oFolderItem.Top + 55, 90, 14, getTranslatedWord("Waste Description"), Nothing)
            doAddDBEdit(oForm, "IDHWSDSC", "OSCL", "U_WasDsc", 20, 107, oFolderItem.Top + 55, 80, 14, Nothing)
            doAddStatic(oForm, "IDHEWSDESC", 20, 11, oFolderItem.Top + 70, 90, 14, getTranslatedWord("Waste Description"), Nothing)
            doAddDBEdit(oForm, "IDHWSDESC", "OSCL", "U_WASTEDESC", 20, 107, oFolderItem.Top + 70, 80, 14, Nothing)
            doAddStatic(oForm, "IDHECONT", 20, 11, oFolderItem.Top + 85, 90, 14, getTranslatedWord("Container"), Nothing)
            doAddDBEdit(oForm, "IDHCONT", "OSCL", "U_CONTAINER", 20, 107, oFolderItem.Top + 85, 80, 14, Nothing)
            'doAddLookupButton(oForm, "LKUPCONT", 10, 187, 286, "IDHCONT")
            doAddStatic(oForm, "IDHBP", 20, 11, oFolderItem.Top + 100, 90, 14, getTranslatedWord("Business Partners"), Nothing)
            doAddStatic(oForm, "IDHEBPCOD", 20, 11, oFolderItem.Top + 115, 90, 14, getTranslatedWord("Disposal Supplier"), Nothing)
            doAddDBEdit(oForm, "IDHBPCOD", "OSCL", "U_DISPSUPP", 20, 107, oFolderItem.Top + 115, 80, 14, "IDHEBPCOD")
            'doAddLookupButton(oForm, "LKUPDSPSUP", 10, 187, 316, "IDHBPCOD")
            doAddStatic(oForm, "IDHESUBCON", 20, 11, oFolderItem.Top + 130, 95, 14, getTranslatedWord("Subcontractor Name"), Nothing)
            doAddDBEdit(oForm, "IDHSUBCON", "OSCL", "U_IDHSubN", 20, 107, oFolderItem.Top + 130, 80, 14, "IDHESUBCON")
            doAddStatic(oForm, "IDHFRE", 20, 205, oFolderItem.Top + 25, 90, 14, getTranslatedWord("Frequencies"), Nothing)
            doAddStatic(oForm, "IDHEFREQ", 20, 205, oFolderItem.Top + 40, 90, 14, getTranslatedWord("Frequency"), "IDHFREQ")
            doAddDBEdit(oForm, "IDHFREQ", "OSCL", "U_FREQUENCY", 20, 300, oFolderItem.Top + 40, 80, 14, Nothing)
            doAddStatic(oForm, "IDHEDAYS", 20, 205, oFolderItem.Top + 55, 90, 14, getTranslatedWord("Days"), "IDHDAYS")
            doAddDBEdit(oForm, "IDHDAYS", "OSCL", "U_DAYS", 20, 300, oFolderItem.Top + 55, 80, 14, "IDHEDAYS")
            doAddStatic(oForm, "IDHEOLD", 20, 205, oFolderItem.Top + 70, 90, 14, getTranslatedWord("Original Log Date"), "IDHFRE")
            doAddDBEdit(oForm, "IDHOLD", "OSCL", "U_LOGDATE", 20, 300, oFolderItem.Top + 70, 80, 14, "IDHEOLD")
            doAddStatic(oForm, "IDHEREDT", 20, 205, oFolderItem.Top + 85, 90, 14, getTranslatedWord("Requested Date"), "IDHFRE")
            doAddDBEdit(oForm, "IDHREQDT", "OSCL", "U_REQDATE", 20, 300, oFolderItem.Top + 85, 80, 14, "IDHEREDT")
            doAddStatic(oForm, "IDHCSTPRC", 20, 385, oFolderItem.Top + 25, 80, 14, getTranslatedWord("Cost Prices"), "IDHEREDT")
            doAddStatic(oForm, "IDHEDSPCO", 20, 385, oFolderItem.Top + 40, 80, 14, getTranslatedWord("Disposal Cost"), "IDHFRE")
            doAddDBEdit(oForm, "IDHDSPCST", "OSCL", "U_DISPCOST", 20, 468, oFolderItem.Top + 40, 80, 14, "IDHEFREQ")
            doAddStatic(oForm, "IDHEDSPUM", 20, 385, oFolderItem.Top + 55, 80, 14, getTranslatedWord("Disposal UOM"), "IDHFRE")
            doAddDBEdit(oForm, "IDHDSPUOM", "OSCL", "U_DISPUOM", 20, 468, oFolderItem.Top + 55, 80, 14, "IDHEFREQ")
            'doAddCombo(oForm, "IDHDSPUOM", 10, 468, 256, 80, 14, "IDHFRE")
            'doFillCombo(oForm, "IDHDSPUOM", "OWGT", "UnitDisply", "UnitName")
            doAddStatic(oForm, "IDHEHOUCO", 20, 385, oFolderItem.Top + 70, 80, 14, getTranslatedWord("Haulage Cost"), "IDHFRE")
            doAddDBEdit(oForm, "IDHHOUCOS", "OSCL", "U_HAULCOST", 20, 468, oFolderItem.Top + 70, 80, 14, "IDHEOLD")
            doAddStatic(oForm, "IDHEHOUUM", 20, 385, oFolderItem.Top + 85, 80, 14, getTranslatedWord("Haulage UOM"), "IDHFRE")
            doAddDBEdit(oForm, "IDHHOUUOM", "OSCL", "U_HAULUOM", 20, 468, oFolderItem.Top + 85, 80, 14, "IDHEOLD")
            'doAddCombo(oForm, "IDHHOUUOM", 10, 468, 286, 80, 14, "IDHFRE")
            'doFillCombo(oForm, "IDHHOUUOM", "OWGT", "UnitDisply", "UnitName")
            doAddStatic(oForm, "IDHADMIN", 20, 575, oFolderItem.Top + 25, 120, 14, getTranslatedWord("Administration"), "IDHEREDT")
            doAddStatic(oForm, "IDHECRTBY", 20, 575, oFolderItem.Top + 40, 120, 14, getTranslatedWord("Created By"), "IDHFRE")
            doAddDBEdit(oForm, "IDHCRTBY", "OSCL", "U_CreatedBy", 20, 690, oFolderItem.Top + 40, 80, 14, "IDHEFREQ")
            'doAddLookupButton(oForm, "LKUPCRTBY", 10, 770, 241, "IDHCRTBY")
            doAddStatic(oForm, "IDHECRSMWR", 20, 575, oFolderItem.Top + 55, 120, 14, getTranslatedWord("Created By SmartWorld"), "IDHFRE")
            doAddDBEdit(oForm, "IDHCRSMWR", "OSCL", "U_IDHOrigin", 20, 690, oFolderItem.Top + 55, 80, 14, "IDHEDAYS")
            doAddStatic(oForm, "IDHEACCRES", 20, 575, oFolderItem.Top + 70, 120, 14, getTranslatedWord("Access Restrictions"), "IDHFRE")
            doAddDBEdit(oForm, "IDHACCRES", "OSCL", "U_ACCRESTR", 20, 690, oFolderItem.Top + 70, 80, 14, "IDHEOLD")
            doAddStatic(oForm, "IDHETMRES", 20, 575, oFolderItem.Top + 85, 120, 14, getTranslatedWord("Time Restrictions"), "IDHFRE")
            doAddDBEdit(oForm, "IDHTMRES", "OSCL", "U_TIMERESTR", 20, 690, oFolderItem.Top + 85, 80, 14, "IDHEREDT")
            doAddStatic(oForm, "IDHEORTYP", 20, 575, oFolderItem.Top + 100, 120, 14, getTranslatedWord("Order Type"), "IDHFRE")
            doAddDBEdit(oForm, "IDHORTYP", "OSCL", "U_ORDERTYPE", 20, 690, oFolderItem.Top + 100, 80, 14, "IDHEDAYS")
            'doAddLookupButton(oForm, "LKUPORTYP", 10, 770, 301, "IDHORTYP")
            doAddStatic(oForm, "IDHEPONUM", 20, 575, oFolderItem.Top + 115, 120, 14, getTranslatedWord("PO Number"), "IDHFRE")
            doAddDBEdit(oForm, "IDHPONUM", "OSCL", "U_PONUM", 20, 690, oFolderItem.Top + 115, 80, 14, "IDHEOLD")
            'doAddLookupButton(oForm, "LKUPPONUM", 10, 770, 316, "IDHPONUM")
            doAddCheck(oForm, "IDHREB", 20, 690, oFolderItem.Top + 130, 80, 14, "Rebate", "IDHFRE")
            doAddDBCheck(oForm, "OSCL", "IDHREB", "U_REBATE")
            doAddDF(oForm, "OSCL", "IDHDSPUOM", "U_DISPUOM")
            doAddDF(oForm, "OSCL", "IDHHOUUOM", "U_HAULUOM")

            doAddWF(oForm, "IDH_WPFLAG", Nothing)

        End Sub
	End Class
End Namespace
