
Imports System.Collections
Imports System.IO
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge
Imports com.idh.bridge.lookups

Namespace idh.forms.SBO
    Public Class Activity
        Inherits IDHAddOns.idh.forms.Base

        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "651", -1, Nothing)
            gsSystemMenuID = "2563"

            'doAddImagesToFix("IDH_WOHLK")
            'doAddImagesToFix("IDH_WORLK")
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)

            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD)

        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        Public Overrides Sub doLoadForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                doActivityForm(oForm)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString, "doLoadForm()")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDLF", {Nothing})
            End Try
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sEvtItem As String = pVal.ItemUID
            Try
                If (pVal.EventType = SAPbouiCOM.BoEventTypes.et_GOT_FOCUS OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE) AndAlso getWFValue(oForm, "SETENQIDs") = "Y" Then
                    Dim bUpdateMode As Boolean = False
                    If getWFValue(oForm, "CardCode").ToString <> "" Then
                        Dim oEdit As SAPbouiCOM.EditText
                        oEdit = oForm.Items.Item("9").Specific
                        If (oEdit.Value.ToString().Trim() <> getWFValue(oForm, "CardCode").ToString().Trim()) Then
                            oEdit.Value = getWFValue(oForm, "CardCode")
                            bUpdateMode = True

                        End If
                        If getWFValue(oForm, "Personal").ToString = "N" Then
                            Dim oCheckbox As SAPbouiCOM.CheckBox = oForm.Items.Item("123").Specific
                            If oCheckbox.Checked Then
                                oCheckbox.Checked = False
                            End If
                        End If
                    ElseIf getWFValue(oForm, "Personal").ToString = "Y" Then
                        bUpdateMode = True
                        Dim oCheckbox As SAPbouiCOM.CheckBox = oForm.Items.Item("123").Specific
                        oCheckbox.Checked = True
                    End If
                    If getWFValue(oForm, "SelectEnquiry").ToString <> "" Then
                        Dim oCombo As SAPbouiCOM.ComboBox = oForm.Items.Item("68").Specific
                        Try
                            bUpdateMode = True
                            oCombo.SelectExclusive(Translation.getTranslatedWord("Enquiry"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Catch ex As Exception

                        End Try
                    End If
                    If getWFValue(oForm, "Remarks").ToString <> "" Then
                        Dim oEdit As SAPbouiCOM.EditText
                        oEdit = oForm.Items.Item("53").Specific
                        Dim iCuurentPan As Int16 = oForm.PaneLevel
                        Try
                            If oForm.PaneLevel <> oForm.Items.Item("53").FromPane Then
                                oForm.PaneLevel = oForm.Items.Item("53").FromPane
                            End If
                            If getWFValue(oForm, "Remarks").ToString.Trim.Length <= 100 Then
                                oEdit.Value = getWFValue(oForm, "Remarks").ToString.Trim
                            Else
                                oEdit.Value = getWFValue(oForm, "Remarks").ToString.Trim.Substring(0, 97) & "..."
                            End If
                        Catch ex As Exception
                        Finally
                            oForm.PaneLevel = iCuurentPan
                            oForm.Freeze(False)
                        End Try


                        iCuurentPan = oForm.PaneLevel
                        Try
                            oForm.Freeze(True)
                            oEdit = oForm.Items.Item("25").Specific
                            If oForm.PaneLevel <> oForm.Items.Item("25").FromPane Then
                                oForm.PaneLevel = oForm.Items.Item("25").FromPane
                            End If
                            oEdit.Value = getWFValue(oForm, "Remarks").ToString.Trim
                        Catch ex As Exception
                        Finally
                            oForm.PaneLevel = iCuurentPan
                            oForm.Freeze(False)
                        End Try


                    End If
                    setWFValue(oForm, "SETENQIDs", "")
                    setWFValue(oForm, "CardCode", "")
                    setWFValue(oForm, "Personal", "")
                    setWFValue(oForm, "SelectEnquiry", "")
                    setWFValue(oForm, "Remarks", "")

                    If bUpdateMode = True AndAlso oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                    End If
                ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                    If pVal.ItemUID = "IDH_WOHLK" Then
                        Dim sOrd As String = getDFValue(oForm, "OCLG", "U_IDHWO")
                        Dim sRow As String = getDFValue(oForm, "OCLG", "U_IDHWOR")
                        If sOrd.Length > 0 Then
                            Dim oData As New ArrayList
                            oData.Add("DoUpdate")
                            oData.Add(sOrd)
                            goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
                        End If
                    ElseIf pVal.ItemUID = "IDH_WORLK" Then
                        Dim sOrd As String = getDFValue(oForm, "OCLG", "U_IDHWO")
                        Dim sRow As String = getDFValue(oForm, "OCLG", "U_IDHWOR")
                        If sRow.Length > 0 Then
                            'Dim oData As New ArrayList
                            'oData.Add("DoUpdate")
                            'oData.Add(sRow)
                            'setSharedData(oForm, "ROWCODE", sRow)
                            'goParent.doOpenModalForm("IDHJOBS", oForm, oData)

                            Try
                                Dim oOrderRow As com.isb.core.forms.OrderRow = New com.isb.core.forms.OrderRow(oForm.UniqueID, sRow)
                                'oOrderRow.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                                'oOrderRow.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                                oOrderRow.doShowModal()
                            Catch Ex As Exception
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                            End Try
                        End If
                    ElseIf pVal.ItemUID = "IDH_DOHLK" Then
                        Dim sOrd As String = getDFValue(oForm, "OCLG", "U_IDHDO")
                        Dim sRow As String = getDFValue(oForm, "OCLG", "U_IDHDOR")
                        If sOrd.Length > 0 Then
                            Dim oData As New ArrayList
                            oData.Add("DoUpdate")
                            oData.Add(sOrd)
                            setSharedData(oForm, "ROWCODE", sRow)
                            goParent.doOpenModalForm("IDH_DISPORD", oForm, oData)
                        End If
                    ElseIf pVal.ItemUID = "IDH_DORLK" Then
                        Dim sOrd As String = getDFValue(oForm, "OCLG", "U_IDHDO")
                        Dim sRow As String = getDFValue(oForm, "OCLG", "U_IDHDOR")
                        If sRow.Length > 0 Then
                            Dim oData As New ArrayList
                            oData.Add("DoUpdate")
                            oData.Add(sOrd)
                            setSharedData(oForm, "ROWCODE", sRow)
                            goParent.doOpenModalForm("IDH_DISPORD", oForm, oData)
                        End If
                    ElseIf pVal.ItemUID = "IDH_EVNLK" Then
                        Dim sEVNNum As String = getDFValue(oForm, "OCLG", "U_IDHEVNID") 'getFormDFValue(oForm, "U_IDHEVNID")

                        If sEVNNum IsNot Nothing AndAlso sEVNNum.Length > 0 Then
                            'Open the EVN form 
                            setSharedData(oForm, "EVNNUM", sEVNNum)
                            goParent.doOpenForm("IDHEVNMAS", oForm, False)
                        End If
                        'ElseIf pVal.BeforeAction = False AndAlso ((pVal.ItemUID = "IDH_ENQIRY") OrElse
                        '    (pVal.ItemUID = "IDH_ENQLK" AndAlso getDFValue(oForm, "OCLG", "U_IDHEnqID").ToString.Trim <> "")) Then
                        '    setWFValue(oForm, "CardCode", "")
                        '    setWFValue(oForm, "Personal", "")
                        '    setWFValue(oForm, "SelectEnquiry", "")
                        '    setWFValue(oForm, "Remarks", "")

                        '    Dim sEnqID As String = getDFValue(oForm, "OCLG", "U_IDHEnqID")
                        '    Dim oOOForm As com.isb.forms.Enquiry.Enquiry = New com.isb.forms.Enquiry.Enquiry(Nothing, oForm.UniqueID, Nothing)
                        '    oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandleEnquiryOKReturn)
                        '    oOOForm.ActivityID = getDFValue(oForm, "OCLG", "ClgCode")
                        '    If sEnqID IsNot Nothing AndAlso sEnqID.Trim <> "" AndAlso sEnqID <> "0" Then
                        '        oOOForm.EnquiryID = sEnqID
                        '        oOOForm.bLoadEnquiry = True
                        '    Else
                        '        oOOForm.CardCode = getDFValue(oForm, "OCLG", "CardCode")
                        '        oOOForm.CardName = getDFValue(oForm, "OCRD", "CardName")
                        '        '   dsffvv()
                        '        If (Not String.IsNullOrEmpty(getDFValue(oForm, "OCLG", "AttendUser"))) Then
                        '            Dim sUserID As String = Config.INSTANCE.doGetUserCode(getDFValue(oForm, "OCLG", "AttendUser").ToString())
                        '            oOOForm.AttendUser = sUserID
                        '        End If
                        '        oOOForm.Recontact = utils.Dates.doStrToDate(getDFValue(oForm, "OCLG", "Recontact"))
                        '        If Not String.IsNullOrEmpty(getDFValue(oForm, "OCLG", "CntctCode")) Then
                        '            Dim sContactName As Object = Config.INSTANCE.doLookupTableField("OCPR", "[Name]", "CardCode='" & getDFValue(oForm, "OCLG", "CardCode") & "' And [CntctCode] = '" + getDFValue(oForm, "OCLG", "CntctCode") + "'")
                        '            If Not String.IsNullOrEmpty(sContactName) Then
                        '                oOOForm.ContactName = sContactName.ToString()
                        '            End If
                        '        End If
                        '        oOOForm.ContactPhone = getDFValue(oForm, "OCLG", "Tel")
                        '        oOOForm.Notes = getDFValue(oForm, "OCLG", "Notes")
                        '    End If
                        '    oOOForm.bFromActivity = True
                        '    oOOForm.doShowModal(oForm)
                        '    oOOForm.DBEnquiry.SBOForm = oOOForm.SBOForm
                        'ElseIf pVal.BeforeAction = False AndAlso ((pVal.ItemUID = "IDH_WOQLK" AndAlso getDFValue(oForm, "OCLG", "U_IDHWOQID").ToString.Trim <> "")) Then
                        '    setWFValue(oForm, "CardCode", "")
                        '    setWFValue(oForm, "Personal", "")
                        '    setWFValue(oForm, "SelectEnquiry", "")
                        '    setWFValue(oForm, "Remarks", "")

                        '    Dim sWOQID As String = getDFValue(oForm, "OCLG", "U_IDHWOQID")
                        '    Dim oOOForm As com.isb.forms.Enquiry.WOQuote = New com.isb.forms.Enquiry.WOQuote(Nothing, oForm.UniqueID, Nothing)
                        '    'oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandleWOQOKReturn)
                        '    oOOForm.Handler_DialogButton1Return = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandleWOQOKReturn)
                        '    'oOOForm.ActivityID = getDFValue(oForm, "OCLG", "ClgCode")
                        '    If sWOQID IsNot Nothing AndAlso sWOQID.Trim <> "" AndAlso sWOQID <> "0" Then
                        '        oOOForm.WOQID = sWOQID
                        '        oOOForm.bLoadWOQ = True
                        '    End If
                        '    '    oOOForm.bFromActivity = True
                        '    oOOForm.doShowModal(oForm)
                        '    oOOForm.DBWOQ.SBOForm = oOOForm.SBOForm
                        '    'ElseIf pVal.ItemUID = "IDH_ENQLK" AndAlso pVal.BeforeAction = False AndAlso getFormDFValue(oForm, "U_IDHEnqID").ToString.Trim <> "" Then
                        '    '    setSharedData(oForm, "CardCode", "")
                        '    '    setSharedData(oForm, "Personal", "")
                        '    '    setSharedData(oForm, "SelectEnquiry", "")
                        '    '    setSharedData(oForm, "Remarks", "")

                        '    '    Dim sEnqID As String = getDFValue(oForm, "OCLG", "U_IDHEnqID")
                        '    '    'If sEnqID.Trim.Length > 0 AndAlso sEnqID.Trim <> "0" Then
                        '    '    Dim oOOForm As com.isb.forms.Enquiry.Enquiry = New com.isb.forms.Enquiry.Enquiry(Nothing, oForm.UniqueID, Nothing)
                        '    '    oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandleEnquiryOKReturn)
                        '    '    'oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandleEnquiryCancelReturn)
                        '    '    oOOForm.ActivityID = getDFValue(oForm, "OCLG", "ClgCode")
                        '    '    If sEnqID IsNot Nothing AndAlso sEnqID.Trim <> "" AndAlso sEnqID <> "0" Then
                        '    '        oOOForm.EnquiryID = sEnqID
                        '    '        oOOForm.bLoadEnquiry = True
                        '    '    End If
                        '    '    oOOForm.bFromActivity = True
                        '    '    oOOForm.doShowModal(oForm)
                        '    '    oOOForm.DBEnquiry.SBOForm = oOOForm.SBOForm
                    End If
                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXITME", {pVal.ItemUID, pVal.EventType})
            End Try
            Return True
        End Function

        'Public Function doHandleEnquiryOKReturn(ByVal oDialogForm As com.idh.forms.oo.Form) As Boolean
        '    Dim oOOForm As com.isb.forms.Enquiry.Enquiry = oDialogForm
        '    If oOOForm.bLoadEnquiry = False AndAlso oOOForm.bAddUpdateOk Then
        '        Dim oForm As SAPbouiCOM.Form = oOOForm.SBOParentForm
        '        Dim oEdit As SAPbouiCOM.EditText
        '        oEdit = oForm.Items.Item("IDH_EnqID").Specific
        '        oEdit.Value = oOOForm.EnquiryID
        '        setWFValue(oForm, "SETENQIDs", "Y")
        '        setWFValue(oForm, "CardCode", "")
        '        setWFValue(oForm, "Personal", "")
        '        setWFValue(oForm, "SelectEnquiry", "")
        '        setWFValue(oForm, "Remarks", oOOForm.DBEnquiry.U_Notes)
        '        If (oOOForm.DBEnquiry.U_CardCode = "") Then
        '            setWFValue(oForm, "Personal", "Y")
        '        Else
        '            setWFValue(oForm, "CardCode", oOOForm.DBEnquiry.U_CardCode)
        '        End If
        '        setWFValue(oForm, "SelectEnquiry", "Y")
        '        Try
        '            oForm.Items.Item("9").Click()
        '        Catch ex As Exception
        '        End Try
        '    ElseIf oOOForm.bLoadEnquiry AndAlso oOOForm.bAddUpdateOk Then

        '        Dim oForm As SAPbouiCOM.Form = oOOForm.SBOParentForm
        '        setWFValue(oForm, "SETENQIDs", "")
        '        setWFValue(oForm, "CardCode", "")
        '        setWFValue(oForm, "Personal", "")
        '        setWFValue(oForm, "SelectEnquiry", "")
        '        setWFValue(oForm, "Remarks", oOOForm.DBEnquiry.U_Notes)

        '        Dim oEdit As SAPbouiCOM.EditText
        '        oEdit = oForm.Items.Item("9").Specific
        '        Dim sCardCode As String = oEdit.Value
        '        ' If oOOForm.DBEnquiry.U_CardCode.Trim <> "" AndAlso sCardCode = "" Then
        '        setWFValue(oForm, "SETENQIDs", "Y")
        '        setWFValue(oForm, "CardCode", oOOForm.DBEnquiry.U_CardCode)
        '        setWFValue(oForm, "Personal", "N")
        '        'End If
        '        Try
        '            oForm.Items.Item("9").Click()
        '        Catch ex As Exception
        '        End Try
        '    End If

        '    Return True
        'End Function

        'Public Function doHandleWOQOKReturn(ByVal oDialogForm As com.idh.forms.oo.Form) As Boolean
        '    Dim oOOForm As com.isb.forms.Enquiry.WOQuote = oDialogForm
        '    If oOOForm.bLoadWOQ = False AndAlso oOOForm.bAddUpdateOk Then
        '        Dim oForm As SAPbouiCOM.Form = oOOForm.SBOParentForm
        '        Dim oEdit As SAPbouiCOM.EditText
        '        oEdit = oForm.Items.Item("IDH_WOQID").Specific
        '        oEdit.Value = oOOForm.EnquiryID
        '        setWFValue(oForm, "SETENQIDs", "Y")
        '        setWFValue(oForm, "CardCode", "")
        '        setWFValue(oForm, "Personal", "")
        '        setWFValue(oForm, "SelectEnquiry", "")
        '        setWFValue(oForm, "Remarks", oOOForm.DBWOQ.U_Notes)
        '        If (oOOForm.DBWOQ.U_CardCd = "") Then
        '            setWFValue(oForm, "Personal", "Y")
        '        Else
        '            setWFValue(oForm, "CardCode", oOOForm.DBWOQ.U_CardCd)
        '        End If
        '        setWFValue(oForm, "SelectEnquiry", "Y")
        '        Try
        '            oForm.Items.Item("9").Click()
        '        Catch ex As Exception
        '        End Try
        '    ElseIf oOOForm.bLoadWOQ AndAlso oOOForm.bAddUpdateOk Then

        '        Dim oForm As SAPbouiCOM.Form = oOOForm.SBOParentForm
        '        setWFValue(oForm, "SETENQIDs", "")
        '        setWFValue(oForm, "CardCode", "")
        '        setWFValue(oForm, "Personal", "")
        '        setWFValue(oForm, "SelectEnquiry", "")
        '        setWFValue(oForm, "Remarks", oOOForm.DBWOQ.U_Notes)

        '        Dim oEdit As SAPbouiCOM.EditText
        '        oEdit = oForm.Items.Item("9").Specific
        '        Dim sCardCode As String = oEdit.Value
        '        ' If oOOForm.DBEnquiry.U_CardCode.Trim <> "" AndAlso sCardCode = "" Then
        '        setWFValue(oForm, "SETENQIDs", "Y")
        '        setWFValue(oForm, "CardCode", oOOForm.DBWOQ.U_CardCd)
        '        setWFValue(oForm, "Personal", "N")
        '        Try
        '            oForm.Items.Item("9").Click()
        '        Catch ex As Exception
        '        End Try
        '        'End If
        '    End If
        '    Return True
        'End Function

        'Public Sub HandleFormDataEvent(ByVal oForm As SAPbouiCOM.Form, ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        '    'Try
        '    '    If BusinessObjectInfo.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD AndAlso BusinessObjectInfo.ActionSuccess Then
        '    '        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE Then
        '    '            oForm.Items.Item("IDH_ENQIRY").Enabled = True
        '    '        End If
        '    '    End If
        '    'Catch ex As Exception
        '    '    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHFE", {Nothing})
        '    'End Try
        'End Sub
        ''** The Menu Event handler
        ''** Return True if the Event must be handled by the other Objects
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            'If pVal.MenuUID = "1281" AndAlso Not pVal.BeforeAction Then
            '    oForm.Items.Item("IDH_ENQIRY").Enabled = False
            'End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Public Overrides Sub doClose()
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = False Then
                Try
                    Dim isWODraftActivity As Boolean
                    If getParentSharedData(oForm, "WODRFTACTY") IsNot Nothing AndAlso getParentSharedData(oForm, "WODRFTACTY").ToString().Length > 0 Then
                        isWODraftActivity = getParentSharedData(oForm, "WODRFTACTY")
                        If isWODraftActivity Then
                            oForm.Close()
                        End If
                    End If
                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.ActionSuccess = True Then
                        Dim iClgCode As Integer = getSharedData(oForm, "ACTVTYCD")
                        If iClgCode > 0 Then
                            'setSharedData(oForm, "ACTVTYCD", iClgCode)
                            setParentSharedData(oForm, "ACTVTYCD", iClgCode)
                        Else
                            setParentSharedData(oForm, "ACTVTYCD", "-1")
                        End If
                        doReturnFromModalShared(oForm, True)
                    End If
                Catch ex As Exception
                End Try
            Else
                'Prepare when opened from EVN Master form and to send Activity Code back 
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    If getParentSharedData(oForm, "IDH_EVN") IsNot Nothing AndAlso getParentSharedData(oForm, "IDH_EVN").ToString() <> String.Empty Then
                        Dim iClgCode As Integer = getDFValue(oForm, "OCLG", "ClgCode")
                        setSharedData(oForm, "ACTVTYCD", iClgCode)
                    End If
                End If
            End If
        End Sub

        Public Sub doActivityForm(ByVal oForm As SAPbouiCOM.Form)
            Dim sFMode As String = getParentSharedData(oForm, "sFMode")

            Dim owItemLabel As SAPbouiCOM.Item
            Dim owItem As SAPbouiCOM.Item

            Dim owItemControl As SAPbouiCOM.Item
            Dim oEdit As SAPbouiCOM.EditText
            Dim oBTNLink As SAPbouiCOM.Item
            Dim oMastLink As SAPbouiCOM.Button

            Dim oCmb As SAPbouiCOM.ComboBox

            'Dim iHeight As Integer

            'WO Header Edit 
            owItemControl = oForm.Items.Item("148")
            owItem = oForm.Items.Add("IDH_WOHdr", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 5
            owItem.Width = owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            'owItem.AffectsFormMode = False
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            'owItem.Enabled = False
            'Adding link buttons for future
            oBTNLink = oForm.Items.Add("IDH_WOHLK", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oBTNLink.Left = owItemControl.Left - 13
            oBTNLink.Top = owItemControl.Top + owItemControl.Height + 5
            oBTNLink.Width = 12
            oBTNLink.Height = 12
            oBTNLink.FromPane = owItemControl.FromPane
            oBTNLink.ToPane = owItemControl.ToPane
            oMastLink = oBTNLink.Specific
            oMastLink.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oMastLink.Image = IDHAddOns.idh.addon.Base.doFixImagePath("Drill.png")
            'WO Header Label 
            owItemLabel = oForm.Items.Item("149") 'Duration Label
            owItem = oForm.Items.Add("IDH_WOHLBL", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("WO Header No")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDH_WOHdr"


            'WO Row Edit 
            owItemControl = oForm.Items.Item("IDH_WOHdr")
            owItem = oForm.Items.Add("IDH_WORow", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 1
            owItem.Width = owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            'owItem.AffectsFormMode = False
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            'owItem.Enabled = False
            'Adding link buttons for future
            oBTNLink = oForm.Items.Add("IDH_WORLK", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oBTNLink.Left = owItemControl.Left - 13
            oBTNLink.Top = owItemControl.Top + owItemControl.Height + 1
            oBTNLink.Width = 12
            oBTNLink.Height = 12
            oBTNLink.FromPane = owItemControl.FromPane
            oBTNLink.ToPane = owItemControl.ToPane
            oMastLink = oBTNLink.Specific
            oMastLink.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oMastLink.Image = IDHAddOns.idh.addon.Base.doFixImagePath("Drill.png")
            'WO Row Label 
            owItemLabel = oForm.Items.Item("IDH_WOHLBL") 'Duration Label
            owItem = oForm.Items.Add("IDH_WORLBL", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("WO Row No")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDH_WORow"

            'Now adding Disposal Order fields 
            'Disposal Order Header Edit 
            owItemControl = oForm.Items.Item("IDH_WORow")
            owItem = oForm.Items.Add("IDH_DOHdr", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 4
            owItem.Width = owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            'owItem.AffectsFormMode = False
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            'owItem.Enabled = False
            'Adding link buttons for future
            oBTNLink = oForm.Items.Add("IDH_DOHLK", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oBTNLink.Left = owItemControl.Left - 13
            oBTNLink.Top = owItemControl.Top + owItemControl.Height + 4
            oBTNLink.Width = 12
            oBTNLink.Height = 12
            oBTNLink.FromPane = owItemControl.FromPane
            oBTNLink.ToPane = owItemControl.ToPane
            oMastLink = oBTNLink.Specific
            oMastLink.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oMastLink.Image = IDHAddOns.idh.addon.Base.doFixImagePath("Drill.png")
            'Disposal Order Header Label 
            owItemLabel = oForm.Items.Item("IDH_WORLBL") 'Duration Label
            owItem = oForm.Items.Add("IDH_DOHLBL", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 4
            owItem.Width = owItemLabel.Width
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("Disposal Order No")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDH_DOHdr"

            'Disposal Order Row Edit 
            owItemControl = oForm.Items.Item("IDH_DOHdr")
            owItem = oForm.Items.Add("IDH_DORow", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 1
            owItem.Width = owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            'owItem.AffectsFormMode = False
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            'owItem.Enabled = False
            'Adding link buttons for future
            oBTNLink = oForm.Items.Add("IDH_DORLK", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oBTNLink.Left = owItemControl.Left - 13
            oBTNLink.Top = owItemControl.Top + owItemControl.Height + 1
            oBTNLink.Width = 12
            oBTNLink.Height = 12
            oBTNLink.FromPane = owItemControl.FromPane
            oBTNLink.ToPane = owItemControl.ToPane
            oMastLink = oBTNLink.Specific
            oMastLink.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oMastLink.Image = IDHAddOns.idh.addon.Base.doFixImagePath("Drill.png")
            'Disposal Order Row Label 
            owItemLabel = oForm.Items.Item("IDH_DOHLBL") 'Duration Label
            owItem = oForm.Items.Add("IDH_DORLBL", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("Disposal Order Row")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDH_DORow"

            ''Enquiry ID
            owItemControl = oForm.Items.Item("IDH_DORow")
            owItem = oForm.Items.Add("IDH_EnqID", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 1
            owItem.Width = owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True

            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.Enabled = False
            owItem.AffectsFormMode = False
            owItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, _
                                           15, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            'Adding link buttons for Enquiry
            oBTNLink = oForm.Items.Add("IDH_ENQLK", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oBTNLink.Left = owItemControl.Left - 13
            oBTNLink.Top = owItemControl.Top + owItemControl.Height + 1
            oBTNLink.Width = 12
            oBTNLink.Height = 12
            oBTNLink.FromPane = owItemControl.FromPane
            oBTNLink.ToPane = owItemControl.ToPane
            oMastLink = oBTNLink.Specific
            oMastLink.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oMastLink.Image = IDHAddOns.idh.addon.Base.doFixImagePath("Drill.png")

            'Enquiry ID Label 
            owItemLabel = oForm.Items.Item("IDH_DORLBL") 'Duration Label
            owItem = oForm.Items.Add("IDH_ENQLBL", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("Enquiry ID")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDH_EnqID"


            ''WOQ ID
            owItemControl = oForm.Items.Item("IDH_EnqID")
            owItem = oForm.Items.Add("IDH_WOQID", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 1
            owItem.Width = owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True

            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.Enabled = False
            owItem.AffectsFormMode = False
            owItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, _
                                           15, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            'Adding link buttons for Enquiry
            oBTNLink = oForm.Items.Add("IDH_WOQLK", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oBTNLink.Left = owItemControl.Left - 13
            oBTNLink.Top = owItemControl.Top + owItemControl.Height + 1
            oBTNLink.Width = 12
            oBTNLink.Height = 12
            oBTNLink.FromPane = owItemControl.FromPane
            oBTNLink.ToPane = owItemControl.ToPane
            oMastLink = oBTNLink.Specific
            oMastLink.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oMastLink.Image = IDHAddOns.idh.addon.Base.doFixImagePath("Drill.png")

            'WOQ ID Label 
            owItemLabel = oForm.Items.Item("IDH_ENQLBL") 'Label
            owItem = oForm.Items.Add("IDH_WOQLBL", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("WOQ ID")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDH_WOQID"

            'Adding Fields for EVN Number 
            ''EVN ID
            owItemControl = oForm.Items.Item("IDH_WOQID")
            owItem = oForm.Items.Add("IDH_EVNID", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 1
            owItem.Width = owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True

            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.Enabled = False
            owItem.AffectsFormMode = False
            owItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, _
                                           15, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            'Adding link buttons for EVN
            oBTNLink = oForm.Items.Add("IDH_EVNLK", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oBTNLink.Left = owItemControl.Left - 13
            oBTNLink.Top = owItemControl.Top + owItemControl.Height + 1
            oBTNLink.Width = 12
            oBTNLink.Height = 12
            oBTNLink.FromPane = owItemControl.FromPane
            oBTNLink.ToPane = owItemControl.ToPane
            oMastLink = oBTNLink.Specific
            oMastLink.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oMastLink.Image = IDHAddOns.idh.addon.Base.doFixImagePath("Drill.png")

            'EVN ID Label 
            owItemLabel = oForm.Items.Item("IDH_WOQLBL") 'Label
            owItem = oForm.Items.Add("IDH_EVNLBL", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("EVN ID")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDH_EVNID"

            'Customer Code 
            If sFMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE.ToString() Then
                oEdit = oForm.Items.Item("9").Specific
                If getParentSharedData(oForm, "IDH_CUST").ToString() <> String.Empty Then
                    oEdit.Value = getParentSharedData(oForm, "IDH_CUST").ToString()
                End If
            End If

            'WO Header Number 
            oEdit = oForm.Items.Item("IDH_WOHdr").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_IDHWO")
            If (getParentSharedData(oForm, "IDH_WOH") <> String.Empty) And (getParentSharedData(oForm, "IDH_WOH") <> Nothing) Then
                oEdit.Value = getParentSharedData(oForm, "IDH_WOH").ToString()
            End If

            'WO Row Number
            oEdit = oForm.Items.Item("IDH_WORow").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_IDHWOR")
            Dim sVal As String = getParentSharedData(oForm, "IDH_WOR")

            If (sVal <> String.Empty) And (sVal <> Nothing) Then
                oEdit.Value = getParentSharedData(oForm, "IDH_WOR").ToString()
            End If

            'Disposal Order Header Number 
            oEdit = oForm.Items.Item("IDH_DOHdr").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_IDHDO")
            If (getParentSharedData(oForm, "IDH_DOH") <> String.Empty) And (getParentSharedData(oForm, "IDH_DOH") <> Nothing) Then
                oEdit.Value = getParentSharedData(oForm, "IDH_DOH").ToString()
            End If

            'Disposal Order Row Number
            oEdit = oForm.Items.Item("IDH_DORow").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_IDHDOR")
            If (getParentSharedData(oForm, "IDH_DOR") <> String.Empty) And (getParentSharedData(oForm, "IDH_DOR") <> Nothing) Then
                oEdit.Value = getParentSharedData(oForm, "IDH_DOR").ToString()
            End If

            'Enquiry ID
            oEdit = oForm.Items.Item("IDH_EnqID").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_IDHEnqID")

            'WOQ ID
            oEdit = oForm.Items.Item("IDH_WOQID").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_IDHWOQID")

            'EVN Number 
            oEdit = oForm.Items.Item("IDH_EVNID").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_IDHEVNID")
            If (getParentSharedData(oForm, "IDH_EVN") <> String.Empty) And (getParentSharedData(oForm, "IDH_EVN") <> Nothing) Then
                oEdit.Value = getParentSharedData(oForm, "IDH_EVN").ToString()
            End If

            If Config.INSTANCE.getParameterAsBool("USAREL", False) = True AndAlso getParentSharedData(oForm, "WODRFTACTY") IsNot Nothing AndAlso getParentSharedData(oForm, "WODRFTACTY").ToString().Length > 0 Then
                'Default Activity, Type and Subject fields value as per WR Config
                Dim sDfActivity As String = Config.ParameterWithDefault("ADACTVTY", "") 'getParentSharedDataAsString(oForm, "")
                Dim sDfType As String = Config.ParameterWithDefault("ADFTTYPE", "") 'getParentSharedDataAsString(oForm, "")
                Dim sDfSubject As String = Config.ParameterWithDefault("ADFTSUBJ", "") 'getParentSharedDataAsString(oForm, "")

                Try
                    If sDfActivity.Length > 0 Then
                        oCmb = oForm.Items.Item("67").Specific 'Action
                        oCmb.Select(sDfActivity, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                    End If

                    If sDfType.Length > 0 Then
                        oCmb = oForm.Items.Item("68").Specific 'Type 
                        oCmb.Select(sDfType, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                    End If

                    If sDfSubject.Length > 0 Then
                        oCmb = oForm.Items.Item("35").Specific 'Subject
                        oCmb.Select(sDfSubject, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                    End If
                Catch ex As Exception
                End Try
            End If

            'Check if the Activity is closed, enable the Drill buttons for WOH, WOR, DOH and DOR Link buttons 
            Try
                Dim sIsClosed As String = getDFValue(oForm, "OCLG", "Closed")

                If sIsClosed.Equals("Y") Then
                    oForm.Items.Item("IDH_WOHLK").Enabled = True
                    oForm.Items.Item("IDH_WORLK").Enabled = True
                    oForm.Items.Item("IDH_DOHLK").Enabled = True
                    oForm.Items.Item("IDH_DORLK").Enabled = True
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception enabling Drill buttons: " + ex.ToString, "doActivityForm()")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDB", {Nothing})
            End Try
            ''Add Enquiry button
            'Adding link buttons for future
            oBTNLink = oForm.Items.Add("IDH_ENQIRY", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItemControl = oForm.Items.Item("75")
            oBTNLink.Left = owItemControl.Left
            oBTNLink.Top = oForm.Items.Item("1").Top
            oBTNLink.Width = owItemControl.Width
            oBTNLink.Height = oForm.Items.Item("1").Height

            oBTNLink.FromPane = oForm.Items.Item("1").FromPane
            oBTNLink.ToPane = oForm.Items.Item("1").ToPane


            oBTNLink.AffectsFormMode = False
            oMastLink = oBTNLink.Specific
            oMastLink.Type = SAPbouiCOM.BoButtonTypes.bt_Caption
            oMastLink.Caption = getTranslatedWord("Enquiry")

            '#MA Start 30-03-2017
            owItemLabel = oForm.Items.Item("540000151") 'Source User ID  ''77
            owItem = oForm.Items.Add("IDHESWSUID", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 2
            owItem.Width = owItemLabel.Width + 80
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("SW Source User ID")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDHSWSUID"

            owItemControl = oForm.Items.Item("540000152") 'Source User ID Edit  ''79
            owItem = oForm.Items.Add("IDHSWSUID", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left + 40
            owItem.Top = owItemControl.Top + owItemControl.Height + 2 'owItemControl.Top + (2 * owItemControl.Height) + 1
            owItem.Width = owItemControl.Width + owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.AffectsFormMode = False
            owItem.LinkTo = "540000152"
            owItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable,
            SAPbouiCOM.BoAutoFormMode.afm_Find, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            oEdit = oForm.Items.Item("IDHSWSUID").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_SrcUsrID")

            owItemLabel = oForm.Items.Item("IDHESWSUID") 'Destination User ID'77
            owItem = oForm.Items.Add("IDHESWDUID", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 2 'owItemLabel.Top + (1 * owItemLabel.Height) + 5  '3
            owItem.Width = owItemLabel.Width + 80
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("SW Destination User ID")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDHSWDUID"

            owItemControl = oForm.Items.Item("IDHSWSUID") 'Destination User ID Edit '79
            owItem = oForm.Items.Add("IDHSWDUID", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 2 'owItemControl.Top + (4 * owItemControl.Height) + 5
            owItem.Width = owItemControl.Width '+ owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.AffectsFormMode = False
            owItem.LinkTo = "IDHSWSUID"
            owItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable,
            SAPbouiCOM.BoAutoFormMode.afm_Find, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            oEdit = oForm.Items.Item("IDHSWDUID").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_DstUsrID")

            owItemLabel = oForm.Items.Item("IDHESWDUID") 'Source User Email
            owItem = oForm.Items.Add("IDHESWSUEm", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 2 ' owItemLabel.Top + (2 * owItemLabel.Height) + 3
            owItem.Width = owItemLabel.Width + 80
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("SW Source User Email")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDHSWSUEm"

            owItemControl = oForm.Items.Item("IDHSWDUID") 'Source User Email Edit
            owItem = oForm.Items.Add("IDHSWSUEm", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 2 'owItemControl.Top + (3 * owItemControl.Height) + 3
            owItem.Width = owItemControl.Width '+ owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.AffectsFormMode = False
            owItem.LinkTo = "IDHSWDUID"
            owItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable,
            SAPbouiCOM.BoAutoFormMode.afm_Find, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            oEdit = oForm.Items.Item("IDHSWSUEm").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_SrcUsrEml")

            owItemLabel = oForm.Items.Item("IDHESWSUEm") 'Destination User Email
            owItem = oForm.Items.Add("IDHESWDUEm", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 2 ' owItemLabel.Top + (4 * owItemLabel.Height) + 7
            owItem.Width = owItemLabel.Width + 80
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("SW Destination User Email")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDHSWDUEm"

            owItemControl = oForm.Items.Item("IDHSWSUEm") 'Destination User Email Edit
            owItem = oForm.Items.Add("IDHSWDUEm", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 2 'owItemControl.Top + (5 * owItemControl.Height) + 7
            owItem.Width = owItemControl.Width '+ owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.AffectsFormMode = False
            owItem.LinkTo = "IDHSWSUEm"
            owItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable,
            SAPbouiCOM.BoAutoFormMode.afm_Find, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            oEdit = oForm.Items.Item("IDHSWDUEm").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_DstUsrEml")

            '#MA Start 30-03-2017

            ''Add a rectangle 
            'Dim oItm As SAPbouiCOM.Item
            'oItm = oForm.Items.Item("148")

            'owItem = oForm.Items.Add("IDH_RECT04", SAPbouiCOM.BoFormItemTypes.it_RECTANGLE)
            ''owItem.Left = oItm.Left - 25
            'owItem.Left = 12
            'owItem.Top = oItm.Top + oItm.Height + 2
            'owItem.Width = (oItm.Width * 2) + 15
            'owItem.Height = oItm.Height * 5
            'owItem.Visible = True
            ''owItem.AffectsFormMode = False
            'owItem.FromPane = oItm.FromPane
            'owItem.ToPane = oItm.ToPane
            doAddWF(oForm, "SETENQIDs", "")
            doAddWF(oForm, "CardCode", "")
            doAddWF(oForm, "Personal", "")
            doAddWF(oForm, "SelectEnquiry", "")
            doAddWF(oForm, "Remarks", "")
        End Sub

    End Class
End Namespace
