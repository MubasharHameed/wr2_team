'
' Created by SharpDevelop.
' User: Louis Viljoen
' Date: 16/04/2008
' Time: 11:12
'
Imports System.Collections
Imports System.IO

Namespace idh.forms.SBO
    Public Class SalesOrder
        Inherits IDHAddOns.idh.forms.Base
        
        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "139", -1, Nothing)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_PRINT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_PRINT_DATA)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_PRINT_LAYOUT_KEY)
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "38" AndAlso pVal.ColUID = "U_IDH_WOR" Then
                    	Dim oMatrix As SAPbouiCOM.Matrix
                        oMatrix = oForm.Items.Item("38").Specific
                        Dim sKey As String = oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Specific.Value
                        If sKey.Length > 2 Then
                        	Dim sAct As String = sKey.Substring(0,2)
                        	Dim sParams() As String = sKey.Substring(2).Split(".")
                        	If sParams.Length > 1 Then
                        		Dim oData As New ArrayList
                        		If sAct = "WO" Then
                                    '                        			oData.Add("DoView")
                                    ''                        				oData.Add(sJobEntr)
                                    ''                        				goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
                                    '                        			oData.Add(sParams(1))
                                    '                                    goParent.doOpenModalForm("IDHJOBS", oForm, oData)

                                    Try
                                        Dim oOrderRow As com.isb.core.forms.OrderRow = New com.isb.core.forms.OrderRow(oForm.UniqueID, sParams(1))
                                        'oOrderRow.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                                        'oOrderRow.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                                        oOrderRow.doShowModal()
                                    Catch Ex As Exception
                                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                                    End Try
                                ElseIf sAct = "DO" Then
                        			oData.Add("DoView")
                                	oData.Add(sParams(0))
                                	setSharedData(oForm, "ROWCODE", sParams(1))
                                	goParent.doOpenModalForm("IDH_DISPORD", oForm, oData)
                        		End If
                        	End If
                        End If
                    End If
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                setWFValue(oForm, "ISDRAFT", False)
                Dim oItem As SAPbouiCOM.ComboBox = CType(oForm.Items.Item("81").Specific, SAPbouiCOM.ComboBox)
                Dim sStatus As String = oItem.Value
                'Draft Status = 6
                If sStatus.Equals("6") Then
                    setWFValue(oForm, "ISDRAFT", True)
                End If
            Else
                If getWFValue(oForm, "ISDRAFT") Then
                    com.idh.bridge.action.MarketingDocs.doUpdateDrafts()
                End If
            End If
        End Sub

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
