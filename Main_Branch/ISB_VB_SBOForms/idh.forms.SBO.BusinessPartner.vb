Imports System.Collections
Imports System.IO
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge
Imports IDHAddOns.idh.controls
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.data
Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User
Imports com.idh.dbObjects
Imports com.idh.controls
Imports com.uBC.utils
Imports com.idh.bridge.resources.Messages
Imports com.idh.dbObjects.numbers
Imports com.idh.forms.oo.Form

Namespace idh.forms.SBO
    Public Class BusinessPartner
        Inherits IDHAddOns.idh.forms.Base
        Class WORef_Pair
            Public WOCode As String
            Public CustRef As String
        End Class
        'Shared FORECASTPANE As Integer = 101
        'Shared FORECASTTAB As String = "IDH_TABFCS"
        'Shared FCGRIDSTENCIL As String = "21"
        Shared TABWIDTH As Integer = -1

        Dim miDefaultMonthPeriod As Integer = 1
        '        Dim AxBrowser As SHDocVw.InternetExplorer
        Private iFilterBackColor As Integer = -1
        Dim x As Integer = 7
        Dim y As Integer = 135
        Dim w As Integer
        Dim h As Integer = 800

        Dim h_old As Integer = -1
        Dim w_old As Integer = -1
        Dim ch_old As Integer = -1
        Dim cw_old As Integer = -1

        Dim h_25_old As Integer
        Dim t_23_old As Integer

        Dim t_1_old As Integer
        Dim t_2_old As Integer
        ' Private aCustomButtons() As String = {"IDH_BACOPY", "IDH_WROSMC", "IDH_WRDOMC", "IDH_WSTPRF", "IDH_CPYADD"}    Comment By #MA
        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "134", -1, Nothing)
            gsSystemMenuID = "2561"
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD)

            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD)
        End Sub

        Public Overrides Sub doLoadForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                Dim owItem As SAPbouiCOM.Item
                'Dim oFolder As SAPbouiCOM.Folder
                Dim ow2Item As SAPbouiCOM.Item
                'Dim oLink As SAPbouiCOM.LinkedButton
                'Dim oCheck As SAPbouiCOM.CheckBox
                'Dim oButton As SAPbouiCOM.Button
                'Dim oEdit As SAPbouiCOM.EditText

                'Dim iTabWidth As Integer = 160
                'Dim iHeight As Integer = 14

                Dim t As Integer
                Dim h As Integer
                Dim w As Integer
                Dim diff As Integer
                Dim iFrameSpace As Integer
                Dim h_new As Integer = -1
                Dim w_new As Integer = -1
                'Dim bMoved As Boolean = False

                Dim iButtonHeight As Integer

                owItem = oForm.Items.Item("1")          'OK Button
                iButtonHeight = owItem.Height

                'w_old = oForm.Width
                'h_old = oForm.Height
                'oForm.Width = oForm.Width + iTabWidth 'ow2Item.Width + 40
                'w = oForm.Width

                If h_old = -1 Then
                    h_old = oForm.Height
                End If

                ow2Item = oForm.Items.Item("23")
                'ow2Item.LinkTo = "223"
                t = ow2Item.Top
                h = ow2Item.Height
                w = ow2Item.Width
                diff = oForm.ClientHeight - (t + h + iButtonHeight + 15)
                iFrameSpace = oForm.Height - oForm.ClientHeight
                'diff = iButtonTop - t
                If diff < 5 Then
                    'oForm.Height = oForm.Height + iButtonHeight + 1
                    'h_new = oForm.Height + iButtonHeight + 1

                    h_new = t + (iButtonHeight * 2) + 15 + iFrameSpace
                    'bMoved = True
                End If

                If w_old = -1 Then
                    w_old = oForm.Width
                End If

                '				ow2Item = oForm.Items.Item("9")
                '				Dim iTabsWidth As Integer = ow2Item.Left + ow2Item.Width + iTabWidth
                '
                '				If w_old < iTabsWidth Then
                '					'oForm.Width = oForm.Width + iTabWidth
                '					w_new = oForm.Width + iTabWidth
                '					'w_old = oForm.Width
                '					If h_new < 0 Then
                '						h_new = oForm.Height
                '					End If
                '				Else
                '					If h_new > 0 Then
                w_new = oForm.Width
                '					End If
                '				End If
                '
                '				If h_new > 0 AndAlso w_new > 0 Then
                '					oForm.Resize(w_new, h_new)
                '					'oForm.Height = h_new
                '					doAddWF(oForm, "RESIZED", True)
                '				Else
                '					doAddWF(oForm, "RESIZED", False)
                '				End If

                doHeaderPlacement(oForm)
                doAddressTabPLacementnew(oForm)
                'doAddressTabPLacement(oForm)

                If IDHAddOns.idh.addon.Base.LAYOUTTYPE = FixedValues.SBOLayout2008 OrElse
                        IDHAddOns.idh.addon.Base.LAYOUTTYPE = FixedValues.SBOLayout2009PL5 Then
                    doGeneralTabPlacement88106(oForm)
                    '  doFooterPlacement88106(oForm)      Funcaion Comment by #MA 
                    doFooterPlacement88106New(oForm)       '#MA Added New WR1 Core Button in New Function
                Else
                    doGeneralTabPlacement(oForm)
                    doPaymentTermsTabPlacement(oForm)
                    ' doFooterPlacement(oForm)  Comment By #MA
                    doFooterPlacementNew(oForm)   'Added New WR1 Core Button in New Function
                End If
                doPaymentTermsTabPlacement(oForm)

                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    ' doShowWR1(oForm, False, "")  Funcaion  comment by #MA  
                    doShowWR1New(oForm, False, "")   '#MA Added New Forms Names in New Funcion

                Else
                    'doShowWR1(oForm, True, "")   Funcaion Comment by #MA
                    doShowWR1New(oForm, True, "")  '#MA Added New Forms Names in New Funcion

                End If

                'If IDHAddOns.idh.addon.Base.LAYOUTTYPE = "8.81-06" Then
                '    doHeaderPlacement(oForm)
                '    doGeneralTabPlacement88106(oForm)
                '    doAddressTabPLacement(oForm)
                '    doPaymentTermsTabPlacement(oForm)
                '    doFooterPlacement88106(oForm)
                'Else
                '    doHeaderPlacement(oForm)
                '    doGeneralTabPlacement(oForm)
                '    doAddressTabPLacement(oForm)
                '    doPaymentTermsTabPlacement(oForm)
                '    doFooterPlacement(oForm)
                'End If

                If h_new > 0 AndAlso w_new > 0 Then
                    oForm.Resize(w_new, h_new)
                    doAddWF(oForm, "RESIZED", True)
                Else
                    doAddWF(oForm, "RESIZED", False)
                End If
                doAddWF(oForm, "HandleValidate", True)
                'If Config.INSTANCE.doGetDoForecasting() Then
                '    'OnTime 35413: WR1_NewDev_PO3 Development
                '    'Add the Forecast Tab
                '    doAddForecastTab(oForm)
                '    'Add Forecast Grid
                '    doAddForecastGrid(oForm)
                'End If

                'doShowWR1(oForm, True)
                doAddWF(oForm, "DOWR1", False)
                doAddWF(oForm, "BUSWLBP", False)
                doAddWF(oForm, "DOVAL", True)
                setWFValue(oForm, "handleduplicateAdrsCD", False)
                'doAddWF(oForm, "WR1REFRESH", True)
                'setWFValue(oForm, "WasOnForeCast", False)
                doBranchCombo(oForm)
                doObligatedCombo(oForm)
                '#MA Start 19-06-2017
                oForm.Items.Item("IDH_WRBNT").AffectsFormMode = False
                oForm.Items.Item("IDH_WRADD").AffectsFormMode = False
                '#MA End
                'Dim sBType As String = getDFValue(oForm, "OCRD", "CardType")
                'FillCombos.PriceGroupsCombo(oForm.Items.Item("IDHPRGRP").Specific, sBType, "", "FALLBACK")

                'Dim bWasOnForeCast As Boolean = getWFValue(oForm, "WasOnForeCast")
                'If bWasOnForeCast Then
                '    doSetFocus(oForm, "IDH_TABFCS")
                'End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString, "Error loading the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDLF", {Nothing})
                BubbleEvent = False
                Exit Sub
            End Try
        End Sub

        Private Sub doMapTab(ByRef oForm As SAPbouiCOM.Form)
            Dim owItem As SAPbouiCOM.Item
            '                owItem = oForm.Items.Add("IDH_BRWS01", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            '                owItem.Left = ow2Item.Left + ow2Item.Width
            '                owItem.Top = ow2Item.Top
            '                owItem.Width = iTabWidth 'ow2Item.Width + 40
            '                owItem.Height = ow2Item.Height
            '                owItem.Visible = True
            '                owItem.AffectsFormMode = False
            '                oFolder = owItem.Specific
            '                oFolder.Caption = "Map"
            '                oFolder.GroupWith("9")
            '
            owItem = oForm.Items.Item("25")
            h_25_old = owItem.Height
            '                owItem.Left = owItem.Left + iTabWidth 'ow2Item.Width + 40
            '
            owItem = oForm.Items.Item("23")
            t_23_old = owItem.Top
            '                owItem.Width = owItem.Width + iTabWidth 'ow2Item.Width + 40
            '
            '                owItem = oForm.Items.Item("21")
            '                owItem.Width = owItem.Width + iTabWidth 'ow2Item.Width + 40
            '
            '                owItem = oForm.Items.Item("276")
            '                owItem.Width = owItem.Width + iTabWidth 'ow2Item.Width + 40
            '
            '                owItem = oForm.Items.Item("275")
            '                owItem.Width = owItem.Width + iTabWidth 'ow2Item.Width + 40
            '
            '                owItem = oForm.Items.Item("274")
            '                owItem.Left = owItem.Left + iTabWidth 'ow2Item.Width + 40

            t_1_old = oForm.Items.Item("1").Top
            t_2_old = oForm.Items.Item("2").Top

            '                owItem = oForm.Items.Add("IDHBRW", SAPbouiCOM.BoFormItemTypes.it_ACTIVE_X)
            '                owItem.Top = y
            '                owItem.Left = x
            '                owItem.Width = -1
            '                owItem.Height = -1
            '                owItem.FromPane = 11
            '                owItem.ToPane = 11
            '                owItem.AffectsFormMode = False
        End Sub

        Private Sub doHeaderPlacement(ByRef oForm As SAPbouiCOM.Form)
            Dim owItemLabel As SAPbouiCOM.Item
            Dim owItemControl As SAPbouiCOM.Item
            Dim owItem As SAPbouiCOM.Item
            Dim oEdit As SAPbouiCOM.EditText
            Dim iHeight As Integer

            'Add the WRBalance
            owItemControl = oForm.Items.Item("149") 'Oppertunities
            iHeight = owItemControl.Height
            owItemLabel = oForm.Items.Item("148") 'Oppertunities Label

            owItem = oForm.Items.Add("IDH_WR1ORD", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight + 1
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.Visible = True
            owItem.AffectsFormMode = True
            oEdit = owItem.Specific
            '## Start 01-10-2013
            'owItem.Enabled = False
            '## End
            owItem.RightJustified = True
            doAddUF(oForm, "IDH_WR1ORD", SAPbouiCOM.BoDataType.dt_PRICE)
            owItemControl = owItem
            '## Start 01-10-2013
            owItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable,
                            SAPbouiCOM.BoFormMode.fm_ADD_MODE,
                            SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            owItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable,
            SAPbouiCOM.BoFormMode.fm_ADD_MODE + SAPbouiCOM.BoFormMode.fm_EDIT_MODE +
            SAPbouiCOM.BoFormMode.fm_OK_MODE + SAPbouiCOM.BoFormMode.fm_PRINT_MODE +
            SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + SAPbouiCOM.BoFormMode.fm_VIEW_MODE,
            SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            '## End

            owItem = oForm.Items.Add("IDH_WR1OLB", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.Visible = False
            owItem.Specific.Caption = getTranslatedWord("WR1 Orders")
            owItem.LinkTo = "IDH_WR1ORD"
            owItemLabel = owItem

            owItem = oForm.Items.Add("RR", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Width = 0
            owItem.Height = 0
            oEdit = owItem.Specific
            oEdit.DataBind.SetBound(True, "CRD1", "U_ROUTE")
        End Sub

        '8.81 PL 06 Layout
        Private Sub doFooterPlacement88106(ByRef oForm As SAPbouiCOM.Form)
            Dim owItemControl As SAPbouiCOM.Item
            Dim owItem As SAPbouiCOM.Item
            Dim iButtonHeight As Integer
            Dim iButtonTop As Integer

            owItemControl = oForm.Items.Item("1")           'OK Button
            iButtonHeight = owItemControl.Height

            iButtonTop = owItemControl.Top

            owItem = oForm.Items.Item("2")              'Cancel Button
            owItem.Top = iButtonTop
            owItemControl = owItem

            owItem = oForm.Items.Item("271")            'Related Service Call
            If owItem.Visible Then
                owItem.Left = owItemControl.Left + owItemControl.Width + 10
                owItem.LinkTo = "1"
                owItem.Top = iButtonTop
                owItemControl = owItem
            End If

            owItem = oForm.Items.Item("103")            'Activity
            If owItem.Visible Then
                owItem.Left = owItemControl.Left + owItemControl.Width + 2
                owItem.LinkTo = "1"
                owItem.Top = iButtonTop
                owItemControl = owItem
            End If

            owItem = oForm.Items.Item("104")            'Related Activities
            If owItem.Visible Then
                owItem.Left = owItemControl.Left + owItemControl.Width + 2
                owItem.LinkTo = "1"
                owItem.Top = iButtonTop
                owItemControl = owItem
            End If

            If Config.INSTANCE.getParameterAsBool("PBIENABL", True) Then
                owItem = oForm.Items.Add("IDH_PBI", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
                owItem.Left = owItemControl.Left + owItemControl.Width + 2
                owItem.Top = iButtonTop
                owItem.Width = 35
                owItem.Height = iButtonHeight
                owItem.Visible = True
                owItem.Specific.Caption = getTranslatedWord("PBI")
                owItem.LinkTo = "1"
                owItem.FromPane = owItemControl.FromPane
                owItem.ToPane = owItemControl.ToPane
                owItemControl = owItem
            End If

            owItem = oForm.Items.Add("IDH_OSMB", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 35
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("OSM")
            owItem.LinkTo = "1"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("IDH_DOMB", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 35
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("DOM")
            owItem.LinkTo = "1"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("IDH_CIP", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 35
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("CIP")
            owItem.LinkTo = "1"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("IDH_CWABT", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 100
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("Container Whereabouts")
            owItem.LinkTo = "1"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("IDH_ONSITE", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 46
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("OnSite")
            owItem.LinkTo = "1"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("uBC_TRANCD", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 80
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("Transaction Codes...")
            owItem.LinkTo = "1"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("uBC_ADDCHG", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 80
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("Additional Charges")
            owItem.LinkTo = "1"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            '#MA Start 01-06-2017
            owItem = oForm.Items.Add("uBC_COMRCL", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 80
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("Commercials")
            owItem.LinkTo = "1"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem
            '#MA End 01-06-2017

        End Sub
        '#MA Start 19-06-2017
        Private Sub doFooterPlacement88106New(ByRef oForm As SAPbouiCOM.Form)
            Dim owItemControl As SAPbouiCOM.Item
            Dim owItem As SAPbouiCOM.Item
            Dim iButtonHeight As Integer
            Dim iButtonTop As Integer

            owItemControl = oForm.Items.Item("1")           'OK Button
            iButtonHeight = owItemControl.Height

            iButtonTop = owItemControl.Top

            owItem = oForm.Items.Item("2")              'Cancel Button
            owItem.Top = iButtonTop
            owItemControl = owItem

            owItem = oForm.Items.Item("271")            'Related Service Call
            If owItem.Visible Then
                owItem.Left = owItemControl.Left + owItemControl.Width + 10
                owItem.LinkTo = "1"
                owItem.Top = iButtonTop
                owItemControl = owItem
            End If

            owItem = oForm.Items.Item("103")            'Activity
            If owItem.Visible Then
                owItem.Left = owItemControl.Left + owItemControl.Width + 2
                owItem.LinkTo = "1"
                owItem.Top = iButtonTop
                owItemControl = owItem
            End If

            owItem = oForm.Items.Item("104")            'Related Activities
            If owItem.Visible Then
                owItem.Left = owItemControl.Left + owItemControl.Width + 2
                owItem.LinkTo = "1"
                owItem.Top = iButtonTop
                owItemControl = owItem
            End If
            owItemControl = oForm.Items.Item("540002072")
            iButtonHeight = owItemControl.Height
            iButtonTop = owItemControl.Top
            owItem = oForm.Items.Add("IDH_WRBNT", SAPbouiCOM.BoFormItemTypes.it_BUTTON_COMBO)
            owItem.Left = owItemControl.Left - 167
            owItem.Top = iButtonTop
            owItem.Width = 165
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.DisplayDesc = True
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            Dim ComboBtn As SAPbouiCOM.ButtonCombo = owItem.Specific
            ComboBtn.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
            ComboBtn.Caption = Translation.getTranslatedWord("Waste & Recycle")
        End Sub
        '#MA End

        'Normal 2007 Layout
        Private Sub doFooterPlacement(ByRef oForm As SAPbouiCOM.Form)
            Dim owItemControl As SAPbouiCOM.Item
            Dim owItem As SAPbouiCOM.Item
            Dim iButtonHeight As Integer
            Dim iButtonTop As Integer

            owItemControl = oForm.Items.Item("1")           'OK Button
            iButtonHeight = owItemControl.Height
            'owItemControl.Top = oForm.ClientHeight - (((iButtonHeight + 1) * 3 ) + 5)
            'owItemControl.Top = owItemControl.Top + iButtonHeight + 1
            'owItemControl.LinkTo = "23"

            iButtonTop = owItemControl.Top

            owItem = oForm.Items.Item("2")          'Cancel Button
            owItem.Top = iButtonTop
            'owItemControl.LinkTo = "23"
            owItemControl = owItem

            owItem = oForm.Items.Item("271")            'Related Service Call
            owItem.Left = owItemControl.Left + owItemControl.Width + 10
            owItem.LinkTo = "1"
            owItem.Top = iButtonTop
            owItemControl = owItem

            owItem = oForm.Items.Item("103")            'Activity
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.LinkTo = "1"
            owItem.Top = iButtonTop
            owItemControl = owItem

            owItem = oForm.Items.Item("104")            'Related Activities
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.LinkTo = "1"
            owItem.Top = iButtonTop
            owItemControl = owItem

            If Config.INSTANCE.getParameterAsBool("PBIENABL", True) Then
                owItem = oForm.Items.Add("IDH_PBI", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
                owItem.Left = owItemControl.Left + owItemControl.Width + 2
                owItem.Top = iButtonTop
                owItem.Width = 35
                owItem.Height = iButtonHeight
                owItem.Visible = True
                owItem.Specific.Caption = getTranslatedWord("PBI")
                owItem.LinkTo = "271"
                owItem.FromPane = owItemControl.FromPane
                owItem.ToPane = owItemControl.ToPane
                owItemControl = owItem
            End If

            owItem = oForm.Items.Add("IDH_OSMB", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 35
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("OSM")
            owItem.LinkTo = "271"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("IDH_DOMB", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 35
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("DOM")
            owItem.LinkTo = "271"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("IDH_CIP", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 35
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("CIP")
            owItem.LinkTo = "271"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("IDH_CWABT", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 100
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("Container Whereabouts")
            owItem.LinkTo = "271"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("IDH_ONSITE", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 46
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("OnSite")
            owItem.LinkTo = "271"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("uBC_TRANCD", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 80
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("Transaction Codes...")
            owItem.LinkTo = "271"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("uBC_ADDCHG", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = iButtonTop
            owItem.Width = 80
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("Additional Charges")
            owItem.LinkTo = "271"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem
        End Sub

        Private Sub doFooterPlacementNew(ByRef oForm As SAPbouiCOM.Form)
            Dim owItemControl As SAPbouiCOM.Item
            Dim owItem As SAPbouiCOM.Item
            Dim iButtonHeight As Integer
            Dim iButtonTop As Integer

            owItemControl = oForm.Items.Item("1")           'OK Button
            iButtonHeight = owItemControl.Height
            'owItemControl.Top = oForm.ClientHeight - (((iButtonHeight + 1) * 3 ) + 5)
            'owItemControl.Top = owItemControl.Top + iButtonHeight + 1
            'owItemControl.LinkTo = "23"

            iButtonTop = owItemControl.Top

            owItem = oForm.Items.Item("2")          'Cancel Button
            owItem.Top = iButtonTop
            'owItemControl.LinkTo = "23"
            owItemControl = owItem

            owItem = oForm.Items.Item("271")            'Related Service Call
            owItem.Left = owItemControl.Left + owItemControl.Width + 10
            owItem.LinkTo = "1"
            owItem.Top = iButtonTop
            owItemControl = owItem

            owItem = oForm.Items.Item("103")            'Activity
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.LinkTo = "1"
            owItem.Top = iButtonTop
            owItemControl = owItem

            owItem = oForm.Items.Item("104")            'Related Activities
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.LinkTo = "1"
            owItem.Top = iButtonTop
            owItemControl = owItem
            owItemControl = oForm.Items.Item("540002072")
            iButtonHeight = owItemControl.Height
            iButtonTop = owItemControl.Top
            owItem = oForm.Items.Add("IDH_WRBNT", SAPbouiCOM.BoFormItemTypes.it_BUTTON_COMBO)
            owItem.Left = owItemControl.Left - 162
            owItem.Top = iButtonTop
            owItem.Width = 160
            owItem.Height = iButtonHeight
            owItem.Visible = True
            owItem.DisplayDesc = True
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            Dim ComboBtn As SAPbouiCOM.ButtonCombo = owItem.Specific
            ComboBtn.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
            ComboBtn.Caption = Translation.getTranslatedWord("Waste & Recycle")
        End Sub

        '8.881 Layout  88106
        Private Sub doGeneralTabPlacement88106(ByRef oForm As SAPbouiCOM.Form)
            Dim owItemLabel As SAPbouiCOM.Item
            Dim owItemControl As SAPbouiCOM.Item
            Dim owItem As SAPbouiCOM.Item
            Dim oEdit As SAPbouiCOM.EditText
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim oCheck As SAPbouiCOM.CheckBox
            Dim oLink As SAPbouiCOM.LinkedButton
            Dim oButton As SAPbouiCOM.Button
            Dim iWidth As Integer
            Dim iHeight As Integer

            'Encrypted Password Field
            owItemControl = oForm.Items.Item("185")
            iWidth = owItemControl.Width / 2
            iHeight = owItemControl.Height

            ''EDIT FIRST COLUMN
            owItemControl.Width = iWidth
            oForm.Items.Add("IDH_EPAS", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem = doAddUF(oForm, "IDH_EPAS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 15, False, True)
            owItem.Left = owItemControl.Left + iWidth + 1
            owItem.Width = iWidth - 1
            owItem.Top = owItemControl.Top
            owItem.Height = owItemControl.Height
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.AffectsFormMode = True
            owItem.LinkTo = "185"
            oEdit = owItem.Specific
            oEdit.TabOrder = owItemControl.Specific.TabOrder + 1

            'Linked Items
            Try
                owItemControl = oForm.Items.Item("350001035")
                owItemControl.LinkTo = "223"
            Catch
                'Not in older versions
            End Try

            owItemControl = oForm.Items.Item("166") 'Active Checkbox
            owItemControl.LinkTo = "223"

            owItemControl = oForm.Items.Item("157") 'On Hold Checkbox
            owItemControl.LinkTo = "223"

            'THE LINKED BP
            owItemControl = oForm.Items.Item("222")             'Project
            owItem = oForm.Items.Add("IDHLBPC", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + (3 * (iHeight + 1))
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "222"
            doAddDF(oForm, "OCRD", "IDHLBPC", "U_IDHLBPC")
            owItemControl = owItem

            owItemLabel = oForm.Items.Item("223")               'Project Label
            owItem = oForm.Items.Add("2830", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width - 30
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("Linked BP Code")
            owItem.LinkTo = "IDHLBPC"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem

            owItem = oForm.Items.Add("IDHLBPL", SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)
            owItem.Height = iHeight - 1
            owItem.Width = 20
            owItem.Left = owItemControl.Left - 20
            owItem.Top = owItemControl.Top
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "IDHLBPC"
            oLink = owItem.Specific
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

            owItem = oForm.Items.Add("IDHLBPS", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 1
            owItem.Top = owItemControl.Top
            owItem.Width = 14
            owItem.Height = iHeight
            oButton = owItem.Specific
            oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            'Dim sImage As String = IDHAddOns.idh.addon.Base.IMAGEPATH & "\" & "CFL.bmp"
            'oButton.Image = sImage 'IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\" & "CFL.bmp"
            oButton.Image = IDHAddOns.idh.addon.Base.doFixImagePath("CFL.bmp")
            owItem.LinkTo = "IDHLBPC"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane

            owItem = oForm.Items.Add("IDHLBPN", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight + 1
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "IDHLBPC"
            doAddDF(oForm, "OCRD", "IDHLBPN", "U_IDHLBPN")
            owItemControl = owItem

            owItem = oForm.Items.Add("2831", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("Linked BP Name")
            owItem.LinkTo = "IDHLBPN"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem

            'BP Cost Centre 
            'Edit Text 
            owItemControl = oForm.Items.Item("222")             'Project
            owItem = oForm.Items.Add("IDHCSTC", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + (5 * (iHeight + 1))
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "222"
            doAddDF(oForm, "OCRD", "IDHCSTC", "U_COSTCNTR")
            owItemControl = owItem
            'Label
            owItemLabel = oForm.Items.Item("223")               'Project Label
            owItem = oForm.Items.Add("2832", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width - 30
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("BP Cost Centre")
            owItem.LinkTo = "IDHCSTC"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem
            'Link Button 
            owItem = oForm.Items.Add("IDHLCSC", SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)
            owItem.Height = iHeight - 1
            owItem.Width = 20
            owItem.Left = owItemControl.Left - 20
            owItem.Top = owItemControl.Top
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "IDHCSTC"
            oLink = owItem.Specific
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_ProfitCenter
            'Lookup btn
            owItem = oForm.Items.Add("IDHLCCS", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 1
            owItem.Top = owItemControl.Top
            owItem.Width = 14
            owItem.Height = iHeight
            oButton = owItem.Specific
            oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oButton.Image = IDHAddOns.idh.addon.Base.doFixImagePath("CFL.bmp")
            owItem.LinkTo = "IDHCSTC"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane

            ''EDIT SECOND COLUMN
            ''Set Links to keep items in place
            owItemControl = oForm.Items.Item("117")         'Sales Employee
            owItemControl.LinkTo = "43"                     'Link to the Telephone1 Field

            owItemControl = oForm.Items.Item("108")         'Remarks
            owItemControl.LinkTo = "117"                    'Link to the Sales Employee Field

            owItemControl = oForm.Items.Item("52")          'Sales Employee
            owItemLabel = oForm.Items.Item("59")            'Sales Employee Label

            owItem = oForm.Items.Add("IDHBRAN", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight - 1
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "52"
            doAddDF(oForm, "OCRD", "IDHBRAN", "U_IDHBRAN")
            owItemControl = owItem

            ''BRANCH CODE LABEL
            owItem = oForm.Items.Add("IDHBRLB", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDHBRAN"
            owItem.Specific.Caption = getTranslatedWord("Branch")
            owItemLabel = owItem

            ''OBLIGATION CODE COMBO
            owItem = oForm.Items.Add("IDHOBLED", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight + 1
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "IDHBRAN"
            doAddDF(oForm, "OCRD", "IDHOBLED", "U_IDHOBLGT")
            owItemControl = owItem

            ''OBLIGATION CODE LABEL
            owItem = oForm.Items.Add("IDHOBLLB", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDHOBLED"
            owItem.Specific.Caption = getTranslatedWord("Obligated")
            owItemLabel = owItem

            'OnTime [#Ico000????] USA 
            'START

            ''Fuel Surcharge 
            owItem = oForm.Items.Add("IDHFSCHK", SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + (owItemControl.Height * 4 + 4)
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.Specific.Caption = getTranslatedWord("Fuel Surcharge")
            oCheck = owItem.Specific
            oCheck.ValOn = "Y"
            oCheck.ValOff = "N"
            doAddDF(oForm, "OCRD", "IDHFSCHK", "U_FULSCHG")
            owItemLabel = owItem

            ''County Tariff 
            owItem = oForm.Items.Add("IDHCTCHK", SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.Specific.Caption = getTranslatedWord("County Tariff")
            oCheck = owItem.Specific
            oCheck.ValOn = "Y"
            oCheck.ValOff = "N"
            doAddDF(oForm, "OCRD", "IDHCTCHK", "U_COUNTAR")
            owItemLabel = owItem

            ''Port Tariff 
            owItem = oForm.Items.Add("IDHPTCHK", SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.Specific.Caption = getTranslatedWord("Port Tariff")
            oCheck = owItem.Specific
            oCheck.ValOn = "Y"
            oCheck.ValOff = "N"
            doAddDF(oForm, "OCRD", "IDHPTCHK", "U_PORTARF")
            owItemLabel = owItem

            'NOTE: clean the code before check-in 

            ''20120418 testing Default values for a field 
            'owItem = oForm.Items.Add("IDHTSTAD", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
            'owItem.Left = owItemLabel.Left
            'owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            'owItem.Width = owItemLabel.Width
            'owItem.Height = iHeight
            'owItem.FromPane = owItemLabel.FromPane
            'owItem.ToPane = owItemLabel.ToPane
            ''owItem.Specific.Caption = "TST ADD"
            'doAddDF(oForm, "CRD1", "IDHTSTAD", "U_MAILADD")
            'owItemLabel = owItem

            'END
            'OnTime [#Ico000????] USA 

            ''ADD THE THIRD COLUMN TO THE TAB
            Dim iPosLabel As Integer
            Dim iPosControl As Integer
            owItemControl = oForm.Items.Item("117") 'Active Checkbox
            owItemControl.LinkTo = "44"
            iPosLabel = owItemControl.Left + owItemControl.Width + 20
            iPosControl = owItemControl.Left + owItemControl.Width + 120

            '                owItem = oForm.Items.Add("IDHINTR", SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
            '                owItem.Left = iCol3LeftLbl
            '                owItem.Top = ow2Item.Top
            '                owItem.Width = 110
            '                owItem.Height = iHeight
            '                owItem.FromPane = ow2Item.FromPane
            '                owItem.ToPane = ow2Item.ToPane
            '                owItem.LinkTo = "114" '"166"
            '                doAddDF(oForm, "OCRD", "IDHINTR", "U_IDHINTR")
            '                oCheck = owItem.Specific
            '                oCheck.Caption = "Intra Company"
            '                oCheck.ValOn = "Y"
            '                oCheck.ValOff = "N"
            owItemControl = doAddDBCheck(oForm, "OCRD", "IDHINTR", "U_IDHINTR", owItemControl.FromPane, iPosLabel, owItemControl.Top, 160, iHeight, getTranslatedWord("Intra Company"), "113")
            
            ''UOM
            owItemControl = doAddDBEdit(oForm, "IDHUOM", "OCRD", "U_IDHUOM", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 60, iHeight, "IDHINTR")
            owItemLabel = doAddStatic(oForm, "S100", owItemControl.FromPane, iPosLabel, owItemControl.Top, 55, iHeight, getTranslatedWord("WR1 Uom"), "IDHUOM")

            'checkbox Use BP Address Premises as Consignment No
            owItemControl = doAddDBCheck(oForm, "OCRD", "UsBPAdCon", "U_UsBPAdCon", owItemControl.FromPane, iPosLabel, owItemControl.Top + ((iHeight + 2)), 230, iHeight, getTranslatedWord("Use BP Address Premises as Consignment No."), "IDHINTR")

            ''IS COMPLIANCE SCHEME
            owItemControl = doAddDBCheck(oForm, "OCRD", "IDHISCS", "U_IDHISCS", owItemControl.FromPane, iPosLabel, owItemControl.Top + ((iHeight + 1) * 3), 130, iHeight, getTranslatedWord("Is a Compliance Scheme"), "IDHINTR")

            ''ON COMPLIANCE SCHEME
            owItemControl = doAddDBEdit(oForm, "IDHONCS", "OCRD", "U_IDHONCS", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 60, iHeight, "IDHINTR")
            'ow2Item = doAddEdit(oForm, "IDHONCS", ow2Item.FromPane, iCol3Left, ow2Item.Top + 15, 60, iHeight, "IDHINTR")
            'doAddDF(oForm, "OCRD", "IDHONCS", "U_IDHONCS")
            owItemLabel = doAddStatic(oForm, "S101", owItemControl.FromPane, iPosLabel, owItemControl.Top, 80, iHeight, getTranslatedWord("Compl. Scheme"), "IDHONCS")

            owItem = oForm.Items.Add("IDHLCS", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 1
            owItem.Top = owItemControl.Top
            owItem.Width = 14
            owItem.Height = 14
            oButton = owItem.Specific
            oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            'oButton.Image = IDHAddOns.idh.addon.Base.IMAGEPATH & "\" & "CFL.bmp"
            oButton.Image = IDHAddOns.idh.addon.Base.doFixImagePath("CFL.bmp")
            owItem.LinkTo = "IDHONCS"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("IDHLBCS", SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)
            owItem.Height = 13
            owItem.Width = 20
            owItem.Left = owItemControl.Left - 20
            owItem.Top = owItemControl.Top
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "IDHONCS"
            oLink = owItem.Specific
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

            ''Values on WB / Job Ticket
            owItemControl = doAddDBCheck(oForm, "OCRD", "IDHDTDO", "U_IDHDTDO", owItemControl.FromPane, iPosLabel, owItemControl.Top + iHeight + 1, 160, iHeight, getTranslatedWord("Display Value on WB Ticket"), "IDHINTR")
            owItemControl = doAddDBCheck(oForm, "OCRD", "IDHDTWO", "U_IDHDTWO", owItemControl.FromPane, iPosLabel, owItemControl.Top + iHeight + 1, 160, iHeight, getTranslatedWord("Display Value on Job Ticket"), "IDHINTR")
            owItemControl = doAddDBCheck(oForm, "OCRD", "IDHCOMB", "U_IDHCOMB", owItemControl.FromPane, iPosLabel, owItemControl.Top + iHeight + 1, 160, iHeight, getTranslatedWord("Combine Order Lines"), "IDHINTR")

            owItemControl = doAddDBEdit(oForm, "IDHRTNG", "OCRD", "U_IDHRTNG", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 60, iHeight, "IDHCOMB")
            owItemLabel = doAddStatic(oForm, "S1101", owItemControl.FromPane, iPosLabel, owItemControl.Top, 80, iHeight, getTranslatedWord("Rating"), "IDHRTNG")

            owItemControl = doAddDBCombo(oForm, "IDHMDDT", "OCRD", "U_IDHMDDT", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 60, iHeight, "IDHCOMB")
            owItemControl.DisplayDesc = True
            oCombo = owItemControl.Specific
            oCombo.ValidValues.Add("", getTranslatedWord("Default"))
            oCombo.ValidValues.Add("NOW", getTranslatedWord("Current Date"))
            oCombo.ValidValues.Add("BDATE", getTranslatedWord("Booking Date"))
            oCombo.ValidValues.Add("ASDATE", getTranslatedWord("Job Start Date"))
            oCombo.ValidValues.Add("AEDATE", getTranslatedWord("Job Completion Date"))
            owItemLabel = doAddStatic(oForm, "S1201", owItemControl.FromPane, iPosLabel, owItemControl.Top, 80, iHeight, getTranslatedWord("Date Source"), "IDHMDDT")

            If Config.ParamaterWithDefault("TFSMOD", "FALSE") = "TFSMOD_TRUE" Then
                'TFS Registration Number 
                owItemControl = doAddDBEdit(oForm, "IDHTFSREG", "OCRD", "U_REGNO", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 120, iHeight, "IDHMDDT")
                owItemLabel = doAddStatic(oForm, "S1301", owItemControl.FromPane, iPosLabel, owItemControl.Top, 100, iHeight, getTranslatedWord("TFS Registration No."), "IDHTFSREG")

                'RGVLDFM and RGVLDTO
                owItemControl = doAddDBEdit(oForm, "IDHTFSVFM", "OCRD", "U_RGVLDFM", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 120, iHeight, "IDHMDDT")
                owItemLabel = doAddStatic(oForm, "S1302", owItemControl.FromPane, iPosLabel, owItemControl.Top, 100, iHeight, getTranslatedWord("TFS Reg. Valid From"), "IDHTFSVFM")

                owItemControl = doAddDBEdit(oForm, "IDHTFSVTO", "OCRD", "U_RGVLDTO", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 120, iHeight, "IDHMDDT")
                owItemLabel = doAddStatic(oForm, "S1303", owItemControl.FromPane, iPosLabel, owItemControl.Top, 100, iHeight, getTranslatedWord("TFS Reg. Valid To"), "IDHTFSVTO")
            End If

            '#MA Start 31-03-2017
            owItemControl = doAddDBEdit(oForm, "IDHAcMgEm", "OCRD", "U_ActMgrEml", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 120, iHeight, "IDHMDDT")
            owItemLabel = doAddStatic(oForm, "IDHEAcMgEm", owItemControl.FromPane, iPosLabel, owItemControl.Top, 120, iHeight, getTranslatedWord("Accnt Manager Email"), "IDHAcMgEm")

            owItemControl = doAddDBEdit(oForm, "IDHAcDrEm", "OCRD", "U_ActDirEml", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 120, iHeight, "IDHMDDT")
            owItemLabel = doAddStatic(oForm, "IDHEAcDrEm", owItemControl.FromPane, iPosLabel, owItemControl.Top, 120, iHeight, getTranslatedWord("Accnt Director Email"), "IDHAcDrEm")
            '#MA End 31-03-2017

        End Sub

        'Normal 2007 Layout
        Private Sub doGeneralTabPlacement(ByRef oForm As SAPbouiCOM.Form)
            Dim owItemLabel As SAPbouiCOM.Item
            Dim owItemControl As SAPbouiCOM.Item
            Dim owItem As SAPbouiCOM.Item
            Dim oEdit As SAPbouiCOM.EditText
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim oCheck As SAPbouiCOM.CheckBox
            Dim oLink As SAPbouiCOM.LinkedButton
            Dim oButton As SAPbouiCOM.Button
            Dim iWidth As Integer
            Dim iHeight As Integer

            'Encrypted Password Field
            owItemControl = oForm.Items.Item("185")
            iWidth = owItemControl.Width / 2
            iHeight = owItemControl.Height

            owItemControl.Width = iWidth
            oForm.Items.Add("IDH_EPAS", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem = doAddUF(oForm, "IDH_EPAS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 15, False, True)
            owItem.Left = owItemControl.Left + iWidth + 1
            owItem.Width = iWidth - 1
            owItem.Top = owItemControl.Top
            owItem.Height = owItemControl.Height
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.AffectsFormMode = True
            owItem.LinkTo = "185"
            oEdit = owItem.Specific
            oEdit.TabOrder = owItemControl.Specific.TabOrder + 1

            'Linked Items
            Try
                owItemControl = oForm.Items.Item("350001035")
                owItemControl.LinkTo = "223"
            Catch
                'Not in older versions
            End Try

            owItemControl = oForm.Items.Item("166") 'Active Checkbox
            owItemControl.LinkTo = "223"

            owItemControl = oForm.Items.Item("157") 'On Hold Checkbox
            owItemControl.LinkTo = "223"

            'THE LINKED BP
            owItemControl = oForm.Items.Item("222")             'Project
            owItem = oForm.Items.Add("IDHLBPC", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + (2 * (iHeight + 1))
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "222"
            doAddDF(oForm, "OCRD", "IDHLBPC", "U_IDHLBPC")
            owItemControl = owItem

            owItemLabel = oForm.Items.Item("223")               'Project Label
            owItem = oForm.Items.Add("2830", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width - 30
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("Linked BP Code")
            owItem.LinkTo = "IDHLBPC"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem

            owItem = oForm.Items.Add("IDHLBPL", SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)
            owItem.Height = iHeight - 1
            owItem.Width = 20
            owItem.Left = owItemControl.Left - 20
            owItem.Top = owItemControl.Top
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "IDHLBPC"
            oLink = owItem.Specific
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

            owItem = oForm.Items.Add("IDHLBPN", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight + 1
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "IDHLBPC"
            doAddDF(oForm, "OCRD", "IDHLBPN", "U_IDHLBPN")
            owItemControl = owItem

            owItem = oForm.Items.Add("2831", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("Linked BP Name")
            owItem.LinkTo = "IDHLBPN"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem

            owItem = oForm.Items.Add("IDHLBPS", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 1
            owItem.Top = owItemControl.Top
            owItem.Width = 14
            owItem.Height = iHeight
            oButton = owItem.Specific
            oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            'oButton.Image = IDHAddOns.idh.addon.Base.IMAGEPATH & "\" & "CFL.bmp"
            oButton.Image = IDHAddOns.idh.addon.Base.doFixImagePath("CFL.bmp")
            owItem.LinkTo = "IDHLBPC"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane


            'BP Cost Centre 
            'Edit Text 
            owItemControl = oForm.Items.Item("222")             'Project
            owItem = oForm.Items.Add("IDHCSTC", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + (4 * (iHeight + 1))
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "222"
            doAddDF(oForm, "OCRD", "IDHCSTC", "U_COSTCNTR")
            owItemControl = owItem
            'Label
            owItemLabel = oForm.Items.Item("223")               'Project Label
            owItem = oForm.Items.Add("2832", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width - 30
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("BP Cost Centre")
            owItem.LinkTo = "IDHCSTC"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem
            'Link Button 
            owItem = oForm.Items.Add("IDHLCSC", SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)
            owItem.Height = iHeight - 1
            owItem.Width = 20
            owItem.Left = owItemControl.Left - 20
            owItem.Top = owItemControl.Top
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "IDHCSTC"
            oLink = owItem.Specific
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner
            'Lookup btn
            owItem = oForm.Items.Add("IDHLCCS", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 1
            owItem.Top = owItemControl.Top
            owItem.Width = 14
            owItem.Height = iHeight
            oButton = owItem.Specific
            oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oButton.Image = IDHAddOns.idh.addon.Base.doFixImagePath("CFL.bmp")
            owItem.LinkTo = "IDHCSTC"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane

            ''EDIT SECOND COLUMN
            ''BRANCH CODE COMBO
            owItemControl = oForm.Items.Item("52")          'Sales Employee
            owItemLabel = oForm.Items.Item("59")            'Sales Employee Label

            owItem = oForm.Items.Add("IDHBRAN", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "52"
            doAddDF(oForm, "OCRD", "IDHBRAN", "U_IDHBRAN")
            owItemControl = owItem

            ''BRANCH CODE LABEL
            owItem = oForm.Items.Add("IDHBRLB", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDHBRAN"
            owItem.Specific.Caption = getTranslatedWord("Branch")
            owItemLabel = owItem

            ''OBLIGATION CODE COMBO
            owItem = oForm.Items.Add("IDHOBLED", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight + 1
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "IDHBRAN"
            doAddDF(oForm, "OCRD", "IDHOBLED", "U_IDHOBLGT")
            owItemControl = owItem

            ''OBLIGATION CODE LABEL
            owItem = oForm.Items.Add("IDHOBLLB", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDHOBLED"
            owItem.Specific.Caption = getTranslatedWord("Obligated")
            owItemLabel = owItem

            'OnTime [#Ico000????] USA 
            'START

            ''Fuel Surcharge 
            owItem = oForm.Items.Add("IDHFSCHK", SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + (owItemControl.Height * 4 + 4)
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.Specific.Caption = getTranslatedWord("Fuel Surcharge")
            oCheck = owItem.Specific
            oCheck.ValOn = "Y"
            oCheck.ValOff = "N"
            doAddDF(oForm, "OCRD", "IDHFSCHK", "U_FULSCHG")
            owItemLabel = owItem

            ''County Tariff 
            owItem = oForm.Items.Add("IDHCTCHK", SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.Specific.Caption = getTranslatedWord("County Tariff")
            oCheck = owItem.Specific
            oCheck.ValOn = "Y"
            oCheck.ValOff = "N"
            doAddDF(oForm, "OCRD", "IDHCTCHK", "U_COUNTAR")
            owItemLabel = owItem

            ''Port Tariff 
            owItem = oForm.Items.Add("IDHPTCHK", SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.Specific.Caption = getTranslatedWord("Port Tariff")
            oCheck = owItem.Specific
            oCheck.ValOn = "Y"
            oCheck.ValOff = "N"
            doAddDF(oForm, "OCRD", "IDHPTCHK", "U_PORTARF")
            owItemLabel = owItem

            'END
            'OnTime [#Ico000????] USA 

            ''ADD THE THIRD COLUMN TO THE TAB
            Dim iPosLabel As Integer
            Dim iPosControl As Integer
            owItemControl = oForm.Items.Item("117") 'Active Checkbox
            owItemControl.LinkTo = "44"
            iPosLabel = owItemControl.Left + owItemControl.Width + 20
            iPosControl = owItemControl.Left + owItemControl.Width + 120

            '                owItem = oForm.Items.Add("IDHINTR", SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
            '                owItem.Left = iCol3LeftLbl
            '                owItem.Top = ow2Item.Top
            '                owItem.Width = 110
            '                owItem.Height = iHeight
            '                owItem.FromPane = ow2Item.FromPane
            '                owItem.ToPane = ow2Item.ToPane
            '                owItem.LinkTo = "114" '"166"
            '                doAddDF(oForm, "OCRD", "IDHINTR", "U_IDHINTR")
            '                oCheck = owItem.Specific
            '                oCheck.Caption = "Intra Company"
            '                oCheck.ValOn = "Y"
            '                oCheck.ValOff = "N"
            owItemControl = doAddDBCheck(oForm, "OCRD", "IDHINTR", "U_IDHINTR", owItemControl.FromPane, iPosLabel, owItemControl.Top, 160, iHeight, getTranslatedWord("Intra Company"), "113")

            ''UOM
            owItemControl = doAddDBEdit(oForm, "IDHUOM", "OCRD", "U_IDHUOM", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 60, iHeight, "IDHINTR")
            owItemLabel = doAddStatic(oForm, "S100", owItemControl.FromPane, iPosLabel, owItemControl.Top, 55, iHeight, getTranslatedWord("WR1 Uom"), "IDHUOM")

            ''IS COMPLIANCE SCHEME
            owItemControl = doAddDBCheck(oForm, "OCRD", "IDHISCS", "U_IDHISCS", owItemControl.FromPane, iPosLabel, owItemControl.Top + ((iHeight + 1) * 3), 130, iHeight, getTranslatedWord("Is a Compliance Scheme"), "IDHINTR")

            ''ON COMPLIANCE SCHEME
            owItemControl = doAddDBEdit(oForm, "IDHONCS", "OCRD", "U_IDHONCS", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 60, iHeight, "IDHINTR")
            'ow2Item = doAddEdit(oForm, "IDHONCS", ow2Item.FromPane, iCol3Left, ow2Item.Top + 15, 60, iHeight, "IDHINTR")
            'doAddDF(oForm, "OCRD", "IDHONCS", "U_IDHONCS")
            owItemLabel = doAddStatic(oForm, "S101", owItemControl.FromPane, iPosLabel, owItemControl.Top, 80, iHeight, getTranslatedWord("Compl. Scheme"), "IDHONCS")

            owItem = oForm.Items.Add("IDHLCS", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 1
            owItem.Top = owItemControl.Top
            owItem.Width = 14
            owItem.Height = 14
            oButton = owItem.Specific
            oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            'oButton.Image = IDHAddOns.idh.addon.Base.IMAGEPATH & "\" & "CFL.bmp"
            oButton.Image = IDHAddOns.idh.addon.Base.doFixImagePath("CFL.bmp")
            owItem.LinkTo = "IDHONCS"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("IDHLBCS", SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)
            owItem.Height = 13
            owItem.Width = 20
            owItem.Left = owItemControl.Left - 20
            owItem.Top = owItemControl.Top
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "IDHONCS"
            oLink = owItem.Specific
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

            ''Values on WB / Job Ticket
            owItemControl = doAddDBCheck(oForm, "OCRD", "IDHDTDO", "U_IDHDTDO", owItemControl.FromPane, iPosLabel, owItemControl.Top + iHeight + 1, 160, iHeight, getTranslatedWord("Display Value on WB Ticket"), "IDHINTR")
            owItemControl = doAddDBCheck(oForm, "OCRD", "IDHDTWO", "U_IDHDTWO", owItemControl.FromPane, iPosLabel, owItemControl.Top + iHeight + 1, 160, iHeight, getTranslatedWord("Display Value on Job Ticket"), "IDHINTR")
            owItemControl = doAddDBCheck(oForm, "OCRD", "IDHCOMB", "U_IDHCOMB", owItemControl.FromPane, iPosLabel, owItemControl.Top + iHeight + 1, 160, iHeight, getTranslatedWord("Combine Order Lines"), "IDHINTR")

            owItemControl = doAddDBEdit(oForm, "IDHRTNG", "OCRD", "U_IDHRTNG", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 60, iHeight, "IDHCOMB")
            owItemLabel = doAddStatic(oForm, "S1101", owItemControl.FromPane, iPosLabel, owItemControl.Top, 80, iHeight, getTranslatedWord("Rating"), "IDHRTNG")

            owItem = doAddDBCombo(oForm, "IDHMDDT", "OCRD", "U_IDHMDDT", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 60, iHeight, "IDHCOMB")
            owItem.FromPane = 1
            owItem.ToPane = 1
            owItemControl = owItem
            owItem.DisplayDesc = True
            oCombo = owItem.Specific
            oCombo.ValidValues.Add("", getTranslatedWord("Default"))
            oCombo.ValidValues.Add("NOW", getTranslatedWord("Current Date"))
            oCombo.ValidValues.Add("BDATE", getTranslatedWord("Booking Date"))
            oCombo.ValidValues.Add("ASDATE", getTranslatedWord("Job Start Date"))
            oCombo.ValidValues.Add("AEDATE", getTranslatedWord("Job Completion Date"))
            owItemLabel = doAddStatic(oForm, "S1201", owItemControl.FromPane, iPosLabel, owItemControl.Top, 80, iHeight, getTranslatedWord("Date Source"), "IDHMDDT")

            If Config.ParamaterWithDefault("TFSMOD", "FALSE") = "TFSMOD_TRUE" Then
                'TFS Registration Number 
                owItemControl = doAddDBEdit(oForm, "IDHTFSREG", "OCRD", "U_REGNO", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 120, iHeight, "IDHMDDT")
                owItemLabel = doAddStatic(oForm, "S1301", owItemControl.FromPane, iPosLabel, owItemControl.Top, 100, iHeight, getTranslatedWord("TFS Registration No."), "IDHTFSREG")

                'RGVLDFM and RGVLDTO
                owItemControl = doAddDBEdit(oForm, "IDHTFSVFM", "OCRD", "U_RGVLDFM", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 120, iHeight, "IDHMDDT")
                owItemLabel = doAddStatic(oForm, "S1302", owItemControl.FromPane, iPosLabel, owItemControl.Top, 100, iHeight, getTranslatedWord("TFS Reg. Valid From"), "IDHTFSVFM")

                owItemControl = doAddDBEdit(oForm, "IDHTFSVTO", "OCRD", "U_RGVLDTO", owItemControl.FromPane, iPosControl, owItemControl.Top + iHeight + 1, 120, iHeight, "IDHMDDT")
                owItemLabel = doAddStatic(oForm, "S1303", owItemControl.FromPane, iPosLabel, owItemControl.Top, 100, iHeight, getTranslatedWord("TFS Reg. Valid To"), "IDHTFSVTO")
            End If
        End Sub

        Private Sub doAddressTabPLacement(ByRef oForm As SAPbouiCOM.Form)
            Dim owItemControl As SAPbouiCOM.Item
            'Dim owItemControl1 As SAPbouiCOM.Item
            Dim owItem As SAPbouiCOM.Item
            Dim iHeight As Integer = 14
            setVisible(oForm, "480002075", False)
            'oForm.Items.Item("480002075").Visible = False
            owItemControl = oForm.Items.Item("70")          'Set As Default Button
            ' owItemControl = oForm.Items.Item("480002075")          'Set As Default Button

            Dim iTop As Int16 = oForm.Items.Item("480002075").Top 'owItemControl.Top + owItemControl.Height + 5
            'If oForm.Items.Item("178").Top + oForm.Items.Item("178").Height + 5 > owItemControl.Top + owItemControl.Height + 5 Then
            '    iTop = oForm.Items.Item("178").Top + oForm.Items.Item("178").Height + 5
            'End If
            ''THE ADDRESS BUTTON
            owItem = oForm.Items.Add("IDH_BACOPY", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left '+ owItemControl.Width + 2
            owItem.Top = iTop
            owItem.Width = owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = False
            owItem.Specific.Caption = getTranslatedWord("Copy Billing To Ship")
            'owItem.LinkTo = "70"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            'owItemControl1 = oForm.Items.Item("178")			'Address Details Control

            owItem = oForm.Items.Add("IDH_WROSMC", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = owItemControl.Top '+ owItemControl1.Height + 15
            owItem.Width = owItemControl.Width      '80
            owItem.Height = owItemControl.Height     'iHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("WO's on Ref.")
            owItem.LinkTo = "70"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("IDH_WRDOMC", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = owItemControl.Top
            owItem.Width = owItemControl.Width      '80
            owItem.Height = owItemControl.Height    'iHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("DO's on Ref.")
            owItem.LinkTo = "70"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            'OnTime [#Ico000????] USA 
            'START
            'Waste Profile Button
            owItem = oForm.Items.Add("IDH_WSTPRF", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 2
            owItem.Top = owItemControl.Top
            owItem.Width = owItemControl.Width      '80
            owItem.Height = owItemControl.Height    'iHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("Waste Profile")
            owItem.LinkTo = "70"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            'Copy Address Button
            owItem = oForm.Items.Add("IDH_CPYADD", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left '+ owItemControl.Width + 2
            owItem.Top = owItemControl.Top + owItemControl.Height + 5
            owItem.Width = owItemControl.Width      '80
            owItem.Height = owItemControl.Height    'iHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("Copy From...")
            owItem.LinkTo = "70"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem
            'END 
            'OnTime [#Ico000????] USA 
            doTranslateAddress(oForm)


            'Copy Address Button
            'owItem = oForm.Items.Add("IDH_TEST", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            'owItem.Left = owItemControl.Left '+ owItemControl.Width + 2
            'owItem.Top = owItemControl.Top + owItemControl.Height + 5
            'owItem.Width = owItemControl.Width      '80
            'owItem.Height = owItemControl.Height    'iHeight
            'owItem.Visible = True
            'owItem.Specific.Caption = getTranslatedWord("Test")
            'owItem.LinkTo = "70"
            'owItem.FromPane = owItemControl.FromPane
            'owItem.ToPane = owItemControl.ToPane
            'owItemControl = owItem

            'Address Search Button(PostCode Lookup)
			owItemControl = oForm.Items.Item("178")
			owItem = oForm.Items.Add("IDH_POSTCS", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
			owItem.Left = owItemControl.Left + owItemControl.Width + 1
			owItem.Top = owItemControl.Top '+ owItemControl.Height + 5
			owItem.Width = 15      '80
			owItem.Height = 14   'iHeight
			owItem.Visible = True
			'owItem.Specific.Caption = getTranslatedWord("Copy From...")
			owItem.LinkTo = owItemControl.UniqueID
			owItem.FromPane = owItemControl.FromPane
			owItem.ToPane = owItemControl.ToPane
			Dim oButton As SAPbouiCOM.Button = owItem.Specific
			oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Image
			oButton.Caption = "Postcode"
			oButton.Image = IDHAddOns.idh.addon.Base.doFixImagePath("search.png") ' "Images\search.png"
            owItemControl = owItem

            'Address Search Button(PostCode Lookup)
            Dim owItemControl1 As SAPbouiCOM.Item = oForm.Items.Item("1")
            owItemControl = oForm.Items.Item("178")
            owItem = oForm.Items.Add("IDH_ALTADR", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 1
            owItem.Top = owItemControl.Top + 18 '+ owItemControl.Height + 5
            owItem.Width = 100      '80
            owItem.Height = owItemControl1.Height 'iHeight
            owItem.Visible = True
            'owItem.Specific.Caption = getTranslatedWord("Copy From...")
            owItem.LinkTo = owItemControl.UniqueID
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            oButton = owItem.Specific
            oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Caption
            oButton.Caption = "Supplier Detail"
        End Sub
        Private Sub doAddressTabPLacementnew(ByRef oForm As SAPbouiCOM.Form)
            Dim owItemControl As SAPbouiCOM.Item
            'Dim owItemControl1 As SAPbouiCOM.Item
            Dim owItem As SAPbouiCOM.Item
            Dim iHeight As Integer = 14
            setVisible(oForm, "480002075", False)
            'oForm.Items.Item("480002075").Visible = False
            owItemControl = oForm.Items.Item("70")          'Set As Default Button
            ' owItemControl = oForm.Items.Item("480002075")          'Set As Default Button
            owItemControl.Width = 96           'Set default button width 
            Dim iTop As Int16 = oForm.Items.Item("480002075").Top 'owItemControl.Top + owItemControl.Height + 5
            'If oForm.Items.Item("178").Top + oForm.Items.Item("178").Height + 5 > owItemControl.Top + owItemControl.Height + 5 Then
            '    iTop = oForm.Items.Item("178").Top + oForm.Items.Item("178").Height + 5
            'End If
            '#MA Start 22-06-20147
            ''THE ADDRESS Tab WR1 CORE BUTTON
            owItem = oForm.Items.Add("IDH_WRADD", SAPbouiCOM.BoFormItemTypes.it_BUTTON_COMBO)
            owItem.Left = owItemControl.Left + 98
            owItem.Top = owItemControl.Top
            owItem.Width = 170
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.DisplayDesc = True
            Dim ComboBtn As SAPbouiCOM.ButtonCombo = owItem.Specific
            ComboBtn.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
            ComboBtn.Caption = Translation.getTranslatedWord("Waste & Recycle")
            '#MA End 22-06-2017
            'Address Search Button(PostCode Lookup)
            owItemControl = oForm.Items.Item("178")
            owItem = oForm.Items.Add("IDH_POSTCS", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 1
            owItem.Top = owItemControl.Top '+ owItemControl.Height + 5
            owItem.Width = 15      '80
            owItem.Height = 14   'iHeight
            owItem.Visible = True
            'owItem.Specific.Caption = getTranslatedWord("Copy From...")
            owItem.LinkTo = owItemControl.UniqueID
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            Dim oButton As SAPbouiCOM.Button = owItem.Specific
            oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oButton.Caption = "Postcode"
            oButton.Image = IDHAddOns.idh.addon.Base.doFixImagePath("search.png") ' "Images\search.png"
            owItemControl = owItem

            'Address Search Button(PostCode Lookup)
            Dim owItemControl1 As SAPbouiCOM.Item = oForm.Items.Item("1")
            owItemControl = oForm.Items.Item("178")
            owItem = oForm.Items.Add("IDH_ALTADR", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = owItemControl.Left + owItemControl.Width + 1
            owItem.Top = owItemControl.Top + 18 '+ owItemControl.Height + 5
            owItem.Width = 100      '80
            owItem.Height = owItemControl1.Height 'iHeight
            owItem.Visible = True
            'owItem.Specific.Caption = getTranslatedWord("Copy From...")
            owItem.LinkTo = owItemControl.UniqueID
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            oButton = owItem.Specific
            oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Caption
            oButton.Caption = "Supplier Detail"
        End Sub
        Private Sub doPaymentTermsTabPlacement(ByRef oForm As SAPbouiCOM.Form)
            Dim owItemLabel As SAPbouiCOM.Item
            Dim owItemControl As SAPbouiCOM.Item
            Dim owItem As SAPbouiCOM.Item

            Dim oCheck As SAPbouiCOM.CheckBox
            Dim oEdit As SAPbouiCOM.EditText
            'Dim iWidth As Integer
            Dim iHeight As Integer

            'Credit limit deviation
            owItemControl = oForm.Items.Item("331")         'Dunning Term
            iHeight = owItemControl.Height
            owItem = oForm.Items.Add("IDH_CRDEV", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight + 1
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.Visible = True
            owItem.ToPane = owItemControl.ToPane
            owItem.FromPane = owItemControl.FromPane
            owItem.AffectsFormMode = True
            owItem.LinkTo = "331"
            oEdit = owItem.Specific
            oEdit.TabOrder = owItemControl.Specific.TabOrder + 1
            oEdit.DataBind.SetBound(True, "OCRD", "U_IDHCRDV")

            owItemLabel = oForm.Items.Item("330")           'Dunning Term Label
            owItem = oForm.Items.Add("IDH_CDLB", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + iHeight + 2
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("Credit Deviation %")
            owItem.ToPane = owItemLabel.ToPane
            owItem.FromPane = owItemLabel.FromPane
            owItem.LinkTo = "IDH_CRDEV"

            'Linking fields to assist with resize
            Try
                owItemControl = oForm.Items.Item("210000435")       'Control Internal ID
                owItemControl.LinkTo = "75"                     'Link to Payment Terms
            Catch ee As Exception
            End Try

            owItemControl = oForm.Items.Item("153")         'Credit Card Type
            owItemControl.LinkTo = "75"                     'Link to Payment Terms

            owItemControl = oForm.Items.Item("154")         'Credit Card No
            owItemControl.LinkTo = "75"                     'Link to Payment Terms

            owItemControl = oForm.Items.Item("155")         'Expiry Date
            owItemControl.LinkTo = "75"                     'Link to Payment Terms

            owItemControl = oForm.Items.Item("211")         'ID Number
            owItemControl.LinkTo = "75"                     'Link to Payment Terms

            owItemLabel = oForm.Items.Item("212")           'ID Number Label

            owItem = oForm.Items.Add("IDHCCIs", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight + 1
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "211"
            doAddDF(oForm, "OCRD", "IDHCCIs", "U_IDHCCIs")
            owItemControl = owItem

            owItem = oForm.Items.Add("2840", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("Issue#")
            owItem.LinkTo = "IDHCCIs"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem

            owItem = oForm.Items.Add("IDHCCSec", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight + 1
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "211"
            doAddDF(oForm, "OCRD", "IDHCCSec", "U_IDHCCSec")
            owItemControl = owItem

            owItem = oForm.Items.Add("2841", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("Security No.")
            owItem.LinkTo = "IDHCCSec"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem

            owItem = oForm.Items.Add("IDHCCHNm", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight + 1
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "211"
            doAddDF(oForm, "OCRD", "IDHCCHNm", "U_IDHCCHNm")
            owItemControl = owItem

            owItem = oForm.Items.Add("2842", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("CC Home Number")
            owItem.LinkTo = "IDHCCHNm"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem

            owItem = oForm.Items.Add("IDHCCPCd", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight + 1
            owItem.Width = owItemControl.Width
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "211"
            doAddDF(oForm, "OCRD", "IDHCCPCd", "U_IDHCCPCd")
            owItemControl = owItem

            owItem = oForm.Items.Add("2843", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = owItemLabel.Width
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("CC Zip Code")
            owItem.LinkTo = "IDHCCPCd"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem

            'Move to create space for the new CC fields
            '********* CREDIT CARD **************
            owItem = oForm.Items.Item("283")                'Holidays
            owItem.Top = owItem.Top + (4 * (iHeight + 1))
            owItem.LinkTo = "211"
            owItem = oForm.Items.Item("284")                'Holidays Label
            owItem.Top = owItem.Top + (4 * (iHeight + 1))

            owItem = oForm.Items.Item("187")                'Invoice Frequency
            owItem.Top = owItem.Top + (4 * (iHeight + 1))
            owItem.LinkTo = "211"
            owItem = oForm.Items.Item("186")                'Invoice Frequency Label
            owItem.Top = owItem.Top + (4 * (iHeight + 1))

            owItem = oForm.Items.Item("179")                'Invoicing
            owItem.Top = owItem.Top + (4 * (iHeight + 1))
            owItem.LinkTo = "211"
            owItem = oForm.Items.Item("72")                 'Invoicing Label
            owItem.Top = owItem.Top + (4 * (iHeight + 1))


            owItem = oForm.Items.Item("145")                'Average Delay
            owItem.Top = owItem.Top + (4 * (iHeight + 1))
            owItem.LinkTo = "211"
            owItem = oForm.Items.Item("144")                'Average Delay Label
            owItem.Top = owItem.Top + (4 * (iHeight + 1))

            owItemControl = oForm.Items.Item("23")
            owItem = oForm.Items.Item("191")                'Partial Delivery Of Sales Order
            owItem.Top = owItemControl.Top - (2 * iHeight + 5)
            owItem.LinkTo = "211"
            owItem = oForm.Items.Item("192")                'Partial Delivery Per Row
            owItem.Top = owItemControl.Top - (iHeight + 5)
            owItem.LinkTo = "211"

            owItemControl = oForm.Items.Item("153") 'Credit Card Type
            Dim iLabelLeft As Integer = owItemControl.Left + owItemControl.Width + 15

            owItem = oForm.Items.Add("IDH_ADDCC", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            owItem.Left = iLabelLeft + 130
            owItem.Top = owItemControl.Top
            owItem.Width = 20
            owItem.Height = iHeight
            owItem.Visible = True
            owItem.Specific.Caption = "..."
            owItem.LinkTo = "280"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemControl = owItem

            owItem = oForm.Items.Add("IDH_STCC", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = iLabelLeft
            owItem.Top = owItemControl.Top
            owItem.Width = 130
            owItem.Height = iHeight
            owItem.Visible = True
            owItem.Specific.Caption = getTranslatedWord("Additional CC's")
            owItem.LinkTo = "IDH_ADDCC"
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItemLabel = owItem

            'Move the Payment Date Fields
            owItem = oForm.Items.Item("280")    'Payment Dates Button
            owItem.Top = owItemControl.Top + iHeight + 1
            owItem.Width = owItemControl.Width
            owItem.Left = owItemControl.Left
            owItemControl = owItem

            owItem = oForm.Items.Item("282")    'Payment Dates Label
            owItem.Top = owItemLabel.Top + iHeight + 1
            owItem.Width = owItemLabel.Width
            owItem.Left = owItemLabel.Left
            owItemLabel = owItem

            'Price Group
            owItem = oForm.Items.Add("IDHPRGRP", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + (iHeight + 1)
            owItem.Width = 100
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "280"
            owItem.DisplayDesc = True
            doAddDF(oForm, "OCRD", "IDHPRGRP", "U_IDHPGRP")
            owItemControl = owItem

            owItem = oForm.Items.Add("3840", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = 130
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("Price Group ")
            owItem.LinkTo = "IDHPRGRP"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem

            owItem = oForm.Items.Add("IDHDCON", SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + (iHeight + 1)
            owItem.Width = 17
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "280"
            doAddDF(oForm, "OCRD", "IDHDCON", "U_IDHDCON")
            oCheck = owItem.Specific
            oCheck.ValOn = "Y"
            oCheck.ValOff = "N"
            owItemControl = owItem

            owItem = oForm.Items.Add("2820", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = 130
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("Charge Congestion ")
            owItem.LinkTo = "IDHDCON"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem

            owItem = oForm.Items.Add("IDHICL", SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + iHeight + 1
            owItem.Width = 17
            owItem.Height = iHeight
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            owItem.LinkTo = "IDHDCON"
            doAddDF(oForm, "OCRD", "IDHICL", "U_IDHICL")
            oCheck = owItem.Specific
            oCheck.ValOn = "Y"
            oCheck.ValOff = "N"
            owItemControl = owItem

            owItem = oForm.Items.Add("2821", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemControl.Top
            owItem.Width = 130 'ow2Item.Width
            owItem.Height = iHeight
            owItem.Specific.Caption = getTranslatedWord("Don't Do WR1 Creditcheck")
            owItem.LinkTo = "IDHICL"
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItemLabel = owItem
        End Sub

        Private Sub doSetECPassword(ByRef oForm As SAPbouiCOM.Form)
            Dim sPass As String = getDFValue(oForm, "OCRD", "Password")
            Dim sECPass As String = ""
            If sPass.Length > 0 Then
                For iPos As Integer = 0 To sPass.Length - 1
                    sECPass = sECPass & "*"
                Next
            End If
            setUFValue(oForm, "IDH_EPAS", sECPass)
        End Sub

        Public Sub HandleFormDataEvent(ByVal oForm As SAPbouiCOM.Form, ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
            Try
                If Config.ParamaterWithDefault("ENBADSYN", "FALSE") = "FALSE" Then
                    Return ''do not update address
                End If
                Dim sCardCode As String = oForm.Items.Item("5").Specific.Value
                sCardCode = getDFValue(oForm, "OCRD", "CardCode")
                Dim sAddressList As String = ""
                Dim sAddressLog As Hashtable
                If getSharedData(oForm, "sAddressLog") IsNot Nothing Then
                    sAddressLog = getSharedData(oForm, "sAddressLog")
                    For Each entry As Collections.DictionaryEntry In sAddressLog
                        If entry.Value = sCardCode Then
                            sAddressList &= entry.Key & ","
                        End If
                    Next
                    If sAddressList.Trim <> String.Empty Then
                        sAddressList = sAddressList.Trim(",")
                        sAddressList = " And CRD1.U_AddrsCd in (" & sAddressList & ") "
                    End If
                    sAddressLog.Clear()
                    setSharedData(oForm, "sAddressLog", sAddressLog)
                End If
                If String.IsNullOrWhiteSpace(sAddressList) Then
                    Return
                End If
                Dim sSQL As String = "select CRD1.U_AddrsCd,[Address],Street,Block,City, County,[State], ZipCode from CRD1 WITH(NOLOCK) Where " _
                                    & " CRD1.cardCode='" & sCardCode & "' And CRD1.ObjType=2 " _
                                    & " And AdresType='S' " & sAddressList
                Dim oRecordSet As SAPbobsCOM.Recordset
                oRecordSet = DataHandler.INSTANCE.doSBOSelectQuery("doGetBPAllAddresses", sSQL)

                If oRecordSet Is Nothing OrElse oRecordSet.RecordCount = 0 Then
                    Return
                End If
                oForm.Freeze(True)
                ''..................................................................
                Dim bAnyAddresstoUpdate As Boolean = False
                Dim sOldAddress As String, sNewAddress As String
                Dim sOldStreet As String, sNewStreet As String
                Dim sOldBlock As String, sNewBlock As String
                Dim sOldCity As String, sNewCity As String
                Dim sOldCounty As String, sNewCounty As String
                Dim sOldState As String, sNewState As String
                Dim sOldPostcode As String, sNewPostcode As String
                Dim sAddrsCd As String
                Dim oRecordSetOldAddress As SAPbobsCOM.Recordset

                While Not oRecordSet.EoF
                    sNewAddress = oRecordSet.Fields.Item("Address").Value
                    sNewStreet = oRecordSet.Fields.Item("Street").Value
                    sNewBlock = oRecordSet.Fields.Item("Block").Value
                    sNewCity = oRecordSet.Fields.Item("City").Value
                    sNewCounty = oRecordSet.Fields.Item("County").Value
                    sNewState = oRecordSet.Fields.Item("State").Value
                    sNewPostcode = oRecordSet.Fields.Item("ZipCode").Value

                    sAddrsCd = oRecordSet.Fields.Item("U_AddrsCd").Value
                    sSQL = "select top 1 ACR1.Address,Street,Block,City, County,[State], ZipCode " _
                                & " From ACR1 WITH(NOLOCK)" _
                                & " Where ACR1.cardCode='" & sCardCode & "' And ACR1.ObjType=2 " _
                                & " And ACR1.U_AddrsCd=" & sAddrsCd & " And AdresType='S' " _
                                & "	Order by LogInstanc Desc "
                    oRecordSetOldAddress = DataHandler.INSTANCE.doSBOSelectQuery("doGetBPAllAddresses", sSQL)
                    If oRecordSetOldAddress IsNot Nothing AndAlso oRecordSetOldAddress.RecordCount > 0 Then
                        sOldAddress = oRecordSetOldAddress.Fields.Item("Address").Value
                        sOldStreet = oRecordSetOldAddress.Fields.Item("Street").Value
                        sOldBlock = oRecordSetOldAddress.Fields.Item("Block").Value
                        sOldCity = oRecordSetOldAddress.Fields.Item("City").Value
                        sOldCounty = oRecordSetOldAddress.Fields.Item("County").Value
                        sOldState = oRecordSetOldAddress.Fields.Item("State").Value
                        sOldPostcode = oRecordSetOldAddress.Fields.Item("ZipCode").Value

                        If sOldAddress <> sNewAddress OrElse sOldStreet <> sNewStreet OrElse sOldBlock <> sNewBlock OrElse sOldCity <> sNewCity _
                            OrElse sOldCounty <> sNewCounty OrElse sOldState <> sNewState OrElse sOldPostcode <> sNewPostcode Then 'Update address
                            bAnyAddresstoUpdate = True
                            Exit While
                        End If
                    End If
                    oRecordSet.MoveNext()
                End While
                If bAnyAddresstoUpdate = False Then
                    Exit Try
                End If
                ''......................................................................
                oRecordSet.MoveFirst()
                setSharedData(oForm, "oRecordSet", oRecordSet)
                setSharedData(oForm, "sCardCode", sCardCode)

                doWarnMess(getGMessage("WRNBPA1", Nothing), SAPbouiCOM.BoMessageTime.bmt_Short)
                goParent.doOpenModalForm("IDH_BPADRUP", oForm) 'IDH_BPADRUP

            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREUPADR", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
        End Sub
        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            'If getWFValue(oForm, "WR1REFRESH") = True AndAlso pVal.BeforeAction = False Then
            '    doShowWR1(oForm, True)
            'End If
            'IDHAddOns.idh.forms.Base.getSharedDataAsString(oForm, "SETENQIDs")
            '#MA Start 19-06-2017

            If pVal.FormMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Or pVal.FormMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then   '#MA3 Start 18-07-2017 Point#602 Reset Caption of Button After Model Forms Closed.
                Dim oItm As SAPbouiCOM.Item = oForm.Items.Item("IDH_WRBNT")
                Dim oCombo As SAPbouiCOM.ButtonCombo = oItm.Specific
                If oItm.Visible = True Then
                    oCombo.Caption = Translation.getTranslatedWord("Waste & Recycle")
                End If
                oItm = oForm.Items.Item("IDH_WRADD")
                oCombo = oItm.Specific
                If oItm.Visible = True Then
                    oCombo.Caption = Translation.getTranslatedWord("Waste & Recycle")
                End If
            End If                                                                                                             '#MA3 End 18-07-2017
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT And pVal.BeforeAction = True Then
                If pVal.ItemUID = "IDH_WRBNT" Then
                    Dim oitm As SAPbouiCOM.Item = oForm.Items.Item("IDH_WRBNT")
                    Dim combo As SAPbouiCOM.ButtonCombo = oitm.Specific
                    Dim oValue As String = combo.ValidValues.Item(pVal.PopUpIndicator).Value

                    If oValue = "IDH_COMMERICALS" Then
                        If getDFValue(oForm, "OCRD", "CardCode").ToString() = "" Then
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please Select Customer or Supplier", "WRINNOBP", New String() {"Customer Code"})
                            Exit Function
                        End If
                        If getDFValue(oForm, "OCRD", "CardType").Equals("L") Then '#MA issue:325 20170328
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please Select Customer or Supplier for Commercial Search", "WRINNOBP", New String() {"Customer "})
                        Else
                            setSharedData(oForm, "CardCode", getDFValue(oForm, "OCRD", "CardCode").ToString())
                            setSharedData(oForm, "CardName", getDFValue(oForm, "OCRD", "CardName").ToString())
                            'setSharedData(oForm, "AdrCod", getDFValue(oForm, "CRD1", "U_AddrsCd").ToString())
                            'Dim zipCd As String = getDFValue(oForm, "CRD1", "ZipCode").ToString()
                            'setSharedData(oForm, "AddressZipCode", getDFValue(oForm, "CRD1", "ZipCode").ToString())
                            'setSharedData(oForm, "Address", getDFValue(oForm, "CRD1", "Address").ToString())
                            'setSharedData(oForm, "AddressType", getDFValue(oForm, "CRD1", "AdresType").ToString())
                            'setSharedData(oForm, "call", True)
                            goParent.doOpenModalForm("IDH_COMMERCIALS", oForm)

                        End If
                    ElseIf oValue = "IDH_PBI" Then
                        Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                        Dim sCardName As String = getDFValue(oForm, "OCRD", "CardName")
                        setSharedData(oForm, "e_CardCode", sCardCode)
                        setSharedData(oForm, "e_CardName", sCardName)
                        setSharedData(oForm, "TRG", "IDH_PBM")
                        goParent.doOpenModalForm("IDH_PBM", oForm)
                    ElseIf oValue = "IDH_OSM" Then
                        Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                        Dim sCardType As String = getDFValue(oForm, "OCRD", "CardType")

                        '20151026: Remarked block below as it was causing issues where 
                        'setting the Sup.Code was causing an extended where clause in manager.Templ.vb to be concatinated 
                        'Therefore have to change this to one below the remarked block 
                        'Also have change the concatination block in manager.Templ.vb Ln: 788 
                        'If sCardType.Equals("C") Then
                        '    setSharedData(oForm, "IDH_CUST", sCardCode)
                        '    setSharedData(oForm, "IDH_CARCD", "")
                        'setSharedData(oForm, "SUPPLIERS", sCardCode) ''MA 20-11-2014
                        'Else
                        '    setSharedData(oForm, "IDH_CUST", "")
                        '    setSharedData(oForm, "IDH_CARCD", "") 'sCardCode)
                        '    setSharedData(oForm, "SUPPLIERS", sCardCode)
                        'End If
                        If sCardType.Equals("C") Then
                            setSharedData(oForm, "IDH_CUST", sCardCode)
                        Else
                            setSharedData(oForm, "SUPPLIERS", sCardCode)
                        End If

                        setSharedData(oForm, "IDH_ADDR", "")
                        Dim sOSM = Config.Parameter("OSMBP")
                        If sOSM Is Nothing OrElse sOSM.Length = 0 Then
                            sOSM = "IDHJOBR"
                        End If
                        goParent.doOpenModalForm(sOSM, oForm)

                    ElseIf oValue = "IDH_DOMB" Then
                        Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                        setSharedData(oForm, "IDH_CUST", sCardCode)
                        setSharedData(oForm, "IDH_ADDR", "")
                        goParent.doOpenModalForm("IDHDOM", oForm)
                    ElseIf oValue = "IDH_ONSITE" Then
                        Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                        setSharedData(oForm, "IDH_CUST", sCardCode)
                        'goParent.doOpenModalForm("IDHONST", oForm)
                        goParent.doOpenForm("IDHONST", oForm, False)
                    ElseIf oValue = "IDH_TRANCD" Then
                        Dim oOOForm As com.uBC.forms.fr3.admin.BPTransactionCodesLink = New com.uBC.forms.fr3.admin.BPTransactionCodesLink(Nothing, oForm.UniqueID, Nothing)
                        Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                        oOOForm.BPCode = sCardCode
                        oOOForm.doShowModal()
                    ElseIf oValue = "IDH_ADDCHG" Then
                        Dim oOOForm As com.uBC.forms.fr3.admin.BPAdditionalCharges = New com.uBC.forms.fr3.admin.BPAdditionalCharges(Nothing, oForm.UniqueID, Nothing)
                        Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                        Dim sCardName As String = getDFValue(oForm, "OCRD", "CardName")
                        oOOForm.BPCode = sCardCode
                        oOOForm.BPName = sCardName
                        oOOForm.doShowModal()
                    ElseIf oValue = "IDH_CIP" Then
                        Dim sType As String = getDFValue(oForm, "OCRD", "CardType")
                        Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                        If sType = "C" Then
                            Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.prices.CIP = New WR1_FR2Forms.idh.forms.fr2.prices.CIP(Nothing, oForm.UniqueID, Nothing)
                            oOOForm.CustomerCode = sCardCode
                            oOOForm.FilterMode = 1
                            oOOForm.doShowModal()
                        ElseIf sType = "S" Then
                            Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.prices.SIP = New WR1_FR2Forms.idh.forms.fr2.prices.SIP(Nothing, oForm.UniqueID, Nothing)
                            oOOForm.SupplierCode = sCardCode
                            oOOForm.FilterMode = 1
                            oOOForm.doShowModal()
                        End If
                    ElseIf oValue = "IDH_CWABT" Then
                        Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                        Dim sSiteAddr As String = ""

                        If sCardCode.Trim().Length > 0 Then
                            Dim sReportFile As String
                            sReportFile = Config.Parameter("WOCWAB")

                            Dim oParams As New Hashtable
                            oParams.Add("Customer", sCardCode)
                            oParams.Add("SiteAddr", sSiteAddr)

                            If (Config.INSTANCE.useNewReportViewer()) Then
                                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                            Else
                                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                            End If
                        End If
                    End If
                ElseIf pVal.ItemUID = "IDH_WRADD" Then
                    Dim oitm As SAPbouiCOM.Item = oForm.Items.Item("IDH_WRADD")
                    Dim combo As SAPbouiCOM.ButtonCombo = oitm.Specific
                    Dim oValue As String = combo.ValidValues.Item(pVal.PopUpIndicator).Value
                    If oValue = "IDH_BACOPY" Then
                        '#MA2 issue:390 20170620
                        'Address Code assigned when field is Editable
                        doSwitchAddressCode(oForm, True)
                        doCopyBPAddress(oForm)
                        doShowWR1New(oForm, True, "")
                    ElseIf oForm.PaneLevel = 7 Then
                        Dim sType As String = "" 'getDFValue(oForm, "CRD1", "AdresType")
                        Dim sAddress As String = "" ' getDFValue(oForm, "CRD1", "Address")
                        Dim aExtraFields As New ArrayList
                        aExtraFields.Add("U_ISBCUSTRN")
                        GetSelectedAddressAndType(oForm, sAddress, sType, aExtraFields)
                        Dim U_ISBCUSTRN As String = aExtraFields(0)
                        'Dim sType As String = getDFValue(oForm, "CRD1", "AdresType")
                        'Dim sAddress As String = getDFValue(oForm, "CRD1", "Address")
                        If oValue = "IDH_WROSMC" AndAlso sAddress <> getTranslatedWord("Define New") Then
                            If sType = "S" Then
                                'setSharedData(oForm, "IDH_CUST", getDFValue(oForm, "CRD1", "CardCode"))
                                'setSharedData(oForm, "IDH_ADDR", getDFValue(oForm, "CRD1", "Address"))
                                'setSharedData(oForm, "IDH_CUSTREF", getDFValue(oForm, "CRD1", "U_ISBCUSTRN"))
                                setSharedData(oForm, "IDH_CUST", getDFValue(oForm, "CRD1", "CardCode"))
                                setSharedData(oForm, "IDH_ADDR", sAddress)
                                setSharedData(oForm, "IDH_CUSTREF", U_ISBCUSTRN)
                                Dim sOSM = Config.Parameter("OSMB")
                                If sOSM Is Nothing OrElse sOSM.Length = 0 Then
                                    sOSM = "IDHJOBR"
                                End If
                                goParent.doOpenModalForm(sOSM, oForm)
                            Else
                                doWarnMess("This Function can only be used with Ship to Addresses", SAPbouiCOM.BoMessageTime.bmt_Short)
                            End If
                        ElseIf oValue = "IDH_WRDOMC" AndAlso sAddress <> getTranslatedWord("Define New") Then
                            If sType = "S" Then
                                setSharedData(oForm, "IDH_CUST", getDFValue(oForm, "CRD1", "CardCode"))
                                setSharedData(oForm, "IDH_ADDR", sAddress)
                                setSharedData(oForm, "IDH_CUSTREF", U_ISBCUSTRN)
                                goParent.doOpenModalForm("IDHDOM", oForm)
                            Else
                                doWarnMess("This Function can only be used with Ship to Addresses", SAPbouiCOM.BoMessageTime.bmt_Short)
                            End If
                        ElseIf oValue = "IDH_WSTPRF" AndAlso sAddress <> getTranslatedWord("Define New") Then
                            If sType = "S" Then
                                Dim sBPCd As String = getDFValue(oForm, "OCRD", "CardCode")
                                If sBPCd.Contains("'") Then
                                    sBPCd = sBPCd.Replace("'", "''")
                                End If
                                setSharedData(oForm, "IDH_CUST", sBPCd)
                                Dim sBPNm As String = getDFValue(oForm, "OCRD", "CardName")
                                If sBPNm.Contains("'") Then
                                    sBPNm = sBPNm.Replace("'", "''")
                                End If
                                setSharedData(oForm, "IDH_CUSTNM", sBPNm)
                                'Dim sAdd As String = getDFValue(oForm, "CRD1", "Address")
                                'If sAdd.Contains("'") Then
                                '    sAdd = sAdd.Replace("'", "''")
                                'End If
                                setSharedData(oForm, "IDH_ADDR", sAddress)
                                goParent.doOpenModalForm("IDHWPRS", oForm)
                            Else
                                doWarnMess("This Function can only be used with Ship to Addresses", SAPbouiCOM.BoMessageTime.bmt_Short)
                            End If
                        ElseIf oValue = "IDH_CPYADD" Then
                            If sAddress = getTranslatedWord("Define New") Then
                                setSharedData(oForm, "CPYTRG", sType)
                                setSharedData(oForm, "IDH_ADRTYP", sType)
                                goParent.doOpenModalForm("IDHASRCH2", oForm)
                            Else
                                doWarnMess("This function can only be used when 'Define New' address is selected.", SAPbouiCOM.BoMessageTime.bmt_Short)
                            End If
                            'ElseIf oValue = "Supplier Detail" Then
                            '    doLoadAlterNateAddress(oForm)

                        End If
                        'End With
                    End If


                End If

            End If
            '#MA End 21-06-2017
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE AndAlso pVal.BeforeAction = False AndAlso getSharedDataAsString(oForm, "SETENQIDs") = "Yes" _
                    AndAlso getSharedData(oForm, "AddressDetail") IsNot Nothing Then
                Dim bUpdateMode As Boolean = False
                Try
                    oForm.Freeze(True)
                    'setSharedData(oOOForm.SBOParentForm, "AddressDetail", BPCompleteAddress.obj)
                    setBPAddressFromPOstCodeLookup(oForm)
                Catch ex As Exception
                Finally
                    oForm.Freeze(False)
                End Try
                setSharedData(oForm, "SETENQIDs", "")
                setSharedData(oForm, "AddressDetail", Nothing)

                If bUpdateMode = True AndAlso oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                End If
            End If
            Dim sEvtItem As String = pVal.ItemUID

            If sEvtItem.Equals("FORECGRID") Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                    If pVal.BeforeAction = True AndAlso
                     pVal.ItemChanged Then
                        Dim oGridN As DBOGrid = getWFValue(oForm, "FORECGRID")
                        Dim oForecast As IDH_BPFORECST = oGridN.DBObject
                        Dim sFieldItem As String = oGridN.doFindDBField(pVal.ColUID, True).Trim()
                        Dim iRow As Integer = oGridN.GetDataTableRowIndex(pVal.Row)
                        If sFieldItem.Equals(IDH_BPFORECST._UOM) Then
                            'If oForecast.U_ComQty <> 0 OrElse oForecast.U_DoneQty <> 0 Then
                            oForecast.OldUOM = oGridN.doGetFieldValue(IDH_BPFORECST._UOM, iRow)
                            'Else
                            '   oForecast.OldUOM = "NS"
                            'End If
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST Then
                If pVal.Before_Action = False AndAlso (pVal.ItemUID.Length = 0 OrElse pVal.ItemUID = "5") Then
                    '		doShowWR1(oForm, True)
                    'setWFValue(oForm, "WR1REFRESH", True)
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.Before_Action = True Then
                    If sEvtItem = "IDH_EPAS" Then
                        Dim iKey As Integer = pVal.CharPressed
                        If (iKey >= 32 AndAlso iKey <= 128) Then
                            Dim sECPass As String = getUFValue(oForm, "IDH_EPAS")
                            Dim sPass As String = getDFValue(oForm, "OCRD", "Password")
                            sPass &= Convert.ToChar(iKey)
                            'setDFValue( oForm, "OCRD", "Password", sPass )

                            'With oForm.DataSources.DBDataSources.Item("OCRD")
                            '    .SetValue("Password", .Offset, sPass )
                            'End With
                            setItemValue(oForm, "185", sPass)

                            sECPass &= "*"
                            setUFValue(oForm, "IDH_EPAS", sECPass)
                        ElseIf iKey = 8 Then 'BackSpace = 8
                            Dim sECPass As String = getUFValue(oForm, "IDH_EPAS")
                            Dim sPass As String = getDFValue(oForm, "OCRD", "Password")

                            If sPass.Length > 0 Then
                                sPass = sPass.Substring(0, sPass.Length - 1)
                                setItemValue(oForm, "185", sPass)
                            End If

                            If sECPass.Length > 0 Then
                                sECPass = sECPass.Substring(0, sECPass.Length - 1)
                                setUFValue(oForm, "IDH_EPAS", sECPass)
                            End If
                        End If

                        'Tab = 9
                        'Enter = 13

                        'BubbleEvent = False
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = True Then
                    If sEvtItem = "178" AndAlso pVal.ColUID = "1" AndAlso pVal.Row = 1 Then
                        '#MA2 issue:418 20170620
                        'Address Code is non editable.
                        If pVal.ItemChanged AndAlso (((getDFValue(oForm, "CRD1", "U_AddrsCd")) Is Nothing OrElse (getDFValue(oForm, "CRD1", "U_AddrsCd")).ToString().Trim().Equals(String.Empty))) Then
                            doSwitchAddressCode(oForm, True)
                        ElseIf Not pVal.ItemChanged Then
                            doSwitchAddressCode(oForm, False)
                        End If
                    End If
                    If sEvtItem = "178" AndAlso ((getDFValue(oForm, "CRD1", "U_AddrsCd")) Is Nothing OrElse (getDFValue(oForm, "CRD1", "U_AddrsCd")).ToString().Trim().Equals(String.Empty)) Then
                            Try
                                'If getWFValue(oForm, "HandleValidate") = True Then
                                'setWFValue(oForm, "HandleValidate", False)
                                'Dim sNextNum As String = DataHandler.INSTANCE.NumbersGenerator.getNextNumber("SEQCRD1").CodeCode.ToString()
                                doCopyAddressID(oForm)
                                '    setWFValue(oForm, "HandleValidate", True)
                                'End If
                            Catch ex As Exception
                            Finally
                                setWFValue(oForm, "HandleValidate", True)
                            End Try
                        End If

                        If pVal.ItemUID = "178" AndAlso pVal.ItemChanged AndAlso getDFValue(oForm, "CRD1", "AdresType") = "S" AndAlso getDFValue(oForm, "CRD1", "U_AddrsCd").ToString() <> String.Empty Then
                        Dim sAddressLog As Hashtable
                        If getSharedData(oForm, "sAddressLog") IsNot Nothing Then
                            sAddressLog = getSharedData(oForm, "sAddressLog")
                        Else
                            sAddressLog = New Hashtable
                        End If
                            If Not sAddressLog.Contains(getDFValue(oForm, "CRD1", "U_AddrsCd").ToString()) Then
                                sAddressLog.Add(getDFValue(oForm, "CRD1", "U_AddrsCd"), getDFValue(oForm, "OCRD", "CardCode").ToString())
                            setSharedData(oForm, "sAddressLog", sAddressLog)
                        End If
                            'If (getDFValue(oForm, "CRD1", "U_AddrsCd")) Is Nothing OrElse (getDFValue(oForm, "CRD1", "U_AddrsCd")).ToString().Trim().Equals(String.Empty) Then
                            '    Dim sNextNum As String = DataHandler.INSTANCE.NumbersGenerator.getNextNumber("SEQCRD1").CodeCode.ToString()
                            '    doCopyAddressID(oForm, sNextNum)
                            'End If
                    End If
                    If sEvtItem = "178" AndAlso pVal.ColUID = "U_ROUTE" AndAlso pVal.Row = 1 Then
                        If pVal.ItemChanged Then
                            Dim sRouteCode As String
                            Dim sType As String
                            sType = getDFValue(oForm, "CRD1", "AdresType")
                            If sType.Equals("S") Then
                                With oForm.DataSources.DBDataSources.Item("CRD1")
                                    sRouteCode = .GetValue("U_ROUTE", .Offset)
                                End With
                                If sRouteCode.Length = 0 Then
                                    doChooseRoute(oForm, True, sRouteCode)
                                End If
                            End If
                        End If
                    ElseIf pVal.ItemUID = "IDHLBPC" OrElse pVal.ItemUID = "IDHLBPN" Then
                        If pVal.ItemChanged Then
                            doChooseLinkedBP(oForm, False, pVal.ItemUID)
                        End If
                    ElseIf pVal.ItemUID = "IDHCSTC" Then
                        If pVal.ItemChanged Then
                            'doChooseBPCostCentre(oForm, False)
                        End If
                    End If
                ElseIf sEvtItem = "178" AndAlso pVal.ColUID = "U_Origin" AndAlso pVal.Row = 1 Then
                    If pVal.ItemChanged Then
                        If getWFValue(oForm, "DOVAL") Then
                            Dim sOrigin As String
                            sOrigin = getDFValue(oForm, "CRD1", "U_Origin")
                            If sOrigin.Length > 0 Then
                                doChooseOrigin(oForm, True, sOrigin)
                            End If
                        Else
                            setWFValue(oForm, "DOVAL", True)
                        End If
                    End If
                ElseIf sEvtItem = "178" AndAlso pVal.ColUID = "U_USACOUNTY" AndAlso pVal.Row = 1 Then
                    If pVal.ItemChanged Then
                        Dim sUSACounty As String = getDFValue(oForm, "CRD1", "U_USACounty")
                        Dim sState As String = getDFValue(oForm, "CRD1", "State")

                        If sUSACounty.Length > 0 AndAlso sUSACounty.Contains("*") Then
                            doChooseUSACounty(oForm, True, sUSACounty, sState)
                        End If
                    End If
                ElseIf sEvtItem = "178" AndAlso pVal.ColUID = "U_ROUTE" AndAlso pVal.Row = 1 Then
                    If pVal.ItemChanged Then
                        Dim sRoute As String = getDFValue(oForm, "CRD1", "U_Route")

                        If sRoute.Length > 0 AndAlso sRoute.Contains("*") Then
                            doChooseRoute(oForm, True, sRoute)
                        End If
                    End If
                ElseIf sEvtItem = "178" AndAlso pVal.ColUID = "U_SalesCnt" AndAlso pVal.Row = 1 Then
                    If pVal.ItemChanged Then
                        Dim sSlpName As String = getDFValue(oForm, "CRD1", "U_SalesCnt")

                        If sSlpName.Length > 0 AndAlso sSlpName.Contains("*") Then
                            doChooseSalesContactPerson(oForm, True, sSlpName)
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE Then
                If getHasSharedData(oForm) Then
                    Dim sAction As String = getParentSharedData(oForm, "ACT")
                    If Not sAction Is Nothing AndAlso sAction.Length() > 0 AndAlso sAction.ToUpper.Equals("ADD") = True Then
                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            goParent.goApplication.ActivateMenuItem(Config.NAV_ADD)
                        End If
                    End If
                End If
                'Dim bWasOnForeCast As Boolean = getWFValue(oForm, "WasOnForeCast")
                'If bWasOnForeCast Then
                '    doClick(oForm, "IDH_TABFCS")
                'End If
                'ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD Then
                '    Dim bWasOnForeCast As Boolean = getWFValue(oForm, "WasOnForeCast")
                '    If bWasOnForeCast Then
                '        doClick(oForm, "IDH_TABFCS")
                '    End If
                'ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE Then
                '    Dim bWasOnForeCast As Boolean = getWFValue(oForm, "WasOnForeCast")
                '    If bWasOnForeCast Then
                '        doClick(oForm, "IDH_TABFCS")
                '    End If
                'ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD Then
                '    Dim bWasOnForeCast As Boolean = getWFValue(oForm, "WasOnForeCast")
                '    If bWasOnForeCast Then
                '        doClick(oForm, "IDH_TABFCS")
                '    End If

                'ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_RESIZE Then
                'Dim bWasOnForeCast As Boolean = getWFValue(oForm, "WasOnForeCast")
                'If bWasOnForeCast Then
                '    doClick(oForm, "IDH_TABFCS")
                'End If

                'If Config.INSTANCE.doGetDoForecasting() Then
                '    Dim owItem As SAPbouiCOM.Item = oForm.Items.Item(FCGRIDSTENCIL)
                '    Dim oFcItem As SAPbouiCOM.Item = oForm.Items.Item("FORECGRID")
                '    oFcItem.Width = owItem.Width + TABWIDTH - 10
                '    oFcItem.Height = owItem.Height
                'End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction Then
                    If pVal.ItemUID = "69" Then
                        setVisible(oForm, "480002075", False)
                    End If
                ElseIf pVal.BeforeAction = False Then
                    'If sEvtItem = "3" Or sEvtItem = "13" Or sEvtItem = "15" Or sEvtItem = "156" Or _
                    '    sEvtItem = "214" Or sEvtItem = "4" Or sEvtItem = "10" Or sEvtItem = "9" Or _
                    '    sEvtItem = "1320002081" Then
                    '    setWFValue(oForm, "WasOnForeCast", False)
                    'End If

                    Dim owItem As SAPbouiCOM.Item
                    If pVal.ItemUID = "15" And oForm.PaneLevel = "7" Then 'Address tab
                        'oForm.Items.Item("69").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                        docheckAddressAndSetCopyToButton(oForm)
                    ElseIf pVal.ItemUID = "IDHLBPS" Then 'Linked BP CFL
                        doChooseLinkedBP(oForm, True, pVal.ItemUID)
                    ElseIf pVal.ItemUID = "IDHLCCS" Then
                        doChooseBPCostCentre(oForm, True)
                    ElseIf pVal.ItemUID = "IDHLCS" Then
                        doChooseComplianceScheme(oForm, False)
                        'ElseIf pVal.ItemUID = "IDH_TABFCS" Then
                        '    oForm.PaneLevel = FORECASTPANE
                        '    If Config.INSTANCE.doGetDoForecasting() Then
                        '        doLoadForecastGrid(oForm)
                        '    End If
                        '    setWFValue(oForm, "WasOnForeCast", True)
                    ElseIf pVal.ItemUID = "IDH_BRWS01" Then
                        Dim sZip As String
                        With oForm.DataSources.DBDataSources.Item("CRD1")
                            sZip = .GetValue("ZipCode", .Offset)
                        End With
                        oForm.PaneLevel = 11

                        oForm.Height = h
                        Try
                            Dim iHDif As Integer = h - h_old

                            owItem = oForm.Items.Item("IDHBRW")
                            owItem.Width = w - 22
                            owItem.Height = h - y - 70

                            oForm.Items.Item("25").Height = h_25_old + iHDif
                            oForm.Items.Item("24").Height = h_25_old + iHDif
                            oForm.Items.Item("23").Top = t_23_old + iHDif
                            oForm.Items.Item("1").Top = t_1_old + iHDif
                            oForm.Items.Item("2").Top = t_2_old + iHDif

                            '                            Dim oBrowser As SAPbouiCOM.ActiveX
                            '                            oBrowser = owItem.Specific
                            '                            oBrowser.ClassID = Config.Parameter("AXBRW") '"Shell.Explorer.2"
                            '                            AxBrowser = oBrowser.Object
                            '                            sZip = sZip.Trim().Replace(" ", "%20")
                            '
                            '                            Dim sURL As String = Config.Parameter("MAPURL") & "?PC=" & sZip
                            '                            AxBrowser.Navigate2(sURL)
                        Catch ex As Exception

                        End Try
                    Else
                        '                        owItem = oForm.Items.Item("IDHBRW")
                        '                        If owItem.Width > -1 Then
                        '                            owItem.Width = -1
                        '                            owItem.Height = -1
                        '
                        '                            oForm.Items.Item("25").Height = h_25_old
                        '                            oForm.Items.Item("24").Height = h_25_old
                        '                            oForm.Items.Item("23").Top = t_23_old
                        '                            oForm.Items.Item("1").Top = t_1_old
                        '                            oForm.Items.Item("2").Top = t_2_old
                        '
                        '                            oForm.Height = h_old
                        '                        End If
                        If pVal.ItemUID = "IDH_ADDCC" Then
                            Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                            setSharedData(oForm, "IDH_CUSTCD", sCardCode)
                            goParent.doOpenModalForm("IDHCCDT", oForm)
                        ElseIf pVal.ItemUID = "IDH_PBI" Then
                            Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                            Dim sCardName As String = getDFValue(oForm, "OCRD", "CardName")
                            setSharedData(oForm, "e_CardCode", sCardCode)
                            setSharedData(oForm, "e_CardName", sCardName)
                            setSharedData(oForm, "TRG", "IDH_PBM")
                            goParent.doOpenModalForm("IDH_PBM", oForm)
                        ElseIf pVal.ItemUID = "IDH_OSMB" Then
                            Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                            Dim sCardType As String = getDFValue(oForm, "OCRD", "CardType")

                            '20151026: Remarked block below as it was causing issues where 
                            'setting the Sup.Code was causing an extended where clause in manager.Templ.vb to be concatinated 
                            'Therefore have to change this to one below the remarked block 
                            'Also have change the concatination block in manager.Templ.vb Ln: 788 
                            'If sCardType.Equals("C") Then
                            '    setSharedData(oForm, "IDH_CUST", sCardCode)
                            '    setSharedData(oForm, "IDH_CARCD", "")
                            'setSharedData(oForm, "SUPPLIERS", sCardCode) ''MA 20-11-2014
                            'Else
                            '    setSharedData(oForm, "IDH_CUST", "")
                            '    setSharedData(oForm, "IDH_CARCD", "") 'sCardCode)
                            '    setSharedData(oForm, "SUPPLIERS", sCardCode)
                            'End If

                            If sCardType.Equals("C") Then
                                setSharedData(oForm, "IDH_CUST", sCardCode)
                            Else
                                setSharedData(oForm, "SUPPLIERS", sCardCode)
                            End If

                            setSharedData(oForm, "IDH_ADDR", "")
                            Dim sOSM = Config.Parameter("OSMBP")
                            If sOSM Is Nothing OrElse sOSM.Length = 0 Then
                                sOSM = "IDHJOBR"
                            End If
                            goParent.doOpenModalForm(sOSM, oForm)
                        ElseIf pVal.ItemUID = "IDH_DOMB" Then
                            Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                            setSharedData(oForm, "IDH_CUST", sCardCode)
                            setSharedData(oForm, "IDH_ADDR", "")
                            goParent.doOpenModalForm("IDHDOM", oForm)
                        ElseIf pVal.ItemUID = "IDH_ONSITE" Then
                            Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                            setSharedData(oForm, "IDH_CUST", sCardCode)
                            'goParent.doOpenModalForm("IDHONST", oForm)
                            goParent.doOpenForm("IDHONST", oForm, False)
                        ElseIf pVal.ItemUID = "uBC_TRANCD" Then
                            Dim oOOForm As com.uBC.forms.fr3.admin.BPTransactionCodesLink = New com.uBC.forms.fr3.admin.BPTransactionCodesLink(Nothing, oForm.UniqueID, Nothing)
                            Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                            oOOForm.BPCode = sCardCode
                            oOOForm.doShowModal()
                        ElseIf pVal.ItemUID = "uBC_ADDCHG" Then
                            Dim oOOForm As com.uBC.forms.fr3.admin.BPAdditionalCharges = New com.uBC.forms.fr3.admin.BPAdditionalCharges(Nothing, oForm.UniqueID, Nothing)
                            Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                            Dim sCardName As String = getDFValue(oForm, "OCRD", "CardName")
                            oOOForm.BPCode = sCardCode
                            oOOForm.BPName = sCardName
                            oOOForm.doShowModal()
                        ElseIf pVal.ItemUID = "IDH_CIP" Then
                            Dim sType As String = getDFValue(oForm, "OCRD", "CardType")
                            Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                            If sType = "C" Then
                                Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.prices.CIP = New WR1_FR2Forms.idh.forms.fr2.prices.CIP(Nothing, oForm.UniqueID, Nothing)
                                oOOForm.CustomerCode = sCardCode
                                oOOForm.FilterMode = 1
                                oOOForm.doShowModal()
                            ElseIf sType = "S" Then
                                Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.prices.SIP = New WR1_FR2Forms.idh.forms.fr2.prices.SIP(Nothing, oForm.UniqueID, Nothing)
                                oOOForm.SupplierCode = sCardCode
                                oOOForm.FilterMode = 1
                                oOOForm.doShowModal()
                            End If
                            '#MA Start 07-06-2017
                        ElseIf pVal.ItemUID = "uBC_COMRCL" Then
                            If getDFValue(oForm, "OCRD", "CardCode").ToString() = "" Then
                                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please Select Customer or Supplier", "WRINNOBP", New String() {"Customer Code"})
                                Exit Function
                            End If
                            If getDFValue(oForm, "OCRD", "CardType").Equals("L") Then '#MA issue:325 20170328
                                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please Select Customer or Supplier for Commercial Search", "WRINNOBP", New String() {"Customer "})
                            Else
                                setSharedData(oForm, "CardCode", getDFValue(oForm, "OCRD", "CardCode").ToString())
                                setSharedData(oForm, "CardName", getDFValue(oForm, "OCRD", "CardName").ToString())
                                'setSharedData(oForm, "AdrCod", getDFValue(oForm, "CRD1", "U_AddrsCd").ToString())
                                'Dim zipCd As String = getDFValue(oForm, "CRD1", "ZipCode").ToString()
                                'setSharedData(oForm, "AddressZipCode", getDFValue(oForm, "CRD1", "ZipCode").ToString())
                                'setSharedData(oForm, "Address", getDFValue(oForm, "CRD1", "Address").ToString())
                                'setSharedData(oForm, "AddressType", getDFValue(oForm, "CRD1", "AdresType").ToString())
                                'setSharedData(oForm, "call", True)
                                goParent.doOpenModalForm("IDH_COMMERCIALS", oForm)
                            End If
                            '#MA Start 07-06-2017
                        ElseIf pVal.ItemUID = "IDH_CWABT" Then
                            Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                            Dim sSiteAddr As String = ""

                            If sCardCode.Trim().Length > 0 Then
                                Dim sReportFile As String
                                sReportFile = Config.Parameter("WOCWAB")

                                Dim oParams As New Hashtable
                                oParams.Add("Customer", sCardCode)
                                oParams.Add("SiteAddr", sSiteAddr)

                                If (Config.INSTANCE.useNewReportViewer()) Then
                                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                                    IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                                Else
                                    IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                                End If
                            End If
                        ElseIf pVal.ItemUID = "69" Then
                            docheckAddressAndSetCopyToButton(oForm)
                            'oForm.Freeze(True)
                            'With oForm.DataSources.DBDataSources.Item("CRD1")
                            '    Dim sAddress As String = getDFValue(oForm, "CRD1", "Address")
                            '    Dim sAddrType As String = getDFValue(oForm, "CRD1", "AdresType")
                            '    Dim oItem As SAPbouiCOM.Item
                            '    setVisible(oForm, "480002075", False)
                            '    oItem = oForm.Items.Item("IDH_BACOPY")
                            '    If Not sAddress = getTranslatedWord("Define New") Then ' Denine New Must be translate into the exact word as per SBO's own translation
                            '        If sAddrType = "S" Then
                            '            oItem.Specific.Caption = getTranslatedWord("Copy Ship To Billing")
                            '        Else
                            '            oItem.Specific.Caption = getTranslatedWord("Copy Billing To Ship")
                            '            'oForm.Items.Item("480002075").Visible = False
                            '        End If
                            '        oItem.Visible = True
                            '    Else
                            '        oItem.Visible = False
                            '    End If
                            'End With
                            'oForm.Freeze(False)
                            'ElseIf pVal.ItemUID = "IDH_BACOPY" Then
                            '    doCopyBPAddress(oForm)
                        ElseIf pVal.ItemUID = "IDH_POSTCS" Then
                            doFindAddress(oForm)
                        ElseIf pVal.ItemUID = "IDH_ALTADR" Then
                            doLoadAlterNateAddress(oForm)
                            'ElseIf oForm.PaneLevel = 7 Then

                            '    Dim sType As String = "" 'getDFValue(oForm, "CRD1", "AdresType")
                            '    Dim sAddress As String = "" ' getDFValue(oForm, "CRD1", "Address")
                            '    Dim aExtraFields As New ArrayList
                            '    aExtraFields.Add("U_ISBCUSTRN")
                            '    GetSelectedAddressAndType(oForm, sAddress, sType, aExtraFields)
                            '    Dim U_ISBCUSTRN As String = aExtraFields(0)
                            '    'Dim sType As String = getDFValue(oForm, "CRD1", "AdresType")
                            '    'Dim sAddress As String = getDFValue(oForm, "CRD1", "Address")
                            '    If pVal.ItemUID = "IDH_WROSMC" AndAlso sAddress <> getTranslatedWord("Define New") Then
                            '        If sType = "S" Then
                            '            'setSharedData(oForm, "IDH_CUST", getDFValue(oForm, "CRD1", "CardCode"))
                            '            'setSharedData(oForm, "IDH_ADDR", getDFValue(oForm, "CRD1", "Address"))
                            '            'setSharedData(oForm, "IDH_CUSTREF", getDFValue(oForm, "CRD1", "U_ISBCUSTRN"))
                            '            setSharedData(oForm, "IDH_CUST", getDFValue(oForm, "CRD1", "CardCode"))
                            '            setSharedData(oForm, "IDH_ADDR", sAddress)
                            '            setSharedData(oForm, "IDH_CUSTREF", U_ISBCUSTRN)
                            '            Dim sOSM = Config.Parameter("OSMB")
                            '            If sOSM Is Nothing OrElse sOSM.Length = 0 Then
                            '                sOSM = "IDHJOBR"
                            '            End If
                            '            goParent.doOpenModalForm(sOSM, oForm)
                            '        Else
                            '            doWarnMess("This Function can only be used with Ship to Addresses", SAPbouiCOM.BoMessageTime.bmt_Short)
                            '        End If
                            '    ElseIf pVal.ItemUID = "IDH_WRDOMC" AndAlso sAddress <> getTranslatedWord("Define New") Then
                            '        If sType = "S" Then
                                    'setSharedData(oForm, "IDH_CUST", getDFValue(oForm, "CRD1", "CardCode"))
                            '            setSharedData(oForm, "IDH_ADDR", sAddress)
                            '            setSharedData(oForm, "IDH_CUSTREF", U_ISBCUSTRN)
                            '            goParent.doOpenModalForm("IDHDOM", oForm)
                            '        Else
                            '            doWarnMess("This Function can only be used with Ship to Addresses", SAPbouiCOM.BoMessageTime.bmt_Short)
                            '        End If
                            '    ElseIf pVal.ItemUID = "IDH_WSTPRF" AndAlso sAddress <> getTranslatedWord("Define New") Then
                            '        If sType = "S" Then
                            '            Dim sBPCd As String = getDFValue(oForm, "OCRD", "CardCode")
                            '            If sBPCd.Contains("'") Then
                            '                sBPCd = sBPCd.Replace("'", "''")
                            '            End If
                            '            setSharedData(oForm, "IDH_CUST", sBPCd)
                            '            Dim sBPNm As String = getDFValue(oForm, "OCRD", "CardName")
                            '            If sBPNm.Contains("'") Then
                            '                sBPNm = sBPNm.Replace("'", "''")
                            '            End If
                            '            setSharedData(oForm, "IDH_CUSTNM", sBPNm)
                            '            'Dim sAdd As String = getDFValue(oForm, "CRD1", "Address")
                            '            'If sAdd.Contains("'") Then
                            '            '    sAdd = sAdd.Replace("'", "''")
                            '            'End If
                            '            setSharedData(oForm, "IDH_ADDR", sAddress)
                            '            goParent.doOpenModalForm("IDHWPRS", oForm)
                            '        Else
                            '            doWarnMess("This Function can only be used with Ship to Addresses", SAPbouiCOM.BoMessageTime.bmt_Short)
                            '        End If
                            '    ElseIf pVal.ItemUID = "IDH_CPYADD" Then
                            '        If sAddress = getTranslatedWord("Define New") Then
                            '            setSharedData(oForm, "CPYTRG", sType)
                            '            setSharedData(oForm, "IDH_ADRTYP", sType)
                            '            goParent.doOpenModalForm("IDHASRCH2", oForm)
                            '        Else
                            '            doWarnMess("This function can only be used when 'Define New' address is selected.", SAPbouiCOM.BoMessageTime.bmt_Short)
                            '        End If
                                    'End If
                            '    'End With
                        End If
                    End If
                End If
            Else
            End If
            Return False
        End Function
 
        Protected Overridable Sub doChooseOrigin(ByRef oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sOriginCd As String)
            If sOriginCd = "*" Then
                sOriginCd = ""
            End If
            setSharedData(oForm, "IDH_ORCODE", sOriginCd)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            goParent.doOpenModalForm("IDHEAORSR", oForm)
        End Sub

        Protected Overridable Sub doChooseUSACounty(ByRef oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sUSACounty As String, ByVal sState As String)
            If sUSACounty = "*" Then
                sUSACounty = ""
            End If
            setSharedData(oForm, "IDH_STATCD", sState)
            setSharedData(oForm, "IDH_CNTYCD", sUSACounty)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            goParent.doOpenModalForm("IDHCNTYSR", oForm)
        End Sub

        Protected Overridable Sub doChooseSalesContactPerson(ByRef oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sSlpName As String)
            If sSlpName = "*" Then
                sSlpName = ""
            End If
            setSharedData(oForm, "IDH_SALCNT", sSlpName)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            goParent.doOpenModalForm("IDHSALCNT", oForm)
        End Sub

        Protected Overridable Sub doChooseRoute(ByRef oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sRoute As String)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            setSharedData(oForm, "IDH_RTCOD", sRoute)
            goParent.doOpenModalForm("IDHROSRC", oForm)
        End Sub

        Private Sub doChooseComplianceScheme(ByRef oForm As SAPbouiCOM.Form, ByVal bDoSilent As Boolean)
            Dim sBPCode As String = getDFValue(oForm, "OCRD", "U_IDHONCS")

            'If sBPCode.Length > 0 AndAlso (sBPCode.StartsWith("*") OrElse sBPCode.EndsWith("*")) Then
            '	sBPName = ""
            'End If

            setSharedData(oForm, "TRG", "CS")
            setSharedData(oForm, "IDH_BPCOD", sBPCode)
            'setSharedData(oForm, "IDH_NAME", sBPName)
            'setSharedData(oForm, "IDH_TYPE", "F-S")
            'setSharedData(oForm, "IDH_TYPE", sType)
            'setSharedData(oForm, "IDH_GROUP", "")
            setSharedData(oForm, "IDH_CS", "Y")
            If bDoSilent Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Private Sub doChooseLinkedBP(ByRef oForm As SAPbouiCOM.Form, ByVal bForceSearch As Boolean, ByVal sItemUID As String)
            If getWFValue(oForm, "BUSWLBP") = True Then
                Exit Sub
            End If

            Dim sBPCode As String = getDFValue(oForm, "OCRD", "U_IDHLBPC")
            Dim sBPName As String = getDFValue(oForm, "OCRD", "U_IDHLBPN")

            If bForceSearch = False Then
                If sBPCode.Length = 0 AndAlso sBPName.Length = 0 Then
                    Exit Sub
                End If
            End If
            If sItemUID = "IDHLBPC" Then
                sBPName = ""
            ElseIf sItemUID = "IDHLBPN" Then
                sBPCode = ""
            Else
                If sBPCode.Length > 0 AndAlso (sBPCode.Contains("*")) Then
                    sBPName = ""
                ElseIf sBPName.Length > 0 AndAlso (sBPName.Contains("*")) Then
                    sBPCode = ""
                End If
            End If


            Dim sType As String = getDFValue(oForm, "OCRD", "CardType")
            If sType = "S" Then
                sType = "C"
            Else
                sType = "S"
            End If

            setSharedData(oForm, "TRG", "LBP")
            setSharedData(oForm, "IDH_BPCOD", sBPCode)
            setSharedData(oForm, "IDH_NAME", sBPName)
            'setSharedData(oForm, "IDH_TYPE", "F-S")
            setSharedData(oForm, "IDH_TYPE", sType)
            setSharedData(oForm, "IDH_GROUP", "")
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Private Sub doChooseBPCostCentre(ByRef oForm As SAPbouiCOM.Form, ByVal bForceSearch As Boolean)
            Dim sBPCC As String = getDFValue(oForm, "OCRD", "U_COSTCNTR")

            If bForceSearch = False Then
                If sBPCC.Length = 0 Then
                    Exit Sub
                End If
            End If

            'setSharedData(oForm, "TRG", "LBP")
            'setSharedData(oForm, "IDH_BPCOD", sBPCode)
            'setSharedData(oForm, "IDH_NAME", sBPName)
            ''setSharedData(oForm, "IDH_TYPE", "F-S")
            'setSharedData(oForm, "IDH_TYPE", sType)
            'setSharedData(oForm, "IDH_GROUP", "")
            'setSharedData(oForm, "SILENT", "SHOWMULTI")
            goParent.doOpenModalForm("IDHPRCSRC", oForm)
        End Sub

        Private Sub doCopyBPAddress(ByRef oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Dim oBillExcl() As String = {"U_ROUTE", "U_ISBACCDF", "U_ISBACCTF", "U_ISBACCTT", "U_ISBACCRA", "U_ISBACCRV", "U_IDHSEQ", "U_IDHDCON", "U_IDHPOQT", "U_IDHPOVA", "U_Origin"}

            Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("69")
            Dim oMatrix1 As SAPbouiCOM.Matrix = oItem.Specific
            Dim oMatrix2 As SAPbouiCOM.Matrix
            Dim oColumn As SAPbouiCOM.Column = oMatrix1.Columns.Item("20")
            Dim oCell As SAPbouiCOM.Cell

            oItem = oForm.Items.Item("178")
            oMatrix2 = oItem.Specific

            Dim oType As Object
            Dim hColData As New Hashtable
            Dim oValue As Object
            Dim oData As Object
            Dim oColumn2 As SAPbouiCOM.Column
            '            Dim oDataBind As SAPbouiCOM.DataBind
            Dim sColID As String
            Try
                oType = getDFValue(oForm, "CRD1", "AdresType")
                If oType.Equals("S") Then
                    oType = SAPbobsCOM.BoAddressType.bo_BillTo
                Else
                    oType = SAPbobsCOM.BoAddressType.bo_ShipTo
                End If

                For iCell As Integer = 1 To oMatrix2.Columns.Count - 1
                    oColumn2 = oMatrix2.Columns.Item(iCell)
                    oCell = oColumn2.Cells.Item(1)
                    oData = oCell.Specific
                    sColID = oColumn2.UniqueID

                    Dim oCombo As SAPbouiCOM.ComboBox
                    Dim oValid As SAPbouiCOM.ValidValues
                    Dim oSelected As SAPbouiCOM.ValidValue
                    If oColumn2.Editable = True Then
                        If oType = SAPbobsCOM.BoAddressType.bo_BillTo AndAlso Array.IndexOf(oBillExcl, sColID) >= 0 Then
                            oValue = Nothing
                        Else
                            Try
                                If oColumn2.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                                    oCombo = oData
                                    oValid = oCombo.ValidValues
                                    oSelected = oCombo.Selected
                                    If Not oValid Is Nothing AndAlso
                                        oValid.Count > 0 AndAlso
                                        Not oSelected Is Nothing Then
                                        oValue = oCombo.Selected.Value()
                                    Else
                                        oValue = Nothing
                                    End If
                                Else
                                    oValue = oData.Value
                                End If
                            Catch ex As Exception
                                oValue = Nothing
                            End Try
                        End If

                        If Not oValue Is Nothing Then
                            If Not (oValue.GetType Is GetType(String) AndAlso oValue.Length = 0) Then
                                hColData.Add(sColID, oValue)
                            End If
                        End If
                    End If
                Next

                Dim bStartCount As Boolean = False
                Dim oCellObject As Object
                Dim iAddressCount As Integer = 0
                For iRow As Integer = 1 To oColumn.Cells.Count
                    oCell = oColumn.Cells.Item(iRow)
                    oCellObject = oCell.Specific
                    oValue = oCellObject.Value

                    If iAddressCount = 0 Then
                        If oValue.ToString().ToUpper() = Translation.getTranslatedWord("BILL TO") AndAlso oType = SAPbobsCOM.BoAddressType.bo_BillTo Then
                            bStartCount = True
                            iAddressCount = iAddressCount + 1
                        ElseIf oValue.ToString().ToUpper() = Translation.getTranslatedWord("SHIP TO") AndAlso oType = SAPbobsCOM.BoAddressType.bo_ShipTo Then
                            bStartCount = True
                            iAddressCount = iAddressCount + 1
                        ElseIf oValue.ToString.ToUpper() = Translation.getTranslatedWord("PAY TO") AndAlso oType = SAPbobsCOM.BoAddressType.bo_BillTo Then
                            bStartCount = True
                            iAddressCount = iAddressCount + 1
                        End If
                    ElseIf iAddressCount > 0 Then
                        'If oType = SAPbobsCOM.BoAddressType.bo_BillTo AndAlso iAddressCount >= 2 Then
                        '    com.idh.bridge.DataHandler.INSTANCE.doError("Can not create more than one Bill To Address")
                        '    Exit Sub
                        'End If
                        iAddressCount = iAddressCount + 1
                        If oValue = "Define New" Then
                            setParentSharedData(oForm, "DONOTEXECUTE", "True")
                            oCell.Click()
                            setParentSharedData(oForm, "DONOTEXECUTE", "False")

                            For iCell As Integer = 1 To oMatrix2.Columns.Count - 1
                                oColumn2 = oMatrix2.Columns.Item(iCell)
                                sColID = oColumn2.UniqueID
                                If hColData.ContainsKey(sColID) Then
                                    oValue = hColData.Item(sColID)
                                    oCell = oColumn2.Cells.Item(1)

                                    oData = oCell.Specific
                                    Try
                                        If oColumn2.Editable = True Then
                                            If oColumn2.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                                                Dim oCombo As SAPbouiCOM.ComboBox = oData
                                                oCombo.Select(oValue, SAPbouiCOM.BoSearchKey.psk_ByValue)
                                            ElseIf oColumn2.TitleObject.Caption = "Address Code" Then
                                                doCopyAddressID(oForm)
                                            Else
                                                oData.Value = oValue
                                            End If
                                        End If
                                    Catch ex As Exception
                                    End Try
                                End If
                            Next
                            Exit Sub
                        End If
                    End If
                Next
            Catch ex As Exception
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Private Sub doCopyBPAddress(ByRef oForm As SAPbouiCOM.Form, ByVal sCardCode As String)
            '#MA  issue:418 20170522
            Try
                oForm.Items.Item("15").Click()
            Catch ex As Exception

            End Try

            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
            Dim oMatrixAddressdetail As SAPbouiCOM.Matrix
            Dim oMatrixAddressList As SAPbouiCOM.Matrix
            Dim oCell As SAPbouiCOM.Cell
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("178")
            oMatrixAddressdetail = oItem.Specific
            oMatrixAddressList = oForm.Items.Item("69").Specific
            Dim hColData As New Hashtable
            Dim oData As Object
            Dim oColumn2 As SAPbouiCOM.Column
            Dim sColID As String
            Try

                If oBP.GetByKey(getDFValue(oForm, "OCRD", "CardCode")) = True Then
                    Dim MyAddressesToCopy As New List(Of String)
                    Dim iCount As Int32 = 0
                    For index = 1 To oMatrixAddressList.Columns.Item("20").Cells.Count
                        Dim oEditColumn As SAPbouiCOM.EditText = oMatrixAddressList.Columns.Item("20").Cells.Item(index).Specific
                        'If Not oEditColumn.Value.ToString().ToUpper() = "PAY TO" AndAlso Not oEditColumn.Value.ToString().ToUpper() = "SHIP TO" AndAlso Not oEditColumn.Value.ToString().ToUpper() = "DEFINE NEW" Then
                        If Not oEditColumn.Value.ToString().ToUpper() = Translation.getTranslatedWord("DEFINE NEW") Then
                            MyAddressesToCopy.Add(oEditColumn.Value)
                            iCount = iCount + 1
                        End If
                    Next

                    For index = 1 To oMatrixAddressList.Columns.Item("20").Cells.Count
                        Dim oEditColumn As SAPbouiCOM.EditText = oMatrixAddressList.Columns.Item("20").Cells.Item(index).Specific
                        If Not oEditColumn.Value.ToString().ToUpper() = Translation.getTranslatedWord("PAY TO") _
                            AndAlso Not oEditColumn.Value.ToString().ToUpper() = Translation.getTranslatedWord("SHIP TO") _
                            AndAlso Not oEditColumn.Value.ToString().ToUpper() = Translation.getTranslatedWord("DEFINE NEW") Then
                            oMatrixAddressList.Columns.Item("20").Cells.Item(index).Click()
                            oColumn2 = oMatrixAddressdetail.Columns.Item("U_AddrsCd")
                            oCell = oColumn2.Cells.Item(1)
                            oData = oCell.Specific
                            sColID = oColumn2.UniqueID
                            oData = oCell.Specific
                            If oColumn2.Editable Then
                                Dim sNextNum As String = DataHandler.INSTANCE.NumbersGenerator.getNextNumber("SEQCRD1").CodeCode.ToString()
                                oData.value = sNextNum
                            End If
                        End If
                    Next


                    'For index = 1 To MyAddressesToCopy.Count
                    '    For index1 = 1 To oMatrixAddressList.Columns.Item("20").Cells.Count
                    '        Dim oEditColumn As SAPbouiCOM.EditText = oMatrixAddressList.Columns.Item("20").Cells.Item(index1).Specific
                    '        If oEditColumn.Value = MyAddressesToCopy(index - 1) Then
                    '            oMatrixAddressList.Columns.Item("20").Cells.Item(index1).Click()
                    '            oColumn2 = oMatrixAddressdetail.Columns.Item("U_AddrsCd")
                    '            oCell = oColumn2.Cells.Item(1)
                    '            oData = oCell.Specific
                    '            sColID = oColumn2.UniqueID
                    '            oData = oCell.Specific
                    '            If oColumn2.Editable Then
                    '                Dim sNextNum As String = DataHandler.INSTANCE.NumbersGenerator.getNextNumber("SEQCRD1").CodeCode.ToString()
                    '                oData.value = sNextNum
                    '            End If
                    '            Exit For
                    '        End If
                    '    Next
            '            Next
                    '#MA  issue:418 20170522
                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString, "Error Copying Addresses Code for BP " & sCardCode)
            Finally

            End Try
        End Sub
        Private Sub doCopyAddressID(ByRef oForm As SAPbouiCOM.Form)

            oForm.Freeze(True)
            Try
                If getWFValue(oForm, "HandleValidate") = True Then
                    setWFValue(oForm, "HandleValidate", False)
                    Dim oItem As SAPbouiCOM.Item ' = oForm.Items.Item("69")
                    'Dim oMatrix1 As SAPbouiCOM.Matrix = oItem.Specific
                    Dim oMatrix2 As SAPbouiCOM.Matrix
                    'Dim oColumn As SAPbouiCOM.Column '= oMatrix1.Columns.Item("20")
                    Dim oCell As SAPbouiCOM.Cell

                    oItem = oForm.Items.Item("178")
                    oMatrix2 = oItem.Specific

                    Dim hColData As New Hashtable
                    Dim oData As Object
                    Dim oColumn2 As SAPbouiCOM.Column
                    Dim sColID As String
                    Try
                        For iCell As Integer = 1 To oMatrix2.Columns.Count - 1
                            oColumn2 = oMatrix2.Columns.Item("U_AddrsCd")
                            oCell = oColumn2.Cells.Item(1)
                            oData = oCell.Specific
                            sColID = oColumn2.UniqueID
                            oData = oCell.Specific
                            ' setWFValue(oForm, "HandleValidate", False)
                            If oColumn2.Editable AndAlso (oData.value Is Nothing OrElse oData.value.ToString().Trim() = String.Empty) Then
                                Dim sNextNum As String = DataHandler.INSTANCE.NumbersGenerator.getNextNumber("SEQCRD1").CodeCode.ToString()
                                oData.value = sNextNum
                            End If
                            Exit Sub
                        Next
                    Catch ex As Exception
                    Finally
                        'oForm.Freeze(False)

                    End Try
                    setWFValue(oForm, "HandleValidate", True)
                End If
            Catch ex As Exception
            Finally
                oForm.Freeze(False)
            End Try

        End Sub

        Private Sub doCopyBPAddressDelayed(ByRef oForm As SAPbouiCOM.Form)
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing

            Dim sName As String = ""
            'Dim sName2 As String = ""
            'Dim sName3 As String = ""
            Dim sType As String = ""
            'Dim sBuildingFloorRoom As String = ""
            'Dim City As String = ""
            'Dim Country As String = ""
            'Dim County As String = ""
            'Dim FederalTaxID As String = ""
            'Dim State As String = ""
            'Dim Street As String = ""
            'Dim TaxCode As String = ""
            'Dim ZipCode As String = ""
            Try
                sName = getDFValue(oForm, "CRD1", "Address")
                'sName2 = ""
                'sName3 = ""
                'sType = ""
                'sBuildingFloorRoom = ""
                'City = ""
                'Country = ""
                'County = ""
                'FederalTaxID = ""
                'State = ""
                'Street = ""
                'TaxCode = ""
                'ZipCode = ""

                oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                If oBP.GetByKey(getDFValue(oForm, "OCRD", "CardCode")) = True Then
                    oBP.Addresses.Add()
                    oBP.Addresses.AddressName = getDFValue(oForm, "CRD1", "Address")
                    oBP.Addresses.AddressName2 = getDFValue(oForm, "CRD1", "Address2")
                    oBP.Addresses.AddressName3 = getDFValue(oForm, "CRD1", "Address3")
                    sType = getDFValue(oForm, "CRD1", "AdresType")
                    If sType = "S" Then
                        oBP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_BillTo
                    Else
                        oBP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo
                    End If
                    oBP.Addresses.BuildingFloorRoom = getDFValue(oForm, "CRD1", "Building")
                    oBP.Addresses.City = getDFValue(oForm, "CRD1", "City")
                    oBP.Addresses.Country = getDFValue(oForm, "CRD1", "Country")
                    oBP.Addresses.County = getDFValue(oForm, "CRD1", "County")
                    oBP.Addresses.FederalTaxID = getDFValue(oForm, "CRD1", "LicTradNum")
                    oBP.Addresses.State = getDFValue(oForm, "CRD1", "State")
                    oBP.Addresses.Street = getDFValue(oForm, "CRD1", "Street")
                    oBP.Addresses.TaxCode = getDFValue(oForm, "CRD1", "TaxCode")
                    oBP.Addresses.ZipCode = getDFValue(oForm, "CRD1", "ZipCode")
                    oBP.Addresses.UserFields.Fields.Item("U_Origin").Value = getDFValue(oForm, "CRD1", "U_Origin")
                    oBP.Addresses.UserFields.Fields.Item("U_AddrsCd").Value = Config.INSTANCE.GetAddressNextCode()
                    oBP.Update()
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString, "Error Copying Address - " & sName)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCA", {sName})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            End Try
        End Sub

        Private Function doGetWRBalance(ByVal sCardCode As String) As Double
            If sCardCode Is Nothing OrElse sCardCode.Length = 0 Then
                Return 0
            End If

            Dim sQry As String
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                sQry = "select WRBalance From IDH_VWR1BAL Where CardCode = '" & sCardCode & "'"
                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If Not oRecordSet Is Nothing Then
                    If oRecordSet.RecordCount > 0 Then
                        Return oRecordSet.Fields.Item(0).Value
                    End If
                End If
            Catch ex As Exception
                Throw ex
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            End Try
            Return 0
        End Function

        Public Sub doShowWR1(ByRef oForm As SAPbouiCOM.Form, ByRef bShow As Boolean, ByVal sMenuID As String)
            'setWFValue(oForm, "WR1REFRESH", False)

            doSetECPassword(oForm)
            Dim owItem As SAPbouiCOM.Item = oForm.Items.Item("185")
            owItem.Enabled = False
            '#MA issue:390 20170620
            Dim oMatrix As SAPbouiCOM.Matrix
            Dim oCols As SAPbouiCOM.Column
            oMatrix = oForm.Items.Item("178").Specific
            oCols = oMatrix.Columns.Item("U_AddrsCd")
            oCols.Editable = False
            '#MA issue:390 20170620 end

            If bShow = True Then
                Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                Dim sCardType As String = getDFValue(oForm, "OCRD", "CardType")
                Dim oItem As SAPbouiCOM.Item
                If sCardType.Equals("C") = False Then
                    '                    oItem = oForm.Items.Item("IDH_WR1OLB")
                    '                    If oItem.Visible = True Then
                    '                        oItem.Visible = False
                    '                        oItem = oForm.Items.Item("IDH_WR1ORD")
                    '                        oItem.Visible = False
                    '                        oItem = oForm.Items.Item("IDH_OSMB")
                    '                        oItem.Visible = False
                    '                        oItem = oForm.Items.Item("IDH_DOMB")
                    '                        oItem.Visible = False
                    '                        oItem = oForm.Items.Item("IDH_CIP")
                    '                        oItem.Visible = False
                    '                        oItem = oForm.Items.Item("IDH_CWABT")
                    '                        oItem.Visible = False
                    '                    End If

                    oItem = oForm.Items.Item("IDH_WR1OLB")
                    oItem.Visible = False
                    oItem = oForm.Items.Item("IDH_WR1ORD")
                    oItem.Visible = False

                    If sCardType.Equals("S") Then
                        oItem = oForm.Items.Item("IDH_OSMB")
                        oItem.Visible = True

                        oItem = oForm.Items.Item("IDH_CIP")
                        oItem.Visible = True
                        oItem.Specific.Caption = getTranslatedWord("SIP")
                    Else
                        oItem = oForm.Items.Item("IDH_OSMB")
                        oItem.Visible = False

                        oItem = oForm.Items.Item("IDH_CIP")
                        oItem.Visible = False
                        oItem.Specific.Caption = getTranslatedWord("SIP")
                    End If

                    If Config.INSTANCE.getParameterAsBool("PBIENABL", True) Then
                        oItem = oForm.Items.Item("IDH_PBI")
                        oItem.Visible = False
                    End If

                    oItem = oForm.Items.Item("IDH_DOMB")
                    oItem.Visible = False

                    'oItem = oForm.Items.Item("IDH_CIP")
                    'oItem.Visible = True
                    'oItem.Specific.Caption = getTranslatedWord("SIP")

                    oItem = oForm.Items.Item("IDH_CWABT")
                    oItem.Visible = False
                    oItem = oForm.Items.Item("IDH_ONSITE")
                    oItem.Visible = False

                    oItem = oForm.Items.Item("uBC_TRANCD")
                    oItem.Visible = IIf(sCardType.Equals("S"), True, False)

                    oItem = oForm.Items.Item("uBC_ADDCHG")
                    oItem.Visible = False

                    setVisible(oForm, "IDHMDDT", False)
                    setVisible(oForm, "S1201", False)
                    setUFValue(oForm, "IDH_WR1ORD", 0)
                Else
                    Dim dWRBalance As Double = doGetWRBalance(sCardCode)
                    oItem = oForm.Items.Item("IDH_WR1OLB")
                    oItem.Visible = True
                    oItem.Enabled = False
                    oItem = oForm.Items.Item("IDH_WR1ORD")
                    oItem.Visible = True
                    '## Start 01-10-2013
                    'oItem.Enabled = False
                    '## End
                    setUFValue(oForm, "IDH_WR1ORD", dWRBalance)

                    oItem = oForm.Items.Item("IDH_OSMB")
                    oItem.Visible = True

                    If Config.INSTANCE.getParameterAsBool("PBIENABL", True) Then
                        oItem = oForm.Items.Item("IDH_PBI")
                        oItem.Visible = True
                    End If

                    oItem = oForm.Items.Item("IDH_DOMB")
                    oItem.Visible = True

                    oItem = oForm.Items.Item("IDH_CIP")
                    oItem.Visible = True
                    oItem.Specific.Caption = getTranslatedWord("CIP")

                    oItem = oForm.Items.Item("IDH_CWABT")
                    oItem.Visible = True
                    oItem = oForm.Items.Item("IDH_ONSITE")
                    oItem.Visible = True
                    oItem = oForm.Items.Item("uBC_TRANCD")
                    oItem.Visible = True

                    oItem = oForm.Items.Item("uBC_ADDCHG")
                    oItem.Visible = True

                    setVisible(oForm, "IDHMDDT", True)
                    setVisible(oForm, "S1201", True)
                    '                    If dWRBalance > 0 Then
                    '                        oItem = oForm.Items.Item("IDH_WR1OLB")
                    '                        If oItem.Visible = False Then
                    '                            oItem.Visible = True
                    '                            oItem.Enabled = False
                    '                            oItem = oForm.Items.Item("IDH_WR1ORD")
                    '                            oItem.Visible = True
                    '                            oItem.Enabled = False
                    '
                    '                            oItem = oForm.Items.Item("IDH_OSMB")
                    '                            oItem.Visible = True
                    '                            oItem = oForm.Items.Item("IDH_DOMB")
                    '                            oItem.Visible = True
                    '                            oItem = oForm.Items.Item("IDH_CIP")
                    '                            oItem.Visible = True
                    '                            oItem = oForm.Items.Item("IDH_CWABT")
                    '                            oItem.Visible = True
                    '                        End If
                    '                        setUFValue(oForm, "IDH_WR1ORD", dWRBalance)
                    '                    Else
                    '                        oItem = oForm.Items.Item("IDH_WR1OLB")
                    '                        If oItem.Visible = True Then
                    '                            oItem.Visible = False
                    '                            oItem.Enabled = False
                    '                            oItem = oForm.Items.Item("IDH_WR1ORD")
                    '                            oItem.Visible = False
                    '                            oItem.Enabled = False
                    '
                    '                            oItem = oForm.Items.Item("IDH_OSMB")
                    '                            oItem.Visible = True
                    '                            oItem = oForm.Items.Item("IDH_DOMB")
                    '                            oItem.Visible = False
                    '                            oItem = oForm.Items.Item("IDH_CIP")
                    '                            oItem.Visible = True
                    '                            oItem = oForm.Items.Item("IDH_CWABT")
                    '                            oItem.Visible = True
                    '                        End If
                    '                        setUFValue(oForm, "IDH_WR1ORD", 0)
                    '                    End If
                End If
            Else
                Dim oItem As SAPbouiCOM.Item
                oItem = oForm.Items.Item("IDH_WR1OLB")
                oItem.Visible = False

                oItem = oForm.Items.Item("IDH_WR1ORD")
                oItem.Visible = False

                setUFValue(oForm, "IDH_WR1ORD", 0)
                '## Start 01-10-2013
                'oItem.Enabled = False
                '## End
                oItem = oForm.Items.Item("IDH_OSMB")
                oItem.Visible = False

                If Config.INSTANCE.getParameterAsBool("PBIENABL", True) Then
                    oItem = oForm.Items.Item("IDH_PBI")
                    oItem.Visible = False
                End If

                oItem = oForm.Items.Item("IDH_DOMB")
                oItem.Visible = False
                oItem = oForm.Items.Item("IDH_CIP")
                oItem.Visible = False
                oItem = oForm.Items.Item("IDH_CWABT")
                oItem.Visible = False
                oItem = oForm.Items.Item("IDH_ONSITE")
                oItem.Visible = False
                oItem = oForm.Items.Item("uBC_TRANCD")
                oItem.Visible = False
                oItem = oForm.Items.Item("uBC_ADDCHG")
                oItem.Visible = False
            End If

            'OnTime [#Ico000????] USA 
            'START
            If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
                doSetUSARequirements(oForm, True)
            Else
                doSetUSARequirements(oForm, False)
            End If

            'END
            'OnTime [#Ico000????] USA

            'If Config.INSTANCE.doGetDoForecasting() Then
            '    'Load BP Forecast rows
            '    If oForm.PaneLevel = FORECASTPANE Then
            '        doLoadForecastGrid(oForm)
            '    End If
            'End If

            'Setting form pane for Data Source dropdown
            Dim oItm As SAPbouiCOM.Item
            oItm = oForm.Items.Item("IDHMDDT")
            oItm.FromPane = 1
            oItm.ToPane = 1
            'Label for Data Source field 
            oItm = oForm.Items.Item("S1201")
            oItm.FromPane = 1
            oItm.ToPane = 1

            'Set pane level for Fuel County and Port Surcharges 
            oItm = oForm.Items.Item("IDHFSCHK") 'Fuel Surcharge
            oItm.FromPane = 1
            oItm.ToPane = 1

            oItm = oForm.Items.Item("IDHCTCHK") 'Coutny Tariff 
            oItm.FromPane = 1
            oItm.ToPane = 1

            oItm = oForm.Items.Item("IDHPTCHK") 'Port Tariff 
            oItm.FromPane = 1
            oItm.ToPane = 1

            Dim sBType As String = getDFValue(oForm, "OCRD", "CardType")
            FillCombos.PriceGroupsCombo(oForm.Items.Item("IDHPRGRP").Specific, sBType, "", "FALLBACK")
        End Sub

        Public Sub doShowWR1New(ByRef oForm As SAPbouiCOM.Form, ByRef bShow As Boolean, ByVal sMenuID As String)
            'setWFValue(oForm, "WR1REFRESH", False)

            doSetECPassword(oForm)
            Dim owItem As SAPbouiCOM.Item = oForm.Items.Item("185")
            owItem.Enabled = False
            '#MA2 issue:390 20170620
            'doSwitchAddressCode(oForm, False)
            Dim oMatrix As SAPbouiCOM.Matrix
            Dim oCols As SAPbouiCOM.Column
            oMatrix = oForm.Items.Item("178").Specific
            oCols = oMatrix.Columns.Item("U_AddrsCd")
            oCols.Editable = False

            '#MA2 issue:390 20170620 end
            If bShow = True Then
                Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                Dim sCardType As String = getDFValue(oForm, "OCRD", "CardType")
                Dim oItem As SAPbouiCOM.Item
                If sCardType.Equals("C") = False Then
                    '                    oItem = oForm.Items.Item("IDH_WR1OLB")
                    '                    If oItem.Visible = True Then
                    '                        oItem.Visible = False
                    '                        oItem = oForm.Items.Item("IDH_WR1ORD")
                    '                        oItem.Visible = False
                    '                        oItem = oForm.Items.Item("IDH_OSMB")
                    '                        oItem.Visible = False
                    '                        oItem = oForm.Items.Item("IDH_DOMB")
                    '                        oItem.Visible = False
                    '                        oItem = oForm.Items.Item("IDH_CIP")
                    '                        oItem.Visible = False
                    '                        oItem = oForm.Items.Item("IDH_CWABT")
                    '                        oItem.Visible = False
                    '                    End If

                    oItem = oForm.Items.Item("IDH_WR1OLB")
                    oItem.Visible = False
                    oItem = oForm.Items.Item("IDH_WR1ORD")
                    oItem.Visible = False
                    '#MA Start 20-06-2017
                    oItem = oForm.Items.Item("IDH_WRBNT")
                    oItem.Visible = True
                    Dim oComboBtn As SAPbouiCOM.ButtonCombo = oItem.Specific
                    Dim oCount As Integer = oComboBtn.ValidValues.Count
                    For index As Integer = 0 To oCount - 1
                        oComboBtn.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                    Next
                    oComboBtn.Caption = Translation.getTranslatedWord("Waste & Recycle")
                    oComboBtn.ValidValues.Add("IDH_COMMERICALS", Translation.getTranslatedWord("Show Budget/Target Data"))
                    If sCardType.Equals("S") Then
                        oComboBtn.ValidValues.Add("IDH_OSM", Translation.getTranslatedWord("Show All Work Orders"))
                        oComboBtn.ValidValues.Add("IDH_CIP", Translation.getTranslatedWord("Show the Price File"))
                    End If

                    If Not Config.INSTANCE.getParameterAsBool("PBIENABL", True) Then
                        oComboBtn.ValidValues.Add("IDH_PBI", Translation.getTranslatedWord("Show my Scheduled Orders"))
                    End If
                    If (sCardType.Equals("S")) Then
                        oComboBtn.ValidValues.Add("IDH_TRANCD", Translation.getTranslatedWord("Set up Transaction Codes"))
                    End If
                    '#MA End 20-06-2017
                    setVisible(oForm, "IDHMDDT", False)
                    setVisible(oForm, "S1201", False)
                    setUFValue(oForm, "IDH_WR1ORD", 0)
                Else
                    Dim dWRBalance As Double = doGetWRBalance(sCardCode)
                    oItem = oForm.Items.Item("IDH_WR1OLB")
                    oItem.Visible = True
                    oItem.Enabled = False
                    oItem = oForm.Items.Item("IDH_WR1ORD")
                    oItem.Visible = True
                    '## Start 01-10-2013
                    'oItem.Enabled = False
                    '## End
                    setUFValue(oForm, "IDH_WR1ORD", dWRBalance)
                    '#MA Start 20-06-2017
                    oItem = oForm.Items.Item("IDH_WRBNT")
                    oItem.Visible = True
                    Dim oComboBtn As SAPbouiCOM.ButtonCombo = oItem.Specific
                    Dim oCount As Integer = oComboBtn.ValidValues.Count
                    For index As Integer = 0 To oCount - 1
                        oComboBtn.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                    Next
                    oComboBtn.Caption = Translation.getTranslatedWord("Waste & Recycle")
                    oComboBtn.ValidValues.Add("IDH_ADDCHG", Translation.getTranslatedWord("Configure Additional Charges"))
                    oComboBtn.ValidValues.Add("IDH_CIP", Translation.getTranslatedWord("Show the Price File"))
                    oComboBtn.ValidValues.Add("IDH_COMMERICALS", Translation.getTranslatedWord("Show Budget/Target Data"))
                    oComboBtn.ValidValues.Add("IDH_DOMB", Translation.getTranslatedWord("Show All Scale/Wbridge Tickets"))
                    oComboBtn.ValidValues.Add("IDH_OSM", Translation.getTranslatedWord("Show All Work Orders"))

                    If Config.INSTANCE.getParameterAsBool("PBIENABL", True) Then
                        oComboBtn.ValidValues.Add("IDH_PBI", Translation.getTranslatedWord("Show my Scheduled Orders"))
                    End If
                    oComboBtn.ValidValues.Add("IDH_TRANCD", Translation.getTranslatedWord("Set up Transaction Codes"))
                    oComboBtn.ValidValues.Add("IDH_CWABT", Translation.getTranslatedWord("Where are the Containers?"))
                    oComboBtn.ValidValues.Add("IDH_ONSITE", Translation.getTranslatedWord("What Containers are OnSite?"))
                    setVisible(oForm, "IDHMDDT", True)
                    setVisible(oForm, "S1201", True)
                    '                    If dWRBalance > 0 Then
                    '                        oItem = oForm.Items.Item("IDH_WR1OLB")
                    '                        If oItem.Visible = False Then
                    '                            oItem.Visible = True
                    '                            oItem.Enabled = False
                    '                            oItem = oForm.Items.Item("IDH_WR1ORD")
                    '                            oItem.Visible = True
                    '                            oItem.Enabled = False
                    '
                    '                            oItem = oForm.Items.Item("IDH_OSMB")
                    '                            oItem.Visible = True
                    '                            oItem = oForm.Items.Item("IDH_DOMB")
                    '                            oItem.Visible = True
                    '                            oItem = oForm.Items.Item("IDH_CIP")
                    '                            oItem.Visible = True
                    '                            oItem = oForm.Items.Item("IDH_CWABT")
                    '                            oItem.Visible = True
                    '                        End If
                    '                        setUFValue(oForm, "IDH_WR1ORD", dWRBalance)
                    '                    Else
                    '                        oItem = oForm.Items.Item("IDH_WR1OLB")
                    '                        If oItem.Visible = True Then
                    '                            oItem.Visible = False
                    '                            oItem.Enabled = False
                    '                            oItem = oForm.Items.Item("IDH_WR1ORD")
                    '                            oItem.Visible = False
                    '                            oItem.Enabled = False
                    '
                    '                            oItem = oForm.Items.Item("IDH_OSMB")
                    '                            oItem.Visible = True
                    '                            oItem = oForm.Items.Item("IDH_DOMB")
                    '                            oItem.Visible = False
                    '                            oItem = oForm.Items.Item("IDH_CIP")
                    '                            oItem.Visible = True
                    '                            oItem = oForm.Items.Item("IDH_CWABT")
                    '                            oItem.Visible = True
                    '                        End If
                    '                        setUFValue(oForm, "IDH_WR1ORD", 0)
                    '                    End If
                End If
            Else
                Dim oItem As SAPbouiCOM.Item
                oItem = oForm.Items.Item("IDH_WR1OLB")
                oItem.Visible = False

                oItem = oForm.Items.Item("IDH_WR1ORD")
                oItem.Visible = False

                setUFValue(oForm, "IDH_WR1ORD", 0)

                '#MA Start 20-06-2017
                oItem = oForm.Items.Item("IDH_WRBNT")
                oItem.Visible = False
                Dim oComboBtn As SAPbouiCOM.ButtonCombo = oItem.Specific
                Dim oCount As Integer = oComboBtn.ValidValues.Count
                For index As Integer = 0 To oCount - 1
                    oComboBtn.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                Next
                oComboBtn.Caption = Translation.getTranslatedWord("Waste & Recycle")
                If Not Config.INSTANCE.getParameterAsBool("PBIENABL", True) Then
                    oComboBtn.ValidValues.Add("IDH_PBI", Translation.getTranslatedWord("Show my Scheduled Orders"))
                    oItem.Visible = True
                End If
            End If
            '#MA End 20-06-2017
            'OnTime [#Ico000????] USA 
            'START
            If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
                doSetUSARequirements(oForm, True)
            Else
                doSetUSARequirements(oForm, False)
            End If

            'END
            'OnTime [#Ico000????] USA

            'If Config.INSTANCE.doGetDoForecasting() Then
            '    'Load BP Forecast rows
            '    If oForm.PaneLevel = FORECASTPANE Then
            '        doLoadForecastGrid(oForm)
            '    End If
            'End If

            'Setting form pane for Data Source dropdown
            Dim oItm As SAPbouiCOM.Item
            oItm = oForm.Items.Item("IDHMDDT")
            oItm.FromPane = 1
            oItm.ToPane = 1
            'Label for Data Source field 
            oItm = oForm.Items.Item("S1201")
            oItm.FromPane = 1
            oItm.ToPane = 1

            'Set pane level for Fuel County and Port Surcharges 
            oItm = oForm.Items.Item("IDHFSCHK") 'Fuel Surcharge
            oItm.FromPane = 1
            oItm.ToPane = 1

            oItm = oForm.Items.Item("IDHCTCHK") 'Coutny Tariff 
            oItm.FromPane = 1
            oItm.ToPane = 1

            oItm = oForm.Items.Item("IDHPTCHK") 'Port Tariff 
            oItm.FromPane = 1
            oItm.ToPane = 1

            Dim sBType As String = getDFValue(oForm, "OCRD", "CardType")
            FillCombos.PriceGroupsCombo(oForm.Items.Item("IDHPRGRP").Specific, sBType, "", "FALLBACK")
        End Sub

        ''** The Menu Event handler
        ''** Return True if the Event must be handled by the other Objects
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.MenuUID = Config.NAV_FIND Then
                    oForm.Items.Item("3").Click()

                ElseIf pVal.MenuUID = Config.DUPLICATE Then
                    setWFValue(oForm, "DUPBP", getDFValue(oForm, "OCRD", "CardCode"))
                End If
            Else
                oForm.Freeze(True)
                Try
                    'If pVal.MenuUID = Config.NAV_FIND OrElse _
                    If pVal.MenuUID = Config.NAV_FIRST OrElse
                       pVal.MenuUID = Config.NAV_LAST OrElse
                       pVal.MenuUID = Config.NAV_NEXT OrElse
                       pVal.MenuUID = Config.NAV_PREV Then
                        '    doShowWR1(oForm, True, pVal.MenuUID)      Function Comment By #MA
                        doShowWR1New(oForm, True, pVal.MenuUID)     '#MA Start 20-06-2017
                    ElseIf pVal.MenuUID = Config.NAV_ADD Then
                        ' doShowWR1(oForm, False, pVal.MenuUID)      Function Comment By #MA
                        doShowWR1New(oForm, False, pVal.MenuUID)    '#MA Start 20-06-2017
                    ElseIf pVal.MenuUID = Config.NAV_FIND Then
                        'doShowWR1(oForm, False, pVal.MenuUID)         Funcion Comment By #MA
                        doShowWR1New(oForm, False, pVal.MenuUID)     '#MA Start 20-06-2017
                    ElseIf pVal.MenuUID = Config.DUPLICATE Then
                        '#MA2 issue:390 20170620
                        'Address Code assigned when field is Editable
                        setWFValue(oForm, "handleduplicateAdrsCD", True)
                        doSwitchAddressCode(oForm, True)
                        doCopyBPAddress(oForm, getWFValue(oForm, "DUPBP"))
                        doShowWR1New(oForm, True, pVal.MenuUID)     '#MA Start 20-06-2017 again reload combo button when duplicate BP
                        setWFValue(oForm, "handleduplicateAdrsCD", False)
                    End If
                    'oForm.Items.Item("IDH_BACOPY").Visible = False
                Catch ex As Exception
                Finally
                    oForm.Freeze(False)
                End Try
            End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            '            If oData Is Nothing Then Exit Sub
            '            Dim oArr As ArrayList = oData
            '            If oArr.Count = 0 Then
            '                Return
            '            End If
            '        	If sModalFormType = "IDHROSRC" Then
            '                Try
            '                    Dim oMatrix As SAPbouiCOM.Matrix
            '                    Dim oCols As SAPbouiCOM.Column
            '                    Dim sRouteCode As String = oArr.Item(0).Trim()
            '                    Dim oEdit As SAPbouiCOM.EditText
            '
            '                    oMatrix = oForm.Items.Item("178").Specific
            '                    oCols = oMatrix.Columns.Item("U_ROUTE")
            '
            '                    Dim oCell As SAPbouiCOM.Cell
            '                    oCell = oCols.Cells.Item(1)
            '                    Dim oObj As Object = oCell.Specific
            '                    oEdit = oCell.Specific
            '
            '
            '                    Dim sTest As String = oEdit.Value
            '
            '                    oEdit = oForm.Items.Item("RR").Specific
            '                    sTest = oEdit.Value
            '                    oEdit.Value = sRouteCode
            '                Catch ex As Exception
            '                    'doError("doHandleModalBufferedResult", "Excption: " & ex.ToString, "")
            '                End Try
            '                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            '            End If
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDHROSRC" Then
                    Dim sRoute As String = getSharedData(oForm, "IDH_RTCODE")
                    Dim iSeq As String = getSharedData(oForm, "IDH_RTSEQ")
                    setRoute(oForm, sRoute)
                ElseIf sModalFormType = "IDHEAORSR" Then
                    setWFValue(oForm, "DOVAL", False)
                    setOrigin(oForm, getSharedData(oForm, "IDH_ORCODE"))
                ElseIf sModalFormType = "IDHCNTYSR" Then
                    'setWFValue(oForm, "DOVAL", False)
                    setUSACounty(oForm, getSharedData(oForm, "IDH_CNTYNM"))
                ElseIf sModalFormType = "IDHSALCNT" Then
                    'setWFValue(oForm, "DOVAL", False)
                    setSalesContact(oForm, getSharedData(oForm, "IDHSLPNM"))
                ElseIf sModalFormType = "IDHCSRCH" Or sModalFormType = "IDHNCSRCH" Then
                    setWFValue(oForm, "BUSWLBP", True)

                    Dim sTarget As String = getSharedData(oForm, "TRG")
                    Dim sCardCode As String = getSharedData(oForm, "CARDCODE")
                    Dim sCardName As String = getSharedData(oForm, "CARDNAME")

                    If Not (sCardCode Is Nothing) AndAlso sCardCode.Length > 0 Then
                        If sTarget = "LBP" Then
                            setItemValue(oForm, "IDHLBPC", sCardCode)
                            setItemValue(oForm, "IDHLBPN", sCardName)
                        ElseIf sTarget = "CS" Then
                            setItemValue(oForm, "IDHONCS", sCardCode)
                        End If
                    Else
                        If sTarget = "LBP" Then
                            setItemValue(oForm, "IDHLBPC", "")
                            setItemValue(oForm, "IDHLBPN", "")
                        ElseIf sTarget = "CS" Then
                            setItemValue(oForm, "IDHONCS", "")
                        End If
                    End If
                    setWFValue(oForm, "BUSWLBP", False)
                ElseIf sModalFormType = "IDHASRCH2" Then
                    Dim sCpyTarget As String = getSharedData(oForm, "CPYTRG")
                    Dim sCardCode As String = getSharedData(oForm, "CARDCODE")
                    Dim sCardName As String = getSharedData(oForm, "CARDNAME")
                    Dim sAddress As String = getSharedData(oForm, "ADDRESS")
                    Dim sAddType As String = getSharedData(oForm, "ADRESTYPE")

                    setCopyAddress(oForm, sCpyTarget, sCardCode, sCardName, sAddress, sAddType)
                ElseIf sModalFormType = "IDHPRCSRC" Then
                    Dim sCCCode As String = getSharedData(oForm, "IDH_CODE")
                    setItemValue(oForm, "IDHCSTC", sCCCode)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Results - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
                setWFValue(oForm, "DOVAL", True)
            End Try
        End Sub

        Protected Sub setRoute(ByRef oForm As SAPbouiCOM.Form, ByVal sRoute As String)
            Try
                Dim oMatrix As SAPbouiCOM.Matrix
                Dim oCols As SAPbouiCOM.Column
                Dim oEdit As SAPbouiCOM.EditText

                oMatrix = oForm.Items.Item("178").Specific
                oCols = oMatrix.Columns.Item("U_ROUTE")

                Dim oCell As SAPbouiCOM.Cell
                oCell = oCols.Cells.Item(1)
                Dim oObj As Object = oCell.Specific
                oEdit = oCell.Specific

                oEdit.Value = sRoute
            Catch ex As Exception
                'doError("doHandleModalBufferedResult", "Excption: " & ex.ToString, "")
            End Try
        End Sub

        Protected Sub setCopyAddress(ByRef oForm As SAPbouiCOM.Form, ByVal sCpyTarget As String, ByVal sCardCode As String, ByVal sCardName As String, ByVal sAddress As String, ByVal sAddType As String)
            Try
                Dim oMatrix As SAPbouiCOM.Matrix
                Dim oCols As SAPbouiCOM.Column
                Dim oEdit As SAPbouiCOM.EditText
                Dim oCombo As SAPbouiCOM.ComboBox
                Dim oCell As SAPbouiCOM.Cell

                oMatrix = oForm.Items.Item("178").Specific

                oForm.Freeze(True)

                Try
                    oCols = oMatrix.Columns.Item("1")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "ADDRESS"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "ADDRESS")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("2")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "STREET"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "STREET")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("3")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "BLOCK"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "BLOCK")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("4")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "CITY"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "CITY")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("5")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "ZIPCODE"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "ZIPCODE")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("6")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "COUNTY"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "COUNTY")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("8")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "COUNTRY"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "COUNTRY")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("7")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "STATE"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "STATE")
                        End If
                    End If
                Catch ex As Exception
                End Try


                Try
                    oCols = oMatrix.Columns.Item("9")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "LICTRADNUM"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "LICTRADNUM")
                        End If
                    End If
                Catch ex As Exception
                End Try


                Try
                    oCols = oMatrix.Columns.Item("2000")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "BUILDING"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "BUILDING")
                        End If
                    End If
                Catch ex As Exception
                End Try


                Try
                    oCols = oMatrix.Columns.Item("2003")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "STREETNO"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "STREETNO")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_ISBACCDF")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "ISBACCDF"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "ISBACCDF")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_ISBACCTF")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "ISBACCTF"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "ISBACCTF")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_ISBACCTT")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "ISBACCTT"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "ISBACCTT")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_ISBACCRA")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "ISBACCRA"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "ISBACCRA")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_ISBACCRV")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "ISBACCRV"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "ISBACCRV")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_ISBCUSTRN")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "ISBCUSTRN"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "ISBCUSTRN")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_TEL1")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "TEL1"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "TEL1")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_ROUTE")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "ROUTE"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "ROUTE")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_IDHSEQ")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "SEQ"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "SEQ")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_IDHACN")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "CONA"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "CONA")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_Origin")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "ORIGIN"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "ORIGIN")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_IDHSLCNO")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "SITELIC"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "SITELIC")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_IDHSteId")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "SITEID"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "SITEID")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_IDHFAX")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "FAX"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "FAX")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_IDHEMAIL")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "EMAIL"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "EMAIL")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_IDHSTRNO")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "STRNO"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "STRNO")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_IDHPCD")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "PRECD"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "PRECD")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_PREEXPDTTO")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "PREEXPDTTO"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "PREEXPDTTO").ToString()
                            'oEdit.Value = Convert.ToDateTime(getSharedData(oForm, "PREEXPDTTO").ToString()).ToShortDateString().Replace("/", "")
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("U_EPASITEID")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "EPASITEID"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = getSharedData(oForm, "EPASITEID")
                        End If
                    End If
                Catch ex As Exception
                End Try


                'put this col lin last to set focus back on top 
                Try
                    oCols = oMatrix.Columns.Item("1")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(getSharedData(oForm, "ADDRESS"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = oEdit.Value
                        End If
                    End If
                Catch ex As Exception
                End Try


                oForm.Freeze(False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error copying address")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCA", {""})
            End Try
        End Sub

        Protected Sub setOrigin(ByRef oForm As SAPbouiCOM.Form, ByVal sOrigin As String)
            Try
                Dim oMatrix As SAPbouiCOM.Matrix
                Dim oCols As SAPbouiCOM.Column
                Dim oEdit As SAPbouiCOM.EditText

                oMatrix = oForm.Items.Item("178").Specific
                oCols = oMatrix.Columns.Item("U_Origin")

                Dim oCell As SAPbouiCOM.Cell
                oCell = oCols.Cells.Item(1)
                Dim oObj As Object = oCell.Specific
                oEdit = oCell.Specific
                oEdit.Value = sOrigin
            Catch ex As Exception
                'doError("doHandleModalBufferedResult", "Excption: " & ex.ToString, "")
            End Try
        End Sub

        Protected Sub setUSACounty(ByRef oForm As SAPbouiCOM.Form, ByVal sUSACounty As String)
            Try
                Dim oMatrix As SAPbouiCOM.Matrix
                Dim oCols As SAPbouiCOM.Column
                Dim oEdit As SAPbouiCOM.EditText

                oMatrix = oForm.Items.Item("178").Specific
                oCols = oMatrix.Columns.Item("U_USACOUNTY")

                Dim oCell As SAPbouiCOM.Cell
                oCell = oCols.Cells.Item(1)
                Dim oObj As Object = oCell.Specific
                oEdit = oCell.Specific
                oEdit.Value = sUSACounty
            Catch ex As Exception
                'doError("doHandleModalBufferedResult", "Excption: " & ex.ToString, "")
            End Try
        End Sub

        Protected Sub setSalesContact(ByRef oForm As SAPbouiCOM.Form, ByVal sSalesContact As String)
            Try
                Dim oMatrix As SAPbouiCOM.Matrix
                Dim oCols As SAPbouiCOM.Column
                Dim oEdit As SAPbouiCOM.EditText

                oMatrix = oForm.Items.Item("178").Specific
                oCols = oMatrix.Columns.Item("U_SalesCnt")

                Dim oCell As SAPbouiCOM.Cell
                oCell = oCols.Cells.Item(1)
                Dim oObj As Object = oCell.Specific
                oEdit = oCell.Specific
                oEdit.Value = sSalesContact
            Catch ex As Exception
                'doError("doHandleModalBufferedResult", "Excption: " & ex.ToString, "")
            End Try
        End Sub

        Protected Overrides Sub doHandleModalCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            Try
                If sModalFormType = "IDHCSRCH" Or sModalFormType = "IDHNCSRCH" Then
                    setWFValue(oForm, "BUSWLBP", True)

                    Dim sTarget As String = getSharedData(oForm, "TRG")
                    If sTarget = "LBP" Then
                        setItemValue(oForm, "IDHLBPN", "")
                        setItemValue(oForm, "IDHLBPC", "")
                        doSetFocus(oForm, "IDHLBPC")
                    End If
                    setWFValue(oForm, "BUSWLBP", False)
                ElseIf sModalFormType = "IDHEAORSR" Then
                    Dim sOrigin As String = getSharedData(oForm, "IDH_ORCODE")
                    doWarnMess("The Origin ('" + sOrigin + "') you type, does not exist in the system.", SAPbouiCOM.BoMessageTime.bmt_Short)
                    setOrigin(oForm, "")
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error canceling the Form - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCF", {sModalFormType})
            End Try
        End Sub


        Public Overrides Sub doClose()
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)

            Dim bResized As Boolean = getWFValue(oForm, "RESIZED")
            If bResized Then
                oForm.Resize(w_old, h_old)
            End If
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                'If oForm.PaneLevel = FORECASTPANE Then
                '    setWFValue(oForm, "WasOnForeCast", True)
                'Else
                '    setWFValue(oForm, "WasOnForeCast", False)
                'End If

                '*** Check for a valid UOM
                'OnTime 22323: UOM validation
                'Note: validate UOM right in the begining of the price calculation
                'Dim sUOM As String = getFormDFValue(oForm, "U_IDHUOM")
                Dim sUOM As String = oForm.DataSources.DBDataSources.Item("OCRD").GetValue("U_IDHUOM", oForm.DataSources.DBDataSources.Item("OCRD").Offset).Trim()

                'if it is empty or 'CM' dont validated
                'Note: 'CM' UOM is coming from business logic: so it doesn't exist in the OWGT table
                If (Not (sUOM = String.Empty)) And
                   (Not (sUOM.ToUpper() = "cm".ToUpper())) Then
                    If Not isValidUOM(oForm, sUOM) Then
                        Dim saParam As String() = {sUOM}
                        doSetFocus(oForm, "IDHUOM")
                        doResError("Incorrect UOM entered: " & sUOM, "INVLDUOM", saParam)
                        BubbleEvent = False
                        Exit Sub
                    End If
                End If

                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    Dim sCardCode As String
                    With oForm.DataSources.DBDataSources.Item("OCRD")
                        sCardCode = .GetValue("CardCode", .Offset).Trim()
                    End With
                    setParentSharedData(oForm, "CARDCODE", sCardCode)
                Else
                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        setWFValue(oForm, "DOWR1", True)
                    Else
                        setWFValue(oForm, "DOWR1", False)
                    End If
                End If

                If oForm.Mode.Equals(SAPbouiCOM.BoFormMode.fm_FIND_MODE) = True Then
                    oForm.Items.Item("3").Click()
                End If
                ''MA Start 16-07-2015
                ''validate Forecast
                'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                '    If lookups.Config.INSTANCE.doGetDoForecasting() Then
                '        'Dim oUpdateGrid As DBOGrid = getWFValue(oForm, "FORECGRID")
                '        'Validate Grid for Multi Ccy BP's 
                '        If getDFValue(oForm, "OCRD", "Currency").ToString.Trim = "##" Then
                '            Dim oDBGrid As DBOGrid = DBOGrid.getInstance(oForm, "FORECGRID")
                '            Dim bValid As Boolean = True
                '            Dim iRowCount As Integer = If(oDBGrid.getRowCount > 1, oDBGrid.getRowCount - 2, oDBGrid.getRowCount - 1)
                '            Dim iLastIndex As Integer = 0
                '            For iIndex As Integer = 0 To iRowCount
                '                iLastIndex = iIndex
                '                Dim sBPCurr As String = oDBGrid.doGetFieldValue(IDH_BPFORECST._BPCurr, iIndex)
                '                If sBPCurr = "" Then
                '                    bValid = False
                '                    Exit For
                '                End If
                '            Next
                '            If Not bValid Then
                '                doResError("Invalid Currency, please select a valid currency for the forecast row(" + iLastIndex.ToString + ").", "ERIVCBPF", Nothing)
                '                BubbleEvent = False
                '                Exit Sub
                '            End If
                '        End If
                '    End If
                'End If
                ' ''MA End 16-07-2015 
                'Else
                If oForm.Mode.Equals(SAPbouiCOM.BoFormMode.fm_FIND_MODE) = False Then
                    'If Config.INSTANCE.doGetDoForecasting() Then
                    '    Dim oUpdateGrid As DBOGrid = getWFValue(oForm, "FORECGRID")
                    '    If oUpdateGrid.DBObject.doProcessData() = False Then
                    '        BubbleEvent = False
                    '    Else
                    '        'Dim oWO As IDH_JOBENTR
                    '        'Dim oWOR As IDH_JOBSHD
                    '        'Dim oWORef_Pair As Hashtable = getWFValue(oForm, "WOREF_PAIR")

                    '        'If Not oWORef_Pair Is Nothing Then
                    '        '    oWO = New IDH_JOBENTR
                    '        '    Dim sCustRef As String
                    '        '    For Each sWOCode As String In oWORef_Pair.Keys
                    '        '        sCustRef = oWORef_Pair.Item(sWOCode)

                    '        '        If oWO.getByKey(sWOCode) Then
                    '        '            oWO.U_CustRef = sCustRef

                    '        '            oWOR = oWO.getJobRows()
                    '        '            oWOR.setValueForAllRows(IDH_JOBSHD._CustRef, sCustRef)

                    '        '            oWO.MustUpdateForecast = False
                    '        '            oWO.doProcessData()
                    '        '            oWO.MustUpdateForecast = True

                    '        '            oWOR.doProcessData()
                    '        '        End If
                    '        '    Next
                    '        '    oWORef_Pair.Clear()
                    '        'End If
                    '    End If
                    'End If
                    If goParent.doCheckModal(oForm.UniqueID) = True Then
                        doReturnFromModalShared(oForm, True)
                        'Else
                        '    If getWFValue(oForm, "DOWR1") = True Then
                        '        'doShowWR1(oForm, True)
                        '        'oForm.Items.Item("IDH_BACOPY").Visible = False
                        '    End If
                    End If
                Else
                    '    'doShowWR1(oForm, True)
                    'setWFValue(oForm, "WR1REFRESH", True)
                End If

                'Dim bWasOnForeCast As Boolean = getWFValue(oForm, "WasOnForeCast")
                'If bWasOnForeCast Then
                '    'oForm.PaneLevel = FORECASTPANE
                '    doClick(oForm, "IDH_TABFCS")
                'End If
            End If

        End Sub

        '*** Handles Item Event
        Public Overrides Function doFormDataEvent(ByVal oForm As SAPbouiCOM.Form, ByRef oBusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean) As Boolean
            If oBusinessObjectInfo.BeforeAction = False Then
                If getWFValue(oForm, "DOWR1") = True Then
                    '  doShowWR1(oForm, True, "")   'Commit By #MA
                    doShowWR1New(oForm, True, "")     '#MA Start 20-06-2017

                    'oForm.Items.Item("IDH_BACOPY").Visible = False
                End If
            End If
            If oBusinessObjectInfo.BeforeAction = False AndAlso oBusinessObjectInfo.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD Then
                setSharedData(oForm, "SETENQIDs", "")
                setSharedData(oForm, "AddressDetail", Nothing)
                If getSharedData(oForm, "sAddressLog") IsNot Nothing Then
                    Dim sAddressLog As Hashtable = getSharedData(oForm, "sAddressLog")
                    sAddressLog.Clear()
                    setSharedData(oForm, "sAddressLog", sAddressLog)
                End If
                If oForm.PaneLevel = "7" Then
                    'oForm.Items.Item("69").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                    docheckAddressAndSetCopyToButton(oForm)
                End If
            ElseIf oBusinessObjectInfo.BeforeAction = False AndAlso oBusinessObjectInfo.EventType = SAPbouiCOM.BoEventTypes.et_FORM_LOAD Then
                Dim sAddressLog As New Hashtable '= getSharedData(oForm, "sAddressLog")
                'sAddressLog.Clear()
                setSharedData(oForm, "sAddressLog", sAddressLog)
            ElseIf oBusinessObjectInfo.BeforeAction = False AndAlso oBusinessObjectInfo.EventType = SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE Then


            End If
            Return True
        End Function


        Private Sub doBranchCombo(ByRef oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDHBRAN", "OUBR", "Code", "Remarks")
        End Sub

        Private Sub doMDateCombo(ByRef oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDHMDDT")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oItem.Specific
            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("", getTranslatedWord("Default"))
            oCombo.ValidValues.Add("NOW", getTranslatedWord("Current Date"))
            oCombo.ValidValues.Add("BDATE", getTranslatedWord("Booking Date"))
            oCombo.ValidValues.Add("AEDATE", getTranslatedWord("Job Completion Date"))

        End Sub

        Private Sub doObligatedCombo(ByRef oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDHOBLED")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oItem.Specific
            doClearValidValues(oCombo.ValidValues)

            'oCombo.ValidValues.Add("", "None")
            Dim sWord As String
            '        	sWord = getTranslatedWord("Obligated")
            '       		oCombo.ValidValues.Add(sWord, sWord)
            '
            '       		sWord = getTranslatedWord("Non-Obligated")
            '        	oCombo.ValidValues.Add(sWord, sWord)

            sWord = Config.Parameter("OBTRUE")
            If sWord Is Nothing OrElse sWord.Length = 0 Then
                sWord = getTranslatedWord("Obligated")
            End If
            oCombo.ValidValues.Add(sWord, sWord)

            sWord = Config.Parameter("OBFALSE")
            If sWord Is Nothing OrElse sWord.Length = 0 Then
                sWord = getTranslatedWord("Non-Obligated")
            End If
            oCombo.ValidValues.Add(sWord, sWord)
        End Sub

        'OnTime 22323: UOM validation
        'Note: validate UOM right in the begining of the price calculation
        Private Function isValidUOM(ByRef oForm As SAPbouiCOM.Form, ByRef strUOM As String) As Boolean
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Dim bolValidateUOM As Boolean = True

            Try
                oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                oRecordSet.DoQuery("select UnitCode from OWGT")
                If oRecordSet.RecordCount > 0 Then
                    Dim oWeightMeasures As SAPbobsCOM.WeightMeasures = Nothing
                    oWeightMeasures = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oWeightMeasures)
                    oWeightMeasures.Browser.Recordset = oRecordSet
                    While Not oWeightMeasures.Browser.EoF
                        If strUOM.ToUpper() = oWeightMeasures.UnitDisplay.ToUpper() Then
                            bolValidateUOM = True
                            Exit While
                        Else
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Invalid UOM! Please provide a valid UOM.")
                            bolValidateUOM = False
                        End If
                        oWeightMeasures.Browser.MoveNext()
                    End While
                    Return bolValidateUOM
                End If
                Return bolValidateUOM
            Catch ex As Exception
            Finally
                oRecordSet = Nothing
            End Try
            Return bolValidateUOM
        End Function

        'OnTime 35413: PO3 Outbound Material
        'Adding Material Monitoring Tab
        Private Sub doSetWidth(ByRef oForm As SAPbouiCOM.Form, ByVal sItem As String, ByVal iWidth As Integer)
            Try
                oForm.Items.Item(sItem).Width = iWidth
            Catch ex As Exception
            End Try
        End Sub

        'Public Sub doAddForecastTab(ByRef oForm As SAPbouiCOM.Form)
        '    'Dim oItem As SAPbouiCOM.Item
        '    Dim owItem As SAPbouiCOM.Item
        '    Dim oFolder As SAPbouiCOM.Folder
        '    Dim ow2Item As SAPbouiCOM.Item = oForm.Items.Item("23") 'Attachments Tab

        '    TABWIDTH = ow2Item.Width / 10

        '    doSetWidth(oForm, "3", TABWIDTH)
        '    doSetWidth(oForm, "13", TABWIDTH)
        '    doSetWidth(oForm, "15", TABWIDTH)
        '    doSetWidth(oForm, "156", TABWIDTH)
        '    doSetWidth(oForm, "214", TABWIDTH)
        '    doSetWidth(oForm, "4", TABWIDTH)
        '    doSetWidth(oForm, "540002085", TABWIDTH)
        '    doSetWidth(oForm, "134", TABWIDTH)

        '    ow2Item = oForm.Items.Item("9")
        '    ow2Item.Width = TABWIDTH

        '    'ow2Item = oForm.Items.Item("9") 'Attachments Tab

        '    owItem = oForm.Items.Add(FORECASTTAB, SAPbouiCOM.BoFormItemTypes.it_FOLDER)
        '    owItem.Left = ow2Item.Left + ow2Item.Width
        '    owItem.Top = ow2Item.Top
        '    owItem.Width = TABWIDTH 'ow2Item.Width
        '    owItem.Height = ow2Item.Height
        '    owItem.Visible = True
        '    owItem.AffectsFormMode = False
        '    oFolder = owItem.Specific
        '    oFolder.Caption = getTranslatedWord("Forecasting")
        '    oFolder.GroupWith("9")
        'End Sub

        'Public Sub doAddForecastGrid(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oForeGrid As DBOGrid = getWFValue(oForm, "FORECGRID")
        '    If oForeGrid Is Nothing Then
        '        oForeGrid = DBOGrid.getInstance(oForm, "FORECGRID")
        '        If oForeGrid Is Nothing Then
        '            Dim oForecast As IDH_BPFORECST = New IDH_BPFORECST(Me, oForm)

        '            Dim owItem As SAPbouiCOM.Item = oForm.Items.Item(FCGRIDSTENCIL)
        '            oForeGrid = New DBOGrid(Me, oForm, "FORECGRID", _
        '                owItem.Left, owItem.Top, owItem.Width + TABWIDTH - 10, owItem.Height, _
        '                oForecast)

        '            'oForeGrid = New DBOGrid(Me, oForm, "21", "FORECGRID", oForecast)
        '            oForeGrid.getSBOItem.FromPane = FORECASTPANE
        '            oForeGrid.getSBOItem.ToPane = FORECASTPANE
        '            'oForeGrid.getSBOItem().AffectsFormMode = False
        '        End If
        '        setWFValue(oForm, "FORECGRID", oForeGrid)
        '    End If
        '    oForeGrid.doSetDoCount(True)
        '    doSetFilterFields(oForeGrid)
        '    doSetListFields(oForeGrid)
        'End Sub


        Protected Overridable Sub doSetFilterFields(ByVal oGridN As DBOGrid)
        End Sub

        Protected Overridable Sub doSetListFields(ByVal oGridN As DBOGrid)
            oGridN.doAddListField(IDH_BPFORECST._Code, "Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._Name, "Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._MatStatus, "Status", True, 20, "CHECKBOX", Nothing)
            oGridN.doAddListField(IDH_BPFORECST._DocType, "Doc Type", True, 20, "COMBOBOX", Nothing)
            ''oGridN.doAddListField("U_ItemCd", "Waste Code", True, 20, "SRC:IDHWISRC(WST)[IDH_ITMCOD][ITEMCODE;U_ItemDsc=ITEMNAME;U_UOM=UOM]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField(IDH_BPFORECST._ItemCd, "Waste Code", True, 20, "SRC:IDHWISRC(WST)[IDH_ITMCOD;IDH_INVENT=][ITEMCODE;U_ItemDsc=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField(IDH_BPFORECST._ItemDsc, "Waste Description", True, -1, "SRC:IDHWISRC(WST)[IDH_ITMNAM;IDH_INVENT=%N][U_ItemCd=ITEMCODE;ITEMNAME]", Nothing)

            If Config.ParameterAsBool("FLALTITM", False) = True Then
                oGridN.doAddListField(IDH_BPFORECST._AltWasDsc, "Alternate Waste Description", True, 20, "SRC:IDHALTWISRC(AWST)[IDH_ALITM=" + IDH_BPFORECST._AltWasDsc + ";IDH_ITMCOD=#" + IDH_BPFORECST._ItemCd + ";IDH_ITMNAM=#" + IDH_BPFORECST._ItemDsc + "][ItemAltName;" + IDH_BPFORECST._AltWDSCd + "=ItemAltCode]", Nothing)
                oGridN.doAddListField(IDH_BPFORECST._AltWDSCd, "Alternate Waste Desc Code", False, 0, Nothing, Nothing)
            End If

            oGridN.doAddListField(IDH_BPFORECST._EstQty, "Estimated Qty", True, 15, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._UOM, "UOM", True, 15, "COMBOBOX", Nothing)
            oGridN.doAddListField(IDH_BPFORECST._ActiveFrom, "Active From", True, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._ActiveTo, "Active To", True, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._ExpLdWgt, "Expected Load Weight", True, 15, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._ComQty, "Committed Qty", True, 15, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._RemCQty, "Remaining Commited Qty", True, 15, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._DoneQty, "Done Qty", True, 15, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._RemQty, "Remaining Qty", True, 15, Nothing, Nothing)
            ''##MA Start 23-10-2014 Issue#419
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_BPFORECAST_PRICERS) = False Then '
                oGridN.doAddListField(IDH_BPFORECST._BuyingPrice, "Buying Price", True, 0, Nothing, Nothing)
                oGridN.doAddListField(IDH_BPFORECST._SellingPrice, "Selling Price", True, 0, Nothing, Nothing)
            Else
                oGridN.doAddListField(IDH_BPFORECST._BuyingPrice, "Buying Price", True, 20, Nothing, Nothing)
                oGridN.doAddListField(IDH_BPFORECST._SellingPrice, "Selling Price", True, 20, Nothing, Nothing)
            End If
            ''##MA Start 23-10-2014 Issue#419
            oGridN.doAddListField(IDH_BPFORECST._CardCd, "Customer", False, 0, Nothing, Nothing) ', iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField(IDH_BPFORECST._CardName, "Customer Name", False, 0, Nothing, Nothing)
            'oGridN.doAddListField(IDH_BPFORECST._BPCurr, "Customer Currency", False, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._BPCurr, "Customer Currency", True, 20, "COMBOBOX", Nothing)
            'oGridN.doAddListField(IDH_BPFORECST._CardType, "Customer Type", False, 20, Nothing, Nothing)

            oGridN.doAddListField(IDH_BPFORECST._Address, "Address", True, -1, "SRC*IDHASRCH(ADDR)[IDH_CUSCOD=>IDH_CUST;IDH_ADRTYP=%S;IDH_APPLYFLTR=" & Config.INSTANCE.getParameterAsBool("FLTRADDR", False).ToString & "][ADDRESS;U_AddrssLN=ADDRSCD]", Nothing)
            oGridN.doAddListField(IDH_BPFORECST._AddrssLN, "Address Line Num", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._CCIC, "CCIC", True, 20, "COMBOBOX", Nothing)
            oGridN.doAddListField(IDH_BPFORECST._CustRef, "Customer PO", True, -1, Nothing, Nothing)
            '## MA Start 15-10-2014
            oGridN.doAddListField(IDH_BPFORECST._TRNCd, "Transaction Code", True, 30, "COMBOBOX", Nothing)
            '## MA End 15-10-2014
            oGridN.doAddListField(IDH_BPFORECST._LnkWOH, "Linked WOH", False, -1, Nothing, Nothing)

            oGridN.doSynchFields()
        End Sub



        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            'If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH AndAlso pVal.BeforeAction AndAlso pVal.ColUID = IDH_BPFORECST._AltWasDsc Then
            '    'goParent.STATUSBAR.SetText(pVal.EventType.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            '    setParentSharedData(oForm, "IDH_ITMCOD", pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemCd), pVal.Row))
            '    setParentSharedData(oForm, "IDH_ITMNAM", pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemDsc), pVal.Row))
            ''below is the logic, transfered to DB Object
            'ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED AndAlso pVal.BeforeAction = False Then
            '    If pVal.ColUID = IDH_BPFORECST._Address Then
            '        Dim sAddress As String = pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._Address), pVal.Row)
            '        If sAddress.Trim = "" Then
            '            pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._AddrssLN), pVal.Row, "")
            '        Else
            '            Dim sCardCode As String = IDH_BPFORECST._CardCd
            '            If sCardCode.Trim <> "" AndAlso sAddress.Trim <> "" Then
            '                Dim sAddressLineNum As String = com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCardCode, sAddress, "LineNum")
            '                pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._AddrssLN), pVal.Row, sAddressLineNum)
            '            Else
            '                pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._AddrssLN), pVal.Row, "")
            '            End If
            '        End If
            '    End If

            'If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
            '    If pVal.BeforeAction = True Then
            '        If Config.INSTANCE.doGetDoForecasting() Then
            '            Dim oGridN As DBOGrid = getWFValue(oForm, "FORECGRID")
            '            Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
            '            Dim iSelectionCount As Integer = oSelected.Count

            '            If iSelectionCount = 1 Then
            '                Dim oMenuItem As SAPbouiCOM.MenuItem
            '                Dim oMenus As SAPbouiCOM.Menus
            '                Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
            '                'Dim sSelectedForcast As String
            '                Dim iRow As Integer
            '                Dim sForecastNr As String
            '                Dim sWONr As String

            '                'iRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)
            '                iRow = oGridN.GetDataTableRowIndex(oSelected, 0)
            '                sForecastNr = oGridN.doGetFieldValue("Code", iRow)
            '                sWONr = oGridN.doGetFieldValue("U_LnkWOH", iRow)

            '                oMenuItem = goParent.goApplication.Menus.Item(IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU)
            '                oMenus = oMenuItem.SubMenus
            '                oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
            '                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING

            '                oCreationPackage.UniqueID = "NEXTPER"
            '                oCreationPackage.String = getTranslatedWord("Create Forecast for Next Period") & " (" & (iRow + 1) & ")[" & sForecastNr & "]"
            '                oCreationPackage.Enabled = True
            '                oMenus.AddEx(oCreationPackage)
            '                '## MA Start 28-08-2014 Issue#409; Stop Create WOR if not active of below Activeto date
            '                Dim sActiveTo As String = oGridN.doGetFieldValue("U_ActiveTo", iRow)
            '                Dim bActive As Boolean = True
            '                If sActiveTo IsNot Nothing AndAlso sActiveTo <> "" AndAlso CDate(sActiveTo) < Today Then
            '                    bActive = False
            '                End If
            '                Dim sStart As String = oGridN.doGetFieldValue("U_MatStatus", iRow)
            '                If sWONr.Length = 0 AndAlso sStart = "Y" AndAlso bActive Then


            '                    oCreationPackage.UniqueID = "NEWWO"
            '                    oCreationPackage.String = getTranslatedWord("Create Waste Order from Forecast") & " (" & (iRow + 1) & ")[" & sForecastNr & "]"
            '                    oCreationPackage.Enabled = True
            '                    oMenus.AddEx(oCreationPackage)
            '                End If
            '                '## MA End 28-08-2014 Issue#409; Stop Create WOR if not active of below Activeto date
            '            End If
            '            Return True
            '        End If
            '    Else
            '        If goParent.goApplication.Menus.Exists("NEXTPER") Then
            '            goParent.goApplication.Menus.RemoveEx("NEXTPER")
            '        End If
            '        If goParent.goApplication.Menus.Exists("NEWWO") Then
            '            goParent.goApplication.Menus.RemoveEx("NEWWO")
            '        End If
            '        Return True
            '    End If
            'ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then
            '    Dim sField As String = pVal.ColUID
            '    If sField.Equals("U_LnkWOH") Then
            '        If pVal.BeforeAction = False Then
            '            Dim oGridN As DBOGrid = getWFValue(oForm, "FORECGRID")
            '            Dim oForecast As IDH_BPFORECST = oGridN.DBObject
            '            Dim oData As New ArrayList

            '            ' 0 - Action
            '            ' 1 - WO Code
            '            ' 2 - IDH_BPFORECST
            '            oData.Add("FCDBOUpdate")
            '            oData.Add(oForecast.U_LnkWOH)
            '            oData.Add(oForecast)
            '            goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
            '        End If
            '    End If
            'ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
            '    If pVal.BeforeAction = False Then
            '        If pVal.oData.MenuUID.Equals("NEXTPER") Then
            '            Dim oGridN As DBOGrid = getWFValue(oForm, "FORECGRID")
            '            doCreateNextPeriod(oGridN)
            '        ElseIf pVal.oData.MenuUID.Equals("NEWWO") Then

            '            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
            '                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse _
            '                oForm.Mode = SAPbouiCOM.BoFormMode.fm_EDIT_MODE Then
            '                com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage("UNSAVED")
            '            Else
            '                Dim oWO As com.idh.dbObjects.User.IDH_JOBENTR = New com.idh.dbObjects.User.IDH_JOBENTR()
            '                Dim sBP1 As String = getDFValue(oForm, "OCRD", "CardCode")
            '                Dim sBR2 As String = getDFValue(oForm, "OCRD", "U_IDHLBPC")
            '                Dim sBPType As String = getDFValue(oForm, "OCRD", "CardType")

            '                Dim oSelected As SAPbouiCOM.SelectedRows = pVal.oGrid.getGrid().Rows.SelectedRows
            '                Dim iSrcRow As Integer = pVal.oGrid.GetDataTableRowIndex(oSelected, 0)

            '                If Config.INSTANCE.doGetDoForecasting() Then
            '                    Dim oGridN As DBOGrid = getWFValue(oForm, "FORECGRID")
            '                    Dim oForecast As IDH_BPFORECST = oGridN.DBObject
            '                    oForecast.gotoRow(iSrcRow)
            '                    Dim sAddress As String = oForecast.U_Address

            '                    oWO.doSetDefaultBPs()
            '                    ''## MA Start 03-07-2014
            '                    If sBPType = "C" Then
            '                        'oWO.doSetCustomer(sBP1, sAddress)
            '                        'oWO.doSetProducer(sBR2)
            '                        oWO.doSetCustomer(sBP1, sAddress, True)
            '                        oWO.doSetProducer(sBR2, sAddress, True)
            '                    Else
            '                        'oWO.doSetCustomer(sBR2)
            '                        'oWO.doSetProducer(sBP1, sAddress)
            '                        oWO.doSetProducer(sBP1, sAddress, True)
            '                        oWO.doSetCustomer(sBR2, sAddress, True)
            '                    End If
            '                    ''## MA End 03-07-2014
            '                    oWO.U_ForeCS = oForecast.Code
            '                    oWO.U_Status = oForecast.U_DocType

            '                    oWO.MustUpdateForecast = False
            '                    oWO.U_User = goParent.goDICompany.UserSignature()
            '                    oWO.U_LckPrc = "Y"
            '                    ''## MA Start 15-10-2017
            '                    oWO.U_TRNCd = oForecast.U_TRNCd
            '                    ''## MA End 15-10-2017
            '                    If Not oForecast.U_CustRef Is Nothing AndAlso oForecast.U_CustRef.Length > 0 Then
            '                        oWO.U_CustRef = oForecast.U_CustRef
            '                    End If

            '                    If oWO.doAddDataRow() Then
            '                        oForecast.U_LnkWOH = oWO.Code
            '                        If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
            '                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            '                        End If
            '                        Dim oData As New ArrayList
            '                        ' 0 - Action
            '                        ' 1 - WO Code
            '                        ' 2 - IDH_BPFORECST
            '                        oData.Add("FCDBOUpdate")
            '                        oData.Add(oWO.Code)
            '                        oData.Add(oForecast)
            '                        goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
            '                    End If
            '                    oWO.MustUpdateForecast = True
            '                End If
            '            End If
            '        End If
            '    End If
            'ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
            '    If pVal.BeforeAction = False Then
            '        doSetLastLine(oForm, pVal)
            '    End If
            'ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then

            'End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function


        Public Sub doCreateNextPeriod(ByVal oGridN As DBOGrid)
            oGridN.getSBOForm().Freeze(True)
            Try
                Dim oForecast As IDH_BPFORECST = oGridN.DBObject

                'Dim sForecastNr As String
                Dim iSrcRow As Integer
                Dim iDstRow As Integer = oGridN.getLastRowIndex()
                Dim oSelected As SAPbouiCOM.SelectedRows

                oSelected = oGridN.getGrid().Rows.SelectedRows()
                oGridN.doAddRow(False, True)

                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_BPFORECST.AUTONUMPREFIX, "BPFORECST")
                iSrcRow = oGridN.GetDataTableRowIndex(oSelected, 0)
                oGridN.doCopyRow(iSrcRow, iDstRow)

                'Dim oFromDate As DateTime = oGridN.doGetFieldValue("U_ActiveFrom", iSrcRow)
                Dim oToDate As DateTime = oGridN.doGetFieldValue("U_ActiveTo", iSrcRow)
                If com.idh.utils.Dates.isValidDate(oToDate) Then
                    Dim oFromDate As DateTime
                    Dim iMonth As Integer = oToDate.Month
                    Dim iYear As Integer = oToDate.Year

                    iMonth = iMonth + 1
                    If iMonth > 12 Then
                        iMonth = iMonth - 12
                        iYear = iYear + 1
                    End If
                    oFromDate = New DateTime(iYear, iMonth, 1)
                    oToDate = oForecast.doCalcToDate(oFromDate, miDefaultMonthPeriod)

                    oGridN.doSetFieldValue("U_ActiveFrom", iDstRow, oFromDate, True)
                    oGridN.doSetFieldValue("U_ActiveTo", iDstRow, oToDate, True)
                End If
                Dim oEstQty As Double = oGridN.doGetFieldValue("U_EstQty", iSrcRow)

                oGridN.doActivateRow(iDstRow)
                oGridN.doSetFieldValue("Code", iDstRow, oNumbers.CodeCode, True)
                oGridN.doSetFieldValue("Name", iDstRow, oNumbers.NameCode, True)

                'oGridN.doSetFieldValue("U_EstQty", iDstRow, 0, True)
                oGridN.doSetFieldValue("U_ComQty", iDstRow, 0, True)
                oGridN.doSetFieldValue("U_DoneQty", iDstRow, 0, True)
                oGridN.doSetFieldValue("U_RemQty", iDstRow, oEstQty, True)
                oGridN.doSetFieldValue("U_RemCQty", iDstRow, oEstQty, True)
                oGridN.doSetFieldValue("U_LnkWOH", iDstRow, "", True)

                If Not oGridN.getSBOForm().Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    oGridN.getSBOForm().Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                End If
            Finally
                oGridN.getSBOForm().Freeze(False)
            End Try
        End Sub

        Public Sub doSetLastLine(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base)
            If Config.INSTANCE.doGetDoForecasting() Then
                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "FORECGRID")
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_BPFORECST.AUTONUMPREFIX, "BPFORECST")
                oGridN.doSetFieldValue("Code", pVal.Row, oNumbers.CodeCode, True)
                oGridN.doSetFieldValue("Name", pVal.Row, oNumbers.NameCode, True)

                Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                oGridN.doSetFieldValue("U_CardCd", pVal.Row, sCardCode, True)

                Dim sCardName As String = getDFValue(oForm, "OCRD", "CardName")
                oGridN.doSetFieldValue("U_CardName", pVal.Row, sCardName, True)

                Dim sBPUOM As String = getDFValue(oForm, "OCRD", "U_IDHUOM")
                oGridN.doSetFieldValue("U_UOM", pVal.Row, sBPUOM, True)

                Dim sCardType As String = getDFValue(oForm, "OCRD", "CardType")
                oGridN.doSetFieldValue("U_CardType", pVal.Row, sCardType, True)

                oGridN.doSetFieldValue("U_BPCurr", pVal.Row, "", True)
                oGridN.doSetFieldValue("U_EstQty", pVal.Row, 0, True)
                oGridN.doSetFieldValue("U_ComQty", pVal.Row, 0, True)
                oGridN.doSetFieldValue("U_DoneQty", pVal.Row, 0, True)
                oGridN.doSetFieldValue("U_RemQty", pVal.Row, 0, True)
                oGridN.doSetFieldValue("U_RemCQty", pVal.Row, 0, True)
            End If
        End Sub

        Public Sub doLoadForecastGrid(ByVal oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.doGetDoForecasting() Then
                Dim oForeGrid As DBOGrid = getWFValue(oForm, "FORECGRID")
                Dim oForecast As IDH_BPFORECST = oForeGrid.DBObject

                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    oForeGrid.doEnabled(False)
                Else
                    oForeGrid.doEnabled(True)
                End If
                Dim sCardCode As String = getDFValue(oForm, "OCRD", "CardCode")
                Dim sCardType As String = getDFValue(oForm, "OCRD", "CardType")
                If sCardCode.Length = 0 OrElse sCardCode.StartsWith("*") OrElse sCardCode.EndsWith("*") Then
                    sCardCode = "1QAZ2WSX"
                End If
                oForecast.getByBP(sCardCode)
                oForeGrid.doPostReloadData(True)

                'If sCardType = "C" Then
                '    oForeGrid.doEnableColumn(IDH_BPFORECST._SellingPrice, False)
                '    oForeGrid.doEnableColumn(IDH_BPFORECST._BuyingPrice, True)
                'Else
                '    oForeGrid.doEnableColumn(IDH_BPFORECST._SellingPrice, True)
                '    oForeGrid.doEnableColumn(IDH_BPFORECST._BuyingPrice, False)
                'End If
                Dim sBPCurrency As String = getDFValue(oForm, "OCRD", "Currency") 'getItemValue(oForm, "IDH_BPCURR").ToString.Trim
                doUOMCombo(oForeGrid)
                doDocTypeCombo(oForeGrid)
                doCCICCombo(oForeGrid)
                doTransactionCode(oForeGrid)
                doCurrencyCombo(oForeGrid)
                If Not sBPCurrency.Equals("##") Then
                    oForeGrid.getSBOGrid.Columns.Item(oForeGrid.doIndexFieldWC("U_BPCurr")).Editable = False
                Else
                    oForeGrid.getSBOGrid.Columns.Item(oForeGrid.doIndexFieldWC("U_BPCurr")).Editable = True
                End If
            End If
        End Sub

        Private Sub doTransactionCode(ByVal oGridN As DBOGrid)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_TRNCd"))
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCombo.ValidValues)

            Try
                oCombo = oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_TRNCd"))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                FillCombos.TransactionCodeCombo(oCombo, getDFValue(oGridN.getSBOForm, "OCRD", "CardCode").ToString.Trim, Nothing, "WO")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Transaction Code Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Transaction Code")})
            End Try
        End Sub

        Private Sub doCurrencyCombo(ByVal oGridN As DBOGrid)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BPCurr"))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                doClearValidValues(oCombo.ValidValues)
                FillCombos.CurrencyCombo(oCombo)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            End Try
        End Sub

        Private Sub doDocTypeCombo(ByVal oGridN As DBOGrid)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_DocType"))
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            Try
                doFillCombo(oGridN.getSBOForm(), oCombo, "[@IDH_WRSTATUS]", "Code", "Name", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Type Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Type")})
            End Try
        End Sub

        Private Sub doUOMCombo(ByVal oGridN As DBOGrid)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_UOM"))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doFillCombo(oGridN.getSBOForm(), oCombo, "OWGT", "UnitDisply", "UnitName", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            End Try
        End Sub

        Private Sub doSetUSARequirements(ByVal oForm As SAPbouiCOM.Form, ByVal isVisible As Boolean)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDHFSCHK")
            oItem.Visible = isVisible

            oItem = oForm.Items.Item("IDHCTCHK")
            oItem.Visible = isVisible

            oItem = oForm.Items.Item("IDHPTCHK")
            oItem.Visible = isVisible
        End Sub

        Private Sub doCCICCombo(ByVal oGridN As DBOGrid)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_CCIC"))
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCombo.ValidValues)

            Try
                Dim sCCICValue As String = Config.INSTANCE.getParameterWithDefault("CCICSTR", "")
                Dim oList() As String = sCCICValue.Split(",")
                If oList.Length > 0 Then
                    oCombo.ValidValues.Add("", "")
                    Dim iCount As Integer
                    Dim sCode As String
                    For iCount = 0 To oList.Length() - 1
                        sCode = oList(iCount).Trim()
                        oCombo.ValidValues.Add(sCode, sCode)
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Type Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Type")})
            End Try
        End Sub

        Private Sub doTranslateAddress(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oAddressMatrix As SAPbouiCOM.Matrix = oForm.Items.Item("178").Specific
                Dim i As Int32 = 0
                Dim oCol As SAPbouiCOM.Column
                'oAddressMatrix.GetCellSpecific(i, 0)
                For i = 0 To oAddressMatrix.Columns.Count - 1
                    oCol = oAddressMatrix.Columns.Item(i)
                    'If resxSet.GetString("CRD1." & oCol.UniqueID) IsNot Nothing Then
                    If oCol.UniqueID.StartsWith("U_") Then
                        oCol.TitleObject.Caption = getTranslatedWord(oCol.TitleObject.Caption)
                    End If
                    'End If
                Next
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error translating address.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXTA", {Nothing})
            End Try
        End Sub

        Private Sub GetSelectedAddressAndType(ByVal oForm As SAPbouiCOM.Form, ByRef sAddress As String, ByRef sAddrType As String, ByRef aExtraFields As ArrayList)
            oForm.Freeze(True)
            ' With oForm.DataSources.DBDataSources.Item("CRD1")

            sAddress = "" ' getDFValue(oForm, "CRD1", "Address")
            sAddrType = "" ' getDFValue(oForm, "CRD1", "AdresType")

            Dim oMatrix As SAPbouiCOM.Matrix
            Dim oCols As SAPbouiCOM.Column
            Dim oEdit As SAPbouiCOM.EditText

            oMatrix = oForm.Items.Item("178").Specific
            oCols = oMatrix.Columns.Item(1)
            Dim oCell As SAPbouiCOM.Cell
            oCell = oCols.Cells.Item(1)
            Dim oObj As Object = oCell.Specific
            oEdit = oCell.Specific
            sAddress = oEdit.Value

            oCols = oMatrix.Columns.Item(14)
            oCell = oCols.Cells.Item(1)
            oObj = oCell.Specific
            oEdit = oCell.Specific
            sAddrType = oEdit.Value
            If aExtraFields IsNot Nothing AndAlso aExtraFields.Count > 0 Then
                For i As Int16 = 0 To aExtraFields.Count - 1
                    oCols = oMatrix.Columns.Item(aExtraFields(i).ToString())
                    oCell = oCols.Cells.Item(1)
                    oObj = oCell.Specific
                    oEdit = oCell.Specific
                    aExtraFields(i) = oEdit.Value
                Next

            End If

            'If sAddress = getTranslatedWord("Define New") Then
            'sAddress = ""
            'End If
            'End With
            oForm.Freeze(False)
        End Sub
        Private Sub docheckAddressAndSetCopyToButton(ByVal oForm As SAPbouiCOM.Form)
            If getParentSharedData(oForm, "DONOTEXECUTE") IsNot Nothing AndAlso getParentSharedDataAsString(oForm, "DONOTEXECUTE") = "True" Then
                Exit Sub
            End If

            'With oForm.DataSources.DBDataSources.Item("CRD1")
            Dim sAddress As String = "" ' getDFValue(oForm, "CRD1", "Address")
            Dim sAddrType As String = "" ' getDFValue(oForm, "CRD1", "AdresType")
            GetSelectedAddressAndType(oForm, sAddress, sAddrType, Nothing)
            oForm.Freeze(True)
            Dim oItem As SAPbouiCOM.Item
            setVisible(oForm, "480002075", False)

            'Dim oMatrix As SAPbouiCOM.Matrix
            'Dim oCols As SAPbouiCOM.Column
            'Dim oEdit As SAPbouiCOM.EditText

            'oMatrix = oForm.Items.Item("178").Specific
            'oCols = oMatrix.Columns.Item(1)
            'Dim oCell As SAPbouiCOM.Cell
            'oCell = oCols.Cells.Item(1)
            'Dim oObj As Object = oCell.Specific
            'oEdit = oCell.Specific
            'sAddress = oEdit.Value

            'oCols = oMatrix.Columns.Item(14)
            'oCell = oCols.Cells.Item(1)
            'oObj = oCell.Specific
            'oEdit = oCell.Specific
            'sAddrType = oEdit.Value

            ' oItem = oForm.Items.Item("IDH_BACOPY")
            '#MA Start  22-06-2017
            oItem = oForm.Items.Item("IDH_WRADD")
            Dim oComboBtn As SAPbouiCOM.ButtonCombo = oItem.Specific
            Dim oCount As Integer = oComboBtn.ValidValues.Count
            For index As Integer = 0 To oCount - 1
                oComboBtn.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
            Next
            oComboBtn.Caption = "Waste & Recycle"
            If Not sAddress = getTranslatedWord("Define New") Then ' Denine New Must be translate into the exact word as per SBO's own translation
                If sAddrType = "S" Then
                    oComboBtn.ValidValues.Add("IDH_BACOPY", Translation.getTranslatedWord("Copy Ship To Addr to Billing"))
                Else
                    oComboBtn.ValidValues.Add("IDH_BACOPY", Translation.getTranslatedWord("Copy Billing Addr to Ship To"))
                End If
                oItem.Visible = True
                'Else
                '   oItem.Visible = False
            End If
            oComboBtn.ValidValues.Add("IDH_CPYADD", Translation.getTranslatedWord("Copy Waste Profile From"))
            oComboBtn.ValidValues.Add("IDH_WRDOMC", Translation.getTranslatedWord("Check Scale Tkts with PO No."))
            oComboBtn.ValidValues.Add("IDH_WROSMC", Translation.getTranslatedWord("Check Work Orders with PO No."))
            oComboBtn.ValidValues.Add("IDH_WSTPRF", Translation.getTranslatedWord("Show Waste Profiles for Site"))
            '#MA END 22-06-2017 
            'End With
            oForm.Freeze(False)
        End Sub

#Region "Post Code Lookup"
        Private Sub doFindAddress(ByVal oForm As SAPbouiCOM.Form)
            If Config.ParamaterWithDefault("PCODEKEY", "").Trim.Equals(String.Empty) Then
                'doResUserError
                com.idh.bridge.DataHandler.INSTANCE.doResConfigError("Address search module is not active. Please contact support.", "ERPCDNAC", {""})
                Return
            End If

            Dim oOOForm As com.uBC.forms.fr3.PostCode = New com.uBC.forms.fr3.PostCode(Nothing, oForm.UniqueID, oForm)
            Dim sAddress As String = (getDFValue(oForm, "CRD1", "Address").ToString() + " " +
                                 getDFValue(oForm, "CRD1", "Street").ToString() + " " +
                                 getDFValue(oForm, "CRD1", "City").ToString() + " " +
                                 getDFValue(oForm, "CRD1", "Country").ToString() + " " +
                                 getDFValue(oForm, "CRD1", "State").ToString() + " " +
                                 getDFValue(oForm, "CRD1", "ZipCode").ToString()).Replace("  ", " ").Trim()

            oOOForm.setUFValue("IDH_ADDRES", sAddress)
            oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandlePostCodeOKReturn)
            ''oOOForm.Handler_DialogButton1Return = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandlePostCodeOKReturn)
            '' oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandlePostCodeCancelReturn)
            oOOForm.doShowModal(oForm)
            setSharedData(oForm, "SETENQIDs", "")
            setSharedData(oForm, "AddressDetail", Nothing)
        End Sub

        Private Function doHandlePostCodeOKReturn(oDialogForm As com.idh.forms.oo.Form) As Boolean
            Try

                Dim oOOForm As com.uBC.forms.fr3.PostCode = CType(oDialogForm, com.uBC.forms.fr3.PostCode)
                setSharedData(oOOForm.SBOParentForm, "SETENQIDs", "")
                setSharedData(oOOForm.SBOParentForm, "AddressDetail", Nothing)
                Dim Company As String = oOOForm.Company
                Dim SubBuilding As String = oOOForm.SubBuilding
                Dim BuildingNumber As String = oOOForm.BuildingNumber
                Dim BuildingName As String = oOOForm.BuildingName
                Dim SecondaryStreet As String = oOOForm.SecondaryStreet
                Dim Street As String = oOOForm.Street
                Dim Block As String = oOOForm.Block
                ''''Neighbourhood
                Dim District As String = oOOForm.District
                Dim City As String = oOOForm.City
                Dim Line1 As String = oOOForm.Line1
                Dim Line2 As String = oOOForm.Line2
                Dim Line3 As String = oOOForm.Line3
                Dim Line4 As String = oOOForm.Line4
                Dim Line5 As String = oOOForm.Line5
                Dim AdminAreaName As String = oOOForm.AdminAreaName

                ''Province
                Dim ProvinceName As String = oOOForm.ProvinceName
                Dim ProvinceCode As String = oOOForm.ProvinceCode
                Dim PostalCode As String = oOOForm.PostalCode
                Dim CountryName As String = oOOForm.CountryName
                Dim CountryIso2 As String = oOOForm.CountryIso2
                '////CountryIso3
                '////CountryIsoNumber
                '////SortingNumber1
                '////SortingNumber2
                '////Barcode
                Dim POBoxNumber As String = oOOForm.POBoxNumber


                BPCompleteAddress.Address = (Company & " " & SubBuilding & " " & BuildingNumber & " " & BuildingName).Replace("  ", " ")
                BPCompleteAddress.Block = Block
                BPCompleteAddress.Building = BuildingName
                BPCompleteAddress.City = City
                BPCompleteAddress.Country = CountryIso2
                BPCompleteAddress.County = AdminAreaName
                BPCompleteAddress.PostCode = PostalCode
                BPCompleteAddress.State = ProvinceCode
                If Street <> "" Then
                    BPCompleteAddress.Street_POBox = Street
                ElseIf POBoxNumber <> "" Then
                    BPCompleteAddress.Street_POBox = POBoxNumber
                End If
                BPCompleteAddress.StreetNo = SecondaryStreet
                If Street <> "" AndAlso POBoxNumber <> "" And SecondaryStreet = "" Then
                    BPCompleteAddress.StreetNo = "POBox: " & POBoxNumber
                End If
                'IDHAddOns.idh.forms.Base.setSharedData(oOOForm.SBOParentForm, "SETENQIDs", "Yes")
                'IDHAddOns.idh.forms.Base.setSharedData(oOOForm.SBOParentForm, "AddressDetail", BPCompleteAddress.INSTANCE)

                setSharedData(oOOForm.SBOParentForm, "SETENQIDs", "Yes")
                setSharedData(oOOForm.SBOParentForm, "AddressDetail", BPCompleteAddress.INSTANCE)

                Dim oVal As String = getSharedDataAsString(oOOForm.SBOParentForm, "SETENQIDs")
                oVal = 1
                'Dim oMatrix2 As SAPbouiCOM.Matrix
                'oMatrix2 = oOOForm.SBOParentForm.Items.Item("178").Specific 'oItem.Specific
                'Dim oColumn2 As SAPbouiCOM.Column
                ''            Dim oDataBind As SAPbouiCOM.DataBind
                'Dim sColID As String
                'For iCell As Integer = 1 To oMatrix2.Columns.Count - 1
                '    oColumn2 = oMatrix2.Columns.Item(iCell)
                '    sColID = oColumn2.UniqueID
                '    Select Case sColID
                '        Case "1" 'Address
                '            dosetValuetoAddressField(oColumn2, (SubBuilding & " " & BuildingNumber & " " & BuildingName).Replace("  ", " "), oMatrix2, sColID)
                '        Case "2" 'Street
                '            dosetValuetoAddressField(oColumn2, SecondaryStreet + Street, oMatrix2, sColID)
                '        Case "3" 'Block
                '            dosetValuetoAddressField(oColumn2, Block, oMatrix2, sColID)
                '        Case "4" 'City
                '            dosetValuetoAddressField(oColumn2, City, oMatrix2, sColID)
                '        Case "5" 'ZipCode
                '            dosetValuetoAddressField(oColumn2, PostalCode, oMatrix2, sColID)
                '        Case "6" 'County
                '            dosetValuetoAddressField(oColumn2, AdminAreaName, oMatrix2, sColID)
                '        Case "8" 'Country"
                '            dosetValuetoAddressField(oColumn2, CountryIso2, oMatrix2, sColID)
                '        Case "7" 'State
                '            dosetValuetoAddressField(oColumn2, ProvinceCode, oMatrix2, sColID)
                '    End Select

                'Next
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", Nothing)
            End Try
            Return True
        End Function

        'public bool doHandlePostCodeCancelReturn(com.idh.forms.oo.Form oDeialogForm) {
        '    com.uBC.forms.fr3.PostCode oOOForm = (com.uBC.forms.fr3.PostCode)oDeialogForm;

        '    return true;
        '}
        ''' <summary>
        ''' Set BP Address from Postcode Lookup; Shared in shared Object AddressDetail; 
        ''' </summary>
        ''' <param name="oForm"></param>
        ''' <remarks></remarks>
        Protected Sub setBPAddressFromPOstCodeLookup(ByRef oForm As SAPbouiCOM.Form)
            Try
                Dim oMatrix As SAPbouiCOM.Matrix
                Dim oCols As SAPbouiCOM.Column
                Dim oEdit As SAPbouiCOM.EditText
                Dim oCombo As SAPbouiCOM.ComboBox
                Dim oCell As SAPbouiCOM.Cell
                BPCompleteAddress.INSTANCE = getSharedData(oForm, "AddressDetail")
                oMatrix = oForm.Items.Item("178").Specific

                oForm.Freeze(True)

                Try
                    oCols = oMatrix.Columns.Item("1")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(BPCompleteAddress.Address, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = BPCompleteAddress.Address
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("2")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(BPCompleteAddress.Street_POBox, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = BPCompleteAddress.Street_POBox
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("3")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(BPCompleteAddress.Block, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = BPCompleteAddress.Block
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("4")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(BPCompleteAddress.City, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = BPCompleteAddress.City
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("5")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(BPCompleteAddress.PostCode, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = BPCompleteAddress.PostCode
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("6")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(BPCompleteAddress.County, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = BPCompleteAddress.County
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("8")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(BPCompleteAddress.Country, SAPbouiCOM.BoSearchKey.psk_ByValue)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = BPCompleteAddress.Country
                        End If
                    End If
                Catch ex As Exception
                End Try

                Try
                    oCols = oMatrix.Columns.Item("7")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(BPCompleteAddress.State, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = BPCompleteAddress.State
                        End If
                    End If
                Catch ex As Exception
                End Try


                'Try
                '    oCols = oMatrix.Columns.Item("9")
                '    oCell = oCols.Cells.Item(1)
                '    If oCols.Editable Then
                '        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                '            oCombo = oCell.Specific
                '            oCombo.Select(getSharedData(oForm, "LICTRADNUM"), SAPbouiCOM.BoSearchKey.psk_ByDescription)
                '        Else
                '            oEdit = oCell.Specific
                '            oEdit.Value = getSharedData(oForm, "LICTRADNUM")
                '        End If
                '    End If
                'Catch ex As Exception
                'End Try


                Try
                    oCols = oMatrix.Columns.Item("2000")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(BPCompleteAddress.Building, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = BPCompleteAddress.Building
                        End If
                    End If
                Catch ex As Exception
                End Try


                Try
                    oCols = oMatrix.Columns.Item("2003")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(BPCompleteAddress.StreetNo, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = BPCompleteAddress.StreetNo
                        End If
                    End If
                Catch ex As Exception
                End Try
                'put this col lin last to set focus back on top 
                Try
                    oCols = oMatrix.Columns.Item("1")
                    oCell = oCols.Cells.Item(1)
                    If oCols.Editable Then
                        If oCols.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            oCombo = oCell.Specific
                            oCombo.Select(BPCompleteAddress.Address, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        Else
                            oEdit = oCell.Specific
                            oEdit.Value = oEdit.Value
                        End If
                    End If
                Catch ex As Exception
                End Try

                oForm.Freeze(False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error copying address")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCA", {""})
            End Try
        End Sub

        Private Sub dosetValuetoAddressField(ByRef oColumn2 As SAPbouiCOM.Column, oValue As String, ByRef oMatrix As SAPbouiCOM.Matrix, sColID As String)
            Try
                ' Dim oValue As Object
                Dim oData As Object
                Dim oCell As SAPbouiCOM.Cell
                'If hColData.ContainsKey(sColID) Then
                'oValue = hColData.Item(sColID)
                oCell = oColumn2.Cells.Item(1)
                ' oCell.Click()
                oData = oCell.Specific
                Try
                    If oColumn2.Editable = True Then
                        If oColumn2.Type = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                            Dim oCombo As SAPbouiCOM.ComboBox = oData
                            Dim oval As String = oCombo.Selected.Value
                            oval = oCombo.Selected.Description
                            oCombo.SelectExclusive(oValue, SAPbouiCOM.BoSearchKey.psk_ByValue)
                            oCombo.Select(oValue, SAPbouiCOM.BoSearchKey.psk_ByValue)
                        Else
                            '  oMatrix.SetCellWithoutValidation(1, sColID, oValue)
                            oData.Value = oValue
                        End If
                    End If
                Catch ex As Exception
                End Try
                'End If
            Catch ex As Exception

            End Try
        End Sub
        Structure BPCompleteAddress
            Public Shared INSTANCE As BPCompleteAddress
            Public Shared Address As String
            Public Shared Street_POBox As String
            Public Shared Block As String
            Public Shared City As String
            Public Shared PostCode As String
            Public Shared County As String
            Public Shared State As String
            Public Shared Country As String
            Public Shared StreetNo As String
            Public Shared Building As String
        End Structure
#End Region

        '##S Start 19-01-2017
#Region "Alternate Supplier Address"
        Private Sub doLoadAlterNateAddress(ByVal oForm As SAPbouiCOM.Form)
            If getDFValue(oForm, "OCRD", "CardCode").ToString() = "" Then
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please select customer code before address search", "WRINNOBP", New String() {"Customer Code"})
                'setFormDFValue(oForm, "U_Address", "")
                Return
            End If
            If getDFValue(oForm, "OCRD", "CardType").Equals("S") Then '#MA issue:325 20170328
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please select customer for address search", "WRINNOBP", New String() {"Customer "})
            Else
                '#MA end 20170328
                setSharedData(oForm, "CardCode", getDFValue(oForm, "OCRD", "CardCode").ToString())
                setSharedData(oForm, "CardName", getDFValue(oForm, "OCRD", "CardName").ToString())
                setSharedData(oForm, "AdrCod", getDFValue(oForm, "CRD1", "U_AddrsCd").ToString())
                Dim zipCd As String = getDFValue(oForm, "CRD1", "ZipCode").ToString()
                setSharedData(oForm, "AddressZipCode", getDFValue(oForm, "CRD1", "ZipCode").ToString())
                setSharedData(oForm, "Address", getDFValue(oForm, "CRD1", "Address").ToString())
                setSharedData(oForm, "AddressType", getDFValue(oForm, "CRD1", "AdresType").ToString())
                'setSharedData(oForm, "call", True)
                goParent.doOpenModalForm("IDH_ALSPAD", oForm)
            End If
        End Sub
        '##S End 19-01-2017

        'public bool doHandlePostCodeCancelReturn(com.idh.forms.oo.Form oDeialogForm) {
        '    com.uBC.forms.fr3.PostCode oOOForm = (com.uBC.forms.fr3.PostCode)oDeialogForm;

        '    return true;
        '}



#End Region
#Region "Address Code Editabel"
        Private Sub doSwitchAddressCode(ByVal oForm As SAPbouiCOM.Form, ByRef sVal As Boolean)
            '#MA2 issue:390 20170620
            If sVal Then
                Dim oMatrix As SAPbouiCOM.Matrix
                Dim oCols As SAPbouiCOM.Column
                oMatrix = oForm.Items.Item("178").Specific
                oCols = oMatrix.Columns.Item("U_AddrsCd")
                If oCols.Editable = False Then
                    oCols.Editable = True
                End If
            ElseIf sVal = False AndAlso Not getWFValue(oForm, "handleduplicateAdrsCD") Then
                Dim oMatrix As SAPbouiCOM.Matrix
                Dim oCols As SAPbouiCOM.Column
                oMatrix = oForm.Items.Item("178").Specific
                oCols = oMatrix.Columns.Item("U_AddrsCd")
                If oCols.Editable = True Then
                    oCols.Editable = False
                End If
            End If

        End Sub
#End Region

    End Class

End Namespace
