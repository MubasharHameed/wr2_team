﻿Imports System.Collections
Imports System.IO
Imports System

Namespace idh.forms.SBO
    Public Class GoodsReceipt
        Inherits IDHAddOns.idh.forms.Base

        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "721", -1, Nothing)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
        End Sub

        Public Overrides Sub doLoadForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)

        End Sub

        Public Overrides Sub doClose()
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sEvtItem As String = pVal.ItemUID
            Return False
        End Function

    End Class
End Namespace
