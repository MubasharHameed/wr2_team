﻿Imports System.Collections
Imports System.IO
Imports com.idh.bridge.lookups

Namespace idh.forms.SBO
    Public Class JournalVoucher
        Inherits IDHAddOns.idh.forms.Base

        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "229", -1, Nothing)
            gsSystemMenuID = "1541"
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
        End Sub

        Public Overrides Sub doLoadForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        	Try
        		Dim obItem As SAPbouiCOM.Item
        		obItem = oForm.Items.Item("2")
        		
        		Dim owItem As SAPbouiCOM.Item
                owItem = oForm.Items.Add("IDH_IMPORT", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            	owItem.Left = obItem.Left + obItem.Width + 30
            	owItem.Top = obItem.Top
            	owItem.Width = obItem.Width
            	owItem.Height = obItem.Height
            	owItem.Visible = True
                owItem.Specific.Caption = getTranslatedWord("Import")
            	owItem.LinkTo = "2"
            Catch ex As Exception

            End Try
        End Sub

        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            Return True
        End Function

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
            	If pVal.BeforeAction = False Then
            		If pVal.ItemUID = "IDH_IMPORT" Then
            			doImportExcelJournalVoucher(oForm)		
            		End If
                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Public Overrides Sub doClose()
        End Sub
        
        Protected Sub doImportExcelJournalVoucher(ByVal oForm As SAPbouiCOM.Form)
            Dim sFileName As String
            Dim oOpenFile As System.Windows.Forms.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()

            Try
                Dim sExt As String = Config.INSTANCE.getImportExtension()
                If Not sExt Is Nothing AndAlso sExt.Length > 0 Then
                    If sExt.IndexOf("|") = -1 Then
                        sExt = "(" & sExt & ")|" & sExt
                    End If
                    oOpenFile.Filter = sExt
                End If
            Catch ex As Exception

            End Try
            oOpenFile.FileName = "OpenExcelFile"
            oOpenFile.InitialDirectory = Config.INSTANCE.getImportFolder()
            If com.idh.win.AppForm.OpenFileDialog(oOpenFile) = System.Windows.Forms.DialogResult.OK Then
                sFileName = oOpenFile.FileName

                Dim oParam As String() = {sFileName}
                Try
                	Dim oImporter As New com.idh.bridge.import.Journal()
                    oImporter.doReadFile(sFileName)
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error importing the data.")
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", {Nothing})
                Finally
                    goParent.doProgress("EXIMP", 100)

                    Dim sCurrDirectory As String = Directory.GetCurrentDirectory()
                    Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR)
                End Try
            End If
        End Sub
        
        
    End Class
End Namespace
