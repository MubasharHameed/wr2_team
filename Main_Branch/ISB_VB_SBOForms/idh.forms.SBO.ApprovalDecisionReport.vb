﻿Imports System.Collections
Imports System.IO
Imports System

'idh.forms.SBO.ApprovalDecisionReport
Namespace idh.forms.SBO
    Public Class ApprovalDecisionReport
        Inherits IDHAddOns.idh.forms.Base

        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "50107", -1, Nothing)
            gsSystemMenuID = "14852"
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
        End Sub

        Public Overrides Sub doLoadForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)

            Dim sApprover As String = getParentSharedData(oForm, "APPROVER")
            If Not sApprover Is Nothing AndAlso sApprover.Length > 0 Then

                Dim oCheckBox As SAPbouiCOM.CheckBox
                Dim oItem As SAPbouiCOM.Item
                Dim oEdit As SAPbouiCOM.EditText

                oItem = oForm.Items.Item("19")
                oCheckBox = oItem.Specific()
                oCheckBox.Checked = True

                oItem = oForm.Items.Item("20")
                oCheckBox = oItem.Specific()
                oCheckBox.Checked = False

                oItem = oForm.Items.Item("21")
                oCheckBox = oItem.Specific()
                oCheckBox.Checked = False

                oItem = oForm.Items.Item("11")
                oEdit = oItem.Specific()
                oEdit.Value = sApprover

                oItem = oForm.Items.Item("10")
                oEdit = oItem.Specific()
                oEdit.Value = sApprover

                oForm.Items.Item("1").Click()
            End If
        End Sub

        Public Overrides Sub doClose()
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sEvtItem As String = pVal.ItemUID
            Return False
        End Function

    End Class
End Namespace
