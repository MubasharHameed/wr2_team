﻿Imports System.Collections
Imports System.IO

Namespace idh.forms.SBO
    Public Class PurchaseOrder
        Inherits IDHAddOns.idh.forms.Base

        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "142", -1, Nothing)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                setWFValue(oForm, "ISDRAFT", False)
                Dim oItem As SAPbouiCOM.ComboBox = oForm.Items.Item("81").Specific
                Dim sStatus As String = oItem.Value
                'Draft Status = 6
                If sStatus.Equals("6") Then
                    setWFValue(oForm, "ISDRAFT", True)
                End If
            Else
                If getWFValue(oForm, "ISDRAFT") Then
                    com.idh.bridge.action.MarketingDocs.doUpdateDrafts()
                End If
            End If
        End Sub

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
