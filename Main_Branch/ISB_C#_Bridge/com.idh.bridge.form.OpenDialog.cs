﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WR1Bridge {
    public partial class frmOpenFileDialog : Form {
        private string fileName;

        public string FileName {
            get {
                return fileName;
            }

            set {
                fileName = value;
            }
        }

        private string fileExt;

        public string FileExt {
            get {
                return fileExt;
            }

            set {
                fileExt = value;
            }
        }

        private string fileDirectory;

        public string FileDirectory {
            get {
                return fileDirectory;
            }

            set {
                fileDirectory = value;
            }
        }

        public frmOpenFileDialog() {
            InitializeComponent();
            System.Windows.Forms.DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) {
                fileName = System.IO.Path.GetFileNameWithoutExtension(openFileDialog1.FileName);
                fileExt = System.IO.Path.GetExtension(openFileDialog1.FileName).Substring(1);
                fileDirectory = System.IO.Path.GetDirectoryName(openFileDialog1.FileName);
            }
            this.Close();
        }
    }
}
