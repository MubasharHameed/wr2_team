﻿/*
 * Created by SharpDevelop.
 * User: Louis
 * Date: 2011/02/19
 * Time: 11:35 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using com.idh.bridge;
using com.idh.bridge.form;
using com.idh.bridge.lookups;
using com.idh.bridge.data;
using com.idh.controls;
using com.idh.dbObjects.User;

namespace com.idh.bridge.grid	
{
	/// <summary>
	/// Description of com_idh_bridge_grid_Additional.
	/// </summary>
	public class Additional
	{
		private com.idh.win.controls.WR1Grid moAddGrid;
		private IDH_DISPROW moDispRow;
	
		public Additional(IDH_DISPROW oDispRow, com.idh.win.controls.WR1Grid oAddGrid)	{
			moAddGrid = oAddGrid;
			moDispRow = oDispRow;
			
			doSetAdditionalGridOptions();
			moAddGrid.doApplyControl();		
		}
		
        private void doSetAdditionalGridOptions(){
            DBOGridControl oGridControl = moAddGrid.GridControl;
        	bool bCanEdit = true;
        	string sAddGrp = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup();
        		
        	oGridControl.DBObject = moDispRow.AdditionalExpenses;
        	oGridControl.DBObject.AutoAddEmptyLine = false;
     	
        	oGridControl.setRequiredFilter(IDH_DOADDEXP._JobNr + " = '-1' AND " + IDH_DOADDEXP._RowNr + " = '-1'");
                    	
	        oGridControl.doAddListField(IDH_DOADDEXP._Code, "Code", false, -1, "IGNORE", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._Name, "Name", false, 0, "IGNORE", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._JobNr, "Order Number", false, 0, "IGNORE", null, -1, SAPbouiCOM.BoLinkedObject.lf_UserDefinedObject);
	        oGridControl.doAddListField(IDH_DOADDEXP._RowNr, "Row Order Number", false, 0, "IGNORE", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._ItemCd, "Item Code", bCanEdit, -1, "SRC:IDHISRC(ADD)[IDH_ITMCOD;IDH_GRPCOD=" + sAddGrp + "][ITEMCODE;U_ItemDsc=ITEMNAME;U_UOM=SUOM]", null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
	        oGridControl.doAddListField(IDH_DOADDEXP._ItemDsc, "Item Description", bCanEdit, -1, "IGNORE", null);

            //OnTime [#Ico000????] USA 
            //START
            oGridControl.doAddListField(IDH_DOADDEXP._EmpId, "Employee ID", bCanEdit, -1, "IGNORE", null, -1, SAPbouiCOM.BoLinkedObject.lf_Employee);
            oGridControl.doAddListField(IDH_DOADDEXP._EmpNm, "Employee Name", false, -1, "IGNORE", null);
            //END 
            //OnTime [#Ico000????] USA

	        //moGrid.doAddListField("U_CustCd", "Customer Code", False, -1, "SRC*IDHCSRCH(CUST)[IDH_BPCOD;IDH_TYPE=%F-C][CARDCODE]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
	        oGridControl.doAddListField(IDH_DOADDEXP._CustCd, "Customer Code", false, -1, "IGNORE", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._CustNm, "Customer Name", false, -1, "IGNORE", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._SuppCd, "Supplier Code", bCanEdit, -1, "SRC:IDHCSRCH(SUP)[IDH_BPCOD;IDH_TYPE=%F-S][CARDCODE;U_SuppNm=CARDNAME]", null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
	        oGridControl.doAddListField(IDH_DOADDEXP._SuppNm, "Supplier Name", bCanEdit, -1, "IGNORE", null);
            oGridControl.doAddListField(IDH_DOADDEXP._UOM, "Unit Of Measure", bCanEdit, -1, "COMBOBOX", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._Quantity, "Order Quantity", bCanEdit, -1, "IGNORE", null);

            //Switch As Per Rows Start
            //MA Start 27-10-2014 Issue#417 Issue#418
            int iWidth_Cost = 0, iWidth_Charge = 0;
            if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) && Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES))
            {
                iWidth_Cost = -1;
            }
            if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES)&& Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES))
            {
                iWidth_Charge = -1;
            }
            bool bPCanEdit_Cost = Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) && Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Cost_PRICES);
            bool bPCanEdit_Charge =Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) && Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Charge_PRICES);

	        oGridControl.doAddListField(IDH_DOADDEXP._ItmCost, "Cost/Rebate",bPCanEdit_Cost, iWidth_Cost, "IGNORE", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._ItmChrg, "Charge", bPCanEdit_Charge, iWidth_Charge, "IGNORE", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._CostVatGrp, "Purchase Vat Group", false, iWidth_Cost, "COMBOBOX", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._ItmCostVat, "Purchase Vat", false, iWidth_Cost, "IGNORE", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._ChrgVatGrp, "Selling Vat Group", false, iWidth_Charge, "COMBOBOX", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._ItmChrgVat, "Selling Vat", false, iWidth_Charge, "IGNORE", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._ItmTCost, "Line Cost/Rebate", false, iWidth_Cost, "IGNORE", null);
	        oGridControl.doAddListField(IDH_DOADDEXP._ItmTChrg, "Line Charge", false, iWidth_Charge, "IGNORE", null);
            //MA End 27-10-2014 Issue#417 Issue#418
	        oGridControl.doAddListField(IDH_DOADDEXP._SONr, "Sales Order", false, -1, "IGNORE", null, -1, SAPbouiCOM.BoLinkedObject.lf_Order);
	        oGridControl.doAddListField(IDH_DOADDEXP._PONr, "Purchase Order", false, -1, "IGNORE", null, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder);
	        oGridControl.doAddListField(IDH_DOADDEXP._SINVNr, "AR Invoice", false, -1, "IGNORE", null, -1, SAPbouiCOM.BoLinkedObject.lf_Invoice);
	        oGridControl.doAddListField(IDH_DOADDEXP._PINVNr, "AP Invoice", false, -1, "IGNORE", null, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseInvoice);
            oGridControl.doAddListField(IDH_DOADDEXP._FPCost, "Freeze Cost Price", bCanEdit, -1, "CHECKBOX", null, -1);
            oGridControl.doAddListField(IDH_DOADDEXP._FPChrg, "Freeze Charge Price", bCanEdit, -1, "CHECKBOX", null, -1);        	
        }		
	}
}
