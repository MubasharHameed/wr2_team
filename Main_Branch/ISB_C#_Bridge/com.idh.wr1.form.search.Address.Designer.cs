﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/17
 * Time: 02:03 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace com.idh.wr1.form.search
{
	partial class Address
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.oSearchGrid = new  idh.win.controls.WR1Grid();
			this.btn1 = new System.Windows.Forms.Button();
			this.btn2 = new System.Windows.Forms.Button();
			this.IDH_ADRTYP = new System.Windows.Forms.ComboBox();
			this.IDH_ADDRES = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.IDH_CUSCOD = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.IDH_ZIP = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// oSearchGrid
			// 
			this.oSearchGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.oSearchGrid.Location = new System.Drawing.Point(6, 32);
			this.oSearchGrid.Name = "oSearchGrid";
			this.oSearchGrid.Size = new System.Drawing.Size(833, 440);
			this.oSearchGrid.TabIndex = 0;
			// 
			// btn1
			// 
			this.btn1.Location = new System.Drawing.Point(12, 478);
			this.btn1.Name = "btn1";
			this.btn1.Size = new System.Drawing.Size(75, 23);
			this.btn1.TabIndex = 1;
			this.btn1.Text = "Ok";
			this.btn1.UseVisualStyleBackColor = true;
			// 
			// btn2
			// 
			this.btn2.Location = new System.Drawing.Point(93, 478);
			this.btn2.Name = "btn2";
			this.btn2.Size = new System.Drawing.Size(75, 23);
			this.btn2.TabIndex = 6;
			this.btn2.Text = "Cancel";
			this.btn2.UseVisualStyleBackColor = true;
			// 
			// IDH_ADRTYP
			// 
			this.IDH_ADRTYP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.IDH_ADRTYP.FormattingEnabled = true;
			this.IDH_ADRTYP.Location = new System.Drawing.Point(403, 4);
			this.IDH_ADRTYP.Name = "IDH_ADRTYP";
			this.IDH_ADRTYP.Size = new System.Drawing.Size(99, 21);
			this.IDH_ADRTYP.TabIndex = 4;
			this.IDH_ADRTYP.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
			// 
			// IDH_ADDRES
			// 
			this.IDH_ADDRES.Location = new System.Drawing.Point(215, 4);
			this.IDH_ADDRES.Name = "IDH_ADDRES";
			this.IDH_ADDRES.Size = new System.Drawing.Size(144, 20);
			this.IDH_ADDRES.TabIndex = 3;
			this.IDH_ADDRES.Validated += new System.EventHandler(this.IDH_TextChanged);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(516, 7);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(66, 23);
			this.label4.TabIndex = 23;
			this.label4.Text = "Postal Code";
			// 
			// IDH_CUSCOD
			// 
			this.IDH_CUSCOD.Location = new System.Drawing.Point(68, 4);
			this.IDH_CUSCOD.Name = "IDH_CUSCOD";
			this.IDH_CUSCOD.Size = new System.Drawing.Size(90, 20);
			this.IDH_CUSCOD.TabIndex = 2;
			this.IDH_CUSCOD.Validated += new System.EventHandler(this.IDH_TextChanged);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(368, 7);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(41, 23);
			this.label3.TabIndex = 24;
			this.label3.Text = "Type";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(164, 7);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(55, 23);
			this.label2.TabIndex = 22;
			this.label2.Text = "Address";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 7);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(58, 23);
			this.label1.TabIndex = 21;
			this.label1.Text = "CardCode";
			// 
			// IDH_ZIP
			// 
			this.IDH_ZIP.Location = new System.Drawing.Point(591, 4);
			this.IDH_ZIP.Name = "IDH_ZIP";
			this.IDH_ZIP.Size = new System.Drawing.Size(90, 20);
			this.IDH_ZIP.TabIndex = 5;
			this.IDH_ZIP.Validated += new System.EventHandler(this.IDH_TextChanged);
			// 
			// Address
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(844, 504);
			this.Controls.Add(this.IDH_ADRTYP);
			this.Controls.Add(this.IDH_ADDRES);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.IDH_ZIP);
			this.Controls.Add(this.IDH_CUSCOD);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btn2);
			this.Controls.Add(this.btn1);
			this.Controls.Add(this.oSearchGrid);
			this.KeyPreview = true;
			this.Name = "Address";
			this.Text = "Address Search";
			((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private  idh.win.controls.WR1Grid oSearchGrid;
		private System.Windows.Forms.TextBox IDH_ZIP;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox IDH_CUSCOD;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox IDH_ADDRES;
		private System.Windows.Forms.ComboBox IDH_ADRTYP;
		private System.Windows.Forms.Button btn2;
		private System.Windows.Forms.Button btn1;

        protected void IDH_SelectedValueChanged(object sender, System.EventArgs e)
        {
            SelectedValueChanged_Internal( sender, e);
        }

        protected void IDH_TextChanged(object sender, System.EventArgs e)
        {
            TextChanged_Internal(sender, e);
        }
	}
}
