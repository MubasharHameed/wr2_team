﻿namespace com.idh.wr1 {
    partial class PriceUpdateOption {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.IDH_APPTS = new System.Windows.Forms.CheckBox();
            this.IDH_10 = new System.Windows.Forms.CheckBox();
            this.IDH_15 = new System.Windows.Forms.CheckBox();
            this.IDH_20 = new System.Windows.Forms.CheckBox();
            this.IDH_100 = new System.Windows.Forms.CheckBox();
            this.IDH_200 = new System.Windows.Forms.CheckBox();
            this.IDH_300 = new System.Windows.Forms.CheckBox();
            this.btn_OK = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.IDH_FRMDAT = new System.Windows.Forms.TextBox();
            this.IDH_TODAT = new System.Windows.Forms.TextBox();
            this.IDH_500 = new System.Windows.Forms.CheckBox();
            this.IDH_400 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Form Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "To Date";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // IDH_APPTS
            // 
            this.IDH_APPTS.AutoSize = true;
            this.IDH_APPTS.Location = new System.Drawing.Point(12, 50);
            this.IDH_APPTS.Name = "IDH_APPTS";
            this.IDH_APPTS.Size = new System.Drawing.Size(234, 16);
            this.IDH_APPTS.TabIndex = 2;
            this.IDH_APPTS.Text = "to apply price to ALL sites for this Business Partner.";
            this.IDH_APPTS.UseVisualStyleBackColor = true;
            // 
            // IDH_10
            // 
            this.IDH_10.AutoSize = true;
            this.IDH_10.Enabled = false;
            this.IDH_10.Location = new System.Drawing.Point(12, 70);
            this.IDH_10.Name = "IDH_10";
            this.IDH_10.Size = new System.Drawing.Size(78, 16);
            this.IDH_10.TabIndex = 3;
            this.IDH_10.Text = "Tipping UOM";
            this.IDH_10.UseVisualStyleBackColor = true;
            // 
            // IDH_15
            // 
            this.IDH_15.AutoSize = true;
            this.IDH_15.Enabled = false;
            this.IDH_15.Location = new System.Drawing.Point(12, 85);
            this.IDH_15.Name = "IDH_15";
            this.IDH_15.Size = new System.Drawing.Size(88, 16);
            this.IDH_15.TabIndex = 4;
            this.IDH_15.Text = "Purchase UOM";
            this.IDH_15.UseVisualStyleBackColor = true;
            // 
            // IDH_20
            // 
            this.IDH_20.AutoSize = true;
            this.IDH_20.Enabled = false;
            this.IDH_20.Location = new System.Drawing.Point(12, 100);
            this.IDH_20.Name = "IDH_20";
            this.IDH_20.Size = new System.Drawing.Size(86, 16);
            this.IDH_20.TabIndex = 5;
            this.IDH_20.Text = "Producer UOM";
            this.IDH_20.UseVisualStyleBackColor = true;
            // 
            // IDH_100
            // 
            this.IDH_100.AutoSize = true;
            this.IDH_100.Enabled = false;
            this.IDH_100.Location = new System.Drawing.Point(12, 115);
            this.IDH_100.Name = "IDH_100";
            this.IDH_100.Size = new System.Drawing.Size(85, 16);
            this.IDH_100.TabIndex = 6;
            this.IDH_100.Text = "Tipping Charge";
            this.IDH_100.UseVisualStyleBackColor = true;
            // 
            // IDH_200
            // 
            this.IDH_200.AutoSize = true;
            this.IDH_200.Enabled = false;
            this.IDH_200.Location = new System.Drawing.Point(12, 130);
            this.IDH_200.Name = "IDH_200";
            this.IDH_200.Size = new System.Drawing.Size(90, 16);
            this.IDH_200.TabIndex = 7;
            this.IDH_200.Text = "Haulage Charge";
            this.IDH_200.UseVisualStyleBackColor = true;
            // 
            // IDH_300
            // 
            this.IDH_300.AutoSize = true;
            this.IDH_300.Enabled = false;
            this.IDH_300.Location = new System.Drawing.Point(12, 145);
            this.IDH_300.Name = "IDH_300";
            this.IDH_300.Size = new System.Drawing.Size(82, 16);
            this.IDH_300.TabIndex = 8;
            this.IDH_300.Text = "Disposal Cost";
            this.IDH_300.UseVisualStyleBackColor = true;
            // 
            // btn_OK
            // 
            this.btn_OK.Location = new System.Drawing.Point(12, 202);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(75, 23);
            this.btn_OK.TabIndex = 9;
            this.btn_OK.Text = "OK";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(94, 201);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_Cancel.TabIndex = 10;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // IDH_FRMDAT
            // 
            this.IDH_FRMDAT.Location = new System.Drawing.Point(107, 5);
            this.IDH_FRMDAT.Name = "IDH_FRMDAT";
            this.IDH_FRMDAT.Size = new System.Drawing.Size(100, 18);
            this.IDH_FRMDAT.TabIndex = 11;
            // 
            // IDH_TODAT
            // 
            this.IDH_TODAT.Location = new System.Drawing.Point(107, 25);
            this.IDH_TODAT.Name = "IDH_TODAT";
            this.IDH_TODAT.Size = new System.Drawing.Size(100, 18);
            this.IDH_TODAT.TabIndex = 12;
            // 
            // IDH_500
            // 
            this.IDH_500.AutoSize = true;
            this.IDH_500.Enabled = false;
            this.IDH_500.Location = new System.Drawing.Point(12, 175);
            this.IDH_500.Name = "IDH_500";
            this.IDH_500.Size = new System.Drawing.Size(83, 16);
            this.IDH_500.TabIndex = 14;
            this.IDH_500.Text = "Producer Cost";
            this.IDH_500.UseVisualStyleBackColor = true;
            // 
            // IDH_400
            // 
            this.IDH_400.AutoSize = true;
            this.IDH_400.Enabled = false;
            this.IDH_400.Location = new System.Drawing.Point(12, 160);
            this.IDH_400.Name = "IDH_400";
            this.IDH_400.Size = new System.Drawing.Size(80, 16);
            this.IDH_400.TabIndex = 13;
            this.IDH_400.Text = "Haulage Cost";
            this.IDH_400.UseVisualStyleBackColor = true;
            // 
            // PriceUpdateOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 232);
            this.Controls.Add(this.IDH_500);
            this.Controls.Add(this.IDH_400);
            this.Controls.Add(this.IDH_TODAT);
            this.Controls.Add(this.IDH_FRMDAT);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.IDH_300);
            this.Controls.Add(this.IDH_200);
            this.Controls.Add(this.IDH_100);
            this.Controls.Add(this.IDH_20);
            this.Controls.Add(this.IDH_15);
            this.Controls.Add(this.IDH_10);
            this.Controls.Add(this.IDH_APPTS);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "PriceUpdateOption";
            this.Text = "Price Update / Create Option";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox IDH_APPTS;
        private System.Windows.Forms.CheckBox IDH_10;
        private System.Windows.Forms.CheckBox IDH_15;
        private System.Windows.Forms.CheckBox IDH_20;
        private System.Windows.Forms.CheckBox IDH_100;
        private System.Windows.Forms.CheckBox IDH_200;
        private System.Windows.Forms.CheckBox IDH_300;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.TextBox IDH_FRMDAT;
        private System.Windows.Forms.TextBox IDH_TODAT;
        private System.Windows.Forms.CheckBox IDH_500;
        private System.Windows.Forms.CheckBox IDH_400;
    }
}