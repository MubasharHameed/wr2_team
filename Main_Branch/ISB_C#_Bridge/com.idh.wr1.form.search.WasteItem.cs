/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/17
 * Time: 04:12 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
//using System.Windows.Forms;
using com.idh.win.controls;
using com.idh.controls;
using com.idh.win.forms;

namespace com.idh.wr1.form.search
{
	/// <summary>
	/// Description of com_idh_wr1_form_search_WasteItem.
	/// </summary>
	public partial class WasteItem : SearchBase {
		private idh.bridge.search.WasteItem moWasteSearch;
		public idh.bridge.search.WasteItem WasteSearch {
			get { return moWasteSearch; }
		}	
				
		public WasteItem()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			doSetupFormDefaults(btn1, btn2, oSearchGrid);
			oSearchGrid.setWinForm(this);
			
			moWasteSearch = new idh.bridge.search.WasteItem();

            System.Windows.Forms.Form oForm = this;
			moWasteSearch.setWinForm( ref oForm );		
			
			doSetupControl();
		}
		
		public override void doSetupControl(){
			moWasteSearch.doSetGridOptions(this.oSearchGrid.GridControl);
			this.oSearchGrid.doApplyControl();
		}	
	}
}
