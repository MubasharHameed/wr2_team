/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/17
 * Time: 01:50 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using com.idh.controls;
using System.Windows.Forms;
using com.idh.bridge.form;
using com.idh.bridge.data;
using com.idh.win;

namespace com.idh.bridge.search
{
	/// <summary>
	/// Description of com_idh_bridge_search_Address.
	/// </summary>
	public class Address: FormBridge	{

		public override void setWinForm( ref Form oForm ){
			base.setWinForm( ref oForm );
			doFillCombos();
		}

		public override void setSBOForm( ref IDHAddOns.idh.forms.Base oIDHForm, ref SAPbouiCOM.Form oSBOForm ){
			base.setSBOForm( ref oIDHForm, ref oSBOForm );
			doFillCombos();
		}
		
		private void doFillCombos(){
			doFillType();
		}
		
		public void doSetGridOptions( DBOGridControl oGridControl ) {
            //oGridControl.setTableValue("CRD1");
            oGridControl.doAddGridTable(new GridTable("CRD1", null, null, false, true), true);
            
            oGridControl.doAddFilterField("IDH_CUSCOD", "CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30);
            oGridControl.doAddFilterField("IDH_ADDRES", "Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
            oGridControl.doAddFilterField("IDH_ADRTYP", "AdresType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
            oGridControl.doAddFilterField("IDH_ZIP", "ZipCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);

            oGridControl.doAddListField("AdresType", "Type", false, -1, null, "ADRESTYPE");
            oGridControl.doAddListField("CardCode", "Customer", false, -1, null, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            oGridControl.doAddListField("Address", "Address", false, -1, null, "ADDRESS");
            oGridControl.doAddListField("Street", "Street", false, -1, null, "STREET");
            oGridControl.doAddListField("Block", "Block", false, -1, null, "BLOCK");
            oGridControl.doAddListField("ZipCode", "Postal Code", false, -1, null, "ZIPCODE");
            oGridControl.doAddListField("City", "City", false, -1, null, "CITY");
            oGridControl.doAddListField("County", "County", false, -1, null, "COUNTY");
            oGridControl.doAddListField("Country", "Country", false, -1, null, "COUNTRY");
            oGridControl.doAddListField("U_ROUTE", "Route", false, -1, null, "ROUTE");
            oGridControl.doAddListField("U_ISBACCDF", "Site Access Day", false, -1, null, "ISBACCDF");
            oGridControl.doAddListField("U_ISBACCTF", "Access Time From", false, -1, null, "ISBACCTF");
            oGridControl.doAddListField("U_ISBACCTT", "Access Time To", false, -1, null, "ISBACCTT");
            oGridControl.doAddListField("U_ISBACCRA", "Access Restriction", false, -1, null, "ISBACCRA");
            oGridControl.doAddListField("U_ISBACCRV", "Vehicle Access Rest.", false, -1, null, "ISBACCRV");
            oGridControl.doAddListField("U_ISBCUSTRN", "Customer Ref. No.", false, -1, null, "ISBCUSTRN");
            oGridControl.doAddListField("U_TEL1", "Site Tel.", false, -1, null, "TEL1");
            //oGridControl.doAddListField("U_ROUTE", "Route", false, -1, null, "ROUTE");
            oGridControl.doAddListField("U_IDHSEQ", "Route Seq.", false, -1, null, "SEQ");
            oGridControl.doAddListField("U_IDHACN", "Contact Perso", false, -1, null, "CONA");
            oGridControl.doAddListField("U_IDHPCD", "Premisses Code", false, -1, null, "PRECD");
            oGridControl.doAddListField("U_Origin", "Origin", false, -1, null, "ORIGIN");
            oGridControl.doAddListField("U_IDHSLCNO", "Site Licence Number", false, -1, null, "SITELIC");
            oGridControl.doAddListField("U_IDHSteId", "Site Id", false, -1, null, "STEID");
            
            oGridControl.doAddListField("U_IDHFAX", "Fax", false, -1, null, "FAX");
            oGridControl.doAddListField("U_IDHEMAIL", "Email", false, -1, null, "EMAIL");
            oGridControl.doAddListField("U_IDHSTRNO", "Street Number", false, -1, null, "STRNO");			
		}
		
#region fillcombos
		private void doFillType(){
			object oNCombo = null;
			if ( moSBOForm != null ){
	            SAPbouiCOM.Item oItem;
	            SAPbouiCOM.ComboBox oCombo;
	            oItem = moSBOForm.Items.Item("IDH_ADRTYP");
	            oItem.DisplayDesc = true;
	            oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
	
	            oNCombo = oCombo;
	        	com.idh.bridge.utils.Combobox.doClearCombo( ref oNCombo );
			}
			else if ( moWinForm != null ) {
				oNCombo = getWinControl("IDH_ADRTYP");
			}
	            
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"", "Any");
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"S", "Ship To");
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"B", "Billing");
		}
#endregion
	}
}
