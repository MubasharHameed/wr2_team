/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/23
 * Time: 01:02 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
//using System.Drawing;
//using System.Windows.Forms;
using com.idh.win.controls;
using com.idh.controls;
using com.idh.win.forms;

namespace com.idh.wr1.form.search
{
	/// <summary>
	/// Description of com_idh_wr1_form_search_Origin.
	/// </summary>
	public partial class Origin : SearchBase {
        private  idh.bridge.search.Origin moOriginSearch;
		public  idh.bridge.search.Origin OriginSearch {
			get { return moOriginSearch; }
		}	
		
		public Origin()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			doSetupFormDefaults(btn1, btn2, oSearchGrid);
			oSearchGrid.setWinForm(this);
			
			moOriginSearch = new  idh.bridge.search.Origin();

            System.Windows.Forms.Form oForm = this;
			moOriginSearch.setWinForm( ref oForm );	
		}
		
		public override void doSetupControl(){
			moOriginSearch.doSetGridOptions(this.oSearchGrid.GridControl);
			this.oSearchGrid.doApplyControl();
		}
	}
}
