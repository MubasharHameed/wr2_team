/*
 * Created by SharpDevelop.
 * User: Louis
 * Date: 2009/07/20
 * Time: 01:08 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using com.idh.controls;
using System.Windows.Forms;
using com.idh.bridge.form;
using com.idh.bridge.data;
using com.idh.win;

namespace com.idh.bridge.search
{
	/// <summary>
	/// Search control for waste items
	/// </summary>
	public class Item: FormBridge	{

		public override void setWinForm( ref Form oForm ){
			base.setWinForm( ref oForm );
			doFillCombos();
		}

		public override void setSBOForm( ref IDHAddOns.idh.forms.Base oIDHForm, ref SAPbouiCOM.Form oSBOForm ){
			base.setSBOForm( ref oIDHForm, ref oSBOForm );
			doFillCombos();
		}
		
		private void doFillCombos(){
		}
		
		public void doSetGridOptions( DBOGridControl oGridControl ) {
            //oGridControl.setTableValue("OITM i, OITB ig");
            oGridControl.doAddGridTable(new GridTable("OITM", "i", null, false, true));
            oGridControl.doAddGridTable(new GridTable("OITB", "ig", null, false, true));
            
            oGridControl.setRequiredFilter("i.ItmsGrpCod = ig.ItmsGrpCod ");
            oGridControl.setOrderValue("i.ItmsGrpCod, i.ItemCode ");

            oGridControl.doAddFilterField("IDH_ITMCOD", "i.ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);
            oGridControl.doAddFilterField("IDH_ITMNAM", "i.ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);
            oGridControl.doAddFilterField("IDH_GRPCOD", "i.ItmsGrpCod", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 30);
            oGridControl.doAddFilterField("IDH_GRPNAM", "ig.ItmsGrpNam", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);

            oGridControl.doAddListField("i.ItmsGrpCod", "Item Group Code", false, 0, null, "ITMSGRPCOD");
            oGridControl.doAddListField("ig.ItmsGrpNam", "Item Group Name", false, 0, null, "ITMSGRPNAM");
            oGridControl.doAddListField("i.ItemCode", "Item Code", false, 100, null, "ITEMCODE");
            oGridControl.doAddListField("i.ItemName", "Item Name", false, 410, null, "ITEMNAME");
            oGridControl.doAddListField("i.SalUnitMsr", "Sales UOM", false, 20, null, "SUOM");
            oGridControl.doAddListField("i.BuyUnitMsr", "Purchase UOM", false, 20, null, "PUOM");
            oGridControl.doAddListField("i.U_WTDFW", "Default Weight", false, 50, null, "DEFWEI");
            oGridControl.doAddListField("i.U_WTUDW", "Use Default Weight", false, 50, null, "USEDEFWEI");
		}
	}
}
