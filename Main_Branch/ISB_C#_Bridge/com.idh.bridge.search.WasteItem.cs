/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/17
 * Time: 03:58 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using com.idh.controls;
using System.Windows.Forms;
using com.idh.bridge.form;
using com.idh.bridge.data;
using com.idh.win;

namespace com.idh.bridge.search
{
	/// <summary>
	/// Search control for waste items
	/// </summary>
	public class WasteItem: FormBridge	{

		public override void setWinForm( ref Form oForm ){
			base.setWinForm( ref oForm );
			doFillCombos();
		}

		public override void setSBOForm( ref IDHAddOns.idh.forms.Base oIDHForm, ref SAPbouiCOM.Form oSBOForm ){
			base.setSBOForm( ref oIDHForm, ref oSBOForm );
			doFillCombos();
		}
		
		private void doFillCombos(){
			doFillInventory();
			doFillWasteClass();
		}
		
		public void doSetGridOptions( DBOGridControl oGridControl ) {
            //oGridControl.setTableValue("OITM i, OITB ig");
            oGridControl.doAddGridTable(new GridTable("OITM", "i", null, false, true));
            oGridControl.doAddGridTable(new GridTable("OITB", "ig", null, false, true));
            
            oGridControl.setRequiredFilter("i.ItmsGrpCod = ig.ItmsGrpCod ");
            oGridControl.setOrderValue("i.ItmsGrpCod, i.ItemCode ");

            oGridControl.doAddFilterField("IDH_ITMCOD", "i.ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);
            oGridControl.doAddFilterField("IDH_ITMNAM", "i.ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);
            oGridControl.doAddFilterField("IDH_GRPCOD", "i.ItmsGrpCod", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 30);
            oGridControl.doAddFilterField("IDH_GRPNAM", "ig.ItmsGrpNam", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);
            oGridControl.doAddFilterField("IDH_INVENT", "i.InvntItem", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 1);
            oGridControl.doAddFilterField("IDH_PURWB", "i.U_IDHCWBB", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1);

            oGridControl.doAddListField("i.ItmsGrpCod", "Item Group Code", false, 0, null, "ITMSGRPCOD");
            oGridControl.doAddListField("ig.ItmsGrpNam", "Item Group Name", false, 0, null, "ITMSGRPNAM");
            oGridControl.doAddListField("i.ItemCode", "Item Code", false, 100, null, "ITEMCODE");
            oGridControl.doAddListField("i.ItemName", "Item Name", false, 410, null, "ITEMNAME");
            oGridControl.doAddListField("i.SalUnitMsr", "S_UOM", false, 50, null, "UOM");
            oGridControl.doAddListField("i.BuyUnitMsr", "P_UOM", false, 50, null, "PUOM");
            oGridControl.doAddListField("i.InvntItem", "Inventory", false, 20, null, "INVENT");
            //oGridControl.doAddListField("i.frozenFor", "Frozen", false, 10, null, "FROZEN");
             string sFrozenSQL = com.idh.bridge.lookups.Config.INSTANCE.getActiveCheckSQL("i", DateTime.Now).Trim();
            if (sFrozenSQL.ToUpper().StartsWith("AND")==true) {
                sFrozenSQL = sFrozenSQL.Substring(sFrozenSQL.ToUpper().IndexOf("AND") + 3);
            }
            oGridControl.doAddListField("(CASE when " + sFrozenSQL + " Then 'N' else 'Y' end)", "Frozen", false, 10, null, "FROZEN");
            oGridControl.doAddListField("i.U_WTDFW", "Default Weight", false, 50, null, "DEFWEI");
            oGridControl.doAddListField("i.U_WTUDW", "Use Default Weight", false, 50, null, "USEDEFWEI");
            oGridControl.doAddListField("i.U_IDHCWBB", "Weighbridge Purchase", false, 20, null, "CWBB");
            oGridControl.doAddListField("i.U_IDHCREB", "Carrier Rebate", false, 20, null, "CREB");
            oGridControl.doAddListField("i.U_HAZCD", "Haz Classification", false, 20, null, "HAZCD");
		}
#region fillcombos
		private void doFillInventory(){
			object oNCombo = null;
			if ( moSBOForm != null ){
	            SAPbouiCOM.Item oItem;
	            SAPbouiCOM.ComboBox oCombo;
	            oItem = moSBOForm.Items.Item("IDH_INVENT");
	            oItem.DisplayDesc = true;
	            oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
	
	            oNCombo = oCombo;
	        	com.idh.bridge.utils.Combobox.doClearCombo( ref oNCombo );
			}
			else if ( moWinForm != null ) {
				oNCombo = getWinControl("IDH_INVENT");
			}
	            
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"", "Any");
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"Y", "Stock");
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"N", "None Stock");	
		}

        //Populate the Waste Classification Combo
        private void doFillWasteClass() {
			object oForm = ( moWinForm != null ? (object)moWinForm: (object)moSBOForm );
			com.idh.bridge.utils.Combobox.doFillCombo( oForm, "IDH_LEV1", "[@IDH_WASTCL]", "U_LCode", "U_Desc", null, "CAST(U_LCode As Numeric)", true);
        }
#endregion		
		
	}
}
