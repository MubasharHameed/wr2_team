﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/15
 * Time: 10:01 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using com.idh.win.controls;
using com.idh.controls;
using com.idh.win.forms;

namespace com.idh.wr1.form.search
{
	partial class BP {
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.IDH_NAME = new System.Windows.Forms.TextBox();
            this.IDH_BPCOD = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.oSearchGrid = new  idh.win.controls.WR1Grid();
            this.IDH_TYPE = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.IDH_GROUP = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.IDH_BRANCH = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // IDH_NAME
            // 
            this.IDH_NAME.Location = new System.Drawing.Point(198, 4);
            this.IDH_NAME.Name = "IDH_NAME";
            this.IDH_NAME.Size = new System.Drawing.Size(144, 20);
            this.IDH_NAME.TabIndex = 3;
            this.IDH_NAME.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // IDH_BPCOD
            // 
            this.IDH_BPCOD.Location = new System.Drawing.Point(62, 4);
            this.IDH_BPCOD.Name = "IDH_BPCOD";
            this.IDH_BPCOD.Size = new System.Drawing.Size(90, 20);
            this.IDH_BPCOD.TabIndex = 2;
            this.IDH_BPCOD.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(354, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 23);
            this.label3.TabIndex = 16;
            this.label3.Text = "Type";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(162, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 23);
            this.label2.TabIndex = 15;
            this.label2.Text = "Name";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 23);
            this.label1.TabIndex = 14;
            this.label1.Text = "CardCode";
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(94, 478);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(75, 23);
            this.btn2.TabIndex = 21;
            this.btn2.Text = "Cancel";
            this.btn2.UseVisualStyleBackColor = true;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(13, 478);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(75, 23);
            this.btn1.TabIndex = 1;
            this.btn1.Text = "Ok";
            this.btn1.UseVisualStyleBackColor = true;
            // 
            // oSearchGrid
            // 
            this.oSearchGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oSearchGrid.Location = new System.Drawing.Point(6, 32);
            this.oSearchGrid.Name = "oSearchGrid";
            this.oSearchGrid.Size = new System.Drawing.Size(833, 440);
            this.oSearchGrid.TabIndex = 0;
            // 
            // IDH_TYPE
            // 
            this.IDH_TYPE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_TYPE.FormattingEnabled = true;
            this.IDH_TYPE.Location = new System.Drawing.Point(387, 4);
            this.IDH_TYPE.Name = "IDH_TYPE";
            this.IDH_TYPE.Size = new System.Drawing.Size(99, 21);
            this.IDH_TYPE.TabIndex = 4;
            this.IDH_TYPE.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(497, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 23);
            this.label4.TabIndex = 16;
            this.label4.Text = "Group";
            // 
            // IDH_GROUP
            // 
            this.IDH_GROUP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_GROUP.FormattingEnabled = true;
            this.IDH_GROUP.Location = new System.Drawing.Point(536, 4);
            this.IDH_GROUP.Name = "IDH_GROUP";
            this.IDH_GROUP.Size = new System.Drawing.Size(129, 21);
            this.IDH_GROUP.TabIndex = 5;
            this.IDH_GROUP.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(677, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 23);
            this.label5.TabIndex = 16;
            this.label5.Text = "Branch";
            // 
            // IDH_BRANCH
            // 
            this.IDH_BRANCH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_BRANCH.FormattingEnabled = true;
            this.IDH_BRANCH.Location = new System.Drawing.Point(718, 4);
            this.IDH_BRANCH.Name = "IDH_BRANCH";
            this.IDH_BRANCH.Size = new System.Drawing.Size(120, 21);
            this.IDH_BRANCH.TabIndex = 6;
            this.IDH_BRANCH.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
            // 
            // BP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 504);
            this.Controls.Add(this.IDH_BRANCH);
            this.Controls.Add(this.IDH_GROUP);
            this.Controls.Add(this.IDH_TYPE);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.IDH_NAME);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.IDH_BPCOD);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.oSearchGrid);
            this.KeyPreview = true;
            this.Name = "BP";
            this.Text = "BP Search";
            ((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private System.Windows.Forms.ComboBox IDH_BRANCH;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox IDH_GROUP;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox IDH_TYPE;
		private  idh.win.controls.WR1Grid oSearchGrid;
		private System.Windows.Forms.Button btn1;
		private System.Windows.Forms.Button btn2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox IDH_BPCOD;
		private System.Windows.Forms.TextBox IDH_NAME;

        protected void IDH_SelectedValueChanged(object sender, System.EventArgs e)
        {
            SelectedValueChanged_Internal(sender, e);
        }

        protected void IDH_TextChanged(object sender, System.EventArgs e)
        {
            TextChanged_Internal(sender, e);
        }
	}
}
