/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/06/22
 * Time: 08:20 PM
 *  
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
//using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Timers;

using com.idh.dbObjects.User;
using com.bridge;
using com.idh.bridge;
using com.idh.bridge.lookups;
using IDHAddOns.idh.controls; 
using com.idh.bridge.utils;
using com.idh.win.forms;
using com.idh.win;
using com.idh.utils;
using com.idh.bridge.resources;
using com.idh.bridge.reports;

using com.idh.win.controls.SBO;
using com.uBC.utils;
//using com.idh.bridge.utils;

using com.idh.dbObjects.strct;
//using com.idh.utils;

using com.idh.controls;
using com.idh.form.SBO;

namespace com.idh.bridge.form
{
	/// <summary>
	/// Description of com_idh_bridge_DisposalOrder.
	/// </summary>
	public class DisposalOrder: FormBridge {
		public static string BP_CUSTOMER = "CU";
		public static string BP_CARRIER = "WC";
		public static string BP_PRODUCER = "PR";
        public static string BP_BUYFROM = "BF";
		public static string BP_DISPOSALSITE = "ST";
        public static string BP_DISPOSALSITE2 = "ST2";
		public static string BP_COMPLIANCE = "CO";

        public static string BP_CUSTOMERNAME = "CUN";
        public static string BP_CARRIERNAME = "WCN";
        public static string BP_PRODUCERNAME = "PRN";
        public static string BP_BUYFROMNAME = "BFN";
        public static string BP_DISPOSALSITENAME = "STN";
        public static string BP_DISPOSALSITENAME2 = "STN2";

        private bool mbISSecondWeigh = false;
        public bool ISSecondWeigh {
            get { return mbISSecondWeigh; }
            set { mbISSecondWeigh = value; }
        }
		private IDH_DISPORD moDispOrdDB;
		public IDH_DISPORD DBObject {
			get { return moDispOrdDB; }
		}

		private DisposalRow moDispRows;
		public DisposalRow DisposalRows {
			get { return moDispRows; }
		}
		
		private string msOrderMode = "DK";
		
        private static string[] moAllWeightItems = {
                                            "IDH_WEI1", "IDH_ACC1", "IDH_BUF1", "IDH_TAR1", 
                                            "IDH_WEI2", "IDH_ACC2", "IDH_BUF2", "IDH_TAR2", 
                                            "IDH_RDWGT", "IDH_USERE", "IDH_USEAU", "IDH_CUSCM", "IDH_WGTDED", "IDH_ADJWGT", "IDH_BPWGT" 
                                        };                       

		private static string[] moAlwaysOn1 =   { "btn1", "btFind", "btAdd", "btPrevious", 
										   "btNext", "btLast", "btFirst", "oPNavigation",
										   "stl01","statusStrip1",
                                           "IDH_DOC", "IDH_CON",
										   "oMainTabs", "btnHelp", "btnReadConfig", "btnSummary"};
										   //"oTPCarrier", "oTPWeight", "oTPCustomer",
										   //"oTPProducer", "oTPSite", "oTPDetails"};
		private static string[] moAccountingChkBox = 
										{ "IDH_FOC", "IDH_AVDOC", "IDH_AVCONV", 
										  "IDH_DOORD", "IDH_DOARI" };
//										{ "IDH_FOC", "IDH_AVDOC", "IDH_AVCONV", 
//										  "IDH_DOORD", "IDH_AINV", "IDH_DOARI", 
//										  "IDH_DOARIP" };
		private static string[] moFindEnableItems = 
										{ "IDH_JOBTTP", "IDH_VEHREG", "IDH_CUST", 
										  "IDH_BOOREF", "IDH_WRORD", "IDH_WRROW", 
										  "IDH_WOCF", "IDH_WOCFL", "IDH_CUSL", 
										  "IDH_OS", "IDH_REGL" };
		private static string[] moUpdateDisabledWC = 
										{ "IDH_BOOREF", "IDH_STATUS", "IDH_ITMGRC", 
                						  "IDH_ITMGRN", "IDH_CUSTOT", "IDH_SUBTOT", "IDH_ADDEX", "IDH_ADDCOS", "IDH_ADDCHR", 
                						  "IDH_TAX", "IDH_TOTAL", "IDH_SER1", 
                						  "IDH_WDT1", "IDH_SER2", "IDH_WEIB", 
                						  "IDH_SERB", "IDH_WDTB", "IDH_WDT2", 
                						  "IDH_RSTAT", "IDH_CASHMT", "IDH_ROW",
                						  "IDH_PSTAT", "IDH_USER", 
                                          //"IDH_PURWGT", "IDH_PURUOM", "IDH_PRCOST", 
                                          "IDH_PURTOT", "IDH_TIPTOT", 
                                          "IDH_ORDTOT", "IDH_PURTOT", "IDH_TAXO", "IDH_TOTCOS", 
                                          "IDH_CONGEN", "IDH_AINV", "IDH_DOARIP",
                                          "IDH_BOKSTA",
                                          "IDH_DCGCUR","IDH_PCOCUR","IDH_DCOCUR","IDH_CCOCUR", "IDH_SAMREF", "IDH_SAMSTA" 
										  //, "IDH_HAZCN"
										  };
		//Disable the BP's as well
		private static string[] moUpdateDisabledWOC = 
										{ "IDH_BOOREF", "IDH_STATUS", "IDH_ITMGRC", 
										  "IDH_ITMGRN", "IDH_CUSTOT", "IDH_SUBTOT", "IDH_ADDEX", "IDH_ADDCOS", "IDH_ADDCHR", 
										  "IDH_TAX", "IDH_TOTAL", "IDH_SER1", 
										  "IDH_WDT1", "IDH_SER2", "IDH_WDT2",
                                          "IDH_WEIB", "IDH_SERB", "IDH_WDTB", 
                                          "IDH_RSTAT", "IDH_CASHMT", "IDH_ROW", 
                                          "IDH_CUST", "IDH_CUSTNM", "IDH_CARRIE", 
                                          "IDH_CARNAM", "IDH_WPRODU", "IDH_WNAM",
                                          "IDH_DISSIT", "IDH_DISNAM", "IDH_TAXO", 
                                          "IDH_PSTAT", "IDH_USER", 
                                          //"IDH_PURWGT", "IDH_PURUOM", "IDH_PRCOST", 
                                          "IDH_PURTOT", "IDH_TIPTOT", 
                                          "IDH_ORDTOT", "IDH_PURTOT", "IDH_TAXO", "IDH_TOTCOS", 
                                          "IDH_CUSL", "IDH_CARL",
                                          "IDH_CONGEN", "IDH_AINV", "IDH_DOARIP",
                                          "IDH_BOKSTA",
                                          "IDH_DCGCUR","IDH_PCOCUR","IDH_DCOCUR","IDH_CCOCUR", "IDH_SAMREF", "IDH_SAMSTA" 
                                          //, "IDH_HAZCN"
										  };
        //With Create Vehicle
        private static string[] moAddDisableWC = 
                          				{ "IDH_BOOREF", "IDH_STATUS", "IDH_ITMGRC", 
										  "IDH_ITMGRN", "IDH_CUSTOT", "IDH_SUBTOT", "IDH_ADDEX", "IDH_ADDCOS", "IDH_ADDCHR", 
										  "IDH_TAX", "IDH_TOTAL", "IDH_SER1", 
										  "IDH_WDT1", "IDH_SER2", "IDH_WDT2", 
                                          "IDH_WEIB", "IDH_SERB", "IDH_WDTB", 
                                          "IDH_RSTAT", "IDH_CASHMT", "IDH_ROW", 
                                          //"IDH_WRORD", "IDH_WRROW", 
                                          "IDH_CHKCA",
                                          "IDH_PSTAT", "IDH_USER", 
                                          //"IDH_PURWGT", "IDH_PURUOM", "IDH_PRCOST", 
                                          "IDH_PURTOT", "IDH_TIPTOT", 
                                          "IDH_ORDTOT", "IDH_PURTOT", "IDH_TAXO", "IDH_TOTCOS", 
                                          "IDH_CONGEN", "IDH_HAZCN",
        								  "IDH_AINV", "IDH_DOARIP",
                                          "IDH_BOKSTA",
                                          "IDH_DCGCUR","IDH_PCOCUR","IDH_DCOCUR","IDH_CCOCUR", "IDH_SAMREF", "IDH_SAMSTA" 
                                        };
        //With Create Without Create Vehicle
		private static string[] moAddDisableWOC = 
										{ "IDH_BOOREF", "IDH_STATUS", "IDH_ITMGRC", 
										  "IDH_ITMGRN", "IDH_CUSTOT", "IDH_SUBTOT", "IDH_ADDEX", "IDH_ADDCOS", "IDH_ADDCHR", 
										  "IDH_TAX", "IDH_TOTAL", "IDH_SER1", 
										  "IDH_WDT1", "IDH_SER2", "IDH_WDT2",
                                          "IDH_WEIB", "IDH_SERB", "IDH_WDTB", 
                                          "IDH_RSTAT", "IDH_CASHMT", "IDH_ROW", 
                                          "IDH_CUSWEI", "IDH_CUST", "IDH_CUSTNM", 
                                          "IDH_CARRIE", "IDH_CARNAM", "IDH_WPRODU", 
                                          "IDH_WNAM", "IDH_DISSIT", "IDH_DISNAM", 
                                          //"IDH_WRORD", "IDH_WRROW", 
                                          "IDH_CHKCA",
                                          "IDH_PSTAT", "IDH_USER", 
                                          //"IDH_PURWGT", "IDH_PURUOM", "IDH_PRCOST", 
                                          "IDH_PURTOT", "IDH_TIPTOT", 
                                          "IDH_ORDTOT", "IDH_PURTOT", "IDH_TAXO", "IDH_TOTCOS", 
                                          "IDH_CUSL", "IDH_CARL",
                                          "IDH_CONGEN", "IDH_HAZCN",
										  "IDH_AINV", "IDH_DOARIP",
                                          "IDH_BOKSTA",
                                          "IDH_DCGCUR","IDH_PCOCUR","IDH_DCOCUR","IDH_CCOCUR", "IDH_SAMREF", "IDH_SAMSTA" 
                                        };
		private static string[] moWeighBridgeEnabled = 
										{ "IDH_REF", "IDH_ACC1", "IDH_ACC2", 
										  "IDH_BUF1", "IDH_BUF2", "IDH_TMR" };
		private static string[] moExcludeForOrdered = { "IDH_AVCONV", "IDH_AVDOC", "IDH_CUSL", "IDH_CARL", "btnSummary" };
		
        private static string[] moCostIncommingItems = { 
                                            "IDH_PROCD", "IDH_PRONM", "IDH_PROCF", 
                                            "IDH_PURWGT", "IDH_PURUOM", "IDH_PRCOST", 
                                            "IDH_ADDEX", "IDH_ADDCOS", "IDH_NOVAT", 
        };

		private static string[] moDisposalSiteItems = {
                                            "IDH_DISPCD", "IDH_DISPNM", "IDH_DISPCF", 
                                            "IDH_SITAL", "IDH_DISPAD", 
                                            "IDH_DISSIT", "IDH_DISNAM", "IDH_SITL", "IDH_SITCON", 
                                            "IDH_SITCRF", "IDH_SITADD", "IDH_SITAL", 
                                            "IDH_SITSTR", "IDH_SITBLO", "IDH_SITCIT", 
                                            "IDH_SITSTA", "IDH_SITPOS", "IDH_SITPHO" 
        };

        private static string[] moCostOutgoingItems = { 
        	                                //"IDH_DISPCD", "IDH_DISPNM", "IDH_DISPCF", 
        	                                "IDH_TIPWEI", "IDH_PUOM", "IDH_TIPCOS", 
        	                                "IDH_ORDQTY", "IDH_ORDCOS", 
        	                                "IDH_ADDEX", "IDH_ADDCOS" 
        };

        private static string[] moSalesItems = { 
                                             "IDH_CUSWEI", "IDH_UOM", "IDH_CUSCHG", "IDH_DISCNT", 
                                             "IDH_TIPWEI", "IDH_PUOM", "IDH_TIPCOS",
                                             "IDH_ORDQTY", "IDH_ORDCOS", 
                                             "IDH_DSCPRC", "IDH_DOORD", "IDH_DOARI", "IDH_DOARIP", "IDH_AINV", "IDH_FOC" 
        };

        private static string[] moMarketingCheckboxes = { 
                                             "IDH_AINV", "IDH_FOC", "IDH_DOORD", "IDH_DOARI", "IDH_DOARIP", "IDH_BOOKIN", "IDH_DOPO" 
        };

        //MA Start 24-10-2014 Issue#417 Issue#418
        private static string[] moPUpdateFlds = { "IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST", "IDH_CUSCHG" };
        private static string[] moPUpdateFlds_Cost = { "IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST" };
        private static string[] moPUpdateFlds_Charge = { "IDH_CUSCHG" };
        private static string[] moPShowFlds = { "IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST", "IDH_CUSCHG", 
                                              "IDH_TIPTOT", "IDH_ORDTOT", "IDH_PURTOT", "IDH_TAXO", 
                                              "IDH_ADDCOS", "IDH_TOTCOS", "IDH_CUSTOT", 
                                              "IDH_DSCPRC", "IDH_SUBTOT", "IDH_TAX", "IDH_ADDCHR", "IDH_VALDED", "IDH_TOTAL", 
                                              "IDH_ADDCO", "IDH_ADDCH" 
		};
		private static string[] moP_Cost_ShowFlds = { "IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST", 
                                              "IDH_TIPTOT", "IDH_ORDTOT", "IDH_PURTOT","IDH_ADDCOS", "IDH_TAXO", 
                                              "IDH_TOTCOS","IDH_ADDCO"
        };

        private static string[] moP_Charge_ShowFlds = { "IDH_CUSCHG", "IDH_CUSTOT", "IDH_DSCPRC","IDH_SUBTOT", 
                                              "IDH_ADDCHR", "IDH_TAX", "IDH_VALDED","IDH_TOTAL", 
                                              "IDH_ADDCH" 
        };
        //MA End 24-10-2014 Issue#417 Issue#418
        
		private bool mbCreateNewVechicle = false;

		private System.Timers.Timer oTimer;
        private bool bBtnSwitch = false;
        //private System.Windows.Forms.Button moTimerBtn = null;
		//public System.Windows.Forms.Button TimerButton {
        private SBOButton moTimerBtn = null;
        public SBOButton TimerButton {
			set { moTimerBtn = value; }
		}
		private System.Drawing.Color mcButtonColor;
		private System.Drawing.Color mcButtonColor1 = System.Drawing.Color.FromArgb(0xdd, 0xff, 0xdd);
		private System.Drawing.Color mcButtonColor2 = System.Drawing.Color.FromArgb(0xdd, 0xdd, 0xff);

        //private com.uBCoded.uBXCRViewer moCRViewer = null;
        //public com.uBCoded.uBXCRViewer CRViewer {
        //    set { moCRViewer = value; }
        //    get { return moCRViewer; }
        //}

        Reporter moSharedReporter = null;
        public Reporter SharedReporter {
            set { moSharedReporter = value; }
            get { return moSharedReporter; }
        }

		private int miTimerInterval = 1000;
		public int TimerInterval {
			set { miTimerInterval = value; }
			get { return miTimerInterval; }
		}
		
		private bool mbTimerStarted;
		public bool TimerStarted {
			get { return mbTimerStarted; }
		}
		
		private bool mbEnableTimer;
		public bool EnableTimer {
			set { mbEnableTimer = value; }
			get { return mbEnableTimer; }
		}
		
		private Config.MarketingMode moMarketingMode;
		public Config.MarketingMode MMode {
			get { return moMarketingMode; }
			set { moMarketingMode = value; }
		}

        private string msWeighBridge = "1";
        public string WeighBridge {
            set { 
                 msWeighBridge = value;
                 if ( msWeighBridge != null && msWeighBridge.Length > 0  ) {
                     setUFValue("IDH_WEIBRG", msWeighBridge);
                 }
            }
            get { return msWeighBridge; }
        }

        public void SetWeighBridgeNoSwitch(string sID) {
            msWeighBridge = sID;
        }

//		public IDH_DISPORD getDBObject(){
//			return moDispOrdDB;
//		}
//
//		public DisposalRow getRows(){
//			return moDispRow;
//		}		
		
		private string msCompanyName;

        protected string msDatabaseServer;
        public string DatabaseServer {
            get { return msDatabaseServer; }
            set { msDatabaseServer = value; }
        }

        protected string msDatabaseUsername;
        public string DatabaseUsername
        {
            get { return msDatabaseUsername; }
            set { msDatabaseUsername = value; }
        }

        protected string msDatabasePassword;
        public string DatabasePassword
        {
            get { return msDatabasePassword; }
            set { msDatabasePassword = value; }
        }

        protected string msDatabaseName;
        public string DatabaseName
        {
            get { return msDatabaseName; }
            set { msDatabaseName = value; }
        }

        //protected string msUserName;
        //public string UserName
        //{
        //    get { return msUserName; }
        //}

        protected string msBranch;
        public string Branch
        {
            get { return msBranch; }
            set { msBranch = value; }
        }

        protected string msPrinterName;
        public string PrinterName
        {
            get { return msPrinterName; }
            set { msPrinterName = value; }
        }

        protected string msLocalReportDirectory;
        public string LocalReportDirectory
        {
            get { return msLocalReportDirectory; }
            set { msLocalReportDirectory = value; }
        }
#region WFReplacements
		bool mbFindBranchDisposalSite = true;
#endregion
		
		public DisposalOrder(string sCompanyName){
			msCompanyName = sCompanyName;
			miTimerInterval = 1000;
			
			moAlwaysOn = moAlwaysOn1;
			
			moDBO = new IDH_DISPORD();
            moDBO.MustLoadChildren = true;
			moDispOrdDB = (IDH_DISPORD)moDBO;
			
			moDispRows = new DisposalRow( this );
            moDispRows.DBObject.MustLoadChildren = true;
		}
		
		public override void doSetInitialFormState(){
			Mode = Mode_FIND;
			MMode = Config.MarketingMode.SO;
		}
				
		public override void doClose(){
			DoStopTimer();
			moDispRows.doClose();
		}
		
		public override void setSBOForm( ref IDHAddOns.idh.forms.Base oIDHForm, 
		                                 ref SAPbouiCOM.Form oSBOForm ) {
			base.setSBOForm( ref oIDHForm, ref oSBOForm );
			moDispRows.setSBOForm( ref oIDHForm, ref oSBOForm );
		}

        public override void setWinForm(ref System.Windows.Forms.Form oWinForm) {
			base.setWinForm( ref oWinForm );
			moDispRows.setWinForm( ref oWinForm );
		}

		public void doInitialize(){
			moDispRows.doGetBridges();
			moDispRows.doGetJobTypes();
			moDispRows.doGetObligated();
			moDispRows.doGetBranches();
			moDispRows.doGetTZones();

            moDispRows.doGetUOM("IDH_PUOM");
            moDispRows.doGetUOM("IDH_PURUOM");
            moDispRows.doGetUOM("IDH_UOM");

			
			if ( MMode == Config.MarketingMode.PO )
				doSetVisible("IDH_FOC", false, 1000 );
			else {
				bool bShowFreeCharge = moLookup.getParameterAsBool("MASFCD", true );
				doSetVisible("IDH_FOC", bShowFreeCharge, 1000 );
			}

            com.uBC.utils.FillCombos.HeaderStatus(getWinControl("IDH_STATUS"));

            if (Config.ParameterAsBool("VMUSENEW", false)) {
                EnableItem("label98", false);
                EnableItem("IDH_TRLTar", false);
            } else {
                EnableItem("label98", true);
                EnableItem("IDH_TRLTar", true);
            }
		}
		
		public override void doLinkItems(){
			//DataHandler.INSTANCE.doError("test1","test1");
			
            doAddFormDF("IDH_BOOREF", IDH_DISPORD._Code);
            doAddFormDF("IDH_STATUS", IDH_DISPORD._Status);
            doAddFormDF("IDH_BOOKDT", IDH_DISPORD._BDate);
            doAddFormDF("IDH_BOOKTM", IDH_DISPORD._BTime);
            doAddFormDF("IDH_IGRP", IDH_DISPORD._ItemGrp);
            doAddFormDF("IDH_CHECK", IDH_DISPORD._ORoad);
            doAddFormDF("IDH_SKPLIC", IDH_DISPORD._SLicNr);
            doAddFormDF("IDH_LICEXP", IDH_DISPORD._SLicExp);
            doAddFormDF("IDH_LICCHR", IDH_DISPORD._SLicCh);
            doAddFormDF("IDH_SPECIN", IDH_DISPORD._SpInst);
            doAddFormDF("IDH_LICSUP", IDH_DISPORD._SLicSp);
            doAddFormDF("IDH_SLICNM", IDH_DISPORD._SLicNm);
            doAddFormDF("IDH_CONTRN", IDH_DISPORD._CntrNo);

            //doAddFormDF("IDH_TRWTE1", IDH_DISPROW._TaCntrNo);
            //doAddFormDF("IDH_TRWTE2", IDH_DISPORD._CntrNo);

            //CUSTOMER
            doAddFormDF("IDH_CUST", IDH_DISPORD._CardCd);
            doAddFormDF("IDH_CUSTNM", IDH_DISPORD._CardNM);
            doAddFormDF("IDH_CUSCON", IDH_DISPORD._Contact);
            doAddFormDF("IDH_CUSADD", IDH_DISPORD._Address);
            doAddFormDF("IDH_CUSSTR", IDH_DISPORD._Street);
            doAddFormDF("IDH_CUSBLO", IDH_DISPORD._Block);
            doAddFormDF("IDH_CUSCIT", IDH_DISPORD._City);
            doAddFormDF("IDH_COUNTY", IDH_DISPORD._County);
            doAddFormDF("IDH_CUSPOS", IDH_DISPORD._ZpCd);
            doAddFormDF("IDH_CUSPHO", IDH_DISPORD._Phone1);
            doAddFormDF("IDH_SITETL", IDH_DISPORD._SiteTl);
            doAddFormDF("IDH_SteId", IDH_DISPORD._SteId);
            doAddFormDF("IDH_CUSCRF", IDH_DISPORD._CustRef);
            doAddFormDF("IDH_ROUTE", IDH_DISPORD._Route);
            doAddFormDF("IDH_SEQ", IDH_DISPORD._Seq);
            doAddFormDF("IDH_CUSLIC", IDH_DISPORD._WasLic);

            //doAddFormDF("uBC_TRNCd", IDH_DISPORD._TRNCd);

            doAddFormDF("IDH_PCD", IDH_DISPORD._PremCd);
            doAddFormDF("IDH_SLCNO", IDH_DISPORD._SiteLic);

            //PRODUCER
            doAddFormDF("IDH_WPRODU", IDH_DISPORD._PCardCd);
            doAddFormDF("IDH_WNAM", IDH_DISPORD._PCardNM);
            doAddFormDF("IDH_PRDCON", IDH_DISPORD._PContact);
            doAddFormDF("IDH_PRDADD", IDH_DISPORD._PAddress);
            doAddFormDF("IDH_PRDSTR", IDH_DISPORD._PStreet);
            doAddFormDF("IDH_PRDBLO", IDH_DISPORD._PBlock);
            doAddFormDF("IDH_PRDCIT", IDH_DISPORD._PCity);
            doAddFormDF("IDH_PRDPOS", IDH_DISPORD._PZpCd);
            doAddFormDF("IDH_PRDPHO", IDH_DISPORD._PPhone1);
            doAddFormDF("IDH_ORIGIN", IDH_DISPORD._Origin);
            doAddFormDF("IDH_ORIGIN2", IDH_DISPORD._Origin);
            doAddFormDF("IDH_PRDCRF", IDH_DISPORD._ProRef);

            doAddFormDF("IDH_PROCD", IDH_DISPORD._PCardCd);
            doAddFormDF("IDH_PRONM", IDH_DISPORD._PCardNM);

            //SITE
            doAddFormDF("IDH_DISSIT", IDH_DISPORD._SCardCd);
            doAddFormDF("IDH_DISPCD", IDH_DISPORD._SCardCd);
            doAddFormDF("IDH_DISNAM", IDH_DISPORD._SCardNM);
            doAddFormDF("IDH_DISPNM", IDH_DISPORD._SCardNM);
            doAddFormDF("IDH_SITCON", IDH_DISPORD._SContact);
            doAddFormDF("IDH_SITADD", IDH_DISPORD._SAddress);
            doAddFormDF("IDH_SITSTR", IDH_DISPORD._SStreet);
            doAddFormDF("IDH_SITBLO", IDH_DISPORD._SBlock);
            doAddFormDF("IDH_SITCIT", IDH_DISPORD._SCity);
            doAddFormDF("IDH_SITPOS", IDH_DISPORD._SZpCd);
            doAddFormDF("IDH_SITPHO", IDH_DISPORD._SPhone1);
            doAddFormDF("IDH_SITCRF", IDH_DISPORD._SiteRef);

            //CARRIER
            doAddFormDF("IDH_CARRIE", IDH_DISPORD._CCardCd);
            doAddFormDF("IDH_CARNAM", IDH_DISPORD._CCardNM);
            doAddFormDF("IDH_CONTNM", IDH_DISPORD._CContact);
            doAddFormDF("IDH_ADDRES", IDH_DISPORD._CAddress);
            doAddFormDF("IDH_STREET", IDH_DISPORD._CStreet);
            doAddFormDF("IDH_BLOCK", IDH_DISPORD._CBlock);
            doAddFormDF("IDH_CITY", IDH_DISPORD._CCity);
            doAddFormDF("IDH_ZIP", IDH_DISPORD._CZpCd);
            doAddFormDF("IDH_PHONE", IDH_DISPORD._CPhone1);
            doAddFormDF("IDH_CARREF", IDH_DISPORD._SupRef);

            ////Customer
            //setUFFormat("IDH_DCGCUR", new DBFieldInfo(null, null, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 0, 0));
            ////Producer
            //setUFFormat("IDH_PCOCUR", new DBFieldInfo(null, null, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 0, 0));
            ////Disposal
            //setUFFormat("IDH_DCOCUR", new DBFieldInfo(null, null, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 0, 0));
            ////Carrier
            //setUFFormat("IDH_CCOCUR", new DBFieldInfo(null, null, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 0, 0));

            moDispRows.doLinkItems();
		}

        public void setCustomerCurrency() {
            //setUFValue("IDH_DCGCUR", Config.INSTANCE.getBPCurrency(DBObject.U_CardCd));
            DisposalRows.DBObject.U_DispCgCc = Config.INSTANCE.getBPCurrency(DBObject.U_CardCd);
        }
        public void setProducerCurrency() {
            //setUFValue("IDH_PCOCUR", Config.INSTANCE.getBPCurrency(moDispRows.DBObject.U_ProCd));
            DisposalRows.DBObject.U_PurcCoCc = Config.INSTANCE.getBPCurrency(moDispRows.DBObject.U_ProCd);
        }
        public void setDisposalSiteCurrency() {
            setUFValue("IDH_DCOCUR", Config.INSTANCE.getBPCurrency(DBObject.U_SCardCd));
            DisposalRows.DBObject.U_DispCoCc = Config.INSTANCE.getBPCurrency(DBObject.U_SCardCd);
        }
        public void setCarrierCurrency() {
            setUFValue("IDH_CCOCUR", Config.INSTANCE.getBPCurrency(DBObject.U_CCardCd));
            DisposalRows.DBObject.U_CarrCoCc = Config.INSTANCE.getBPCurrency(DBObject.U_CCardCd);
        }
        
        public void setCurrencies() {
            setCustomerCurrency();
            setProducerCurrency();
            setDisposalSiteCurrency();
            setCarrierCurrency();
        }

		/*
		 * Set the labels to bold for all the mandatory fields
		 */
		public void setMandatoryLabels(){
			if ( moWinForm != null ){
				
			}
		}

        /*
         * Check if we need to switch to PO
         */
        public bool doCheckPurchase() {
            string sWasteCode = moDispRows.getFormDFValueAsString(IDH_DISPROW._WasCd);
            string sJobTp = (string)moDispRows.getFormDFValue(IDH_DISPROW._JobTp, true);
            return (moLookup.doCheckCanPurchaseWB(sWasteCode) && moLookup.isIncomingJob(moLookup.doGetDOContainerGroup(), sJobTp));
            //return moLookup.doCheckCanPurchaseWB(sWasteCode);
        }

        public override bool doValidateUserInput(string sFieldId, string sValue) {
            bool bResult = base.doValidateUserInput(sFieldId, sValue) & moDispRows.doValidateUserInput(sFieldId, sValue);
            if (bResult) {
                if (sFieldId == "IDH_CUSADD" && sValue.Length > 0 && DataHandler.INSTANCE.SBOCompany != null) {
                    EnableItem("IDH_CNA", true);
                }
            }
            return bResult;
        }

		/*
		 * Use this for the Windows Form
		 */
		public override void doUpdateFormData(){
			base.doUpdateFormData();
			moDispRows.doUpdateFormData();

//            string sStatus = moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._Status);
//            string sPayMeth = moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._PayMeth);

            string sDO = getFormDFValueAsStringNZ(IDH_DISPORD._Code);
            string sDOR = moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._Code);
//            if (sStatus.Length > 0)
//            {
//                if (com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus))
//                {
//                    if (sPayMeth.Equals(com.idh.bridge.lookups.FixedValues.getPayMethPrePaid()))
//                        moDispRows.doCheckAccountingCheckBox("IDH_AINV", true);
//                    else
//                        moDispRows.doCheckAccountingCheckBox("IDH_DOARI", true);
//                }
//                else if (com.idh.bridge.lookups.FixedValues.isWaitingForPInvoice(sStatus))
//                {
//                    moDispRows.doCheckAccountingCheckBox("IDH_DOARIP", true);
//                }
//                else if (com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus))
//                {
//                    moDispRows.doCheckAccountingCheckBox("IDH_DOORD", true);
//                }
//                else if (com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus))
//                {
//                    moDispRows.doCheckAccountingCheckBox("IDH_FOC", true);
//                }
//                else
//                {
//                    moDispRows.doCheckAccountingCheckBox("IDH", true);
//                    //if ( sDO.Length > 0 && sDOR.Length > 0 ) {
//                    //    moDispRows.doSwitchAccountingCheckBox( "IDH_FOC", true, false );
//                    //}
//                }
//            }
//            else
//            {
//                moDispRows.doCheckAccountingCheckBox("IDH", true);
//            }
            
//            string sPStatus = moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._PStat);
//            if (sPStatus.Length > 0)
//            {
//                if (com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sPStatus))
//                {
//                 	moDispRows.doCheckAccountingCheckBox("IDH_DOPO", true);
//                }
//            }

            string sJobTp = (string)moDispRows.getFormDFValue(IDH_DISPROW._JobTp, true);
            string sWasteCode = moDispRows.getFormDFValueAsString(IDH_DISPROW._WasCd);
            if (sJobTp != null && sJobTp.Length > 0 && sWasteCode != null && sWasteCode.Length > 0)
            {
                ////if (moLookup.doCheckCanPurchaseWB(sWasteCode) && sJobTp.Equals(moLookup.doGetJopTypeIncoming()))
                //if (moLookup.doCheckCanPurchaseWB(sWasteCode) && moLookup.isIncomingJob(moLookup.doGetDOContainerGroup(), sJobTp))
                if (doCheckPurchase()) {
                    doSwitchToPO(false,false, false);
                }
                else {
                    doSwitchToSO(false);
                }
            }
            else {
                doSwitchToSO(false);
            }

            doSwitchGenButton();
		}
		
		/**
		 * Set the PO Checkboxs
		 */
		public void doSetPOCheckBoxes(){
			string sPStatus = moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._PStat);
            if (sPStatus.Length > 0)
            {
                if (com.idh.bridge.lookups.FixedValues.isWaitingForPurchaseOrder(sPStatus))
                {
                 	moDispRows.doCheckAccountingCheckBox("IDH_DOPO", true);
                }
            }
		}
		
		/**
		 * Set the SO Checkboxes
		 */
		public void doSetSOCheckBoxes(){
            string sStatus = moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._Status);
            string sPayMeth = moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._PayMeth);

            if (sStatus.Length > 0)
            {
                if (com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus))
                {
                    if (sPayMeth.Equals(com.idh.bridge.lookups.FixedValues.getPayMethPrePaid()))
                        moDispRows.doCheckAccountingCheckBox("IDH_AINV", true);
                    else
                        moDispRows.doCheckAccountingCheckBox("IDH_DOARI", true);
                }
                else if (com.idh.bridge.lookups.FixedValues.isWaitingForPInvoice(sStatus))
                {
                    moDispRows.doCheckAccountingCheckBox("IDH_DOARIP", true);
                }
                else if (com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus))
                {
                    moDispRows.doCheckAccountingCheckBox("IDH_DOORD", true);
                }
                else if (com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus))
                {
                    moDispRows.doCheckAccountingCheckBox("IDH_FOC", true);
                }
                else
                {
                    moDispRows.doCheckAccountingCheckBox("IDH", true);
                    //if ( sDO.Length > 0 && sDOR.Length > 0 ) {
                    //    moDispRows.doSwitchAccountingCheckBox( "IDH_FOC", true, false );
                    //}
                }
            }
            else
            {
                moDispRows.doCheckAccountingCheckBox("IDH", true);
            }
		}
		
        /*
         * Do Update Forms changed Fields
         */
        public override void doUpdateFormChangedData(){
            base.doUpdateFormChangedData();
            moDispRows.doUpdateFormChangedData();
        }
		
		/**
		 * Set the checkboxes
		 */
		public void doSetReportCheckBoxes() {
            double dW1 = 0;
            double dW2 = 0;
            double dAlternative = 0;
            double dReadWeight = 0;
            double dCustWeight = 0;
            try {
            	dW1 = moDispRows.getFormDFValueAsDouble(IDH_DISPROW._Wei1);
            	dW2 = moDispRows.getFormDFValueAsDouble(IDH_DISPROW._Wei2);
                
            	string sCardCode = getFormDFValueAsString(IDH_DISPORD._CardCd);
	            string sCustUOM = moLookup.getBPUOM(sCardCode);
	            string sDefUOM  = moLookup.getDefaultUOM();
	            if ( !sCustUOM.Equals(sDefUOM) ) 
	            	dAlternative = moDispRows.getFormDFValueAsDouble( IDH_DISPROW._AUOMQt );
                else
                	dAlternative = 0;
                
                dReadWeight = moDispRows.getFormDFValueAsDouble( IDH_DISPROW._RdWgt );
                dCustWeight = moDispRows.getFormDFValueAsDouble( IDH_DISPROW._CstWgt );
            }
            catch ( Exception ) {
            }

            //string sAVDOC = Config.Parameter("MDDAPD");
            //string sAVCONV = Config.Parameter("MDDAPC");
            //if ( ( moDispRows.DBObject.U_Wei1 > 0 && moDispRows.DBObject.U_Wei2 > 0) 
            //    || moDispRows.DBObject.U_AUOMQt != 0 
            //    || moDispRows.DBObject.U_RdWgt != 0 
            //    || moDispRows.DBObject.U_CstWgt != 0 ) {
            //    string sWastCd = moDispRows.DBObject.U_WasCd;
            //    string sJobType = moDispRows.DBObject.U_JobTp;
            //    if ( Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) && 
            //        com.idh.bridge.lookups.Config.INSTANCE.isHazardousItem(sWastCd) ) {
            //        setUFValue(oForm, "IDH_AVDOC", "Y")
            //        setUFValue(oForm, "IDH_AVCONV", "Y")
            //    } else {
            //        If Not (sAVDOC Is Nothing) AndAlso sAVDOC.ToUpper().Equals("TRUE") Then
            //            setUFValue(oForm, "IDH_AVDOC", "Y")
            //        Else
            //            setUFValue(oForm, "IDH_AVDOC", "N")
            //        End If
                    
            //        If Not (sAVCONV Is Nothing) AndAlso sAVCONV.ToUpper().Equals("TRUE") Then
            //            setUFValue(oForm, "IDH_AVCONV", "Y")
            //        Else
            //            setUFValue(oForm, "IDH_AVCONV", "N")
            //        End If
            //    }
            //} else {
            //    setUFValue(oForm, "IDH_DOARIP", "N")
            //    doSetEnabled(oForm.Items.Item("IDH_DOARIP"), False)
            //    setUFValue(oForm, "IDH_AVCONV", "N")
            //    setUFValue(oForm, "IDH_AVDOC", "N")
            //}


            //Note: 20150508 - As per discussion with HN of MA on this
            //the tick/untick of the checkbox is solely based on the conditions below. 
            bool bAVDOC = Config.ParameterAsBool("MDDAPD", false);
            //if (  bAVDOC ) 
            //    setUFValue("IDH_AVDOC", "Y");
            //else
            //    setUFValue("IDH_AVDOC", "N");
           
            bool bAVCONV = Config.ParameterAsBool("MDDAPC", false);
            //if ( bAVCONV ) 
            //    setUFValue("IDH_AVCONV", "Y");
            //else
            //    setUFValue("IDH_AVCONV", "N");
            
            if ( ( dW1 > 0 && dW2 > 0) || dAlternative != 0 || dReadWeight != 0 || dCustWeight != 0) {
                EnableItems( ref moAccountingChkBox, true );
                //EnableItem( "IDH_DOARIP", false );

                string sWastCd = moDispRows.getFormDFValueAsString( IDH_DISPROW._WasCd);
				string sJobType = moDispRows.getFormDFValueAsString( IDH_DISPROW._JobTp);
                //if (sJobType.Equals(Config.INSTANCE.doGetJopTypeOutgoing()) && Config.INSTANCE.isHazardousItem(sWastCd))
                if (Config.INSTANCE.isOutgoingJob(moLookup.doGetDOContainerGroup(), sJobType) && Config.INSTANCE.isHazardousItem(sWastCd)) {
                    setUFValue( "IDH_AVDOC", "Y");
                    setUFValue( "IDH_AVCONV", "Y");
                } else {
                    setUFValue("IDH_AVDOC", (bAVDOC ? "Y" : "N"));
                    setUFValue("IDH_AVCONV", (bAVCONV ? "Y" : "N"));
                }
	        }
            else {
                setUFValue( "IDH_DOARIP", "N");
                setUFValue( "IDH_AVDOC", "N");
                setUFValue( "IDH_AVCONV", "N");
            }
		}
		
		/**
		 * set the Form Defaults and Data Defaults
		 */
		public void doAllDefaults(){
			setUFValue("IDH_DOORD", "N");
			setUFValue("IDH_DOARI", "N");
			setUFValue("IDH_DOARIP", "N");
			setUFValue("IDH_AINV", "N");
			setUFValue("IDH_FOC", "N");
			
			setUFValue("IDH_AVCONV", "N");
			setUFValue("IDH_AVDOC", "N");
			setUFValue("IDH_NOVAT", "N");
			moDispOrdDB.doApplyDefaults();
//20120218			moDispOrdDB.doAllDefaults();

            //string sJob = Config.INSTANCE.doGetJopTypeIncoming();
            //moDispRows.DBObject.setDefaultValue(IDH_DISPROW._JobTp, sJob);        
        }
		
		/**
		 * Set Defaults for add mode
		 */
		public void doAllDefaultsForAdd(){
			doAllDefaults();
			//setFoUFValue("IDH_DOORD", "N");
		}
		
		/**
		 * Form Field changed
		 */
        public override bool doPostUpdateDBFieldFromUser(string sControlName, string sValue) {
            bool bResult = true;

            if (base.doPostUpdateDBFieldFromUser(sControlName, sValue)) {
                //bResult = moDispRows.doPostUpdateDBFieldFromUser(sControlName, sValue);

                //if (sControlName == "IDH_WEIBRG") {
                //    string sID = getUFValue(sControlName);
                //    DisposalRows.doConfigWeighBridge(sID);
                //    SetWeighBridgeNoSwitch(sID);
                //}

            } else {
                bResult = false;
            }
            return bResult;

            //if (base.doPostUpdateDBFieldFromUser(sControlName, sValue)) {
            //    return moDispRows.doPostUpdateDBFieldFromUser(sControlName, sValue);
            //}
            //return true;
    	}

        /**
         * Update the DBField values as well as all other linked Form field Values
         */
        public override bool doUpdateDBFieldFromUser(System.Windows.Forms.Control oControl) {
            bool bResult = true;
            if (base.doUpdateDBFieldFromUser(oControl)) {
                bResult = moDispRows.doUpdateDBFieldFromUser(oControl);
            } else
                bResult = false;

            return bResult;

            //bool bResult = true;
            //if (base.doUpdateDBFieldFromUser(oControl)) {
            //    bResult = moDispRows.doUpdateDBFieldFromUser(oControl);
            //} 

            //if (oControl.Name == "IDH_WEIBRG") {
            //    string sID = getUFValue(oControl.Name);
            //    DisposalRows.doConfigWeighBridge(sID);
            //    SetWeighBridgeNoSwitch(sID);
            //}

            //return bResult;
        }

        ///**
        // * do Validate the Order and Row
        // */
        //public override bool doValidateUserInput(string sFieldId, string sValue) {
        //    if ( !base.doValidateUserInput(sFieldId, sValue) )
        //        return false;
            
        //    return moDispRows.doValidateUserInput(sFieldId, sValue);
        //}

#region FormModes		
        /**
         * Switch the Generate Button
         */
        protected void doSwitchGenButton() {
			string sConNum = moDispRows.getFormDFValueAsString(IDH_DISPROW._ConNum);
			bool bDoEnable = false;
            bool bWasSetByUser = wasFormFieldSetByUser("IDH_HAZCN");
			if ( bWasSetByUser || sConNum == null || sConNum.Length == 0 ) {
				string sWasteCode = moDispRows.getFormDFValueAsString(IDH_DISPROW._WasCd);
				if ( Config.INSTANCE.isHazardousItem(sWasteCode) )
					bDoEnable = true;
			}
			EnableItem("IDH_CONGEN", bDoEnable );
            
            if ( bDoEnable ) {
            	bool bAllowHazChange = moLookup.getParameterAsBool("DOCACCN", false); 
            	EnableItem( "IDH_HAZCN", bAllowHazChange );
            }
            else
            	EnableItem( "IDH_HAZCN", false );
		}

		/**
		 * Reset all controls to their default values
		 */
		protected override void doSetFindMode(){
            DisposalRows.DBObject.doClearBuffers();

			doAllDefaults();
			doUpdateFormData();
            if (msWeighBridge != null && msWeighBridge.Length > 0) {
                setUFValue("IDH_WEIBRG", msWeighBridge);
            }

			if ( Mode == Mode_FIND ) {
				return;
            }
			
			if ( moIDHForm != null ) {
	            moSBOForm.Items.Item("IDH_VEHREG").Click(SAPbouiCOM.BoCellClickType.ct_Regular);
			}

            doSetReportCheckBoxes();
			DisableAllEditItems( ref moFindEnableItems );

            if ( moDispRows.DBObject.AdditionalExpenses != null )
			    moDispRows.DBObject.AdditionalExpenses.doClearBuffers();

            if (moDispRows.DBObject.Deductions != null )
                moDispRows.DBObject.Deductions.doClearBuffers();
			
            msLastMode = Mode_FIND;
		}
		
		/**
		 * Set to Ok Mode
		 */
		protected override void doSetOkMode(){
			if ( doSwitchOrderMode() ) {
				return;
			}
			if ( Mode == Mode_OK ) {
                return;
            }
            doSetToViewOKUpdateMode();

            msLastMode = Mode_OK;
		}		
		
		/**
		 * Set to View Mode
		 */
		protected override void doSetViewMode(){
			if ( doSwitchOrderMode() ) {
				return;
			}
			
			if ( Mode == Mode_VIEW ) {
                return;
            }
            doSetToViewOKUpdateMode();

            msLastMode = Mode_VIEW;
        }
		
		/**
		 * Set to Update Mode
		 */
		protected override void doSetUpdateMode(){
			if ( Mode == Mode_UPDATE ) {
                return;
            }
            else
            if (Mode == Mode_ADD)
            {
                msLastMode = Mode_ADD;
                return;
            }
                        
            doSetToViewOKUpdateMode();

            msLastMode = Mode_UPDATE;
		}
		
		
		protected override void doSetAddMode() {
			moDispOrdDB.doClearBuffers();
				
			doAllDefaultsForAdd();

            doUpdateFormData();
            if (msWeighBridge != null && msWeighBridge.Length > 0) {
                setUFValue("IDH_WEIBRG", msWeighBridge);
            }

            if ( Mode == Mode_ADD ) {
                msLastMode = Mode_ADD;
                return;
            }			
            
            string[] oWrkArray;
            bool bAllowChanging = moLookup.getParameterAsBool("DOACBP", false);
			//bool bAllowHazChange = moLookup.getParameterAsBool("DOCACCN", false); 
			if ( bAllowChanging || mbCreateNewVechicle ) 
                oWrkArray = moAddDisableWC;
            else
                oWrkArray = moAddDisableWOC;

            if (Config.ParameterAsBool("DODCEDI", false)) {
                oWrkArray = com.idh.utils.General.doCombineArrays(oWrkArray, moDisposalSiteItems);
            }

            EnableAllEditItems( ref oWrkArray );
            
            //EnableItem( "IDH_HAZCN", bAllowHazChange );
            doSetReportCheckBoxes();

			if ( moIDHForm != null ){
	       		if ( ((SAPbouiCOM.ComboBox)moSBOForm.Items.Item("IDH_WEIBRG").Specific).ValidValues.Count > 0 )
	                EnableItems( ref moWeighBridgeEnabled, true );

			}
			else if ( moWinForm != null ){
            	//if ( ((ComboBox)moWinForm.Controls.Find("IDH_WEIBRG", true)[0]).Items.Count > 0 )
                if ( this.getComboItems("IDH_WEIBRG").Count > 0 ) 
            		EnableItems( ref moWeighBridgeEnabled, true );
			}
            
            EnableItem( "IDH_TMR", mbEnableTimer );
            doSwitchGenButton();

            msLastMode = Mode_ADD;
		}		
		
		/**
		 * Set to View and Update Mode
		 */
		public void doSetToViewOKUpdateMode(){

            //bool bIsRedo = false;
            string[] oExclude;
			bool bAllowBPChanging = moLookup.getParameterAsBool("DOACBP", false);
			//bool bAllowHazChange = moLookup.getParameterAsBool("DOCACCN", false);            
            if ( bAllowBPChanging || mbCreateNewVechicle ) 
                oExclude = moUpdateDisabledWC;
            else
                oExclude = moUpdateDisabledWOC;
            
            if ( moDispRows.DBObject.U_Status == FixedValues.getReDoOrderStatus() ||
                moDispRows.DBObject.U_Status == FixedValues.getReDoInvoiceStatus() ||
                moDispRows.DBObject.U_Status == FixedValues.getReDoOrderStatus() ||
                moDispRows.DBObject.U_PStat == FixedValues.getReDoInvoiceStatus() ) {
                    oExclude = com.idh.utils.General.doCombineArrays(oExclude, moAllWeightItems);
                //bIsRedo = true;
            }
            
            if ( Config.ParameterAsBool("DODCEDI", false) ) {
                oExclude = com.idh.utils.General.doCombineArrays(oExclude, moDisposalSiteItems);
            }

            EnableAllEditItems( ref oExclude );
            //if ( !bAllowHazChange )
            //	EnableItem( "IDH_HAZCN", bAllowHazChange );
            
            doSetReportCheckBoxes();
            
			if ( moIDHForm != null ){
	       		if ( ((SAPbouiCOM.ComboBox)moSBOForm.Items.Item("IDH_WEIBRG").Specific).ValidValues.Count > 0 )
	                EnableItems( ref moWeighBridgeEnabled, true );

			}
			else if ( moWinForm != null ){
            	//if ( ((ComboBox)moWinForm.Controls.Find("IDH_WEIBRG", true)[0]).Items.Count > 0 )
                if  (this.getComboItems("IDH_WEIBRG").Count > 0)
            		EnableItems( ref moWeighBridgeEnabled, true );
			}
	         	
	        string sOrder = moDispRows.getFormDFValueAsString( IDH_DISPROW._WROrd );
	        //bool bFocusSet = false;
	        if ( sOrder.Length == 0 )
                EnableItem( "IDH_WRORD", true );
            else {
            	//doSetFocus( "IDH_WEI1");
            	//bFocusSet = true;
            	EnableItem( "IDH_WRORD", false );
            }
	                	
	        string sRow = moDispRows.getFormDFValueAsString( IDH_DISPROW._WRRow );
	        if ( sRow.Length == 0  ) 
	        	EnableItem( "IDH_WRROW", true );
	        else {
	           	//if ( bFocusSet ) 
	            //	doSetFocus( "IDH_WEI1");

	            EnableItem( "IDH_WRROW", false );
	        }
	        
	        EnableItem( "IDH_TMR", mbEnableTimer );
            doSwitchGenButton();
		}
#endregion
#region FinancialModes
		/*
		 * Check to see if the code must be switched to Ordered
		 */
		private bool doSwitchOrderMode() {
            if (moDispRows.DBObject.doCheckCanEdit(false)) {
                doSetToViewOKUpdateMode();
                msOrderMode = "DK";
                return false;
            } else {
                doSetOrdered(FixedValues.getStatusOrdered());
                msOrderMode = FixedValues.getStatusOrdered();
                return true;
            }
            ////if (moDispRows.DBObject.doCheckHaveMarketingDocuments(false)) {
            ////    doSetOrdered();
            ////    return true;
            ////} else
            ////    return false;

            ////EnableItem("IDH_Amend", false);
            //string sPONr = moDispRows.getFormDFValueAsString(IDH_DISPROW._ProPO);
            //string sStatus;
            //if ((sPONr != null) && sPONr.Length > 0 && sPONr.Equals("ERROR") == false) {
            //    sStatus = com.idh.bridge.lookups.FixedValues.getStatusOrdered();
            //    doSetOrdered(sStatus);
            //    msOrderMode = sStatus;
            //    return true;
            //}
            //else {
            //    sStatus = moDispRows.getFormDFValueAsString(IDH_DISPROW._Status);
            //    if ( sStatus != null && sStatus.Length > 0 ){
            //        if (com.idh.bridge.lookups.FixedValues.canUpdate(sStatus)) {
            //            doSetToViewOKUpdateMode();
            //        } else {
            //            doSetOrdered(sStatus);

            //            msOrderMode = sStatus;
            //            return true;
            //        }
            //    }
            //}
            //msOrderMode = "DK";
            //return false;
		}
		/*
		 * do Enable or Disable for Ordered
		 */
		private bool doSetOrdered(string sStatus) {
            if (msOrderMode.Equals(FixedValues.getStatusOrdered())
                    || msOrderMode.Equals(FixedValues.getStatusFoc())
                    || msOrderMode.Equals(FixedValues.getStatusInvoice())) {
                return false;
            } else
                msOrderMode = sStatus;

			DisableAllEditItems(ref moExcludeForOrdered);

            if ( ( Config.ParameterAsBool("DOSHWAMD", false) ) )
                EnableItem("IDH_Amend", true);

            EnableItem("btnSummary", true);

			return true;
		}	
		
		//Switch to PO Mode and apply the BP Rules - this will override the Producer info as per BP Rules.
		//public void doSwitchToPO( bool bReplaceProd ){
		//	doSwitchToPO( bReplaceProd, true, true );
		//}
		//public void doSwitchToPO( bool bReplaceProd, bool bDoBPRules ) {
		//	doSwitchToPO( bReplaceProd, bDoBPRules, true );
		//}
		public void doSwitchToPO( bool bReplaceProd, bool bDoBPRules, bool bSetByUser) {

            //if ( !string.IsNullOrWhiteSpace(moDispRows.DBObject.U_TrnCode) ) {
            //    EnableItems(ref moMarketingCheckboxes, false);
            //}

            if (moMarketingMode == Config.MarketingMode.PO) {
                doSwitchTransactionCodeFields();
                return;
            } else
                moMarketingMode = Config.MarketingMode.PO;

            //moDispRows.doCheckAccountingCheckBox("IDH_FOC", true);
            //doSetPOCheckBoxes();
            doSetCheckBoxes();

            EnableItems(ref moDisposalSiteItems, Config.ParameterAsBool("DODCEDI", false));

            EnableItems(ref moCostOutgoingItems, false);
            EnableItems(ref moCostIncommingItems, true);
            EnableItems(ref moSalesItems, false);
            EnableItem("IDH_NOVAT", true);

            doSetPOVatCheckbox();         
            
			//Clear all manual price change flags
			//moDispRows.setFormDFValue(IDH_DISPROW._MANPRC, 0);
			
            /*
            moDispRows.setFormDFValue(IDH_DISPROW._TRdWgt, 0);
            moDispRows.setFormDFValue(IDH_DISPROW._PRdWgt, 0);
            */
            moDispRows.setFormDFValue(IDH_DISPROW._CstWgt, 0);
            moDispRows.setFormDFValue(IDH_DISPROW._TCharge, 0);
            moDispRows.setFormDFValue(IDH_DISPROW._TCTotal, 0);


            moDispRows.DBObject.doCalculateTotals(IDH_DISPROW.TOTAL_CALCULATE);

			if (bReplaceProd){
				LinkedBP oLBP = moLookup.doGetLinkedBP( (string)getFormDFValue( IDH_DISPORD._CardCd));
				if ( oLBP != null) {
                	BP oBP = oLBP.getBP(false);
                	if ( oBP!=null ) {
				    	doSetChoosenProducer( ref oBP, bDoBPRules, true);
                	}
                	if ( bSetByUser )
                        doMarkFormFieldSetFromUser("IDH_PROCD");
				}
			}
            doSwitchPriceFields();

            doSwitchTransactionCodeFields();
		}

		public void doSwitchToSO(bool bDoBPRulesOnProd)	{
            string sJobType = moDispRows.getFormDFValueAsString( IDH_DISPROW._JobTp );
            bool bIsOutgoingJob = Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType);
            
			//doSetSOCheckBoxes();
            doSetCheckBoxes();

            EnableItems(ref moDisposalSiteItems, Config.ParameterAsBool("DODCEDI", false));

            EnableItems(ref moCostIncommingItems, false);
            EnableItems(ref moCostOutgoingItems, bIsOutgoingJob);

            EnableItem("IDH_NOVAT", true);

            //if (!string.IsNullOrWhiteSpace(moDispRows.DBObject.U_TrnCode)) {
            //    EnableItems(ref moMarketingCheckboxes, false);
            //}

            if (moMarketingMode == Config.MarketingMode.SO) {
                doSwitchTransactionCodeFields();
                return;
            } else
                moMarketingMode = Config.MarketingMode.SO;

            EnableItems(ref moSalesItems, true);

            //EnableItem("IDH_NOVAT", true);
 			
			//Clear all manual price change flags
			//moDispRows.setFormDFValue(IDH_DISPROW._MANPRC, 0);

            moDispRows.setFormDFValue(IDH_DISPROW._PRdWgt, 0);
			moDispRows.setFormDFValue(IDH_DISPROW._ProWgt, 0);
			moDispRows.setFormDFValue(IDH_DISPROW._PCost, 0);

            moDispRows.setFormDFValue(IDH_DISPROW._TRdWgt, 0);
            moDispRows.setFormDFValue(IDH_DISPROW._TipWgt, 0);
			moDispRows.setFormDFValue(IDH_DISPROW._TipCost, 0);

			if (bDoBPRulesOnProd) {
				doBusinessPartnerRules( false, false, false, true);
			}

            doSetSOVatCheckbox();
            doSwitchPriceFields();

            doSwitchTransactionCodeFields();
		}

        public void doSwitchTransactionCodeFields() {
            if (!string.IsNullOrWhiteSpace(moDispRows.DBObject.U_TrnCode)) {
                EnableItems(ref moMarketingCheckboxes, false);
            }
        }

        public void doSetCheckBoxes() {
            doSetPOCheckBoxes();
            doSetSOCheckBoxes();
        }

        private void doSwitchPriceFields() {
			//MA Start 27-10-2014 Issue#417 Issue#418
            if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) == false || Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) == false) 
            {
                EnableItems(ref moP_Cost_ShowFlds, false);
                doBlankOutItems(ref moP_Cost_ShowFlds, 14930874);
            }
            else if (!Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) || !Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Cost_PRICES))
            {
                EnableItems(ref moPUpdateFlds_Cost, false);
            }
            else if (!doUsePrices())
            {
                EnableItems(ref moPUpdateFlds_Cost, false);
            }


            if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) == false || Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) == false)
            {
                EnableItems(ref moP_Charge_ShowFlds, false);
                doBlankOutItems(ref moP_Charge_ShowFlds, 14930874);
            }
            else if (!Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) || !Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Charge_PRICES))
            {
                EnableItems(ref moPUpdateFlds_Charge, false);
            }
            else if (!doUsePrices())
            {
                EnableItems(ref moPUpdateFlds_Charge, false);
            }
            /*
			if (!Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES)) {
                EnableItems(ref moPShowFlds, false);
                doBlankOutItems( ref moPShowFlds, 14930874);
            } else if (!Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES)) {
                EnableItems(ref moPUpdateFlds, false);
            } else if ( !doUsePrices() ) {
                EnableItems(ref moPUpdateFlds, false);
            }
			*/
			//MA End 27-10-2014 Issue#417 Issue#418
        }

        private bool doUsePrices() {
            //Dim sLinkedWOR As String = getDFValue(oForm, msRowTable, "U_WRRow")
            if (moDispRows.DBObject.U_WRRow != null && moDispRows.DBObject.U_WRRow.Length > 0) {
                return Config.INSTANCE.getParameterAsBool("DOPLLNK", true);
            }
            return true;
        }
#endregion		
#region WeighBridge
		public void  doReadWeight( string sReadType ){
			if ( oTimer != null && mbTimerStarted ) {
				oTimer.Stop();
				
				try {
	            	moDispRows.doReadWeight(sReadType);
				}
				catch ( Exception ){}
	            
				oTimer.Enabled = true;
	            oTimer.Start();
			}
			else {
				moDispRows.doReadWeight(sReadType);	
			}
		}
#endregion
#region VatSettings
		private void doSetVatCheckbox()
		{
			if ( MMode == Config.MarketingMode.PO ) {
				doSetPOVatCheckbox();
			}
			else if ( MMode == Config.MarketingMode.SO ) {
				doSetSOVatCheckbox();
			}
		}

		private void doSetPOVatCheckbox()
		{
			string sVatGroup = moDispRows.getFormDFValueAsString(IDH_DISPROW._TCostVtGrp);
			if ((sVatGroup != null) && sVatGroup.Equals(Config.Parameter("MDVTEXGI"))) {
				setItemValue("IDH_NOVAT", "Y");
                moDispRows.DBObject.DoChargeVat = false;
			}
			else {
                setItemValue("IDH_NOVAT", "N");
                moDispRows.DBObject.DoChargeVat = true;
			}
		}

		private void doSetSOVatCheckbox()
		{
			string sVatGroup = moDispRows.getFormDFValueAsString(IDH_DISPROW._TChrgVtGrp);
			if ((sVatGroup != null) && sVatGroup.Equals(Config.Parameter("MDVTEXGO"))) {
				setItemValue("IDH_NOVAT", "Y");
                moDispRows.DBObject.DoChargeVat = false;
			}
			else {
				setItemValue("IDH_NOVAT", "N");
                moDispRows.DBObject.DoChargeVat = true;
			}
		}
#endregion

#region GeneralCalcs
#endregion
#region DoSearches
    public bool doSetVehicleSearchParams(ref object oSearch, string sTarget, bool bDoSilent, bool bFromFocus, bool bIgnoreData)
    {
			if ( oSearch is SearchBase ) {
				SearchDialog = oSearch as SearchBase;
				SearchDialog.Target = sTarget;
				//SearchDialog.DoSilent = bDoSilent;
                SearchDialog.DoShowMulti = bDoSilent;
			}
			else {
				if ( bDoSilent )
					setDialogValue("SILENT", "SHOWMULTI");
				setDialogValue("TRG", sTarget);
			}

			string sReg = moDispRows.getItemValue("IDH_VEHREG");
            //sReg = moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._Lorry); //IDH_VEHREG"); //.Text;
           
		    string sRegChanged = utils.General.doFixReg(sReg);
			string sCustomer = "";
				
		    //if ( !sRegChanged.Equals( sReg ) )
		    //	moDispRows.setFormDFValue(IDH_DISPROW._Lorry, sRegChanged, true);

		   	if ( moLookup.getParameterAsBool( "DOVMSUC", true ) ) {
		    	sCustomer = getFormDFValueAsStringNZ(IDH_DISPORD._CardCd); //"IDH_CUST");
		    }

            if (moLookup.getParameterAsBool("VSUBRA", true)) {
                setDialogValue("IDH_BRANCH", moDispRows.DBObject.U_Branch);
            }

            setDialogValue("IDH_VEHCUS", bIgnoreData?"":sCustomer );
		   	setDialogValue("IDH_VEHREG", bIgnoreData?"":sRegChanged );

            //LPV #38 Begin
            //setDialogValue("IDH_VEHTP", "" );
            setDialogValue("IDH_VEHTP", Config.ParameterWithDefault("DFLSRTYE", ""));
            //LPV #38 End

		   	setDialogValue("IDH_ITMGRP","" );

            return true;
		}

        /**
         * Search for open WOrders to link
         */
        private string msLastLinkWO = null;
        private ArrayList moLastLinkWORs = null;
        public void doResetLastLinked() {
            msLastLinkWO = null;
            moLastLinkWORs = null;
        }
        public bool doSetLinkWOParams(ref object oSearch, string sTarget, bool bDoSilent, bool bFromFocus, bool bIgnoreData, bool bUseLast)  {
            if (oSearch is idh.wr1.form.search.LinkWO) { //SearchBase) {
                //idh.wr1.form.search.LinkWO
                SearchDialog = oSearch as SearchBase;
                SearchDialog.Target = sTarget;
                //SearchDialog.DoSilent = bDoSilent;
                SearchDialog.DoShowMulti = bDoSilent;

                string sOrd = moDispRows.getItemValue("IDH_BOOREF");
                string sRow = moDispRows.getItemValue("IDH_ROW");
                string sUsedRows = null;
                if ( moLastLinkWORs != null && moLastLinkWORs.Count > 0 ) {
                    sUsedRows = com.idh.utils.General.doCreateStringCSL(moLastLinkWORs);
                }
                ((idh.wr1.form.search.LinkWO)oSearch).doSetupControl(sOrd, sRow, sUsedRows);
            }
            else {
                if (bDoSilent)
                    setDialogValue("SILENT", "SHOWMULTI");
                setDialogValue("TRG", sTarget);
            }
 
            string sReg = moDispRows.getItemValue("IDH_VEHREG");
            string sRegChanged = utils.General.doFixReg(sReg);

            String sWORowID = moDispRows.getItemValue("IDH_WRROW");
            String sWOCode = moDispRows.getItemValue("IDH_WRORD");
        	//String sDORowID = getFormDFValueAsStringNZ( IDH_DISPROW._Code);
        	//String sDOCode = getFormDFValueAsStringNZ( IDH_DISPORD._Code);

            if ( sWOCode.Length == 0 && sWORowID.Length == 0 ) {
                if (bUseLast) {
                    if (!string.IsNullOrEmpty(msLastLinkWO)) {
                        sWOCode = msLastLinkWO;
                        moDispRows.DBObject.U_WROrd = sWOCode;
                    }
                }
            } else if (msLastLinkWO != null && !msLastLinkWO.Equals(sWOCode)) {
                doResetLastLinked();
            }
            
            setDialogValue("IDH_VEHREG", bIgnoreData?"":"^" + sRegChanged );
            //setDialogValue("DISPORD", sDOCode);
            //setDialogValue("DISPROW", sDORowID);
            setDialogValue("IDH_WOHD", bIgnoreData ? "" : sWOCode );
            setDialogValue("IDH_WOROW", bIgnoreData ? "" : sWORowID );
            setDialogValue("SILENT", "SHOWMULTI");

            return true;
        }

		/*
		 * Search for a Business Partner: WC = Waste Carrier
         * bIgoreData - if this is set the data will not be fitered by the current data
		 */
		public bool doSetBPSearchParams(  ref object oSearch, string sTarget, bool bDoSilent, bool FromFocus, bool bIgnoreData  ) {
			if ( oSearch is SearchBase ) {
				SearchDialog = oSearch as SearchBase;
				SearchDialog.Target = sTarget;
				//SearchDialog.DoSilent = bDoSilent;
                SearchDialog.DoShowMulti = bDoSilent;
			}
			else {
				if ( bDoSilent )
					setDialogValue("SILENT", "SHOWMULTI");
				setDialogValue("TRG", sTarget);
			}
			
			string sType = "";
            string sGroup = "";
            string sBranch = "";
            string sBPCode = "";
            string sBPName = "";
			bool bDoFilter = moLookup.getParameterAsBool( "DOBPFLT", true );
			
			if ( sTarget.Equals( BP_CUSTOMER ) ) {
                
                if ( !bIgnoreData ) 
                {
                    sBPCode = moDispRows.getItemValue("IDH_CUST");  //getFormDFValueAsStringNZ(IDH_DISPORD._CardCd);
                    sBPName = ""; // getFormDFValueAsStringNZ(IDH_DISPORD._CardNM);
                }
                
                if ( bDoFilter && sBPCode.Length == 0 ) 
					sType = "F-C";
			}
            else if (sTarget.Equals(BP_CUSTOMERNAME))
            {

                if (!bIgnoreData)
                {
                    sBPCode = moDispRows.getItemValue("IDH_CUST"); //getFormDFValueAsStringNZ(IDH_DISPORD._CardCd);
                    sBPName = moDispRows.getItemValue("IDH_CUSTNM"); //getFormDFValueAsStringNZ(IDH_DISPORD._CardNM);
                    if (sBPName.Length > 0 && sBPName.EndsWith("*"))
                    {
                        sBPCode = "";
                        sBPName = sBPName.Substring(0, sBPName.Length - 1);
                    }
                    else {
                        if (bDoFilter)
                    		sType = "F-C";
                        
                    	return false;
                    }
                }
            }
			else if ( sTarget.Equals( BP_CARRIER ) ) {

                if (!bIgnoreData)
                {
                    sBPCode = moDispRows.getItemValue("IDH_CARRIE");  //getFormDFValueAsStringNZ(IDH_DISPORD._CCardCd);
                    sBPName = ""; // getFormDFValueAsStringNZ(IDH_DISPORD._CCardNM);
                }

                if ( bDoFilter && sBPCode.Length == 0 ) {
					sType = "C";
					sGroup = Config.Parameter( "BGCWCarr");
				}
			}
            else if (sTarget.Equals(BP_CARRIERNAME))
            {

                if (!bIgnoreData)
                {
                    sBPCode = ""; // getFormDFValueAsStringNZ(IDH_DISPORD._CCardCd);
                    sBPName = moDispRows.getItemValue("IDH_CARNAM");  //getFormDFValueAsStringNZ(IDH_DISPORD._CCardNM);
                    if (sBPName.Length > 0 && sBPName.EndsWith("*"))
                    {
                        sBPCode = "";
                        sBPName = sBPName.Substring(0, sBPName.Length - 1);
                    }
                    else {
		                if (bDoFilter)
		                {
		                    sType = "C";
		                    sGroup = Config.Parameter("BGCWCarr");
		                }                        
                    	return false;
                    }
                }
            } else if (sTarget.Equals(BP_PRODUCER) || sTarget.Equals(BP_BUYFROM)) {
                if (!bIgnoreData)
                {
                    if ( sTarget.Equals(BP_BUYFROM) )
                        sBPCode = moDispRows.getItemValue("IDH_PROCD");
                    else
                        sBPCode = moDispRows.getItemValue("IDH_WPRODU");  //getFormDFValueAsStringNZ(IDH_DISPORD._PCardCd);
                    sBPName = ""; // getFormDFValueAsStringNZ(IDH_DISPORD._PCardNM);
                }
                
            	if ( bDoFilter && sBPCode.Length == 0) {
                    if (sTarget.Equals(BP_BUYFROM)) {
                        sType = "S";
                        sGroup = Config.Parameter("BGSProdu");
                    } else {
                        sType = "C";
                        sGroup = Config.Parameter("BGCProdu");
                    }
            	}                
			} else if (sTarget.Equals(BP_PRODUCERNAME) || sTarget.Equals(BP_BUYFROMNAME))
            {
                if (!bIgnoreData)
                {
                    sBPCode = ""; // getFormDFValueAsStringNZ(IDH_DISPORD._PCardCd);
                    if ( sTarget.Equals(BP_BUYFROMNAME) )
                        sBPName = moDispRows.getItemValue("IDH_PRONM");
                    else
                        sBPName = moDispRows.getItemValue("IDH_WNAM");  //getFormDFValueAsStringNZ(IDH_DISPORD._PCardNM);

                    if (sBPName.Length > 0 && sBPName.EndsWith("*"))
                    {
                        sBPCode = "";
                        sBPName = sBPName.Substring(0, sBPName.Length - 1);
                    }
                    else {
		            	if (bDoFilter){
		                    sType = "C";
		                    sGroup = Config.Parameter( "BGCProdu" );
		            	}                        
                    	return false;
                    }
                }
            } 
            else if (sTarget.Equals(BP_DISPOSALSITE) || sTarget.Equals(BP_DISPOSALSITE2)) {
            	string sSiteOption = Config.Parameter( "DODEDII");
            	string sJobType = moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._JobTp);
            	bool bCanEditSite = false;
            
            	if ( sSiteOption != null && sSiteOption.Equals("USER") ) 
	            	bCanEditSite = true;
            	//else if ( sJobType.Equals( moLookup.doGetJopTypeOutgoing() ))
            	else if ( moLookup.isOutgoingJob(moLookup.doGetDOContainerGroup(), sJobType) )
	                bCanEditSite = true;
	            else {
	            	bCanEditSite = moLookup.getParameterAsBool( "DODCEDI", false );
                    if (!bCanEditSite)
                        //DataHandler.INSTANCE.doError("User: Can only change the Disposal site for outgoing Jobs", "Can only change the Disposal site for outgoing Jobs");
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Can only change the Disposal site for outgoing Jobs", "ERUSCDSP", null);
	            }
                        
	            if ( bCanEditSite ) {
                    if ( sTarget.Equals(BP_DISPOSALSITE2) )
                        sBPCode = (bIgnoreData ? "" : moDispRows.getItemValue("IDH_DISPCD"));
                    else
                        sBPCode = (bIgnoreData?"":moDispRows.getItemValue("IDH_DISSIT"));  //getFormDFValueAsStringNZ(IDH_DISPORD._SCardCd);
                    sBPName = "";

            		if ( bDoFilter && sBPCode.Length == 0 ) {
        				sType = "S";  
		            	sGroup = Config.Parameter( "BGCDispo");
       				 	sBranch = "";   
       				 	
		            	//if ( sJobType.Equals( moLookup.doGetJopTypeIncoming()) ) {
		            	if ( moLookup.isIncomingJob(moLookup.doGetDOContainerGroup(), sJobType) ) {
		            		if ( sSiteOption != null && sSiteOption.Equals("USER") ) {
								sType = "S";
								sGroup = Config.Parameter( "BGSDispo" );
            					sBranch =  moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._Branch);
            				}
		            	}
            		}
	            } 
            	else
            		return false;
			} else if (sTarget.Equals(BP_DISPOSALSITENAME) || sTarget.Equals(BP_DISPOSALSITENAME2))
            {
            	string sSiteOption = Config.Parameter( "DODEDII");
				string sJobType = moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._JobTp);
            	bool bCanEditSite = false;
            
            	if ( sSiteOption != null && sSiteOption.Equals("USER") ) 
	            	bCanEditSite = true;
            	//else if ( sJobType.Equals( moLookup.doGetJopTypeOutgoing() ))
            	else if ( moLookup.isOutgoingJob(moLookup.doGetDOContainerGroup(), sJobType))
	                bCanEditSite = true;
	            else {
	            	bCanEditSite = moLookup.getParameterAsBool( "DODCEDI", false );
                    if (!bCanEditSite)
                        //DataHandler.INSTANCE.doError("User: Can only change the Disposal site for outgoing Jobs", "Can only change the Disposal site for outgoing Jobs");
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Can only change the Disposal site for outgoing Jobs", "ERUSCDSP", null);
	            }				

            	if ( bCanEditSite ) {
                    sBPCode = "";
                    if ( sTarget.Equals(BP_DISPOSALSITENAME2) )
                        sBPName = (bIgnoreData ? "" : moDispRows.getItemValue("IDH_DISPNM"));
                    else
                        sBPName = (bIgnoreData?"":moDispRows.getItemValue("IDH_DISNAM"));  //getFormDFValueAsStringNZ(IDH_DISPORD._SCardNM);
                    
                    if (sBPName.Length > 0 && sBPName.EndsWith("*"))
                    {
                        sBPCode = "";
                        sBPName = sBPName.Substring(0, sBPName.Length - 1);
                    }
                    else {
                         if ( bDoFilter ) {
        					sType = "S";  
		            		sGroup = Config.Parameter( "BGCDispo");
       				 		sBranch = "";   
       				 	
		            		//if ( sJobType.Equals( moLookup.doGetJopTypeIncoming()) ) {
		            		if ( moLookup.isIncomingJob(moLookup.doGetDOContainerGroup(), sJobType) ) {
		            			if ( sSiteOption != null && sSiteOption.Equals("USER") ) {
									sType = "S";
									sGroup = Config.Parameter( "BGSDispo" );
            						sBranch =  moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._Branch);
            					}
		            		}
            			}
                                        
                    	return false;
                    }
                }
            	else
            		return false;
            }
    	    else if ( sTarget.Equals( BP_COMPLIANCE ) ) {
				setDialogValue( "IDH_CS", "Y");
    	   	}
			
            setDialogValue( "IDH_TYPE", sType);
            setDialogValue( "IDH_GROUP", sGroup);
            setDialogValue( "IDH_BRANCH", sBranch);
            setDialogValue( "IDH_BPCOD", sBPCode);
            setDialogValue( "IDH_NAME", sBPName);

			return true;
		}

		/*
		 * Search for a Business Partner: WC = Waste Carrier
		 */
		public bool doSetAddressSearchParams(  ref object oSearch, string sTarget, bool bDoSilent ) {
			if ( oSearch is SearchBase ) {
				SearchDialog = oSearch as SearchBase;
				SearchDialog.Target = sTarget;
				//SearchDialog.DoSilent = bDoSilent;
                SearchDialog.DoShowMulti = bDoSilent;
			}
			else {
				if ( bDoSilent )
					setDialogValue("SILENT", "SHOWMULTI");
				setDialogValue("TRG", sTarget);
			}
			
			string sCardCode = "";
			if ( sTarget.Equals(BP_CUSTOMER) )
                sCardCode = getFormDFValueAsString(IDH_DISPORD._CardCd);
            else if ( sTarget.Equals(BP_CARRIER) )
                sCardCode = getFormDFValueAsString(IDH_DISPORD._CCardCd);
            else if ( sTarget.Equals(BP_PRODUCER) )
                sCardCode = getFormDFValueAsString(IDH_DISPORD._PCardCd);
            else if ( sTarget.Equals(BP_DISPOSALSITE) )
                sCardCode = getFormDFValueAsString(IDH_DISPORD._SCardCd);


			setDialogValue("IDH_CUSCOD", sCardCode);
            setDialogValue("IDH_ADRTYP", "S");
			return true;
		}
		
		public bool doSetWasteItemSearchParams( ref object oSearch, bool bDoSilent, bool bFromFocus, bool bIgnoreData ) {
			if ( oSearch is SearchBase ) {
				SearchDialog = oSearch as SearchBase;
				SearchDialog.Target = "WAST";
				//SearchDialog.DoSilent = bDoSilent;
                SearchDialog.DoShowMulti = bDoSilent;
			}
			else {
				if ( bDoSilent )
					setDialogValue("SILENT", "SHOWMULTI");

                setDialogValue("TRG", "WAST");
			}

            if ( bFromFocus || !bIgnoreData )
            {
                string sWasteCode = moDispRows.getItemValue("IDH_WASCL1");
                string sWasteDesc = moDispRows.getItemValue("IDH_WASMAT");
                if (sWasteDesc.Length > 0 && sWasteDesc.EndsWith("*")) {
                    sWasteCode = "";

//                    setDialogValue("IDH_ITMCOD", sWasteCode);
//	                setDialogValue("IDH_ITMNAM", sWasteDesc);
                }
                else
                    sWasteDesc = "";

                setDialogValue("IDH_ITMCOD", sWasteCode);
                setDialogValue("IDH_ITMNAM", sWasteDesc);
            }
            else
            {
                setDialogValue("IDH_ITMCOD", "");
                setDialogValue("IDH_ITMNAM", "");
            }
            setDialogValue( "IDH_GRPCOD", moLookup.doWasteMaterialGroup());
			setDialogValue( "IDH_CRDCD", getFormDFValueAsString(IDH_DISPORD._CardCd));
            setDialogValue("IDH_INVENT", ""); //"N");
			
			return true;
		}

        public bool doSetItemSearchParams(ref object oSearch, bool bDoSilent, bool bFromFocus, bool bIgnoreData )
        {
			if ( oSearch is SearchBase ) {
				SearchDialog = oSearch as SearchBase;
				SearchDialog.Target = "PROD";
				//SearchDialog.DoSilent = bDoSilent;
                SearchDialog.DoShowMulti = bDoSilent;
			}
			else {
				if ( bDoSilent )
					setDialogValue("SILENT", "SHOWMULTI");
				setDialogValue("TRG", "PROD");
			}

            string sContainerCode = "";
            if (!bIgnoreData)
            {
                sContainerCode = moDispRows.getItemValue("IDH_ITMCOD");
                if (sContainerCode == null)
                    sContainerCode = "";
                else if (sContainerCode.EndsWith("*"))
                {
                    if (sContainerCode.Length == 0)
                        sContainerCode = "";
                    else
                        sContainerCode = sContainerCode.Substring(sContainerCode.Length - 1);
                }
            }

			setDialogValue(  "IDH_ITMCOD", sContainerCode);
            setDialogValue(  "IDH_GRPCOD", Config.Parameter( "MDDDIG"));

			return true;
		}		
		
		public bool doSetOriginSearchParams(string sControlName, ref object oSearch, bool bDoSilent ) {
			if ( oSearch is SearchBase ) {
				SearchDialog = oSearch as SearchBase;
				SearchDialog.Target = "ORI";
				//SearchDialog.DoSilent = bDoSilent;
                SearchDialog.DoShowMulti = bDoSilent;
			}
			else {
				if ( bDoSilent )
					setDialogValue("SILENT", "SHOWMULTI");
				setDialogValue("TRG", "ORI");
			}

            //string sOrigin = moDispRows.getItemValue("IDH_RORIGI"); //DisposalRows.getFormDFValueAsStringNZ(IDH_DISPROW._Origin);
            string sOrigin = moDispRows.getItemValue(sControlName); //DisposalRows.getFormDFValueAsStringNZ(IDH_DISPROW._Origin);

           	if ( sOrigin.Length > 0  && sOrigin.Equals("*") )
        		sOrigin = "";

           	setDialogValue("IDH_ORCODE", sOrigin);

           	return true;
		}			
		
#endregion
#region HandleSearchResults
		/*
		 * Handels the result from the Vehicle Search Dialog
		 */
		public void doHandleLorryResult(ref object oSearch, bool bFromFocus, bool bIsNew ){
			if ( oSearch is SearchBase ) 
				SearchDialog = oSearch as SearchBase;
			
            string sCustomerCode = getDialogValue("CUSCD");
            string sWasteCarrierCode = getDialogValue("WCCD"); //Onsite Carrier
            string sCustomerName = getDialogValue("CUSTOMER");
            string sWasteCarrierName = getDialogValue("WCNAM");
            string sCarrierAddress = null;
            string sCustomerAddress = getDialogValue("CADDRESS");
            string sVehicleCode = getDialogValue("VEHCODE");

            bool bSetCustomerAddress = true;
            bool bSetDisposalAddress = true;
            bool bSetProducerAddress = true;
            bool bSetCarrierAddress = true;

            bool bDoRules = true;
            
            //			string sPayType = getDialogValue("MARKAC");
			
//            string sJobTp = (string)moDispRows.getFormDFValue(IDH_DISPROW._JobTp, true);
//            if (sJobTp == null || sJobTp.Length == 0)
//            {
//                DataHandler.INSTANCE.doError("The Job Type must be selected", "The Job Type must be selected");
//                return;
//            }

            //moDispRows.DBObject.BlockChangeNotif = true;
            //DBObject.BlockChangeNotif = true;
            string sJobTp = (string)moDispRows.getFormDFValue(IDH_DISPROW._JobTp, true);
            try {
                string sVehicleReg = getDialogValue("REG");
                if (bIsNew) {

                    if (sJobTp == null || sJobTp.Length == 0) {
                        //DataHandler.INSTANCE.doError("User: The Job Type must be selected", "The Job Type must be selected");
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Job Type must be selected", "ERUSJTSR", null);
                        return;
                    }

                    mbCreateNewVechicle = true;

                    if (sVehicleReg == null || sVehicleReg.Length == 0) {
                        sVehicleReg = (string)moDispRows.getFormDFValue(IDH_DISPROW._Lorry, true);
                    } else {
                        string sDriver = getDialogValue("DRIVER");
                        moDispRows.VType = getDialogValue("VEHTYPE");
                        moDispRows.DBObject.VehicleType = getDialogValue("WR1Type");

                        string sVehGroup = getDialogValue("VEHGROUP");
                        if (String.IsNullOrWhiteSpace(sCustomerCode))
                            sCustomerCode = getDialogValue("CUSTOMER");

                        if (String.IsNullOrWhiteSpace(sWasteCarrierCode))
                            sWasteCarrierCode = getDialogValue("CARRIER");

                        if (!String.IsNullOrWhiteSpace(sWasteCarrierCode)) {
                            sWasteCarrierName = Config.INSTANCE.doGetBPName(sWasteCarrierCode);
                            if (Config.ParameterAsBool("VMUSENEW", false)) {
                                LinkedBP oBinkedBP = Config.INSTANCE.doGetLinkedBP(sWasteCarrierCode);
                                if (oBinkedBP != null) {
                                    sCustomerCode = oBinkedBP.LinkedBPCardCode;
                                } else {
                                    sCustomerCode = "";
                                    sCustomerName = "";
                                }
                            }
                        }

                        if (!String.IsNullOrWhiteSpace(sCustomerCode)) {
                            sCustomerName = Config.INSTANCE.doGetBPName(sCustomerCode);
                        }

                        //moDispRows.setFormDFValue(IDH_DISPROW._Driver, sDriver);
                        //moDispRows.setFormDFValue(IDH_DISPROW._ItmGrp, sVehGroup);

                        //sVehicleReg = utils.General.doFixReg(sVehicleReg);
                        //moDispRows.setFormDFValue(IDH_DISPROW._LorryCd, "", false, true, data_SetFrom.idh_SET_MANUALLY);

                        if (Mode == Mode_FIND || Mode == Mode_STARTUP)
                            Mode = Mode_ADD;
                        else if (Mode == Mode_OK || Mode == Mode_VIEW)
                            Mode = Mode_UPDATE;

                        //Mode = Mode_ADD;
                        //oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                        //doLoadDataLocal(oForm, False)

                        moDispRows.setFormDFValue(IDH_DISPROW._Driver, sDriver);
                        //moDispRows.setFormDFValue(IDH_DISPROW._ItmGrp, sVehGroup);

                        sVehicleReg = utils.General.doFixReg(sVehicleReg);
                        moDispRows.setFormDFValue(IDH_DISPROW._LorryCd, "", false, true, data_SetFrom.idh_SET_MANUALLY);

                        //doSetVehicleReg(oForm, sReg, "", sJob, False, False)
                        moDispRows.setFormDFValue(IDH_DISPROW._Lorry, sVehicleReg);
                        //moDispRows.setFormDFValue(IDH_DISPROW._JobTp, sJobTp);
                        EnableItem("IDH_TARWEI", true);
                        EnableItem("IDH_TRLTar", true);

                        EnableItem("IDH_TRWTE1", true);
                        EnableItem("IDH_TRWTE2", true);

                        if ((sWasteCarrierCode != null) && sWasteCarrierCode.Length > 0) {
                            setFormDFValue(IDH_DISPORD._CCardCd, sWasteCarrierCode);
                            setFormDFValue(IDH_DISPORD._CCardNM, sWasteCarrierName);
                        }

                        //doSetFocus(oForm, "IDH_VEHREG")
                        //setWFValue(oForm, "LastVehAct", "CREATE")
                        if ((sCarrierAddress != null) && sCarrierAddress.Length > 0) {
                            BPAddress oAddress = new BPAddress();
                            if (oAddress.doGetAddress(sWasteCarrierCode, BPAddress.ADDRTYPE.SHIPTO, true) > 0) {
                                doSetCarrierAddress(ref oAddress);
                                bSetCarrierAddress = false;
                            }
                        }

                        if ((sCustomerCode != null) && sCustomerCode.Length > 0) {
                            setFormDFValue(IDH_DISPORD._CardCd, sCustomerCode);
                            setFormDFValue(IDH_DISPORD._CardNM, sCustomerName);

                            BP oBP = new BP();
                            if (oBP.doGetBPInfo(sCustomerCode, false) > 0) {
                                if (oBP.U_IDHUOM != null && oBP.U_IDHUOM.Length > 0) {
                                    moDispRows.setFormDFValue(IDH_DISPROW._UOM, oBP.U_IDHUOM, true);
                                }
                                if (oBP.U_IDHONCS != null && oBP.U_IDHONCS.Length > 0) {
                                    moDispRows.setFormDFValue(IDH_DISPROW._CustCs, oBP.U_IDHONCS, true);
                                }

                                if (oBP.U_IDHOBLGT != null && oBP.U_IDHOBLGT.Length > 0) {
                                    moDispRows.setFormDFValue(IDH_DISPROW._Obligated, oBP.U_IDHOBLGT, true);
                                }

                                if (oBP.U_WASLIC != null && oBP.U_WASLIC.Length > 0) {
                                    setFormDFValue(IDH_DISPORD._WasLic, oBP.U_WASLIC, true);
                                    moDispRows.setFormDFValue(IDH_DISPROW._WASLIC, oBP.U_WASLIC, true);
                                }
                            }

                            BPAddress oAddress = null;
                            if ((sCustomerAddress != null) && sCustomerAddress.Length > 0) {
                                string sPh1 = "";
                                oAddress = new BPAddress();
                                if (oAddress.doGetAddress(sCustomerCode, BPAddress.ADDRTYPE.SHIPTO, sCustomerAddress, true) > 0) {
                                    doSetCustomerAddress(ref oAddress, sPh1);
                                    bSetCustomerAddress = false;
                                } else
                                    oAddress = null;
                            }

                            if (moLookup.getParameterAsBool("DOVMSCAP", true)) {
                                setFormDFValue(IDH_DISPORD._PCardCd, sCustomerCode);
                                setFormDFValue(IDH_DISPORD._PCardNM, sCustomerName);

                                if ((sCustomerAddress != null) && sCustomerAddress.Length > 0) {
                                    if (oAddress != null && oAddress.Count > 0) {
                                        doSetProducerAddress(ref oAddress, false);
                                        bSetProducerAddress = false;
                                    }
                                }
                            }
                        }

                        //if (bDoRules)
                        //    doBusinessPartnerRules(bSetCustomerAddress, bSetCarrierAddress, bSetDisposalAddress, bSetProducerAddress);

                    }
                } else {
                    //			if ( sPayType!=null && (sPayType.Equals("DOORD") || sPayType.Equals("REQORD") ) ) {
                    if (sCustomerCode != null && sCustomerCode.Length > 0) {
                        //					if ( moLookup.getParameterAsBool( "DOVMUC", true ) ||
                        //					    ( sCarrier==null && sCustomer.Equals(sCarrier) ) ) {
                        double dChargeTotal = moDispOrdDB.getUnCommittedRowTotals();
                        bool bBlockOnFail = Config.INSTANCE.getParameterAsBool("DOBCCL", true);
                        if (!moLookup.doCheckCredit(sCustomerCode, dChargeTotal, false, bBlockOnFail))
                            return;
                    }
                    //				}
                    //			}

                    mbCreateNewVechicle = false;

                    string sJobNr = null;
                    string sJobRow = null;

                    //** The onsite search **'
                    sJobNr = getDialogValue("DONR");
                    //if (sJobNr != null && sJobNr.Length == 0)
                    //    sJobNr = null;

                    sJobRow = getDialogValue("DOROW");
                    //if (sJobRow != null && sJobRow.Length == 0)
                    //    sJobRow = null;

                    if (string.IsNullOrWhiteSpace(sJobNr) && string.IsNullOrWhiteSpace(sJobRow)) {
                        ValuePair oPair = Config.INSTANCE.doFindDOSecondWeighForVehicle(sVehicleReg, sVehicleCode, sJobTp, sCustomerCode);
                        if (oPair != null) {
                            sJobNr = oPair.Value1;
                            sJobRow = oPair.Description;
                        }
                    }
                    
                    if (!string.IsNullOrWhiteSpace(sJobNr) || !string.IsNullOrWhiteSpace(sJobRow)) {
                        moDispRows.DBObject.BlockChangeNotif = true;
                        DBObject.BlockChangeNotif = true;
                        try {
                            doLoad2ndWeigh(sJobNr, sJobRow);

                            if (sJobTp == null || sJobTp.Length == 0) {
                                //DataHandler.INSTANCE.doError("User: The Job Type must be selected", "The Job Type must be selected");
                                com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Job Type must be selected", "ERUSJTSR", null);
                                return;
                            }

                            //string sWasteCode = moDispRows.getFormDFValueAsString(IDH_DISPROW._WasCd);
                            ////if (moLookup.doCheckCanPurchaseWB(sWasteCode) && sJobTp.Equals(moLookup.doGetJopTypeIncoming()))
                            //if (moLookup.doCheckCanPurchaseWB(sWasteCode) && moLookup.isIncomingJob(moLookup.doGetDOContainerGroup(), sJobTp))
                            if (doCheckPurchase()) {
                                doSwitchToPO(true, false, false);  //2010-01-26 - doSwitchToPO(true, false); - Added to avoid the Rules from replacing the Linked BP
                            } else {
                                doSwitchToSO(false);
                            }
                        } catch (Exception ex) {
                            throw( ex);
                        } finally {
                            moDispRows.DBObject.BlockChangeNotif = false;
                            DBObject.BlockChangeNotif = false;
                        }
                    } else {
                        if (Mode == Mode_FIND || Mode == Mode_STARTUP)
                            Mode = Mode_ADD;
                        else if (Mode == Mode_OK || Mode == Mode_VIEW)
                            Mode = Mode_UPDATE;


                        //string sJobTp = (string)moDispRows.getFormDFValue(IDH_DISPROW._JobTp, true);
                        if (sJobTp == null || sJobTp.Length == 0) {
                            //DataHandler.INSTANCE.doError("User: The Job Type must be selected", "The Job Type must be selected");
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Job Type must be selected", "ERUSJTSR", null);
                            return;
                        }

                        string sVehicleType = getDialogValue("VEHDESC");
                        string sDriver = getDialogValue("DRIVER");

                        if (string.IsNullOrWhiteSpace(sCustomerCode))
                            sCustomerCode = getDialogValue("CUSCD");
                        //string sCustomerName = getDialogValue("CUSTOMER");

                        if (string.IsNullOrWhiteSpace(sWasteCarrierCode))
                            sWasteCarrierCode = getDialogValue("WCCD");

                        //string sWasteCarrierName = getDialogValue("WCNAM");
                        string sDisposalSiteCode = getDialogValue("SUPCD");
                        string sDisposalSiteName = getDialogValue("SUPPLIER");

                        //string sCustomerAddress = getDialogValue("CADDRESS");
                        string sDisposalAddress = getDialogValue("SADDRESS");
                        //string sCarrierAddress = null;

                        string sOrigin = getDialogValue("ORIGIN");

                        if (!String.IsNullOrWhiteSpace(sWasteCarrierCode)) {
                            sWasteCarrierName = Config.INSTANCE.doGetBPName(sWasteCarrierCode);
                            if (Config.ParameterAsBool("VMUSENEW", false)) {
                                LinkedBP oBinkedBP = Config.INSTANCE.doGetLinkedBP(sWasteCarrierCode);
                                if (oBinkedBP != null) {
                                    sCustomerCode = oBinkedBP.LinkedBPCardCode;
                                } else {
                                    sCustomerCode = "";
                                    sCustomerName = "";
                                }
                            }
                        }

                        if (!String.IsNullOrWhiteSpace(sCustomerCode)) {
                            sCustomerName = Config.INSTANCE.doGetBPName(sCustomerCode);
                        }

                        if (moDispRows.doSetVehicleReg(sCustomerCode, sVehicleReg, sVehicleCode, sVehicleType, sDriver, sJobTp)) {
                            moDispRows.VType = getDialogValue("VTYPE");

                            //setUFValue("IDH_TARWEI", getDialogValue("TARRE"));
                            //setUFValue("IDH_TRLTar", getDialogValue("TRLTAR"));

                            moDispRows.setFormDFValue(IDH_DISPROW._VehTyp, sVehicleType); //getDialogValue("VEHDESC"));
                            moDispRows.setFormDFValue(IDH_DISPROW._Driver, sDriver); //getDialogValue("DRIVER"));

                            string sItemCd = getDialogValue("ITEMCD");
                            if (!string.IsNullOrWhiteSpace(sItemCd)) {
                                moDispRows.setFormDFValue(IDH_DISPROW._ItemCd, sItemCd);
                                string sContainerDesc = getDialogValue("ITEMDSC");
                                if (sContainerDesc == null || sContainerDesc.Length == 0) {
                                    sContainerDesc = Config.INSTANCE.doGetItemDescription(getDialogValue("ITEMCD"));
                                }
                                moDispRows.DBObject.U_ItemDsc = sContainerDesc;
                            }

                            string sWasteCd = getDialogValue("WASCD");
                            if ( !string.IsNullOrWhiteSpace(sWasteCd) ) {
                                moDispRows.setFormDFValue(IDH_DISPROW._WasCd, sWasteCd );
                                string sWasteDesc = getDialogValue("WASDSC");
                                if (sWasteDesc == null || sWasteDesc.Length == 0) {
                                    sWasteDesc = Config.INSTANCE.doGetItemDescription(getDialogValue("WASCD"));
                                }
                                moDispRows.DBObject.U_WasDsc = sWasteDesc;
                            }
                            moDispRows.setFormDFValue(IDH_DISPROW._TRLNM, getDialogValue("TRLNM"));

                            //if (bFromFocus)
                            //    setItemValue("IDH_VEHREG", getDialogValue("TRLREG"));
                            //else
                            moDispRows.setFormDFValue(IDH_DISPROW._TRLReg, getDialogValue("TRLREG"));

                            string sWOLink = moDispRows.getFormDFValueAsString(IDH_DISPROW._WROrd);
                            if (sWOLink == null || sWOLink.Length == 0) {
                                string sMarkAct = null;
                                sMarkAct = getDialogValue("MARKAC");
                                if (sMarkAct != null) {

                                    //Only if not do Marketing docyments
                                    if ( Config.INSTANCE.getParameterAsBool("MDAGEN", false) || !string.IsNullOrWhiteSpace(moDispRows.DBObject.U_TrnCode) ) {
                                        if (sMarkAct.Equals("DOFOC") || sMarkAct.Equals(com.idh.bridge.lookups.FixedValues.getStatusFoc())) {
                                            moDispRows.setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getDoFocStatus());
                                        } else if (sMarkAct.Equals("DOORD")) {
                                            moDispRows.setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getDoOrderStatus());
                                        } else if (sMarkAct.Equals("DOARI")) {
                                            moDispRows.setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus());
                                        } else if (sMarkAct.Equals("DOARIP")) {
                                            moDispRows.setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getDoPInvoiceStatus());
                                        } else if (sMarkAct.Equals("DOAINV")) {
                                            moDispRows.setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getStatusInvoiced());
                                        } else {
                                            moDispRows.setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getStatusOpen());
                                        }
                                    } else {
                                        if (sMarkAct.Equals("DOFOC") || sMarkAct.Equals(com.idh.bridge.lookups.FixedValues.getStatusFoc())) {
                                            moDispRows.setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getReqFocStatus());
                                        } else if (sMarkAct.Equals("DOORD")) {
                                            moDispRows.setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getReqOrderStatus());
                                        } else if (sMarkAct.Equals("DOARI")) {
                                            moDispRows.setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getReqInvoiceStatus());
                                        } else if (sMarkAct.Equals("DOARIP")) {
                                            moDispRows.setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getReqPInvoiceStatus());
                                        } else if (sMarkAct.Equals("DOAINV")) {
                                            moDispRows.setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getStatusInvoiced());
                                        } else {
                                            moDispRows.setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getStatusOpen());
                                        }
                                    }
                                }
                            }

                            if ((sCustomerCode != null) && sCustomerCode.Length > 0) {
                                setFormDFValue(IDH_DISPORD._CardCd, sCustomerCode);
                                setFormDFValue(IDH_DISPORD._CardNM, sCustomerName);

                                //string sUOM = moLookup.getBPUOM(sCustomerCode);
                                //if ((sUOM != null) && sUOM.Length > 0) {
                                //    moDispRows.setFormDFValue(IDH_DISPROW._UOM, sUOM, true);
                                //}

                                //string sCs = moLookup.getBPCs(sCustomerCode);
                                //if ((sCs != null) && sCs.Length > 0) {
                                //    moDispRows.setFormDFValue(IDH_DISPROW._CustCs, sCs, true);
                                //}

                                //string sObligated = null;
                                //sObligated = moLookup.getBPOligated(sCustomerCode);
                                //moDispRows.setFormDFValue(IDH_DISPROW._Obligated, sObligated);

                                BP oBP = new BP();
                                if (oBP.doGetBPInfo(sCustomerCode, false) > 0) {
                                    if (oBP.U_IDHUOM != null && oBP.U_IDHUOM.Length > 0) {
                                        moDispRows.setFormDFValue(IDH_DISPROW._UOM, oBP.U_IDHUOM, true);
                                    }
                                    if (oBP.U_IDHONCS != null && oBP.U_IDHONCS.Length > 0) {
                                        moDispRows.setFormDFValue(IDH_DISPROW._CustCs, oBP.U_IDHONCS, true);
                                    }

                                    if (oBP.U_IDHOBLGT != null && oBP.U_IDHOBLGT.Length > 0) {
                                        moDispRows.setFormDFValue(IDH_DISPROW._Obligated, oBP.U_IDHOBLGT, true);
                                    }

                                    if (oBP.U_WASLIC != null && oBP.U_WASLIC.Length > 0) {
                                        setFormDFValue(IDH_DISPORD._WasLic, oBP.U_WASLIC, true);
                                        moDispRows.setFormDFValue(IDH_DISPROW._WASLIC, oBP.U_WASLIC, true);
                                    }
                                }


                                BPAddress oAddress = null;
                                if ((sCustomerAddress != null) && sCustomerAddress.Length > 0) {
                                    string sPh1 = "";
                                    oAddress = new BPAddress();
                                    if (oAddress.doGetAddress(sCustomerCode, BPAddress.ADDRTYPE.SHIPTO, sCustomerAddress, true) > 0) {
                                        doSetCustomerAddress(ref oAddress, sPh1);
                                        bSetCustomerAddress = false;
                                    } else
                                        oAddress = null;
                                }

                                if (moLookup.getParameterAsBool("DOVMSCAP", true)) {
                                    setFormDFValue(IDH_DISPORD._PCardCd, sCustomerCode);
                                    setFormDFValue(IDH_DISPORD._PCardNM, sCustomerName);

                                    if ((sCustomerAddress != null) && sCustomerAddress.Length > 0) {
                                        if (oAddress != null && oAddress.Count > 0) {
                                            doSetProducerAddress(ref oAddress, false);
                                            bSetProducerAddress = false;
                                        }
                                    }
                                }
                            }

                            if ((sWasteCarrierCode != null) && sWasteCarrierCode.Length > 0) {
                                setFormDFValue(IDH_DISPORD._CCardCd, sWasteCarrierCode);
                                setFormDFValue(IDH_DISPORD._CCardNM, sWasteCarrierName);
                            }

                            if ((sCarrierAddress != null) && sCarrierAddress.Length > 0) {
                                BPAddress oAddress = new BPAddress();
                                if (oAddress.doGetAddress(sWasteCarrierCode, BPAddress.ADDRTYPE.SHIPTO, true) > 0) {
                                    doSetCarrierAddress(ref oAddress);
                                    bSetCarrierAddress = false;
                                }
                            }

                            if ((sDisposalSiteCode != null) && sDisposalSiteCode.Length > 0) {
                                setFormDFValue(IDH_DISPORD._SCardCd, sDisposalSiteCode);
                                setFormDFValue(IDH_DISPORD._SCardNM, sDisposalSiteName);

                                if ((sDisposalAddress != null) && sDisposalAddress.Length > 0) {
                                    BPAddress oAddress = new BPAddress();
                                    if (oAddress.doGetAddress(sDisposalSiteCode, BPAddress.ADDRTYPE.SHIPTO, sDisposalAddress, true) > 0) {
                                        moDispOrdDB.doSetDisposalSiteAddress(oAddress);
                                        bSetDisposalAddress = false;
                                    }
                                }
                            }

                            //string sWasteCode = moDispRows.getFormDFValueAsString(IDH_DISPROW._WasCd);
                            ////if (moLookup.doCheckCanPurchaseWB(sWasteCode) && sJobTp.Equals(moLookup.doGetJopTypeIncoming()))
                            //if (moLookup.doCheckCanPurchaseWB(sWasteCode) && moLookup.isIncomingJob(moLookup.doGetDOContainerGroup(), sJobTp))
                            if (doCheckPurchase()) {
                                //Applying the rules here to get the Customer Address populated, the customer address is needed 
                                //to populate the Producer address in the doSetChoosenProducer function later on
                                doBusinessPartnerRules(bSetCustomerAddress, bSetCarrierAddress, bSetDisposalAddress, bSetProducerAddress);

                                doSwitchToPO(true, false, false); //2010-01-26 - doSwitchToPO(true); Added to avoid the Rules from replacing the Linked BP
                                bSetProducerAddress = false;

                                //Set this to avoid re-applying the rules later on.
                                bDoRules = false;
                            } else {
                                doSwitchToSO(false);
                            }

                            if (bDoRules)
                                doBusinessPartnerRules(bSetCustomerAddress, bSetCarrierAddress, bSetDisposalAddress, bSetProducerAddress);
                        }

                        bool bDoEnableWOLink = true;
                        string[] oItems = { "IDH_WRORD", "IDH_WRROW" };
                        EnableItems(ref oItems, bDoEnableWOLink);

                        if ((sOrigin != null) && sOrigin.Length > 0) {
                            setOrigin(sOrigin);
                        }
                        doSwitchGenButton();

                        DisposalRows.doItAll();
                    }
                }

            
                base.doUpdateFormChangedData();
                doFillTransactionCodes();
            } catch (Exception exx) {
                throw (exx);
            } finally {
                //moDispRows.DBObject.BlockChangeNotif = false;
                //DBObject.BlockChangeNotif = false;
            }


		}

        /*
         * Handels the result from the Linked Waste Dialog
         */
        public void doHandleLinkWOResult(ref object oSearch)
        {
            //string sItemCode = getDialogValue("ITEMCODE");
            //DisposalRows.setFormDFValueFromUser(IDH_DISPROW._WasCd, sItemCode);
            string sOrd = getDialogValue("ORDNO");
            string sRow = getDialogValue("ROWNO");
            string sCode = getDialogValue("DISPORD");
            string sRowNr = getDialogValue("DISPROW");

            if (sOrd.Length == 0 || sRow.Length == 0)
            {
                //setWFValue(ref oForm, "LastVehAct", "DOSEARCH");
                DisposalRows.setFormDFValueFromUser(IDH_DISPROW._WROrd, "");
                DisposalRows.setFormDFValueFromUser(IDH_DISPROW._WRRow, "");
                doSetFocus("IDH_VEHREG");
                return;
            }

            doSetVisible("IDH_ORDLK",true,-1);
            doSetVisible("IDH_ROWLK",true,-1);

            //** First try and get a DO linked to this WO-ROW
            if (Mode.Equals(Mode_FIND))
            {
                if ((sOrd.Length > 0 && sRow.Length > 0))
                {
                    if (sCode != null && sCode.Length > 0 && sRowNr != null && sRowNr.Length > 0)
                    {
                        if (getGetByHeadAndRow(sCode, sRowNr))
                        {
                            //** Check the Weights
                            double dWei1 = DisposalRows.getFormDFValueAsDouble(IDH_DISPROW._Wei1);
                            double dWei2 = DisposalRows.getFormDFValueAsDouble(IDH_DISPROW._Wei2);
                            if (dWei1 == 0)
                            {
                                doSetFocus("IDH_WEI1");
                                setTitle("Disposal Order - First Weigh");
                            }
                            else if (dWei2 == 0)
                            {
                                doSetFocus("IDH_WEI2");
                                setTitle("Disposal Order - Second Weigh");
                            }
                            return;
                        }
                    }
                }
            }
            sCode = "";
            sRowNr = "";

            if ((sOrd.Length > 0 && sRow.Length > 0))
            {
                if (moLookup.getParameterAsBool("DOLKWA", false))
                {
                    if (Messages.INSTANCE.doResourceMessageYNAsk("DODLWUD", null, 1) == 2)
                    {
                        return;
                    }
                }

                //We have to keep the Job Type
                string sJobType = DisposalRows.DBObject.U_JobTp;
                if (Mode.Equals(Mode_FIND)) {
                    Mode = Mode_ADD;
                }

                //setUFValue(oForm, "IDH_TARWEI", 0);
                //setUFValue(oForm, "IDH_TRLTar", 0);

                string sContainerType = getDialogValue("CONTP");
                string sContainerCode = getDialogValue("CONCD");
                string sContainerDesc = getDialogValue("CONDSC");
                string sWasteCode = getDialogValue("WASTCD");
                string sWasteDesc = getDialogValue("WASTDE");
                string sWasteAlt = getDialogValue("ALTWSTDE");

                string sCustomerCode = getDialogValue("CUSTCD");
                string sCustomerName = getDialogValue("CUSTNM");
                string sCarrCode = getDialogValue("CARRCD");
                string sHeadCarrCode = getDialogValue("HDCARRCD");
                string sCarrName = getDialogValue("CARRNM");
                string sPCode = getDialogValue("PCD");
                string sPName = getDialogValue("PNM");
                string sSCode = getDialogValue("SCD");
                string sSName = getDialogValue("SNM");
                string sOrigin = getDialogValue("ORIGIN");

                string sWastTNN = getDialogValue("WASTTNN");
                //string sHazWCNN = getDialogValue("HAZWCNN")
                string sSiteRef = getDialogValue("SITEREF");
                string sExtWeig = getDialogValue("EXTWEIG");
                string sComNum = getDialogValue("CONNUM");

                string sLorrDesc = getDialogValue("LORRYDESC");
                string sDriver = getDialogValue("DRIVER");
                string sObligated = getDialogValue("OBLIGATED");
                string sCompliance = getDialogValue("COMSCHEME");

                string sLorry = getDialogValue("LORRY");
                string sLorrTest = getDialogValue("U_Lorry");
                string sLorryCode = getDialogValue("LORRYCD");

                string sAUOM = getDialogValue("AUOM");
                string sUOM = getDialogValue("UOM");
                string sPUOM = getDialogValue("PUOM");
                string sProUOM = getDialogValue("PROUOM");

                string sWORJOPTP = getDialogValue("JOBTP");

                double dAUOMQt = getDialogValueAsDouble("AUOMQT");
                double dPAUOMQt = getDialogValueAsDouble("PAUOMQT");
                double dTAUOMQt = getDialogValueAsDouble("TAUOMQT");
                double dEXPLdWGT = getDialogValueAsDouble("EXPLdWGT");

                double dTipWgt = getDialogValueAsDouble("TIPWGT");
                double dProWgt = getDialogValueAsDouble("PROWGT");
                double dCstWgt = getDialogValueAsDouble("QTY");
                
                string  sUseWeight = getDialogValue( "USEWGT");
                string  sMANPRC = getDialogValue("MANPRC");

                double dDisposalCharge = getDialogValueAsDouble("DISPCHRG");
                double dDisposalCost = getDialogValueAsDouble("DISPCST");
                double dHaulCost = getDialogValueAsDouble("HAULCST");
                double dProCost = getDialogValueAsDouble("PRODCST");
                
                DisposalRows.DBObject.BlockChangeNotif = true;

                try {
                    if ((sCustomerCode != null) && sCustomerCode.Length > 0) {
                        double dChargeTotal = moDispOrdDB.getUnCommittedRowTotals();
                        if (!moLookup.doCheckCredit(sCustomerCode, dChargeTotal, false, false))
                            return;

                        if (sCompliance == null || sCompliance.Length == 0) {
                            sCompliance = moLookup.getBPCs(sCustomerCode);
                        }
                        DisposalRows.setFormDFValue(IDH_DISPROW._CustCs, sCompliance);
                    }

                    if ((sLorrTest == null || sLorrTest.Length == 0) && sLorry != null && sLorry.Length > 0) {
                        DisposalRows.setFormDFValue(IDH_DISPROW._Lorry, sLorry, false);
                        DisposalRows.setFormDFValue(IDH_DISPROW._LorryCd, sLorryCode, false);
                    }

                    DisposalRows.setFormDFValue(IDH_DISPROW._VehTyp, sLorrDesc);
                    DisposalRows.setFormDFValue(IDH_DISPROW._Driver, sDriver);

                    DisposalRows.setFormDFValue(IDH_DISPROW._WROrd, sOrd);
                    DisposalRows.setFormDFValue(IDH_DISPROW._WRRow, sRow, false);

                    if (string.IsNullOrEmpty(msLastLinkWO) || !msLastLinkWO.Equals(sOrd)) {
                        msLastLinkWO = sOrd;

                        moLastLinkWORs = new ArrayList();
                        moLastLinkWORs.Add(sRow);
                    } else {
                        moLastLinkWORs.Add(sRow);
                    }
                    //msLastLinkWO = sOrd;
                    //msLastLinkWOR = sRow;

                    if (!string.IsNullOrEmpty(sOrd)) {
                        doSetVisible("IDH_WOCFL", true, 0);
                    }

                    //*** CUSTOMER
                    if (sCustomerName.Length > 0) {
                        DisposalRows.setFormDFValue(IDH_DISPROW._CustCd, sCustomerCode, false);
                        DisposalRows.setFormDFValue(IDH_DISPROW._CustNm, sCustomerName, false);

                        setFormDFValue(IDH_DISPORD._CardCd, sCustomerCode);
                        setFormDFValue(IDH_DISPORD._CardNM, sCustomerName);

                        setFormDFValue(IDH_DISPORD._Contact, getDialogValue("CONTACT"));
                        setFormDFValue(IDH_DISPORD._Address, getDialogValue("ADDRESS"));
                        setFormDFValue(IDH_DISPORD._Street, getDialogValue("STREET"));
                        setFormDFValue(IDH_DISPORD._Block, getDialogValue("BLOCK"));
                        setFormDFValue(IDH_DISPORD._County, getDialogValue("COUNTY"));
                        setFormDFValue(IDH_DISPORD._City, getDialogValue("CITY"));
                        setFormDFValue(IDH_DISPORD._ZpCd, getDialogValue("ZPCD"));
                        setFormDFValue(IDH_DISPORD._Phone1, getDialogValue("PHONE1"));
                        setFormDFValue(IDH_DISPORD._SiteTl, getDialogValue("SITETL"));
                        setFormDFValue(IDH_DISPORD._SteId, getDialogValue("STEID"));
                        setFormDFValue(IDH_DISPORD._Route, getDialogValue("ROUTE"));
                        setFormDFValue(IDH_DISPORD._Seq, getDialogValue("SEQ"));

                        setFormDFValue(IDH_DISPORD._PremCd, getDialogValue("PRECD"));
                        setFormDFValue(IDH_DISPORD._SiteLic, getDialogValue("SITELIC"));
                    }

                    //*** CARRIER
                    if (sCarrCode.Length > 0) {
                        DisposalRows.setFormDFValue(IDH_DISPROW._CarrCd, sCarrCode, false);
                        DisposalRows.setFormDFValue(IDH_DISPROW._CarrNm, sCarrName, false);

                        setFormDFValue(IDH_DISPORD._CCardCd, sCarrCode, false);
                        setFormDFValue(IDH_DISPORD._CCardNM, sCarrName, false);

                        if (sHeadCarrCode.Equals(sCarrCode) == false) {
                            BPAddress oAddress = new BPAddress();
                            if (oAddress.doGetAddress(sCarrCode, BPAddress.ADDRTYPE.SHIPTO, true) > 0) {
                                doSetCarrierAddress(ref oAddress);
                            }
                        } else {
                            setFormDFValue(IDH_DISPORD._CContact, getDialogValue("CCONTACT"));
                            setFormDFValue(IDH_DISPORD._CAddress, getDialogValue("CADDRESS"));
                            setFormDFValue(IDH_DISPORD._CStreet, getDialogValue("CSTREET"));
                            setFormDFValue(IDH_DISPORD._CBlock, getDialogValue("CBLOCK"));
                            setFormDFValue(IDH_DISPORD._CCity, getDialogValue("CCITY"));
                            setFormDFValue(IDH_DISPORD._CZpCd, getDialogValue("CZPCD"));
                            setFormDFValue(IDH_DISPORD._CPhone1, getDialogValue("CPHONE1"));
                        }
                    }
                    setFormDFValue(IDH_DISPORD._SupRef, getDialogValue("CREF"));

                    //*** Site
                    if (sSCode.Length > 0) {
                        DisposalRows.setFormDFValue(IDH_DISPROW._Tip, sSCode);
                        DisposalRows.setFormDFValue(IDH_DISPROW._TipNm, sSName);

                        setFormDFValue(IDH_DISPORD._SCardCd, sSCode);
                        setFormDFValue(IDH_DISPORD._SCardNM, sSName);
                        setFormDFValue(IDH_DISPORD._SContact, getDialogValue("SCONTACT"));
                        setFormDFValue(IDH_DISPORD._SAddress, getDialogValue("SADDRESS"));
                        setFormDFValue(IDH_DISPORD._SStreet, getDialogValue("SSTREET"));
                        setFormDFValue(IDH_DISPORD._SBlock, getDialogValue("SBLOCK"));
                        setFormDFValue(IDH_DISPORD._SCity, getDialogValue("SCITY"));
                        setFormDFValue(IDH_DISPORD._SZpCd, getDialogValue("SZPCD"));
                        setFormDFValue(IDH_DISPORD._SPhone1, getDialogValue("SPHONE1"));
                    }

                    DisposalRows.setFormDFValue(IDH_DISPROW._ItemCd, sContainerCode, false);
                    DisposalRows.setFormDFValue(IDH_DISPROW._ItemDsc, sContainerDesc, false);
                    DisposalRows.setFormDFValue(IDH_DISPROW._WasCd, sWasteCode, false);
                    DisposalRows.setFormDFValue(IDH_DISPROW._WasDsc, sWasteDesc, false);

                    doFillAlternativeDescription(sWasteCode);
                    DisposalRows.setFormDFValue(IDH_DISPROW._AltWasDsc, sWasteAlt, false);
                    
                    DisposalRows.setFormDFValue(IDH_DISPROW._ItmGrp, sContainerType, false);

                    if (sObligated != null && sObligated.ToUpper().Equals("YES"))
                        DisposalRows.setFormDFValue(IDH_DISPROW._Obligated, "Yes");
                    else
                        DisposalRows.setFormDFValue(IDH_DISPROW._Obligated, "No");

                    DisposalRows.setFormDFValue(IDH_DISPROW._WastTNN, sWastTNN);
                    DisposalRows.setFormDFValue(IDH_DISPROW._SiteRef, sSiteRef);
                    DisposalRows.setFormDFValue(IDH_DISPROW._ExtWeig, sExtWeig);

                    
                    if ( sUseWeight == null || sUseWeight.Length == 0 ) {
                        DisposalRows.setFormDFValue(IDH_DISPROW._UseWgt, "1");
                        //if (moWinForm != null) {
                        //    RadioButton oUseWeight = (RadioButton)getWinControl("IDH_USERE");
                        //    oUseWeight.Checked = true;
                        //}
                    } else {
                        DisposalRows.setFormDFValue(IDH_DISPROW._UseWgt, sUseWeight);
                        //if (moWinForm != null) {
                        //    if (sUseWeight == "2") {
                        //        RadioButton oUseAUOM = (RadioButton)getWinControl("IDH_USEAU");
                        //        oUseAUOM.Checked = true;
                        //    } else {
                        //        RadioButton oUseWeight = (RadioButton)getWinControl("IDH_USERE");
                        //        oUseWeight.Checked = true;
                        //    }
                        //}
                    }

                    //Set The Additional UOM Weights
                    DisposalRows.setFormDFValue(IDH_DISPROW._AUOM, sAUOM);
                    DisposalRows.setFormDFValue(IDH_DISPROW._AUOMQt, dAUOMQt);
                    DisposalRows.setFormDFValue(IDH_DISPROW._PAUOMQt, dPAUOMQt);
                    DisposalRows.setFormDFValue(IDH_DISPROW._TAUOMQt, dTAUOMQt);
                                                            
                    //Set the UOM's
                    DisposalRows.setFormDFValue(IDH_DISPROW._UOM, sUOM);
                    DisposalRows.setFormDFValue(IDH_DISPROW._PUOM, sPUOM);
                    DisposalRows.setFormDFValue(IDH_DISPROW._ProUOM, sProUOM);
                    
                    //Set The Weights
                    DisposalRows.setFormDFValue(IDH_DISPROW._TipWgt, dTipWgt);
                    DisposalRows.setFormDFValue(IDH_DISPROW._ProWgt, dProWgt);
                    DisposalRows.setFormDFValue(IDH_DISPROW._CstWgt, dCstWgt);

                    DisposalRows.setFormDFValue(IDH_DISPROW._ExpLdWgt, dEXPLdWGT);

                    DisposalRows.setFormDFValue(IDH_DISPROW._TCharge, dDisposalCharge);
                    DisposalRows.setFormDFValue(IDH_DISPROW._TipCost, dDisposalCost);
                    DisposalRows.setFormDFValue(IDH_DISPROW._OrdCost, dHaulCost);
                    DisposalRows.setFormDFValue(IDH_DISPROW._PCost, dProCost);

                    DisposalRows.setFormDFValue(IDH_DISPROW._ConNum, sComNum);

                    setOrigin(sOrigin);
                    //moDispRows.doSwitchAccountingCheckBox("IDH_FOC", true, true);
                    moDispRows.doCheckAccountingCheckBox("IDH_FOC", true);
                  
                    //*** Producer
                    if (sPCode.Length > 0) {
                        setFormDFValue(IDH_DISPORD._PCardCd, sPCode);
                        setFormDFValue(IDH_DISPORD._PCardNM, sPName);
                        setFormDFValue(IDH_DISPORD._PContact, getDialogValue("PCONTACT"));
                        setFormDFValue(IDH_DISPORD._PAddress, getDialogValue("PADDRESS"));
                        setFormDFValue(IDH_DISPORD._PStreet, getDialogValue("PSTREET"));
                        setFormDFValue(IDH_DISPORD._PBlock, getDialogValue("PBLOCK"));
                        setFormDFValue(IDH_DISPORD._PCity, getDialogValue("PCITY"));
                        setFormDFValue(IDH_DISPORD._PZpCd, getDialogValue("PZPCD"));
                        setFormDFValue(IDH_DISPORD._PPhone1, getDialogValue("PPHONE1"));
                    }
                    //if (moLookup.doCheckCanPurchaseWB(DisposalRows.getFormDFValueAsString(IDH_DISPROW._WasCd)) 
                    //      && moLookup.isIncomingJob(moLookup.doGetDOContainerGroup(), DisposalRows.getFormDFValueAsString(IDH_DISPROW._JobTp)))
                    ////    && DisposalRows.getFormDFValueAsString(IDH_DISPROW._JobTp).Equals(moLookup.doGetJopTypeIncoming()))
                    
                    
                    string sTrnCode = getDialogValue("TRNCODE");
                    string sTrnDescp = string.Empty;
                    if ( !string.IsNullOrWhiteSpace(sTrnCode) ) { 
                        DisposalRows.setFormDFValue(IDH_DISPROW._TrnCode, sTrnCode);
                        
                        IDH_TRANCD oTransCode = new IDH_TRANCD();
                        if ( oTransCode.getByKey(sTrnCode) ) {
                            sTrnDescp = oTransCode.U_Process;
                        }

                        string sSpInst = DBObject.U_SpInst;
                        if ( !string.IsNullOrWhiteSpace(sSpInst) ) {
                            if ( sSpInst.Contains("#") ) {
                                sSpInst = sSpInst.Replace(sSpInst.Substring(sSpInst.IndexOf("#"), sSpInst.Length - sSpInst.IndexOf("#")), String.Empty);
                                sSpInst = sSpInst.Trim().Trim('-');
                            }
                            sSpInst = (sSpInst.Length != 0 ? sSpInst + " - ": sSpInst) + "#WOR Transaction Code/Desp: " + sTrnCode + "-" + sTrnDescp + "#";
                        } else {
                            sSpInst = "#WOR Transaction Code/Desp: " + sTrnCode + "-" + sTrnDescp + "#";
                        }
                        setFormDFValue(IDH_DISPORD._SpInst, sSpInst);
                    }
                    
                    EnableItem("uBC_TRNCd", false);
                                        
                    if (doCheckPurchase()) {
                        //As requested by MDJ - 21Dec 2009 - doSwitchToPO(true);
                        doSwitchToPO(true, false, false);
                    } else {
                        doSwitchToSO(false);
                    }
                    doSwitchGenButton();

                    if (!string.IsNullOrWhiteSpace(sMANPRC)) {
                        DisposalRows.setFormDFValue(IDH_DISPROW._MANPRC, sMANPRC);
                    }

                    setFormDFValue(IDH_DISPORD._RTIMEF, getDialogValue("RTIMEF"));
                    setFormDFValue(IDH_DISPORD._RTIMET, getDialogValue("RTIMET"));

                    string sCustRef = getDialogValue("CUSTREF");
                    if ((sCustRef != null)
                        && sCustRef.Length > 0
                        && !moLookup.getParameterAsBool("MDREAE", true)) {
                        EnableItem("IDH_CUSCRF", false);
                    } else {
                        EnableItem("IDH_CUSCRF", true);
                    }
                    setFormDFValue(IDH_DISPORD._CustRef, sCustRef);

                    if ( !string.IsNullOrWhiteSpace(sWORJOPTP) ) {
                        //string sNewJopbType = "";
                        if ( Config.INSTANCE.isOutgoingJob(sContainerType, sWORJOPTP) ) {
                            if ( !Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) )
                                sJobType = Config.INSTANCE.doGetJopTypeOutgoing();
                        } else {
                            if (!Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType))
                                sJobType = Config.INSTANCE.doGetJopTypeIncoming();
                        }
                    }
                    DisposalRows.setFormDFValue(IDH_DISPROW._JobTp, sJobType);

                } catch (Exception e) {
                    throw e;
                } finally {
                    DisposalRows.DBObject.BlockChangeNotif = false;
                }
                //DisposalRows.doItAll();
            }
            base.doUpdateFormChangedData();
        }

		/*
		 * Handels the result from the BP Search Dialog
		 */
		public void doHandleAddressResult(ref object oSearch, string sTarget ){
			if ( oSearch is SearchBase ) 
				SearchDialog = oSearch as SearchBase;
		
			BPAddress oAddress = new BPAddress();
			
			oAddress.AddressType = getDialogValue("ADRESTYPE");
			oAddress.Address = getDialogValue("ADDRESS");
			oAddress.Street = getDialogValue("STREET");
			oAddress.Block = getDialogValue("BLOCK");
			oAddress.ZipCode = getDialogValue("ZIPCODE");
			oAddress.City = getDialogValue("CITY");
			oAddress.Country = getDialogValue("COUNTY");
			oAddress.U_ISBACCDF = getDialogValue("ISBACCDF");
			
			try {
				oAddress.U_ISBACCTF = getDialogValueAsInt("ISBACCTF");
				oAddress.U_ISBACCTT = getDialogValueAsInt("ISBACCTT");
			}
			catch (Exception ){}
			
			oAddress.U_ISBACCRA = getDialogValue("ISBACCRA");
			oAddress.U_ISBACCRV = getDialogValue("ISBACCRV");
			oAddress.U_ISBCUSTRN = getDialogValue("ISBCUSTRN");
			oAddress.U_TEL1 = getDialogValue("TEL1");
			oAddress.U_ROUTE = getDialogValue("ROUTE");
			oAddress.U_IDHSEQ = getDialogValueAsInt("SEQ");
			oAddress.U_IDHACN = getDialogValue("CONA");
			oAddress.County = getDialogValue("COUNTRY");
			oAddress.U_IDHPCD = getDialogValue("PRECD");
			oAddress.U_Origin = getDialogValue("ORIGIN");
			oAddress.U_IDHSLCNO = getDialogValue("SITELIC");
			oAddress.U_IDHSteId = getDialogValue("STEID");
			oAddress.U_IDHFAX = getDialogValue("FAX");
			oAddress.U_IDHEMAIL = getDialogValue("EMAIL");
			oAddress.U_IDHSTRNO = getDialogValue("STRNO");         

			//oGridControl.doAddListField("CardCode", "Customer", false, -1, null, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
			
			if ( sTarget.Equals( BP_CUSTOMER ) ) {
            	doSetCustomerAddress( ref oAddress, oAddress.U_TEL1);
			} 
			else if ( sTarget.Equals( BP_CARRIER ) ) {
            	doSetCarrierAddress( ref oAddress );
			} 
			else if ( sTarget.Equals( BP_PRODUCER ) ) {
            	doSetProducerAddress( ref oAddress, true );
			}
			else if ( sTarget.Equals( BP_DISPOSALSITE ) ) {
                moDispOrdDB.doSetDisposalSiteAddress(oAddress);
                base.doUpdateFormChangedData();
				//moDispRows.doUpdateFormChangedData();
			}
			
			oAddress = null;

            //base.doUpdateFormChangedData();
		}		
		/*
		 * Handels the result from the Waste Item Dialog
		 */
		public void doHandleWasteItemResult(ref object oSearch ){
            string sWasteCode = getDialogValue("ITEMCODE");
            string sWasteDesc = getDialogValue("ITEMNAME");
            string sUOM = getDialogValue("UOM");
            DisposalRows.setFormDFValueFromUser(IDH_DISPROW._WasCd, sWasteCode );
			DisposalRows.setFormDFValueFromUser(IDH_DISPROW._WasDsc, sWasteDesc ); //, false, true);
			DisposalRows.setFormDFValueFromUser(IDH_DISPROW._AUOM, sUOM );

			string sCWBB = getDialogValue("CWBB");
			if ((sCWBB == "Y" || sCWBB == "y") && 
			    //moLookup.isIncomingJob(moLookup.doGetDOContainerGroup(), DisposalRows.getFormDFValueAsString(IDH_DISPROW._JobTp)) ){
                doCheckPurchase() ) {
//			    DisposalRows.getFormDFValueAsString(IDH_DISPROW._JobTp).Equals( moLookup.doGetJopTypeIncoming())) { //, true).Equals( moLookup.doGetJopTypeIncoming())) {
				doSwitchToPO(true, false, false); //Set to false to block the Address Lookup of the Producer

				string sPUOM = DisposalRows.getFormDFValueAsStringNZ(IDH_DISPROW._PUOM);
				if (sPUOM.Length == 0) {
					sPUOM = getDialogValue("PUOM");
					if ((sPUOM != null) && sPUOM.Length > 0) {
						DisposalRows.setFormDFValue(IDH_DISPROW._PUOM, sPUOM);
					}
				}
			}
			else {
				doSwitchToSO(true);
			}
            doSwitchGenButton();

			DisposalRows.doItAll();

            doFillAlternativeDescription(sWasteCode);
		}
        
		/*
		 * Handels the result from the Item Search Dialog
		 */
		public void doHandleItemResult(ref object oSearch ){
			DisposalRows.setFormDFValueFromUser(IDH_DISPROW._ItemCd, getDialogValue("ITEMCODE")); //, False, True)
            DisposalRows.setFormDFValueFromUser(IDH_DISPROW._ItemDsc, getDialogValue("ITEMNAME"));

			DisposalRows.doItAll();
		}		
		
		/*
		 * Handels Origin result
		 */
		public void doHandleOriginResult( ref object oSearch ) {
			setOrigin(getDialogValue("IDH_ORCODE"));
		}
		
		/*
		 * Handels the result from the BP Search Dialog
		 */
		public bool doHandleBPResult(ref object oSearch, string sTarget ){
			if ( oSearch is SearchBase ) 
				SearchDialog = oSearch as SearchBase;
			
			if ( sTarget.Equals( BP_COMPLIANCE ) ) {
            	moDispRows.setFormDFValueFromUser( IDH_DISPROW._CustCs, getDialogValue("CARDCODE") );
            }
			else {
				BP oBP = new BP();
				
	            oBP.CardCode = getDialogValue("CARDCODE");
			    oBP.CardName = getDialogValue("CARDNAME");
	            oBP.Phone1 = getDialogValue("PHONE1");
	            oBP.ContactPerson = getDialogValue("CNTCTPRSN");

                if (sTarget.Equals(BP_CUSTOMER) || sTarget.Equals(BP_CUSTOMERNAME)) {
	            	oBP.U_IDHOBLGT = getDialogValue("IDHOBLGT");
	            	oBP.U_IDHONCS = getDialogValue("IDHONCS");
		            oBP.CreditLine = getDialogValueAsDouble("CREDITLINE");
	            	oBP.CurrentAccountBalance = getDialogValueAsDouble("BALANCE");
	
	            	try {
			        	oBP.RemainBal = getDialogValueAsDouble("CRREMAIN");
			            oBP.OrdersBal = getDialogValueAsDouble("ORDERS");
			            oBP.TBalance = getDialogValueAsDouble("TBALANCE");            	
			        	oBP.Deviation = getDialogValueAsDouble("DEVIATION");
						oBP.WRBalance = getDialogValueAsDouble("WRORDERS");		        	
	            	}
	            	catch ( Exception ) {}
	            	
	   	            oBP.U_IDHUOM = getDialogValue("IDHUOM");
	            	oBP.FrozenFor = getDialogValue("FROZEN");
		            oBP.FrozenComm = getDialogValue("FROZENC");
	            
	//	            oBP.FrozenFrom = (DateTime)getDialogValueRAW("FROZENF");
	//	            oBP.FrozenTo = (DateTime)getDialogValueRAW("FROZENT");
		            
					object oVal = getDialogValueRAW("FROZENF");
					if ( oVal != null && !(oVal is DBNull) )
						oBP.FrozenFrom = Conversions.ToDateTime(oVal);
					
					oVal = getDialogValueRAW("FROZENT");
					if ( oVal != null && !(oVal is DBNull) )
						oBP.FrozenTo = Conversions.ToDateTime(oVal);
	
				    oBP.CardType = getDialogValue("CARDTYPE");
				    oBP.GroupCode = getDialogValue("GROUPCODE");
				    oBP.GroupName = getDialogValue("GROUPNAME");
		            oBP.U_WASLIC = getDialogValue("WASLIC");		    
		            oBP.U_IDHICL = getDialogValue("IDHICL");
		            oBP.U_IDHBRAN = getDialogValue("BRANCH");
		            oBP.U_IDHLBPC = getDialogValue("IDHLBPC");
		            oBP.U_IDHLBPN = getDialogValue("IDHLBPN");
	
		            if ( !doSetChoosenCustomer( ref oBP, true) ){
		            	return false;
		            }
		            else {
                        doMarkFormFieldSetFromUser("IDH_CUST");
                        doMarkFormFieldSetFromUser("IDH_CUSTNM");
                        doMarkFormFieldSetFromUser("IDH_OBLED");
		            }
				}
                else if (sTarget.Equals(BP_CARRIER) || sTarget.Equals(BP_CARRIERNAME)) {
                    doMarkFormFieldSetFromUser("IDH_CARRIE");
                    doMarkFormFieldSetFromUser("IDH_CARNAM");

                    if ( !doSetChoosenCarrier( ref oBP, true ) )
                		return false;
				} else if (sTarget.Equals(BP_PRODUCER) || sTarget.Equals(BP_PRODUCERNAME) || sTarget.Equals(BP_BUYFROM) || sTarget.Equals(BP_BUYFROMNAME)) {
                    doMarkFormFieldSetFromUser("IDH_WPRODU");
                    doMarkFormFieldSetFromUser("IDH_PROCD");
                    doMarkFormFieldSetFromUser("IDH_WNAM");
                    doMarkFormFieldSetFromUser("IDH_PRONM");

                    if ( !doSetChoosenProducer( ref oBP, true ) )
                		return false;

                    
				}
                else if (sTarget.Equals(BP_DISPOSALSITE) || sTarget.Equals(BP_DISPOSALSITENAME) || sTarget.Equals(BP_DISPOSALSITE2) || sTarget.Equals(BP_DISPOSALSITENAME2) ) {
                    doMarkFormFieldSetFromUser("IDH_DISSIT");
                    doMarkFormFieldSetFromUser("IDH_DISNAM");
                    doMarkFormFieldSetFromUser("IDH_DISPCD");
                    doMarkFormFieldSetFromUser("IDH_DISNAM");
                    
                	if ( !doSetChoosenSite( ref oBP, true ) )
                		return false;
				}
	
				oBP = null;
			}
            //base.doUpdateFormChangedData(); - don't need this if the setFormDFValue used
			return true;
		}

        public bool doSetChoosenCustomer(ref BP oBP, bool bDoBPRules) {
            //			string sCardCode = oBP.CardCode;
            //			string sName = oBP.CardName;
            //			string sPhone = oBP.Phone1;
            //			string sContactPerson = oBP.ContactPerson;
            double dCreditLimit = 0;
            double dCreditRemain = 0;
            double dDeviationPrc = 0;
            double dTBalance = 0;
            string sFrozen = "N";
            string sFrozenComm = null;
            //			string sUOM = null;
            //			string sCustCs = null;
            //			string sObligated = null;
            string sIgnoreCheck = null;

            System.DateTime dFrozenFrom = default(System.DateTime);
            System.DateTime dFrozenTo = default(System.DateTime);

            try {
                dCreditLimit = oBP.CreditLine;
                dCreditRemain = oBP.RemainBal;
                dDeviationPrc = oBP.Deviation;
                dTBalance = oBP.TBalance;

                //				sUOM = getDialogValue("IDHUOM");
                //				sCustCs = getDialogValue("IDHONCS");
                //				sObligated = getDialogValue("IDHOBLGT");
                sIgnoreCheck = oBP.U_IDHICL;

                sFrozen = oBP.FrozenFor;
                sFrozenComm = oBP.FrozenComm;
                dFrozenFrom = oBP.FrozenFrom;
                dFrozenTo = oBP.FrozenTo;
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError( "Exception: " + ex.ToString(), "");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
                return false;
            }

            try {
                if (oBP.CardCode != null && oBP.CardCode.Length > 0) {
                    double dChargeTotal = 0;
                    if (!(sIgnoreCheck == "Y")) {
                        dChargeTotal = moDispOrdDB.getUnCommittedRowTotals();
                        bool bBlockOnFail = Config.INSTANCE.getParameterAsBool("DOBCCL", true);
                        //if (Config.INSTANCE.doCheckCredit2(oBP.CardCode, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, dChargeTotal, sFrozen, sFrozenComm, false, bBlockOnFail, null, dFrozenFrom, dFrozenTo) == false) {
                        //    return false;
                        //}
                        if (Config.INSTANCE.doCheckCredit2(oBP.CardCode, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, dChargeTotal, false, bBlockOnFail, null) == false) {
                            return false;
                        }

                    }

                    moDispRows.DBObject.AdditionalPricesLookups = false;

                    moDispRows.DBObject.AdditionalPricesLookups = true;
                    setFormDFValue(IDH_DISPORD._CardCd, oBP.CardCode, true);
                    setFormDFValue(IDH_DISPORD._CardNM, oBP.CardName, true);
                    setFormDFValue(IDH_DISPORD._Contact, oBP.ContactPerson, true);
                    moDispRows.setFormDFValue(IDH_DISPROW._Obligated, oBP.Obligated, true);
                    setFormDFValue(IDH_DISPORD._WasLic, oBP.U_WASLIC);

                    if (oBP.U_IDHONCS != null && oBP.U_IDHONCS.Length > 0 &&
                            (moDispRows.getFormDFValueAsStringNZ(IDH_DISPROW._CustCd).Length == 0 || wasFormFieldSetWithCode("IDH_ONCS"))) {
                        moDispRows.setFormDFValue(IDH_DISPROW._CustCd, oBP.U_IDHONCS, true);
                    }

                    string sWOLink = moDispRows.getFormDFValueAsString(IDH_DISPROW._WROrd);
                    if ((sWOLink != null) && sWOLink.Length > 0) {
                        BPAddress oAddress = new BPAddress();
                        if (oAddress.doGetAddress(oBP.CardCode, BPAddress.ADDRTYPE.SHIPTO, true) > 0) {
                            doSetCustomerAddress(ref oAddress, oBP.Phone1);
                        }
                    } else {
                        if (bDoBPRules == true) {
                            doBusinessPartnerRules(true, false, false, false);
                        }

                        if (MMode == Config.MarketingMode.PO) {
                            doSynchPOAddress();
                        }
                    }

                    //Set the producer to the Customers Linked Supplier or clear it
                    if (MMode == Config.MarketingMode.PO) {
                        if (oBP.U_IDHLBPC.Length > 0) {
                            setFormDFValue(IDH_DISPORD._PCardCd, oBP.U_IDHLBPC);
                            setFormDFValue(IDH_DISPORD._PCardNM, oBP.U_IDHLBPN);
                        } else {
                            //if ( !wasSetByUser( "IDH_PROCD" )) {
                            setFormDFValue(IDH_DISPORD._PCardCd, "");
                            setFormDFValue(IDH_DISPORD._PCardNM, "");
                            //}
                        }
                    }

                    if (bDoBPRules)
                        DisposalRows.doItAll();
                    else
                        DisposalRows.doItAll_Customer();

                    //string sCardCode = oBP.CardCode;
                    //if (DBObject.U_PCardCd != null && DBObject.U_PCardCd.Length > 0)
                    //    sCardCode = sCardCode + ',' + DBObject.U_PCardCd;
                    //doFillTransactionCodes(sCardCode);

                    doFillTransactionCodes();
                    setCustomerCurrency();

                    return true;
                }
            } catch (Exception) {

            } finally {
                if (!moDispRows.DBObject.AdditionalPricesLookups) {
                    moDispRows.DBObject.AdditionalPricesLookups = true;
                    moDispRows.DBObject.AdditionalExpenses.doCalculateLinesPrices();
                }
            }
            return false;
        }

        protected bool doSetChoosenCarrier( ref BP oBP, bool bDoBPRules )	{
			setFormDFValue(IDH_DISPORD._CCardCd, oBP.CardCode);
			setFormDFValue(IDH_DISPORD._CCardNM,  oBP.CardName);
			setFormDFValue(IDH_DISPORD._CPhone1, oBP.Phone1);
			setFormDFValue(IDH_DISPORD._CContact, oBP.ContactPerson);

			string sWOLink = moDispRows.getFormDFValueAsString(IDH_DISPROW._WROrd);
			if ((sWOLink != null) && sWOLink.Length > 0) {
				BPAddress oAddress = new BPAddress();
				if ( oAddress.doGetAddress(oBP.CardCode, BPAddress.ADDRTYPE.SHIPTO, true ) > 0 ) {
					doSetCarrierAddress(ref oAddress);
				}
			}
			else {
				if (bDoBPRules) {
					doBusinessPartnerRules(false, true, false, false);
				}
			}
			setUFValue( "IDH_LICREG", oBP.U_WASLIC );

            if (bDoBPRules)
                DisposalRows.doItAll();
            else
                DisposalRows.doItAll_Carrier();

            setCarrierCurrency();

			return true;
		}
		
		protected bool doSetChoosenSite( ref BP oBP, bool bDoBPRules)	{
			setFormDFValue(IDH_DISPORD._SCardCd, oBP.CardCode);
			setFormDFValue(IDH_DISPORD._SCardNM, oBP.CardName);
			setFormDFValue(IDH_DISPORD._SPhone1, oBP.Phone1);
			setFormDFValue(IDH_DISPORD._SContact, oBP.ContactPerson);

			string sWOLink = moDispRows.getFormDFValueAsString(IDH_DISPROW._WROrd);
			if ((sWOLink != null) && sWOLink.Length > 0) {
				BPAddress oAddress = new BPAddress();
                if (oAddress.doGetAddress(oBP.CardCode, BPAddress.ADDRTYPE.SHIPTO, true) > 0) {
                    moDispOrdDB.doSetDisposalSiteAddress(oAddress);
				}				
			}
			else {
				if (bDoBPRules) {
					mbFindBranchDisposalSite = false;
					doBusinessPartnerRules( false, false, true, false);
				}
			}

            if (bDoBPRules)
                DisposalRows.doItAll();
            else
                DisposalRows.doItAll_DisposalSite();

            setDisposalSiteCurrency();

			return true;
		}		

		public bool doSetChoosenProducer( ref BP oBP, bool bDoBPRulesOnProd ) {
			return doSetChoosenProducer( ref oBP, bDoBPRulesOnProd, false );
		}
		/**
		 * Use the bLinkedCustomer when working with Linked WO's with WC that you 
		 * can buy over the WB and the customer is linked to a supplier
		 */
		public bool doSetChoosenProducer( ref BP oBP, bool bDoBPRulesOnProd, bool bLinkedCustomer ) {
			setFormDFValue(IDH_DISPORD._PCardCd, oBP.CardCode);
			setFormDFValue(IDH_DISPORD._PCardNM, oBP.CardName);
			setFormDFValue(IDH_DISPORD._PPhone1, oBP.Phone1);
			setFormDFValue(IDH_DISPORD._PContact, oBP.ContactPerson);
			
			string sCustomer = getFormDFValueAsStringNZ(IDH_DISPORD._CardCd);

			string sWOLink = moDispRows.getFormDFValueAsString(IDH_DISPROW._WROrd);
			if ((sWOLink != null) && sWOLink.Length > 0) {
//				if ( bDoBPRules ) {
//					doBusinessPartnerRules( false, false, false, true);
//				}
//				else if ( bLinkedCustomer && sCustomer.Length > 0 ){
//					setFormDFValue(IDH_DISPORD._PAddress, getFormDFValueAsStringNZ(IDH_DISPORD._Address) );
//                	setFormDFValue(IDH_DISPORD._PStreet, getFormDFValueAsStringNZ(IDH_DISPORD._Street) );
//                	setFormDFValue(IDH_DISPORD._PBlock, getFormDFValueAsStringNZ(IDH_DISPORD._Block) );
//                	setFormDFValue(IDH_DISPORD._PZpCd, getFormDFValueAsStringNZ(IDH_DISPORD._ZpCd) );
//                	setFormDFValue(IDH_DISPORD._PCity, getFormDFValueAsStringNZ(IDH_DISPORD._City) );
//				}
//				else {
//					BPAddress oAddress = new BPAddress();
//					if ( oAddress.doGetAddress(oBP.CardCode, ADDRESS_SHIPTO, true ) > 0 ) {
//						doSetProducerAddress(ref oAddress, true);
//					}	
//				}
			}
			else {
//				if (bDoBPRules) {
//					doBusinessPartnerRules( false, false, false, true);
//				}
//				else if ( bLinkedCustomer && sCustomer.Length > 0 ){
//					setFormDFValue(IDH_DISPORD._PAddress, getFormDFValueAsStringNZ(IDH_DISPORD._Address) );
//                	setFormDFValue(IDH_DISPORD._PStreet, getFormDFValueAsStringNZ(IDH_DISPORD._Street) );
//                	setFormDFValue(IDH_DISPORD._PBlock, getFormDFValueAsStringNZ(IDH_DISPORD._Block) );
//                	setFormDFValue(IDH_DISPORD._PZpCd, getFormDFValueAsStringNZ(IDH_DISPORD._ZpCd) );
//                	setFormDFValue(IDH_DISPORD._PCity, getFormDFValueAsStringNZ(IDH_DISPORD._City) );
//				}
//				else {
//					BPAddress oAddress = new BPAddress();
//					if ( oAddress.doGetAddress(oBP.CardCode, ADDRESS_SHIPTO, true ) > 0 ) {
//						doSetProducerAddress(ref oAddress, true);
//					}	
//				}
			}

			//Moved here to cater for the PO Producer 
			if (bDoBPRulesOnProd) {
				doBusinessPartnerRules( false, false, false, true);
			}
			else if ( bLinkedCustomer && sCustomer.Length > 0 ){
				doSynchPOAddress();			}
			else {
				BPAddress oAddress = new BPAddress();
                if (oAddress.doGetAddress(oBP.CardCode, BPAddress.ADDRTYPE.SHIPTO, true) > 0) {
					doSetProducerAddress(ref oAddress, true);
				}	
			}			
			
			if ( MMode == Config.MarketingMode.PO ) {
				string sPUOM = null;
				
				sPUOM = oBP.U_IDHUOM;
				if ( sPUOM == null )
					sPUOM = moLookup.getDefaultUOM();
				
				moDispRows.setFormDFValue(IDH_DISPROW._PUOM, sPUOM);
			}
			
			if (bDoBPRulesOnProd)
                DisposalRows.doItAll();
            else
			    DisposalRows.doItAll_Producer();

            //string sCardCode = oBP.CardCode;
            //if (DBObject.U_CardCd != null && DBObject.U_CardCd.Length > 0)
            //    sCardCode = sCardCode + ',' + DBObject.U_CardCd;
            //doFillTransactionCodes(sCardCode);
            doFillTransactionCodes();
            setProducerCurrency();

			return true;
		}	
		
		/**
		 * Synchrinize the Produser Address with the Customer Address
		 */
		protected void doSynchPOAddress(){
			setFormDFValue(IDH_DISPORD._PAddress, getFormDFValueAsStringNZ(IDH_DISPORD._Address) );
        	setFormDFValue(IDH_DISPORD._PStreet, getFormDFValueAsStringNZ(IDH_DISPORD._Street) );
        	setFormDFValue(IDH_DISPORD._PBlock, getFormDFValueAsStringNZ(IDH_DISPORD._Block) );
        	setFormDFValue(IDH_DISPORD._PZpCd, getFormDFValueAsStringNZ(IDH_DISPORD._ZpCd) );
        	setFormDFValue(IDH_DISPORD._PCity, getFormDFValueAsStringNZ(IDH_DISPORD._City) );
		}

        public void doFillAlternativeDescription(string sWateCode) {
            if (!Config.ParameterAsBool("FLALTITM", true))
                return;
 
            if (moWinForm != null)
                FillCombos.AltWasteDescriptionCombo(FillCombos.getCombo(moWinForm, "IDH_ALTITM"), sWateCode);
            else if (moSBOForm != null)
                FillCombos.AltWasteDescriptionCombo(FillCombos.getCombo(moSBOForm, "IDH_ALTITM"), sWateCode);
        }

        private void doLoadCombos() {
            doFillTransactionCodes();
        }

        private void doFillTransactionCodes() {
            string sCardCode = DBObject.U_CardCd;
            string sProducerCode = DBObject.U_PCardCd; 
            
            //if (DBObject.U_PCardCd != null && DBObject.U_PCardCd.Length > 0)
            //    sCardCode = ( sCardCode==null || sCardCode.Length == 0 ? "" : sCardCode + ',') + DBObject.U_PCardCd;

            //if (sCardCode.Length == 0) {
            //if ((sCardCode == null || sCardCode.Length == 0) && (sProducerCode == null || sProducerCode.Length == 0)) {
            //    if (moWinForm != null)
            //        Combobox2.doClearCombo(FillCombos.getCombo(moWinForm, "uBC_TRNCd"));
            //    else if (moSBOForm != null)
            //        Combobox2.doClearCombo(FillCombos.getCombo(moSBOForm, "uBC_TRNCd"));
            //} else {
                //doFillTransactionCodes(sCardCode);
                if (moWinForm != null)
                    FillCombos.TransactionCodeCombo(FillCombos.getCombo(moWinForm, "uBC_TRNCd"), sCardCode, sProducerCode, "DO");
                else if (moSBOForm != null)
                    FillCombos.TransactionCodeCombo(FillCombos.getCombo(moSBOForm, "uBC_TRNCd"), sCardCode, sProducerCode, "DO");
            //}
        }

        //public void doFillTransactionCodes(string sCardCode) {
        //    if (moWinForm != null)
        //        FillCombos.TransactionCodeCombo(FillCombos.getCombo(moWinForm, "uBC_TRNCd"), sCardCode, "DO");
        //    else if (moSBOForm != null)
        //        FillCombos.TransactionCodeCombo(FillCombos.getCombo(moSBOForm, "uBC_TRNCd"), sCardCode, "DO");
        //}

		public void doLoad2ndWeigh( string sJobNr,string sJobRow ) {
			string sTitle;

            if ( getGetByHeadAndRow(sJobNr, sJobRow) ){
				doSetAsSecondWeigh(true);
				return;
//				sTitle = Translation.getTranslated("IDH_DISPORD", "#TITLE", null, "Disposal Order") +
//								" - " + Translation.getTranslatedWord("Second Weigh");
//				doSetFocus("IDH_WEI2");
			}
			else {
				sTitle = Translation.getTranslated("IDH_DISPORD", "#TITLE", null, "Disposal Order") +
								" - " + Translation.getTranslatedWord("First Weigh");

                if (Mode == Mode_FIND || Mode == Mode_STARTUP)
                    Mode = Mode_ADD;
                else if (Mode == Mode_OK || Mode == Mode_VIEW)
                    Mode = Mode_UPDATE;
			}

			if ( moSBOForm != null )
				moSBOForm.Title = sTitle;
			else if ( moWinForm != null )
				moWinForm.Text = sTitle;
		}
		
		public void doSetAsSecondWeigh(bool bIsSecond){
			string sTitle;
			if ( bIsSecond ) {
				sTitle = Translation.getTranslated("IDH_DISPORD", "#TITLE", null, "Disposal Order") +
									" - " + Translation.getTranslatedWord("Second Weigh");
				doSetFocus("IDH_WEI2");
                mbISSecondWeigh = true;
			}
			else {
				sTitle = Translation.getTranslated("IDH_DISPORD", "#TITLE", null, "Disposal Order");
				doSetFocus("IDH_VEHREG");
                mbISSecondWeigh = false;
			}

			if ( moSBOForm != null )
				moSBOForm.Title = sTitle;
			else if ( moWinForm != null )
				moWinForm.Text = sTitle;
            
            base.doUpdateFormChangedData();
		}
		
		protected void setOrigin(string sOriginCd) {
			setFormDFValue(IDH_DISPORD._Origin, sOriginCd);
			moDispRows.setFormDFValue( IDH_DISPROW._Origin, sOriginCd);
		}	
		
#endregion
#region dbDetailsAddresses
		protected bool doSetCustomerAddress( ref BPAddress oAddress, string sPhone1) {
			if ((oAddress != null) && oAddress.Count > 0) {
				string sAddress = oAddress.Address;
				string sCustRef = oAddress.U_ISBCUSTRN;
				string bFoc = (string)getUFValue("IDH_FOC");
				
				double dChargeTotal = 0;
				if ( !bFoc.Equals("Y") == false) {
					dChargeTotal = moDispOrdDB.getUnCommittedRowTotals();
				}

//				if ( moLookup.doGetPOLimitsDOWO("DO", sCardCode, sAddress, sCustRef, dChargeTotal) == false) {
//					return false;
//				}
	
                setFormDFValue(IDH_DISPORD._Address, oAddress.Address);
                setFormDFValue(IDH_DISPORD._Street, oAddress.Street);
                setFormDFValue(IDH_DISPORD._Block, oAddress.Block);
                setFormDFValue(IDH_DISPORD._ZpCd, oAddress.ZipCode);
                setFormDFValue(IDH_DISPORD._City, oAddress.City);
                setFormDFValue(IDH_DISPORD._RTIMEF, oAddress.U_ISBACCTF);
                setFormDFValue(IDH_DISPORD._RTIMET, oAddress.U_ISBACCTT);

                string sPhone = oAddress.U_TEL1;
                if ( (sPhone == null || sPhone.Length == 0 ) &&
                	sPhone1 != null ) {
                        sPhone = sPhone1;
                }
                setFormDFValue(IDH_DISPORD._SiteTl, sPhone);
                setFormDFValue(IDH_DISPORD._Route, oAddress.U_ROUTE);
                setFormDFValue(IDH_DISPORD._Seq, oAddress.U_IDHSEQ);
                setFormDFValue(IDH_DISPORD._PremCd, oAddress.U_IDHPCD);
                setFormDFValue(IDH_DISPORD._SiteLic, oAddress.U_IDHSLCNO);
                setFormDFValue(IDH_DISPORD._SteId, oAddress.U_IDHSteId);

                if ( sCustRef != null && sCustRef.Length > 0 &&
                    !moLookup.getParameterAsBool( "MDREAE", false ) )
                    EnableItem("IDH_CUSCRF", false );
                else
                	EnableItem("IDH_CUSCRF", true );

                string sCurrCustRef;
                sCurrCustRef = getFormDFValueAsString(IDH_DISPORD._CustRef);
                if (sCurrCustRef == null || sCurrCustRef.Length == 0) 
                    setFormDFValue(IDH_DISPORD._CustRef, sCustRef);

                setFormDFValue(IDH_DISPORD._County, oAddress.County);
                
                EnableItem("IDH_CNA", false);
//                EnableItem("IDH_CUSRF", false);
                
               	setOrigin(oAddress.U_Origin);
			}
			else {
                setFormDFValue(IDH_DISPORD._Address, "");
                setFormDFValue(IDH_DISPORD._Street, "");
                setFormDFValue(IDH_DISPORD._Block, "");
                setFormDFValue(IDH_DISPORD._ZpCd, "");
                setFormDFValue(IDH_DISPORD._City, "");
                setFormDFValue(IDH_DISPORD._County, "");
                setFormDFValue(IDH_DISPORD._RTIMEF, "");
                setFormDFValue(IDH_DISPORD._RTIMET, "");
                //setFormDFValue(IDH_DISPORD._CustRef, "");
                setFormDFValue(IDH_DISPORD._SiteTl, "");
                setFormDFValue(IDH_DISPORD._SteId, "");
                setFormDFValue(IDH_DISPORD._Route, "");
                setFormDFValue(IDH_DISPORD._Seq, 0);
                setFormDFValue(IDH_DISPORD._PremCd, "");
                setFormDFValue(IDH_DISPORD._SiteLic, "");
			}
            if ( MMode == Config.MarketingMode.PO ) {
				doSynchPOAddress();	
			}

			return true;
		}
		protected bool doSetCarrierAddress( ref BPAddress oAddress) {
			if ( oAddress != null && oAddress.Count > 0) {
                setFormDFValue( IDH_DISPORD._CAddress, oAddress.Address);
                setFormDFValue(IDH_DISPORD._CStreet, oAddress.Street);
                setFormDFValue(IDH_DISPORD._CBlock, oAddress.Block);
                setFormDFValue(IDH_DISPORD._CZpCd, oAddress.ZipCode);
                setFormDFValue(IDH_DISPORD._CCity, oAddress.City);

                string sSupRef;
                sSupRef = getFormDFValueAsString(IDH_DISPORD._SupRef);
                if ( sSupRef == null || sSupRef.Length == 0 ) 
                    setFormDFValue( IDH_DISPORD._SupRef, oAddress.U_ISBCUSTRN );
			}
			else {
                setFormDFValue(IDH_DISPORD._CAddress, "");
                setFormDFValue(IDH_DISPORD._CStreet, "");
                setFormDFValue(IDH_DISPORD._CBlock, "");
                setFormDFValue(IDH_DISPORD._CZpCd, "");
                setFormDFValue(IDH_DISPORD._CCity, "");
                //setFormDFValue(IDH_DISPORD._SupRef, "");
			}
			return true;	
		}
        //protected bool doSetSiteAddress( ref BPAddress oAddress) {
        //    if ( oAddress != null && oAddress.Count > 0 ) {
        //        setFormDFValue(IDH_DISPORD._SAddress, oAddress.Address);
        //        setFormDFValue(IDH_DISPORD._SStreet, oAddress.Street);
        //        setFormDFValue(IDH_DISPORD._SBlock, oAddress.Block);
        //        setFormDFValue(IDH_DISPORD._SZpCd, oAddress.ZipCode);
        //        setFormDFValue(IDH_DISPORD._SCity, oAddress.City);
        //        if (oAddress.U_IDHACN != null && oAddress.U_IDHACN.Length > 0) 
        //            setFormDFValue(IDH_DISPORD._SContact, oAddress.U_IDHACN);
        //    }
        //    else {
        //        setFormDFValue(IDH_DISPORD._SAddress, "");
        //        setFormDFValue(IDH_DISPORD._SStreet, "");
        //        setFormDFValue(IDH_DISPORD._SBlock, "");
        //        setFormDFValue(IDH_DISPORD._SZpCd, "");
        //        setFormDFValue(IDH_DISPORD._SCity,  "");
        //    }
        //    return true;	
        //}
		protected bool doSetProducerAddress( ref BPAddress oAddress, bool bFromUser ) {
   			string sOrigin = "";
   			if ( oAddress != null && oAddress.Count > 0 ) {
   				if ( bFromUser )
            		setFormDFValueFromUser( IDH_DISPORD._PAddress, oAddress.Address );
            	else
            		setFormDFValue( IDH_DISPORD._PAddress, oAddress.Address );

                setFormDFValue(IDH_DISPORD._PStreet, oAddress.Street);
                setFormDFValue(IDH_DISPORD._PBlock, oAddress.Block);
                setFormDFValue(IDH_DISPORD._PZpCd, oAddress.ZipCode);
                setFormDFValue(IDH_DISPORD._PCity, oAddress.City);

                string sCurrRef;
                sCurrRef = getFormDFValueAsString(IDH_DISPORD._ProRef);
                if (sCurrRef == null || sCurrRef.Length == 0)
                    setFormDFValue(IDH_DISPORD._ProRef, oAddress.U_ISBCUSTRN);

                sOrigin = oAddress.U_Origin;

                string sJobType = moDispRows.getFormDFValueAsString(IDH_DISPROW._JobTp);
                if ( !Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) ) {
                    if (sOrigin != null && sOrigin.Length > 0)
                        setOrigin(sOrigin);
                }
   			
	    		if ( !moLookup.getParameterAsBool("DOORLU", false) ) {
	   				sOrigin = moDispRows.getFormDFValueAsString(IDH_DISPROW._Origin);
					if (sOrigin == null || sOrigin.Length == 0) {
						moDispRows.setFormDFValue(IDH_DISPROW._Origin, oAddress.City );
					}
				}   			
   			}
   			else {
                setFormDFValue(IDH_DISPORD._PAddress, "");
                setFormDFValue(IDH_DISPORD._PStreet, "");
                setFormDFValue(IDH_DISPORD._PBlock, "");
                setFormDFValue(IDH_DISPORD._PZpCd, "");
                setFormDFValue(IDH_DISPORD._PCity, "");
                //setFormDFValue(IDH_DISPORD._ProRef, "");
   			}

   			return true;
		}
#endregion
#region DoApplyBPRules
		public void doBusinessPartnerRules(bool bDoCustAddr, bool bDoCarrAddr, bool bDoSiteAddr, bool bDoProdAddr)
		{
			//IGNORE THIS IF THE DO IS LINKED TO A WO
			BPAddress oAddress = new BPAddress();
			
			string sVType = moDispRows.VType;
			if (sVType == null || sVType.Length == 0) {
				sVType = "A";
			}

			string sCustomerCode = getFormDFValueAsString(IDH_DISPORD._CardCd);
			string sCustomerName = getFormDFValueAsString(IDH_DISPORD._CardNM);
			string sPh1 = getFormDFValueAsString(IDH_DISPORD._Phone1);

			string sWasteCarrierCode = getFormDFValueAsString(IDH_DISPORD._CCardCd);
			string sWasteCarrierName = getFormDFValueAsString(IDH_DISPORD._CCardNM);

			string sDisposalSiteCode = getFormDFValueAsString(IDH_DISPORD._SCardCd);
			string sDisposalSiteName = getFormDFValueAsString(IDH_DISPORD._SCardNM);
			string sDisposalSiteOld = sDisposalSiteCode;

			string sProducerCode = getFormDFValueAsString(IDH_DISPORD._PCardCd);
			string sProducerName = getFormDFValueAsString(IDH_DISPORD._PCardNM);

			string sCustomerOption = null;
			string sCarrierOption = null;
			string sProducerOption = null;
			string sSiteOption = null;
			//bool bJobIsIn = moDispRows.getFormDFValueAsString(IDH_DISPROW._JobTp).Equals(moLookup.doGetJopTypeIncoming());
			bool bJobIsIn = moLookup.isIncomingJob(moLookup.doGetDOContainerGroup(), moDispRows.getFormDFValueAsString(IDH_DISPROW._JobTp));
			
			if (bJobIsIn) {
				sCustomerOption = Config.ParameterWithDefault("DODECUI", "NONE"); // (BLANK, DB, NONE, CARDCODE, CARRIER)
                sCarrierOption = Config.ParameterWithDefault("DODEWCI", "NONE"); // (BLANK, DB, NONE, CARDCODE, CUSTOMER)
                sProducerOption = Config.ParameterWithDefault("DODEPRI", "NONE"); // (BLANK, DB, NONE, CARDCODE, CUSTOMER)
                sSiteOption = Config.ParameterWithDefault("DODEDII", "NONE"); // (BLANK, DB, NONE, CARDCODE, USER, USERW)
			}
			else {
                sCustomerOption = Config.ParameterWithDefault("DODECUO", "NONE"); // (BLANK, DB, NONE, CARDCODE)
                sCarrierOption = Config.ParameterWithDefault("DODEWCO", "NONE"); // (BLANK, DB, NONE, CARDCODE)
                sProducerOption = Config.ParameterWithDefault("DODEPRO", "NONE"); // (BLANK, DB, NONE, CARDCODE)
                sSiteOption = Config.ParameterWithDefault("DODEDIO", "NONE"); // (BLANK, DB, NONE, CARDCODE, CUSTOMER, USERW)
			}

			if ( sSiteOption.Equals("USER")) {
				if ( mbFindBranchDisposalSite ) {
					sDisposalSiteCode = "";
				}
			}
			mbFindBranchDisposalSite = true;

			string sPrevCustomer = sCustomerCode;
			string sPrevCarrier = sWasteCarrierCode;
			string sPrevProducer = sProducerCode;
			string sPrevSite = sDisposalSiteCode;

			bool bCustChanged = false;
			bool bCarrierChanged = false;
			bool bDisposalSiteChanged = false;
			bool bProducerChanged = false;

			bool bCustChangedALOnce = false;
			bool bCarrierChangedALOnce = false;
			bool bDisposalSiteChangedALOnce = false;
			bool bProducerChangedALOnce = false;			
			
			for (int iTry = 0; iTry <= 1; iTry++) {
				bCustChanged = false;
				bCarrierChanged = false;
				bCustChanged = false;
				bCustChanged = false;

				//'***********************************************************
				//'  CUSTOMER
				//'***********************************************************
				if (sCustomerOption.Equals("BLANK")) {
					sCustomerCode = "";
					sCustomerName = "";
				}
				else {
					if (sCustomerCode.Length == 0 && sCustomerOption.Equals("IGNORE") == false ) {
						if (sCustomerOption.Length == 0 || sCustomerOption.Equals("NONE")) {
							if (bJobIsIn) {
								if (sWasteCarrierCode.Length > 0 && sVType.ToUpper().Equals("S")) {
									sCustomerCode = sWasteCarrierCode;
									sCustomerName = sWasteCarrierName;
								}
							}
						}
						else if (sCustomerOption.Equals("DB")) {
							sCustomerCode = msCompanyName;
							sCustomerName = msCompanyName;
						}
						else if (sCustomerOption.StartsWith("CARRIER")) {
							if (sWasteCarrierCode.Length > 0) {
                                //**********************
                                //To Be Developed
                                //**********************
                                //if ( sCustomerOption.EndsWith("-LINKED")) {
                                //    LinkedBP oLink = Config.INSTANCE.doGetLinkedBP(sWasteCarrierCode);
                                //    if ( oLink != null ) {
                                //        sCustomerCode = oLink.LinkedBPCardCode;
                                //        sCustomerName = oLink.LinkedBPName;
                                //    }
                                //}
                                //else {
								    sCustomerCode = sWasteCarrierCode;
								    sCustomerName = sWasteCarrierName;
                                //}
							}
						}
						else if (sCustomerOption.Equals("PRODUCER")) {
							if (sProducerCode.Length > 0) {
								sCustomerCode = sProducerCode;
								sCustomerName = sProducerName;
							}
						}
						else if (sCustomerOption.Equals("SITE")) {
							if (sDisposalSiteCode.Length > 0) {
								sCustomerCode = sDisposalSiteCode;
								sCustomerName = sDisposalSiteName;
							}
						}
						else {
							sCustomerCode = sCustomerOption;
							
							BP oBP = new BP();
							if ( oBP.doGetBPInfo( sCustomerCode, true) > 0 ) {
								doSetChoosenCustomer( ref oBP, false);
								sCustomerName = getFormDFValueAsString(IDH_DISPORD._CardNM);
							}
							oBP = null;
						}
					}

					if ( !sPrevCustomer.Equals(sCustomerCode) ) {
						bCustChanged = true;
						bCarrierChangedALOnce = true;

						setFormDFValue(IDH_DISPORD._CardCd, sCustomerCode);
						setFormDFValue(IDH_DISPORD._CardNM, sCustomerName);
						sPrevCustomer = sCustomerCode;
					}

//					if (bDoCustAddr || bCustChanged) {
//						if (sCustomerCode.Equals(msCompanyName) == true) {
//							oAddress.doGetCompanyAddress();
//						}
//						else {
//							oAddress.doGetAddress(sCustomerCode, "S");
//						}
//
//						if ( !doSetCustomerAddress(ref oAddress,sPh1) ) {
//							if ( Mode.Equals(Mode_FIND) ) {
//								moDispRows.setFormDFValue(IDH_DISPROW._Lorry, "", true);
//								moDispRows.setFormDFValue(IDH_DISPROW._LorryCd, "");
//								moDispRows.setFormDFValue(IDH_DISPROW._JobTp, "", true);
//								setUFValue("IDH_TARWEI", 0);
//								setUFValue("IDH_TRLTar", 0);
//								//oForm.Title = getTranslatedCaption("#TITLE", "Disposal Order");
//								setTitle("Disposal Order");
//							}
//						}
//					}
				}

				//'***********************************************************
				//'  CARRIER
				//'***********************************************************
				if (sCarrierOption.Equals("BLANK")) {
					sWasteCarrierCode = "";
					sWasteCarrierName = "";
				}
				else {
					if (sWasteCarrierCode.Length == 0 && sCarrierOption.Equals("IGNORE") == false) {
						if (sCarrierOption.Length == 0 || sCarrierOption.Equals("NONE")) {
							if (sVType.ToUpper().Equals("S")) {
								if (bJobIsIn) {
									sWasteCarrierCode = msCompanyName;
									sWasteCarrierName = msCompanyName;
								}
								else {
									if (sDisposalSiteCode.Length > 0) {
										sWasteCarrierCode = sDisposalSiteCode;
										sWasteCarrierName = sDisposalSiteName;
									}
								}
							}
						}
						else if (sCarrierOption.Equals("DB")) {
							sWasteCarrierCode = msCompanyName;
							sWasteCarrierName = msCompanyName;
						}
						else if (sCarrierOption.Equals("PRODUCER")) {
							if (sProducerCode.Length > 0) {
								sWasteCarrierCode = sProducerCode;
								sWasteCarrierName = sProducerName;
							}
						}
						else if (sCarrierOption.Equals("SITE")) {
							if (sDisposalSiteCode.Length > 0) {
								sWasteCarrierCode = sDisposalSiteCode;
								sWasteCarrierName = sDisposalSiteName;
							}
						}
						else if (sCarrierOption.Equals("CUSTOMER")) {
							if (sCustomerCode.Length > 0) {
								sWasteCarrierCode = sCustomerCode;
								sWasteCarrierName = sCustomerName;
							}
						}
						else {
							sWasteCarrierCode = sCarrierOption;
							
							BP oBP = new BP();
							if ( oBP.doGetBPInfo( sWasteCarrierCode, false ) > 0 ) {
								doSetChoosenCarrier( ref oBP, false);
								sWasteCarrierName = getFormDFValueAsString(IDH_DISPORD._CCardNM);
							}
							oBP = null;
						}
					}

					if (sPrevCarrier.Equals(sWasteCarrierCode) == false) {
						bCarrierChanged = true;
						bCarrierChangedALOnce = true;
						
						setFormDFValue(IDH_DISPORD._CCardCd, sWasteCarrierCode);
						setFormDFValue(IDH_DISPORD._CCardNM, sWasteCarrierName);
						sPrevCarrier = sWasteCarrierCode;
					}

//					if (bCarrierChanged || bDoCarrAddr) {
//						if (sWasteCarrierCode.Equals(msCompanyName) == true) {
//							oAddress.doGetCompanyAddress();
//						}
//						else {
//							oAddress.doGetAddress(sWasteCarrierCode, "S");
//						}
//						doSetCarrierAddress(ref oAddress);
//					}
				}

				//'***********************************************************
				//'  DISPOSAL SITE
				//'***********************************************************
				if (sSiteOption.Equals("BLANK")) {
					sDisposalSiteCode = "";
					sDisposalSiteName = "";
				}
				else {
                    if (!wasFormFieldSetByUser("IDH_DISSIT") && (sDisposalSiteCode.Length == 0 && sDisposalSiteCode.Equals("IGNORE") == false)) {
						if (sSiteOption.Length == 0 || sSiteOption.Equals("NONE")) {
						}
						else if (sSiteOption.Equals("DB")) {
							sDisposalSiteCode = msCompanyName;
							sDisposalSiteName = msCompanyName;
						}
						else if (sSiteOption.Equals("USER")) {
							//try to select the disposal site linked to the users branch
							string sBranch = moDispRows.getFormDFValueAsString(IDH_DISPROW._Branch);
							string sGroup = Config.Parameter("BGSDispo");
							string sQuery = "Select CardCode, CardName FROM OCRD WHERE U_IDHBRAN = '" + sBranch + "' And CardType='S' And GroupCode in (" + sGroup + ")";
							DataRecords oResult = null;
							try {
								oResult = DataHandler.INSTANCE.doBufferedSelectQuery("doBusinessPartnerRules", sQuery);
                                if ((oResult != null) && oResult.RecordCount > 0) {
									sDisposalSiteCode = oResult.getValueAsString(0);
									sDisposalSiteName = oResult.getValueAsString(1);
								}
								else {
									if ((sDisposalSiteOld != null) && sDisposalSiteOld.Length > 0) {
										sDisposalSiteCode = sDisposalSiteOld;
									}
									else {
										sDisposalSiteCode = msCompanyName;
										sDisposalSiteName = msCompanyName;
									}
								}
							}
							catch (Exception ex) {
								//DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
							}
							finally {
								DataHandler.INSTANCE.doReleaseDataRecords( ref oResult );
							}
						}

						else if (sSiteOption.Equals("CARRIER")) {
							if (sWasteCarrierCode.Length > 0) {
								sDisposalSiteCode = sWasteCarrierCode;
								sDisposalSiteName = sWasteCarrierName;
							}
						}
						else if (sSiteOption.Equals("PRODUCER")) {
							if (sProducerCode.Length > 0) {
								sDisposalSiteCode = sProducerCode;
								sDisposalSiteName = sProducerName;
							}
						}
						else if (sSiteOption.Equals("CUSTOMER")) {
							if (sCustomerCode.Length > 0) {
								sDisposalSiteCode = sCustomerCode;
								sDisposalSiteName = sCustomerName;
							}
						} else if (sSiteOption.Equals("USERWH")) {
                            sDisposalSiteCode = Config.INSTANCE.doGetUserWarehouseDisposalBPCode();
                            if (sDisposalSiteCode != null && sDisposalSiteCode.Length > 0 ) {
                                sDisposalSiteName = Config.INSTANCE.doGetBPName(sDisposalSiteCode);
                            }
						} else {
							sDisposalSiteCode = sSiteOption;
							BP oBP = new BP();
							if ( oBP.doGetBPInfo(sDisposalSiteCode, false) > 0 ) {
								doSetChoosenSite( ref oBP, false);
								sDisposalSiteName = getFormDFValueAsString(IDH_DISPORD._SCardNM);
							}
							oBP = null;
						}
					}

					if (sPrevSite.Equals(sDisposalSiteCode) == false) {
						bDisposalSiteChanged = true;
						bDisposalSiteChangedALOnce = true;
						
						setFormDFValue(IDH_DISPORD._SCardCd, sDisposalSiteCode);
						setFormDFValue(IDH_DISPORD._SCardNM, sDisposalSiteName);
						sPrevSite = sDisposalSiteCode;
					}

//					if (bDisposalSiteChanged || bDoSiteAddr) {
//						if (sDisposalSiteCode.Equals(msCompanyName) == true) {
//							oAddress.doGetCompanyAddress();
//						}
//						else {
//							oAddress.doGetAddress(sDisposalSiteCode, "S");
//						}
//						doSetSiteAddress(ref oAddress);
//					}
				}

				//'***********************************************************
				//'  PRODUCER
				//'***********************************************************
				if (sProducerOption!=null && sProducerOption.Equals("BLANK")) {
					sProducerCode = "";
					sProducerName = "";
				}
				else {
					if ((
                        (wasFormFieldSetWithCode("IDH_WPRODU") && wasFormFieldSetWithCode("IDH_PROCD")) || 
							sProducerCode.Length == 0) && sProducerOption.Equals("IGNORE") == false) {
						if (sProducerOption.Length == 0 || sProducerOption.Equals("NONE")) {
							if (bJobIsIn) {
								if (sWasteCarrierCode.Length > 0 && sVType.ToUpper().Equals("S")) {
									//'SET THE EMPTY PRODUCER TO WAST CARRIER
									sProducerCode = sWasteCarrierCode;
									sProducerName = sWasteCarrierName;
								}
								else {
									//'PRODUCER
									if (sCustomerCode.Length > 0) {
										sProducerCode = sCustomerCode;
										sProducerName = sCustomerName;
									}
								}
							}
						}
						else if (sProducerOption.Equals("DB")) {
							sProducerCode = msCompanyName;
							sProducerName = msCompanyName;
						}
						else if (sProducerOption.Equals("CARRIER")) {
							if (sWasteCarrierCode.Length > 0) {
								sProducerCode = sWasteCarrierCode;
								sProducerName = sWasteCarrierName;
							}
						}
						else if (sProducerOption.Equals("SITE")) {
							if (sDisposalSiteCode.Length > 0) {
								sProducerCode = sDisposalSiteCode;
								sProducerName = sDisposalSiteName;
							}
						}
						else if (sProducerOption.Equals("CUSTOMER")) {
							if (sCustomerCode.Length > 0) {
								sProducerCode = sCustomerCode;
								sProducerName = sCustomerName;
							}
						}
						else if (sProducerOption.Equals("USER")) {
						}
						else {
							sProducerCode = sProducerOption;
							BP oBP = new BP();
							if (oBP.doGetBPInfo(sProducerCode, false) > 0 ) {
								doSetChoosenProducer( ref oBP, false);
								sProducerName = getFormDFValueAsString(IDH_DISPORD._PCardNM);
							}
						}
					}

                    if (sPrevProducer.Equals(sProducerCode) == false && wasFormFieldSetWithCode("IDH_WPRODU")) {
						bProducerChanged = true;
						bProducerChangedALOnce = true;
						
						setFormDFValue(IDH_DISPORD._PCardCd, sProducerCode);
						setFormDFValue(IDH_DISPORD._PCardNM, sProducerName);
						sPrevProducer = sProducerCode;
					}

//					if (bProducerChanged || bDoProdAddr) {
//						if (sProducerCode.Equals(msCompanyName) == true) {
//							oAddress.doGetCompanyAddress();
//						}
//						else {
//							oAddress.doGetAddress(sProducerCode, "S");
//						}
//						doSetProducerAddress( ref oAddress, false);
//					}
				}

				if ( !bCustChanged && !bCarrierChanged && !bDisposalSiteChanged && !bProducerChanged ) {
					break; 
				}
				else {
//					bDoCustAddr = false;
//					bDoCarrAddr = false;
//					bDoSiteAddr = false;
//					bDoProdAddr = false;
				}
			}
			
			if (bCustChangedALOnce || bDoCustAddr) {
				if (sCustomerCode.Equals(msCompanyName) == true) {
					oAddress.doGetCompanyAddress();
				}
				else {
					oAddress.doGetAddress(sCustomerCode, BPAddress.ADDRTYPE.SHIPTO, true);
				}

				if ( !doSetCustomerAddress(ref oAddress,sPh1) ) {
					if ( Mode.Equals(Mode_FIND) ) {
						moDispRows.setFormDFValue(IDH_DISPROW._Lorry, "", true);
						moDispRows.setFormDFValue(IDH_DISPROW._LorryCd, "");
						moDispRows.setFormDFValue(IDH_DISPROW._JobTp, "", true);
						setUFValue("IDH_TARWEI", 0);
						setUFValue("IDH_TRLTar", 0);

                        setUFValue("IDH_TRWTE1", "");
                        setUFValue("IDH_TRWTE1", "");

						//oForm.Title = getTranslatedCaption("#TITLE", "Disposal Order");
						setTitle("Disposal Order");
					}
				}
			}			
			
			if (bCarrierChangedALOnce || bDoCarrAddr) {
				if (sWasteCarrierCode.Equals(msCompanyName) == true) {
					oAddress.doGetCompanyAddress();
				}
				else {
					oAddress.doGetAddress(sWasteCarrierCode, BPAddress.ADDRTYPE.SHIPTO, true);
				}
				doSetCarrierAddress(ref oAddress);
			}
			
			if (bDisposalSiteChangedALOnce || bDoSiteAddr) {
				if ( sSiteOption.Equals("USERWH") ) {
                    string sAddress = Config.INSTANCE.doGetUserWarehouseAddress();
                    if ( sAddress != null && sAddress.Length > 0 )
                        oAddress.doGetAddress(sDisposalSiteCode, BPAddress.ADDRTYPE.SHIPTO, sAddress,true);
                    else
                        oAddress.doGetAddress(sDisposalSiteCode, BPAddress.ADDRTYPE.SHIPTO, true);
                } else if (sDisposalSiteCode.Equals(msCompanyName) == true) {
					oAddress.doGetCompanyAddress();
				}
				else {
					oAddress.doGetAddress(sDisposalSiteCode, BPAddress.ADDRTYPE.SHIPTO, true);
				}
                moDispOrdDB.doSetDisposalSiteAddress(oAddress);
			}			
			
			if (bProducerChangedALOnce || bDoProdAddr) {
				if (sProducerCode.Equals(msCompanyName) == true) {
					oAddress.doGetCompanyAddress();
				}
				else {
					oAddress.doGetAddress(sProducerCode, BPAddress.ADDRTYPE.SHIPTO, true);
				}
				doSetProducerAddress( ref oAddress, false);
			}			
			
			oAddress = null;
		}

#endregion
#region TimerFunction
	public void DoStartTimer(){
        if ( moDispRows.WBridge != null && 
	    	miTimerInterval > 0 && 
	    	mbEnableTimer ) {
			mbTimerStarted = true;

			if ( moWinForm != null && moTimerBtn != null ) {
				mcButtonColor = moTimerBtn.BackColor;
				moTimerBtn.BackColor = mcButtonColor1;
			}
			moDispRows.doReadWeight("CR");
			
			initializeTimer();
        }			
	}
	public void DoStopTimer(){
		if ( oTimer != null ) {
			oTimer.Stop();
			oTimer.Enabled = false;
			mbTimerStarted = false;
			
			object oObject = oTimer;
			DataHandler.INSTANCE.doReleaseObject( ref oObject );
			
			setUFValue("IDH_WEIG", "Waiting" );
			
			if ( moWinForm != null && moTimerBtn != null ) {
				moTimerBtn.BackColor = mcButtonColor;
			}
		}
	}	
	public void initializeTimer() {
		 oTimer = new System.Timers.Timer();
		 oTimer.Elapsed += new ElapsedEventHandler(oTimer_Elapsed);
		 try {
	        oTimer.Interval = miTimerInterval;
	        oTimer.Enabled = true;
	        oTimer.Start();
		 }
		 catch ( Exception ex ) {
			//DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
		 }
		 finally {
	     	GC.Collect();
		 }
	}

	private void oTimer_Elapsed( object oSender, ElapsedEventArgs eArgs) {
		try {
            oTimer.Stop();
            if ( mbTimerStarted ) {
            	
            	try {
	            	moDispRows.doReadWeight("CR");
            	}
            	catch ( Exception ){}
            	
	            TimerReadWeight();
	            oTimer.Start();
            }
		}
		catch ( Exception ex ) {
			//DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
			DoStopTimer();
		}
		finally {
            GC.Collect();
		}
	}
	
	private delegate void ReadDelegate();
	private void TimerReadWeight(){
		if ( mbTimerStarted ) {
			if ( moWinForm != null ) {
				if ( moWinForm.InvokeRequired ){
					ReadDelegate oRead = new ReadDelegate( TimerReadWeight );
					moWinForm.Invoke(oRead);
				}
				else {
					//moDispRows.doReadWeight("CR");
					setUFValue("IDH_WEIG", moDispRows.LastWeight );
					
					if ( moTimerBtn != null ) {
						if ( bBtnSwitch^=true ) 
							moTimerBtn.BackColor = mcButtonColor1;
						else 
							moTimerBtn.BackColor = mcButtonColor2;
					}
				}
			}
		}
	}
#endregion		
#region dataFunctions
		/*
		 * This is called after the Form has been updated from the DataSource
		 */
		protected override void doDataLoaded(){
		}

        public bool doAddCustomerShipToAddress() {
            return BPAddress.doAddCustomerShipToAddress(DataHandler.INSTANCE.SBOCompany, DBObject);
        }
#endregion
#region overridesToCallRow

        /**
		 * Get the first record By Key
		 */
        public override int doReLoadData() {
            //20100911 - START - try to eliminate false Row Changes
            moDispOrdDB.BussyLoading = true;
            moDispRows.DBObject.BussyLoading = true;
            try {
            //20100911 - END - try to eliminate false Row Changes
            
                int iResult = moDispOrdDB.doReLoadData();
                if (iResult > 0) {
                    doLoadCombos();

                    //20100911 - START - try to eliminate false Row Changes
                    //setCurrencies();
                    //20100911 - END - try to eliminate false Row Changes

                    Mode = FormBridge.Mode_OK;
                    doUpdateFormData();
                    //Mode = FormBridge.Mode_OK;
                }
                return iResult;

            //20100911 - START - try to eliminate false Row Changes
            } catch (Exception e) {
                throw (e);
            } finally {
                moDispOrdDB.BussyLoading = false;
                moDispRows.DBObject.BussyLoading = false;
            }
            //20100911 - END - try to eliminate false Row Changes
        }

		public bool getGetByHeadAndRow( string sJobNr,string sJobRow ) {
            //20100911 - START - try to eliminate false Row Changes
            moDispOrdDB.BussyLoading = true;
            moDispRows.DBObject.BussyLoading = true;
            try {
            //20100911 - END - try to eliminate false Row Changes
            
                if ( moDispOrdDB.getGetByHeadAndRow(sJobNr, sJobRow) ){
                    doLoadCombos();
                    Mode = FormBridge.Mode_OK;
                    doUpdateFormData();
				    //Mode = FormBridge.Mode_OK;
				    return true;
			    }
			    else
				    return false;

            //20100911 - START - try to eliminate false Row Changes
            } catch (Exception e) {
                throw (e);
            } finally {
                moDispOrdDB.BussyLoading = false;
                moDispRows.DBObject.BussyLoading = false;
            }
            //20100911 - END - try to eliminate false Row Changes
		}
		/*
		 * Get the record by Key
		 */
		public override bool getByKey( string sCode){
            //20100911 - START - try to eliminate false Row Changes
            moDispOrdDB.BussyLoading = true;
            moDispRows.DBObject.BussyLoading = true;
            try {
            //20100911 - END - try to eliminate false Row Changes
            
                if ( moDispOrdDB.getByKey(sCode) ) {
                    doLoadCombos();
                    Mode = FormBridge.Mode_OK;
				    doUpdateFormData();
				    //Mode = FormBridge.Mode_OK;
				    return true;
			    }
			    else
				    return false;

            //20100911 - START - try to eliminate false Row Changes
            } catch (Exception e) {
                throw (e);
            } finally {
                moDispOrdDB.BussyLoading = false;
                moDispRows.DBObject.BussyLoading = false;
            }
            //20100911 - END - try to eliminate false Row Changes
		}
		
		/**
		 * Get the first record By Key
		 */
		public override bool getByKeyFirst(){
            //20100911 - START - try to eliminate false Row Changes
            moDispOrdDB.BussyLoading = true;
            moDispRows.DBObject.BussyLoading = true;
            try {
            //20100911 - END - try to eliminate false Row Changes
            
                if ( moDispOrdDB.getByKeyFirst() ){
                    doLoadCombos();

                    //20100911 - START - try to eliminate false Row Changes
                    //setCurrencies();
                    //20100911 - END - try to eliminate false Row Changes
                
                    Mode = FormBridge.Mode_OK;
				    doUpdateFormData();
				    //Mode = FormBridge.Mode_OK;
				    return true;
			    }
			    else
				    return false;

            //20100911 - START - try to eliminate false Row Changes
            } catch (Exception e) {
                throw (e);
            } finally {
                moDispOrdDB.BussyLoading = false;
                moDispRows.DBObject.BussyLoading = false;
            }
            //20100911 - END - try to eliminate false Row Changes
		}

		/**
		 * Get the previous record By Key
		 */
		public override bool getByKeyPrevious(){
            //20100911 - START - try to eliminate false Row Changes
            moDispOrdDB.BussyLoading = true;
            moDispRows.DBObject.BussyLoading = true;
            try {
            //20100911 - END - try to eliminate false Row Changes
            
                if ( moDispOrdDB.getByKeyPrevious() ){
                    doLoadCombos();

                    //20100911 - START - try to eliminate false Row Changes
                    //setCurrencies();
                    //20100911 - END - try to eliminate false Row Changes

                    Mode = FormBridge.Mode_OK;
                    doUpdateFormData();
				    //Mode = FormBridge.Mode_OK;
				    return true;
			    }
			    else
				    return false;

            //20100911 - START - try to eliminate false Row Changes
            } catch (Exception e) {
                throw (e);
            } finally {
                moDispOrdDB.BussyLoading = false;
                moDispRows.DBObject.BussyLoading = false;
            }
            //20100911 - END - try to eliminate false Row Changes
		}
		
		/**
		 * Get the nect record By Key
		 */
		public override bool getByKeyNext(){
            //20100911 - START - try to eliminate false Row Changes
            moDispOrdDB.BussyLoading = true;
            moDispRows.DBObject.BussyLoading = true;
            try {
            //20100911 - END - try to eliminate false Row Changes
            
                if ( moDispOrdDB.getByKeyNext() ) {
                    doLoadCombos();

                    //20100911 - START - try to eliminate false Row Changes
                    //setCurrencies();
                    //20100911 - END - try to eliminate false Row Changes

                    Mode = FormBridge.Mode_OK;
                    doUpdateFormData();
				    //Mode = FormBridge.Mode_OK;
				    return true;
			    }
			    else
				    return false;

            //20100911 - START - try to eliminate false Row Changes
            } catch (Exception e) {
                throw (e);
            } finally {
                moDispOrdDB.BussyLoading = false;
                moDispRows.DBObject.BussyLoading = false;
            }
            //20100911 - END - try to eliminate false Row Changes
		}

		/**
		 * Get the first record By Key
		 */
		public override bool getByKeyLast(){
            //20100911 - START - try to eliminate false Row Changes
            moDispOrdDB.BussyLoading = true;
            moDispRows.DBObject.BussyLoading = true;
            try {
            //20100911 - END - try to eliminate false Row Changes
            
                if ( moDispOrdDB.getByKeyLast() ){
                    doLoadCombos();

                    //20100911 - START - try to eliminate false Row Changes
                    //setCurrencies();
                    //20100911 - END - try to eliminate false Row Changes

                    Mode = FormBridge.Mode_OK;
				    doUpdateFormData();
				    //Mode = FormBridge.Mode_OK;
				    return true;
			    }
			    else
				    return false;

                //20100911 - START - try to eliminate false Row Changes
            } catch (Exception e) {
                throw (e);
            } finally {
                moDispOrdDB.BussyLoading = false;
                moDispRows.DBObject.BussyLoading = false;
            }
            //20100911 - END - try to eliminate false Row Changes
		}
#endregion
#region Reports
        public void doDocReport()
        {
            string sRowCode = DisposalRows.getFormDFValueAsString("Code");
            DocCon oReport = new local.reports.DocCon(this, "DO", "IDH_DISPORD", true);
            if (oReport.doDocConReport(sRowCode) && oReport.LastConsignmentNumber.Length > 0)
            {
                DisposalRows.setFormDFValue(IDH_DISPROW._ConNum, oReport.LastConsignmentNumber);
            }
        }

        public void doConvReport() {
            string sRowID = DisposalRows.getFormDFValueAsString(IDH_DISPROW._Code);
            string sCode = getFormDFValueAsString(IDH_DISPORD._Code);

            if (sRowID.Trim().Length > 0) {
                //Check for in or out
                string sReportFile;

                string sJobTp = DisposalRows.getFormDFValueAsString(IDH_DISPROW._JobTp);
                //if (sJobTp.Equals(Config.INSTANCE.doGetJopTypeIncoming()))
                if (Config.INSTANCE.isIncomingJob(moLookup.doGetDOContainerGroup(), sJobTp)) {
                    sReportFile = Config.Parameter("MDCREPI");
                    if (sReportFile == null)
                        sReportFile = "WasteTransferNoteGoodsINv1.1.rpt";
                }
                else {
                    sReportFile = Config.Parameter("MDCREPO");
                    if (sReportFile == null)
                        sReportFile = "WasteTransferNoteGoodsOUTv1.0.rpt";
                }

                Hashtable oParams = new Hashtable();
                oParams.Add("OrdNum", sCode);
                oParams.Add("RowNum", sRowID);

                if (moWinForm != null)
                {
                    doReport("IDH_DISPORD", sReportFile, oParams);
                    //if ( moSharedReporter == null )
                    //    moSharedReporter = new com.idh.wr1.form.Reporter();

                    //moSharedReporter.LocalReportDirectory = msLocalReportDirectory;
                    //moSharedReporter.ReportFileOnly = sReportFile;
                    //moSharedReporter.ReportFromForm = "IDH_DISPORD";
                    //moSharedReporter.Params = oParams;
                    //moSharedReporter.Branch = msBranch;
                    //moSharedReporter.PrinterName = msPrinterName;
                    ////moReporter.DoReLoad = false;

                    //moSharedReporter.ODBCDSN = Config.Parameter("REPDSN");
                    //moSharedReporter.DB = msDatabaseName;
                    //moSharedReporter.Username = msDatabaseUsername;
                    //moSharedReporter.Password = msDatabasePassword;
                    ////moReporter.ShowDialog();
                    //moSharedReporter.Show();
                }
            }
        }

        public void doReport(string sCallerForm, string sReportFile, Hashtable oParams) {
            bool bShowReport = Config.ParameterAsBool("MDSWDR", true);
            bool bAutoPrint = Config.ParameterAsBool("AUTOPRNT", true);
            bool bDoShowPrintDialog = Config.INSTANCE.getParameterAsBool("MDSWPD", true);
            int iCopies = Config.INSTANCE.getParameterAsInt("MDPCC", 1, true);

            string sSupplier = "", sDOcNum = "", sPostingDate = "";
            double sTotalAmount = 0;
            SAPbouiCOM.EditText odeit = (SAPbouiCOM.EditText)moSBOForm.Items.Item("4").Specific;
            sSupplier =odeit.Value;
            odeit = (SAPbouiCOM.EditText)moSBOForm.Items.Item("29").Specific;
            sTotalAmount = Convert.ToDouble( odeit.Value);

            odeit = (SAPbouiCOM.EditText)moSBOForm.Items.Item("8").Specific;
            sDOcNum = odeit.Value;

            odeit = (SAPbouiCOM.EditText)moSBOForm.Items.Item("10").Specific;
            sPostingDate= odeit.Value;
            

            //System.Windows.Forms.MessageBox.Show("MDSWDR: " + bShowReport +'\n' +
            //        "MDSWPD: " + bDoShowPrintDialog  + '\n' +
            //        "AUTOPRNT: " + bAutoPrint  + '\n'
            //    );

            if (SharedReporter == null || SharedReporter.FormIsClosed) {
                SharedReporter = new Reporter();
                SharedReporter.DoShowReport = bShowReport;
                SharedReporter.DoShowPrintDialog = bDoShowPrintDialog;
                SharedReporter.NumberOfCopies = iCopies;

                //com.idh.wr1.form.Reporter oReporter = new com.idh.wr1.form.Reporter();
                SharedReporter.LocalReportDirectory = LocalReportDirectory;
                SharedReporter.ReportFileOnly = sReportFile;
                SharedReporter.ReportFromForm = sCallerForm;
                SharedReporter.AlwaysShowPrintButton = true;
                SharedReporter.Params = oParams;

                SharedReporter.Branch = Branch;
                SharedReporter.PrinterName = PrinterName;

                SharedReporter.ODBCDSN = Config.Parameter("REPDSN");
                SharedReporter.DB = DatabaseName;
                SharedReporter.Username = DatabaseUsername;
                SharedReporter.Password = DatabasePassword;
                //oReporter.ShowDialog();
                if (bShowReport) {
                    if ( bAutoPrint )
                        SharedReporter.doPrint();

                    SharedReporter.doInitializeComponent();
                    SharedReporter.Show();
                } else {
                    SharedReporter.doPrint();
                }
            } else {
                SharedReporter.LocalReportDirectory = LocalReportDirectory;
                SharedReporter.ReportFileOnly = sReportFile;
                SharedReporter.ReportFromForm = sCallerForm;
                SharedReporter.AlwaysShowPrintButton = true;
                SharedReporter.Params = oParams;

                if (bShowReport) {
                    SharedReporter.doReport(true);
                } else {
                    SharedReporter.doPrint();
                }
            }
        }
#endregion
#region AddUpdate
        public bool doUpDateDataRow() {
            DisposalRows.DBObject.TareWeight = Conversions.ToDouble(getUFValue("IDH_TARWEI"));
            DisposalRows.DBObject.TrailerTareWeight = Conversions.ToDouble(getUFValue("IDH_TRLTar"));

            if (getUFValue("IDH_TRWTE1").Length > 0)
                DisposalRows.DBObject.TareWeightDate = Conversions.ToDateTime(getUFValue("IDH_TRWTE1"));

            if (getUFValue("IDH_TRWTE2").Length > 0)
                DisposalRows.DBObject.SecondTareWeightDate = Conversions.ToDateTime(getUFValue("IDH_TRWTE2"));

            //DisposalRows.DBObject.DoCheckAddUpdateVehicle = true;

            DisposalRows.doRequestCIPSIPUpdateOnOK();
            //DisposalRows.doCheckAndAddVehicle();

            if (!Config.INSTANCE.getParameterAsBool("CCONROW", false)) {
                if (!doCreditCardPayment()) {
                    return false;
                }
            } else {
                DisposalRows.doCreditCardPayment();
            }
            
            return DBObject.doUpdateDataRow();
        }

        public bool doAddDataRow() {
            DisposalRows.DBObject.TareWeight = Conversions.ToDouble(getUFValue("IDH_TARWEI"));
            DisposalRows.DBObject.TrailerTareWeight = Conversions.ToDouble(getUFValue("IDH_TRLTar"));

            if (getUFValue("IDH_TRWTE1").Length > 0 )
                DisposalRows.DBObject.TareWeightDate = Conversions.ToDateTime(getUFValue("IDH_TRWTE1"));

            if (getUFValue("IDH_TRWTE2").Length > 0)
                DisposalRows.DBObject.SecondTareWeightDate = Conversions.ToDateTime(getUFValue("IDH_TRWTE2"));

            //DisposalRows.DBObject.DoCheckAddUpdateVehicle = true;

            DisposalRows.doRequestCIPSIPUpdateOnOK();
            //DisposalRows.doCheckAndAddVehicle();

            if (!Config.INSTANCE.getParameterAsBool("CCONROW", false)) {
                if (!doCreditCardPayment()) {
                    return false;
                }
            } else {
                DisposalRows.doCreditCardPayment();
            }
          
            return DBObject.doAddDataRow();
        }
#endregion

        protected bool doCreditCardPayment() {
            //Do the Credit Card processing here
            
            double dCCTotal = 0;
            ArrayList saRows = new ArrayList();
            System.Windows.Forms.DialogResult oResult;

            IDH_DISPROW oDOR = moDispRows.DBObject;
            oDOR.doBookmark();
            try {
                oDOR.first();
                while ( oDOR.next() ){
                    if ( oDOR.isChangedRow() || oDOR.IsAddedRow() ) {
                        if ( FixedValues.isWaitingForInvoice(oDOR.U_Status) && 
                            oDOR.U_PayMeth.Equals(Config.INSTANCE.getCreditCardName()) &&
                            oDOR.U_PayStat.Equals(FixedValues.getStatusUnPaid()) && 
                            oDOR.U_Total != 0 ) {
                            dCCTotal += oDOR.U_Total;
                            saRows.Add(oDOR.CurrentRowIndex);
                        }
                    }
                }

                if ( dCCTotal != 0 ) {
                    CC oCC = new CC();
                    oCC.CustomerCode = moDispOrdDB.U_CardCd;
                    oCC.CustomerName = moDispOrdDB.U_CardNM;
                    oCC.Amount = dCCTotal;
                    oResult = oCC.doShowDialog();
                    if (oResult == System.Windows.Forms.DialogResult.OK) {
                        foreach (int iRow in saRows) {
                            oDOR.gotoRow(iRow, false);

                            oDOR.U_CCNum = oCC.CCNUM; //setDFValue(oForm, msRowTable, "U_CCNum", sCCNum)
                            oDOR.U_CCType = oCC.CCTYPE; //", sCCType)
                            oDOR.U_CCStat = oCC.CCSTAT; //", sCCStatPre & getSharedData(oForm, "CCSTAT"))
                            oDOR.U_PayStat = oCC.PayStatus; //", sPStat)
                            oDOR.U_CCExp = oCC.CCEXP; //", sCCExp)
                            oDOR.U_CCIs = oCC.CCIs; //", sCCIs)
                            oDOR.U_CCSec = oCC.CCSec; //", sCCSec)
                            oDOR.U_CCHNum = oCC.CCHNum; //", sCCHNm)
                            oDOR.U_CCPCd = oCC.CCPCd; //, sCCPCd)
                        }
                    }
                }
            } catch (Exception ex) {
                throw (ex);
            } finally {
                oDOR.doRecallBookmark();
            }

            return true;
        }
        
        public void doGenerateConsignmentNo()
        {
        	string sConNum = "";
        	string sBPCd = "";
        	string sBPNM = "";
        	string sBPAddress = "";
        	
    		sBPCd = DBObject.U_PCardCd;
    		sBPAddress = DBObject.U_PAddress;
    		//sBPNM = DBObject.U_CardNM;
            sBPNM = DBObject.U_PCardNM;

            sConNum = com.idh.bridge.utils.General.doCreateConNumber(sBPCd, sBPNM, sBPAddress);

            moDispRows.setFormDFValue(IDH_DISPROW._ConNum, sConNum);
        }
    }
}
