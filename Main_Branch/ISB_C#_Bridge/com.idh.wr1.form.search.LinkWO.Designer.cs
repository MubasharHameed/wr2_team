﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/14
 * Time: 12:00 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using com.idh.win.controls;
using com.idh.controls;
using com.idh.win.forms;
using com.idh.bridge;

namespace com.idh.wr1.form.search
{
	partial class LinkWO : SearchBase {
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            DBOGridControl control1 = new DBOGridControl();
            this.oSearchGrid = new com.idh.win.controls.WR1Grid();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.IDH_VEHREG = new System.Windows.Forms.TextBox();
            this.IDH_WOHD = new System.Windows.Forms.TextBox();
            this.IDH_WOROW = new System.Windows.Forms.TextBox();
            this.IDH_CREATE = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // oSearchGrid
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Beige;
            this.oSearchGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.oSearchGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.oSearchGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.oSearchGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oSearchGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oSearchGrid.GrayReadOnly = false;
            control1.CurrentMaxRows = 2000;
            control1.DBObject = null;
            control1.DoAsCol = true;
            control1.DoFilter = true;
            control1.SoftLimit = -1;
            this.oSearchGrid.GridControl = control1;
            this.oSearchGrid.Location = new System.Drawing.Point(5, 32);
            this.oSearchGrid.Name = "oSearchGrid";
            this.oSearchGrid.Size = new System.Drawing.Size(833, 440);
            this.oSearchGrid.TabIndex = 0;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(12, 478);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(75, 23);
            this.btn1.TabIndex = 1;
            this.btn1.Text = "Ok";
            this.btn1.UseVisualStyleBackColor = true;
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(93, 478);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(75, 23);
            this.btn2.TabIndex = 12;
            this.btn2.Text = "Cancel";
            this.btn2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 23);
            this.label1.TabIndex = 3;
            this.label1.Text = "Vehicle Registration";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(280, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Waste Order";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(528, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "Waste Order Row";
            // 
            // IDH_VEHREG
            // 
            this.IDH_VEHREG.Location = new System.Drawing.Point(114, 4);
            this.IDH_VEHREG.Name = "IDH_VEHREG";
            this.IDH_VEHREG.Size = new System.Drawing.Size(144, 20);
            this.IDH_VEHREG.TabIndex = 6;
            this.IDH_VEHREG.Text = "^";
            this.IDH_VEHREG.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // IDH_WOHD
            // 
            this.IDH_WOHD.Location = new System.Drawing.Point(354, 4);
            this.IDH_WOHD.Name = "IDH_WOHD";
            this.IDH_WOHD.Size = new System.Drawing.Size(144, 20);
            this.IDH_WOHD.TabIndex = 7;
            this.IDH_WOHD.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // IDH_WOROW
            // 
            this.IDH_WOROW.Location = new System.Drawing.Point(628, 3);
            this.IDH_WOROW.Name = "IDH_WOROW";
            this.IDH_WOROW.Size = new System.Drawing.Size(144, 20);
            this.IDH_WOROW.TabIndex = 8;
            this.IDH_WOROW.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // IDH_CREATE
            // 
            this.IDH_CREATE.Location = new System.Drawing.Point(763, 478);
            this.IDH_CREATE.Name = "IDH_CREATE";
            this.IDH_CREATE.Size = new System.Drawing.Size(75, 23);
            this.IDH_CREATE.TabIndex = 13;
            this.IDH_CREATE.Text = "Create";
            this.IDH_CREATE.UseVisualStyleBackColor = true;
            this.IDH_CREATE.Visible = false;
            this.IDH_CREATE.Click += new System.EventHandler(this.IDH_CREATE_Click);
            // 
            // LinkWO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 504);
            this.Controls.Add(this.IDH_CREATE);
            this.Controls.Add(this.IDH_WOROW);
            this.Controls.Add(this.IDH_WOHD);
            this.Controls.Add(this.IDH_VEHREG);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.oSearchGrid);
            this.KeyPreview = true;
            this.Name = "LinkWO";
            this.Text = "Available Waste Orders";
            ((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private System.Windows.Forms.TextBox IDH_WOROW;
		private System.Windows.Forms.TextBox IDH_WOHD;
		private System.Windows.Forms.TextBox IDH_VEHREG;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btn2;
		private System.Windows.Forms.Button btn1;
		private  idh.win.controls.WR1Grid oSearchGrid;

        protected void IDH_SelectedValueChanged(object sender, System.EventArgs e)
        {
            SelectedValueChanged_Internal(sender, e);
        }

        protected void IDH_TextChanged(object sender, System.EventArgs e)
        {
            TextChanged_Internal(sender, e);
        }

        private System.Windows.Forms.Button IDH_CREATE;
	}
}
