/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/15
 * Time: 10:01 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
//using System.Windows.Forms;
using com.idh.win.controls;
using com.idh.controls;
using com.idh.win.forms;

namespace com.idh.wr1.form.search
{
	/// <summary>
	/// BP Search Form
	/// </summary>
	public partial class BP : SearchBase {
		
		private  idh.bridge.search.BP moBPSearch;
		public  idh.bridge.search.BP BPSearch {
			get { return moBPSearch; }
		}	
				
		public BP(string sISCS) {
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			doSetupFormDefaults(btn1, btn2, oSearchGrid);
			oSearchGrid.setWinForm(this);
			
			moBPSearch = new  idh.bridge.search.BP();

            System.Windows.Forms.Form oForm = this;
			moBPSearch.setWinForm( ref oForm );		
			
			doSetupControl(sISCS);
		}

		public void doSetupControl(string sISCS){
			moBPSearch.ISCS = sISCS;
			moBPSearch.doSetGridOptions(this.oSearchGrid.GridControl);
			this.oSearchGrid.doApplyControl();
		}			

	}
}
