/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/14
 * Time: 12:00 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
//using System.Drawing;
//using System.Windows.Forms;
using com.idh.win.controls;
using com.idh.controls;
using com.idh.win.forms;
using com.idh.dbObjects.User;
using com.idh.bridge.lookups;

namespace com.idh.wr1.form.search {

	/// <summary>
	/// Description of com_idh_wr1_form_search_LinkWO.
	/// </summary>
	public partial class  LinkWO : SearchBase
	{
		private  idh.bridge.search.LinkWO moLinkWOSearch;
		public  idh.bridge.search.LinkWO LinkWOSearch {
			get { return moLinkWOSearch; }
		}

        private string msExludedRows;
        public string ExcludedRows {
            set { msExludedRows = value; }
        }

		public LinkWO()	{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			doSetupFormDefaults(btn1, btn2, oSearchGrid);
			oSearchGrid.setWinForm(this);
			
			moLinkWOSearch = new  idh.bridge.search.LinkWO();

            System.Windows.Forms.Form oForm = this;
			moLinkWOSearch.setWinForm( ref oForm );			
		}
		
		public  void doSetupControl(string sDispOrd, string sDispRow, string sUsedRows){
            moLinkWOSearch.doSetGridOptions(this.oSearchGrid.GridControl, sDispOrd, sDispRow, sUsedRows);
			this.oSearchGrid.doApplyControl();
		}

        public override void doLoadData() {
            base.doLoadData();
            if ( Config.INSTANCE.doGetDoBuyingAndTrade() ) {
                if ( oSearchGrid.RowCount <= 1 ) {
                    IDH_CREATE.Visible = true;
                    IDH_WOHD.Enabled = false;
                    IDH_WOROW.Enabled = false;
                } else {
                    IDH_CREATE.Visible = false;
                }
            } else {
                IDH_CREATE.Visible = false;
            }
        }

        private IDH_JOBSHD moWOR;
        protected IDH_JOBSHD WOR {
            get { return moWOR; }
        }

        private void IDH_CREATE_Click(object sender, EventArgs e) {
            string sWO = IDH_WOHD.Text; // moLinkWOSearch.getDialogValue("IDH_WOHD"); 
            moWOR = IDH_JOBSHD.doAutoCreateNewRowFromLastJobRow(sWO, true, false, false, true);

            if ( moWOR != null ) {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Hide();
            }

            //if (string.IsNullOrEmpty(sWOR))
            //    doLoadData();
        }

        public override object getReturnValueRAW(string sField) {
            if (moWOR != null && moWOR.Count > 0) {
                string sDBField = SearchGrid.getDBFieldNameFromCol(sField);
                if (sDBField == null) {
                } else {
                    int iIndex = sDBField.IndexOf('.');
                    if (iIndex != -1)
                        sDBField = sDBField.Substring(iIndex + 1);

                    if (sDBField != null)
                        return moWOR.getValue(sDBField);
                }
            }
            return base.getReturnValueRAW(sField);
        }
	}
}
