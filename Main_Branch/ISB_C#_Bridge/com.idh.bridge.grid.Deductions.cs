﻿/*
 * Created by SharpDevelop.
 * User: Louis
 * Date: 2011/02/19
 * Time: 11:35 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using com.idh.bridge;
using com.idh.bridge.form;
using com.idh.bridge.lookups;
using com.idh.bridge.data;
using com.idh.controls;
using com.idh.dbObjects.User;

namespace com.idh.bridge.grid	
{
	/// <summary>
	/// Description of com_idh_bridge_grid_Additional.
	/// </summary>
	public class Deductions
	{
		private com.idh.win.controls.WR1Grid moDeductionsGrid;
		private IDH_DISPROW moDispRow;
	
		public Deductions(IDH_DISPROW oDispRow, com.idh.win.controls.WR1Grid oDeductionsGrid)	{
            moDeductionsGrid = oDeductionsGrid;
			moDispRow = oDispRow;
			
			doSetAdditionalGridOptions();
            moDeductionsGrid.doApplyControl();		
		}
		
        private void doSetAdditionalGridOptions(){
            DBOGridControl oGridControl = moDeductionsGrid.GridControl;
        	bool bCanEdit = true;
        	//string sAddGrp = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup();
        		
        	oGridControl.DBObject = moDispRow.Deductions;
        	oGridControl.DBObject.AutoAddEmptyLine = false;
     	
        	oGridControl.setRequiredFilter(IDH_DODEDUCT._JobNr + " = '-1' AND " + IDH_DODEDUCT._RowNr + " = '-1'");

            oGridControl.doAddListField(IDH_DODEDUCT._Code, "Code", false, -1, "IGNORE", null);
            oGridControl.doAddListField(IDH_DODEDUCT._Name, "Name", false, 0, "IGNORE", null);
            oGridControl.doAddListField(IDH_DODEDUCT._JobNr, "Order Number", false, 0, "IGNORE", null, -1, SAPbouiCOM.BoLinkedObject.lf_UserDefinedObject);
            oGridControl.doAddListField(IDH_DODEDUCT._RowNr, "Row Order Number", false, 0, "IGNORE", null);
            oGridControl.doAddListField(IDH_DODEDUCT._ItemCd, "Item Code", bCanEdit, -1, "SRC:IDHISRC(ADD)[IDH_ITMCOD][ITEMCODE;U_ItemDsc=ITEMNAME;U_UOM=SUOM;U_ItmWgt=DEFWEI]", null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            oGridControl.doAddListField(IDH_DODEDUCT._ItemDsc, "Item Description", bCanEdit, -1, "IGNORE", null);
            oGridControl.doAddListField(IDH_DODEDUCT._Qty, "Quantity", bCanEdit, -1, "IGNORE", null);
            oGridControl.doAddListField(IDH_DODEDUCT._ItmWgt, "Item Weight", bCanEdit, -1, "IGNORE", null);
            oGridControl.doAddListField(IDH_DODEDUCT._UOM, "Unit Of Measure", bCanEdit, -1, "COMBOBOX", null);
            oGridControl.doAddListField(IDH_DODEDUCT._WgtDed, "Weight Deduction", bCanEdit, -1, "IGNORE", null);
            oGridControl.doAddListField(IDH_DODEDUCT._ValDed, "Value Deduction", bCanEdit, -1, "IGNORE", null);
        }		
	}
}
