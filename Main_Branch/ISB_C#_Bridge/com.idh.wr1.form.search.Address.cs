/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/17
 * Time: 02:03 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
//using System.Windows.Forms;
using com.idh.win.controls;
using com.idh.controls;
using com.idh.win.forms;

namespace com.idh.wr1.form.search
{
	/// <summary>
	/// Description of com_idh_wr1_form_search_Address.
	/// </summary>
	public partial class Address : SearchBase {
		private  idh.bridge.search.Address moAddressSearch;
		public  idh.bridge.search.Address AddressSearch {
			get { return moAddressSearch; }
		}	
			
		public Address() {
			
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			doSetupFormDefaults(btn1, btn2, oSearchGrid);
			oSearchGrid.setWinForm(this);
			
			moAddressSearch = new  idh.bridge.search.Address();

            System.Windows.Forms.Form oForm = this;
			moAddressSearch.setWinForm( ref oForm );	
			doSetupControl();
		}
		
		public override void doSetupControl(){
			moAddressSearch.doSetGridOptions(this.oSearchGrid.GridControl);
			this.oSearchGrid.doApplyControl();
		}			
	}
}
