﻿/*
 * Created by SharpDevelop.
 * User: Louis
 * Date: 2011/02/19
 * Time: 11:35 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using com.idh.bridge;
using com.idh.bridge.form;
using com.idh.bridge.lookups;
using com.idh.bridge.data;
using com.idh.controls;
using com.idh.dbObjects.User;

namespace com.idh.bridge.grid	
{
	/// <summary>
	/// Description of com_idh_bridge_grid_Additional.
	/// </summary>
	public class DisposalRows
	{
		private com.idh.win.controls.WR1Grid moDisposalRowsGrid;
		private IDH_DISPROW moDispRow;

        public DisposalRows(IDH_DISPROW oDispRow, com.idh.win.controls.WR1Grid oDisposalRowGrid) {
            moDisposalRowsGrid = oDisposalRowGrid;
            moDispRow = oDispRow;

			doSetGridOptions();
            moDisposalRowsGrid.doApplyControl();		
		}
		
        private void doSetGridOptions() {
            DBOGridControl oGridControl = moDisposalRowsGrid.GridControl;
        	//bool bCanEdit = false;
        		
        	oGridControl.DBObject = moDispRow;
        	oGridControl.DBObject.AutoAddEmptyLine = false;
     	
        	oGridControl.setRequiredFilter(IDH_DISPROW._JobNr + " = '-1'");

            oGridControl.doAddListField(IDH_DODEDUCT._Code, "Code", false, -1, "IGNORE", null);
            
            string sField = Config.INSTANCE.doGetProgressQueryStr("");
            oGridControl.doAddListField(sField, "Progress", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Status, "Status", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._PStat, "Purchase", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Code, "LineID", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._JobNr, "Job Entry", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._JobTp, "Order Type", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._RDate, "Req. Start", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._RTime, "Req. Time From", false, 0, "TIME", null);
            oGridControl.doAddListField(IDH_DISPROW._BDate, "Row Booking Date", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._BTime, "Row Booking Time", false, 0, "TIME", null);
            oGridControl.doAddListField(IDH_DISPROW._RTimeT, "Req. Time To", false, 0, "TIME", null);
            oGridControl.doAddListField(IDH_DISPROW._ASDate, "Act. Start", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._ASTime, "Act. S-Time", false, -1, "TIME", null);
            oGridControl.doAddListField(IDH_DISPROW._AEDate, "Act End", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._AETime, "Act E-Time", false, -1, "TIME", null);
            oGridControl.doAddListField(IDH_DISPROW._BPWgt, "BP Weight", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._UseBPWgt, "Use BP Weight", false, 0, null, null);
            //oGridControl.doAddListField(IDH_DISPROW._JbToBeDoneDt", "Job To Be Done Date", false, -1, null, null);
            //oGridControl.doAddListField(IDH_DISPROW._JbToBeDoneTm", "Job To Be Done Time", false, -1, "TIME", null);

            //oGridControl.doAddListField(IDH_DISPROW._DrivrOnSitDt", "Driver On Site Date", false, 0, null, null);
            //oGridControl.doAddListField(IDH_DISPROW._DrivrOnSitTm", "Driver On Site Time", false, 0, "TIME", null);

            //oGridControl.doAddListField(IDH_DISPROW._DrivrOfSitDt", "Driver Off Site Date", false, 0, null, null);
            //oGridControl.doAddListField(IDH_DISPROW._DrivrOfSitTm", "Driver Off Site Time", false, 0, "TIME", null);
            oGridControl.doAddListField(IDH_DISPROW._ItmGrp, "Item Grp", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._ItemCd, "Item Code", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._ItemDsc, "Item Desc.", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Serial, "Item Serial No.", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SLicNr, "Skip Lic. No.", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SLicExp, "Skip Exp.", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Lorry, "Vehicle", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Driver, "Driver", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TRLReg, "2nd Tare Id", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TRLNM, "2nd Tare Name", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Tip, "Supplier", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SAddress, "Disp Address", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._ProWgt, "Producer Wgt", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CstWgt, "Cust. Wgt", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._RdWgt, "Read Wgt", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TRdWgt, "Sup. Read Wgt", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._PRdWgt, "Producer Read Wgt", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SAORD, "Sales Order", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TIPPO, "Tipping PO", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Wei1, "Weighbridge Weigh 1", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Wei2, "Weighbridge Weigh 2", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._PCost, "Producer Cost", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._PCTotal, "TipTot", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TCharge, "TCharge", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TCTotal, "TCTotal", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CongCh, "CongCh", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._DocNum, "DocNum", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Price, "Price", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Discnt, "Discnt", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Total, "Total", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SLicCh, "SLicCh", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SLicSp, "SLicSp", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._VehTyp, "Vehicle Type", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._DisAmt, "Discount Amount", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TaxAmt, "Tax Amount", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SupRef, "Supplier Ref No.", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._WasCd, "Waste Code", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._WasDsc, "Waste Description", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Weight, "Weight", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Quantity, "Order Quantity", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._JCost, "Order Cost", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._SAINV, "Sales Invoice", false, 0, null, null);
            //oGridControl.doAddListField(IDH_DISPROW._SAORD, "Sales Order", false, 0, null, null);
            //oGridControl.doAddListField(IDH_DISPROW._TIPPO, "Tipping PO", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TPCN, "Disposal Purchase Credit Note", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TCCN, "Disposal Purchase Credit Note", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._JOBPO, "Job PO", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CustCd, "Customer Code", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CarrCd, "CarrierCode", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CongCd, "Congestion Supplier Code", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TipNm, "Dispoal Site Name", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CustNm, "Customer Name", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CarrNm, "Carrier Name", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SLicNm, "Skip License Supplier Name", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SCngNm, "Congestion Supplier Name", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Dista, "Distance", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Name, "Name", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Ser1, "Flash Serial 1", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Ser2, "Flash Serial 2", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._WDt1, "Weigh Date 1", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._WDt2, "Weigh Date 2", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._AddEx, "Additional Expences", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._CusQty, "Customer Qty", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._HlSQty, "Customer Qty", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CusChr, "Customer Charge", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._UOM, "Sales UOM", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._ProUOM, "Producer UOM", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._TipWgt, "Disposal Weight", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._PUOM, "Purchase UOM", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TipCost, "Tipping Cost", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TipTot, "Tipping Total", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._OrdWgt, "Order Weight", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CarWgt, "Order Weight", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._OrdCost, "Order Cost", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._OrdTot, "Order Total", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._WROrd, "WR Link Order", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._WRRow, "WR Link Order Row", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._LoadSht, "Load Sheet Number", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._ExpLdWgt, "Expected Load Weight", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._WASLIC, "Waste Lic. Reg. No.", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CntrNo, "Contract Number", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._PayMeth, "Payment Method", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._PayStat, "Payment Status", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CCNum, "Credit Card", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CCType, "CreditCard Type", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CCStat, "Credit Card Status", false, -1, null, null);

            oGridControl.doAddListField(IDH_DISPROW._CCExp, "CCard Expiry", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CCIs, "CCard Issue #", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CCSec, "CCard Security Number", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CCHNum, "CCard House Number", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CCPCd, "CCard PostCode", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._CustRef, "Customer Ref No.", false, -1, null, null);
            oGridControl.doAddListField(IDH_DISPROW._LorryCd, "Vehicle Code", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SLPO, "Purchase Order", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Covera, "Coverage String", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Comment, "Comments", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._IntComm, "Internal Comments", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._UseWgt, "Weight to Use", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._AUOMQt, "Overriding Qty", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._PAUOMQt, "Producer Overriding Qty", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._AUOM, "Overriding UOM", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._JobRmD, "Reminder Date", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._JobRmT, "Reminder Time", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._RemNot, "Reminder Message", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._RemCnt, "Reminder Count", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CoverHst, "Coverage Hist.", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._ProPO, "Producer Order.", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._ProNm, "Producer Name.", false, 20, null, null);
            oGridControl.doAddListField(IDH_DISPROW._ProGRPO, "Producer GR PO.", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._GRIn, "Goods Receipt In.", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SODlvNot, "SO Delivery note", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._WHTRNF, "Stock Transfer number", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._User, "User Name.", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Branch, "Branch.", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TZone, "Tip Zone", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._ContNr, "Container Number", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SealNr, "Seal Number", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._ConNum, "Hazardous Waste Consignment Note Number", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._TAddCost, "Additional Cost incl. VAT.", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TAddChrg, "Additional Charge incl. VAT.", false, 0, null, null);
            //''## MA Start 15-04-2014
            oGridControl.doAddListField(IDH_DISPROW._TAddChVat, "Additional Cost VAT", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TAddCsVat, "Additional Charge VAT", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._AddCharge, "Additional Cost", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._AddCost, "Additional Charge", false, 0, null, null);

            //''## MA End 15-04-2014
            oGridControl.doAddListField(IDH_DISPROW._Obligated, "Obligated.", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._Origin, "Origin.", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CustCs, "Compliancs Scheme", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SAddress, "Site Address", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._WastTNN, "Waste Transfer Note Number", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._SiteRef, "Site Reference Number", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._ExtWeig, "External Weighbridge Number", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._TChrgVtRt, "Tipping Vat Charge Rate", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._HChrgVtRt, "Haulage Vat Charge Rate", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TCostVtRt, "Tipping Vat Purchase Rate", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._PCostVtRt, "Supplier Vat Purchase Rate", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._HCostVtRt, "Haulage Vat Purchase Rate", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._TChrgVtGrp, "Tipping Vat Charge Group", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._HChrgVtGrp, "Haulage Vat Charge Group", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TCostVtGrp, "Tipping Vat Purchase Group", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._PCostVtGrp, "Supplier Vat Purchase Group", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._HCostVtGrp, "Haulage Vat Purchase Group", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._VtCostAmt, "Purchase Vat Amount", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CarrReb, "Carrier Rebate", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CustReb, "Customer Rebate", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._MANPRC, "Manual Prices Set", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._LnkPBI, "PBI Code", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._DPRRef, "Daily Profitability Report", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._MDChngd, "Changed Marketing Document", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._Sched, "Scheduled Job", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._USched, "Un-Scheduled", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._RowSta, "Row Status", false, -1, null, null);

            oGridControl.doAddListField(IDH_DISPROW._WgtDed, "Weight Deduction", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._AdjWgt, "Adjusted Weight", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._ValDed, "Value Deduction", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._BookIn, "Book into Stock", false, 0, null, null);

            oGridControl.doAddListField(IDH_DISPROW._IsTrl, "Trail Load", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._LckPrc, "Price Lock", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._TrnCode, "Transaction Code", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._AltWasDsc, "Alternative Waste Description", false, 0, null, null);

            //'Multi Currency Pricing 
            oGridControl.doAddListField(IDH_DISPROW._DispCgCc, "Disp. Charge Currency", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CarrCgCc, "Carr. Charge Currency", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._PurcCoCc, "Purc. Cost Currency", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._DispCoCc, "Disp. Cost Currency", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._CarrCoCc, "Carr. Cost Currency", false, 0, null, null);
            oGridControl.doAddListField(IDH_DISPROW._LiscCoCc, "Lisc. Cost Currency", false, 0, null, null);
        }		
	}
}
