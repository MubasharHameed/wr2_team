﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2008/12/17
 * Time: 07:06 PM
 *  
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using com.idh.controls;
using System.Windows.Forms;
using com.idh.bridge.form;
using com.idh.dbObjects.User;
using com.idh.bridge.data;
using com.idh.win;

using com.idh.bridge.lookups;
using com.uBC.utils;

namespace com.idh.bridge.search {
	/// <summary>
	/// Setting the Lorry search Rules
	/// </summary>
	public class Lorry: FormBridge	{
        private int miVersion = 1;

		public override void setWinForm( ref Form oForm ){
			base.setWinForm( ref oForm );

            if (!Config.ParameterAsBool("VMUSENEW", false)) {
                miVersion = 1;
            } else {
                miVersion = 2;
            }
	
			doFillWR1OT();
			doFillVEHTP();

            if (miVersion == 1) {
                doFillBranch();
                doFillITMGRP();
            } else if (miVersion == 2)
                doFillWFStatus();
		}

		public override void setSBOForm( ref IDHAddOns.idh.forms.Base oIDHForm, ref SAPbouiCOM.Form oSBOForm ){
			base.setSBOForm( ref oIDHForm, ref oSBOForm );

            if (!Config.ParameterAsBool("VMUSENEW", false)) {
                miVersion = 1;
            } else {
                miVersion = 2;
            }

			doFillWR1OT();
			doFillVEHTP();

            if (miVersion == 1) {
                doFillBranch();
                doFillITMGRP();
            }  else if (miVersion == 2)
                doFillWFStatus();
		}
		
		public void doSetGridOptions( DBOGridControl oGridControl, bool bDoShowActivity, bool bDoShowOnSite, string sJobTp, string sSOPO, string sWR1OT ) {
            if (!Config.ParameterAsBool("VMUSENEW", false)) {
                miVersion = 1;
            } else {
                miVersion = 2;
            }

            if (miVersion == 1) {
                doSetGridOptionsVehicleMaster1( oGridControl, bDoShowActivity, bDoShowOnSite, sJobTp, sSOPO, sWR1OT );
            } else {
                doSetGridOptionsVehicleMaster2( oGridControl, bDoShowActivity, bDoShowOnSite, sJobTp, sSOPO, sWR1OT );
            }
		}

        public void doSetGridOptionsVehicleMaster1(DBOGridControl oGridControl, bool bDoShowActivity, bool bDoShowOnSite, string sJobTp, string sSOPO, string sWR1OT) {
            int iSitesWidth = 0;
            bool bDoActivity = false;

            if (sWR1OT != null && sWR1OT.Equals("^DO")) {
                iSitesWidth = 100;
            } else {
                sWR1OT = "";
            }

            if (sJobTp != null && sJobTp.Length > 0 && bDoShowOnSite)
                sJobTp = " And dr.U_JobTp = '" + sJobTp + "' ";
            else
                sJobTp = "";

            oGridControl.doAddGridTable(new GridTable("@IDH_VEHMAS", "v","Code",false,true), true);

            if (bDoShowOnSite) {
                string sTestWght;
                if (sSOPO != null && sSOPO.Equals("PO"))
                    sTestWght = "And dr.U_TipWgt <= 0 ";
                else
                    sTestWght = "And dr.U_CstWgt <= 0 ";

                bDoActivity = false;
                //oGridControl.setTableValue("[@IDH_VEHMAS] v,OITB ig, [@IDH_DISPROW] dr,  [@IDH_DISPORD] do");
                //oGridControl.doAddGridTable( new GridTable("@IDH_VEHMAS", "v"), true);
                oGridControl.doAddGridTable(new GridTable("OITB", "ig", null, false, true));
                oGridControl.doAddGridTable(new GridTable("@IDH_DISPROW", "dr", null, false, true));
                oGridControl.doAddGridTable(new GridTable("@IDH_DISPORD", "do", null, false, true));
                //MA Start 23-07-2015 Issue#887
                string sUserBrachFilter = "";
                if (DataHandler.INSTANCE.User != "" && Config.ParameterAsBool("DOBRFLTR", false) == true)   {
                    sUserBrachFilter = " And dr.U_User in(Select OUSR.USER_CODE from OUSR WITH(noLOCK) where OUSR.Branch=(Select U2.Branch from OUSR U2 WITH(noLOCK) where U2.USER_CODE='" + DataHandler.INSTANCE.User + "'))";
                }
                //MA End 23-07-2015 Issue#887
                oGridControl.setRequiredFilter(
                        "ig.ItmsGrpCod = v.U_ItmGrp " +
                        "And v.U_WFStat != 'Denied' " +
                        "And v.U_VehReg = dr.U_Lorry " +
                        "And ((dr.U_Wei1 > 0 AND dr.U_Wei2 = 0) OR (dr.U_Wei2 > 0 AND dr.U_Wei1 = 0) ) " +
                        "And dr.U_RdWgt = 0 " +
                        sTestWght +
                        sJobTp +
                        "And dr.U_Status != '" + com.idh.bridge.lookups.FixedValues.getStatusOrdered() + "' " +
                        "And dr.U_Status != '" + com.idh.bridge.lookups.FixedValues.getStatusInvoiced() + "' " +
                        "And dr.U_Status != '" + com.idh.bridge.lookups.FixedValues.getStatusFoc() + "' " +
                        "And dr.U_RowSta != '" + com.idh.bridge.lookups.FixedValues.getStatusDeleted() + "' " +
                        "And v.Code In (SELECT Max(Code) FROM [@IDH_VEHMAS] WITH(NOLOCK) Group By  U_VehReg) " +
                        "And do.Code = dr.U_JobNr " + sUserBrachFilter);
                //"And dr.U_LorryCd=v.Code ");

            } else {
                if (bDoShowActivity) {
                    bDoActivity = true;
                    //oGridControl.doAddGridTable( new GridTable("@IDH_VEHMAS", "v"), true);
                    oGridControl.doAddGridTable(new GridTable("OITB", "ig", null, false, true));
                    oGridControl.doAddGridTable(new GridTable("IDH_VEHACT", "va"));

                    oGridControl.setRequiredFilter("ig.ItmsGrpCod = v.U_ItmGrp And v.U_WFStat != 'Denied' AND v.U_VehReg = va.U_VehReg");
                } else {
                    bDoActivity = false;
                    //oGridControl.setTableValue("[@IDH_VEHMAS] v,OITB ig");
                    //oGridControl.doAddGridTable( new GridTable("@IDH_VEHMAS", "v"), true);
                    oGridControl.doAddGridTable(new GridTable("OITB", "ig", null, false, true));

                    oGridControl.setRequiredFilter("ig.ItmsGrpCod = v.U_ItmGrp AND v.U_WFStat != 'Denied'");
                }
            }

            oGridControl.setOrderValue("v.U_WR1ORD DESC, v.U_VehReg");

            //Set the Filters - (bDoShowOnSite?"":
            oGridControl.doAddFilterField("IDH_ITMGRP", (bDoShowOnSite ? "dr.U_ItmGrp" : "v.U_ItmGrp"), SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, false);
            oGridControl.doAddFilterField("IDH_VEHREG", "v.U_VehReg", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);

            oGridControl.doAddFilterField("IDH_DRVR", "v.U_Driver", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);
            oGridControl.doAddFilterField("IDH_VEHTP", "v.U_VehType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 5, null, false);
            oGridControl.doAddFilterField("IDH_VEHSUP", (bDoShowOnSite ? "do.U_SCardCd" : "v.U_SCrdCd"), SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);
            oGridControl.doAddFilterField("IDH_VEHCUS", (bDoShowOnSite ? "do.U_CardCd" : "v.U_CCrdCd"), SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);
            oGridControl.doAddFilterField("IDH_WR1OT", "v.U_WR1ORD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 5, sWR1OT, false);
            oGridControl.doAddFilterField("IDH_BRANCH", "v.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 15, null, false);

            //Set the List fields
            oGridControl.doAddListField("v.U_VehReg", "Registration", false, 70, null, "REG");
            oGridControl.doAddListField("v.U_VehType", "VType", false, 30, null, "VTYPE");
            oGridControl.doAddListField("v.U_VehDesc", "Vehicle Desc.", false, 100, null, "VEHDESC");

            oGridControl.doAddListField("v.U_Driver", "Driver Name", false, 70, null, "DRIVER");
            oGridControl.doAddListField("v.U_Tarre", "Tare", false, 30, null, "TARRE");
            oGridControl.doAddListField("v.U_TRWTE1", "Tare Weight Date", false, 0, null, "TRWTE1");

            oGridControl.doAddListField("v.U_TRLReg", "2nd Tare Reg. No.", false, 0, null, "TRLREG");
            oGridControl.doAddListField("v.U_TRLNM", "2nd Tare Name", false, 0, null, "TRLNM");
            oGridControl.doAddListField("v.U_TRLTar", "2nd Tare Weight", false, 0, null, "TRLTAR");
            oGridControl.doAddListField("v.U_TRWTE2", "2nd Tare Weight Date", false, 0, null, "TRWTE2");

            oGridControl.doAddListField((bDoShowOnSite ? "dr.U_WasCd" : "v.U_WasCd"), "WasteCode", false, 0, null, "WASCD", -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            oGridControl.doAddListField((bDoShowOnSite ? "dr.U_WasDsc" : "v.U_WasDsc"), "WasteCode Descr", false, 100, null, "WASDSC");

            oGridControl.doAddListField((bDoShowOnSite ? "do.U_CardCd" : "v.U_CCrdCd"), "Customer Code", false, 50, null, "CUSCD", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            oGridControl.doAddListField((bDoShowOnSite ? "do.U_CardNM" : "v.U_CName"), "Customer", false, 100, null, "CUSTOMER");

            oGridControl.doAddListField((bDoShowOnSite ? "do.U_Address" : "v.U_CAddress"), "Customer Site", false, iSitesWidth, null, "CADDRESS");

            oGridControl.doAddListField("v.U_CCCrdCd", "Carrier Code (Cust)", false, 50, null, "WCCD", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            oGridControl.doAddListField("v.U_CCName", "Carrier (Cust)", false, 100, null, "WCNAM");

            oGridControl.doAddListField("v.U_CSCrdCd", "Carrier Code (Suppl)", false, 50, null, "WCSCD", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            oGridControl.doAddListField("v.U_CSName", "Carrier (Suppl)", false, 100, null, "WCSNAM");

            oGridControl.doAddListField((bDoShowOnSite ? "do.U_SCardCd" : "v.U_SCrdCd"), "Disposal Site Code", false, 50, null, "SUPCD", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            oGridControl.doAddListField((bDoShowOnSite ? "do.U_SCardNM" : "v.U_SName"), "Disposal Site Name", false, 100, null, "SUPPLIER");
            oGridControl.doAddListField((bDoShowOnSite ? "do.U_SAddress" : "v.U_SAddress"), "Disposal Site", false, iSitesWidth, null, "SADDRESS");

            oGridControl.doAddListField("ig.ItmsGrpNam", "Item Group", false, 0, null, "ITEMGRP", -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups);
            oGridControl.doAddListField("v.U_DrivrNr", "Driver Number", false, 0, null, "DRVNR");

            oGridControl.doAddListField((bDoShowOnSite ? "dr.U_ItemCd" : "v.U_ItemCd"), "Container Code", false, 0, null, "ITEMCD", -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            oGridControl.doAddListField((bDoShowOnSite ? "dr.U_ItemDsc" : "v.U_ItemDsc"), "Container Desc.", false, 0, null, "ITEMDSC");
            oGridControl.doAddListField("v.U_MarkAc", "MarkDoc Act", false, 0, null, "MARKAC");

            oGridControl.doAddListField("v.Code", "Vehicle Code", false, 0, null, "VEHCODE");

            if (bDoShowOnSite) {
                oGridControl.doAddListField("dr.Code", "DO Order", false, 0, null, "DOROW");
                oGridControl.doAddListField("dr.U_JobNr", "DO Row", false, 0, null, "DONR");
                oGridControl.doAddListField("dr.U_Status", "DO Status", false, 0, null, "DOSTATUS");
            }

            if (bDoActivity) {
                oGridControl.doAddListField("va.Allocated", "Allocated", false, 20, null, "ALLOC");
                oGridControl.doAddListField("va.Busy", "Busy", false, 20, null, "BUSY");
                oGridControl.doAddListField("va.Waiting", "Waiting", false, 20, null, "WAIT");
            }

            oGridControl.doAddListField("v.U_WR1ORD", "WR1 Type", false, 10, null, "WR1TYPE");

            if (bDoShowOnSite) {
                if (sJobTp.Length == 0)
                    oGridControl.doAddListField("dr.U_JobTp", "Job Type", false, 50, null, "JBTP");
                else
                    oGridControl.doAddListField("dr.U_JobTp", "Job Type", false, 0, null, "JBTP");
            }

            oGridControl.doAddListField("v.U_Origin", "Origin", false, iSitesWidth, null, "ORIGIN");
            oGridControl.doAddListField("v.U_Branch", "Branch", false, iSitesWidth, null, "BRANCH");
        }

        public void doSetGridOptionsVehicleMaster2(DBOGridControl oGridControl, bool bDoShowActivity, bool bDoShowOnSite, string sJobTp, string sSOPO, string sWR1OT) {
            int iSitesWidth = 0;
            bool bDoActivity = false;

            //if (sWR1OT != null && sWR1OT.Equals("^DO")) {
            //    iSitesWidth = 100;
            //} else {
            //    sWR1OT = "";
            //}

            if (sJobTp != null && sJobTp.Length > 0 && bDoShowOnSite)
                sJobTp = " And " + IDH_JOBSHD._JobTp + " = '" + sJobTp + "' ";
            else
                sJobTp = "";

            oGridControl.doAddGridTable(new GridTable(IDH_VEHMAS2.TableName, "v","Code",false,true), true);

            if (bDoShowOnSite) {
                string sTestWght;
                if (sSOPO != null && sSOPO.Equals("PO"))
                    sTestWght = "And " + IDH_DISPROW._TipWgt + " <= 0 ";
                else
                    sTestWght = "And " + IDH_DISPROW._CstWgt + " <= 0 ";

                bDoActivity = false;
                oGridControl.doAddGridTable(new GridTable(IDH_DISPROW.TableName, "dr", null, false, true));
                oGridControl.doAddGridTable(new GridTable(IDH_DISPORD.TableName, "do", null, false, true));
                //MA Start 23-07-2015 Issue#887
                string sUserBrachFilter = "";
                if (DataHandler.INSTANCE.User != "" && Config.ParameterAsBool("DOBRFLTR", false) == true)   {
                    sUserBrachFilter = " And dr.U_User in(Select OUSR.USER_CODE from OUSR WITH(noLOCK) where OUSR.Branch=(Select U2.Branch from OUSR U2 WITH(noLOCK) where U2.USER_CODE='" + DataHandler.INSTANCE.User + "'))";
                }
                //MA End 23-07-2015 Issue#887
                oGridControl.setRequiredFilter(
                        "v." + IDH_VEHMAS2._WFStat + " != 'Denied' " +
                        "AND v." + IDH_VEHMAS2._Avail + " = 'Y' " +
                        "And v." + IDH_VEHMAS2._VehReg + " = dr." + IDH_DISPROW._Lorry + " " +
                        "And ((dr." + IDH_DISPROW._Wei1 + "> 0 AND dr." + IDH_DISPROW._Wei2 + " = 0) OR (dr." + IDH_DISPROW._Wei2 + " > 0 AND dr." + IDH_DISPROW._Wei1 + " = 0) ) " +
                        "And dr." + IDH_DISPROW._RdWgt + " = 0 " +
                        sTestWght +
                        sJobTp +
                        "And dr." + IDH_DISPROW._Status + " != '" + com.idh.bridge.lookups.FixedValues.getStatusOrdered() + "' " +
                        "And dr." + IDH_DISPROW._Status + " != '" + com.idh.bridge.lookups.FixedValues.getStatusInvoiced() + "' " +
                        "And dr." + IDH_DISPROW._Status + " != '" + com.idh.bridge.lookups.FixedValues.getStatusFoc() + "' " +
                        "And dr." + IDH_DISPROW._RowSta + " != '" + com.idh.bridge.lookups.FixedValues.getStatusDeleted() + "' " +
                        "And do." + IDH_DISPORD._Code + " = dr." + IDH_DISPROW._JobNr + sUserBrachFilter);
            } else {
                if (bDoShowActivity) {
                    bDoActivity = true;
                    oGridControl.doAddGridTable(new GridTable("IDH_VEHACT2", "va"));

                    oGridControl.setRequiredFilter(
                        "v." + IDH_VEHMAS2._WFStat + " != 'Denied' " +
                        "AND v." + IDH_VEHMAS2._Avail + " = 'Y' " +
                        "AND v." + IDH_VEHMAS2._VehReg + " = va.U_VehReg");
                } else {
                    bDoActivity = false;
                    oGridControl.setRequiredFilter(
                        "v." + IDH_VEHMAS2._WFStat + " != 'Denied' " +
                        "AND v." + IDH_VEHMAS2._Avail + " = 'Y' " );
                }
            }

            oGridControl.setOrderValue("v." + IDH_VEHMAS2._WR1ORD + " DESC, v." + IDH_VEHMAS2._VehReg);

            ////Set the Filters - (bDoShowOnSite?"":
            oGridControl.doAddFilterField("IDH_VEHREG", "v." + IDH_VEHMAS2._VehReg, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);
            oGridControl.doAddFilterField("IDH_VEHDIS", "v." + IDH_VEHMAS2._VehDesc, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, false);
            oGridControl.doAddFilterField("IDH_DRVR", "v." + IDH_VEHMAS2._Driver, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);
            oGridControl.doAddFilterField("IDH_VEHTP", "v." + IDH_VEHMAS2._VehT, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 5, null, false);
            oGridControl.doAddFilterField("IDH_CWFS", "v." + IDH_VEHMAS2._WFStat, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);

            //oGridControl.doAddFilterField("IDH_VEHCUS", (bDoShowOnSite ? IDH_DISPROW._CustCd : IDH_VEHMAS2._CCrdCd), SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);

            //oGridControl.doAddFilterField("IDH_WR1OT", "v." + IDH_VEHMAS2._WR1ORD, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 5, sWR1OT, false);
            oGridControl.doAddFilterField("IDH_CARR", "v." + IDH_VEHMAS2._CCCrdCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 15, null, false);

            //Set the List fields
            oGridControl.doAddListField("v." + IDH_VEHMAS2._VehT, "Type", false, 100, ListFields.LISTTYPE_COMBOBOX, "VTYPE", -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true);
            oGridControl.doAddListField("v." + IDH_VEHMAS2._VehReg, "Registration", false, 100, ListFields.LISTTYPE_IGNORE, "REG", -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true);
            oGridControl.doAddListField("v." + IDH_VEHMAS2._VehDesc, "Description", false, 250, ListFields.LISTTYPE_IGNORE, "VEHDESC", -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true);
            oGridControl.doAddListField("v." + IDH_VEHMAS2._Tarre, "Tare", false, 70, ListFields.LISTTYPE_IGNORE, "TARRE", -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true);
            oGridControl.doAddListField("v." + IDH_VEHMAS2._CCCrdCd, "Carrier", false, 100, ListFields.LISTTYPE_IGNORE, "WCCD", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner, false, true);
            oGridControl.doAddListField("v." + IDH_VEHMAS2._Avail, "Available", false, 50, ListFields.LISTTYPE_COMBOBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true);
            oGridControl.doAddListField("v." + IDH_VEHMAS2._Driver, "Driver Name", false, 100, null, "DRIVER");
            
            //oGridControl.doAddListField(IDH_VEHMAS2._TRLReg, "2nd Tare Reg. No.", false, 0, null, "TRLREG");
            //oGridControl.doAddListField(IDH_VEHMAS2._TRLNM, "2nd Tare Name", false, 0, null, "TRLNM");
            //oGridControl.doAddListField(IDH_VEHMAS2._TRLTar, "2nd Tare Weight", false, 0, null, "TRLTAR");

            oGridControl.doAddListField((bDoShowOnSite ? "dr." + IDH_DISPROW._WasCd : "' '"), "WasteCode", false, 0, null, "WASCD", -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            oGridControl.doAddListField((bDoShowOnSite ? "dr." + IDH_DISPROW._WasDsc : "' '"), "WasteCode Descr", false, 100, null, "WASDSC");

            oGridControl.doAddListField((bDoShowOnSite ? "do." + IDH_DISPORD._CardCd : "' '"), "Customer Code", false, 0, null, "CUSCD", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            oGridControl.doAddListField((bDoShowOnSite ? "do." + IDH_DISPORD._CardNM : "' '"), "Customer", false, 0, null, "CUSTOMER");

            oGridControl.doAddListField((bDoShowOnSite ? "do." + IDH_DISPORD._Address : "' '"), "Customer Site", false, iSitesWidth, null, "CADDRESS");

            //oGridControl.doAddListField(IDH_VEHMAS2._CCName, "Carrier (Cust)", false, 100, null, "WCNAM");

            oGridControl.doAddListField((bDoShowOnSite ? "do." + IDH_DISPORD._SCardCd : "' '"), "Disposal Site Code", false, 0, null, "SUPCD", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            oGridControl.doAddListField((bDoShowOnSite ? "do." + IDH_DISPORD._SCardNM : "' '"), "Disposal Site Name", false, 0, null, "SUPPLIER");
            oGridControl.doAddListField((bDoShowOnSite ? "do." + IDH_DISPORD._SAddress : "' '"), "Disposal Site", false, 0, null, "SADDRESS");

            oGridControl.doAddListField("v." + IDH_VEHMAS2._DrivrNr, "Driver Number", false, 0, null, "DRVNR");

            oGridControl.doAddListField((bDoShowOnSite ? "dr." + IDH_DISPROW._ItemCd : "' '"), "Container Code", false, 0, null, "ITEMCD", -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            oGridControl.doAddListField((bDoShowOnSite ? "dr." + IDH_DISPROW._ItemDsc : "' '"), "Container Desc.", false, 0, null, "ITEMDSC");
            oGridControl.doAddListField(IDH_VEHMAS2._MarkAc, "MarkDoc Act", false, 0, null, "MARKAC");

            oGridControl.doAddListField("v." + IDH_VEHMAS2._Code, "Vehicle Code", false, 0, null, "VEHCODE");

            if (bDoShowOnSite) {
                oGridControl.doAddListField("dr." + IDH_DISPROW._Code, "DO Order", false, -1, null, "DOROW");
                oGridControl.doAddListField("dr." + IDH_DISPROW._JobNr, "DO Row", false, -1, null, "DONR");
                oGridControl.doAddListField("dr." + IDH_DISPROW._Status, "DO Status", false, -1, null, "DOSTATUS");
            }

            if (bDoActivity) {
                oGridControl.doAddListField("va.Allocated", "Allocated", false, 20, null, "ALLOC");
                oGridControl.doAddListField("va.Busy", "Busy", false, 20, null, "BUSY");
                oGridControl.doAddListField("va.Waiting", "Waiting", false, 20, null, "WAIT");
            }

            oGridControl.doAddListField("v." + IDH_VEHMAS2._WR1ORD, "WR1 Type", false, 0, null, "WR1TYPE");

            if (bDoShowOnSite) {
                if (sJobTp.Length == 0)
                    oGridControl.doAddListField("dr." + IDH_DISPROW._JobTp, "Job Type", false, 50, null, "JBTP");
                else
                    oGridControl.doAddListField("dr." + IDH_DISPROW._JobTp, "Job Type", false, 0, null, "JBTP");
            }

            oGridControl.doAddListField("' '", "Origin", false, iSitesWidth, null, "ORIGIN");
            oGridControl.doAddListField("' '", "Branch", false, iSitesWidth, null, "BRANCH");
        }

#region fillCombos
		private void doFillWR1OT(){
			object oNCombo = null;
			if ( moSBOForm != null ){
	            SAPbouiCOM.Item  oItem;
	            SAPbouiCOM.ComboBox oCombo;
	            oItem = moSBOForm.Items.Item("IDH_WR1OT");
	            oItem.DisplayDesc = true;
	            oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
	
	        	oNCombo = oCombo;
	        	com.idh.bridge.utils.Combobox.doClearCombo( ref oNCombo );
	        } else if ( moWinForm != null ) {
				oNCombo = getWinControl("IDH_WR1OT");
			}
			com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo, "", "All");
	        com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"WO", "Waste Order");
	        com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"DO", "Disposal  Order");
	        com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"^WO", "Open And Waste Order");
	        com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"^DO", "Open And Disposal  Order");
		}
		
		private void doFillVEHTP(){
			object oNCombo = null;
			if ( moSBOForm != null ){
	            SAPbouiCOM.Item oItem;
	            SAPbouiCOM.ComboBox oCombo;
	            oItem = moSBOForm.Items.Item("IDH_VEHTP");
	            oItem.DisplayDesc = true;
	            oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
	
	            oNCombo = oCombo;
	        	com.idh.bridge.utils.Combobox.doClearCombo( ref oNCombo );
			}
			else if ( moWinForm != null ) {
				oNCombo = getWinControl("IDH_VEHTP");
			}
	            
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"", "All");
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"A", "Asset-OnRoad");
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"O", "Asset-OffRoad");
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"S", "Sub Contract");
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo, "H", "Owner Driver");
			com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"S", "Sub Contractor");
		}

        private void doFillWFStatus() {
            if ( moWinForm != null )
                FillCombos.WFStatusCombo(FillCombos.getCombo(moWinForm, "IDH_CWFS"));
            else if ( moSBOForm != null )
                FillCombos.WFStatusCombo(FillCombos.getCombo(moSBOForm, "IDH_CWFS"));
        }

		private void doFillITMGRP(){
			object oForm = ( moWinForm != null ? (object)moWinForm: (object)moSBOForm );

//...		com.idh.bridge.utils.Combobox.doFillCombo( ref oForm, "IDH_ITMGRP", "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpNam=jt.U_ItemGrp", "ItmsGrpCod", true);
//			com.idh.bridge.utils.Combobox.doFillCombo( oForm, "IDH_ITMGRP", "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpCod=jt.U_ItemGrp", "ItmsGrpCod", true);

            string sDefaultVehicleGroup = Config.ParameterWithDefault("IGR-LOR", "");
            if ( sDefaultVehicleGroup.Length > 0 ) {
                object oNCombo = null;
			    if ( moSBOForm != null ){
	                SAPbouiCOM.Item oItem;
	                SAPbouiCOM.ComboBox oCombo;
                    oItem = moSBOForm.Items.Item("IDH_ITMGRP");
	                oItem.DisplayDesc = true;
	                oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
	
	                oNCombo = oCombo;
	        	    com.idh.bridge.utils.Combobox.doClearCombo( ref oNCombo );
			    } else if ( moWinForm != null ) {
                    oNCombo = getWinControl("IDH_ITMGRP");
			    }
                com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo, "", "All");
                com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo, Config.Parameter("IGR-LOR"), "Default Group");
            }
        }

        private void doFillBranch() {
            object oForm = ( moWinForm != null ? (object)moWinForm: (object)moSBOForm );
            com.idh.bridge.utils.Combobox.doFillCombo(oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks", null, null, true);
        }
#endregion
	}
}
