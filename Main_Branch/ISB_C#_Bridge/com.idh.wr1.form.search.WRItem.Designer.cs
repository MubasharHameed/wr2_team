﻿/*
 * Created by SharpDevelop.
 * User: Louis
 * Date: 2009/07/20
 * Time: 01:23 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace com.idh.wr1.form.search
{
	partial class WRItem
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.oSearchGrid = new  idh.win.controls.WR1Grid();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.btn1 = new System.Windows.Forms.Button();
			this.btn2 = new System.Windows.Forms.Button();
			this.IDH_ITMCOD = new System.Windows.Forms.TextBox();
			this.IDH_ITMNAM = new System.Windows.Forms.TextBox();
			this.IDH_GRPCOD = new System.Windows.Forms.TextBox();
			this.IDH_GRPNAM = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// oSearchGrid
			// 
			this.oSearchGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.oSearchGrid.Location = new System.Drawing.Point(6, 32);
			this.oSearchGrid.Name = "oSearchGrid";
			this.oSearchGrid.Size = new System.Drawing.Size(833, 440);
			this.oSearchGrid.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(6, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "Item Code";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(176, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 23);
			this.label2.TabIndex = 2;
			this.label2.Text = "Item Name";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(398, 9);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 23);
			this.label3.TabIndex = 3;
			this.label3.Text = "Groupe Code";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(588, 9);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 23);
			this.label4.TabIndex = 4;
			this.label4.Text = "Group Name";
			// 
			// btn1
			// 
			this.btn1.Location = new System.Drawing.Point(12, 475);
			this.btn1.Name = "btn1";
			this.btn1.Size = new System.Drawing.Size(75, 23);
			this.btn1.TabIndex = 5;
			this.btn1.Text = "Ok";
			this.btn1.UseVisualStyleBackColor = true;
			// 
			// btn2
			// 
			this.btn2.Location = new System.Drawing.Point(93, 475);
			this.btn2.Name = "btn2";
			this.btn2.Size = new System.Drawing.Size(75, 23);
			this.btn2.TabIndex = 6;
			this.btn2.Text = "Cancel";
			this.btn2.UseVisualStyleBackColor = true;
			// 
			// IDH_ITMCOD
			// 
			this.IDH_ITMCOD.Location = new System.Drawing.Point(65, 6);
			this.IDH_ITMCOD.Name = "IDH_ITMCOD";
			this.IDH_ITMCOD.Size = new System.Drawing.Size(100, 20);
			this.IDH_ITMCOD.TabIndex = 7;
			this.IDH_ITMCOD.Validated += new System.EventHandler(this.IDH_TextChanged);
			// 
			// IDH_ITMNAM
			// 
			this.IDH_ITMNAM.Location = new System.Drawing.Point(238, 6);
			this.IDH_ITMNAM.Name = "IDH_ITMNAM";
			this.IDH_ITMNAM.Size = new System.Drawing.Size(152, 20);
			this.IDH_ITMNAM.TabIndex = 8;
			this.IDH_ITMNAM.Validated += new System.EventHandler(this.IDH_TextChanged);
			// 
			// IDH_GRPCOD
			// 
			this.IDH_GRPCOD.Location = new System.Drawing.Point(474, 6);
			this.IDH_GRPCOD.Name = "IDH_GRPCOD";
			this.IDH_GRPCOD.Size = new System.Drawing.Size(100, 20);
			this.IDH_GRPCOD.TabIndex = 9;
			this.IDH_GRPCOD.Validated += new System.EventHandler(this.IDH_TextChanged);
			// 
			// IDH_GRPNAM
			// 
			this.IDH_GRPNAM.Location = new System.Drawing.Point(661, 6);
			this.IDH_GRPNAM.Name = "IDH_GRPNAM";
			this.IDH_GRPNAM.Size = new System.Drawing.Size(152, 20);
			this.IDH_GRPNAM.TabIndex = 10;
			this.IDH_GRPNAM.Validated += new System.EventHandler(this.IDH_TextChanged);
			// 
			// WRItem
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(836, 500);
			this.Controls.Add(this.IDH_GRPNAM);
			this.Controls.Add(this.IDH_GRPCOD);
			this.Controls.Add(this.IDH_ITMNAM);
			this.Controls.Add(this.IDH_ITMCOD);
			this.Controls.Add(this.btn2);
			this.Controls.Add(this.btn1);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.oSearchGrid);
			this.KeyPreview = true;
			this.Name = "WRItem";
			this.Text = "Item Search";
			((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private idh.win.controls.WR1Grid oSearchGrid;
		private System.Windows.Forms.TextBox IDH_GRPNAM;
		private System.Windows.Forms.TextBox IDH_GRPCOD;
		private System.Windows.Forms.TextBox IDH_ITMNAM;
		private System.Windows.Forms.TextBox IDH_ITMCOD;
		private System.Windows.Forms.Button btn2;
		private System.Windows.Forms.Button btn1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;

        protected void IDH_SelectedValueChanged(object sender, System.EventArgs e)
        {
            SelectedValueChanged_Internal(sender, e);
        }

        protected void IDH_TextChanged(object sender, System.EventArgs e)
        {
            TextChanged_Internal(sender, e);
        }
	}
}
