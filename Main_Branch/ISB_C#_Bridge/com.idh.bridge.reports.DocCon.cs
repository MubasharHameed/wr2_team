using Microsoft.VisualBasic;
using System;
//
// Created by SharpDevelop.
// User: Louis Viljoen
// Date: 2008/12/05
// Time: 03:35 PM
// 
// To change this template use Tools | Options | Coding | Edit Standard Headers.
//
using System.IO;
using System.Collections;

using com.idh.bridge.lookups;
using com.idh.bridge.form;
using com.idh.utils;

namespace com.idh.bridge.local.reports {
	public class DocCon : com.idh.bridge.reports.DocCon {
        private SAPbouiCOM.Form moForm;
        private DisposalOrder moDisposalOrder;

        public DocCon(DisposalOrder oDisposalOrder, string sWR1OrdType, string sFormType, bool bIsFromManager) 
            : base ( null, null, sWR1OrdType, sFormType, bIsFromManager) {
            moDisposalOrder = oDisposalOrder;

            msWR1OrdType = sWR1OrdType;
            msCallerForm = sFormType;
            mbFromManager = bIsFromManager;

            if (msWR1OrdType.Equals("WO")) {
                msHeadTbl = "[@IDH_JOBENTR]";
                msRowTbl = "[@IDH_JOBSHD]";
            } else {
                msHeadTbl = "[@IDH_DISPORD]";
                msRowTbl = "[@IDH_DISPROW]";
            }
            msLastConNum = "";
        }

        //public DocCon(ref IDHAddOns.idh.forms.Base oIDHForm, ref SAPbouiCOM.Form oForm, string sWR1OrdType, string sFormType, bool bIsFromManager) 
        //    : base(ref oIDHForm, ref oForm, sWR1OrdType, sFormType, bIsFromManager) {
        //}

        protected override void doDocConReport(string sCallerForm, string sReportFile, ref Hashtable oParams) { 
            if (moDisposalOrder!=null && moDisposalOrder.getWinForm() != null) {
                moDisposalOrder.doReport(sCallerForm, sReportFile, oParams);
            } else if ( moForm != null ) {
                  if (Config.INSTANCE.useNewReportViewer()==true){
                      IDHAddOns.idh.forms.Base.setSharedData(moForm, "IDH_RPTPARMS", oParams);
                      IDHAddOns.idh.forms.Base.setSharedData(moForm, "IDH_RPTNAME", sReportFile);
                    IDHAddOns.idh.forms.Base.PARENT.doOpenModalForm("IDH_CUSRPT", moForm,false);
                  }
                else{
                        IDHAddOns.idh.report.Base.doCallReportDefaults( moForm, sReportFile, "TRUE", "FALSE", "1", ref oParams, ref sCallerForm);
                }
            }
            
        }
    }
}
