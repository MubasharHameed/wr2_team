/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/06/22
 * Time: 08:55 AM
 *  
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
//using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Data.SqlClient;


using com.idh.dbObjects.User;
using com.bridge;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.win.controls;
using com.idh.win.forms;
using com.idh.controls;
using com.idh.utils;
using com.idh.dbObjects.strct;
using com.idh.forms;

using com.idh.win;
using com.idh.win.controls.SBO;

namespace com.idh.bridge.form {
    /// <summary>
    /// Description of com_idh_bridge_DisposalOrder.
    /// </summary>
    public class FormBridge {
        //** Set the Data Field Value
        public enum data_SetFrom {
            idh_SET_MANUALLY = -1, //'manually
            idh_SET_WITHCODE = 1, //'code using Item
            idh_SET_FROMDATABASE = 2, //'code using database
            idh_SET_FROMUSERFIELD = 3 //'code using user field
        }

        public static string Mode_OK = Translation.getTranslatedWord("Ok");
        public static string Mode_ADD = Translation.getTranslatedWord("Add");
        public static string Mode_VIEW = Translation.getTranslatedWord("View");
        public static string Mode_UPDATE = Translation.getTranslatedWord("Update");
        public static string Mode_FIND = Translation.getTranslatedWord("Find");
        public static string Mode_STARTUP = Translation.getTranslatedWord("DN");

        private bool mbInFormUpdate = false;
        public bool IsBusyUpdatingForm {
            get { return mbInFormUpdate; }
        }

        protected com.idh.dbObjects.DBBase moDBO;
        protected IDHAddOns.idh.forms.Base moIDHForm;
        protected SAPbouiCOM.Form moSBOForm;
        //protected forms.oo.Form moOOForm;
        protected System.Windows.Forms.Form moWinForm;
        protected SearchBase moSearch;
        public SearchBase SearchDialog {
            get { return moSearch; }
            set { moSearch = value; }
        }
        //protected Hashtable mhFieldRelations;
        protected Config moLookup;

        //protected System.Windows.Forms.Button moButton1;
        //public System.Windows.Forms.Button Button1 {
        protected SBOButton moButton1;
        public SBOButton Button1 {
            get { return moButton1; }
            set { moButton1 = value; }
        }

        protected FieldRelations moFieldRelations;
        public FieldRelations FieldRelations {
            get { return moFieldRelations; }
        }

        protected string[] moAlwaysOn;

        protected Hashtable mhUFFormats; // = new Hashtable();
        protected Hashtable mhValueSetFrom; // = new Hashtable();
        protected ArrayList maControls; // = new ArrayList();
        public ArrayList AllControls {
            get { return maControls; }
        }

        protected string msLastMode = "DN";
        public string Mode {
            get { return msLastMode; }
            set {
                if (value.Equals(Mode_UPDATE)) {
                    if (!IsBusyUpdatingForm) {
                        doSetUpdateMode();
                    }
                } else if (value.Equals(Mode_ADD)) {
                    doSetAddMode();
                } else if (value.Equals(Mode_FIND)) {
                    doSetFindMode();
                } else if (value.Equals(Mode_VIEW)) {
                    doSetViewMode();
                } else {
                    doSetViewMode();
                }

                if (msLastMode.Equals(Mode_UPDATE)) {
                    if (moButton1 != null)
                        moButton1.Text = FormBridge.Mode_UPDATE;
                } else if (msLastMode.Equals(Mode_ADD)) {
                    if (moButton1 != null)
                        moButton1.Text = FormBridge.Mode_ADD;
                } else if (msLastMode.Equals(Mode_FIND)) {
                    if (moButton1 != null)
                        moButton1.Text = FormBridge.Mode_FIND;
                } else if (msLastMode.Equals(Mode_VIEW)) {
                    if (moButton1 != null)
                        moButton1.Text = FormBridge.Mode_OK;
                } else {
                    if (moButton1 != null)
                        moButton1.Text = FormBridge.Mode_OK;
                }
                //msLastMode = value;
            }
        }

        protected FormBridge moParent;
        public FormBridge(FormBridge oParent) {
            moParent = oParent;
            moLookup = Config.INSTANCE;

            //mhUFFormats = oParent.mhUFFormats;
            //mhValueSetFrom = oParent.mhValueSetFrom;
            //maControls = oParent.maControls;

            mhUFFormats = new Hashtable();
            mhValueSetFrom = new Hashtable();
            maControls = new ArrayList();
        }

        public FormBridge() {
            moLookup = Config.INSTANCE;

            mhUFFormats = new Hashtable();
            mhValueSetFrom = new Hashtable();
            maControls = new ArrayList();
        }

        public void notImplimented() {
            throw new Exception("Not implemented yet.");
        }

        public virtual void setSBOForm(ref IDHAddOns.idh.forms.Base oIDHForm,
                                       ref SAPbouiCOM.Form oSBOForm) {
            moIDHForm = oIDHForm;
            moSBOForm = oSBOForm;
            moWinForm = null;
        }

        public virtual void setWinForm(ref System.Windows.Forms.Form oWinForm) {
            moWinForm = oWinForm;
            moIDHForm = null;
            moSBOForm = null;

            moFieldRelations = new FieldRelations();
        }

        public System.Windows.Forms.Form getWinForm() {
            return moWinForm;
        }

        public virtual void doSetInitialFormState() {

        }

        public virtual void doClose() {
        }
        #region dataObject
        //20120218		public void addDefaultValueOverride( string sKey, string sValue ){
        //20120218			moDBO.addDefaultValueOverride(sKey,sValue);
        //20120318		}
        public void doSetDefaultValue(string sKey, object oVal) {
            moDBO.doSetDefaultValue(sKey, oVal);
        }
        #endregion
        #region formControls
        /**
         * Register All the windows conttrols
         */
        public void registerControls() {
            if (moWinForm == null)
                return;

            registerControls(moWinForm);
        }
        private void registerControls(System.Windows.Forms.Control oControl) {
            foreach (System.Windows.Forms.Control c in oControl.Controls) {
                if (c is System.Windows.Forms.TabPage ||
                    c is System.Windows.Forms.TabControl ||
                    c is System.Windows.Forms.Panel ||
                    c is System.Windows.Forms.GroupBox)
                    registerControls(c);

                maControls.Add(c);
            }
        }

        /**
		 * get the windows control
		 */
        public System.Windows.Forms.Control getWinControl(string sFieldId) {
            if (moWinForm == null)
                return null;

            return (System.Windows.Forms.Control)getControl(sFieldId);
        }
        /**
		 * get the SBO Item
		 */
        public SAPbouiCOM.Item getSBOControl(string sFieldId) {
            if (moSBOForm == null)
                return null;

            return (SAPbouiCOM.Item)getControl(sFieldId);
        }
        /**
		 * get the windows control or the SBO Item
		 */
        public object getControl(string sFieldId) {
            if (moSBOForm != null) {
                try {
                    return moSBOForm.Items.Item(sFieldId);
                } catch (Exception ex) {
                    //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "");
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
                    return null;
                }
            } else if (moWinForm != null) {
                System.Windows.Forms.Control oControl = moFieldRelations.getControlFromFormField(sFieldId);
                if (oControl != null) {
                    return oControl;
                } else {
                    System.Windows.Forms.Control[] oControls = moWinForm.Controls.Find(sFieldId, true);
                    if (oControls != null && oControls.Length > 0) {
                        return oControls[0];
                    } else
                        return null;
                }
            } else {
                return null;
            }
        }
        /**
		 * Set to Find Mode
		 */
        protected virtual void doSetFindMode() { msLastMode = Mode_FIND; }

        /**
		 * Set to Ok Mode
		 */
        protected virtual void doSetOkMode() { msLastMode = Mode_OK; }

        /**
		 * Set to View Mode
		 */
        protected virtual void doSetViewMode() { msLastMode = Mode_VIEW; }

        /**
		 * Set to Update Mode
		 */
        protected virtual void doSetUpdateMode() { msLastMode = Mode_UPDATE; }

        /**
		 * Set to Add Mode
		 */
        protected virtual void doSetAddMode() { msLastMode = Mode_ADD; }

        protected void EnableItem(string sFieldId, bool bEnable) {
            if (moIDHForm != null) {
                moIDHForm.setEnableItem(moSBOForm, bEnable, sFieldId);
            } else if (moWinForm != null) {
                System.Windows.Forms.Control oControl = getWinControl(sFieldId);
                if (oControl != null)
                    oControl.Enabled = bEnable;
            }
        }

        protected void doSetFocus(string sFieldId) {
            if (moIDHForm != null) {
                moIDHForm.doSetFocus(moSBOForm, sFieldId);
            } else if (moWinForm != null) {
                System.Windows.Forms.Control oControl = getWinControl(sFieldId);
                if (oControl != null)
                    oControl.Focus();
            }
        }

        public void doReactivateLastControl() {
            if (moWinForm != null && moWinForm is AppForm) {
                ((AppForm)moWinForm).doReactivateLastControl();
            }
        }

        protected void EnableItems(ref string[] oFormItems, bool bEnable) {
            if (moIDHForm != null) {
                moIDHForm.setEnableItems(moSBOForm, bEnable, ref oFormItems);
            } else if (moWinForm != null) {
                foreach (string sFieldId in oFormItems) {
                    System.Windows.Forms.Control oControl = getWinControl(sFieldId);
                    if (oControl != null)
                        oControl.Enabled = bEnable;
                }
            }
        }

        protected void EnableAllEditItems(ref string[] oExclude) {
            if (moIDHForm != null) {
                moIDHForm.EnableAllEditItems(moSBOForm, ref oExclude);
            } else if (moWinForm != null) {
                foreach (System.Windows.Forms.Control c in maControls) {
                    if (!(c is System.Windows.Forms.Label)) {
                        if (Array.IndexOf(moAlwaysOn, c.Name) != -1 ||
                             Array.IndexOf(oExclude, c.Name) == -1)
                            c.Enabled = true;
                        else
                            c.Enabled = false;
                    }
                }
            }
        }

        protected void DisableAllEditItems(ref string[] oExclude) {
            if (moIDHForm != null) {
                moIDHForm.DisableAllEditItems(moSBOForm, ref oExclude);
            } else if (moWinForm != null) {
                foreach (System.Windows.Forms.Control c in maControls) {
                    if (!(c is System.Windows.Forms.Label)) {
                        try {
                            if (Array.IndexOf(moAlwaysOn, c.Name) != -1 ||
                                Array.IndexOf(oExclude, c.Name) != -1)
                                c.Enabled = true;
                            else
                                c.Enabled = false;
                        } catch (Exception) {

                        }
                    }
                }
            }
        }

        public void doSetVisible(ref string[] oItems, bool bIsVisible, int iPane) {
            if (moSBOForm != null) {
                moSBOForm.Freeze(true);
                try {
                    SAPbouiCOM.Item oItem = null;
                    for (int iCount = 0; iCount <= oItems.GetLength(0) - 1; iCount++) {
                        oItem = moSBOForm.Items.Item(oItems[iCount]);
                        oItem.Visible = bIsVisible;
                        oItem.FromPane = iPane;
                        oItem.ToPane = iPane;
                    }
                } catch (Exception) {
                } finally {
                    moSBOForm.Freeze(false);
                }
            } else if (moWinForm != null) {
                for (int iCount = 0; iCount <= oItems.GetLength(0) - 1; iCount++) {
                    System.Windows.Forms.Control oControl = getWinControl(oItems[iCount]);
                    if (oControl != null)
                        oControl.Visible = bIsVisible;
                }
            }
        }

        public void doSetVisible(string sItem, bool bIsVisible, int iPane) {
            if (moSBOForm != null) {
                moSBOForm.Freeze(true);
                try {
                    SAPbouiCOM.Item oItem = null;
                    oItem = moSBOForm.Items.Item(sItem);
                    oItem.Visible = bIsVisible;

                    if (iPane >= 0) {
                        oItem.FromPane = iPane;
                        oItem.ToPane = iPane;
                    }
                } catch (Exception) {
                } finally {
                    moSBOForm.Freeze(false);
                }
            } else if (moWinForm != null) {
                System.Windows.Forms.Control oControl = getWinControl(sItem);
                if (oControl != null)
                    oControl.Visible = bIsVisible;
            }
        }

        public void doBlankOutItems(ref string[] oItems, int iBlankColor) {
            if (moSBOForm != null) {
                moSBOForm.Freeze(true);
                try {
                    SAPbouiCOM.Item oItem = null;
                    for (int iCount = 0; iCount <= oItems.GetLength(0) - 1; iCount++) {
                        oItem = moSBOForm.Items.Item(oItems[iCount]);
                        oItem.ForeColor = iBlankColor;
                        oItem.BackColor = iBlankColor;
                    }
                } catch (Exception) {
                } finally {
                    moSBOForm.Freeze(false);
                }
            } else if (moWinForm != null) {
                System.Windows.Forms.Control oControl;
                for (int iCount = 0; iCount <= oItems.GetLength(0) - 1; iCount++) {
                    oControl = getWinControl(oItems[iCount]);
                    if (oControl != null) {
                        if (oControl.GetType() == typeof(System.Windows.Forms.TextBox)) {
                            ((System.Windows.Forms.TextBox)oControl).PasswordChar = '#';
                        }
                        //System.Drawing.Color oColor = System.Drawing.SystemColors.InactiveCaption; // (iBlankColor);
                        //oControl.ForeColor = oColor;
                        //oControl.BackColor = oColor;
                    }
                }
            }
        }

        public bool isVisible(string sFieldId) {
            if (moSBOForm != null) {
                try {
                    if (moSBOForm.Items.Item(sFieldId).Visible == true)
                        return true;
                } catch (Exception) {
                }
            } else if (moWinForm != null) {
                //Control oControl = moFieldRelations.getControlFromFormField(sFieldId);
                System.Windows.Forms.Control oControl = getWinControl(sFieldId);
                if (oControl != null)
                    return oControl.Visible;
            }
            return false;
        }

        #endregion
        #region formControls_data	
        public void setTitle(string sTitle) {
            if (moSBOForm != null) {
                moSBOForm.Title = sTitle;
            } else if (moWinForm != null) {
                moWinForm.Text = sTitle;
            }
        }
        public virtual void doLinkItems() { }

        public void setUFValue(string sFieldId, object oVal) {
            setUFValue(sFieldId, oVal, true, false);
        }
        public void setUFValue(string sFieldId, object oVal, bool bFldsSame, bool bCheckFind) {
            if (moIDHForm != null) {
                moIDHForm.setUFValue(moSBOForm, sFieldId, oVal, bFldsSame, bCheckFind);
            } else if (moWinForm != null) {
                setFormFieldValue(sFieldId, (oVal == null ? "" : oVal.ToString()));
                doMarkFormFieldSetFrom(sFieldId, data_SetFrom.idh_SET_FROMUSERFIELD);
            }
        }

        public string getUFValue(string sFieldId) {
            return getUFValue(sFieldId, false);
        }
        public string getUFValue(string sFieldId, bool bCheckFind) {
            if (moIDHForm != null) {
                return Conversions.ToString(moIDHForm.getUFValue(moSBOForm, sFieldId, bCheckFind));
            } else if (moWinForm != null) {
                return getFormFieldValue(sFieldId);
            }
            return "";
        }

        /**
		 * Get the value from Item directly
		 */
        public string getItemValue(string sFieldId) {
            if (moIDHForm != null) {
                return moIDHForm.getItemValue(moSBOForm, sFieldId);
            } else if (moWinForm != null) {
                return getFormFieldValue(sFieldId);
            }
            return "";
        }

        /**
         * Set the value of the Item directly
         */
        public void setItemValue(string sFieldId, string sValue) {
            if (moIDHForm != null) {
                moIDHForm.setItemValue(moSBOForm, sFieldId, sValue, false);
            } else if (moWinForm != null) {
                setFormFieldValue(sFieldId, sValue);
            }
        }

        /*
		 * Link the DB Field with the Form Field
		 */
        public void doAddFormDF(string sFieldId, string sDBItem) {
            if (moIDHForm != null) {
                moIDHForm.doAddFormDF(moSBOForm, sFieldId, sDBItem);
            } else {
                System.Windows.Forms.Control[] oControls;
                oControls = moWinForm.Controls.Find(sFieldId, true);
                if (oControls != null && oControls.Length > 0) {
                    moFieldRelations.doAddWinField(sFieldId, sDBItem, oControls[0]);
                }
            }
        }

        /**
         * Call to Handle preset options if required - return false if the set must be stopped
         */
        protected virtual bool doPreUpdateDBFieldFromUser(string sControlName, string sValue) {
            return true;
        }
        /**
         * Call to Handle post set options if required - return false if the set must be stopped
         */
        public virtual bool doPostUpdateDBFieldFromUser(string sControlName, string sValue) {
            return true;
        }
        /**
         * Update the DBField values as well as all other linked Form field Values
         */
        public virtual bool doUpdateDBFieldFromUser(System.Windows.Forms.Control oControl) {
            bool bResult = true;
            if (moIDHForm != null) {
                notImplimented();
                return false;
            } else
            if (moWinForm != null) {
                if (oControl != null) {
                    bool bIsTextBox = oControl is System.Windows.Forms.TextBox;
                    if ((bIsTextBox && ((System.Windows.Forms.TextBox)oControl).Modified) || !(oControl is System.Windows.Forms.TextBox)) {
                        string sValue = getFormFieldValue(oControl);
                        if (doPreUpdateDBFieldFromUser(oControl.Name, sValue)) {
                            doMarkFormFieldSetFromUser(oControl.Name);

                            int iIndex = moFieldRelations.indexOfFormField(oControl.Name);
                            if (iIndex >= 0) {
                                string sDBField = moFieldRelations.getDBFieldId(iIndex);
                                moDBO.setValue(sDBField, sValue);

                                setAllLinkedFormFields(oControl, sDBField, sValue);
                            }


                            return doPostUpdateDBFieldFromUser(oControl.Name, sValue);
                        }
                    }
                }
            }
            return bResult;
        }

        /**
         * Validate the Data entered by the user
         */
        public virtual bool doValidateUserInput(string sFieldId, string sValue) {
            if (moIDHForm != null) {
                notImplimented();
            } else if (moWinForm != null) {
                int iIndex = moFieldRelations.indexOfFormField(sFieldId);
                if (iIndex >= 0) {
                    string sDBField = moFieldRelations.getDBFieldId(iIndex);
                    string sResult = moDBO.doValidation(sDBField, sValue);
                    if (sResult.Length > 0) {
                        //DataHandler.INSTANCE.doError("User: " + sResult);
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError(sResult, "ERUSGEN", new String[] { idh.bridge.Translation.getTranslatedWord(sResult) });
                        return false;
                    }
                }
            }
            return true;
        }

        ///**
        // * Update the linked Form field Values
        // */
        //public void doUpdateAllLinkedFormFields(Control oControl)
        //{
        //    if (oControl == null)
        //        return;

        //    if ((oControl is TextBox && ((TextBox)oControl).Modified) ||
        //        !(oControl is TextBox))
        //    {
        //        string sValue = getFormFieldValue(oControl);
        //        int i = moFieldRelations.indexOfFormField(oControl.Name);
        //        if (i >= 0)
        //        {
        //            string sDBFieldName = moFieldRelations.getDBFieldId(i);

        //            ArrayList oAr = moFieldRelations.getControlsFromDBField(sDBFieldName);
        //            if (oAr.Count > 0)
        //            {
        //                Control oWControl;
        //                for (i = 0; i < oAr.Count; i++)
        //                {
        //                    oWControl = (Control)oAr[i];
        //                    if (oWControl != null && oWControl != oControl)
        //                    {
        //                        setFormFieldValue(oWControl, (sValue == null ? null : sValue));
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        /*
         * Set the Data Values - The DB value and the Form Value will be set - else the doUpdateFormChangedData function will have to be called to synch the two
         */
        public void setFormDFValue(string sDBItem, object oVal) {
            setFormDFValue(sDBItem, oVal, false);
        }

        /*
         * Set the Data Values - The DB value and the Form Value will be set - else the doUpdateFormChangedData function will have to be called to synch the two
         */
        public void setFormDFValue(string sDBItem, object oVal, bool bCheckFind) {
            setFormDFValue(sDBItem, oVal, bCheckFind, false, data_SetFrom.idh_SET_FROMDATABASE);
        }

        /*
         * Set the Data Values - The DB value and the Form Value will be set - else the doUpdateFormChangedData function will have to be called to synch the two
         */
        public void setFormDFValue(string sDBItem, object oVal, bool bCheckFind, bool bSetUpdate, data_SetFrom iSetFrom) {
            if (moIDHForm != null) {
                moIDHForm.setFormDFValue(moSBOForm, sDBItem, oVal, bCheckFind, bSetUpdate, (int)iSetFrom);
            } else {
                moDBO.setValue(sDBItem, oVal);
                setAllLinkedFormFields(sDBItem, oVal);

                string sFormItemId = getItemIDFromDBField(sDBItem);
                if (sFormItemId != null)
                    doMarkFormFieldSetFrom(sFormItemId, iSetFrom);
            }
        }
        /*
         * Set the Data Values and indicate that it was set by the user
         * - The DB value and the Form Value will be set - else the doUpdateFormChangedData function will have to be called to synch the two
         */
        public void setFormDFValueFromUser(string sDBItem, object oVal) {
            setFormDFValue(sDBItem, oVal, false, true, data_SetFrom.idh_SET_MANUALLY);
        }

        /*
		 * Get the Data Values
		 */
        public int getFormDFValueAsInt(string sDBItem) {
            object oVal = getFormDFValue(sDBItem, false);
            return Conversions.ToInt(oVal); //Conversions.objectToInt( oVal );
        }
        /*
		 * Get the Data Values As a Double
		 */
        public double getFormDFValueAsDouble(string sDBItem) {
            object oVal = getFormDFValue(sDBItem, false);
            return Conversions.ToDouble(oVal); //Conversions.objectToDouble( oVal );
        }
        /*
		 * Get the Data Values As a DateTime
		 */
        public DateTime getFormDFValueAsDateTime(string sDBItem) {
            object oVal = getFormDFValue(sDBItem, false);
            return Conversions.ToDateTime(oVal); //Conversions.objectToDateTime( oVal );
        }
        /*
		 * Get the Data Values As String - but do not return a null - rather a blank
		 */
        public string getFormDFValueAsStringNZ(string sDBItem) {
            string sVal = getFormDFValueAsString(sDBItem);
            if (sVal == null)
                return "";
            else
                return sVal;
        }
        /*
		 * Get the Data Values As String 
		 */
        public string getFormDFValueAsString(string sDBItem) {
            object oVal = getFormDFValue(sDBItem, false);
            return Conversions.ToString(oVal);
        }

        /*
		 * Get the Data Values
		 */
        public object getFormDFValue(string sDBItem) {
            return getFormDFValue(sDBItem, false);
        }
        /*
		 * Get the Data Values
		 */
        public object getFormDFValue(string sDBItem, bool bCheckFind) {
            if (moIDHForm != null) {
                return moIDHForm.getFormDFValue(moSBOForm, sDBItem, bCheckFind);
            } else {
                return moDBO.getValue(sDBItem);
            }
        }

        /**
         * Set All Forms Field Values Linked to a DB Field
         */
        public void setAllLinkedFormFields(string sDBItem, object oVal) {
            setAllLinkedFormFields(null, sDBItem, oVal);
        }

        /**
         * Set All Forms Field Values Linked to a DB Field
         */
        public void setAllLinkedFormFields(System.Windows.Forms.Control oControl, string sDBItem, object oVal) {
            if (moIDHForm != null) {
                notImplimented();
            } else {
                ArrayList oAr = moFieldRelations.getControlsFromDBField(sDBItem);
                if (oAr.Count > 0) {
                    System.Windows.Forms.Control oWControl;
                    for (int i = 0; i < oAr.Count; i++) {
                        oWControl = (System.Windows.Forms.Control)oAr[i];
                        if (oControl == null || oControl != oWControl)
                            setFormFieldValue(oWControl, (oVal == null ? null : oVal.ToString()));
                    }
                }
            }
        }

        /**
         * Set the Formatter for a UF Field
         */
        public void setUFFormat(string sControlName, com.idh.dbObjects.strct.DBFieldInfo oInfo) {
            mhUFFormats.Add(sControlName, oInfo);
        }

        /**
         * set the RadioButton status according to the value
         */
        public virtual void setRadioValue(System.Windows.Forms.RadioButton oRadio, string sValue) {
            if (sValue == "y" || sValue == "Y")
                oRadio.Checked = true;
            else
                oRadio.Checked = false;
        }

        /**
		 * Set the Form Field value
		 */
        public void setFormFieldValue(System.Windows.Forms.Control oControl, string sValue) {
            if (moIDHForm != null) {
                notImplimented();
            } else {
                if (oControl != null) {
                    if (oControl is System.Windows.Forms.ComboBox) {
                        System.Windows.Forms.ComboBox oCombo = oControl as System.Windows.Forms.ComboBox;
                        if (oCombo != null) {
                            object oVal;
                            string sWrkValue;
                            for (int i = 0; i < oCombo.Items.Count; i++) {
                                oVal = oCombo.Items[i];
                                if (oVal is ValuePair) {
                                    ValuePair oPair = oVal as ValuePair;
                                    sWrkValue = oPair.Value1;
                                } else
                                    sWrkValue = oVal.ToString();

                                if (sWrkValue.Equals(sValue)) {
                                    oCombo.SelectedIndex = i;
                                    return;
                                }
                            }
                            oCombo.Text = sValue;
                        }
                    } else if (oControl is LPVComboBox) {
                        LPVComboBox oCombo = oControl as LPVComboBox;
                        if (oCombo != null) {
                            object oVal;
                            string sWrkValue;
                            for (int i = 0; i < oCombo.Items.Count; i++) {
                                oVal = oCombo.Items[i];
                                if (oVal is ValuePair) {
                                    ValuePair oPair = oVal as ValuePair;
                                    sWrkValue = oPair.Value1;
                                } else
                                    sWrkValue = oVal.ToString();

                                if (sWrkValue.Equals(sValue)) {
                                    oCombo.SelectedIndex = i;
                                    //oCombo.Text = sValue;
                                    return;
                                }
                            }
                            //    oCombo.Text = sValue;

                            string sDBId = getDBFieldFromItemID(oControl.Name);
                            DBFieldInfo oInfo = null;
                            if (sDBId != null) {
                                oInfo = moDBO.getFieldInfo(sDBId);
                            } else if (mhUFFormats.ContainsKey(oControl.Name)) {
                                oInfo = (DBFieldInfo)mhUFFormats[oControl.Name];
                            }

                            if (oInfo != null) {
                                SAPbobsCOM.BoFieldTypes oFormat = oInfo.Type;
                                SAPbobsCOM.BoFldSubTypes oSubFormat = oInfo.SubType;

                                string sFormatValue = Formatter.doFormatForUI(sValue, oFormat, oSubFormat);
                                oControl.Text = sFormatValue;
                            } else
                                oControl.Text = sValue;
                        }
                    } else if (oControl is System.Windows.Forms.CheckBox) {
                        System.Windows.Forms.CheckBox oCheckBox = oControl as System.Windows.Forms.CheckBox;
                        if (sValue != null && sValue.Length > 0) {
                            sValue = sValue.ToLower();
                            if (sValue[0] == 'y' || sValue[0] == 't')
                                oCheckBox.Checked = true;
                            else
                                oCheckBox.Checked = false;
                        }
                    } else if (oControl is LPVCheckBox) {
                        LPVCheckBox oCheckBox = oControl as LPVCheckBox;
                        if (sValue != null && sValue.Length > 0) {
                            sValue = sValue.ToLower();
                            if (sValue[0] == 'y' || sValue[0] == 't')
                                oCheckBox.Checked = true;
                            else
                                oCheckBox.Checked = false;
                        }
                    } else if (oControl is System.Windows.Forms.RadioButton) {
                        System.Windows.Forms.RadioButton oRadio = oControl as System.Windows.Forms.RadioButton;
                        if (sValue != null && sValue.Length > 0) {
                            sValue = sValue.ToLower();
                            setRadioValue(oRadio, sValue);
                        }
                    } else if (oControl is LPVRadioButton) {
                        LPVRadioButton oRadio = oControl as LPVRadioButton;
                        if (sValue != null && sValue.Length > 0) {
                            sValue = sValue.ToLower();
                            setRadioValue(oRadio, sValue);
                        }
                    } else if (oControl is System.Windows.Forms.TextBox) {
                        string sDBId = getDBFieldFromItemID(((System.Windows.Forms.TextBox)oControl).Name);
                        DBFieldInfo oInfo = null;
                        if (sDBId != null) {
                            oInfo = moDBO.getFieldInfo(sDBId);
                        } else if (mhUFFormats.ContainsKey(((System.Windows.Forms.TextBox)oControl).Name)) {
                            oInfo = (DBFieldInfo)mhUFFormats[((System.Windows.Forms.TextBox)oControl).Name];
                        }

                        if (oInfo != null) {
                            SAPbobsCOM.BoFieldTypes oFormat = oInfo.Type;
                            SAPbobsCOM.BoFldSubTypes oSubFormat = oInfo.SubType;

                            string sFormatValue = Formatter.doFormatForUI(sValue, oFormat, oSubFormat);
                            oControl.Text = sFormatValue;
                        } else
                            oControl.Text = sValue;
                    } else if (oControl is LPVTextBox || oControl is SBOTextBox) {
                        string sDBId = getDBFieldFromItemID(((LPVTextBox)oControl).Name);
                        DBFieldInfo oInfo = null;
                        if (sDBId != null) {
                            oInfo = moDBO.getFieldInfo(sDBId);
                        } else if (mhUFFormats.ContainsKey(((LPVTextBox)oControl).Name)) {
                            oInfo = (DBFieldInfo)mhUFFormats[((LPVTextBox)oControl).Name];
                        }

                        if (sDBId == IDH_DISPROW._JCost || sDBId == IDH_DISPROW._TCharge) {
                            string ssValue = sValue;
                        }

                        if (oInfo != null) {
                            SAPbobsCOM.BoFieldTypes oFormat = oInfo.Type;
                            SAPbobsCOM.BoFldSubTypes oSubFormat = oInfo.SubType;

                            string sFormatValue = Formatter.doFormatForUI(sValue, oFormat, oSubFormat);
                            oControl.Text = sFormatValue;
                        } else
                            oControl.Text = sValue;
                    } else
                        oControl.Text = sValue;

                    //doUpdateAllLinkedFormFields(oControl);
                }
            }
        }

        /**
		 * Set the Form Field value
		 */
        private void setFormFieldValue(string sFieldId, string sValue) {
            if (moIDHForm != null) {
                notImplimented();
            } else {
                System.Windows.Forms.Control oControl = moFieldRelations.getControlFromFormField(sFieldId);
                if (oControl != null) {
                    setFormFieldValue(oControl, sValue);
                } else {
                    System.Windows.Forms.Control[] oControls = moWinForm.Controls.Find(sFieldId, true);
                    if (oControls != null && oControls.Length > 0) {
                        setFormFieldValue(oControls[0], sValue);
                    }
                }
            }
        }

        /**
         * Get a FormField value
         * The same as getUFValue
         */
        private string getFormFieldValue(string sFieldId) {
            if (moIDHForm != null) {
                return moIDHForm.getItemValue(moSBOForm, sFieldId);
            } else {
                string sValue = null;
                System.Windows.Forms.Control oControl = moFieldRelations.getControlFromFormField(sFieldId);
                if (oControl != null) {
                    sValue = getFormFieldValue(oControl);
                } else {
                    System.Windows.Forms.Control[] oControls = moWinForm.Controls.Find(sFieldId, true);
                    if (oControls != null && oControls.Length > 0) {
                        sValue = getFormFieldValue(oControls[0]);
                    }
                }
                return sValue;
            }
        }

        /**
		 * Get a FormField value
		 */
        public string getFormFieldValue(System.Windows.Forms.Control oControl) {
            string sValue = null;
            if (oControl is System.Windows.Forms.ComboBox) {
                System.Windows.Forms.ComboBox oCombo = oControl as System.Windows.Forms.ComboBox;
                if (oCombo != null) {
                    object oVal = oCombo.SelectedItem;
                    if (oVal != null) {
                        if (oVal is ValuePair) {
                            ValuePair oPair = oVal as ValuePair;
                            sValue = oPair.Value1;
                        } else
                            sValue = oCombo.Text;
                    } else
                        sValue = oCombo.Text;
                }
            } else if (oControl is LPVComboBox || oControl is SBOComboBox) {
                LPVComboBox oCombo = oControl as LPVComboBox;
                if (oCombo != null) {
                    //object oVal = oCombo.SelectedItem;
                    //if (oVal != null) {
                    //    if (oVal is ValuePair) {
                    //        ValuePair oPair = oVal as ValuePair;
                    //        sValue = oPair.Value;
                    //    } else
                    //        sValue = oCombo.Text;
                    //} else
                    sValue = oCombo.Text;
                }
            } else if (oControl is System.Windows.Forms.CheckBox) {
                System.Windows.Forms.CheckBox oCheckBox = oControl as System.Windows.Forms.CheckBox;
                if (oCheckBox.Checked)
                    sValue = "Y";
                else
                    sValue = "N";
            } else
                sValue = oControl.Text;
            return sValue;
        }

        /**
          * sItemID is the form item ID  
          * -1 - manually
          * 1 - code using Item
          * 2 - code using database
          * 3 - code using user field
          */
        public bool wasFormFieldSetWithCode(string sItemID) {
            if (moIDHForm != null) {
                return moIDHForm.wasSetWithCode(moSBOForm, sItemID);
            } else {
                if (mhValueSetFrom.ContainsKey(sItemID)) {
                    data_SetFrom iResult = (data_SetFrom)mhValueSetFrom[sItemID];
                    return (iResult > data_SetFrom.idh_SET_MANUALLY);
                } else
                    return true;
            }
        }

        /**
          * sItemID is the form item ID  
          * -1 - manually
          * 1 - code using Item
          * 2 - code using database
          * 3 - code using user field
          */
        public bool wasFormFieldSetByUser(string sItemID) {
            if (moIDHForm != null) {
                return moIDHForm.wasSetByUser(moSBOForm, sItemID);
            } else {
                if (mhValueSetFrom.ContainsKey(sItemID)) {
                    data_SetFrom iResult = (data_SetFrom)mhValueSetFrom[sItemID];
                    return (iResult == data_SetFrom.idh_SET_MANUALLY);
                } else
                    return false;
            }
        }

        /**
          * sItemID is the form item ID  
          * -1 - manually
          * 1 - code using Item
          * 2 - code using database
          * 3 - code using user field
          */
        public data_SetFrom wasFormFieldSetFrom(string sItemID) {
            if (moIDHForm != null) {
                return (data_SetFrom)moIDHForm.wasSetFrom(moSBOForm, sItemID);
            } else {
                if (mhValueSetFrom.ContainsKey(sItemID)) {
                    return (data_SetFrom)mhValueSetFrom[sItemID];
                } else
                    return data_SetFrom.idh_SET_WITHCODE;
            }
        }

        /**
          * This is an indicator to indicate from where the form control has been updated. 
          * sItemID is the form item ID
          * sDBField id the DB Field Id
          * -1 - manually
          * 1 - code using Item
          * 2 - code using database
          * 3 - code using user field
          */
        public void doMarkFormFieldSetFromUserUseDBField(string sDBField) {
            string sFormItemId = getItemIDFromDBField(sDBField);
            if (sFormItemId != null)
                doMarkFormFieldSetFromUser(sFormItemId);
        }
        public void doMarkFormFieldSetFromUser(string sItemID) {
            if (moIDHForm != null) {
                moIDHForm.setWasSetFromUser(moSBOForm, sItemID);
            } else {
                if (mhValueSetFrom.ContainsKey(sItemID))
                    mhValueSetFrom[sItemID] = data_SetFrom.idh_SET_MANUALLY;
                else
                    mhValueSetFrom.Add(sItemID, data_SetFrom.idh_SET_MANUALLY);
            }
        }

        /**
          * This is an indicator to indicate from where the form control has been updated. 
          * sItemID is the form item ID  
          * sDBField id the DB Field Id
          * -1 - manually
          * 1 - code using Item
          * 2 - code using database
          * 3 - code using user field
          */
        public void doMarkFormFieldSetFromUseDBField(string sDBField, data_SetFrom iFrom) {
            string sFormItemId = getItemIDFromDBField(sDBField);
            if (sFormItemId != null)
                doMarkFormFieldSetFrom(sFormItemId, iFrom);
        }
        public void doMarkFormFieldSetFrom(string sItemID, data_SetFrom iFrom) {
            if (moIDHForm != null) {
                moIDHForm.setWasSetInternally(moSBOForm, sItemID, (IDHAddOns.idh.forms.Base.data_SetFrom)iFrom);
            } else {
                if (mhValueSetFrom.ContainsKey(sItemID))
                    mhValueSetFrom[sItemID] = iFrom;
                else
                    mhValueSetFrom.Add(sItemID, iFrom);
            }
        }

        /*
		 * This is called after the Form has been updated from the DataSource
		 */
        protected virtual void doDataLoaded() { }

        /*
		 * Do Update Forms changed Fields
		 */
        public virtual void doUpdateFormChangedData() {
            if (moIDHForm != null) {
                notImplimented();
            } else {
                ArrayList oChanged;
                string sValue;
                try {
                    oChanged = moDBO.RecentChangedFields;
                    foreach (string sDBItem in oChanged) {
                        try {
                            sValue = moDBO.getValueAsStringNZ(sDBItem);
                            ArrayList oControls = moFieldRelations.getControlsFromDBField(sDBItem);
                            if (oControls != null && oControls.Count > 0) {
                                //System.Windows.Forms.Control oControl = moFieldRelations.getControlFromDBField(sDBItem);
                                foreach (System.Windows.Forms.Control oControl in oControls)
                                    setFormFieldValue(oControl, sValue);
                            }
                        } catch (Exception) {
                        }
                    }
                } catch (Exception) {
                }
            }
            moDBO.doClearRecentChanged();
        }

        /*
		 * Use this for the Windows Form
		 */
        public virtual void doUpdateFormData() {
            if (moIDHForm != null) {
                notImplimented();
            } else {
                string sVal;
                mbInFormUpdate = true;
                try {
                    for (int i = 0; i < moFieldRelations.Count; i++) {
                        sVal = moDBO.getValueAsStringNZ(moFieldRelations.getDBFieldId(i));
                        setFormFieldValue(moFieldRelations.getControl(i), sVal);
                    }
                    doDataLoaded();
                    mbInFormUpdate = false;
                } catch (Exception) {
                }

                mhValueSetFrom.Clear();
            }
        }

        /*
		 * Update the Data value to the value of the Form field
		 * This is called when the user changed the value of the Form Control
		 */
        public virtual void doUpdateDBFieldValue(string sFieldId) {
            if (moIDHForm != null) {
                notImplimented();
            } else {
                int iIndex = moFieldRelations.indexOfFormField(sFieldId);
                if (iIndex >= 0) {
                    string sDBField = moFieldRelations.getDBFieldId(iIndex);
                    moDBO.setValue(sDBField, getFormFieldValue(sFieldId));
                }
            }
        }

        /*
		 * Update the Data value from the value of the Form field
		 * This is called when the user changed the value of the Form Control
		 */
        public virtual bool doUpdateDBFieldValueUsingFormFieldId(string sFieldId, string sValue) {
            if (moIDHForm != null) {
                notImplimented();
            } else {
                int iIndex = moFieldRelations.indexOfFormField(sFieldId);
                if (doUpdateDBFieldValue(iIndex, sValue)) {
                    return true;
                }
            }
            return false;
        }

        /*
		 * Update the Data value to the value of the Form field
		 */
        public virtual bool doUpdateDBFieldValue(int iIndex, string sValue) {
            if (moIDHForm != null) {
                notImplimented();
            } else {
                if (iIndex >= 0) {
                    string sDBField = moFieldRelations.getDBFieldId(iIndex);
                    moDBO.setValue(sDBField, sValue);
                    return true;
                }
            }
            return false;
        }

        public void doFillCombo(string sItemID, string sTable, string sKeyVal, string sValDesc) {
            doFillCombo(sItemID, sTable, sKeyVal, sValDesc, null, null, false);
        }

        public void doFillCombo(string sItemID, string sTable, string sKeyVal, string sValDesc, string sWhere, string sOrder, string sBlankName) {
            object oForm = (moWinForm != null ? (object)moWinForm : (object)moSBOForm);
            com.idh.bridge.utils.Combobox.doFillCombo(oForm, sItemID, sTable, sKeyVal, sValDesc, sWhere, sOrder, sBlankName);
        }

        public void doFillCombo(string sItemID, string sTable, string sKeyVal, string sValDesc, string sWhere, string sOrder, bool bAddAny) {
            object oForm = (moWinForm != null ? (object)moWinForm : (object)moSBOForm);
            com.idh.bridge.utils.Combobox.doFillCombo(oForm, sItemID, sTable, sKeyVal, sValDesc, sWhere, sOrder, bAddAny);
        }

        public string getDBFieldFromItemID(string sFormItemID) {
            if (moIDHForm != null) {
                notImplimented();
            } else {
                int iIndex = moFieldRelations.indexOfFormField(sFormItemID);
                if (iIndex >= 0) {
                    return moFieldRelations.getDBFieldId(iIndex);
                }
            }
            return null;
        }

        public string getItemIDFromDBField(string sDBItem) {
            if (moIDHForm != null) {
                notImplimented();
            } else {
                int iIndex = moFieldRelations.indexOfDBField(sDBItem);
                if (iIndex >= 0) {
                    return moFieldRelations.getFormFieldId(iIndex);
                }
            }

            //			if ( moWinForm != null && mhFieldRelations!=null ){
            //				if ( mhFieldRelations.ContainsValue(sDBItem) ) { 
            //					string sDBField;
            //					string sFormField;
            //					foreach (DictionaryEntry oField in mhFieldRelations ){
            //						sDBField = (string)oField.Value;
            //						sFormField = (string)oField.Key;
            //						if ( sDBItem.Equals(sDBField) ){
            //							return sFormField;
            //						}
            //					}
            //				}
            //			}
            return null;
        }

        public string getDialogValue(string sField) {
            if (moSBOForm != null) {
                return getDialogValue(moSBOForm, sField);
            } else if (moSearch != null) {
                return getDialogValue(moSearch, sField);
            } else {
                throw new Exception("This can only be called from a SBO application");
            }
        }
        public string getDialogValue(SAPbouiCOM.Form oSearch, string sField) {
            object oVal = IDHAddOns.idh.forms.Base.getSharedData(oSearch, sField);
            if (oVal != null) {
                if (oVal is string)
                    return (string)oVal;
                else
                    return oVal.ToString();
            } else
                return "";
            //return null;
        }
        public string getDialogValue(SearchBase oSearch, string sField) {
            object oVal = oSearch.getReturnValue(sField);
            if (oVal != null) {
                if (oVal is string)
                    return (string)oVal;
                else
                    return oVal.ToString();
            } else
                return "";
            //return null;
        }
        public int getDialogValueAsInt(string sField) {
            object oVal = getDialogValue(sField);
            return Conversions.ToInt(oVal);
        }
        public double getDialogValueAsDouble(string sField) {
            object oVal = getDialogValue(sField);
            return Conversions.ToDouble(oVal);
        }
        public DateTime getDialogValueAsDateTime(string sField) {
            object oVal = getDialogValue(sField);
            return Conversions.ToDateTime(oVal);
        }

        public object getDialogValueRAW(string sField) {
            if (moSBOForm != null) {
                return getDialogValueRAW(moSBOForm, sField);
            } else if (moSearch != null) {
                return getDialogValueRAW(moSearch, sField);
            } else {
                throw new Exception("This can only be called from a SBO application");
            }
        }
        public virtual object getDialogValueRAW(SAPbouiCOM.Form oSearch, string sField) {
            return IDHAddOns.idh.forms.Base.getSharedData(oSearch, sField);
        }
        public virtual object getDialogValueRAW(SearchBase oSearch, string sField) {
            return oSearch.getReturnValueRAW(sField);
        }

        public void setDialogValue(string sField, string sValue) {
            if (moSBOForm != null) {
                setDialogValue(moSBOForm, sField, sValue);
            } else if (moSearch != null) {
                moSearch.setFilterFieldValue(sField, sValue);
            } else {
                throw new Exception("This can only be called from a SBO application");
            }
        }
        public void setDialogValue(SAPbouiCOM.Form oSearch, string sField, string sValue) {
            IDHAddOns.idh.forms.Base.setSharedData(oSearch, sField, sValue);
        }
        public void setDialogValue(SearchBase oSearch, string sField, string sValue) {
            oSearch.setFilterFieldValue(sField, sValue);
        }
        #endregion

        //Find the Combo Item Collection by using the Combo's Text Id
        public IList getComboItems(string sControlID) {
            if (moWinForm == null)
                return null;
            else
                return getComboItems(moWinForm.Controls.Find(sControlID, true)[0]);
        }

        //Find the Combo Item Collection by using the Combo's Control
        public IList getComboItems(System.Windows.Forms.Control oControl) {
            if (oControl == null) {
                return null;
            } else if (oControl is System.Windows.Forms.ComboBox) {
                System.Windows.Forms.ComboBox oCombo = (System.Windows.Forms.ComboBox)oControl;
                return oCombo.Items;
            } else if (oControl is LPVComboBox) {
                LPVComboBox oCombo = (LPVComboBox)oControl;
                return oCombo.Items;
            } else {
                return null;
            }
        }

        #region DataNavigation
        public virtual int doReLoadData() {
            return moDBO.doReLoadData();
        }
        /*
		 * Get the record by Key
		 */
        public virtual bool getByKey(string sCode) {
            return moDBO.getByKey(sCode);
        }

        /**
		 * Get the first record By Key
		 */
        public virtual bool getByKeyFirst() {
            return moDBO.getByKeyFirst();
        }

        /**
		 * Get the previous record By Key
		 */
        public virtual bool getByKeyPrevious() {
            return moDBO.getByKeyPrevious();
        }

        /**
		 * Get the nect record By Key
		 */
        public virtual bool getByKeyNext() {
            return moDBO.getByKeyNext();
        }

        /**
		 * Get the first record By Key
		 */
        public virtual bool getByKeyLast() {
            return moDBO.getByKeyLast();
        }

        #endregion

        //public void doWriteLineToLog(string sMessage) {
        //    if (moWinForm != null) {
        //        if ( moWinForm is SBOForm ) {
        //            SBOForm.doWriteLineToLog(sMessage);
        //        }
        //    }
        //}
    }
}