/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/03/02
 * Time: 06:51 PM
 *  
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using com.idh.controls;
using System.Windows.Forms;
using com.idh.bridge.form;
using com.idh.bridge.data;
using com.idh.dbObjects.User;

namespace com.idh.bridge.search {
	/// <summary>
	/// Setting the LinkWO search Rules
	/// </summary>
	public class LinkWO: FormBridge		{
		public static int WOHCreated = 1;
		
        public void doSetGridOptions(DBOGridControl oGridControl, string sDispOrd, string sDispRow, string sUsedRows) {
            //oGridControl.setTableValue("[@IDH_JOBSHD] r, [@IDH_JOBENTR] h");
            oGridControl.doAddGridTable( new GridTable("@IDH_JOBSHD","r", null, false, true), true);
            oGridControl.doAddGridTable( new GridTable("@IDH_JOBENTR", "h", null, false, true));
            
            string sStartDate = "";
            if ( sDispOrd != null && sDispOrd.Length > 0 ) 
                sDispOrd = " AND ( r.U_WROrd Is NULL Or r.U_WROrd = '' Or r.U_WROrd = '" + sDispOrd + "') ";
            else
                sDispOrd = "";

            if ( sDispRow != null && sDispRow.Length > 0 && !sDispRow.Equals("-1") ) 
                sDispRow = " AND ( r.U_WRRow Is NULL Or r.U_WRRow = '' Or r.U_WRRow = '" + sDispRow + "') ";
            else
                sDispRow = " AND ( r.U_WRRow Is NULL Or r.U_WRRow = '' ) ";

            if (sDispOrd.Length > 0 || sDispRow.Length > 0)
                sStartDate = "   AND (Not r.U_ASDate is null AND r.U_ASDate != '' AND DatePart(year, r.U_ASDATE) > 1900 ) ";

            if (sUsedRows != null && sUsedRows.Length > 0 )
            	sUsedRows = "   AND r.Code not in (" + sUsedRows + ") "; 
            else
            	sUsedRows = "";
            
            string sRequired;
            sRequired = "   h.U_Status = '" + WOHCreated + "' " +
            			" And ((r.U_Status Is NULL Or r.U_Status = '' Or r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getStatusOpen() + "') " +
                        "     OR r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getReqFocStatus() + "' " +
            			"     OR r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getDoFocStatus() + "' " +
                        "     OR r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getReqOrderStatus() + "' " +
            			"     OR r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getDoOrderStatus() + "' " +
                        "     OR r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getReqInvoiceStatus() + "' " +
            			"     OR r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus() + "' " +
                        "     OR r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getReqPInvoiceStatus() + "' " +
            			"     OR r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getDoPInvoiceStatus() + "' " +
                        "     OR r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getReqRebateStatus() + "' " +
                        "     OR r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getDoRebateStatus() + "' " +
                        "     OR r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getReqWarehouseToWarehouseTransferStatus() + "' " +
                        "     OR r.U_Status = '" + com.idh.bridge.lookups.FixedValues.getDoWarehouseToWarehouseTransferStatus() + "' " +
                        "     ) " +
            		    "   And r.U_RowSta != '" + com.idh.bridge.lookups.FixedValues.getStatusDeleted() + "' " +
                        "   And (r.U_AEDate Is null Or r.U_AEDate = '' Or DatePart(year, r.U_AEDATE) <= 1900 ) " +
                        sStartDate +
                        "   AND r.U_JobNr = h.Code " +
                        sDispOrd + sDispRow + sUsedRows;

            oGridControl.setRequiredFilter(sRequired);
            //oGridControl.setOrderValue("r.U_Lorry DESC, r.Code DESC");
            oGridControl.setOrderValue("r.U_Lorry DESC, r.U_ASDate DESC, r.Code");

            oGridControl.doAddFilterField("IDH_VEHREG", "r.U_Lorry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
            oGridControl.doAddFilterField("IDH_WOHD", "r.U_JobNr", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
            oGridControl.doAddFilterField("IDH_WOROW", "r.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);

            oGridControl.doAddListField("r.U_JobNr", "Order No.", false, 50, null, "ORDNO");
            oGridControl.doAddListField("r.Code", "Row No.", false, 50, null, "ROWNO");
            oGridControl.doAddListField("r.U_ASDate", "Act. Start", false, 80, null, "SDATE");
            oGridControl.doAddListField("r.U_Lorry", "Vehicle", false, 60, null, "LORRY");
            oGridControl.doAddListField("r.U_LorryCd", "Vehicle Code", false, 0, null, "LORRYCD");
			oGridControl.doAddListField("r.U_VehTyp", "Vehicle Desc", false, 0, null, "LORRYDESC");
			oGridControl.doAddListField("r.U_Driver", "Driver", false, 0, null, "DRIVER");
			
            oGridControl.doAddListField("r.U_CustCd", "Customer Code", false, 50, null, "CUSTCD");
            oGridControl.doAddListField("r.U_CustNm", "Customer Name", false, 100, null, "CUSTNM");
            oGridControl.doAddListField("r.U_JobTp", "Job Type", false, 50, null, "JOBTP");
            oGridControl.doAddListField("r.U_WasCd", "Waste Code", false, 50, null, "WASTCD");
            oGridControl.doAddListField("r.U_WasDsc", "Waste Desc", false, 0, null, "WASTDE");
            //MA Start 10-09-2015 Issue#909
            oGridControl.doAddListField("r.U_AltWasDsc", "Alternate Waste Desc", false, 0, null, "ALTWSTDE");
            //MA End 10-09-2015 Issue#909
            //U_AltWasDsc
            //oGridControl.doAddListField("r.U_WasDsc", "Waste Description", false, 0, null, "WASTE");
            oGridControl.doAddListField("r.U_CarrCd", "Carrier Code", false, 0, null, "CARRCD");
            oGridControl.doAddListField("r.U_CarrNm", "Carrier Name", false, 0, null, "CARRNM");

            oGridControl.doAddListField("r.U_ItmGrp", "Container Type", false, 0, null, "CONTP");
            oGridControl.doAddListField("r.U_ItemCd", "Container Code", false, 0, null, "CONCD");
            oGridControl.doAddListField("r.U_ItemDsc", "Container Desc", false, 0, null, "CONDSC");

            oGridControl.doAddListField("r.U_CstWgt", "QTY", false, 0, null, "QTY");
            oGridControl.doAddListField("r.U_Origin", "Origin", false, 0, null, "ORIGIN");
            oGridControl.doAddListField("r.U_CustCs", "Compliance Scheme", false, 0, null, "COMSCHEME");

            //The Address Details Customer
            oGridControl.doAddListField("h.U_Contact", "Contact", false, 0, null, "CONTACT");
            oGridControl.doAddListField("r.U_CustRef", "Row Customer Referance", false, 0, null, "CUSTREF");
            oGridControl.doAddListField("h.U_CustRef", "Customer Referance", false, 0, null, "HCUSTREF");
            oGridControl.doAddListField("h.U_Address", "Address", false, 100, null, "ADDRESS");
            oGridControl.doAddListField("h.U_Street", "Street", false, 0, null, "STREET");
            oGridControl.doAddListField("h.U_Block", "Block", false, 0, null, "BLOCK");
            oGridControl.doAddListField("h.U_City", "City", false, 0, null, "CITY");
            oGridControl.doAddListField("h.U_County", "County", false, 0, null, "COUNTY");
            oGridControl.doAddListField("h.U_ZpCd", "Zip Code", false, 0, null, "ZPCD");
            oGridControl.doAddListField("h.U_Phone1", "Phone", false, 0, null, "PHONE1");
            oGridControl.doAddListField("h.U_SiteTl", "Site Tel", false, 0, null, "SITETL");
            oGridControl.doAddListField("h.U_SteId", "Site Id", false, 0, null, "STEID");
            oGridControl.doAddListField("h.U_Route", "Route", false, 0, null, "ROUTE");
            oGridControl.doAddListField("h.U_Seq", "Seq", false, 0, null, "SEQ");
            oGridControl.doAddListField("h.U_PremCd", "Premisis Code", false, 0, null, "PRECD");
            oGridControl.doAddListField("h.U_SiteLic", "Site Lic.", false, 0, null, "SITELIC");
            
            //**CARRIER
            oGridControl.doAddListField("h.U_CCardCd", "Carrier Code", false, 0, null, "HDCARRCD");
            oGridControl.doAddListField("h.U_CContact", "Carrier Contact", false, 0, null, "CCONTACT");
            oGridControl.doAddListField("h.U_CAddress", "Address", false, 0, null, "CADDRESS");
            oGridControl.doAddListField("h.U_CStreet", "Street", false, 0, null, "CSTREET");
            oGridControl.doAddListField("h.U_CBlock", "Block", false, 0, null, "CBLOCK");
            oGridControl.doAddListField("h.U_CCity", "City", false, 0, null, "CCITY");
            oGridControl.doAddListField("h.U_CZpCd", "Zip Code", false, 0, null, "CZPCD");
            oGridControl.doAddListField("h.U_CPhone1", "Phone", false, 0, null, "CPHONE1");
            oGridControl.doAddListField("h.U_SupRef", "Carr Referance", false, 0, null, "CREF");

            //**PRODUCER
            oGridControl.doAddListField("h.U_PCardCd", "Producer Code", false, -1, null, "PCD");
            oGridControl.doAddListField("h.U_PCardNM", "Producer Name", false, -1, null, "PNM");
            oGridControl.doAddListField("h.U_PContact", "Producer Contact", false, 0, null, "PCONTACT");
            oGridControl.doAddListField("h.U_PAddress", "Producer Address", false, 0, null, "PADDRESS");
            oGridControl.doAddListField("h.U_PStreet", "Producer Street", false, 0, null, "PSTREET");
            oGridControl.doAddListField("h.U_PBlock", "Producer Block", false, 0, null, "PBLOCK");
            oGridControl.doAddListField("h.U_PCity", "Producer City", false, 0, null, "PCITY");
            oGridControl.doAddListField("h.U_PZpCd", "Producer Zip Code", false, 0, null, "PZPCD");
            oGridControl.doAddListField("h.U_PPhone1", "Producer Phone", false, 0, null, "PPHONE1");

            //**SITE
            oGridControl.doAddListField("r.U_Tip", "Site Code", false, 0, null, "SCD");
            oGridControl.doAddListField("r.U_TipNm", "Site Name", false, 0, null, "SNM");
            oGridControl.doAddListField("h.U_SContact", "Site Carrier Contact", false, 0, null, "SCONTACT");
            oGridControl.doAddListField("h.U_SAddress", "Site Address", false, 0, null, "SADDRESS");
            oGridControl.doAddListField("r.U_SAddress", "Row Site Address", false, 0, null, "RSADDRESS");
            oGridControl.doAddListField("h.U_SStreet", "Site Street", false, 0, null, "SSTREET");
            oGridControl.doAddListField("h.U_SBlock", "Site Block", false, 0, null, "SBLOCK");
            oGridControl.doAddListField("h.U_SCity", "Site City", false, 0, null, "SCITY");
            oGridControl.doAddListField("h.U_SZpCd", "Site Zip Code", false, 0, null, "SZPCD");
            oGridControl.doAddListField("h.U_SPhone1", "Site Phone", false, 0, null, "SPHONE1");

			oGridControl.doAddListField("r.U_AEDate", "Act. End", false, 80, null, "EDATE");
            oGridControl.doAddListField("h.U_RTIMEF", "RTIMEF", false, 0, null, "RTIMEF");
            oGridControl.doAddListField("h.U_RTIMET", "RTIMET", false, 0, null, "RTIMET");
            oGridControl.doAddListField("r.U_Obligated", "Obligated", false, 0, null, "OBLIGATED");
            
            oGridControl.doAddListField("r.U_WastTNN", "WastTNN", false, 0, null, "WASTTNN");
            //oGridControl.doAddListField("r.U_HazWCNN", "HazWCNN", false, 0, null, "HAZWCNN");
            oGridControl.doAddListField("r.U_SiteRef", "SiteRef", false, 0, null, "SITEREF");
            oGridControl.doAddListField("r.U_ExtWeig", "ExtWeig", false, 0, null, "EXTWEIG");
            
            oGridControl.doAddListField("r.U_ConNum", "Consignment Number", false, 0, null, "CONNUM");
            oGridControl.doAddListField("r.U_WROrd", "Disposal Order", false, 0, null, "DISPORD");
            oGridControl.doAddListField("r.U_WRRow", "Disposal Order Row", false, 0, null, "DISPROW");
            
            oGridControl.doAddListField("r.U_BookIn", "Book into Stock", false, 0, null, "BOOKIN");
            oGridControl.doAddListField("r.U_IsTrl", "Trail Load", false, 0, null, "ISTRL");

            oGridControl.doAddListField("r." + IDH_JOBSHD._UOM, "UOM", false, 0, null, "UOM");
            oGridControl.doAddListField("r." + IDH_JOBSHD._PUOM, "PUOM", false, 0, null, "PUOM");
            oGridControl.doAddListField("r." + IDH_JOBSHD._ProUOM, "ProUOM", false, 0, null, "PROUOM");

            oGridControl.doAddListField("r." + IDH_JOBSHD._AUOMQt, "AUOMQt", false, 0, null, "AUOMQT");
            oGridControl.doAddListField("r." + IDH_JOBSHD._TAUOMQt, "TAUOMQt", false, 0, null, "TAUOMQT");
            oGridControl.doAddListField("r." + IDH_JOBSHD._PAUOMQt, "PAUOMQt", false, 0, null, "PAUOMQT");

            oGridControl.doAddListField("r." + IDH_JOBSHD._TipWgt, "Tip Weight", false, 0, null, "TIPWGT");
            oGridControl.doAddListField("r." + IDH_JOBSHD._ProWgt, "Producer Weight", false, 0, null, "PROWGT");

            oGridControl.doAddListField("r." + IDH_JOBSHD._UseWgt, "Use Weight", false, 0, null, "USEWGT");
 
            oGridControl.doAddListField("r." + IDH_JOBSHD._ExpLdWgt, "ExpLdWgt", false, -1, null, "EXPLdWGT");

            oGridControl.doAddListField("r." + IDH_JOBSHD._AUOM, "AUOM", false, -1, null, "AUOM");
            
            oGridControl.doAddListField("r." + IDH_JOBSHD._TCharge, "Disposal Charge", false, 0, null, "DISPCHRG");
            oGridControl.doAddListField("r." + IDH_JOBSHD._TipCost, "Disposal Cost", false, 0, null, "DISPCST");
            oGridControl.doAddListField("r." + IDH_JOBSHD._OrdCost, "Haulage Cost", false, 0, null, "HAULCST");
            oGridControl.doAddListField("r." + IDH_JOBSHD._PCost, "Producer Cost", false, 0, null, "PRODCST");
            oGridControl.doAddListField("r." + IDH_JOBSHD._TrnCode, "Transaction Code", false, 0, null, "TRNCODE");
            oGridControl.doAddListField("r." + IDH_JOBSHD._MANPRC, "Manual Price Indicator", false, 0, null, "MANPRC");
		}
	}
}
