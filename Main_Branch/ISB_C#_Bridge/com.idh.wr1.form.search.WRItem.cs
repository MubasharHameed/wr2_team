/*
 * Created by SharpDevelop.
 * User: Louis
 * Date: 2009/07/20
 * Time: 01:23 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
//using System.Windows.Forms;
using com.idh.win.controls;
using com.idh.controls;
using com.idh.win.forms;

namespace com.idh.wr1.form.search
{
	/// <summary>
	/// Item Search Form
	/// </summary>
	public partial class WRItem : SearchBase {
		private idh.bridge.search.Item moItemSearch;
		public idh.bridge.search.Item ItemSearch {
			get { return moItemSearch; }
		}	
		
		public WRItem()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			doSetupFormDefaults(btn1, btn2, oSearchGrid);
			oSearchGrid.setWinForm(this);
			
			moItemSearch = new idh.bridge.search.Item();

            System.Windows.Forms.Form oForm = this;
			moItemSearch.setWinForm( ref oForm );	
		}
		
		public override void doSetupControl(){
			moItemSearch.doSetGridOptions(this.oSearchGrid.GridControl);
			this.oSearchGrid.doApplyControl();
		}			
	}
}
