﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/06/29
 * Time: 02:20 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
 using System.Windows.Forms;
 using com.idh.win.controls;
 using com.idh.win.forms;
 
namespace com.idh.wr1.form.search
{
	public partial class Lorry2 //: SearchBase
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.oSearchGrid = new com.idh.win.controls.WR1Grid();
            this.bt1 = new System.Windows.Forms.Button();
            this.bt2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.IDH_VEHREG = new System.Windows.Forms.TextBox();
            this.IDH_DRVR = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.IDH_VEHTP = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.IDH_WR1OT = new System.Windows.Forms.ComboBox();
            this.btCreate = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.IDH_CWFS = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.IDH_VEHDIS = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.IDH_CARR = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // oSearchGrid
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Beige;
            this.oSearchGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.oSearchGrid.CausesValidation = false;
            this.oSearchGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.oSearchGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.oSearchGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oSearchGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.oSearchGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oSearchGrid.GrayReadOnly = false;
            this.oSearchGrid.Location = new System.Drawing.Point(5, 71);
            this.oSearchGrid.MultiSelect = false;
            this.oSearchGrid.Name = "oSearchGrid";
            this.oSearchGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.oSearchGrid.Size = new System.Drawing.Size(833, 401);
            this.oSearchGrid.TabIndex = 0;
            // 
            // bt1
            // 
            this.bt1.Location = new System.Drawing.Point(12, 478);
            this.bt1.Name = "bt1";
            this.bt1.Size = new System.Drawing.Size(75, 23);
            this.bt1.TabIndex = 1;
            this.bt1.Text = "OK";
            this.bt1.UseVisualStyleBackColor = true;
            // 
            // bt2
            // 
            this.bt2.Location = new System.Drawing.Point(93, 478);
            this.bt2.Name = "bt2";
            this.bt2.Size = new System.Drawing.Size(75, 23);
            this.bt2.TabIndex = 9;
            this.bt2.Text = "Cancel";
            this.bt2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Vehicle Registration";
            // 
            // IDH_VEHREG
            // 
            this.IDH_VEHREG.Location = new System.Drawing.Point(114, 5);
            this.IDH_VEHREG.Name = "IDH_VEHREG";
            this.IDH_VEHREG.Size = new System.Drawing.Size(144, 20);
            this.IDH_VEHREG.TabIndex = 1;
            this.IDH_VEHREG.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // IDH_DRVR
            // 
            this.IDH_DRVR.Location = new System.Drawing.Point(398, 5);
            this.IDH_DRVR.Name = "IDH_DRVR";
            this.IDH_DRVR.Size = new System.Drawing.Size(144, 20);
            this.IDH_DRVR.TabIndex = 2;
            this.IDH_DRVR.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(5, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Type";
            // 
            // IDH_VEHTP
            // 
            this.IDH_VEHTP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_VEHTP.FormattingEnabled = true;
            this.IDH_VEHTP.Location = new System.Drawing.Point(114, 25);
            this.IDH_VEHTP.Name = "IDH_VEHTP";
            this.IDH_VEHTP.Size = new System.Drawing.Size(144, 21);
            this.IDH_VEHTP.TabIndex = 3;
            this.IDH_VEHTP.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(289, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "WR1 Order Type";
            // 
            // IDH_WR1OT
            // 
            this.IDH_WR1OT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_WR1OT.FormattingEnabled = true;
            this.IDH_WR1OT.Location = new System.Drawing.Point(398, 25);
            this.IDH_WR1OT.Name = "IDH_WR1OT";
            this.IDH_WR1OT.Size = new System.Drawing.Size(144, 21);
            this.IDH_WR1OT.TabIndex = 5;
            this.IDH_WR1OT.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
            // 
            // btCreate
            // 
            this.btCreate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCreate.Location = new System.Drawing.Point(763, 478);
            this.btCreate.Name = "btCreate";
            this.btCreate.Size = new System.Drawing.Size(75, 23);
            this.btCreate.TabIndex = 10;
            this.btCreate.Text = "Create";
            this.btCreate.UseVisualStyleBackColor = true;
            this.btCreate.Visible = false;
            this.btCreate.Click += new System.EventHandler(this.CreateClick);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(289, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "Driver";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(582, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 17);
            this.label8.TabIndex = 11;
            this.label8.Text = "Workflow Status";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // IDH_CWFS
            // 
            this.IDH_CWFS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_CWFS.FormattingEnabled = true;
            this.IDH_CWFS.Location = new System.Drawing.Point(655, 5);
            this.IDH_CWFS.Name = "IDH_CWFS";
            this.IDH_CWFS.Size = new System.Drawing.Size(177, 21);
            this.IDH_CWFS.TabIndex = 12;
            this.IDH_CWFS.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Vehicle Description";
            // 
            // IDH_VEHDIS
            // 
            this.IDH_VEHDIS.Location = new System.Drawing.Point(114, 46);
            this.IDH_VEHDIS.Name = "IDH_VEHDIS";
            this.IDH_VEHDIS.Size = new System.Drawing.Size(144, 20);
            this.IDH_VEHDIS.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(582, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Carrier";
            // 
            // IDH_CARR
            // 
            this.IDH_CARR.Location = new System.Drawing.Point(656, 25);
            this.IDH_CARR.Name = "IDH_CARR";
            this.IDH_CARR.Size = new System.Drawing.Size(177, 20);
            this.IDH_CARR.TabIndex = 16;
            // 
            // Lorry2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 504);
            this.Controls.Add(this.IDH_CARR);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.IDH_VEHDIS);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.IDH_CWFS);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.IDH_WR1OT);
            this.Controls.Add(this.IDH_VEHTP);
            this.Controls.Add(this.IDH_DRVR);
            this.Controls.Add(this.IDH_VEHREG);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btCreate);
            this.Controls.Add(this.bt2);
            this.Controls.Add(this.bt1);
            this.Controls.Add(this.oSearchGrid);
            this.KeyPreview = true;
            this.Name = "Lorry2";
            this.Text = "Vehicle Search";
            this.Load += new System.EventHandler(this.Lorry2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox IDH_WR1OT;
        private System.Windows.Forms.Button btCreate;
        private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox IDH_VEHTP;
		private System.Windows.Forms.TextBox IDH_VEHREG;
		private System.Windows.Forms.TextBox IDH_DRVR;
        private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button bt2;
		private System.Windows.Forms.Button bt1;
		private  idh.win.controls.WR1Grid oSearchGrid;

		void CreateClick(object sender, System.EventArgs e) {
			this.DialogResult = System.Windows.Forms.DialogResult.Ignore;
			this.Hide();
		}	

        protected void IDH_SelectedValueChanged(object sender, System.EventArgs e)
        {
            SelectedValueChanged_Internal(sender, e);
        }

        protected void IDH_TextChanged(object sender, System.EventArgs e)
        {
            TextChanged_Internal(sender, e);
        }

        /*
         * sField can bet the SQL column name or the set FieldID
         */
        public override string getReturnValue(string sField)
        {
            if (this.DialogResult == DialogResult.Ignore)
            {
                if (sField.Equals("REG")) {
                    return IDH_VEHREG.Text;
                } else if (sField.Equals("DRIVER")) {
                    return IDH_DRVR.Text;
                } else if (sField.Equals("VEHTYPE")) {
                    return getFilterFieldValue("IDH_VEHTP");
                } else if (sField.Equals("VEHGROUP") ) {
                  return getFilterFieldValue("IDH_ITMGRP");  
                } else if (sField.Equals("CUSTOMER")) {
                    return "";
                } else if (sField.Equals("CARRIER")) {
                    return IDH_CARR.Text;
                } else if (sField.Equals("WR1Type")) {
                    string sWR1Type = getFilterFieldValue("IDH_WR1OT");

                    if (sWR1Type.IndexOf("WO") != -1)
                        return "WO";
                    else if (sWR1Type.IndexOf("DO") != -1)
                        return "DO";
                    else
                        return "";
                } else
                    return "";
            }
            return base.getReturnValue(sField);
        }

        public override void doLoadData()
        {
            base.doLoadData();
            if (oSearchGrid.RowCount == 1)
            {
                btCreate.Visible = true;
                bt1.Visible = false;
            }
            else
            {
                btCreate.Visible = false;
                bt1.Visible = true;
            }
        }

        private Label label8;
        private ComboBox IDH_CWFS;
        private Label label4;
        private TextBox IDH_VEHDIS;
        private Label label6;
        private TextBox IDH_CARR;
	}
}
