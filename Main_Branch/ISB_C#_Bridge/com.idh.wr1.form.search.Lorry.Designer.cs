﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/06/29
 * Time: 02:20 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
 using System.Windows.Forms;
 using com.idh.win.controls;
 using com.idh.win.forms;
 
namespace com.idh.wr1.form.search
{
	public partial class Lorry //: SearchBase
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.oSearchGrid = new com.idh.win.controls.WR1Grid();
            this.bt1 = new System.Windows.Forms.Button();
            this.bt2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.IDH_VEHREG = new System.Windows.Forms.TextBox();
            this.IDH_DRVR = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.IDH_VEHTP = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.IDH_ITMGRP = new System.Windows.Forms.ComboBox();
            this.IDH_WR1OT = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.IDH_VEHCUS = new System.Windows.Forms.TextBox();
            this.IDH_VEHSUP = new System.Windows.Forms.TextBox();
            this.btCreate = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.IDH_BRANCH = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // oSearchGrid
            // 
            this.oSearchGrid.CausesValidation = false;
            this.oSearchGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.oSearchGrid.GrayReadOnly = false;
            this.oSearchGrid.Location = new System.Drawing.Point(5, 71);
            this.oSearchGrid.MultiSelect = false;
            this.oSearchGrid.Name = "oSearchGrid";
            this.oSearchGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.oSearchGrid.Size = new System.Drawing.Size(833, 401);
            this.oSearchGrid.TabIndex = 0;
            // 
            // bt1
            // 
            this.bt1.Location = new System.Drawing.Point(12, 478);
            this.bt1.Name = "bt1";
            this.bt1.Size = new System.Drawing.Size(75, 23);
            this.bt1.TabIndex = 1;
            this.bt1.Text = "OK";
            this.bt1.UseVisualStyleBackColor = true;
            // 
            // bt2
            // 
            this.bt2.Location = new System.Drawing.Point(93, 478);
            this.bt2.Name = "bt2";
            this.bt2.Size = new System.Drawing.Size(75, 23);
            this.bt2.TabIndex = 9;
            this.bt2.Text = "Cancel";
            this.bt2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Vehicle Registration";
            // 
            // IDH_VEHREG
            // 
            this.IDH_VEHREG.Location = new System.Drawing.Point(114, 4);
            this.IDH_VEHREG.Name = "IDH_VEHREG";
            this.IDH_VEHREG.Size = new System.Drawing.Size(144, 20);
            this.IDH_VEHREG.TabIndex = 1;
            this.IDH_VEHREG.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // IDH_DRVR
            // 
            this.IDH_DRVR.Location = new System.Drawing.Point(114, 24);
            this.IDH_DRVR.Name = "IDH_DRVR";
            this.IDH_DRVR.Size = new System.Drawing.Size(144, 20);
            this.IDH_DRVR.TabIndex = 2;
            this.IDH_DRVR.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(5, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Type";
            // 
            // IDH_VEHTP
            // 
            this.IDH_VEHTP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_VEHTP.FormattingEnabled = true;
            this.IDH_VEHTP.Location = new System.Drawing.Point(114, 44);
            this.IDH_VEHTP.Name = "IDH_VEHTP";
            this.IDH_VEHTP.Size = new System.Drawing.Size(144, 21);
            this.IDH_VEHTP.TabIndex = 3;
            this.IDH_VEHTP.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(302, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Vehicle Group";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(302, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "WR1 Order Type";
            // 
            // IDH_ITMGRP
            // 
            this.IDH_ITMGRP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_ITMGRP.FormattingEnabled = true;
            this.IDH_ITMGRP.Location = new System.Drawing.Point(398, 4);
            this.IDH_ITMGRP.Name = "IDH_ITMGRP";
            this.IDH_ITMGRP.Size = new System.Drawing.Size(144, 21);
            this.IDH_ITMGRP.TabIndex = 4;
            this.IDH_ITMGRP.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
            // 
            // IDH_WR1OT
            // 
            this.IDH_WR1OT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_WR1OT.FormattingEnabled = true;
            this.IDH_WR1OT.Location = new System.Drawing.Point(398, 25);
            this.IDH_WR1OT.Name = "IDH_WR1OT";
            this.IDH_WR1OT.Size = new System.Drawing.Size(144, 21);
            this.IDH_WR1OT.TabIndex = 5;
            this.IDH_WR1OT.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(582, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Customer";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(582, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "Supplier";
            // 
            // IDH_VEHCUS
            // 
            this.IDH_VEHCUS.Location = new System.Drawing.Point(655, 4);
            this.IDH_VEHCUS.Name = "IDH_VEHCUS";
            this.IDH_VEHCUS.Size = new System.Drawing.Size(177, 20);
            this.IDH_VEHCUS.TabIndex = 6;
            this.IDH_VEHCUS.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // IDH_VEHSUP
            // 
            this.IDH_VEHSUP.Location = new System.Drawing.Point(655, 24);
            this.IDH_VEHSUP.Name = "IDH_VEHSUP";
            this.IDH_VEHSUP.Size = new System.Drawing.Size(177, 20);
            this.IDH_VEHSUP.TabIndex = 7;
            this.IDH_VEHSUP.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // btCreate
            // 
            this.btCreate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCreate.Location = new System.Drawing.Point(763, 478);
            this.btCreate.Name = "btCreate";
            this.btCreate.Size = new System.Drawing.Size(75, 23);
            this.btCreate.TabIndex = 10;
            this.btCreate.Text = "Create";
            this.btCreate.UseVisualStyleBackColor = true;
            this.btCreate.Visible = false;
            this.btCreate.Click += new System.EventHandler(this.CreateClick);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(5, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "Driver";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(582, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 17);
            this.label8.TabIndex = 11;
            this.label8.Text = "Branch";
            // 
            // IDH_BRANCH
            // 
            this.IDH_BRANCH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_BRANCH.FormattingEnabled = true;
            this.IDH_BRANCH.Location = new System.Drawing.Point(655, 45);
            this.IDH_BRANCH.Name = "IDH_BRANCH";
            this.IDH_BRANCH.Size = new System.Drawing.Size(177, 21);
            this.IDH_BRANCH.TabIndex = 12;
            this.IDH_BRANCH.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
            // 
            // Lorry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 504);
            this.Controls.Add(this.IDH_BRANCH);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.IDH_WR1OT);
            this.Controls.Add(this.IDH_ITMGRP);
            this.Controls.Add(this.IDH_VEHTP);
            this.Controls.Add(this.IDH_DRVR);
            this.Controls.Add(this.IDH_VEHSUP);
            this.Controls.Add(this.IDH_VEHCUS);
            this.Controls.Add(this.IDH_VEHREG);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btCreate);
            this.Controls.Add(this.bt2);
            this.Controls.Add(this.bt1);
            this.Controls.Add(this.oSearchGrid);
            this.KeyPreview = true;
            this.Name = "Lorry";
            this.Text = "Vehicle Search";
            ((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox IDH_ITMGRP;
		private System.Windows.Forms.ComboBox IDH_WR1OT;
		private System.Windows.Forms.TextBox IDH_VEHCUS;
		private System.Windows.Forms.TextBox IDH_VEHSUP;
		private System.Windows.Forms.Button btCreate;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox IDH_VEHTP;
		private System.Windows.Forms.TextBox IDH_VEHREG;
		private System.Windows.Forms.TextBox IDH_DRVR;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button bt2;
		private System.Windows.Forms.Button bt1;
		private  idh.win.controls.WR1Grid oSearchGrid;

		void CreateClick(object sender, System.EventArgs e) {
			this.DialogResult = System.Windows.Forms.DialogResult.Ignore;
			this.Hide();
		}	

        protected void IDH_SelectedValueChanged(object sender, System.EventArgs e)
        {
            SelectedValueChanged_Internal(sender, e);
        }

        protected void IDH_TextChanged(object sender, System.EventArgs e)
        {
            TextChanged_Internal(sender, e);
        }

        /*
         * sField can bet the SQL column name or the set FieldID
         */
        public override string getReturnValue(string sField)
        {
            if (this.DialogResult == DialogResult.Ignore)
            {
                if (sField.Equals("REG")) {
                    return IDH_VEHREG.Text;
                } else if (sField.Equals("DRIVER")) {
                    return IDH_DRVR.Text;
                } else if (sField.Equals("VEHTYPE")) {
                    return getFilterFieldValue("IDH_VEHTP");
                } else if (sField.Equals("VEHGROUP") ) {
                  return getFilterFieldValue("IDH_ITMGRP");  
                } else if (sField.Equals("CUSTOMER")) {
                  return IDH_VEHCUS.Text;  
                } else if (sField.Equals("SUPPLIER")) {
                  return IDH_VEHSUP.Text;
                } else if (sField.Equals("WR1Type")) {
                    string sWR1Type = getFilterFieldValue("IDH_WR1OT");

                    if (sWR1Type.IndexOf("WO") != -1)
                        return "WO";
                    else if (sWR1Type.IndexOf("DO") != -1)
                        return "DO";
                    else
                        return "";
                } else
                    return "";
            }
            return base.getReturnValue(sField);
        }

        public override void doLoadData()
        {
            base.doLoadData();
            if (oSearchGrid.RowCount == 1)
            {
                btCreate.Visible = true;
                bt1.Visible = false;
            }
            else
            {
                btCreate.Visible = false;
                bt1.Visible = true;
            }
        }

        private Label label8;
        private ComboBox IDH_BRANCH;
	}
}
