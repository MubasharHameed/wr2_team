/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/06/29
 * Time: 02:20 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
//using System.Drawing;
//using System.Windows.Forms;
using com.idh.win.controls;
using com.idh.controls;
using com.idh.win.forms;

namespace com.idh.wr1.form.search
{
	/// <summary>
	/// Search for Lorries
	/// </summary>
	public partial class Lorry : SearchBase
	{
        //private String msTitle; 
		private  idh.bridge.search.Lorry moLorrySearch;
		public  idh.bridge.search.Lorry Bridge {
			get { return moLorrySearch; }
		}
		
		public Lorry() {
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			doSetupFormDefaults(bt1, bt2, oSearchGrid);
			oSearchGrid.setWinForm(this);
			
			moLorrySearch = new  idh.bridge.search.Lorry();

            System.Windows.Forms.Form oForm = this;
			moLorrySearch.setWinForm( ref oForm );
		}

		public  override void doSetupControl() {
			moLorrySearch.doSetGridOptions(SearchGrid.GridControl, false, false, null, null, null);
			SearchGrid.doApplyControl();
		}
		
		public  void doSetupControlNormalSearch() {
			moLorrySearch.doSetGridOptions(SearchGrid.GridControl, false, false, null, null, "^DO");
			SearchGrid.doApplyControl();
            Text = "Vehicle Search";
		}	
		
		public  void doSetupControlOnsiteSearch() {
			moLorrySearch.doSetGridOptions(SearchGrid.GridControl, false, true, null, null, "^DO");
			SearchGrid.doApplyControl();
            Text = "OnSite Vehicle Search";
		}
	}
}
