/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/05
 * Time: 10:51 AM
 *   
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections;
using System.Windows.Forms;


namespace com.idh.bridge.form
{
	/// <summary>
	/// Description of com_idh_bridge_form_FieldRelations.
	/// </summary>
	public class FieldRelations {
		private ArrayList moFormFieldId = new ArrayList();
		private ArrayList moDBFieldId = new ArrayList();
		private ArrayList moWinFormControl = new ArrayList();
		private ArrayList moSBOFormItem = new ArrayList();

		public int Count {
			get {return moFormFieldId.Count; }
		}
		
		public FieldRelations() {
		}
		
		public void doAddWinField( string sFormFieldId, string sDBFieldId, Control oControl) {
			moFormFieldId.Add( sFormFieldId );
			moDBFieldId.Add( sDBFieldId );
			moWinFormControl.Add( oControl );
			moSBOFormItem.Add(null);
		}
		
		public void doAddSBOField( string sFormFieldId, string sDBFieldId, SAPbouiCOM.Item oItem) {
			moFormFieldId.Add( sFormFieldId );
			moDBFieldId.Add( sDBFieldId );
			moWinFormControl.Add( null );
			moSBOFormItem.Add( oItem );
		}
		
		public int indexOfFormField( string sFormFieldId ){
			return moFormFieldId.IndexOf(sFormFieldId);
		}

        public int indexOfDBField(string sDBFieldId)
        {
            return moDBFieldId.IndexOf(sDBFieldId, 0);
        }
		public int indexOfDBField( string sDBFieldId, int iStart ){
			return moDBFieldId.IndexOf(sDBFieldId, iStart);
		}
		
		public string getFormFieldId( int iIndex ){
			if ( iIndex < 0 || iIndex > moFormFieldId.Count )
				return null;
			else
				return (string)moFormFieldId[iIndex];
		}		

		public string getDBFieldId(  int iIndex ){
			if ( iIndex < 0 || iIndex > moDBFieldId.Count )
				return null;
			else
				return (string)moDBFieldId[iIndex];
		}		
		
        //public Control getControlFromDBField( string sDBFieldId ){
        //    return getControl( indexOfDBField( sDBFieldId ));
        //}

        public ArrayList getControlsFromDBField(string sDBFieldId)
        {
            ArrayList oAr = new ArrayList();
            int iStart = 0;
            while ((iStart = indexOfDBField(sDBFieldId, iStart)) >= 0)
            {
                oAr.Add(getControl(iStart));
                iStart++;
            }

            return oAr;
        }

		public Control getControlFromFormField( string sFormFieldId ){
			return getControl( indexOfFormField( sFormFieldId ));
		}
		
		public Control getControl( int iIndex ){
			if ( iIndex < 0 || iIndex > moWinFormControl.Count )
				return null;
			else
				return (Control)moWinFormControl[iIndex];
		}

		public SAPbouiCOM.Item getItem( int iIndex ){
			if ( iIndex < 0 || iIndex > moSBOFormItem.Count )
				return null;
			else
				return (SAPbouiCOM.Item)moSBOFormItem[iIndex];
		}
		
		
	}
}
