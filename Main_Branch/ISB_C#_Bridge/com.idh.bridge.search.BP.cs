/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/15
 * Time: 12:20 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using com.idh.controls;
using System.Windows.Forms;
using com.idh.bridge.form;
using com.idh.bridge.data;
using com.idh.win;

namespace com.idh.bridge.search {
	/// <summary>
	/// BP Search form control
	/// </summary>
	public class BP: FormBridge	{
		private string msISCS;
		
		public override void setWinForm( ref Form oForm ){
			base.setWinForm( ref oForm );
			doFillCombos();
		}

		public override void setSBOForm( ref IDHAddOns.idh.forms.Base oIDHForm, ref SAPbouiCOM.Form oSBOForm ){
			base.setSBOForm( ref oIDHForm, ref oSBOForm );
			doFillCombos();
		}
		
		private void doFillCombos(){
			doFillType();
			doFillGroup();
			doFillBranch();
		}
		
		public string ISCS {
			get { return msISCS; }
			set { msISCS = value; }
		}		
		
		public void doSetGridOptions( DBOGridControl oGridControl ) {
            bool bIsCompliance = false;
            
			//oGridControl.setTableValue("OCRD b, OCRG g, " + moLookup.getBalanceView() + " c");
			oGridControl.doAddGridTable(new GridTable("OCRD", "b", null, false, true), true);
			oGridControl.doAddGridTable(new GridTable("OCRG", "g", null, false, true));
			oGridControl.doAddGridTable(new GridTable(moLookup.getBalanceView(), "c"));
			
            string sRequired;
            //Dim sISCS As String = getParentSharedData(oGridN.getSBOForm(), "IDH_CS")
            if ( msISCS != null && msISCS.Length > 0)  {
                sRequired = " AND U_IDHISCS = '" + msISCS + "' ";
                bIsCompliance = (msISCS.ToLower()[0] == 'y' );
            }
            else
            	sRequired = "";
			sRequired = "b.GroupCode = g.GroupCode " + 
						" AND (b.U_IDH_DUSJB is NULL Or Upper(b.U_IDH_DUSJB) != 'TRUE') " + 
						" AND b.CardCode=c.CardCode" +
						sRequired;
            oGridControl.setRequiredFilter(sRequired);

            oGridControl.doAddFilterField("IDH_BPCOD", "b.CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);
            oGridControl.doAddFilterField("IDH_NAME", "b.CardName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);
            oGridControl.doAddFilterField("IDH_TYPE", "b.CardType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
            oGridControl.doAddFilterField("IDH_GROUP", "b.GroupCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
            oGridControl.doAddFilterField("IDH_BRANCH", "b.U_IDHBRAN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
            
            oGridControl.doAddListField("b.CardCode", "Code", false, 50, null, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
		    oGridControl.doAddListField("b.CardName", "Name", false, 150, null, "CARDNAME");
		    oGridControl.doAddListField("b.CardType", "Type", false, 40, null, "CARDTYPE");
		    oGridControl.doAddListField("b.GroupCode", "Group Code", false, 0, null, "GROUPCODE");
		    oGridControl.doAddListField("g.GroupName", "Group Name", false, 150, null, "GROUPNAME");
		    
        	oGridControl.doAddListField("c.RemainBal", "Credit Remain", false, (bIsCompliance?0:50) , null, "CRREMAIN");
            oGridControl.doAddListField("b.OrdersBal", "Orders", false, (bIsCompliance?0:50), null, "ORDERS");
            oGridControl.doAddListField("b.CreditLine", "Credit Limit", false, (bIsCompliance?0:50), null, "CREDITLINE");
            oGridControl.doAddListField("c.Balance", "Balance", false, (bIsCompliance?0:50), null, "BALANCE");
            oGridControl.doAddListField("c.TBalance", "Total Balance", false, (bIsCompliance?0:-1), null, "TBALANCE");
            oGridControl.doAddListField("c.WRBalance", "WR1 Orders", false, (bIsCompliance?0:50), null, "WRORDERS");
            oGridControl.doAddListField("b.U_IDHBRAN", "Branch",false, (bIsCompliance?0:50), null, "BRANCH");

            oGridControl.doAddListField("b.Balance", "FC Balance", false, 0, null, "BALANCEFC");
            oGridControl.doAddListField("c.Deviation", "Deviation", false, 0, null, "DEVIATION");
            oGridControl.doAddListField("c.frozenFor", "Frozen", false, 0, null, "FROZEN");
            oGridControl.doAddListField("c.FrozenComm", "FrozenComm", false, 0, null, "FROZENC");
            oGridControl.doAddListField("b.Phone1", "Phone", false, 0, null, "PHONE1");
            oGridControl.doAddListField("b.CntctPrsn", "Contact Person", false, 0, null, "CNTCTPRSN");
            oGridControl.doAddListField("b.U_WASLIC", "Waste Lic. Reg. No.", false, 0, null, "WASLIC");
            oGridControl.doAddListField("b.U_IDHICL", "Don't do Credit Check", false, 0, null, "IDHICL");
            oGridControl.doAddListField("b.U_IDHLBPC", "Linked BP Code", false, 0, null, "IDHLBPC");
            oGridControl.doAddListField("b.U_IDHLBPN", "Linked BP Name", false, 0, null, "IDHLBPN");
            oGridControl.doAddListField("b.U_IDHOBLGT", "Obligated",false, 0, null, "IDHOBLGT");
            oGridControl.doAddListField("b.U_IDHUOM", "BP UOM",false, 0, null, "IDHUOM");
            oGridControl.doAddListField("b.U_IDHONCS", "Compliance Scheme",false, 0, null, "IDHONCS");
            oGridControl.doAddListField("c.frozenFrom", "Frozen From", false, 0, null, "FROZENF");
            oGridControl.doAddListField("c.frozenTo", "Frozen To", false, 0, null, "FROZENT");
		}
		
#region fillcombos
		private void doFillType(){
			object oNCombo = null;
			if ( moSBOForm != null ){
	            SAPbouiCOM.Item oItem;
	            SAPbouiCOM.ComboBox oCombo;
	            oItem = moSBOForm.Items.Item("IDH_TYPE");
	            oItem.DisplayDesc = true;
	            oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
	
	            oNCombo = oCombo;
	        	com.idh.bridge.utils.Combobox.doClearCombo( ref oNCombo );
			}
			else if ( moWinForm != null ) {
				oNCombo = getWinControl("IDH_TYPE");
			}
	            
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"", "Any");
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"S", "Vendor");
            com.idh.bridge.utils.Combobox.doAddValueToCombo(ref oNCombo,"C", "Customer");
		}
		private void doFillGroup(){
			string sType = (string)getUFValue("IDH_TYPE");
			
			object oForm = ( moWinForm != null ? (object)moWinForm: (object)moSBOForm );
			com.idh.bridge.utils.Combobox.doFillCombo( oForm, "IDH_GROUP", "OCRG", "GroupCode", "GroupName", "GroupType Like'%" + sType + '\'', "GroupCode", true);
		}
		private void doFillBranch(){
			object oForm = ( moWinForm != null ? (object)moWinForm: (object)moSBOForm );
			com.idh.bridge.utils.Combobox.doFillCombo( oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks", null, "Code", true);
		}
#endregion
	}
}
