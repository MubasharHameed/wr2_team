﻿using com.idh.win.controls.SBO;
namespace com.idh.bridge.form {
    partial class WO {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.sboTextBox1 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox2 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox3 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox4 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox5 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox6 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox7 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox8 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox9 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox11 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.sboTextBox13 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox15 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox16 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox19 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox20 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.sboTextBox23 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox24 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox25 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox26 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox27 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox28 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox29 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox30 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox32 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox33 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.IDHTabs = new System.Windows.Forms.TabControl();
            this.IDH_TABCOS = new System.Windows.Forms.TabPage();
            this.label68 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.sboTextBox52 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox54 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox55 = new com.idh.win.controls.SBO.SBOTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.sboComboBox4 = new com.idh.win.controls.SBO.SBOComboBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.sboTextBox70 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox71 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox72 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.sboTextBox10 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox12 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox14 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox17 = new com.idh.win.controls.SBO.SBOTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.sboComboBox3 = new com.idh.win.controls.SBO.SBOComboBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.sboTextBox65 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox66 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox67 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.sboTextBox18 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox21 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox50 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox51 = new com.idh.win.controls.SBO.SBOTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.sboComboBox2 = new com.idh.win.controls.SBO.SBOComboBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.sboTextBox58 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox61 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox62 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.sboTextBox46 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox47 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox48 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox49 = new com.idh.win.controls.SBO.SBOTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.sboComboBox1 = new com.idh.win.controls.SBO.SBOComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.sboTextBox60 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox59 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox57 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox56 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox53 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.sboTextBox35 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox43 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox44 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox45 = new com.idh.win.controls.SBO.SBOTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.sboTextBox34 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox36 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox37 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox38 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox39 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox40 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox41 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox42 = new com.idh.win.controls.SBO.SBOTextBox();
            this.IDH_TABCOM = new System.Windows.Forms.TabPage();
            this.IDH_TABADD = new System.Windows.Forms.TabPage();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.sboTextBox73 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox74 = new com.idh.win.controls.SBO.SBOTextBox();
            this.oAddGrid = new com.idh.win.controls.WR1Grid();
            this.label78 = new System.Windows.Forms.Label();
            this.IDH_TABACT = new System.Windows.Forms.TabPage();
            this.wR1Grid2 = new com.idh.win.controls.WR1Grid();
            this.label81 = new System.Windows.Forms.Label();
            this.IDH_TABADM = new System.Windows.Forms.TabPage();
            this.IDH_TABEXP = new System.Windows.Forms.TabPage();
            this.IDH_TABWEI = new System.Windows.Forms.TabPage();
            this.IDH_TABDED = new System.Windows.Forms.TabPage();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.sboTextBox75 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox76 = new com.idh.win.controls.SBO.SBOTextBox();
            this.oDeductionsGrid = new com.idh.win.controls.WR1Grid();
            this.label84 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.sboComboBox5 = new com.idh.win.controls.SBO.SBOComboBox();
            this.sboComboBox6 = new com.idh.win.controls.SBO.SBOComboBox();
            this.sboTextBox22 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox31 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox63 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox64 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox68 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.sboTextBox69 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox77 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox78 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.sboTextBox79 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox80 = new com.idh.win.controls.SBO.SBOTextBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.sboComboBox7 = new com.idh.win.controls.SBO.SBOComboBox();
            this.sboTextBox81 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox82 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox83 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox84 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox85 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox86 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox87 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox88 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox89 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.sboTextBox90 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox91 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox92 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox93 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox94 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox95 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox96 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox97 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox98 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox99 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox100 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox101 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox102 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox103 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox104 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox105 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox106 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox107 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox108 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox109 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox110 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox111 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox112 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox113 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox114 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox115 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox116 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox117 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox118 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox119 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox120 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox121 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox122 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox123 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox124 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox125 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox126 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox127 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox128 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox129 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox130 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.sboTextBox131 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox132 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox133 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox134 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox135 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox136 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox137 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox138 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox139 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox140 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox141 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox142 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox143 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox144 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox145 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox146 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox147 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox148 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox149 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox150 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox151 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox152 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox153 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox154 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox155 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox156 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox157 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox158 = new com.idh.win.controls.SBO.SBOTextBox();
            this.label133 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.sboTextBox160 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox161 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox162 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox163 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox164 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox165 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox166 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox167 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox168 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox169 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboTextBox170 = new com.idh.win.controls.SBO.SBOTextBox();
            this.sboComboBox8 = new com.idh.win.controls.SBO.SBOComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label161 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.IDHTabs.SuspendLayout();
            this.IDH_TABCOS.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.IDH_TABCOM.SuspendLayout();
            this.IDH_TABADD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oAddGrid)).BeginInit();
            this.IDH_TABACT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wR1Grid2)).BeginInit();
            this.IDH_TABADM.SuspendLayout();
            this.IDH_TABEXP.SuspendLayout();
            this.IDH_TABWEI.SuspendLayout();
            this.IDH_TABDED.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oDeductionsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Business Partner";
            // 
            // sboTextBox1
            // 
            this.sboTextBox1.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox1.CornerRadius = 2;
            this.sboTextBox1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox1.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox1.Location = new System.Drawing.Point(97, 5);
            this.sboTextBox1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox1.Multiline = true;
            this.sboTextBox1.Name = "sboTextBox1";
            this.sboTextBox1.ReadOnly = false;
            this.sboTextBox1.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox1.TabIndex = 1;
            this.sboTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox1.TextValue = "";
            // 
            // sboTextBox2
            // 
            this.sboTextBox2.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox2.CornerRadius = 2;
            this.sboTextBox2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox2.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox2.Location = new System.Drawing.Point(97, 20);
            this.sboTextBox2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox2.Multiline = true;
            this.sboTextBox2.Name = "sboTextBox2";
            this.sboTextBox2.ReadOnly = false;
            this.sboTextBox2.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox2.TabIndex = 2;
            this.sboTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox2.TextValue = "";
            // 
            // sboTextBox3
            // 
            this.sboTextBox3.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox3.CornerRadius = 2;
            this.sboTextBox3.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox3.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox3.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox3.Location = new System.Drawing.Point(97, 50);
            this.sboTextBox3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox3.Multiline = true;
            this.sboTextBox3.Name = "sboTextBox3";
            this.sboTextBox3.ReadOnly = false;
            this.sboTextBox3.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox3.TabIndex = 3;
            this.sboTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox3.TextValue = "";
            // 
            // sboTextBox4
            // 
            this.sboTextBox4.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox4.CornerRadius = 2;
            this.sboTextBox4.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox4.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox4.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox4.Location = new System.Drawing.Point(97, 65);
            this.sboTextBox4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox4.Multiline = true;
            this.sboTextBox4.Name = "sboTextBox4";
            this.sboTextBox4.ReadOnly = false;
            this.sboTextBox4.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox4.TabIndex = 4;
            this.sboTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox4.TextValue = "";
            // 
            // sboTextBox5
            // 
            this.sboTextBox5.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox5.CornerRadius = 2;
            this.sboTextBox5.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox5.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox5.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox5.Location = new System.Drawing.Point(97, 80);
            this.sboTextBox5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox5.Multiline = true;
            this.sboTextBox5.Name = "sboTextBox5";
            this.sboTextBox5.ReadOnly = false;
            this.sboTextBox5.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox5.TabIndex = 5;
            this.sboTextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox5.TextValue = "";
            // 
            // sboTextBox6
            // 
            this.sboTextBox6.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox6.CornerRadius = 2;
            this.sboTextBox6.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox6.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox6.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox6.Location = new System.Drawing.Point(97, 95);
            this.sboTextBox6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox6.Multiline = true;
            this.sboTextBox6.Name = "sboTextBox6";
            this.sboTextBox6.ReadOnly = false;
            this.sboTextBox6.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox6.TabIndex = 6;
            this.sboTextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox6.TextValue = "";
            // 
            // sboTextBox7
            // 
            this.sboTextBox7.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox7.CornerRadius = 2;
            this.sboTextBox7.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox7.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox7.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox7.Location = new System.Drawing.Point(97, 110);
            this.sboTextBox7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox7.Multiline = true;
            this.sboTextBox7.Name = "sboTextBox7";
            this.sboTextBox7.ReadOnly = false;
            this.sboTextBox7.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox7.TabIndex = 7;
            this.sboTextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox7.TextValue = "";
            // 
            // sboTextBox8
            // 
            this.sboTextBox8.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox8.CornerRadius = 2;
            this.sboTextBox8.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox8.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox8.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox8.Location = new System.Drawing.Point(97, 125);
            this.sboTextBox8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox8.Multiline = true;
            this.sboTextBox8.Name = "sboTextBox8";
            this.sboTextBox8.ReadOnly = false;
            this.sboTextBox8.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox8.TabIndex = 8;
            this.sboTextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox8.TextValue = "";
            // 
            // sboTextBox9
            // 
            this.sboTextBox9.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox9.CornerRadius = 2;
            this.sboTextBox9.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox9.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox9.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox9.Location = new System.Drawing.Point(97, 140);
            this.sboTextBox9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox9.Multiline = true;
            this.sboTextBox9.Name = "sboTextBox9";
            this.sboTextBox9.ReadOnly = false;
            this.sboTextBox9.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox9.TabIndex = 9;
            this.sboTextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox9.TextValue = "";
            // 
            // sboTextBox11
            // 
            this.sboTextBox11.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox11.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox11.CornerRadius = 2;
            this.sboTextBox11.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox11.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox11.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox11.Location = new System.Drawing.Point(97, 170);
            this.sboTextBox11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox11.Multiline = true;
            this.sboTextBox11.Name = "sboTextBox11";
            this.sboTextBox11.ReadOnly = false;
            this.sboTextBox11.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox11.TabIndex = 11;
            this.sboTextBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox11.TextValue = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 12);
            this.label2.TabIndex = 12;
            this.label2.Text = "Contact Person";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 12);
            this.label3.TabIndex = 13;
            this.label3.Text = "Site Address";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 12);
            this.label4.TabIndex = 14;
            this.label4.Text = "Street";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 12);
            this.label5.TabIndex = 15;
            this.label5.Text = "Block";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 12);
            this.label6.TabIndex = 16;
            this.label6.Text = "City";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 12);
            this.label7.TabIndex = 17;
            this.label7.Text = "Post Code";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 12);
            this.label8.TabIndex = 18;
            this.label8.Text = "Phone No";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 142);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 12);
            this.label9.TabIndex = 19;
            this.label9.Text = "Origin";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 172);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 12);
            this.label11.TabIndex = 21;
            this.label11.Text = "Ref No";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(320, 142);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 12);
            this.label13.TabIndex = 42;
            this.label13.Text = "Vehicle Reg No";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(320, 112);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 12);
            this.label15.TabIndex = 40;
            this.label15.Text = "Material Name";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(320, 97);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 12);
            this.label16.TabIndex = 39;
            this.label16.Text = "Material Code";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(320, 52);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 12);
            this.label19.TabIndex = 36;
            this.label19.Text = "Container Name";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(320, 35);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 12);
            this.label20.TabIndex = 35;
            this.label20.Text = "Container Code";
            // 
            // sboTextBox13
            // 
            this.sboTextBox13.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox13.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox13.CornerRadius = 2;
            this.sboTextBox13.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox13.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox13.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox13.Location = new System.Drawing.Point(412, 140);
            this.sboTextBox13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox13.Multiline = true;
            this.sboTextBox13.Name = "sboTextBox13";
            this.sboTextBox13.ReadOnly = false;
            this.sboTextBox13.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox13.TabIndex = 32;
            this.sboTextBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox13.TextValue = "";
            // 
            // sboTextBox15
            // 
            this.sboTextBox15.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox15.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox15.CornerRadius = 2;
            this.sboTextBox15.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox15.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox15.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox15.Location = new System.Drawing.Point(412, 110);
            this.sboTextBox15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox15.Multiline = true;
            this.sboTextBox15.Name = "sboTextBox15";
            this.sboTextBox15.ReadOnly = false;
            this.sboTextBox15.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox15.TabIndex = 30;
            this.sboTextBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox15.TextValue = "";
            // 
            // sboTextBox16
            // 
            this.sboTextBox16.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox16.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox16.CornerRadius = 2;
            this.sboTextBox16.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox16.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox16.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox16.Location = new System.Drawing.Point(412, 95);
            this.sboTextBox16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox16.Multiline = true;
            this.sboTextBox16.Name = "sboTextBox16";
            this.sboTextBox16.ReadOnly = false;
            this.sboTextBox16.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox16.TabIndex = 29;
            this.sboTextBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox16.TextValue = "";
            // 
            // sboTextBox19
            // 
            this.sboTextBox19.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox19.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox19.CornerRadius = 2;
            this.sboTextBox19.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox19.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox19.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox19.Location = new System.Drawing.Point(412, 50);
            this.sboTextBox19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox19.Multiline = true;
            this.sboTextBox19.Name = "sboTextBox19";
            this.sboTextBox19.ReadOnly = false;
            this.sboTextBox19.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox19.TabIndex = 26;
            this.sboTextBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox19.TextValue = "";
            // 
            // sboTextBox20
            // 
            this.sboTextBox20.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox20.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox20.CornerRadius = 2;
            this.sboTextBox20.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox20.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox20.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox20.Location = new System.Drawing.Point(412, 35);
            this.sboTextBox20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox20.Multiline = true;
            this.sboTextBox20.Name = "sboTextBox20";
            this.sboTextBox20.ReadOnly = false;
            this.sboTextBox20.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox20.TabIndex = 25;
            this.sboTextBox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox20.TextValue = "";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(320, 5);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 12);
            this.label22.TabIndex = 22;
            this.label22.Text = "Order Type";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(731, 185);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(20, 12);
            this.label23.TabIndex = 65;
            this.label23.Text = "PBI";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(731, 170);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(34, 12);
            this.label24.TabIndex = 64;
            this.label24.Text = "Branch";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(731, 155);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(25, 12);
            this.label25.TabIndex = 63;
            this.label25.Text = "User";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(641, 127);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 12);
            this.label26.TabIndex = 62;
            this.label26.Text = "End Date";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(641, 112);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(57, 12);
            this.label27.TabIndex = 61;
            this.label27.Text = "Started Date";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(641, 97);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(72, 12);
            this.label28.TabIndex = 60;
            this.label28.Text = "Collect/Delivery";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(641, 82);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(72, 12);
            this.label29.TabIndex = 59;
            this.label29.Text = "Requested Date";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(641, 52);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(60, 12);
            this.label30.TabIndex = 58;
            this.label30.Text = "Created Date";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(641, 35);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(32, 12);
            this.label31.TabIndex = 57;
            this.label31.Text = "Status";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(641, 20);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(36, 12);
            this.label32.TabIndex = 56;
            this.label32.Text = "Line No";
            // 
            // sboTextBox23
            // 
            this.sboTextBox23.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox23.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox23.CornerRadius = 2;
            this.sboTextBox23.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox23.Enabled = false;
            this.sboTextBox23.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox23.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox23.Location = new System.Drawing.Point(832, 185);
            this.sboTextBox23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox23.Multiline = true;
            this.sboTextBox23.Name = "sboTextBox23";
            this.sboTextBox23.ReadOnly = false;
            this.sboTextBox23.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox23.TabIndex = 55;
            this.sboTextBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox23.TextValue = "";
            // 
            // sboTextBox24
            // 
            this.sboTextBox24.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox24.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox24.CornerRadius = 2;
            this.sboTextBox24.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox24.Enabled = false;
            this.sboTextBox24.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox24.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox24.Location = new System.Drawing.Point(832, 170);
            this.sboTextBox24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox24.Multiline = true;
            this.sboTextBox24.Name = "sboTextBox24";
            this.sboTextBox24.ReadOnly = false;
            this.sboTextBox24.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox24.TabIndex = 54;
            this.sboTextBox24.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox24.TextValue = "";
            // 
            // sboTextBox25
            // 
            this.sboTextBox25.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox25.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox25.CornerRadius = 2;
            this.sboTextBox25.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox25.Enabled = false;
            this.sboTextBox25.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox25.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox25.Location = new System.Drawing.Point(832, 155);
            this.sboTextBox25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox25.Multiline = true;
            this.sboTextBox25.Name = "sboTextBox25";
            this.sboTextBox25.ReadOnly = false;
            this.sboTextBox25.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox25.TabIndex = 53;
            this.sboTextBox25.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox25.TextValue = "";
            // 
            // sboTextBox26
            // 
            this.sboTextBox26.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox26.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox26.CornerRadius = 2;
            this.sboTextBox26.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox26.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox26.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox26.Location = new System.Drawing.Point(733, 125);
            this.sboTextBox26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox26.Multiline = true;
            this.sboTextBox26.Name = "sboTextBox26";
            this.sboTextBox26.ReadOnly = false;
            this.sboTextBox26.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox26.TabIndex = 52;
            this.sboTextBox26.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox26.TextValue = "";
            // 
            // sboTextBox27
            // 
            this.sboTextBox27.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox27.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox27.CornerRadius = 2;
            this.sboTextBox27.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox27.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox27.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox27.Location = new System.Drawing.Point(733, 110);
            this.sboTextBox27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox27.Multiline = true;
            this.sboTextBox27.Name = "sboTextBox27";
            this.sboTextBox27.ReadOnly = false;
            this.sboTextBox27.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox27.TabIndex = 51;
            this.sboTextBox27.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox27.TextValue = "";
            // 
            // sboTextBox28
            // 
            this.sboTextBox28.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox28.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox28.CornerRadius = 2;
            this.sboTextBox28.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox28.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox28.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox28.Location = new System.Drawing.Point(733, 95);
            this.sboTextBox28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox28.Multiline = true;
            this.sboTextBox28.Name = "sboTextBox28";
            this.sboTextBox28.ReadOnly = false;
            this.sboTextBox28.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox28.TabIndex = 50;
            this.sboTextBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox28.TextValue = "";
            // 
            // sboTextBox29
            // 
            this.sboTextBox29.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox29.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox29.CornerRadius = 2;
            this.sboTextBox29.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox29.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox29.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox29.Location = new System.Drawing.Point(733, 80);
            this.sboTextBox29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox29.Multiline = true;
            this.sboTextBox29.Name = "sboTextBox29";
            this.sboTextBox29.ReadOnly = false;
            this.sboTextBox29.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox29.TabIndex = 49;
            this.sboTextBox29.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox29.TextValue = "";
            // 
            // sboTextBox30
            // 
            this.sboTextBox30.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox30.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox30.CornerRadius = 2;
            this.sboTextBox30.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox30.Enabled = false;
            this.sboTextBox30.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox30.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox30.Location = new System.Drawing.Point(733, 50);
            this.sboTextBox30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox30.Multiline = true;
            this.sboTextBox30.Name = "sboTextBox30";
            this.sboTextBox30.ReadOnly = false;
            this.sboTextBox30.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox30.TabIndex = 48;
            this.sboTextBox30.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox30.TextValue = "";
            // 
            // sboTextBox32
            // 
            this.sboTextBox32.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox32.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox32.CornerRadius = 2;
            this.sboTextBox32.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox32.Enabled = false;
            this.sboTextBox32.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox32.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox32.Location = new System.Drawing.Point(733, 20);
            this.sboTextBox32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox32.Multiline = true;
            this.sboTextBox32.Name = "sboTextBox32";
            this.sboTextBox32.ReadOnly = false;
            this.sboTextBox32.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox32.TabIndex = 46;
            this.sboTextBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox32.TextValue = "";
            // 
            // sboTextBox33
            // 
            this.sboTextBox33.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox33.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox33.CornerRadius = 2;
            this.sboTextBox33.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox33.Enabled = false;
            this.sboTextBox33.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox33.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox33.Location = new System.Drawing.Point(733, 5);
            this.sboTextBox33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox33.Multiline = true;
            this.sboTextBox33.Name = "sboTextBox33";
            this.sboTextBox33.ReadOnly = false;
            this.sboTextBox33.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox33.TabIndex = 45;
            this.sboTextBox33.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox33.TextValue = "";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(641, 5);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(42, 12);
            this.label33.TabIndex = 44;
            this.label33.Text = "Order No";
            // 
            // IDHTabs
            // 
            this.IDHTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IDHTabs.Controls.Add(this.IDH_TABCOS);
            this.IDHTabs.Controls.Add(this.IDH_TABCOM);
            this.IDHTabs.Controls.Add(this.IDH_TABADD);
            this.IDHTabs.Controls.Add(this.IDH_TABACT);
            this.IDHTabs.Controls.Add(this.IDH_TABADM);
            this.IDHTabs.Controls.Add(this.IDH_TABEXP);
            this.IDHTabs.Controls.Add(this.IDH_TABWEI);
            this.IDHTabs.Controls.Add(this.IDH_TABDED);
            this.IDHTabs.Location = new System.Drawing.Point(0, 201);
            this.IDHTabs.Name = "IDHTabs";
            this.IDHTabs.Padding = new System.Drawing.Point(10, 3);
            this.IDHTabs.SelectedIndex = 0;
            this.IDHTabs.Size = new System.Drawing.Size(948, 553);
            this.IDHTabs.TabIndex = 66;
            // 
            // IDH_TABCOS
            // 
            this.IDH_TABCOS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(241)))), ((int)(((byte)(246)))));
            this.IDH_TABCOS.Controls.Add(this.label68);
            this.IDH_TABCOS.Controls.Add(this.label63);
            this.IDH_TABCOS.Controls.Add(this.label62);
            this.IDH_TABCOS.Controls.Add(this.sboTextBox52);
            this.IDH_TABCOS.Controls.Add(this.sboTextBox54);
            this.IDH_TABCOS.Controls.Add(this.sboTextBox55);
            this.IDH_TABCOS.Controls.Add(this.groupBox4);
            this.IDH_TABCOS.Controls.Add(this.groupBox5);
            this.IDH_TABCOS.Controls.Add(this.groupBox3);
            this.IDH_TABCOS.Controls.Add(this.groupBox2);
            this.IDH_TABCOS.Controls.Add(this.groupBox1);
            this.IDH_TABCOS.Location = new System.Drawing.Point(4, 21);
            this.IDH_TABCOS.Name = "IDH_TABCOS";
            this.IDH_TABCOS.Padding = new System.Windows.Forms.Padding(3);
            this.IDH_TABCOS.Size = new System.Drawing.Size(940, 528);
            this.IDH_TABCOS.TabIndex = 0;
            this.IDH_TABCOS.Text = "Costing";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(766, 507);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(50, 12);
            this.label68.TabIndex = 32;
            this.label68.Text = "Profit/Loss";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(764, 485);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(52, 12);
            this.label63.TabIndex = 31;
            this.label63.Text = "Total Costs";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(721, 470);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(95, 12);
            this.label62.TabIndex = 30;
            this.label62.Text = "Total Additional Costs";
            // 
            // sboTextBox52
            // 
            this.sboTextBox52.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox52.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox52.CornerRadius = 2;
            this.sboTextBox52.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox52.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox52.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox52.Location = new System.Drawing.Point(828, 507);
            this.sboTextBox52.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox52.Multiline = true;
            this.sboTextBox52.Name = "sboTextBox52";
            this.sboTextBox52.ReadOnly = false;
            this.sboTextBox52.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox52.TabIndex = 29;
            this.sboTextBox52.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox52.TextValue = "";
            // 
            // sboTextBox54
            // 
            this.sboTextBox54.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox54.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox54.CornerRadius = 2;
            this.sboTextBox54.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox54.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox54.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox54.Location = new System.Drawing.Point(828, 485);
            this.sboTextBox54.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox54.Multiline = true;
            this.sboTextBox54.Name = "sboTextBox54";
            this.sboTextBox54.ReadOnly = false;
            this.sboTextBox54.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox54.TabIndex = 27;
            this.sboTextBox54.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox54.TextValue = "";
            // 
            // sboTextBox55
            // 
            this.sboTextBox55.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox55.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox55.CornerRadius = 2;
            this.sboTextBox55.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox55.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox55.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox55.Location = new System.Drawing.Point(828, 470);
            this.sboTextBox55.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox55.Multiline = true;
            this.sboTextBox55.Name = "sboTextBox55";
            this.sboTextBox55.ReadOnly = false;
            this.sboTextBox55.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox55.TabIndex = 26;
            this.sboTextBox55.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox55.TextValue = "";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.sboComboBox4);
            this.groupBox4.Controls.Add(this.label70);
            this.groupBox4.Controls.Add(this.label71);
            this.groupBox4.Controls.Add(this.label72);
            this.groupBox4.Controls.Add(this.label73);
            this.groupBox4.Controls.Add(this.checkBox6);
            this.groupBox4.Controls.Add(this.sboTextBox70);
            this.groupBox4.Controls.Add(this.sboTextBox71);
            this.groupBox4.Controls.Add(this.sboTextBox72);
            this.groupBox4.Controls.Add(this.label40);
            this.groupBox4.Controls.Add(this.label41);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Controls.Add(this.label43);
            this.groupBox4.Controls.Add(this.button10);
            this.groupBox4.Controls.Add(this.sboTextBox10);
            this.groupBox4.Controls.Add(this.sboTextBox12);
            this.groupBox4.Controls.Add(this.sboTextBox14);
            this.groupBox4.Controls.Add(this.sboTextBox17);
            this.groupBox4.Location = new System.Drawing.Point(3, 391);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(932, 77);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Container Licence Cost";
            // 
            // sboComboBox4
            // 
            this.sboComboBox4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboComboBox4.CornerRadius = 2;
            this.sboComboBox4.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboComboBox4.DropDownHight = 100;
            this.sboComboBox4.EnabledColor = System.Drawing.Color.White;
            this.sboComboBox4.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboComboBox4.Location = new System.Drawing.Point(525, 41);
            this.sboComboBox4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboComboBox4.Multiline = false;
            this.sboComboBox4.Name = "sboComboBox4";
            this.sboComboBox4.SelectedIndex = -1;
            this.sboComboBox4.SelectedItem = null;
            this.sboComboBox4.Size = new System.Drawing.Size(50, 14);
            this.sboComboBox4.TabIndex = 54;
            this.sboComboBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboComboBox4.TextBoxReadOnly = true;
            this.sboComboBox4.TextValue = "";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(754, 41);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(59, 12);
            this.label70.TabIndex = 53;
            this.label70.Text = "Licence Cost";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(603, 41);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(78, 12);
            this.label71.TabIndex = 52;
            this.label71.Text = "Licence Unit Cost";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(493, 41);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(28, 12);
            this.label72.TabIndex = 51;
            this.label72.Text = "UOM";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(371, 41);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(54, 12);
            this.label73.TabIndex = 50;
            this.label73.Text = "Licence Qty";
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(825, 56);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(88, 16);
            this.checkBox6.TabIndex = 47;
            this.checkBox6.Text = "Purchase Order";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // sboTextBox70
            // 
            this.sboTextBox70.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox70.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox70.CornerRadius = 2;
            this.sboTextBox70.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox70.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox70.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox70.Location = new System.Drawing.Point(825, 41);
            this.sboTextBox70.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox70.Multiline = true;
            this.sboTextBox70.Name = "sboTextBox70";
            this.sboTextBox70.ReadOnly = false;
            this.sboTextBox70.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox70.TabIndex = 46;
            this.sboTextBox70.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox70.TextValue = "";
            // 
            // sboTextBox71
            // 
            this.sboTextBox71.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox71.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox71.CornerRadius = 2;
            this.sboTextBox71.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox71.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox71.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox71.Location = new System.Drawing.Point(688, 41);
            this.sboTextBox71.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox71.Multiline = false;
            this.sboTextBox71.Name = "sboTextBox71";
            this.sboTextBox71.ReadOnly = false;
            this.sboTextBox71.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox71.TabIndex = 45;
            this.sboTextBox71.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox71.TextValue = "";
            // 
            // sboTextBox72
            // 
            this.sboTextBox72.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox72.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox72.CornerRadius = 2;
            this.sboTextBox72.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox72.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox72.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox72.Location = new System.Drawing.Point(430, 41);
            this.sboTextBox72.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox72.Multiline = false;
            this.sboTextBox72.Name = "sboTextBox72";
            this.sboTextBox72.ReadOnly = false;
            this.sboTextBox72.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox72.TabIndex = 44;
            this.sboTextBox72.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox72.TextValue = "";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(139, 26);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(78, 12);
            this.label40.TabIndex = 31;
            this.label40.Text = "Licence BP Name";
            this.label40.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(146, 56);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(71, 12);
            this.label41.TabIndex = 30;
            this.label41.Text = "Licence Ref No.";
            this.label41.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(140, 41);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(77, 12);
            this.label42.TabIndex = 29;
            this.label42.Text = "Licence Site Addr";
            this.label42.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(142, 11);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(75, 12);
            this.label43.TabIndex = 28;
            this.label43.Text = "Licence BP Code";
            this.label43.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button10.Location = new System.Drawing.Point(25, 30);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 23;
            this.button10.Text = "Price List";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // sboTextBox10
            // 
            this.sboTextBox10.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox10.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox10.CornerRadius = 2;
            this.sboTextBox10.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox10.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox10.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox10.Location = new System.Drawing.Point(228, 56);
            this.sboTextBox10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox10.Multiline = true;
            this.sboTextBox10.Name = "sboTextBox10";
            this.sboTextBox10.ReadOnly = false;
            this.sboTextBox10.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox10.TabIndex = 22;
            this.sboTextBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox10.TextValue = "";
            // 
            // sboTextBox12
            // 
            this.sboTextBox12.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox12.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox12.CornerRadius = 2;
            this.sboTextBox12.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox12.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox12.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox12.Location = new System.Drawing.Point(228, 41);
            this.sboTextBox12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox12.Multiline = true;
            this.sboTextBox12.Name = "sboTextBox12";
            this.sboTextBox12.ReadOnly = false;
            this.sboTextBox12.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox12.TabIndex = 21;
            this.sboTextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox12.TextValue = "";
            // 
            // sboTextBox14
            // 
            this.sboTextBox14.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox14.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox14.CornerRadius = 2;
            this.sboTextBox14.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox14.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox14.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox14.Location = new System.Drawing.Point(228, 26);
            this.sboTextBox14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox14.Multiline = true;
            this.sboTextBox14.Name = "sboTextBox14";
            this.sboTextBox14.ReadOnly = false;
            this.sboTextBox14.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox14.TabIndex = 20;
            this.sboTextBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox14.TextValue = "";
            // 
            // sboTextBox17
            // 
            this.sboTextBox17.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox17.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox17.CornerRadius = 2;
            this.sboTextBox17.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox17.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox17.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox17.Location = new System.Drawing.Point(228, 11);
            this.sboTextBox17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox17.Multiline = true;
            this.sboTextBox17.Name = "sboTextBox17";
            this.sboTextBox17.ReadOnly = false;
            this.sboTextBox17.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox17.TabIndex = 19;
            this.sboTextBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox17.TextValue = "";
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.sboComboBox3);
            this.groupBox5.Controls.Add(this.label64);
            this.groupBox5.Controls.Add(this.label65);
            this.groupBox5.Controls.Add(this.label66);
            this.groupBox5.Controls.Add(this.label67);
            this.groupBox5.Controls.Add(this.checkBox5);
            this.groupBox5.Controls.Add(this.sboTextBox65);
            this.groupBox5.Controls.Add(this.sboTextBox66);
            this.groupBox5.Controls.Add(this.sboTextBox67);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.label37);
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this.label39);
            this.groupBox5.Controls.Add(this.button9);
            this.groupBox5.Controls.Add(this.sboTextBox18);
            this.groupBox5.Controls.Add(this.sboTextBox21);
            this.groupBox5.Controls.Add(this.sboTextBox50);
            this.groupBox5.Controls.Add(this.sboTextBox51);
            this.groupBox5.Location = new System.Drawing.Point(3, 314);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(932, 77);
            this.groupBox5.TabIndex = 24;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Carrier Cost";
            // 
            // sboComboBox3
            // 
            this.sboComboBox3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboComboBox3.CornerRadius = 2;
            this.sboComboBox3.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboComboBox3.DropDownHight = 100;
            this.sboComboBox3.EnabledColor = System.Drawing.Color.White;
            this.sboComboBox3.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboComboBox3.Location = new System.Drawing.Point(525, 41);
            this.sboComboBox3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboComboBox3.Multiline = false;
            this.sboComboBox3.Name = "sboComboBox3";
            this.sboComboBox3.SelectedIndex = -1;
            this.sboComboBox3.SelectedItem = null;
            this.sboComboBox3.Size = new System.Drawing.Size(50, 14);
            this.sboComboBox3.TabIndex = 54;
            this.sboComboBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboComboBox3.TextBoxReadOnly = true;
            this.sboComboBox3.TextValue = "";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(754, 41);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(61, 12);
            this.label64.TabIndex = 53;
            this.label64.Text = "Haulage Cost";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(601, 41);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(80, 12);
            this.label65.TabIndex = 52;
            this.label65.Text = "Haulage Unit Cost";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(493, 41);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(28, 12);
            this.label66.TabIndex = 51;
            this.label66.Text = "UOM";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(369, 41);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(56, 12);
            this.label67.TabIndex = 50;
            this.label67.Text = "Haulage Qty";
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(825, 56);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(88, 16);
            this.checkBox5.TabIndex = 47;
            this.checkBox5.Text = "Purchase Order";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // sboTextBox65
            // 
            this.sboTextBox65.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox65.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox65.CornerRadius = 2;
            this.sboTextBox65.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox65.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox65.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox65.Location = new System.Drawing.Point(825, 41);
            this.sboTextBox65.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox65.Multiline = true;
            this.sboTextBox65.Name = "sboTextBox65";
            this.sboTextBox65.ReadOnly = false;
            this.sboTextBox65.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox65.TabIndex = 46;
            this.sboTextBox65.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox65.TextValue = "";
            // 
            // sboTextBox66
            // 
            this.sboTextBox66.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox66.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox66.CornerRadius = 2;
            this.sboTextBox66.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox66.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox66.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox66.Location = new System.Drawing.Point(688, 41);
            this.sboTextBox66.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox66.Multiline = false;
            this.sboTextBox66.Name = "sboTextBox66";
            this.sboTextBox66.ReadOnly = false;
            this.sboTextBox66.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox66.TabIndex = 45;
            this.sboTextBox66.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox66.TextValue = "";
            // 
            // sboTextBox67
            // 
            this.sboTextBox67.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox67.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox67.CornerRadius = 2;
            this.sboTextBox67.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox67.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox67.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox67.Location = new System.Drawing.Point(430, 41);
            this.sboTextBox67.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox67.Multiline = false;
            this.sboTextBox67.Name = "sboTextBox67";
            this.sboTextBox67.ReadOnly = false;
            this.sboTextBox67.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox67.TabIndex = 44;
            this.sboTextBox67.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox67.TextValue = "";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(143, 26);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(74, 12);
            this.label36.TabIndex = 31;
            this.label36.Text = "Carrier BP Name";
            this.label36.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(150, 56);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(67, 12);
            this.label37.TabIndex = 30;
            this.label37.Text = "Carrier Ref No.";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(144, 41);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(73, 12);
            this.label38.TabIndex = 29;
            this.label38.Text = "Carrier Site Addr";
            this.label38.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(146, 11);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(71, 12);
            this.label39.TabIndex = 28;
            this.label39.Text = "Carrier BP Code";
            this.label39.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button9.Location = new System.Drawing.Point(25, 30);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 23;
            this.button9.Text = "Price List";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // sboTextBox18
            // 
            this.sboTextBox18.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox18.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox18.CornerRadius = 2;
            this.sboTextBox18.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox18.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox18.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox18.Location = new System.Drawing.Point(228, 56);
            this.sboTextBox18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox18.Multiline = true;
            this.sboTextBox18.Name = "sboTextBox18";
            this.sboTextBox18.ReadOnly = false;
            this.sboTextBox18.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox18.TabIndex = 22;
            this.sboTextBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox18.TextValue = "";
            // 
            // sboTextBox21
            // 
            this.sboTextBox21.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox21.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox21.CornerRadius = 2;
            this.sboTextBox21.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox21.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox21.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox21.Location = new System.Drawing.Point(228, 41);
            this.sboTextBox21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox21.Multiline = true;
            this.sboTextBox21.Name = "sboTextBox21";
            this.sboTextBox21.ReadOnly = false;
            this.sboTextBox21.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox21.TabIndex = 21;
            this.sboTextBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox21.TextValue = "";
            // 
            // sboTextBox50
            // 
            this.sboTextBox50.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox50.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox50.CornerRadius = 2;
            this.sboTextBox50.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox50.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox50.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox50.Location = new System.Drawing.Point(228, 26);
            this.sboTextBox50.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox50.Multiline = true;
            this.sboTextBox50.Name = "sboTextBox50";
            this.sboTextBox50.ReadOnly = false;
            this.sboTextBox50.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox50.TabIndex = 20;
            this.sboTextBox50.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox50.TextValue = "";
            // 
            // sboTextBox51
            // 
            this.sboTextBox51.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox51.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox51.CornerRadius = 2;
            this.sboTextBox51.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox51.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox51.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox51.Location = new System.Drawing.Point(228, 11);
            this.sboTextBox51.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox51.Multiline = true;
            this.sboTextBox51.Name = "sboTextBox51";
            this.sboTextBox51.ReadOnly = false;
            this.sboTextBox51.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox51.TabIndex = 19;
            this.sboTextBox51.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox51.TextValue = "";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.sboComboBox2);
            this.groupBox3.Controls.Add(this.label58);
            this.groupBox3.Controls.Add(this.label59);
            this.groupBox3.Controls.Add(this.label60);
            this.groupBox3.Controls.Add(this.label61);
            this.groupBox3.Controls.Add(this.checkBox4);
            this.groupBox3.Controls.Add(this.sboTextBox58);
            this.groupBox3.Controls.Add(this.sboTextBox61);
            this.groupBox3.Controls.Add(this.sboTextBox62);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this.button8);
            this.groupBox3.Controls.Add(this.sboTextBox46);
            this.groupBox3.Controls.Add(this.sboTextBox47);
            this.groupBox3.Controls.Add(this.sboTextBox48);
            this.groupBox3.Controls.Add(this.sboTextBox49);
            this.groupBox3.Location = new System.Drawing.Point(3, 236);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(932, 77);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Disposal Cost";
            // 
            // sboComboBox2
            // 
            this.sboComboBox2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboComboBox2.CornerRadius = 2;
            this.sboComboBox2.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboComboBox2.DropDownHight = 100;
            this.sboComboBox2.EnabledColor = System.Drawing.Color.White;
            this.sboComboBox2.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboComboBox2.Location = new System.Drawing.Point(525, 41);
            this.sboComboBox2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboComboBox2.Multiline = false;
            this.sboComboBox2.Name = "sboComboBox2";
            this.sboComboBox2.SelectedIndex = -1;
            this.sboComboBox2.SelectedItem = null;
            this.sboComboBox2.Size = new System.Drawing.Size(50, 14);
            this.sboComboBox2.TabIndex = 54;
            this.sboComboBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboComboBox2.TextBoxReadOnly = true;
            this.sboComboBox2.TextValue = "";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(754, 41);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(63, 12);
            this.label58.TabIndex = 53;
            this.label58.Text = "Disposal Cost";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(599, 41);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(82, 12);
            this.label59.TabIndex = 52;
            this.label59.Text = "Disposal Unit Cost";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(493, 41);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(28, 12);
            this.label60.TabIndex = 51;
            this.label60.Text = "UOM";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(367, 41);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(58, 12);
            this.label61.TabIndex = 50;
            this.label61.Text = "Disposal Qty";
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(825, 56);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(88, 16);
            this.checkBox4.TabIndex = 47;
            this.checkBox4.Text = "Purchase Order";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // sboTextBox58
            // 
            this.sboTextBox58.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox58.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox58.CornerRadius = 2;
            this.sboTextBox58.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox58.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox58.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox58.Location = new System.Drawing.Point(825, 41);
            this.sboTextBox58.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox58.Multiline = true;
            this.sboTextBox58.Name = "sboTextBox58";
            this.sboTextBox58.ReadOnly = false;
            this.sboTextBox58.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox58.TabIndex = 46;
            this.sboTextBox58.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox58.TextValue = "";
            // 
            // sboTextBox61
            // 
            this.sboTextBox61.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox61.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox61.CornerRadius = 2;
            this.sboTextBox61.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox61.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox61.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox61.Location = new System.Drawing.Point(688, 41);
            this.sboTextBox61.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox61.Multiline = false;
            this.sboTextBox61.Name = "sboTextBox61";
            this.sboTextBox61.ReadOnly = false;
            this.sboTextBox61.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox61.TabIndex = 45;
            this.sboTextBox61.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox61.TextValue = "";
            // 
            // sboTextBox62
            // 
            this.sboTextBox62.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox62.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox62.CornerRadius = 2;
            this.sboTextBox62.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox62.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox62.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox62.Location = new System.Drawing.Point(430, 41);
            this.sboTextBox62.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox62.Multiline = false;
            this.sboTextBox62.Name = "sboTextBox62";
            this.sboTextBox62.ReadOnly = false;
            this.sboTextBox62.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox62.TabIndex = 44;
            this.sboTextBox62.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox62.TextValue = "";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(131, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(86, 12);
            this.label18.TabIndex = 31;
            this.label18.Text = "Disp. Site BP Name";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(138, 56);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 12);
            this.label21.TabIndex = 30;
            this.label21.Text = "Disp. Site Ref No.";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(132, 41);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(85, 12);
            this.label34.TabIndex = 29;
            this.label34.Text = "Disp. Site Site Addr";
            this.label34.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(134, 11);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(83, 12);
            this.label35.TabIndex = 28;
            this.label35.Text = "Disp. Site BP Code";
            this.label35.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button8.Location = new System.Drawing.Point(25, 30);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 23;
            this.button8.Text = "Price List";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // sboTextBox46
            // 
            this.sboTextBox46.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox46.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox46.CornerRadius = 2;
            this.sboTextBox46.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox46.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox46.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox46.Location = new System.Drawing.Point(228, 56);
            this.sboTextBox46.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox46.Multiline = true;
            this.sboTextBox46.Name = "sboTextBox46";
            this.sboTextBox46.ReadOnly = false;
            this.sboTextBox46.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox46.TabIndex = 22;
            this.sboTextBox46.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox46.TextValue = "";
            // 
            // sboTextBox47
            // 
            this.sboTextBox47.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox47.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox47.CornerRadius = 2;
            this.sboTextBox47.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox47.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox47.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox47.Location = new System.Drawing.Point(228, 41);
            this.sboTextBox47.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox47.Multiline = true;
            this.sboTextBox47.Name = "sboTextBox47";
            this.sboTextBox47.ReadOnly = false;
            this.sboTextBox47.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox47.TabIndex = 21;
            this.sboTextBox47.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox47.TextValue = "";
            // 
            // sboTextBox48
            // 
            this.sboTextBox48.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox48.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox48.CornerRadius = 2;
            this.sboTextBox48.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox48.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox48.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox48.Location = new System.Drawing.Point(228, 26);
            this.sboTextBox48.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox48.Multiline = true;
            this.sboTextBox48.Name = "sboTextBox48";
            this.sboTextBox48.ReadOnly = false;
            this.sboTextBox48.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox48.TabIndex = 20;
            this.sboTextBox48.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox48.TextValue = "";
            // 
            // sboTextBox49
            // 
            this.sboTextBox49.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox49.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox49.CornerRadius = 2;
            this.sboTextBox49.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox49.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox49.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox49.Location = new System.Drawing.Point(228, 11);
            this.sboTextBox49.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox49.Multiline = true;
            this.sboTextBox49.Name = "sboTextBox49";
            this.sboTextBox49.ReadOnly = false;
            this.sboTextBox49.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox49.TabIndex = 19;
            this.sboTextBox49.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox49.TextValue = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.sboComboBox1);
            this.groupBox2.Controls.Add(this.label57);
            this.groupBox2.Controls.Add(this.label56);
            this.groupBox2.Controls.Add(this.label55);
            this.groupBox2.Controls.Add(this.label54);
            this.groupBox2.Controls.Add(this.label53);
            this.groupBox2.Controls.Add(this.label52);
            this.groupBox2.Controls.Add(this.checkBox3);
            this.groupBox2.Controls.Add(this.sboTextBox60);
            this.groupBox2.Controls.Add(this.sboTextBox59);
            this.groupBox2.Controls.Add(this.sboTextBox57);
            this.groupBox2.Controls.Add(this.sboTextBox56);
            this.groupBox2.Controls.Add(this.sboTextBox53);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.button7);
            this.groupBox2.Controls.Add(this.sboTextBox35);
            this.groupBox2.Controls.Add(this.sboTextBox43);
            this.groupBox2.Controls.Add(this.sboTextBox44);
            this.groupBox2.Controls.Add(this.sboTextBox45);
            this.groupBox2.Location = new System.Drawing.Point(3, 159);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(932, 77);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Purchase Cost";
            // 
            // sboComboBox1
            // 
            this.sboComboBox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboComboBox1.CornerRadius = 2;
            this.sboComboBox1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboComboBox1.DropDownHight = 100;
            this.sboComboBox1.EnabledColor = System.Drawing.Color.White;
            this.sboComboBox1.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboComboBox1.Location = new System.Drawing.Point(525, 41);
            this.sboComboBox1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboComboBox1.Multiline = false;
            this.sboComboBox1.Name = "sboComboBox1";
            this.sboComboBox1.SelectedIndex = -1;
            this.sboComboBox1.SelectedItem = null;
            this.sboComboBox1.Size = new System.Drawing.Size(50, 14);
            this.sboComboBox1.TabIndex = 41;
            this.sboComboBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboComboBox1.TextBoxReadOnly = true;
            this.sboComboBox1.TextValue = "";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(754, 41);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(66, 12);
            this.label57.TabIndex = 40;
            this.label57.Text = "Purchase Cost";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(596, 41);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(85, 12);
            this.label56.TabIndex = 39;
            this.label56.Text = "Purchase Unit Cost";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(493, 41);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(28, 12);
            this.label55.TabIndex = 38;
            this.label55.Text = "UOM";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(367, 41);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(58, 12);
            this.label54.TabIndex = 37;
            this.label54.Text = "Disposal Qty";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(376, 26);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(49, 12);
            this.label53.TabIndex = 36;
            this.label53.Text = "Actual Qty";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(390, 11);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(35, 12);
            this.label52.TabIndex = 35;
            this.label52.Text = "AUOM";
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(825, 56);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(88, 16);
            this.checkBox3.TabIndex = 34;
            this.checkBox3.Text = "Purchase Order";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // sboTextBox60
            // 
            this.sboTextBox60.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox60.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox60.CornerRadius = 2;
            this.sboTextBox60.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox60.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox60.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox60.Location = new System.Drawing.Point(825, 41);
            this.sboTextBox60.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox60.Multiline = true;
            this.sboTextBox60.Name = "sboTextBox60";
            this.sboTextBox60.ReadOnly = false;
            this.sboTextBox60.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox60.TabIndex = 33;
            this.sboTextBox60.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox60.TextValue = "";
            // 
            // sboTextBox59
            // 
            this.sboTextBox59.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox59.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox59.CornerRadius = 2;
            this.sboTextBox59.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox59.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox59.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox59.Location = new System.Drawing.Point(688, 41);
            this.sboTextBox59.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox59.Multiline = false;
            this.sboTextBox59.Name = "sboTextBox59";
            this.sboTextBox59.ReadOnly = false;
            this.sboTextBox59.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox59.TabIndex = 32;
            this.sboTextBox59.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox59.TextValue = "";
            // 
            // sboTextBox57
            // 
            this.sboTextBox57.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox57.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox57.CornerRadius = 2;
            this.sboTextBox57.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox57.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox57.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox57.Location = new System.Drawing.Point(430, 41);
            this.sboTextBox57.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox57.Multiline = false;
            this.sboTextBox57.Name = "sboTextBox57";
            this.sboTextBox57.ReadOnly = false;
            this.sboTextBox57.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox57.TabIndex = 30;
            this.sboTextBox57.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox57.TextValue = "";
            // 
            // sboTextBox56
            // 
            this.sboTextBox56.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox56.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox56.CornerRadius = 2;
            this.sboTextBox56.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox56.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox56.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox56.Location = new System.Drawing.Point(430, 26);
            this.sboTextBox56.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox56.Multiline = false;
            this.sboTextBox56.Name = "sboTextBox56";
            this.sboTextBox56.ReadOnly = false;
            this.sboTextBox56.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox56.TabIndex = 29;
            this.sboTextBox56.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox56.TextValue = "";
            // 
            // sboTextBox53
            // 
            this.sboTextBox53.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox53.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox53.CornerRadius = 2;
            this.sboTextBox53.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox53.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox53.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox53.Location = new System.Drawing.Point(430, 11);
            this.sboTextBox53.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox53.Multiline = false;
            this.sboTextBox53.Name = "sboTextBox53";
            this.sboTextBox53.ReadOnly = false;
            this.sboTextBox53.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox53.TabIndex = 28;
            this.sboTextBox53.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox53.TextValue = "";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(132, 26);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 12);
            this.label17.TabIndex = 27;
            this.label17.Text = "Purchase BP Name";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(139, 56);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 12);
            this.label14.TabIndex = 26;
            this.label14.Text = "Purchase Ref No.";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(133, 41);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 12);
            this.label12.TabIndex = 25;
            this.label12.Text = "Purchase Site Addr";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(135, 11);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 12);
            this.label10.TabIndex = 24;
            this.label10.Text = "Purchase BP Code";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button7.Location = new System.Drawing.Point(25, 30);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 23;
            this.button7.Text = "Price List";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // sboTextBox35
            // 
            this.sboTextBox35.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox35.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox35.CornerRadius = 2;
            this.sboTextBox35.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox35.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox35.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox35.Location = new System.Drawing.Point(228, 56);
            this.sboTextBox35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox35.Multiline = true;
            this.sboTextBox35.Name = "sboTextBox35";
            this.sboTextBox35.ReadOnly = false;
            this.sboTextBox35.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox35.TabIndex = 22;
            this.sboTextBox35.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox35.TextValue = "";
            // 
            // sboTextBox43
            // 
            this.sboTextBox43.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox43.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox43.CornerRadius = 2;
            this.sboTextBox43.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox43.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox43.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox43.Location = new System.Drawing.Point(228, 41);
            this.sboTextBox43.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox43.Multiline = true;
            this.sboTextBox43.Name = "sboTextBox43";
            this.sboTextBox43.ReadOnly = false;
            this.sboTextBox43.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox43.TabIndex = 21;
            this.sboTextBox43.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox43.TextValue = "";
            // 
            // sboTextBox44
            // 
            this.sboTextBox44.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox44.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox44.CornerRadius = 2;
            this.sboTextBox44.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox44.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox44.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox44.Location = new System.Drawing.Point(228, 26);
            this.sboTextBox44.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox44.Multiline = true;
            this.sboTextBox44.Name = "sboTextBox44";
            this.sboTextBox44.ReadOnly = false;
            this.sboTextBox44.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox44.TabIndex = 20;
            this.sboTextBox44.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox44.TextValue = "";
            // 
            // sboTextBox45
            // 
            this.sboTextBox45.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox45.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox45.CornerRadius = 2;
            this.sboTextBox45.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox45.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox45.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox45.Location = new System.Drawing.Point(228, 11);
            this.sboTextBox45.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox45.Multiline = true;
            this.sboTextBox45.Name = "sboTextBox45";
            this.sboTextBox45.ReadOnly = false;
            this.sboTextBox45.Size = new System.Drawing.Size(120, 14);
            this.sboTextBox45.TabIndex = 19;
            this.sboTextBox45.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox45.TextValue = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label98);
            this.groupBox1.Controls.Add(this.label97);
            this.groupBox1.Controls.Add(this.label96);
            this.groupBox1.Controls.Add(this.label95);
            this.groupBox1.Controls.Add(this.label94);
            this.groupBox1.Controls.Add(this.label93);
            this.groupBox1.Controls.Add(this.label92);
            this.groupBox1.Controls.Add(this.label91);
            this.groupBox1.Controls.Add(this.label90);
            this.groupBox1.Controls.Add(this.label89);
            this.groupBox1.Controls.Add(this.sboTextBox89);
            this.groupBox1.Controls.Add(this.sboTextBox88);
            this.groupBox1.Controls.Add(this.sboTextBox87);
            this.groupBox1.Controls.Add(this.sboTextBox86);
            this.groupBox1.Controls.Add(this.sboTextBox85);
            this.groupBox1.Controls.Add(this.sboTextBox84);
            this.groupBox1.Controls.Add(this.sboTextBox83);
            this.groupBox1.Controls.Add(this.sboTextBox82);
            this.groupBox1.Controls.Add(this.sboTextBox81);
            this.groupBox1.Controls.Add(this.sboComboBox7);
            this.groupBox1.Controls.Add(this.checkBox11);
            this.groupBox1.Controls.Add(this.checkBox10);
            this.groupBox1.Controls.Add(this.checkBox9);
            this.groupBox1.Controls.Add(this.checkBox8);
            this.groupBox1.Controls.Add(this.checkBox7);
            this.groupBox1.Controls.Add(this.label51);
            this.groupBox1.Controls.Add(this.label50);
            this.groupBox1.Controls.Add(this.label49);
            this.groupBox1.Controls.Add(this.label48);
            this.groupBox1.Controls.Add(this.label47);
            this.groupBox1.Controls.Add(this.label46);
            this.groupBox1.Controls.Add(this.label45);
            this.groupBox1.Controls.Add(this.label44);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.sboTextBox34);
            this.groupBox1.Controls.Add(this.sboTextBox36);
            this.groupBox1.Controls.Add(this.sboTextBox37);
            this.groupBox1.Controls.Add(this.sboTextBox38);
            this.groupBox1.Controls.Add(this.sboTextBox39);
            this.groupBox1.Controls.Add(this.sboTextBox40);
            this.groupBox1.Controls.Add(this.sboTextBox41);
            this.groupBox1.Controls.Add(this.sboTextBox42);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(932, 157);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Charges";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(758, 138);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(62, 12);
            this.label51.TabIndex = 29;
            this.label51.Text = "Total Charges";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(800, 118);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(20, 12);
            this.label50.TabIndex = 28;
            this.label50.Text = "Tax";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(758, 103);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(64, 12);
            this.label49.TabIndex = 27;
            this.label49.Text = "Order Subtotal";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(780, 88);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(42, 12);
            this.label48.TabIndex = 26;
            this.label48.Text = "Discount";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(747, 73);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(75, 12);
            this.label47.TabIndex = 25;
            this.label47.Text = "Total Before Disc";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(752, 53);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(68, 12);
            this.label46.TabIndex = 24;
            this.label46.Text = "Additional Total";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(751, 38);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(71, 12);
            this.label45.TabIndex = 23;
            this.label45.Text = "Haulage Charge";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(747, 23);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(73, 12);
            this.label44.TabIndex = 22;
            this.label44.Text = "Disposal Charge";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button6.Location = new System.Drawing.Point(25, 65);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 21;
            this.button6.Text = "Price List";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // sboTextBox34
            // 
            this.sboTextBox34.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox34.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox34.CornerRadius = 2;
            this.sboTextBox34.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox34.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox34.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox34.Location = new System.Drawing.Point(825, 138);
            this.sboTextBox34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox34.Multiline = true;
            this.sboTextBox34.Name = "sboTextBox34";
            this.sboTextBox34.ReadOnly = false;
            this.sboTextBox34.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox34.TabIndex = 20;
            this.sboTextBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox34.TextValue = "";
            // 
            // sboTextBox36
            // 
            this.sboTextBox36.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox36.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox36.CornerRadius = 2;
            this.sboTextBox36.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox36.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox36.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox36.Location = new System.Drawing.Point(825, 118);
            this.sboTextBox36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox36.Multiline = true;
            this.sboTextBox36.Name = "sboTextBox36";
            this.sboTextBox36.ReadOnly = false;
            this.sboTextBox36.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox36.TabIndex = 18;
            this.sboTextBox36.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox36.TextValue = "";
            // 
            // sboTextBox37
            // 
            this.sboTextBox37.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox37.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox37.CornerRadius = 2;
            this.sboTextBox37.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox37.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox37.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox37.Location = new System.Drawing.Point(825, 103);
            this.sboTextBox37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox37.Multiline = true;
            this.sboTextBox37.Name = "sboTextBox37";
            this.sboTextBox37.ReadOnly = false;
            this.sboTextBox37.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox37.TabIndex = 17;
            this.sboTextBox37.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox37.TextValue = "";
            // 
            // sboTextBox38
            // 
            this.sboTextBox38.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox38.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox38.CornerRadius = 2;
            this.sboTextBox38.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox38.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox38.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox38.Location = new System.Drawing.Point(825, 88);
            this.sboTextBox38.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox38.Multiline = true;
            this.sboTextBox38.Name = "sboTextBox38";
            this.sboTextBox38.ReadOnly = false;
            this.sboTextBox38.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox38.TabIndex = 16;
            this.sboTextBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox38.TextValue = "";
            // 
            // sboTextBox39
            // 
            this.sboTextBox39.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox39.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox39.CornerRadius = 2;
            this.sboTextBox39.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox39.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox39.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox39.Location = new System.Drawing.Point(825, 73);
            this.sboTextBox39.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox39.Multiline = true;
            this.sboTextBox39.Name = "sboTextBox39";
            this.sboTextBox39.ReadOnly = false;
            this.sboTextBox39.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox39.TabIndex = 15;
            this.sboTextBox39.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox39.TextValue = "";
            // 
            // sboTextBox40
            // 
            this.sboTextBox40.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox40.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox40.CornerRadius = 2;
            this.sboTextBox40.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox40.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox40.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox40.Location = new System.Drawing.Point(825, 53);
            this.sboTextBox40.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox40.Multiline = true;
            this.sboTextBox40.Name = "sboTextBox40";
            this.sboTextBox40.ReadOnly = false;
            this.sboTextBox40.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox40.TabIndex = 14;
            this.sboTextBox40.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox40.TextValue = "";
            // 
            // sboTextBox41
            // 
            this.sboTextBox41.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox41.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox41.CornerRadius = 2;
            this.sboTextBox41.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox41.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox41.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox41.Location = new System.Drawing.Point(825, 38);
            this.sboTextBox41.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox41.Multiline = true;
            this.sboTextBox41.Name = "sboTextBox41";
            this.sboTextBox41.ReadOnly = false;
            this.sboTextBox41.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox41.TabIndex = 13;
            this.sboTextBox41.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox41.TextValue = "";
            // 
            // sboTextBox42
            // 
            this.sboTextBox42.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox42.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox42.CornerRadius = 2;
            this.sboTextBox42.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox42.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox42.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox42.Location = new System.Drawing.Point(825, 23);
            this.sboTextBox42.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox42.Multiline = true;
            this.sboTextBox42.Name = "sboTextBox42";
            this.sboTextBox42.ReadOnly = false;
            this.sboTextBox42.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox42.TabIndex = 12;
            this.sboTextBox42.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox42.TextValue = "";
            // 
            // IDH_TABCOM
            // 
            this.IDH_TABCOM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(241)))), ((int)(((byte)(246)))));
            this.IDH_TABCOM.Controls.Add(this.sboTextBox79);
            this.IDH_TABCOM.Controls.Add(this.sboTextBox80);
            this.IDH_TABCOM.Controls.Add(this.label88);
            this.IDH_TABCOM.Controls.Add(this.label87);
            this.IDH_TABCOM.Controls.Add(this.label86);
            this.IDH_TABCOM.Controls.Add(this.label85);
            this.IDH_TABCOM.Controls.Add(this.sboTextBox78);
            this.IDH_TABCOM.Controls.Add(this.sboTextBox77);
            this.IDH_TABCOM.Location = new System.Drawing.Point(4, 21);
            this.IDH_TABCOM.Name = "IDH_TABCOM";
            this.IDH_TABCOM.Padding = new System.Windows.Forms.Padding(3);
            this.IDH_TABCOM.Size = new System.Drawing.Size(940, 528);
            this.IDH_TABCOM.TabIndex = 1;
            this.IDH_TABCOM.Text = "Comments";
            // 
            // IDH_TABADD
            // 
            this.IDH_TABADD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(241)))), ((int)(((byte)(246)))));
            this.IDH_TABADD.Controls.Add(this.label79);
            this.IDH_TABADD.Controls.Add(this.label80);
            this.IDH_TABADD.Controls.Add(this.sboTextBox73);
            this.IDH_TABADD.Controls.Add(this.sboTextBox74);
            this.IDH_TABADD.Controls.Add(this.oAddGrid);
            this.IDH_TABADD.Controls.Add(this.label78);
            this.IDH_TABADD.Location = new System.Drawing.Point(4, 21);
            this.IDH_TABADD.Name = "IDH_TABADD";
            this.IDH_TABADD.Size = new System.Drawing.Size(940, 528);
            this.IDH_TABADD.TabIndex = 2;
            this.IDH_TABADD.Text = "Additional";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(727, 508);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(95, 12);
            this.label79.TabIndex = 35;
            this.label79.Text = "Total Additional Costs";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(727, 493);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(100, 12);
            this.label80.TabIndex = 34;
            this.label80.Text = "Total Additional Charge";
            // 
            // sboTextBox73
            // 
            this.sboTextBox73.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox73.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox73.CornerRadius = 2;
            this.sboTextBox73.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox73.Enabled = false;
            this.sboTextBox73.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox73.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox73.Location = new System.Drawing.Point(831, 508);
            this.sboTextBox73.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox73.Multiline = true;
            this.sboTextBox73.Name = "sboTextBox73";
            this.sboTextBox73.ReadOnly = false;
            this.sboTextBox73.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox73.TabIndex = 33;
            this.sboTextBox73.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox73.TextValue = "";
            // 
            // sboTextBox74
            // 
            this.sboTextBox74.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox74.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox74.CornerRadius = 2;
            this.sboTextBox74.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox74.Enabled = false;
            this.sboTextBox74.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox74.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox74.Location = new System.Drawing.Point(831, 493);
            this.sboTextBox74.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox74.Multiline = true;
            this.sboTextBox74.Name = "sboTextBox74";
            this.sboTextBox74.ReadOnly = false;
            this.sboTextBox74.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox74.TabIndex = 32;
            this.sboTextBox74.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox74.TextValue = "";
            // 
            // oAddGrid
            // 
            this.oAddGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.oAddGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oAddGrid.GrayReadOnly = false;
            this.oAddGrid.Location = new System.Drawing.Point(8, 29);
            this.oAddGrid.Name = "oAddGrid";
            this.oAddGrid.Size = new System.Drawing.Size(923, 457);
            this.oAddGrid.TabIndex = 1;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(8, 10);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(283, 12);
            this.label78.TabIndex = 0;
            this.label78.Text = "Add any Additional Items for the order in the grid below";
            // 
            // IDH_TABACT
            // 
            this.IDH_TABACT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(241)))), ((int)(((byte)(246)))));
            this.IDH_TABACT.Controls.Add(this.wR1Grid2);
            this.IDH_TABACT.Controls.Add(this.label81);
            this.IDH_TABACT.Location = new System.Drawing.Point(4, 21);
            this.IDH_TABACT.Name = "IDH_TABACT";
            this.IDH_TABACT.Size = new System.Drawing.Size(940, 528);
            this.IDH_TABACT.TabIndex = 3;
            this.IDH_TABACT.Text = "Activities";
            // 
            // wR1Grid2
            // 
            this.wR1Grid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.wR1Grid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.wR1Grid2.GrayReadOnly = false;
            this.wR1Grid2.Location = new System.Drawing.Point(8, 29);
            this.wR1Grid2.Name = "wR1Grid2";
            this.wR1Grid2.Size = new System.Drawing.Size(923, 457);
            this.wR1Grid2.TabIndex = 3;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(8, 10);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(123, 12);
            this.label81.TabIndex = 2;
            this.label81.Text = "Activities for this order";
            // 
            // IDH_TABADM
            // 
            this.IDH_TABADM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(241)))), ((int)(((byte)(246)))));
            this.IDH_TABADM.Controls.Add(this.label131);
            this.IDH_TABADM.Controls.Add(this.label130);
            this.IDH_TABADM.Controls.Add(this.label129);
            this.IDH_TABADM.Controls.Add(this.label128);
            this.IDH_TABADM.Controls.Add(this.label127);
            this.IDH_TABADM.Controls.Add(this.label126);
            this.IDH_TABADM.Controls.Add(this.label125);
            this.IDH_TABADM.Controls.Add(this.label124);
            this.IDH_TABADM.Controls.Add(this.label123);
            this.IDH_TABADM.Controls.Add(this.label122);
            this.IDH_TABADM.Controls.Add(this.label121);
            this.IDH_TABADM.Controls.Add(this.label120);
            this.IDH_TABADM.Controls.Add(this.label119);
            this.IDH_TABADM.Controls.Add(this.label118);
            this.IDH_TABADM.Controls.Add(this.label117);
            this.IDH_TABADM.Controls.Add(this.label116);
            this.IDH_TABADM.Controls.Add(this.label115);
            this.IDH_TABADM.Controls.Add(this.label114);
            this.IDH_TABADM.Controls.Add(this.label113);
            this.IDH_TABADM.Controls.Add(this.label112);
            this.IDH_TABADM.Controls.Add(this.label111);
            this.IDH_TABADM.Controls.Add(this.label110);
            this.IDH_TABADM.Controls.Add(this.label109);
            this.IDH_TABADM.Controls.Add(this.label108);
            this.IDH_TABADM.Controls.Add(this.label107);
            this.IDH_TABADM.Controls.Add(this.label106);
            this.IDH_TABADM.Controls.Add(this.label105);
            this.IDH_TABADM.Controls.Add(this.label104);
            this.IDH_TABADM.Controls.Add(this.sboTextBox130);
            this.IDH_TABADM.Controls.Add(this.sboTextBox129);
            this.IDH_TABADM.Controls.Add(this.sboTextBox128);
            this.IDH_TABADM.Controls.Add(this.sboTextBox127);
            this.IDH_TABADM.Controls.Add(this.sboTextBox126);
            this.IDH_TABADM.Controls.Add(this.sboTextBox125);
            this.IDH_TABADM.Controls.Add(this.sboTextBox124);
            this.IDH_TABADM.Controls.Add(this.sboTextBox123);
            this.IDH_TABADM.Controls.Add(this.sboTextBox122);
            this.IDH_TABADM.Controls.Add(this.sboTextBox121);
            this.IDH_TABADM.Controls.Add(this.sboTextBox120);
            this.IDH_TABADM.Controls.Add(this.sboTextBox119);
            this.IDH_TABADM.Controls.Add(this.sboTextBox118);
            this.IDH_TABADM.Controls.Add(this.sboTextBox117);
            this.IDH_TABADM.Controls.Add(this.sboTextBox116);
            this.IDH_TABADM.Controls.Add(this.sboTextBox114);
            this.IDH_TABADM.Controls.Add(this.sboTextBox115);
            this.IDH_TABADM.Controls.Add(this.sboTextBox112);
            this.IDH_TABADM.Controls.Add(this.sboTextBox113);
            this.IDH_TABADM.Controls.Add(this.sboTextBox109);
            this.IDH_TABADM.Controls.Add(this.sboTextBox110);
            this.IDH_TABADM.Controls.Add(this.sboTextBox111);
            this.IDH_TABADM.Controls.Add(this.sboTextBox106);
            this.IDH_TABADM.Controls.Add(this.sboTextBox107);
            this.IDH_TABADM.Controls.Add(this.sboTextBox108);
            this.IDH_TABADM.Controls.Add(this.sboTextBox103);
            this.IDH_TABADM.Controls.Add(this.sboTextBox104);
            this.IDH_TABADM.Controls.Add(this.sboTextBox105);
            this.IDH_TABADM.Controls.Add(this.sboTextBox100);
            this.IDH_TABADM.Controls.Add(this.sboTextBox101);
            this.IDH_TABADM.Controls.Add(this.sboTextBox102);
            this.IDH_TABADM.Controls.Add(this.sboTextBox99);
            this.IDH_TABADM.Controls.Add(this.sboTextBox98);
            this.IDH_TABADM.Controls.Add(this.sboTextBox97);
            this.IDH_TABADM.Controls.Add(this.sboTextBox96);
            this.IDH_TABADM.Controls.Add(this.sboTextBox95);
            this.IDH_TABADM.Controls.Add(this.sboTextBox94);
            this.IDH_TABADM.Controls.Add(this.sboTextBox93);
            this.IDH_TABADM.Controls.Add(this.sboTextBox92);
            this.IDH_TABADM.Controls.Add(this.sboTextBox91);
            this.IDH_TABADM.Controls.Add(this.sboTextBox90);
            this.IDH_TABADM.Controls.Add(this.label103);
            this.IDH_TABADM.Controls.Add(this.label102);
            this.IDH_TABADM.Controls.Add(this.label101);
            this.IDH_TABADM.Controls.Add(this.label100);
            this.IDH_TABADM.Controls.Add(this.label99);
            this.IDH_TABADM.Controls.Add(this.checkBox15);
            this.IDH_TABADM.Controls.Add(this.checkBox14);
            this.IDH_TABADM.Controls.Add(this.checkBox13);
            this.IDH_TABADM.Controls.Add(this.checkBox12);
            this.IDH_TABADM.Location = new System.Drawing.Point(4, 21);
            this.IDH_TABADM.Name = "IDH_TABADM";
            this.IDH_TABADM.Size = new System.Drawing.Size(940, 528);
            this.IDH_TABADM.TabIndex = 4;
            this.IDH_TABADM.Text = "Admin";
            // 
            // IDH_TABEXP
            // 
            this.IDH_TABEXP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(241)))), ((int)(((byte)(246)))));
            this.IDH_TABEXP.Controls.Add(this.label160);
            this.IDH_TABEXP.Controls.Add(this.label159);
            this.IDH_TABEXP.Controls.Add(this.label158);
            this.IDH_TABEXP.Controls.Add(this.label157);
            this.IDH_TABEXP.Controls.Add(this.label156);
            this.IDH_TABEXP.Controls.Add(this.label155);
            this.IDH_TABEXP.Controls.Add(this.label154);
            this.IDH_TABEXP.Controls.Add(this.label153);
            this.IDH_TABEXP.Controls.Add(this.label152);
            this.IDH_TABEXP.Controls.Add(this.label151);
            this.IDH_TABEXP.Controls.Add(this.label150);
            this.IDH_TABEXP.Controls.Add(this.label149);
            this.IDH_TABEXP.Controls.Add(this.label148);
            this.IDH_TABEXP.Controls.Add(this.label147);
            this.IDH_TABEXP.Controls.Add(this.label146);
            this.IDH_TABEXP.Controls.Add(this.label145);
            this.IDH_TABEXP.Controls.Add(this.label144);
            this.IDH_TABEXP.Controls.Add(this.label143);
            this.IDH_TABEXP.Controls.Add(this.label142);
            this.IDH_TABEXP.Controls.Add(this.label141);
            this.IDH_TABEXP.Controls.Add(this.label140);
            this.IDH_TABEXP.Controls.Add(this.label139);
            this.IDH_TABEXP.Controls.Add(this.label138);
            this.IDH_TABEXP.Controls.Add(this.label137);
            this.IDH_TABEXP.Controls.Add(this.label136);
            this.IDH_TABEXP.Controls.Add(this.label135);
            this.IDH_TABEXP.Controls.Add(this.label134);
            this.IDH_TABEXP.Controls.Add(this.label133);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox158);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox157);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox156);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox155);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox152);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox153);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox154);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox151);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox150);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox149);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox148);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox147);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox146);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox145);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox144);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox143);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox142);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox141);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox140);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox139);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox138);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox137);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox136);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox135);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox134);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox133);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox132);
            this.IDH_TABEXP.Controls.Add(this.sboTextBox131);
            this.IDH_TABEXP.Controls.Add(this.label132);
            this.IDH_TABEXP.Location = new System.Drawing.Point(4, 21);
            this.IDH_TABEXP.Name = "IDH_TABEXP";
            this.IDH_TABEXP.Size = new System.Drawing.Size(940, 528);
            this.IDH_TABEXP.TabIndex = 5;
            this.IDH_TABEXP.Text = "Exports";
            // 
            // IDH_TABWEI
            // 
            this.IDH_TABWEI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(241)))), ((int)(((byte)(246)))));
            this.IDH_TABWEI.Controls.Add(this.label172);
            this.IDH_TABWEI.Controls.Add(this.label171);
            this.IDH_TABWEI.Controls.Add(this.label170);
            this.IDH_TABWEI.Controls.Add(this.label169);
            this.IDH_TABWEI.Controls.Add(this.label168);
            this.IDH_TABWEI.Controls.Add(this.label167);
            this.IDH_TABWEI.Controls.Add(this.label166);
            this.IDH_TABWEI.Controls.Add(this.label165);
            this.IDH_TABWEI.Controls.Add(this.label164);
            this.IDH_TABWEI.Controls.Add(this.label163);
            this.IDH_TABWEI.Controls.Add(this.label162);
            this.IDH_TABWEI.Controls.Add(this.label161);
            this.IDH_TABWEI.Controls.Add(this.pictureBox1);
            this.IDH_TABWEI.Controls.Add(this.sboComboBox8);
            this.IDH_TABWEI.Controls.Add(this.sboTextBox170);
            this.IDH_TABWEI.Controls.Add(this.sboTextBox169);
            this.IDH_TABWEI.Controls.Add(this.sboTextBox168);
            this.IDH_TABWEI.Controls.Add(this.sboTextBox167);
            this.IDH_TABWEI.Controls.Add(this.sboTextBox166);
            this.IDH_TABWEI.Controls.Add(this.sboTextBox165);
            this.IDH_TABWEI.Controls.Add(this.sboTextBox164);
            this.IDH_TABWEI.Controls.Add(this.sboTextBox163);
            this.IDH_TABWEI.Controls.Add(this.sboTextBox162);
            this.IDH_TABWEI.Controls.Add(this.sboTextBox161);
            this.IDH_TABWEI.Controls.Add(this.sboTextBox160);
            this.IDH_TABWEI.Controls.Add(this.checkBox23);
            this.IDH_TABWEI.Controls.Add(this.checkBox22);
            this.IDH_TABWEI.Controls.Add(this.checkBox21);
            this.IDH_TABWEI.Controls.Add(this.checkBox20);
            this.IDH_TABWEI.Controls.Add(this.checkBox19);
            this.IDH_TABWEI.Controls.Add(this.checkBox18);
            this.IDH_TABWEI.Controls.Add(this.checkBox17);
            this.IDH_TABWEI.Controls.Add(this.checkBox16);
            this.IDH_TABWEI.Controls.Add(this.button20);
            this.IDH_TABWEI.Controls.Add(this.button19);
            this.IDH_TABWEI.Controls.Add(this.button18);
            this.IDH_TABWEI.Controls.Add(this.button17);
            this.IDH_TABWEI.Controls.Add(this.button16);
            this.IDH_TABWEI.Controls.Add(this.button15);
            this.IDH_TABWEI.Controls.Add(this.button14);
            this.IDH_TABWEI.Controls.Add(this.button13);
            this.IDH_TABWEI.Controls.Add(this.button12);
            this.IDH_TABWEI.Controls.Add(this.button11);
            this.IDH_TABWEI.Controls.Add(this.radioButton2);
            this.IDH_TABWEI.Controls.Add(this.radioButton1);
            this.IDH_TABWEI.Location = new System.Drawing.Point(4, 21);
            this.IDH_TABWEI.Name = "IDH_TABWEI";
            this.IDH_TABWEI.Size = new System.Drawing.Size(940, 528);
            this.IDH_TABWEI.TabIndex = 6;
            this.IDH_TABWEI.Text = "Weighbridge";
            // 
            // IDH_TABDED
            // 
            this.IDH_TABDED.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(241)))), ((int)(((byte)(246)))));
            this.IDH_TABDED.Controls.Add(this.label82);
            this.IDH_TABDED.Controls.Add(this.label83);
            this.IDH_TABDED.Controls.Add(this.sboTextBox75);
            this.IDH_TABDED.Controls.Add(this.sboTextBox76);
            this.IDH_TABDED.Controls.Add(this.oDeductionsGrid);
            this.IDH_TABDED.Controls.Add(this.label84);
            this.IDH_TABDED.Location = new System.Drawing.Point(4, 21);
            this.IDH_TABDED.Name = "IDH_TABDED";
            this.IDH_TABDED.Size = new System.Drawing.Size(940, 528);
            this.IDH_TABDED.TabIndex = 7;
            this.IDH_TABDED.Text = "Deductions";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(727, 508);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(100, 12);
            this.label82.TabIndex = 41;
            this.label82.Text = "Total Value Deductions";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(727, 493);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(105, 12);
            this.label83.TabIndex = 40;
            this.label83.Text = "Total Weight Deductions";
            // 
            // sboTextBox75
            // 
            this.sboTextBox75.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox75.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox75.CornerRadius = 2;
            this.sboTextBox75.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox75.Enabled = false;
            this.sboTextBox75.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox75.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox75.Location = new System.Drawing.Point(831, 508);
            this.sboTextBox75.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox75.Multiline = true;
            this.sboTextBox75.Name = "sboTextBox75";
            this.sboTextBox75.ReadOnly = false;
            this.sboTextBox75.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox75.TabIndex = 39;
            this.sboTextBox75.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox75.TextValue = "";
            // 
            // sboTextBox76
            // 
            this.sboTextBox76.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox76.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox76.CornerRadius = 2;
            this.sboTextBox76.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox76.Enabled = false;
            this.sboTextBox76.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox76.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox76.Location = new System.Drawing.Point(831, 493);
            this.sboTextBox76.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox76.Multiline = true;
            this.sboTextBox76.Name = "sboTextBox76";
            this.sboTextBox76.ReadOnly = false;
            this.sboTextBox76.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox76.TabIndex = 38;
            this.sboTextBox76.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox76.TextValue = "";
            // 
            // oDeductionsGrid
            // 
            this.oDeductionsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.oDeductionsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oDeductionsGrid.GrayReadOnly = false;
            this.oDeductionsGrid.Location = new System.Drawing.Point(9, 29);
            this.oDeductionsGrid.Name = "oDeductionsGrid";
            this.oDeductionsGrid.Size = new System.Drawing.Size(923, 457);
            this.oDeductionsGrid.TabIndex = 37;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(9, 10);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(257, 12);
            this.label84.TabIndex = 36;
            this.label84.Text = "Add any Deductions for the order in the grid below";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(7, 759);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 23);
            this.button1.TabIndex = 67;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(89, 758);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(56, 23);
            this.button2.TabIndex = 68;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(696, 758);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(68, 23);
            this.button3.TabIndex = 69;
            this.button3.Text = "Activity";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(773, 758);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(73, 23);
            this.button4.TabIndex = 70;
            this.button4.Text = "Documents";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(865, 757);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(63, 23);
            this.button5.TabIndex = 71;
            this.button5.Text = "Print DOC";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(414, 64);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(63, 16);
            this.checkBox1.TabIndex = 72;
            this.checkBox1.Text = "Collected";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(539, 64);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(64, 16);
            this.checkBox2.TabIndex = 73;
            this.checkBox2.Text = "Delivered";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // sboComboBox5
            // 
            this.sboComboBox5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboComboBox5.CornerRadius = 2;
            this.sboComboBox5.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboComboBox5.DropDownHight = 100;
            this.sboComboBox5.EnabledColor = System.Drawing.Color.White;
            this.sboComboBox5.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboComboBox5.Location = new System.Drawing.Point(412, 5);
            this.sboComboBox5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboComboBox5.Multiline = true;
            this.sboComboBox5.Name = "sboComboBox5";
            this.sboComboBox5.SelectedIndex = -1;
            this.sboComboBox5.SelectedItem = null;
            this.sboComboBox5.Size = new System.Drawing.Size(200, 14);
            this.sboComboBox5.TabIndex = 74;
            this.sboComboBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboComboBox5.TextBoxReadOnly = true;
            this.sboComboBox5.TextValue = "";
            // 
            // sboComboBox6
            // 
            this.sboComboBox6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboComboBox6.CornerRadius = 2;
            this.sboComboBox6.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboComboBox6.DropDownHight = 100;
            this.sboComboBox6.Enabled = false;
            this.sboComboBox6.EnabledColor = System.Drawing.Color.White;
            this.sboComboBox6.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboComboBox6.Location = new System.Drawing.Point(733, 35);
            this.sboComboBox6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboComboBox6.Multiline = true;
            this.sboComboBox6.Name = "sboComboBox6";
            this.sboComboBox6.SelectedIndex = -1;
            this.sboComboBox6.SelectedItem = null;
            this.sboComboBox6.Size = new System.Drawing.Size(100, 14);
            this.sboComboBox6.TabIndex = 75;
            this.sboComboBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboComboBox6.TextBoxReadOnly = true;
            this.sboComboBox6.TextValue = "";
            // 
            // sboTextBox22
            // 
            this.sboTextBox22.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox22.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox22.CornerRadius = 2;
            this.sboTextBox22.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox22.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox22.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox22.Location = new System.Drawing.Point(872, 125);
            this.sboTextBox22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox22.Multiline = true;
            this.sboTextBox22.Name = "sboTextBox22";
            this.sboTextBox22.ReadOnly = false;
            this.sboTextBox22.Size = new System.Drawing.Size(60, 14);
            this.sboTextBox22.TabIndex = 80;
            this.sboTextBox22.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox22.TextValue = "";
            // 
            // sboTextBox31
            // 
            this.sboTextBox31.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox31.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox31.CornerRadius = 2;
            this.sboTextBox31.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox31.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox31.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox31.Location = new System.Drawing.Point(872, 110);
            this.sboTextBox31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox31.Multiline = true;
            this.sboTextBox31.Name = "sboTextBox31";
            this.sboTextBox31.ReadOnly = false;
            this.sboTextBox31.Size = new System.Drawing.Size(60, 14);
            this.sboTextBox31.TabIndex = 79;
            this.sboTextBox31.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox31.TextValue = "";
            // 
            // sboTextBox63
            // 
            this.sboTextBox63.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox63.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox63.CornerRadius = 2;
            this.sboTextBox63.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox63.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox63.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox63.Location = new System.Drawing.Point(872, 95);
            this.sboTextBox63.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox63.Multiline = true;
            this.sboTextBox63.Name = "sboTextBox63";
            this.sboTextBox63.ReadOnly = false;
            this.sboTextBox63.Size = new System.Drawing.Size(60, 14);
            this.sboTextBox63.TabIndex = 78;
            this.sboTextBox63.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox63.TextValue = "";
            // 
            // sboTextBox64
            // 
            this.sboTextBox64.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox64.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox64.CornerRadius = 2;
            this.sboTextBox64.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox64.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox64.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox64.Location = new System.Drawing.Point(872, 80);
            this.sboTextBox64.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox64.Multiline = true;
            this.sboTextBox64.Name = "sboTextBox64";
            this.sboTextBox64.ReadOnly = false;
            this.sboTextBox64.Size = new System.Drawing.Size(60, 14);
            this.sboTextBox64.TabIndex = 77;
            this.sboTextBox64.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox64.TextValue = "";
            // 
            // sboTextBox68
            // 
            this.sboTextBox68.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox68.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox68.CornerRadius = 2;
            this.sboTextBox68.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox68.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox68.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox68.Location = new System.Drawing.Point(872, 50);
            this.sboTextBox68.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox68.Multiline = true;
            this.sboTextBox68.Name = "sboTextBox68";
            this.sboTextBox68.ReadOnly = false;
            this.sboTextBox68.Size = new System.Drawing.Size(60, 14);
            this.sboTextBox68.TabIndex = 76;
            this.sboTextBox68.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox68.TextValue = "";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(840, 52);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(25, 12);
            this.label69.TabIndex = 81;
            this.label69.Text = "Time";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(840, 82);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(25, 12);
            this.label74.TabIndex = 82;
            this.label74.Text = "Time";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(840, 97);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(25, 12);
            this.label75.TabIndex = 83;
            this.label75.Text = "Time";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(840, 112);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(25, 12);
            this.label76.TabIndex = 84;
            this.label76.Text = "Time";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(840, 127);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(25, 12);
            this.label77.TabIndex = 85;
            this.label77.Text = "Time";
            // 
            // sboTextBox69
            // 
            this.sboTextBox69.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox69.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox69.CornerRadius = 2;
            this.sboTextBox69.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox69.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox69.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox69.Location = new System.Drawing.Point(872, 5);
            this.sboTextBox69.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox69.Multiline = true;
            this.sboTextBox69.Name = "sboTextBox69";
            this.sboTextBox69.ReadOnly = false;
            this.sboTextBox69.Size = new System.Drawing.Size(60, 14);
            this.sboTextBox69.TabIndex = 86;
            this.sboTextBox69.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox69.TextValue = "";
            // 
            // sboTextBox77
            // 
            this.sboTextBox77.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox77.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox77.CornerRadius = 2;
            this.sboTextBox77.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox77.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox77.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox77.Location = new System.Drawing.Point(166, 15);
            this.sboTextBox77.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox77.Multiline = true;
            this.sboTextBox77.Name = "sboTextBox77";
            this.sboTextBox77.ReadOnly = false;
            this.sboTextBox77.Size = new System.Drawing.Size(307, 211);
            this.sboTextBox77.TabIndex = 0;
            this.sboTextBox77.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox77.TextValue = "";
            // 
            // sboTextBox78
            // 
            this.sboTextBox78.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox78.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox78.CornerRadius = 2;
            this.sboTextBox78.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox78.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox78.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox78.Location = new System.Drawing.Point(617, 15);
            this.sboTextBox78.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox78.Multiline = true;
            this.sboTextBox78.Name = "sboTextBox78";
            this.sboTextBox78.ReadOnly = false;
            this.sboTextBox78.Size = new System.Drawing.Size(307, 211);
            this.sboTextBox78.TabIndex = 1;
            this.sboTextBox78.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox78.TextValue = "";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(30, 27);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(89, 12);
            this.label85.TabIndex = 2;
            this.label85.Text = "Comment for Tkt";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(512, 27);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(50, 12);
            this.label86.TabIndex = 3;
            this.label86.Text = "Remarks";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(30, 271);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(95, 12);
            this.label87.TabIndex = 4;
            this.label87.Text = "Internal Comment";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(512, 271);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(35, 12);
            this.label88.TabIndex = 5;
            this.label88.Text = "Notes";
            // 
            // sboTextBox79
            // 
            this.sboTextBox79.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox79.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox79.CornerRadius = 2;
            this.sboTextBox79.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox79.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox79.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox79.Location = new System.Drawing.Point(617, 255);
            this.sboTextBox79.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox79.Multiline = true;
            this.sboTextBox79.Name = "sboTextBox79";
            this.sboTextBox79.ReadOnly = false;
            this.sboTextBox79.Size = new System.Drawing.Size(307, 211);
            this.sboTextBox79.TabIndex = 7;
            this.sboTextBox79.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox79.TextValue = "";
            // 
            // sboTextBox80
            // 
            this.sboTextBox80.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox80.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox80.CornerRadius = 2;
            this.sboTextBox80.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox80.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox80.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox80.Location = new System.Drawing.Point(166, 255);
            this.sboTextBox80.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox80.Multiline = true;
            this.sboTextBox80.Name = "sboTextBox80";
            this.sboTextBox80.ReadOnly = false;
            this.sboTextBox80.Size = new System.Drawing.Size(307, 211);
            this.sboTextBox80.TabIndex = 6;
            this.sboTextBox80.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox80.TextValue = "";
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(357, 137);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(97, 16);
            this.checkBox7.TabIndex = 30;
            this.checkBox7.Text = "Customer Rebate";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(480, 137);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(72, 16);
            this.checkBox8.TabIndex = 31;
            this.checkBox8.Text = "Sales Order";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(582, 137);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(71, 16);
            this.checkBox9.TabIndex = 32;
            this.checkBox9.Text = "AR Invoice";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(680, 137);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(44, 16);
            this.checkBox10.TabIndex = 33;
            this.checkBox10.Text = "FOC";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(567, 53);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox11.Size = new System.Drawing.Size(127, 16);
            this.checkBox11.TabIndex = 34;
            this.checkBox11.Text = "Apply Haulage by Weight";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // sboComboBox7
            // 
            this.sboComboBox7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboComboBox7.CornerRadius = 2;
            this.sboComboBox7.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboComboBox7.DropDownHight = 100;
            this.sboComboBox7.EnabledColor = System.Drawing.Color.White;
            this.sboComboBox7.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboComboBox7.Location = new System.Drawing.Point(525, 23);
            this.sboComboBox7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboComboBox7.Multiline = false;
            this.sboComboBox7.Name = "sboComboBox7";
            this.sboComboBox7.SelectedIndex = -1;
            this.sboComboBox7.SelectedItem = null;
            this.sboComboBox7.Size = new System.Drawing.Size(50, 14);
            this.sboComboBox7.TabIndex = 35;
            this.sboComboBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboComboBox7.TextBoxReadOnly = true;
            this.sboComboBox7.TextValue = "";
            // 
            // sboTextBox81
            // 
            this.sboTextBox81.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox81.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox81.CornerRadius = 2;
            this.sboTextBox81.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox81.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox81.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox81.Location = new System.Drawing.Point(228, 23);
            this.sboTextBox81.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox81.Multiline = false;
            this.sboTextBox81.Name = "sboTextBox81";
            this.sboTextBox81.ReadOnly = false;
            this.sboTextBox81.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox81.TabIndex = 36;
            this.sboTextBox81.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox81.TextValue = "";
            // 
            // sboTextBox82
            // 
            this.sboTextBox82.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox82.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox82.CornerRadius = 2;
            this.sboTextBox82.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox82.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox82.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox82.Location = new System.Drawing.Point(335, 8);
            this.sboTextBox82.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox82.Multiline = false;
            this.sboTextBox82.Name = "sboTextBox82";
            this.sboTextBox82.ReadOnly = false;
            this.sboTextBox82.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox82.TabIndex = 37;
            this.sboTextBox82.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox82.TextValue = "";
            // 
            // sboTextBox83
            // 
            this.sboTextBox83.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox83.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox83.CornerRadius = 2;
            this.sboTextBox83.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox83.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox83.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox83.Location = new System.Drawing.Point(335, 23);
            this.sboTextBox83.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox83.Multiline = false;
            this.sboTextBox83.Name = "sboTextBox83";
            this.sboTextBox83.ReadOnly = false;
            this.sboTextBox83.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox83.TabIndex = 38;
            this.sboTextBox83.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox83.TextValue = "";
            // 
            // sboTextBox84
            // 
            this.sboTextBox84.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox84.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox84.CornerRadius = 2;
            this.sboTextBox84.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox84.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox84.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox84.Location = new System.Drawing.Point(335, 38);
            this.sboTextBox84.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox84.Multiline = false;
            this.sboTextBox84.Name = "sboTextBox84";
            this.sboTextBox84.ReadOnly = false;
            this.sboTextBox84.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox84.TabIndex = 39;
            this.sboTextBox84.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox84.TextValue = "";
            // 
            // sboTextBox85
            // 
            this.sboTextBox85.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox85.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox85.CornerRadius = 2;
            this.sboTextBox85.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox85.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox85.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox85.Location = new System.Drawing.Point(442, 23);
            this.sboTextBox85.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox85.Multiline = false;
            this.sboTextBox85.Name = "sboTextBox85";
            this.sboTextBox85.ReadOnly = false;
            this.sboTextBox85.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox85.TabIndex = 40;
            this.sboTextBox85.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox85.TextValue = "";
            // 
            // sboTextBox86
            // 
            this.sboTextBox86.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox86.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox86.CornerRadius = 2;
            this.sboTextBox86.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox86.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox86.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox86.Location = new System.Drawing.Point(442, 38);
            this.sboTextBox86.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox86.Multiline = false;
            this.sboTextBox86.Name = "sboTextBox86";
            this.sboTextBox86.ReadOnly = false;
            this.sboTextBox86.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox86.TabIndex = 41;
            this.sboTextBox86.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox86.TextValue = "";
            // 
            // sboTextBox87
            // 
            this.sboTextBox87.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox87.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox87.CornerRadius = 2;
            this.sboTextBox87.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox87.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox87.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox87.Location = new System.Drawing.Point(680, 23);
            this.sboTextBox87.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox87.Multiline = false;
            this.sboTextBox87.Name = "sboTextBox87";
            this.sboTextBox87.ReadOnly = false;
            this.sboTextBox87.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox87.TabIndex = 42;
            this.sboTextBox87.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox87.TextValue = "";
            // 
            // sboTextBox88
            // 
            this.sboTextBox88.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox88.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox88.CornerRadius = 2;
            this.sboTextBox88.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox88.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox88.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox88.Location = new System.Drawing.Point(680, 38);
            this.sboTextBox88.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox88.Multiline = false;
            this.sboTextBox88.Name = "sboTextBox88";
            this.sboTextBox88.ReadOnly = false;
            this.sboTextBox88.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox88.TabIndex = 43;
            this.sboTextBox88.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox88.TextValue = "";
            // 
            // sboTextBox89
            // 
            this.sboTextBox89.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox89.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox89.CornerRadius = 2;
            this.sboTextBox89.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox89.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox89.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox89.Location = new System.Drawing.Point(680, 88);
            this.sboTextBox89.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox89.Multiline = false;
            this.sboTextBox89.Name = "sboTextBox89";
            this.sboTextBox89.ReadOnly = false;
            this.sboTextBox89.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox89.TabIndex = 44;
            this.sboTextBox89.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox89.TextValue = "";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(188, 23);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(35, 12);
            this.label89.TabIndex = 45;
            this.label89.Text = "AUOM";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(283, 23);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(49, 12);
            this.label90.TabIndex = 46;
            this.label90.Text = "Actual Qty";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(283, 38);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(49, 12);
            this.label91.TabIndex = 47;
            this.label91.Text = "Actual Qty";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(390, 23);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(52, 12);
            this.label92.TabIndex = 48;
            this.label92.Text = "Charge Qty";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(390, 38);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(52, 12);
            this.label93.TabIndex = 49;
            this.label93.Text = "Charge Qty";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(271, 9);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(61, 12);
            this.label94.TabIndex = 50;
            this.label94.Text = "Expected Qty";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(499, 23);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(28, 12);
            this.label95.TabIndex = 51;
            this.label95.Text = "UOM";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(596, 23);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(79, 12);
            this.label96.TabIndex = 52;
            this.label96.Text = "Disposal Unit Chg";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(596, 38);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(77, 12);
            this.label97.TabIndex = 53;
            this.label97.Text = "Haulage Unit Chg";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(621, 88);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(52, 12);
            this.label98.TabIndex = 54;
            this.label98.Text = "Discount %";
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(24, 35);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(63, 16);
            this.checkBox12.TabIndex = 0;
            this.checkBox12.Text = "Trial Load";
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(354, 35);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(67, 16);
            this.checkBox13.TabIndex = 1;
            this.checkBox13.Text = "Scheduled";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(354, 50);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(82, 16);
            this.checkBox14.TabIndex = 2;
            this.checkBox14.Text = "Un-Scheduled";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(24, 305);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(88, 16);
            this.checkBox15.TabIndex = 3;
            this.checkBox15.Text = "Auto Print DOC";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(22, 16);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(44, 12);
            this.label99.TabIndex = 4;
            this.label99.Text = "General";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(352, 16);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(54, 12);
            this.label100.TabIndex = 5;
            this.label100.Text = "Reporting";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(659, 16);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(91, 12);
            this.label101.TabIndex = 6;
            this.label101.Text = "Accounting cont.";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(352, 80);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(62, 12);
            this.label102.TabIndex = 7;
            this.label102.Text = "Accounting";
            this.label102.Click += new System.EventHandler(this.label102_Click);
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(22, 287);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(44, 12);
            this.label103.TabIndex = 8;
            this.label103.Text = "Printing";
            // 
            // sboTextBox90
            // 
            this.sboTextBox90.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox90.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox90.CornerRadius = 2;
            this.sboTextBox90.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox90.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox90.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox90.Location = new System.Drawing.Point(154, 65);
            this.sboTextBox90.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox90.Multiline = true;
            this.sboTextBox90.Name = "sboTextBox90";
            this.sboTextBox90.ReadOnly = false;
            this.sboTextBox90.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox90.TabIndex = 9;
            this.sboTextBox90.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox90.TextValue = "";
            // 
            // sboTextBox91
            // 
            this.sboTextBox91.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox91.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox91.CornerRadius = 2;
            this.sboTextBox91.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox91.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox91.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox91.Location = new System.Drawing.Point(154, 80);
            this.sboTextBox91.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox91.Multiline = true;
            this.sboTextBox91.Name = "sboTextBox91";
            this.sboTextBox91.ReadOnly = false;
            this.sboTextBox91.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox91.TabIndex = 10;
            this.sboTextBox91.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox91.TextValue = "";
            // 
            // sboTextBox92
            // 
            this.sboTextBox92.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox92.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox92.CornerRadius = 2;
            this.sboTextBox92.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox92.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox92.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox92.Location = new System.Drawing.Point(154, 95);
            this.sboTextBox92.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox92.Multiline = true;
            this.sboTextBox92.Name = "sboTextBox92";
            this.sboTextBox92.ReadOnly = false;
            this.sboTextBox92.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox92.TabIndex = 11;
            this.sboTextBox92.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox92.TextValue = "";
            // 
            // sboTextBox93
            // 
            this.sboTextBox93.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox93.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox93.CornerRadius = 2;
            this.sboTextBox93.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox93.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox93.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox93.Location = new System.Drawing.Point(154, 125);
            this.sboTextBox93.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox93.Multiline = true;
            this.sboTextBox93.Name = "sboTextBox93";
            this.sboTextBox93.ReadOnly = false;
            this.sboTextBox93.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox93.TabIndex = 12;
            this.sboTextBox93.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox93.TextValue = "";
            // 
            // sboTextBox94
            // 
            this.sboTextBox94.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox94.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox94.CornerRadius = 2;
            this.sboTextBox94.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox94.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox94.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox94.Location = new System.Drawing.Point(154, 140);
            this.sboTextBox94.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox94.Multiline = true;
            this.sboTextBox94.Name = "sboTextBox94";
            this.sboTextBox94.ReadOnly = false;
            this.sboTextBox94.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox94.TabIndex = 13;
            this.sboTextBox94.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox94.TextValue = "";
            // 
            // sboTextBox95
            // 
            this.sboTextBox95.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox95.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox95.CornerRadius = 2;
            this.sboTextBox95.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox95.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox95.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox95.Location = new System.Drawing.Point(154, 155);
            this.sboTextBox95.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox95.Multiline = true;
            this.sboTextBox95.Name = "sboTextBox95";
            this.sboTextBox95.ReadOnly = false;
            this.sboTextBox95.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox95.TabIndex = 14;
            this.sboTextBox95.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox95.TextValue = "";
            // 
            // sboTextBox96
            // 
            this.sboTextBox96.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox96.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox96.CornerRadius = 2;
            this.sboTextBox96.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox96.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox96.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox96.Location = new System.Drawing.Point(154, 170);
            this.sboTextBox96.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox96.Multiline = true;
            this.sboTextBox96.Name = "sboTextBox96";
            this.sboTextBox96.ReadOnly = false;
            this.sboTextBox96.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox96.TabIndex = 15;
            this.sboTextBox96.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox96.TextValue = "";
            // 
            // sboTextBox97
            // 
            this.sboTextBox97.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox97.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox97.CornerRadius = 2;
            this.sboTextBox97.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox97.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox97.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox97.Location = new System.Drawing.Point(154, 185);
            this.sboTextBox97.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox97.Multiline = true;
            this.sboTextBox97.Name = "sboTextBox97";
            this.sboTextBox97.ReadOnly = false;
            this.sboTextBox97.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox97.TabIndex = 16;
            this.sboTextBox97.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox97.TextValue = "";
            // 
            // sboTextBox98
            // 
            this.sboTextBox98.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox98.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox98.CornerRadius = 2;
            this.sboTextBox98.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox98.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox98.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox98.Location = new System.Drawing.Point(154, 200);
            this.sboTextBox98.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox98.Multiline = true;
            this.sboTextBox98.Name = "sboTextBox98";
            this.sboTextBox98.ReadOnly = false;
            this.sboTextBox98.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox98.TabIndex = 17;
            this.sboTextBox98.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox98.TextValue = "";
            // 
            // sboTextBox99
            // 
            this.sboTextBox99.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox99.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox99.CornerRadius = 2;
            this.sboTextBox99.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox99.Enabled = false;
            this.sboTextBox99.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox99.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox99.Location = new System.Drawing.Point(154, 320);
            this.sboTextBox99.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox99.Multiline = true;
            this.sboTextBox99.Name = "sboTextBox99";
            this.sboTextBox99.ReadOnly = false;
            this.sboTextBox99.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox99.TabIndex = 18;
            this.sboTextBox99.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox99.TextValue = "";
            // 
            // sboTextBox100
            // 
            this.sboTextBox100.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox100.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox100.CornerRadius = 2;
            this.sboTextBox100.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox100.Enabled = false;
            this.sboTextBox100.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox100.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox100.Location = new System.Drawing.Point(828, 65);
            this.sboTextBox100.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox100.Multiline = true;
            this.sboTextBox100.Name = "sboTextBox100";
            this.sboTextBox100.ReadOnly = false;
            this.sboTextBox100.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox100.TabIndex = 21;
            this.sboTextBox100.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox100.TextValue = "";
            // 
            // sboTextBox101
            // 
            this.sboTextBox101.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox101.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox101.CornerRadius = 2;
            this.sboTextBox101.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox101.Enabled = false;
            this.sboTextBox101.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox101.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox101.Location = new System.Drawing.Point(828, 50);
            this.sboTextBox101.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox101.Multiline = true;
            this.sboTextBox101.Name = "sboTextBox101";
            this.sboTextBox101.ReadOnly = false;
            this.sboTextBox101.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox101.TabIndex = 20;
            this.sboTextBox101.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox101.TextValue = "";
            // 
            // sboTextBox102
            // 
            this.sboTextBox102.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox102.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox102.CornerRadius = 2;
            this.sboTextBox102.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox102.Enabled = false;
            this.sboTextBox102.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox102.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox102.Location = new System.Drawing.Point(828, 35);
            this.sboTextBox102.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox102.Multiline = true;
            this.sboTextBox102.Name = "sboTextBox102";
            this.sboTextBox102.ReadOnly = false;
            this.sboTextBox102.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox102.TabIndex = 19;
            this.sboTextBox102.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox102.TextValue = "";
            // 
            // sboTextBox103
            // 
            this.sboTextBox103.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox103.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox103.CornerRadius = 2;
            this.sboTextBox103.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox103.Enabled = false;
            this.sboTextBox103.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox103.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox103.Location = new System.Drawing.Point(828, 155);
            this.sboTextBox103.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox103.Multiline = true;
            this.sboTextBox103.Name = "sboTextBox103";
            this.sboTextBox103.ReadOnly = false;
            this.sboTextBox103.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox103.TabIndex = 24;
            this.sboTextBox103.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox103.TextValue = "";
            // 
            // sboTextBox104
            // 
            this.sboTextBox104.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox104.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox104.CornerRadius = 2;
            this.sboTextBox104.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox104.Enabled = false;
            this.sboTextBox104.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox104.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox104.Location = new System.Drawing.Point(828, 140);
            this.sboTextBox104.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox104.Multiline = true;
            this.sboTextBox104.Name = "sboTextBox104";
            this.sboTextBox104.ReadOnly = false;
            this.sboTextBox104.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox104.TabIndex = 23;
            this.sboTextBox104.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox104.TextValue = "";
            // 
            // sboTextBox105
            // 
            this.sboTextBox105.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox105.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox105.CornerRadius = 2;
            this.sboTextBox105.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox105.Enabled = false;
            this.sboTextBox105.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox105.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox105.Location = new System.Drawing.Point(828, 125);
            this.sboTextBox105.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox105.Multiline = true;
            this.sboTextBox105.Name = "sboTextBox105";
            this.sboTextBox105.ReadOnly = false;
            this.sboTextBox105.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox105.TabIndex = 22;
            this.sboTextBox105.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox105.TextValue = "";
            // 
            // sboTextBox106
            // 
            this.sboTextBox106.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox106.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox106.CornerRadius = 2;
            this.sboTextBox106.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox106.Enabled = false;
            this.sboTextBox106.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox106.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox106.Location = new System.Drawing.Point(828, 215);
            this.sboTextBox106.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox106.Multiline = true;
            this.sboTextBox106.Name = "sboTextBox106";
            this.sboTextBox106.ReadOnly = false;
            this.sboTextBox106.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox106.TabIndex = 27;
            this.sboTextBox106.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox106.TextValue = "";
            // 
            // sboTextBox107
            // 
            this.sboTextBox107.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox107.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox107.CornerRadius = 2;
            this.sboTextBox107.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox107.Enabled = false;
            this.sboTextBox107.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox107.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox107.Location = new System.Drawing.Point(828, 200);
            this.sboTextBox107.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox107.Multiline = true;
            this.sboTextBox107.Name = "sboTextBox107";
            this.sboTextBox107.ReadOnly = false;
            this.sboTextBox107.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox107.TabIndex = 26;
            this.sboTextBox107.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox107.TextValue = "";
            // 
            // sboTextBox108
            // 
            this.sboTextBox108.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox108.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox108.CornerRadius = 2;
            this.sboTextBox108.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox108.Enabled = false;
            this.sboTextBox108.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox108.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox108.Location = new System.Drawing.Point(828, 185);
            this.sboTextBox108.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox108.Multiline = true;
            this.sboTextBox108.Name = "sboTextBox108";
            this.sboTextBox108.ReadOnly = false;
            this.sboTextBox108.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox108.TabIndex = 25;
            this.sboTextBox108.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox108.TextValue = "";
            // 
            // sboTextBox109
            // 
            this.sboTextBox109.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox109.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox109.CornerRadius = 2;
            this.sboTextBox109.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox109.Enabled = false;
            this.sboTextBox109.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox109.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox109.Location = new System.Drawing.Point(828, 275);
            this.sboTextBox109.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox109.Multiline = true;
            this.sboTextBox109.Name = "sboTextBox109";
            this.sboTextBox109.ReadOnly = false;
            this.sboTextBox109.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox109.TabIndex = 30;
            this.sboTextBox109.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox109.TextValue = "";
            // 
            // sboTextBox110
            // 
            this.sboTextBox110.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox110.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox110.CornerRadius = 2;
            this.sboTextBox110.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox110.Enabled = false;
            this.sboTextBox110.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox110.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox110.Location = new System.Drawing.Point(828, 260);
            this.sboTextBox110.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox110.Multiline = true;
            this.sboTextBox110.Name = "sboTextBox110";
            this.sboTextBox110.ReadOnly = false;
            this.sboTextBox110.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox110.TabIndex = 29;
            this.sboTextBox110.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox110.TextValue = "";
            // 
            // sboTextBox111
            // 
            this.sboTextBox111.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox111.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox111.CornerRadius = 2;
            this.sboTextBox111.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox111.Enabled = false;
            this.sboTextBox111.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox111.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox111.Location = new System.Drawing.Point(828, 230);
            this.sboTextBox111.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox111.Multiline = true;
            this.sboTextBox111.Name = "sboTextBox111";
            this.sboTextBox111.ReadOnly = false;
            this.sboTextBox111.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox111.TabIndex = 28;
            this.sboTextBox111.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox111.TextValue = "";
            // 
            // sboTextBox112
            // 
            this.sboTextBox112.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox112.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox112.CornerRadius = 2;
            this.sboTextBox112.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox112.Enabled = false;
            this.sboTextBox112.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox112.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox112.Location = new System.Drawing.Point(828, 320);
            this.sboTextBox112.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox112.Multiline = true;
            this.sboTextBox112.Name = "sboTextBox112";
            this.sboTextBox112.ReadOnly = false;
            this.sboTextBox112.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox112.TabIndex = 32;
            this.sboTextBox112.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox112.TextValue = "";
            // 
            // sboTextBox113
            // 
            this.sboTextBox113.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox113.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox113.CornerRadius = 2;
            this.sboTextBox113.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox113.Enabled = false;
            this.sboTextBox113.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox113.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox113.Location = new System.Drawing.Point(828, 290);
            this.sboTextBox113.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox113.Multiline = true;
            this.sboTextBox113.Name = "sboTextBox113";
            this.sboTextBox113.ReadOnly = false;
            this.sboTextBox113.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox113.TabIndex = 31;
            this.sboTextBox113.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox113.TextValue = "";
            // 
            // sboTextBox114
            // 
            this.sboTextBox114.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox114.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox114.CornerRadius = 2;
            this.sboTextBox114.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox114.Enabled = false;
            this.sboTextBox114.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox114.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox114.Location = new System.Drawing.Point(508, 125);
            this.sboTextBox114.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox114.Multiline = true;
            this.sboTextBox114.Name = "sboTextBox114";
            this.sboTextBox114.ReadOnly = false;
            this.sboTextBox114.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox114.TabIndex = 34;
            this.sboTextBox114.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox114.TextValue = "";
            // 
            // sboTextBox115
            // 
            this.sboTextBox115.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox115.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox115.CornerRadius = 2;
            this.sboTextBox115.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox115.Enabled = false;
            this.sboTextBox115.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox115.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox115.Location = new System.Drawing.Point(508, 110);
            this.sboTextBox115.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox115.Multiline = true;
            this.sboTextBox115.Name = "sboTextBox115";
            this.sboTextBox115.ReadOnly = false;
            this.sboTextBox115.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox115.TabIndex = 33;
            this.sboTextBox115.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox115.TextValue = "";
            // 
            // sboTextBox116
            // 
            this.sboTextBox116.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox116.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox116.CornerRadius = 2;
            this.sboTextBox116.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox116.Enabled = false;
            this.sboTextBox116.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox116.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox116.Location = new System.Drawing.Point(508, 155);
            this.sboTextBox116.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox116.Multiline = true;
            this.sboTextBox116.Name = "sboTextBox116";
            this.sboTextBox116.ReadOnly = false;
            this.sboTextBox116.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox116.TabIndex = 35;
            this.sboTextBox116.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox116.TextValue = "";
            // 
            // sboTextBox117
            // 
            this.sboTextBox117.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox117.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox117.CornerRadius = 2;
            this.sboTextBox117.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox117.Enabled = false;
            this.sboTextBox117.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox117.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox117.Location = new System.Drawing.Point(558, 185);
            this.sboTextBox117.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox117.Multiline = true;
            this.sboTextBox117.Name = "sboTextBox117";
            this.sboTextBox117.ReadOnly = false;
            this.sboTextBox117.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox117.TabIndex = 36;
            this.sboTextBox117.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sboTextBox117.TextValue = "120+";
            // 
            // sboTextBox118
            // 
            this.sboTextBox118.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox118.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox118.CornerRadius = 2;
            this.sboTextBox118.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox118.Enabled = false;
            this.sboTextBox118.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox118.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox118.Location = new System.Drawing.Point(558, 200);
            this.sboTextBox118.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox118.Multiline = true;
            this.sboTextBox118.Name = "sboTextBox118";
            this.sboTextBox118.ReadOnly = false;
            this.sboTextBox118.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox118.TabIndex = 37;
            this.sboTextBox118.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox118.TextValue = "";
            // 
            // sboTextBox119
            // 
            this.sboTextBox119.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox119.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox119.CornerRadius = 2;
            this.sboTextBox119.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox119.Enabled = false;
            this.sboTextBox119.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox119.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox119.Location = new System.Drawing.Point(508, 230);
            this.sboTextBox119.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox119.Multiline = true;
            this.sboTextBox119.Name = "sboTextBox119";
            this.sboTextBox119.ReadOnly = false;
            this.sboTextBox119.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox119.TabIndex = 38;
            this.sboTextBox119.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox119.TextValue = "";
            // 
            // sboTextBox120
            // 
            this.sboTextBox120.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox120.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox120.CornerRadius = 2;
            this.sboTextBox120.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox120.Enabled = false;
            this.sboTextBox120.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox120.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox120.Location = new System.Drawing.Point(508, 245);
            this.sboTextBox120.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox120.Multiline = true;
            this.sboTextBox120.Name = "sboTextBox120";
            this.sboTextBox120.ReadOnly = false;
            this.sboTextBox120.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox120.TabIndex = 39;
            this.sboTextBox120.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox120.TextValue = "";
            // 
            // sboTextBox121
            // 
            this.sboTextBox121.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox121.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox121.CornerRadius = 2;
            this.sboTextBox121.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox121.Enabled = false;
            this.sboTextBox121.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox121.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox121.Location = new System.Drawing.Point(508, 260);
            this.sboTextBox121.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox121.Multiline = true;
            this.sboTextBox121.Name = "sboTextBox121";
            this.sboTextBox121.ReadOnly = false;
            this.sboTextBox121.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox121.TabIndex = 40;
            this.sboTextBox121.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox121.TextValue = "";
            // 
            // sboTextBox122
            // 
            this.sboTextBox122.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox122.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox122.CornerRadius = 2;
            this.sboTextBox122.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox122.Enabled = false;
            this.sboTextBox122.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox122.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox122.Location = new System.Drawing.Point(508, 275);
            this.sboTextBox122.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox122.Multiline = true;
            this.sboTextBox122.Name = "sboTextBox122";
            this.sboTextBox122.ReadOnly = false;
            this.sboTextBox122.Size = new System.Drawing.Size(100, 14);
            this.sboTextBox122.TabIndex = 41;
            this.sboTextBox122.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox122.TextValue = "";
            // 
            // sboTextBox123
            // 
            this.sboTextBox123.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox123.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox123.CornerRadius = 2;
            this.sboTextBox123.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox123.Enabled = false;
            this.sboTextBox123.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox123.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox123.Location = new System.Drawing.Point(354, 185);
            this.sboTextBox123.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox123.Multiline = false;
            this.sboTextBox123.Name = "sboTextBox123";
            this.sboTextBox123.ReadOnly = false;
            this.sboTextBox123.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox123.TabIndex = 42;
            this.sboTextBox123.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sboTextBox123.TextValue = "30";
            // 
            // sboTextBox124
            // 
            this.sboTextBox124.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox124.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox124.CornerRadius = 2;
            this.sboTextBox124.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox124.Enabled = false;
            this.sboTextBox124.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox124.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox124.Location = new System.Drawing.Point(405, 185);
            this.sboTextBox124.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox124.Multiline = false;
            this.sboTextBox124.Name = "sboTextBox124";
            this.sboTextBox124.ReadOnly = false;
            this.sboTextBox124.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox124.TabIndex = 43;
            this.sboTextBox124.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sboTextBox124.TextValue = "60";
            // 
            // sboTextBox125
            // 
            this.sboTextBox125.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox125.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox125.CornerRadius = 2;
            this.sboTextBox125.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox125.Enabled = false;
            this.sboTextBox125.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox125.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox125.Location = new System.Drawing.Point(456, 185);
            this.sboTextBox125.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox125.Multiline = false;
            this.sboTextBox125.Name = "sboTextBox125";
            this.sboTextBox125.ReadOnly = false;
            this.sboTextBox125.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox125.TabIndex = 44;
            this.sboTextBox125.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sboTextBox125.TextValue = "90";
            // 
            // sboTextBox126
            // 
            this.sboTextBox126.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox126.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox126.CornerRadius = 2;
            this.sboTextBox126.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox126.Enabled = false;
            this.sboTextBox126.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox126.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox126.Location = new System.Drawing.Point(507, 185);
            this.sboTextBox126.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox126.Multiline = false;
            this.sboTextBox126.Name = "sboTextBox126";
            this.sboTextBox126.ReadOnly = false;
            this.sboTextBox126.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox126.TabIndex = 45;
            this.sboTextBox126.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sboTextBox126.TextValue = "120";
            // 
            // sboTextBox127
            // 
            this.sboTextBox127.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox127.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox127.CornerRadius = 2;
            this.sboTextBox127.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox127.Enabled = false;
            this.sboTextBox127.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox127.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox127.Location = new System.Drawing.Point(354, 200);
            this.sboTextBox127.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox127.Multiline = false;
            this.sboTextBox127.Name = "sboTextBox127";
            this.sboTextBox127.ReadOnly = false;
            this.sboTextBox127.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox127.TabIndex = 46;
            this.sboTextBox127.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox127.TextValue = "";
            // 
            // sboTextBox128
            // 
            this.sboTextBox128.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox128.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox128.CornerRadius = 2;
            this.sboTextBox128.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox128.Enabled = false;
            this.sboTextBox128.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox128.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox128.Location = new System.Drawing.Point(405, 200);
            this.sboTextBox128.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox128.Multiline = false;
            this.sboTextBox128.Name = "sboTextBox128";
            this.sboTextBox128.ReadOnly = false;
            this.sboTextBox128.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox128.TabIndex = 47;
            this.sboTextBox128.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox128.TextValue = "";
            // 
            // sboTextBox129
            // 
            this.sboTextBox129.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox129.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox129.CornerRadius = 2;
            this.sboTextBox129.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox129.Enabled = false;
            this.sboTextBox129.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox129.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox129.Location = new System.Drawing.Point(456, 199);
            this.sboTextBox129.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox129.Multiline = false;
            this.sboTextBox129.Name = "sboTextBox129";
            this.sboTextBox129.ReadOnly = false;
            this.sboTextBox129.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox129.TabIndex = 48;
            this.sboTextBox129.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox129.TextValue = "";
            // 
            // sboTextBox130
            // 
            this.sboTextBox130.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox130.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox130.CornerRadius = 2;
            this.sboTextBox130.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox130.Enabled = false;
            this.sboTextBox130.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox130.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox130.Location = new System.Drawing.Point(507, 200);
            this.sboTextBox130.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox130.Multiline = false;
            this.sboTextBox130.Name = "sboTextBox130";
            this.sboTextBox130.ReadOnly = false;
            this.sboTextBox130.Size = new System.Drawing.Size(50, 14);
            this.sboTextBox130.TabIndex = 49;
            this.sboTextBox130.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox130.TextValue = "";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(22, 65);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(44, 12);
            this.label104.TabIndex = 50;
            this.label104.Text = "Obligated";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(22, 80);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(90, 12);
            this.label105.TabIndex = 51;
            this.label105.Text = "Compliance Scheme";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(21, 95);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(96, 12);
            this.label106.TabIndex = 52;
            this.label106.Text = "Consignment Note No";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(22, 125);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(31, 12);
            this.label107.TabIndex = 53;
            this.label107.Text = "Driver";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(21, 142);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(28, 12);
            this.label108.TabIndex = 54;
            this.label108.Text = "Crew";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(21, 205);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(66, 12);
            this.label109.TabIndex = 55;
            this.label109.Text = "Miles Traveled";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(352, 157);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(75, 12);
            this.label110.TabIndex = 56;
            this.label110.Text = "Account Balance";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(352, 126);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(55, 12);
            this.label111.TabIndex = 57;
            this.label111.Text = "Pay Method";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Enabled = false;
            this.label112.Location = new System.Drawing.Point(352, 110);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(49, 12);
            this.label112.TabIndex = 58;
            this.label112.Text = "Pay Terms";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(352, 232);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(65, 12);
            this.label113.TabIndex = 59;
            this.label113.Text = "Oldest Invoice";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(352, 247);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(65, 12);
            this.label114.TabIndex = 60;
            this.label114.Text = "Days Overdue";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(352, 262);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(62, 12);
            this.label115.TabIndex = 61;
            this.label115.Text = "Last Payment";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(352, 277);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(52, 12);
            this.label116.TabIndex = 62;
            this.label116.Text = "Credit Limit";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(22, 320);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(53, 12);
            this.label117.TabIndex = 63;
            this.label117.Text = "Print Status";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(659, 35);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(57, 12);
            this.label118.TabIndex = 64;
            this.label118.Text = "Sales Status";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(659, 50);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(73, 12);
            this.label119.TabIndex = 65;
            this.label119.Text = "Purchase Status";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(659, 67);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(110, 12);
            this.label120.TabIndex = 66;
            this.label120.Text = "Linked Weighbridge ticket";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(659, 127);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(53, 12);
            this.label121.TabIndex = 67;
            this.label121.Text = "Sales Order";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(659, 140);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(61, 12);
            this.label122.TabIndex = 68;
            this.label122.Text = "Sales Invoice";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(659, 155);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(109, 12);
            this.label123.TabIndex = 69;
            this.label123.Text = "Customer Goods Receipt";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(659, 185);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(104, 12);
            this.label124.TabIndex = 70;
            this.label124.Text = "Supplier Purchase Order";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(659, 200);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(105, 12);
            this.label125.TabIndex = 71;
            this.label125.Text = "Haulage Purchase Order";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(659, 215);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(109, 12);
            this.label126.TabIndex = 72;
            this.label126.Text = "Displosal Purchase Order";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(659, 230);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(123, 12);
            this.label127.TabIndex = 73;
            this.label127.Text = "Skip Licence Purchase Order";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(659, 260);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(116, 12);
            this.label128.TabIndex = 74;
            this.label128.Text = "Supplier Goods Receipt PO";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(659, 275);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(62, 12);
            this.label129.TabIndex = 75;
            this.label129.Text = "Delivery Note";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(659, 290);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(35, 12);
            this.label130.TabIndex = 76;
            this.label130.Text = "Journal";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(659, 320);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(143, 12);
            this.label131.TabIndex = 77;
            this.label131.Text = "Additional Items Purchase Orders";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.Location = new System.Drawing.Point(22, 16);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(99, 12);
            this.label132.TabIndex = 0;
            this.label132.Text = "Export Information";
            // 
            // sboTextBox131
            // 
            this.sboTextBox131.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox131.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox131.CornerRadius = 2;
            this.sboTextBox131.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox131.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox131.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox131.Location = new System.Drawing.Point(185, 35);
            this.sboTextBox131.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox131.Multiline = true;
            this.sboTextBox131.Name = "sboTextBox131";
            this.sboTextBox131.ReadOnly = false;
            this.sboTextBox131.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox131.TabIndex = 1;
            this.sboTextBox131.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox131.TextValue = "";
            // 
            // sboTextBox132
            // 
            this.sboTextBox132.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox132.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox132.CornerRadius = 2;
            this.sboTextBox132.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox132.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox132.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox132.Location = new System.Drawing.Point(185, 50);
            this.sboTextBox132.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox132.Multiline = true;
            this.sboTextBox132.Name = "sboTextBox132";
            this.sboTextBox132.ReadOnly = false;
            this.sboTextBox132.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox132.TabIndex = 2;
            this.sboTextBox132.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox132.TextValue = "";
            // 
            // sboTextBox133
            // 
            this.sboTextBox133.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox133.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox133.CornerRadius = 2;
            this.sboTextBox133.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox133.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox133.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox133.Location = new System.Drawing.Point(185, 65);
            this.sboTextBox133.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox133.Multiline = true;
            this.sboTextBox133.Name = "sboTextBox133";
            this.sboTextBox133.ReadOnly = false;
            this.sboTextBox133.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox133.TabIndex = 3;
            this.sboTextBox133.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox133.TextValue = "";
            // 
            // sboTextBox134
            // 
            this.sboTextBox134.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox134.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox134.CornerRadius = 2;
            this.sboTextBox134.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox134.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox134.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox134.Location = new System.Drawing.Point(185, 95);
            this.sboTextBox134.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox134.Multiline = true;
            this.sboTextBox134.Name = "sboTextBox134";
            this.sboTextBox134.ReadOnly = false;
            this.sboTextBox134.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox134.TabIndex = 4;
            this.sboTextBox134.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox134.TextValue = "";
            // 
            // sboTextBox135
            // 
            this.sboTextBox135.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox135.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox135.CornerRadius = 2;
            this.sboTextBox135.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox135.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox135.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox135.Location = new System.Drawing.Point(185, 110);
            this.sboTextBox135.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox135.Multiline = true;
            this.sboTextBox135.Name = "sboTextBox135";
            this.sboTextBox135.ReadOnly = false;
            this.sboTextBox135.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox135.TabIndex = 5;
            this.sboTextBox135.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox135.TextValue = "";
            // 
            // sboTextBox136
            // 
            this.sboTextBox136.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox136.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox136.CornerRadius = 2;
            this.sboTextBox136.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox136.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox136.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox136.Location = new System.Drawing.Point(185, 140);
            this.sboTextBox136.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox136.Multiline = true;
            this.sboTextBox136.Name = "sboTextBox136";
            this.sboTextBox136.ReadOnly = false;
            this.sboTextBox136.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox136.TabIndex = 6;
            this.sboTextBox136.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox136.TextValue = "";
            // 
            // sboTextBox137
            // 
            this.sboTextBox137.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox137.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox137.CornerRadius = 2;
            this.sboTextBox137.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox137.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox137.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox137.Location = new System.Drawing.Point(185, 155);
            this.sboTextBox137.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox137.Multiline = true;
            this.sboTextBox137.Name = "sboTextBox137";
            this.sboTextBox137.ReadOnly = false;
            this.sboTextBox137.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox137.TabIndex = 7;
            this.sboTextBox137.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox137.TextValue = "";
            // 
            // sboTextBox138
            // 
            this.sboTextBox138.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox138.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox138.CornerRadius = 2;
            this.sboTextBox138.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox138.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox138.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox138.Location = new System.Drawing.Point(185, 185);
            this.sboTextBox138.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox138.Multiline = true;
            this.sboTextBox138.Name = "sboTextBox138";
            this.sboTextBox138.ReadOnly = false;
            this.sboTextBox138.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox138.TabIndex = 8;
            this.sboTextBox138.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox138.TextValue = "";
            // 
            // sboTextBox139
            // 
            this.sboTextBox139.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox139.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox139.CornerRadius = 2;
            this.sboTextBox139.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox139.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox139.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox139.Location = new System.Drawing.Point(185, 200);
            this.sboTextBox139.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox139.Multiline = true;
            this.sboTextBox139.Name = "sboTextBox139";
            this.sboTextBox139.ReadOnly = false;
            this.sboTextBox139.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox139.TabIndex = 9;
            this.sboTextBox139.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox139.TextValue = "";
            // 
            // sboTextBox140
            // 
            this.sboTextBox140.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox140.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox140.CornerRadius = 2;
            this.sboTextBox140.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox140.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox140.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox140.Location = new System.Drawing.Point(185, 215);
            this.sboTextBox140.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox140.Multiline = true;
            this.sboTextBox140.Name = "sboTextBox140";
            this.sboTextBox140.ReadOnly = false;
            this.sboTextBox140.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox140.TabIndex = 10;
            this.sboTextBox140.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox140.TextValue = "";
            // 
            // sboTextBox141
            // 
            this.sboTextBox141.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox141.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox141.CornerRadius = 2;
            this.sboTextBox141.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox141.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox141.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox141.Location = new System.Drawing.Point(185, 230);
            this.sboTextBox141.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox141.Multiline = true;
            this.sboTextBox141.Name = "sboTextBox141";
            this.sboTextBox141.ReadOnly = false;
            this.sboTextBox141.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox141.TabIndex = 11;
            this.sboTextBox141.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox141.TextValue = "";
            // 
            // sboTextBox142
            // 
            this.sboTextBox142.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox142.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox142.CornerRadius = 2;
            this.sboTextBox142.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox142.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox142.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox142.Location = new System.Drawing.Point(185, 245);
            this.sboTextBox142.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox142.Multiline = true;
            this.sboTextBox142.Name = "sboTextBox142";
            this.sboTextBox142.ReadOnly = false;
            this.sboTextBox142.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox142.TabIndex = 12;
            this.sboTextBox142.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox142.TextValue = "";
            // 
            // sboTextBox143
            // 
            this.sboTextBox143.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox143.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox143.CornerRadius = 2;
            this.sboTextBox143.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox143.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox143.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox143.Location = new System.Drawing.Point(185, 260);
            this.sboTextBox143.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox143.Multiline = true;
            this.sboTextBox143.Name = "sboTextBox143";
            this.sboTextBox143.ReadOnly = false;
            this.sboTextBox143.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox143.TabIndex = 13;
            this.sboTextBox143.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox143.TextValue = "";
            // 
            // sboTextBox144
            // 
            this.sboTextBox144.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox144.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox144.CornerRadius = 2;
            this.sboTextBox144.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox144.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox144.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox144.Location = new System.Drawing.Point(185, 290);
            this.sboTextBox144.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox144.Multiline = true;
            this.sboTextBox144.Name = "sboTextBox144";
            this.sboTextBox144.ReadOnly = false;
            this.sboTextBox144.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox144.TabIndex = 14;
            this.sboTextBox144.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox144.TextValue = "";
            // 
            // sboTextBox145
            // 
            this.sboTextBox145.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox145.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox145.CornerRadius = 2;
            this.sboTextBox145.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox145.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox145.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox145.Location = new System.Drawing.Point(185, 335);
            this.sboTextBox145.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox145.Multiline = true;
            this.sboTextBox145.Name = "sboTextBox145";
            this.sboTextBox145.ReadOnly = false;
            this.sboTextBox145.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox145.TabIndex = 15;
            this.sboTextBox145.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox145.TextValue = "";
            // 
            // sboTextBox146
            // 
            this.sboTextBox146.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox146.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox146.CornerRadius = 2;
            this.sboTextBox146.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox146.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox146.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox146.Location = new System.Drawing.Point(724, 35);
            this.sboTextBox146.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox146.Multiline = true;
            this.sboTextBox146.Name = "sboTextBox146";
            this.sboTextBox146.ReadOnly = false;
            this.sboTextBox146.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox146.TabIndex = 16;
            this.sboTextBox146.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox146.TextValue = "";
            // 
            // sboTextBox147
            // 
            this.sboTextBox147.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox147.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox147.CornerRadius = 2;
            this.sboTextBox147.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox147.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox147.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox147.Location = new System.Drawing.Point(724, 50);
            this.sboTextBox147.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox147.Multiline = true;
            this.sboTextBox147.Name = "sboTextBox147";
            this.sboTextBox147.ReadOnly = false;
            this.sboTextBox147.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox147.TabIndex = 17;
            this.sboTextBox147.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox147.TextValue = "";
            // 
            // sboTextBox148
            // 
            this.sboTextBox148.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox148.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox148.CornerRadius = 2;
            this.sboTextBox148.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox148.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox148.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox148.Location = new System.Drawing.Point(724, 65);
            this.sboTextBox148.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox148.Multiline = true;
            this.sboTextBox148.Name = "sboTextBox148";
            this.sboTextBox148.ReadOnly = false;
            this.sboTextBox148.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox148.TabIndex = 18;
            this.sboTextBox148.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox148.TextValue = "";
            // 
            // sboTextBox149
            // 
            this.sboTextBox149.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox149.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox149.CornerRadius = 2;
            this.sboTextBox149.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox149.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox149.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox149.Location = new System.Drawing.Point(724, 80);
            this.sboTextBox149.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox149.Multiline = true;
            this.sboTextBox149.Name = "sboTextBox149";
            this.sboTextBox149.ReadOnly = false;
            this.sboTextBox149.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox149.TabIndex = 19;
            this.sboTextBox149.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox149.TextValue = "";
            // 
            // sboTextBox150
            // 
            this.sboTextBox150.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox150.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox150.CornerRadius = 2;
            this.sboTextBox150.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox150.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox150.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox150.Location = new System.Drawing.Point(185, 320);
            this.sboTextBox150.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox150.Multiline = true;
            this.sboTextBox150.Name = "sboTextBox150";
            this.sboTextBox150.ReadOnly = false;
            this.sboTextBox150.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox150.TabIndex = 20;
            this.sboTextBox150.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox150.TextValue = "";
            // 
            // sboTextBox151
            // 
            this.sboTextBox151.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox151.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox151.CornerRadius = 2;
            this.sboTextBox151.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox151.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox151.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox151.Location = new System.Drawing.Point(185, 275);
            this.sboTextBox151.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox151.Multiline = true;
            this.sboTextBox151.Name = "sboTextBox151";
            this.sboTextBox151.ReadOnly = false;
            this.sboTextBox151.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox151.TabIndex = 21;
            this.sboTextBox151.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox151.TextValue = "";
            // 
            // sboTextBox152
            // 
            this.sboTextBox152.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox152.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox152.CornerRadius = 2;
            this.sboTextBox152.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox152.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox152.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox152.Location = new System.Drawing.Point(724, 125);
            this.sboTextBox152.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox152.Multiline = true;
            this.sboTextBox152.Name = "sboTextBox152";
            this.sboTextBox152.ReadOnly = false;
            this.sboTextBox152.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox152.TabIndex = 24;
            this.sboTextBox152.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox152.TextValue = "";
            // 
            // sboTextBox153
            // 
            this.sboTextBox153.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox153.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox153.CornerRadius = 2;
            this.sboTextBox153.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox153.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox153.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox153.Location = new System.Drawing.Point(724, 110);
            this.sboTextBox153.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox153.Multiline = true;
            this.sboTextBox153.Name = "sboTextBox153";
            this.sboTextBox153.ReadOnly = false;
            this.sboTextBox153.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox153.TabIndex = 23;
            this.sboTextBox153.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox153.TextValue = "";
            // 
            // sboTextBox154
            // 
            this.sboTextBox154.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox154.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox154.CornerRadius = 2;
            this.sboTextBox154.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox154.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox154.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox154.Location = new System.Drawing.Point(724, 95);
            this.sboTextBox154.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox154.Multiline = true;
            this.sboTextBox154.Name = "sboTextBox154";
            this.sboTextBox154.ReadOnly = false;
            this.sboTextBox154.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox154.TabIndex = 22;
            this.sboTextBox154.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox154.TextValue = "";
            // 
            // sboTextBox155
            // 
            this.sboTextBox155.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox155.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox155.CornerRadius = 2;
            this.sboTextBox155.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox155.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox155.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox155.Location = new System.Drawing.Point(724, 155);
            this.sboTextBox155.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox155.Multiline = true;
            this.sboTextBox155.Name = "sboTextBox155";
            this.sboTextBox155.ReadOnly = false;
            this.sboTextBox155.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox155.TabIndex = 25;
            this.sboTextBox155.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox155.TextValue = "";
            // 
            // sboTextBox156
            // 
            this.sboTextBox156.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox156.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox156.CornerRadius = 2;
            this.sboTextBox156.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox156.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox156.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox156.Location = new System.Drawing.Point(724, 185);
            this.sboTextBox156.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox156.Multiline = true;
            this.sboTextBox156.Name = "sboTextBox156";
            this.sboTextBox156.ReadOnly = false;
            this.sboTextBox156.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox156.TabIndex = 26;
            this.sboTextBox156.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox156.TextValue = "";
            // 
            // sboTextBox157
            // 
            this.sboTextBox157.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox157.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox157.CornerRadius = 2;
            this.sboTextBox157.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox157.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox157.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox157.Location = new System.Drawing.Point(724, 200);
            this.sboTextBox157.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox157.Multiline = true;
            this.sboTextBox157.Name = "sboTextBox157";
            this.sboTextBox157.ReadOnly = false;
            this.sboTextBox157.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox157.TabIndex = 27;
            this.sboTextBox157.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox157.TextValue = "";
            // 
            // sboTextBox158
            // 
            this.sboTextBox158.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox158.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox158.CornerRadius = 2;
            this.sboTextBox158.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox158.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox158.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox158.Location = new System.Drawing.Point(724, 215);
            this.sboTextBox158.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox158.Multiline = true;
            this.sboTextBox158.Name = "sboTextBox158";
            this.sboTextBox158.ReadOnly = false;
            this.sboTextBox158.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox158.TabIndex = 28;
            this.sboTextBox158.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox158.TextValue = "";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(22, 35);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(80, 12);
            this.label133.TabIndex = 29;
            this.label133.Text = "Container Number";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(22, 50);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(58, 12);
            this.label134.TabIndex = 30;
            this.label134.Text = "Seal Number";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(22, 65);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(69, 12);
            this.label135.TabIndex = 31;
            this.label135.Text = "Bale/Bag Count";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(22, 95);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(71, 12);
            this.label136.TabIndex = 32;
            this.label136.Text = "Destination Port";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(22, 110);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(78, 12);
            this.label137.TabIndex = 33;
            this.label137.Text = "Port Of Discharge";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(22, 140);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(49, 12);
            this.label138.TabIndex = 34;
            this.label138.Text = "CCIC Pics";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(22, 155);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(28, 12);
            this.label139.TabIndex = 35;
            this.label139.Text = "CCIS";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(22, 185);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(59, 12);
            this.label140.TabIndex = 36;
            this.label140.Text = "Shipping Line";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(22, 200);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(65, 12);
            this.label141.TabIndex = 37;
            this.label141.Text = "Feeder Vessel";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(22, 215);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(65, 12);
            this.label142.TabIndex = 38;
            this.label142.Text = "Departure Port";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(22, 230);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(84, 12);
            this.label143.TabIndex = 39;
            this.label143.Text = "Feeder Vessel ETS";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(22, 245);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(66, 12);
            this.label144.TabIndex = 40;
            this.label144.Text = "Mother Vessel";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(22, 260);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(65, 12);
            this.label145.TabIndex = 41;
            this.label145.Text = "Departure Port";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(22, 275);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(85, 12);
            this.label146.TabIndex = 42;
            this.label146.Text = "Mother Vessel ETS";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(22, 290);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(86, 12);
            this.label147.TabIndex = 43;
            this.label147.Text = "Mother Vessel ETA";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(22, 320);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(65, 12);
            this.label148.TabIndex = 44;
            this.label148.Text = "Freight Ord No";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(22, 335);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(59, 12);
            this.label149.TabIndex = 45;
            this.label149.Text = "Shipping Line";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(546, 35);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(80, 12);
            this.label150.TabIndex = 46;
            this.label150.Text = "BL Sent to Shipper";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(546, 50);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(112, 12);
            this.label151.TabIndex = 47;
            this.label151.Text = "BL Received from Shipper";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(546, 65);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(69, 12);
            this.label152.TabIndex = 48;
            this.label152.Text = "BL Amendment";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(546, 80);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(55, 12);
            this.label153.TabIndex = 49;
            this.label153.Text = "BL Changed";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(546, 95);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(123, 12);
            this.label154.TabIndex = 50;
            this.label154.Text = "BL Amendment from Shipper";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(546, 110);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(51, 12);
            this.label155.TabIndex = 51;
            this.label155.Text = "BL Sign Off";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(546, 125);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(90, 12);
            this.label156.TabIndex = 52;
            this.label156.Text = "BL Sent to Customer";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(546, 157);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(73, 12);
            this.label157.TabIndex = 53;
            this.label157.Text = "Release Method";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(546, 187);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(66, 12);
            this.label158.TabIndex = 54;
            this.label158.Text = "Notifying Party";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(546, 202);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(73, 12);
            this.label159.TabIndex = 55;
            this.label159.Text = "Consignee Party";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(546, 217);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(55, 12);
            this.label160.TabIndex = 56;
            this.label160.Text = "Contact No.";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(17, 33);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(69, 16);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Incomming";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(17, 55);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(60, 16);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Outgoing";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button11.Location = new System.Drawing.Point(17, 79);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 22;
            this.button11.Text = "On Site";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button12.Location = new System.Drawing.Point(622, 55);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 23;
            this.button12.Text = "Refresh";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button13.Location = new System.Drawing.Point(622, 79);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 24;
            this.button13.Text = "Buffer";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button14.Location = new System.Drawing.Point(622, 126);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 25;
            this.button14.Text = "Bridge";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button15.Location = new System.Drawing.Point(622, 149);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 26;
            this.button15.Text = "Bridge";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button16.Location = new System.Drawing.Point(697, 126);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 27;
            this.button16.Text = "Buffer";
            this.button16.UseVisualStyleBackColor = false;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button17.Location = new System.Drawing.Point(697, 149);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 28;
            this.button17.Text = "Buffer";
            this.button17.UseVisualStyleBackColor = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button18.Location = new System.Drawing.Point(772, 126);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 23);
            this.button18.TabIndex = 29;
            this.button18.Text = "Tarre";
            this.button18.UseVisualStyleBackColor = false;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button19.Location = new System.Drawing.Point(772, 149);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 23);
            this.button19.TabIndex = 30;
            this.button19.Text = "Tarre";
            this.button19.UseVisualStyleBackColor = false;
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.button20.Location = new System.Drawing.Point(535, 326);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 23);
            this.button20.TabIndex = 31;
            this.button20.Text = "Re-Print Tkt";
            this.button20.UseVisualStyleBackColor = false;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(16, 285);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(46, 16);
            this.checkBox16.TabIndex = 32;
            this.checkBox16.Text = "Cash";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(93, 285);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(56, 16);
            this.checkBox17.TabIndex = 33;
            this.checkBox17.Text = "Cheque";
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(174, 285);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(71, 16);
            this.checkBox18.TabIndex = 34;
            this.checkBox18.Text = "Credit Card";
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Location = new System.Drawing.Point(274, 285);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(59, 16);
            this.checkBox19.TabIndex = 35;
            this.checkBox19.Text = "Account";
            this.checkBox19.UseVisualStyleBackColor = true;
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Location = new System.Drawing.Point(16, 330);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(54, 16);
            this.checkBox20.TabIndex = 36;
            this.checkBox20.Text = "WB Tkt";
            this.checkBox20.UseVisualStyleBackColor = true;
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.Location = new System.Drawing.Point(93, 330);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(45, 16);
            this.checkBox21.TabIndex = 37;
            this.checkBox21.Text = "DOC";
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.Location = new System.Drawing.Point(174, 330);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(110, 16);
            this.checkBox22.TabIndex = 38;
            this.checkBox22.Text = "Do Stock Movement";
            this.checkBox22.UseVisualStyleBackColor = true;
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.Location = new System.Drawing.Point(374, 330);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(99, 16);
            this.checkBox23.TabIndex = 39;
            this.checkBox23.Text = "Don\'t Charge VAT";
            this.checkBox23.UseVisualStyleBackColor = true;
            // 
            // sboTextBox160
            // 
            this.sboTextBox160.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox160.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox160.CornerRadius = 2;
            this.sboTextBox160.DisabledColor = System.Drawing.Color.SteelBlue;
            this.sboTextBox160.Enabled = false;
            this.sboTextBox160.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox160.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox160.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sboTextBox160.Location = new System.Drawing.Point(408, 55);
            this.sboTextBox160.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox160.Multiline = true;
            this.sboTextBox160.Name = "sboTextBox160";
            this.sboTextBox160.ReadOnly = false;
            this.sboTextBox160.Size = new System.Drawing.Size(200, 47);
            this.sboTextBox160.TabIndex = 41;
            this.sboTextBox160.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox160.TextValue = "00";
            // 
            // sboTextBox161
            // 
            this.sboTextBox161.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox161.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox161.CornerRadius = 2;
            this.sboTextBox161.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox161.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox161.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox161.Location = new System.Drawing.Point(410, 130);
            this.sboTextBox161.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox161.Multiline = true;
            this.sboTextBox161.Name = "sboTextBox161";
            this.sboTextBox161.ReadOnly = false;
            this.sboTextBox161.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox161.TabIndex = 42;
            this.sboTextBox161.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox161.TextValue = "";
            // 
            // sboTextBox162
            // 
            this.sboTextBox162.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox162.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox162.CornerRadius = 2;
            this.sboTextBox162.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox162.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox162.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox162.Location = new System.Drawing.Point(410, 153);
            this.sboTextBox162.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox162.Multiline = true;
            this.sboTextBox162.Name = "sboTextBox162";
            this.sboTextBox162.ReadOnly = false;
            this.sboTextBox162.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox162.TabIndex = 43;
            this.sboTextBox162.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox162.TextValue = "";
            // 
            // sboTextBox163
            // 
            this.sboTextBox163.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox163.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox163.CornerRadius = 2;
            this.sboTextBox163.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox163.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox163.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox163.Location = new System.Drawing.Point(410, 190);
            this.sboTextBox163.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox163.Multiline = true;
            this.sboTextBox163.Name = "sboTextBox163";
            this.sboTextBox163.ReadOnly = false;
            this.sboTextBox163.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox163.TabIndex = 44;
            this.sboTextBox163.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox163.TextValue = "";
            // 
            // sboTextBox164
            // 
            this.sboTextBox164.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox164.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox164.CornerRadius = 2;
            this.sboTextBox164.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox164.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox164.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox164.Location = new System.Drawing.Point(410, 205);
            this.sboTextBox164.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox164.Multiline = true;
            this.sboTextBox164.Name = "sboTextBox164";
            this.sboTextBox164.ReadOnly = false;
            this.sboTextBox164.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox164.TabIndex = 45;
            this.sboTextBox164.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox164.TextValue = "";
            // 
            // sboTextBox165
            // 
            this.sboTextBox165.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox165.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox165.CornerRadius = 2;
            this.sboTextBox165.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox165.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox165.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox165.Location = new System.Drawing.Point(410, 220);
            this.sboTextBox165.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox165.Multiline = true;
            this.sboTextBox165.Name = "sboTextBox165";
            this.sboTextBox165.ReadOnly = false;
            this.sboTextBox165.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox165.TabIndex = 46;
            this.sboTextBox165.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox165.TextValue = "";
            // 
            // sboTextBox166
            // 
            this.sboTextBox166.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox166.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox166.CornerRadius = 2;
            this.sboTextBox166.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox166.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox166.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox166.Location = new System.Drawing.Point(410, 235);
            this.sboTextBox166.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox166.Multiline = true;
            this.sboTextBox166.Name = "sboTextBox166";
            this.sboTextBox166.ReadOnly = false;
            this.sboTextBox166.Size = new System.Drawing.Size(200, 14);
            this.sboTextBox166.TabIndex = 47;
            this.sboTextBox166.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox166.TextValue = "";
            // 
            // sboTextBox167
            // 
            this.sboTextBox167.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox167.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox167.CornerRadius = 2;
            this.sboTextBox167.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox167.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox167.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox167.Location = new System.Drawing.Point(778, 285);
            this.sboTextBox167.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox167.Multiline = true;
            this.sboTextBox167.Name = "sboTextBox167";
            this.sboTextBox167.ReadOnly = false;
            this.sboTextBox167.Size = new System.Drawing.Size(150, 14);
            this.sboTextBox167.TabIndex = 48;
            this.sboTextBox167.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox167.TextValue = "";
            // 
            // sboTextBox168
            // 
            this.sboTextBox168.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox168.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox168.CornerRadius = 2;
            this.sboTextBox168.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox168.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox168.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox168.Location = new System.Drawing.Point(778, 300);
            this.sboTextBox168.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox168.Multiline = true;
            this.sboTextBox168.Name = "sboTextBox168";
            this.sboTextBox168.ReadOnly = false;
            this.sboTextBox168.Size = new System.Drawing.Size(150, 14);
            this.sboTextBox168.TabIndex = 49;
            this.sboTextBox168.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox168.TextValue = "";
            // 
            // sboTextBox169
            // 
            this.sboTextBox169.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox169.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox169.CornerRadius = 2;
            this.sboTextBox169.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox169.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox169.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox169.Location = new System.Drawing.Point(778, 315);
            this.sboTextBox169.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox169.Multiline = true;
            this.sboTextBox169.Name = "sboTextBox169";
            this.sboTextBox169.ReadOnly = false;
            this.sboTextBox169.Size = new System.Drawing.Size(150, 14);
            this.sboTextBox169.TabIndex = 50;
            this.sboTextBox169.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox169.TextValue = "";
            // 
            // sboTextBox170
            // 
            this.sboTextBox170.BackColor = System.Drawing.Color.Transparent;
            this.sboTextBox170.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboTextBox170.CornerRadius = 2;
            this.sboTextBox170.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboTextBox170.EnabledColor = System.Drawing.Color.White;
            this.sboTextBox170.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboTextBox170.Location = new System.Drawing.Point(778, 330);
            this.sboTextBox170.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboTextBox170.Multiline = true;
            this.sboTextBox170.Name = "sboTextBox170";
            this.sboTextBox170.ReadOnly = false;
            this.sboTextBox170.Size = new System.Drawing.Size(150, 14);
            this.sboTextBox170.TabIndex = 51;
            this.sboTextBox170.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboTextBox170.TextValue = "";
            // 
            // sboComboBox8
            // 
            this.sboComboBox8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.sboComboBox8.CornerRadius = 2;
            this.sboComboBox8.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(229)))), ((int)(((byte)(242)))));
            this.sboComboBox8.DropDownHight = 100;
            this.sboComboBox8.EnabledColor = System.Drawing.Color.White;
            this.sboComboBox8.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(240)))), ((int)(((byte)(158)))));
            this.sboComboBox8.Location = new System.Drawing.Point(408, 35);
            this.sboComboBox8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sboComboBox8.Multiline = true;
            this.sboComboBox8.Name = "sboComboBox8";
            this.sboComboBox8.SelectedIndex = -1;
            this.sboComboBox8.SelectedItem = null;
            this.sboComboBox8.Size = new System.Drawing.Size(200, 14);
            this.sboComboBox8.TabIndex = 52;
            this.sboComboBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.sboComboBox8.TextBoxReadOnly = true;
            this.sboComboBox8.TextValue = "";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WR1Bridge.Properties.Resources.M;
            this.pictureBox1.Location = new System.Drawing.Point(622, 179);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(75, 70);
            this.pictureBox1.TabIndex = 53;
            this.pictureBox1.TabStop = false;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(290, 37);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(84, 12);
            this.label161.TabIndex = 54;
            this.label161.Text = "Select Weighbridge";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(290, 55);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(72, 12);
            this.label162.TabIndex = 55;
            this.label162.Text = "Current Reading";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(290, 131);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(64, 12);
            this.label163.TabIndex = 56;
            this.label163.Text = "1st Weigh (kg)";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(290, 154);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(66, 12);
            this.label164.TabIndex = 57;
            this.label164.Text = "2nd Weigh (kg)";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(290, 192);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(76, 12);
            this.label165.TabIndex = 58;
            this.label165.Text = "Read Weight (kg)";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(290, 207);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(52, 12);
            this.label166.TabIndex = 59;
            this.label166.Text = "AUOM Qty";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(290, 222);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(101, 12);
            this.label167.TabIndex = 60;
            this.label167.Text = "Weight Deductions (kg)";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(290, 237);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(73, 12);
            this.label168.TabIndex = 61;
            this.label168.Text = "Adjusted Weight";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(719, 285);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(40, 12);
            this.label169.TabIndex = 62;
            this.label169.Text = "Zone No";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(719, 300);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(40, 12);
            this.label170.TabIndex = 63;
            this.label170.Text = "WTN No";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(719, 315);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(52, 12);
            this.label171.TabIndex = 64;
            this.label171.Text = "Site Ref No";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(719, 330);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(50, 12);
            this.label172.TabIndex = 65;
            this.label172.Text = "Ext WB No";
            // 
            // WO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 784);
            this.Controls.Add(this.sboTextBox69);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.sboTextBox22);
            this.Controls.Add(this.sboTextBox31);
            this.Controls.Add(this.sboTextBox63);
            this.Controls.Add(this.sboTextBox64);
            this.Controls.Add(this.sboTextBox68);
            this.Controls.Add(this.sboComboBox6);
            this.Controls.Add(this.sboComboBox5);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.IDHTabs);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.sboTextBox23);
            this.Controls.Add(this.sboTextBox24);
            this.Controls.Add(this.sboTextBox25);
            this.Controls.Add(this.sboTextBox26);
            this.Controls.Add(this.sboTextBox27);
            this.Controls.Add(this.sboTextBox28);
            this.Controls.Add(this.sboTextBox29);
            this.Controls.Add(this.sboTextBox30);
            this.Controls.Add(this.sboTextBox32);
            this.Controls.Add(this.sboTextBox33);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.sboTextBox13);
            this.Controls.Add(this.sboTextBox15);
            this.Controls.Add(this.sboTextBox16);
            this.Controls.Add(this.sboTextBox19);
            this.Controls.Add(this.sboTextBox20);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.sboTextBox11);
            this.Controls.Add(this.sboTextBox9);
            this.Controls.Add(this.sboTextBox8);
            this.Controls.Add(this.sboTextBox7);
            this.Controls.Add(this.sboTextBox6);
            this.Controls.Add(this.sboTextBox5);
            this.Controls.Add(this.sboTextBox4);
            this.Controls.Add(this.sboTextBox3);
            this.Controls.Add(this.sboTextBox2);
            this.Controls.Add(this.sboTextBox1);
            this.Controls.Add(this.label1);
            //this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WO";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.IDHTabs.ResumeLayout(false);
            this.IDH_TABCOS.ResumeLayout(false);
            this.IDH_TABCOS.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.IDH_TABCOM.ResumeLayout(false);
            this.IDH_TABCOM.PerformLayout();
            this.IDH_TABADD.ResumeLayout(false);
            this.IDH_TABADD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oAddGrid)).EndInit();
            this.IDH_TABACT.ResumeLayout(false);
            this.IDH_TABACT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wR1Grid2)).EndInit();
            this.IDH_TABADM.ResumeLayout(false);
            this.IDH_TABADM.PerformLayout();
            this.IDH_TABEXP.ResumeLayout(false);
            this.IDH_TABEXP.PerformLayout();
            this.IDH_TABWEI.ResumeLayout(false);
            this.IDH_TABWEI.PerformLayout();
            this.IDH_TABDED.ResumeLayout(false);
            this.IDH_TABDED.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oDeductionsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private win.controls.SBO.SBOTextBox sboTextBox1;
        private win.controls.SBO.SBOTextBox sboTextBox2;
        private win.controls.SBO.SBOTextBox sboTextBox3;
        private win.controls.SBO.SBOTextBox sboTextBox4;
        private win.controls.SBO.SBOTextBox sboTextBox5;
        private win.controls.SBO.SBOTextBox sboTextBox6;
        private win.controls.SBO.SBOTextBox sboTextBox7;
        private win.controls.SBO.SBOTextBox sboTextBox8;
        private win.controls.SBO.SBOTextBox sboTextBox9;
        private win.controls.SBO.SBOTextBox sboTextBox11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private win.controls.SBO.SBOTextBox sboTextBox13;
        private win.controls.SBO.SBOTextBox sboTextBox15;
        private win.controls.SBO.SBOTextBox sboTextBox16;
        private win.controls.SBO.SBOTextBox sboTextBox19;
        private win.controls.SBO.SBOTextBox sboTextBox20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private win.controls.SBO.SBOTextBox sboTextBox23;
        private win.controls.SBO.SBOTextBox sboTextBox24;
        private win.controls.SBO.SBOTextBox sboTextBox25;
        private win.controls.SBO.SBOTextBox sboTextBox26;
        private win.controls.SBO.SBOTextBox sboTextBox27;
        private win.controls.SBO.SBOTextBox sboTextBox28;
        private win.controls.SBO.SBOTextBox sboTextBox29;
        private win.controls.SBO.SBOTextBox sboTextBox30;
        private win.controls.SBO.SBOTextBox sboTextBox32;
        private win.controls.SBO.SBOTextBox sboTextBox33;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TabControl IDHTabs;
        private System.Windows.Forms.TabPage IDH_TABCOS;
        private System.Windows.Forms.TabPage IDH_TABCOM;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private win.controls.SBO.SBOTextBox sboTextBox52;
        private win.controls.SBO.SBOTextBox sboTextBox54;
        private win.controls.SBO.SBOTextBox sboTextBox55;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button button10;
        private win.controls.SBO.SBOTextBox sboTextBox10;
        private win.controls.SBO.SBOTextBox sboTextBox12;
        private win.controls.SBO.SBOTextBox sboTextBox14;
        private win.controls.SBO.SBOTextBox sboTextBox17;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button button9;
        private win.controls.SBO.SBOTextBox sboTextBox18;
        private win.controls.SBO.SBOTextBox sboTextBox21;
        private win.controls.SBO.SBOTextBox sboTextBox50;
        private win.controls.SBO.SBOTextBox sboTextBox51;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button button8;
        private win.controls.SBO.SBOTextBox sboTextBox46;
        private win.controls.SBO.SBOTextBox sboTextBox47;
        private win.controls.SBO.SBOTextBox sboTextBox48;
        private win.controls.SBO.SBOTextBox sboTextBox49;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button7;
        private win.controls.SBO.SBOTextBox sboTextBox35;
        private win.controls.SBO.SBOTextBox sboTextBox43;
        private win.controls.SBO.SBOTextBox sboTextBox44;
        private win.controls.SBO.SBOTextBox sboTextBox45;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button6;
        private win.controls.SBO.SBOTextBox sboTextBox34;
        private win.controls.SBO.SBOTextBox sboTextBox36;
        private win.controls.SBO.SBOTextBox sboTextBox37;
        private win.controls.SBO.SBOTextBox sboTextBox38;
        private win.controls.SBO.SBOTextBox sboTextBox39;
        private win.controls.SBO.SBOTextBox sboTextBox40;
        private win.controls.SBO.SBOTextBox sboTextBox41;
        private win.controls.SBO.SBOTextBox sboTextBox42;
        private System.Windows.Forms.TabPage IDH_TABADD;
        private System.Windows.Forms.TabPage IDH_TABACT;
        private System.Windows.Forms.TabPage IDH_TABADM;
        private System.Windows.Forms.TabPage IDH_TABEXP;
        private System.Windows.Forms.TabPage IDH_TABWEI;
        private System.Windows.Forms.TabPage IDH_TABDED;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private win.controls.SBO.SBOComboBox sboComboBox4;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.CheckBox checkBox6;
        private win.controls.SBO.SBOTextBox sboTextBox70;
        private win.controls.SBO.SBOTextBox sboTextBox71;
        private win.controls.SBO.SBOTextBox sboTextBox72;
        private win.controls.SBO.SBOComboBox sboComboBox3;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.CheckBox checkBox5;
        private win.controls.SBO.SBOTextBox sboTextBox65;
        private win.controls.SBO.SBOTextBox sboTextBox66;
        private win.controls.SBO.SBOTextBox sboTextBox67;
        private win.controls.SBO.SBOComboBox sboComboBox2;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.CheckBox checkBox4;
        private win.controls.SBO.SBOTextBox sboTextBox58;
        private win.controls.SBO.SBOTextBox sboTextBox61;
        private win.controls.SBO.SBOTextBox sboTextBox62;
        private win.controls.SBO.SBOComboBox sboComboBox1;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.CheckBox checkBox3;
        private win.controls.SBO.SBOTextBox sboTextBox60;
        private win.controls.SBO.SBOTextBox sboTextBox59;
        private win.controls.SBO.SBOTextBox sboTextBox57;
        private win.controls.SBO.SBOTextBox sboTextBox56;
        private win.controls.SBO.SBOTextBox sboTextBox53;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private win.controls.SBO.SBOComboBox sboComboBox5;
        private win.controls.SBO.SBOComboBox sboComboBox6;
        private win.controls.SBO.SBOTextBox sboTextBox22;
        private win.controls.SBO.SBOTextBox sboTextBox31;
        private win.controls.SBO.SBOTextBox sboTextBox63;
        private win.controls.SBO.SBOTextBox sboTextBox64;
        private win.controls.SBO.SBOTextBox sboTextBox68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private win.controls.SBO.SBOTextBox sboTextBox69;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private com.idh.win.controls.SBO.SBOTextBox sboTextBox73;
        private com.idh.win.controls.SBO.SBOTextBox sboTextBox74;
        private com.idh.win.controls.WR1Grid oAddGrid;
        private System.Windows.Forms.Label label78;
        private com.idh.win.controls.WR1Grid wR1Grid2;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private com.idh.win.controls.SBO.SBOTextBox sboTextBox75;
        private com.idh.win.controls.SBO.SBOTextBox sboTextBox76;
        private com.idh.win.controls.WR1Grid oDeductionsGrid;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private SBOTextBox sboTextBox89;
        private SBOTextBox sboTextBox88;
        private SBOTextBox sboTextBox87;
        private SBOTextBox sboTextBox86;
        private SBOTextBox sboTextBox85;
        private SBOTextBox sboTextBox84;
        private SBOTextBox sboTextBox83;
        private SBOTextBox sboTextBox82;
        private SBOTextBox sboTextBox81;
        private SBOComboBox sboComboBox7;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private SBOTextBox sboTextBox79;
        private SBOTextBox sboTextBox80;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label85;
        private SBOTextBox sboTextBox78;
        private SBOTextBox sboTextBox77;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private SBOTextBox sboTextBox130;
        private SBOTextBox sboTextBox129;
        private SBOTextBox sboTextBox128;
        private SBOTextBox sboTextBox127;
        private SBOTextBox sboTextBox126;
        private SBOTextBox sboTextBox125;
        private SBOTextBox sboTextBox124;
        private SBOTextBox sboTextBox123;
        private SBOTextBox sboTextBox122;
        private SBOTextBox sboTextBox121;
        private SBOTextBox sboTextBox120;
        private SBOTextBox sboTextBox119;
        private SBOTextBox sboTextBox118;
        private SBOTextBox sboTextBox117;
        private SBOTextBox sboTextBox116;
        private SBOTextBox sboTextBox114;
        private SBOTextBox sboTextBox115;
        private SBOTextBox sboTextBox112;
        private SBOTextBox sboTextBox113;
        private SBOTextBox sboTextBox109;
        private SBOTextBox sboTextBox110;
        private SBOTextBox sboTextBox111;
        private SBOTextBox sboTextBox106;
        private SBOTextBox sboTextBox107;
        private SBOTextBox sboTextBox108;
        private SBOTextBox sboTextBox103;
        private SBOTextBox sboTextBox104;
        private SBOTextBox sboTextBox105;
        private SBOTextBox sboTextBox100;
        private SBOTextBox sboTextBox101;
        private SBOTextBox sboTextBox102;
        private SBOTextBox sboTextBox99;
        private SBOTextBox sboTextBox98;
        private SBOTextBox sboTextBox97;
        private SBOTextBox sboTextBox96;
        private SBOTextBox sboTextBox95;
        private SBOTextBox sboTextBox94;
        private SBOTextBox sboTextBox93;
        private SBOTextBox sboTextBox92;
        private SBOTextBox sboTextBox91;
        private SBOTextBox sboTextBox90;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label133;
        private SBOTextBox sboTextBox158;
        private SBOTextBox sboTextBox157;
        private SBOTextBox sboTextBox156;
        private SBOTextBox sboTextBox155;
        private SBOTextBox sboTextBox152;
        private SBOTextBox sboTextBox153;
        private SBOTextBox sboTextBox154;
        private SBOTextBox sboTextBox151;
        private SBOTextBox sboTextBox150;
        private SBOTextBox sboTextBox149;
        private SBOTextBox sboTextBox148;
        private SBOTextBox sboTextBox147;
        private SBOTextBox sboTextBox146;
        private SBOTextBox sboTextBox145;
        private SBOTextBox sboTextBox144;
        private SBOTextBox sboTextBox143;
        private SBOTextBox sboTextBox142;
        private SBOTextBox sboTextBox141;
        private SBOTextBox sboTextBox140;
        private SBOTextBox sboTextBox139;
        private SBOTextBox sboTextBox138;
        private SBOTextBox sboTextBox137;
        private SBOTextBox sboTextBox136;
        private SBOTextBox sboTextBox135;
        private SBOTextBox sboTextBox134;
        private SBOTextBox sboTextBox133;
        private SBOTextBox sboTextBox132;
        private SBOTextBox sboTextBox131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.PictureBox pictureBox1;
        private SBOComboBox sboComboBox8;
        private SBOTextBox sboTextBox170;
        private SBOTextBox sboTextBox169;
        private SBOTextBox sboTextBox168;
        private SBOTextBox sboTextBox167;
        private SBOTextBox sboTextBox166;
        private SBOTextBox sboTextBox165;
        private SBOTextBox sboTextBox164;
        private SBOTextBox sboTextBox163;
        private SBOTextBox sboTextBox162;
        private SBOTextBox sboTextBox161;
        private SBOTextBox sboTextBox160;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label161;
    }
}