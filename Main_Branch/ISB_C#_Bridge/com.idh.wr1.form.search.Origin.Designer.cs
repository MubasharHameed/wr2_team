﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/23
 * Time: 01:02 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace com.idh.wr1.form.search
{
	partial class Origin
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.IDH_ORCODE = new System.Windows.Forms.TextBox();
            this.oSearchGrid = new  idh.win.controls.WR1Grid();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(11, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "EA Origin Code";
            // 
            // IDH_ORCODE
            // 
            this.IDH_ORCODE.Location = new System.Drawing.Point(98, 4);
            this.IDH_ORCODE.Name = "IDH_ORCODE";
            this.IDH_ORCODE.Size = new System.Drawing.Size(213, 20);
            this.IDH_ORCODE.TabIndex = 1;
            this.IDH_ORCODE.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // oSearchGrid
            // 
            this.oSearchGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oSearchGrid.Location = new System.Drawing.Point(11, 33);
            this.oSearchGrid.Name = "oSearchGrid";
            this.oSearchGrid.Size = new System.Drawing.Size(489, 303);
            this.oSearchGrid.TabIndex = 2;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(12, 342);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(75, 23);
            this.btn1.TabIndex = 0;
            this.btn1.Text = "Ok";
            this.btn1.UseVisualStyleBackColor = true;
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(94, 342);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(75, 23);
            this.btn2.TabIndex = 4;
            this.btn2.Text = "Cancel";
            this.btn2.UseVisualStyleBackColor = true;
            // 
            // Origin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 367);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.oSearchGrid);
            this.Controls.Add(this.IDH_ORCODE);
            this.Controls.Add(this.label1);
            this.KeyPreview = true;
            this.Name = "Origin";
            this.Text = "Origin";
            ((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private System.Windows.Forms.Button btn2;
		private System.Windows.Forms.Button btn1;
		private idh.win.controls.WR1Grid oSearchGrid;
		private System.Windows.Forms.TextBox IDH_ORCODE;
		private System.Windows.Forms.Label label1;
        
        protected void IDH_SelectedValueChanged(object sender, System.EventArgs e)
        {
            SelectedValueChanged_Internal(sender, e);
        }

        protected void IDH_TextChanged(object sender, System.EventArgs e)
        {
            TextChanged_Internal(sender, e);
        }
	}
}
