﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using com.idh.utils;

namespace com.idh.wr1 {
    public partial class PriceUpdateOption : Form {

        public DateTime FromDate {
            get {
                return com.idh.utils.Conversions.ToDateTime(IDH_FRMDAT.Text);
                //return com.idh.utils.dates.doStrToDate( IDH_FRMDAT.Text ); 
            }
            set { IDH_FRMDAT.Text = value.ToString(Dates.DATEFORMAT); }
        }

        public DateTime ToDate {
            get {
                return com.idh.utils.Conversions.ToDateTime(IDH_TODAT.Text);
                //return com.idh.utils.dates.doStrToDate(IDH_TODAT.Text); 
            }
            set { IDH_TODAT.Text = value.ToString(Dates.DATEFORMAT); }
        }

        public bool AllSites {
            get { return IDH_APPTS.Checked; }
            set { IDH_APPTS.Checked = value; }
        }

        //TUOM - 10
        public bool IDH_TUOM {
            get { return IDH_10.Checked; }
            set { 
                IDH_10.Checked = value;
                if ( value )
                    IDH_10.Enabled = true;
                }
        }

        //PUOM - 15
        public bool IDH_PUOM {
            get { return IDH_15.Checked; }
            set { 
                IDH_15.Checked = value;
                if (value)
                    IDH_15.Enabled = true;
            }
        }

        //PRUOM = 20
        public bool IDH_PRUOM {
            get { return IDH_20.Checked; }
            set { 
                IDH_20.Checked = value;
                if (value)
                    IDH_20.Enabled = true;
            }
        }

        //TChrg = 100
        public bool IDH_TChrg {
            get { return IDH_100.Checked; }
            set { 
                IDH_100.Checked = value;
                if (value)
                    IDH_100.Enabled = true;
            }
        }

        //HChrg = 200
        public bool IDH_HChrg {
            get { return IDH_200.Checked; }
            set { 
                IDH_200.Checked = value;
                if (value)
                    IDH_200.Enabled = true;
            }
        }

        //DispCst = 300
        public bool IDH_DispCst {
            get { return IDH_300.Checked; }
            set { 
                IDH_300.Checked = value;
                if (value)
                    IDH_300.Enabled = true;
            }
        }

        //HaulCst = 400
        public bool IDH_HaulCst {
            get { return IDH_400.Checked; }
            set { 
                IDH_400.Checked = value;
                if (value)
                    IDH_400.Enabled = true;
            }
        }

        //ProCst = 500
        public bool IDH_ProCst {
            get { return IDH_500.Checked; }
            set { 
                IDH_500.Checked = value;
                if (value)
                    IDH_500.Enabled = true;
            }
        }
        
        public PriceUpdateOption() {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e) {

        }

        private void btn_OK_Click(object sender, EventArgs e) {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btn_Cancel_Click(object sender, EventArgs e) {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
