﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/17
 * Time: 04:12 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace com.idh.wr1.form.search
{
	partial class WasteItem
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.oSearchGrid = new  idh.win.controls.WR1Grid();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.IDH_LEV1 = new System.Windows.Forms.ComboBox();
            this.IDH_ITMCOD = new System.Windows.Forms.TextBox();
            this.IDH_ITMNAM = new System.Windows.Forms.TextBox();
            this.IDH_GRPCOD = new System.Windows.Forms.TextBox();
            this.IDH_GRPNAM = new System.Windows.Forms.TextBox();
            this.IDH_INVENT = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // oSearchGrid
            // 
            this.oSearchGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oSearchGrid.Location = new System.Drawing.Point(5, 55);
            this.oSearchGrid.Name = "oSearchGrid";
            this.oSearchGrid.Size = new System.Drawing.Size(833, 417);
            this.oSearchGrid.TabIndex = 0;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(5, 477);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(75, 23);
            this.btn1.TabIndex = 1;
            this.btn1.Text = "Ok";
            this.btn1.UseVisualStyleBackColor = true;
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(87, 477);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(75, 23);
            this.btn2.TabIndex = 9;
            this.btn2.Text = "Cancel";
            this.btn2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 23);
            this.label1.TabIndex = 3;
            this.label1.Text = "Level1";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(5, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Item Code";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(171, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "Item Name";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(389, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 23);
            this.label4.TabIndex = 6;
            this.label4.Text = "Group Code";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(534, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 23);
            this.label5.TabIndex = 7;
            this.label5.Text = "Grp Name";
            // 
            // IDH_LEV1
            // 
            this.IDH_LEV1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IDH_LEV1.FormattingEnabled = true;
            this.IDH_LEV1.Location = new System.Drawing.Point(64, 5);
            this.IDH_LEV1.Name = "IDH_LEV1";
            this.IDH_LEV1.Size = new System.Drawing.Size(774, 21);
            this.IDH_LEV1.TabIndex = 3;
            this.IDH_LEV1.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
            // 
            // IDH_ITMCOD
            // 
            this.IDH_ITMCOD.Location = new System.Drawing.Point(64, 26);
            this.IDH_ITMCOD.Name = "IDH_ITMCOD";
            this.IDH_ITMCOD.Size = new System.Drawing.Size(98, 20);
            this.IDH_ITMCOD.TabIndex = 4;
            this.IDH_ITMCOD.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // IDH_ITMNAM
            // 
            this.IDH_ITMNAM.Location = new System.Drawing.Point(231, 26);
            this.IDH_ITMNAM.Name = "IDH_ITMNAM";
            this.IDH_ITMNAM.Size = new System.Drawing.Size(147, 20);
            this.IDH_ITMNAM.TabIndex = 5;
            this.IDH_ITMNAM.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // IDH_GRPCOD
            // 
            this.IDH_GRPCOD.Location = new System.Drawing.Point(456, 26);
            this.IDH_GRPCOD.Name = "IDH_GRPCOD";
            this.IDH_GRPCOD.Size = new System.Drawing.Size(72, 20);
            this.IDH_GRPCOD.TabIndex = 6;
            this.IDH_GRPCOD.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // IDH_GRPNAM
            // 
            this.IDH_GRPNAM.Location = new System.Drawing.Point(595, 26);
            this.IDH_GRPNAM.Name = "IDH_GRPNAM";
            this.IDH_GRPNAM.Size = new System.Drawing.Size(139, 20);
            this.IDH_GRPNAM.TabIndex = 7;
            this.IDH_GRPNAM.Validated += new System.EventHandler(this.IDH_TextChanged);
            // 
            // IDH_INVENT
            // 
            this.IDH_INVENT.FormattingEnabled = true;
            this.IDH_INVENT.Location = new System.Drawing.Point(777, 26);
            this.IDH_INVENT.Name = "IDH_INVENT";
            this.IDH_INVENT.Size = new System.Drawing.Size(61, 21);
            this.IDH_INVENT.TabIndex = 8;
            this.IDH_INVENT.SelectedValueChanged += new System.EventHandler(this.IDH_SelectedValueChanged);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(740, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 23);
            this.label6.TabIndex = 7;
            this.label6.Text = "Stock";
            // 
            // WasteItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 504);
            this.Controls.Add(this.IDH_INVENT);
            this.Controls.Add(this.IDH_GRPCOD);
            this.Controls.Add(this.IDH_GRPNAM);
            this.Controls.Add(this.IDH_ITMNAM);
            this.Controls.Add(this.IDH_ITMCOD);
            this.Controls.Add(this.IDH_LEV1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.oSearchGrid);
            this.KeyPreview = true;
            this.Name = "WasteItem";
            this.Text = "Waste Item Search";
            ((System.ComponentModel.ISupportInitialize)(this.oSearchGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox IDH_INVENT;
		private System.Windows.Forms.TextBox IDH_GRPNAM;
		private System.Windows.Forms.TextBox IDH_GRPCOD;
		private System.Windows.Forms.TextBox IDH_ITMNAM;
		private System.Windows.Forms.TextBox IDH_ITMCOD;
		private System.Windows.Forms.ComboBox IDH_LEV1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btn2;
		private System.Windows.Forms.Button btn1;
		private idh.win.controls.WR1Grid oSearchGrid;

        protected void IDH_SelectedValueChanged(object sender, System.EventArgs e)
        {
            SelectedValueChanged_Internal(sender, e);
        }

        protected void IDH_TextChanged(object sender, System.EventArgs e)
        {
            TextChanged_Internal(sender, e);
        }

        protected override void doHandleSearchValueSelected() {
            string sFrozen = (string)SearchGrid.getSelectedRowValue("FROZEN");//(string)SearchGrid.getSelectedRowValue("i.frozenFor");
            if (sFrozen == "Y") {
                string sItemCode = (string)SearchGrid.getSelectedRowValue("i.ItemCode");
                string[] oItemCode = {sItemCode};
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("The selected Item is Inactive.", "ERUSITF", oItemCode);
            } else {
                base.doHandleSearchValueSelected();
            }
        }
	}
}
