/*
 * Created by SharpDevelop.
 * User: Louis
 * Date: 2009/07/20
 * Time: 01:08 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using com.idh.controls;
using System.Windows.Forms;
using com.idh.bridge.form;
using com.idh.bridge.data;
using com.idh.win;

namespace com.idh.bridge.search
{
	/// <summary>
	/// Search control for waste items
	/// </summary>
	public class Origin: FormBridge	{

        public override void setWinForm(ref System.Windows.Forms.Form oForm) {
			base.setWinForm( ref oForm );
			doFillCombos();
		}

		public override void setSBOForm( ref IDHAddOns.idh.forms.Base oIDHForm, ref SAPbouiCOM.Form oSBOForm ){
			base.setSBOForm( ref oIDHForm, ref oSBOForm );
			doFillCombos();
		}
		
		private void doFillCombos(){
		}
		
		public void doSetGridOptions( DBOGridControl oGridControl ) {
            //oGridControl.setTableValue("[@IDH_ORIGIN]");
            oGridControl.doAddGridTable(new GridTable("@IDH_ORIGIN",null,null,false,true));
            
            oGridControl.setOrderValue("U_ORCODE");
            oGridControl.setRequiredFilter("U_OC='Y'");

            oGridControl.doAddFilterField("IDH_ORCODE", "U_ORCODE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, null, true);

            oGridControl.doAddListField("U_LOAUTHL", "Local Authority List", false, -1, null, "IDH_LOAUTHL");
            oGridControl.doAddListField("U_ORCODE", "Code", false, -1, null, "IDH_ORCODE");
            oGridControl.doAddListField("U_OC", "Origin Code", false, -1, null, "IDH_OC");
            oGridControl.doAddListField("U_AC", "Area Code", false, -1, null, "IDH_AC");
		}
	}
}

