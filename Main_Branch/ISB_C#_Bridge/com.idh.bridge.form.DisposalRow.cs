/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/06/22
 * Time: 08:17 PM
 *  
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

using com.idh.dbObjects.User;
using com.bridge;
using com.idh.bridge;
using com.idh.bridge.utils;
using com.idh.bridge.lookups;
using com.idh.controls;
using com.idh.bridge.resources;
using com.idh.dbObjects.strct;
using com.uBC.utils;
using com.idh.dbObjects;

using com.idh.win.controls.SBO;
using com.idh.form.SBO;

namespace com.idh.bridge.form {
    /// <summary>
    /// Description of com_idh_bridge_DisposalRow.
    /// </summary>
    public class DisposalRow : FormBridge {

        private IDH_DISPROW moDispRowDB;
        public IDH_DISPROW DBObject {
            get { return moDispRowDB; }
        }

        //private IDH_DISPORD moDispOrdDB;
        public IDH_DISPORD DBObjectHead {
            get { return moDisposalOrder.DBObject; }
        }

        private string msRowMode = "SO";

        private Weighbridge moWBridge;
        public Weighbridge WBridge {
            get { return moWBridge; }
        }
        private bool mbKeepWBOpen = false;
        public bool KeepWBOpen {
            get { return mbKeepWBOpen; }
            set { mbKeepWBOpen = value; }
        }

        private string msCommServerIP = null;
        public string CommServerIP {
            get { return msCommServerIP; }
            set { msCommServerIP = value; }
        }

        private int miCommServerPort = 0;
        public int CommServerPort {
            get { return miCommServerPort; }
            set { miCommServerPort = value; }
        }

        private string[] moAvailablePorts = null;
        public string[] AvailablePorts {
            get { return moAvailablePorts; }
            set { moAvailablePorts = value; }
        }

        private string msWeight;
        public string LastWeight {
            get { return msWeight; }
        }

        // Vehicle Type
        private string msVType;
        public string VType {
            get { return msVType; }
            set { msVType = value; }
        }

        ////Vehicle WR! Order Type
        //private string msVWR1Type = "";
        //public string VWR1Type {
        //    get { return msVWR1Type; }
        //    set { msVWR1Type = value; }
        //}

        private DisposalOrder moDisposalOrder;

        private Hashtable mhPriceUpdateList;

        //		private idh.data.RowData(goParent, Me, oForm, "DO" )
        //		private Calculator moCalculator;

        public DisposalRow(DisposalOrder oParent) : base(oParent) {
            moDisposalOrder = oParent;
            moDispRowDB = moDisposalOrder.DBObject.getRowDB();
            //moDispRowDB.CalledFromGUI = true;

            IDH_DISPROW.Handler_CheckNumberRequest = new DBBase.et_ExternalTriggerWithReturn(com.isb.core.popups.Registration.doRequestCheckNumber);

            moDBO = moDispRowDB;

            moDispRowDB.Handler_ChargeTotalsCalculated = new IDH_DISPROW.et_ExternalTrigger(doHandleChargeTotalsCalculated);
            moDispRowDB.Handler_CostTotalsCalculated = new IDH_DISPROW.et_ExternalTrigger(doHandleCostTotalsCalculated);
            moDispRowDB.Handler_ExternalYNOption = new IDH_DISPROW.et_ExternalYNOption(doHandlerYNOption);

            moDispRowDB.Handler_ChargePricesChanged = new IDH_DISPROW.et_ExternalTrigger(doHandleChargePricesChanged);
            moDispRowDB.Handler_CostHaulagePricesChanged = new IDH_DISPROW.et_ExternalTrigger(doHandleCostHaulagePricesChanged);
            moDispRowDB.Handler_CostProducerPricesChanged = new IDH_DISPROW.et_ExternalTrigger(doHandleCostProducerPricesChanged);
            moDispRowDB.Handler_CostTippingPricesChanged = new IDH_DISPROW.et_ExternalTrigger(doHandleCostTippingPricesChanged);

            moDispRowDB.Handler_CheckPaymentReportTrigger = new IDH_DISPROW.et_ExternalTrigger(doHandleCheckReport);
        }

        private void doHandleCheckReport(DBBase oCaller) {
            //'Now get the Check entry from the Payment record
            string sCheckEntry = (string)Config.INSTANCE.doLookupTableField("OCHO", "CheckKey", "TransRef = '" + ((IDH_DISPROW)oCaller).U_ProPay + "'");
            string sCheckReport = Config.Parameter("CHCKREP");
            if (sCheckReport != null && sCheckEntry.Length > 0) {
                Hashtable oParams = new Hashtable();
                oParams.Add("RowNum", sCheckEntry);

                if (moWinForm != null) {
                    moDisposalOrder.doReport("IDH_DISPORD", sCheckReport, oParams);
                }
            }
        }

        //public override void setWinForm(ref Form oWinForm) {
        //    if (moDisposalOrder == null) {
        //        base.setWinForm(ref oWinForm);
        //    } else {
        //        moWinForm = oWinForm;
        //        moIDHForm = null;
        //        moSBOForm = null;

        //        moFieldRelations = moDisposalOrder.FieldRelations;
        //    }
        //}

        public override void doClose() {
            if (moWBridge != null) {
                moWBridge.doClose();
            }
        }

        /**
		 * Set to Find Mode
		 */
        protected override void doSetFindMode() {
            moDisposalOrder.Mode = FormBridge.Mode_FIND;
        }

        /**
		 * Set to Ok Mode
		 */
        protected override void doSetOkMode() {
            moDisposalOrder.Mode = FormBridge.Mode_OK;
        }

        /**
		 * Set to View Mode
		 */
        protected override void doSetViewMode() {
            moDisposalOrder.Mode = FormBridge.Mode_VIEW;
        }

        /**
		 * Set to Update Mode
		 */
        protected override void doSetUpdateMode() {
            moDisposalOrder.Mode = FormBridge.Mode_UPDATE;
        }

        /**
		 * Set to Add Mode
		 */
        protected override void doSetAddMode() {
            moDisposalOrder.Mode = FormBridge.Mode_ADD;
        }

        public override void doLinkItems() {
            doAddFormDF("IDH_ROW", IDH_DISPROW._Code);
            doAddFormDF("IDH_VEHREG", IDH_DISPROW._Lorry);
            doAddFormDF("IDH_VEH", IDH_DISPROW._VehTyp);
            doAddFormDF("IDH_DRIVER", IDH_DISPROW._Driver);
            doAddFormDF("IDH_TRLReg", IDH_DISPROW._TRLReg);

            doAddFormDF("IDH_DISPAD", IDH_DISPROW._SAddress);
            doAddFormDF("IDH_BRANCH", IDH_DISPROW._Branch);
            doAddFormDF("IDH_TZONE", IDH_DISPROW._TZone);
            doAddFormDF("IDH_CONTNR", IDH_DISPROW._ContNr);
            doAddFormDF("IDH_SEALNR", IDH_DISPROW._SealNr);
            doAddFormDF("IDH_USER", IDH_DISPROW._User);
            doAddFormDF("IDH_ONCS", IDH_DISPROW._CustCs);
            doAddFormDF("IDH_TRLNM", IDH_DISPROW._TRLNM);
            doAddFormDF("IDH_JOBTTP", IDH_DISPROW._JobTp);
            doAddFormDF("IDH_ITMCOD", IDH_DISPROW._ItemCd);
            doAddFormDF("IDH_DESC", IDH_DISPROW._ItemDsc);
            doAddFormDF("IDH_WASCL1", IDH_DISPROW._WasCd);
            doAddFormDF("IDH_WASMAT", IDH_DISPROW._WasDsc);
            doAddFormDF("IDH_ALTITM", IDH_DISPROW._AltWasDsc);
            doAddFormDF("IDH_ITMGRC", IDH_DISPROW._ItmGrp);
            doAddFormDF("IDH_CUSWEI", IDH_DISPROW._CstWgt);
            doAddFormDF("IDH_RDWGT", IDH_DISPROW._RdWgt);
            doAddFormDF("IDH_CUSCHG", IDH_DISPROW._TCharge);
            doAddFormDF("IDH_CUSTOT", IDH_DISPROW._TCTotal);
            doAddFormDF("IDH_DISCNT", IDH_DISPROW._Discnt);
            doAddFormDF("IDH_DSCPRC", IDH_DISPROW._DisAmt);
            doAddFormDF("IDH_TAX", IDH_DISPROW._TaxAmt);
            doAddFormDF("IDH_TAXO", IDH_DISPROW._VtCostAmt);
            doAddFormDF("IDH_TOTAL", IDH_DISPROW._Total);
            doAddFormDF("IDH_WEI1", IDH_DISPROW._Wei1);
            doAddFormDF("IDH_WEI2", IDH_DISPROW._Wei2);
            doAddFormDF("IDH_WDT1", IDH_DISPROW._WDt1);
            doAddFormDF("IDH_WDT2", IDH_DISPROW._WDt2);
            doAddFormDF("IDH_SER1", IDH_DISPROW._Ser1);
            doAddFormDF("IDH_SER2", IDH_DISPROW._Ser2);
            doAddFormDF("IDH_ADDEX", IDH_DISPROW._AddEx);

            doAddFormDF("IDH_ADDCH", IDH_DISPROW._AddCharge);
            doAddFormDF("IDH_ADDCHR", IDH_DISPROW._AddCharge);
            doAddFormDF("IDH_TADDCH", IDH_DISPROW._TAddChrg);
            doAddFormDF("IDH_ADDCHV", IDH_DISPROW._TAddChVat);

            doAddFormDF("IDH_ADDCO", IDH_DISPROW._AddCost);
            doAddFormDF("IDH_ADDCOS", IDH_DISPROW._AddCost);
            doAddFormDF("IDH_TADDCO", IDH_DISPROW._TAddCost);
            doAddFormDF("IDH_ADDCOV", IDH_DISPROW._TAddCsVat);

            doAddFormDF("IDH_WRORD", IDH_DISPROW._WROrd);
            doAddFormDF("IDH_WRROW", IDH_DISPROW._WRRow);
            doAddFormDF("IDH_WRROW2", IDH_DISPROW._WRRow2);
            doAddFormDF("IDH_UOMC", IDH_DISPROW._UOM);
            doAddFormDF("IDH_CUSCM", IDH_DISPROW._AUOMQt);
            doAddFormDF("IDH_RSTAT", IDH_DISPROW._Status);
            doAddFormDF("IDH_PSTAT", IDH_DISPROW._PStat);
            doAddFormDF("IDH_CASHMT", IDH_DISPROW._PayMeth);
            doAddFormDF("IDH_PUOM", IDH_DISPROW._PUOM);
            doAddFormDF("IDH_TIPWEI", IDH_DISPROW._TipWgt);
            doAddFormDF("IDH_TIPCOS", IDH_DISPROW._TipCost);
            doAddFormDF("IDH_TIPTOT", IDH_DISPROW._TipTot);
            doAddFormDF("IDH_TOTCOS", IDH_DISPROW._JCost);
            doAddFormDF("IDH_RORIGI", IDH_DISPROW._Origin);
            doAddFormDF("IDH_OBLED", IDH_DISPROW._Obligated);
            doAddFormDF("IDH_WASTTN", IDH_DISPROW._WastTNN);
            doAddFormDF("IDH_HAZCN", IDH_DISPROW._ConNum);
            doAddFormDF("IDH_SITREF", IDH_DISPROW._SiteRef);
            doAddFormDF("IDH_EXTWEI", IDH_DISPROW._ExtWeig);
            doAddFormDF("IDH_EXPLDW", IDH_DISPROW._ExpLdWgt);

            doAddFormDF("IDH_WGTDED", IDH_DISPROW._WgtDed);
            doAddFormDF("IDH_VALDED", IDH_DISPROW._ValDed);
            doAddFormDF("IDH_ADJWGT", IDH_DISPROW._AdjWgt);
            doAddFormDF("IDH_ORDQTY", IDH_DISPROW._OrdWgt);
            doAddFormDF("IDH_ORDCOS", IDH_DISPROW._OrdCost);
            doAddFormDF("IDH_ORDTOT", IDH_DISPROW._OrdTot);
            doAddFormDF("IDH_PURWGT", IDH_DISPROW._ProWgt);
            doAddFormDF("IDH_PRCOST", IDH_DISPROW._PCost);
            doAddFormDF("IDH_PURTOT", IDH_DISPROW._PCTotal);

            doAddFormDF("IDH_PUOM", IDH_DISPROW._PUOM);
            doAddFormDF("IDH_PURUOM", IDH_DISPROW._ProUOM);
            doAddFormDF("IDH_UOM", IDH_DISPROW._UOM);

            doAddFormDF("IDH_BOKSTA", IDH_DISPROW._BookIn);

            doAddFormDF("IDH_USERE", IDH_DISPROW._UseWgt);
            doAddFormDF("IDH_USEAU", IDH_DISPROW._UseWgt);
            doAddFormDF("IDH_BPWGT", IDH_DISPROW._BPWgt);

            doAddFormDF("uBC_TRNCd", IDH_DISPROW._TrnCode);

            setUFFormat("IDH_SUBTOT", new DBFieldInfo(null, null, 0, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 0, 0));
            //setUFFormat("IDH_ADDCH", new DBFieldInfo(null,null, 0,SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement,0,0));
            //setUFFormat("IDH_ADDCO", new DBFieldInfo(null,null, 0,SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement,0,0));
            setUFFormat("IDH_TARWEI", new DBFieldInfo(null, null, 0, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 0, 0));
            setUFFormat("IDH_TRLTar", new DBFieldInfo(null, null, 0, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 0, 0));

            setUFFormat("IDH_TRWTE1", new DBFieldInfo(null, null, 0, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 0, 0));
            setUFFormat("IDH_TRWTE2", new DBFieldInfo(null, null, 0, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 0, 0));

            //setUFFormat("IDH_ADDCH", new DBFieldInfo(null, null, 0, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 0, 0));
            //setUFFormat("IDH_ADDCO", new DBFieldInfo(null, null, 0, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 0, 0));

            //The Currency fields
            doAddFormDF("IDH_DCOCUR", IDH_DISPROW._DispCoCc);
            doAddFormDF("IDH_CCOCUR", IDH_DISPROW._CarrCoCc);
            doAddFormDF("IDH_PCOCUR", IDH_DISPROW._PurcCoCc);
            doAddFormDF("IDH_DCGCUR", IDH_DISPROW._DispCgCc);

            doAddFormDF("IDH_FTORIG", IDH_DISPROW._FTOrig);

            doAddFormDF("IDH_SAMCHK", IDH_DISPROW._Sample);
            doAddFormDF("IDH_SAMREF", IDH_DISPROW._SampleRef);
            doAddFormDF("IDH_SAMSTA", IDH_DISPROW._SampStatus);
        }

        public override void setRadioValue(RadioButton oRadio, string sValue) {
            if (moWinForm != null) {
                if (oRadio.Name == "IDH_USEAU" || oRadio.Name == "IDH_USERE") {
                    if (sValue == "2") {
                        RadioButton oUseAUOM = (RadioButton)getWinControl("IDH_USEAU");
                        oUseAUOM.Checked = true;
                    } else {
                        RadioButton oUseWeight = (RadioButton)getWinControl("IDH_USERE");
                        oUseWeight.Checked = true;
                    }
                }
            }
        }

        /*
		 * Use this for the Windows Form
		 */
        public override void doUpdateFormData() {
            base.doUpdateFormData();

            string sBookIn = getFormDFValueAsStringNZ(IDH_DISPROW._BookIn);
            if (sBookIn.Equals("B"))
                setUFValue("IDH_BOOKIN", "Y");
            else
                setUFValue("IDH_BOOKIN", "N");

            //string sUseWgt = getFormDFValueAsStringNZ(IDH_DISPROW._UseWgt);
            //if (sUseWgt.Equals("2"))
            //    setUFValue("IDH_USEAU", "Y");
            //else
            //    setUFValue("IDH_USERE", "Y");

            //moDispRowDB.ChargeSubTotal();
            //doReadDBOBufferedValues();
        }

        /**
         * Handle the Job Type change
         */
        public void doJobTypeChange() {
            string sWasteCode = getFormDFValueAsString(IDH_DISPROW._WasCd);
            string sJobTp = getFormDFValueAsString(IDH_DISPROW._JobTp);
            moDisposalOrder.doBusinessPartnerRules(false, false, true, true);
            ////if (moLookup.doCheckCanPurchaseWB(sWasteCode) &&
            ////    moLookup.isIncomingJob(moLookup.doGetDisposalGroup(), sJobTp) )
            ////    //sJobTp.Equals(moLookup.doGetJopTypeIncoming()))
            //if (moDisposalOrder.doCheckPurchase()) {
            //    moDisposalOrder.doSwitchToPO(true, false, false);  //Set to false to block the Address Lookup of the Producer
            //} else {
            //    moDisposalOrder.doSwitchToSO(false);
            //}


            if (Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobTp)) {
                doHideLoadSheet();
                if (Config.INSTANCE.doCheckCanPurchaseWB(DBObject.U_WasCd))
                    moDisposalOrder.doSwitchToPO(true, false, false);
                else
                    moDisposalOrder.doSwitchToSO(false);
            } else {
                moDisposalOrder.doSwitchToSO(false);
                doShowLoadSheet();
            }

            doItAll();

            if (DBObject.AdditionalExpenses != null)
                DBObject.AdditionalExpenses.addAutoAdditionalCodes();

        }

        private void doHideLoadSheet() {
            //doSetVisible(oForm, "IDH_LOADSH", False, 1000)
            //doSetVisible(oForm, "349", False, 1000)
            //doSetVisible(oForm, "IDH_LOADSL", False, 1000)
            //setDFValue(oForm, msRowTable, "U_LoadSht", "")

            //setEnableItem(oForm, True, "IDH_CUSWEI")

            //If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso _
            //   Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) Then
            //    setEnableItem(oForm, True, "IDH_UOM")
            //End If
        }

        private void doShowLoadSheet() {
            //doSetVisible(oForm, "IDH_LOADSH", True, 0)
            //doSetVisible(oForm, "349", True, 0)
            //doSetVisible(oForm, "IDH_LOADSL", True, 0)

            //Dim sLoadSht As String = getDFValue(oForm, msRowTable, "U_LoadSht")
            //If sLoadSht.Length > 0 Then
            //    setEnableItem(oForm, False, "IDH_CUSWEI")
            //    setEnableItem(oForm, False, "IDH_UOM")
            //Else
            //    setEnableItem(oForm, True, "IDH_CUSWEI")

            //    If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso _
            //        Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) Then
            //        setEnableItem(oForm, True, "IDH_UOM")
            //    End If
            //End If
        }


        /**
		 * Form Field changed
		 */
        public override bool doPostUpdateDBFieldFromUser(string sControlName, string sValue) {
            if (sControlName.Equals("IDH_JOBTTP")) {
                doJobTypeChange();
            } else if (sControlName.Equals("IDH_WEI1")) {
                moDispRowDB.U_Ser1 = "NONE";
                moDispRowDB.U_WDt1 = DateTime.Today;

                //doCalcWBWeightTotal();
                moDisposalOrder.doSetReportCheckBoxes();
            } else if (sControlName.Equals("IDH_WEI2")) {
                moDispRowDB.U_Ser2 = "NONE";
                moDispRowDB.U_WDt2 = DateTime.Today;

                //doCalcWBWeightTotal();
                //moDisposalOrder.doSetReportCheckBoxes();
                moDisposalOrder.doSetReportCheckBoxes();
            } else if (sControlName.Equals("IDH_ADDEX")) {
                moDispRowDB.doCalculateTotals(IDH_DISPROW.TOTAL_CALCULATE);
                //doReadDBOBufferedValues();

                //doUpdateFormChangedData();
            } else if (sControlName == "IDH_WEIBRG") {
                string sID = getUFValue(sControlName);
                doConfigWeighBridge(sID);
                moDisposalOrder.SetWeighBridgeNoSwitch(sID);
            } else if (sControlName.Equals("IDH_RDWGT")) {
                //double dQty = moDispRowDB.U_RdWgt;
                //if (dQty > 0) {
                //    string sUOM = Config.INSTANCE.getWeighBridgeUOM(); //getCurrentUOM();
                //    Config.INSTANCE.doCheckMaxWeight("DO", dQty, sUOM); //"kg");
                //}

                moDisposalOrder.doSetReportCheckBoxes();
                //doItAll();
                moDisposalOrder.doSetReportCheckBoxes();
            } else if (sControlName.Equals("IDH_CUSWEI")) {
                //moDispRowDB.doCalculateTotals();
                //doReadDBOBufferedValues();

                //doUpdateFormChangedData();
                moDisposalOrder.doSetReportCheckBoxes();
            } else if (sControlName.Equals("IDH_TIPWEI")) {
                //moDispRowDB.doCalcMarketingTotals();
                //doReadDBOBufferedValues();

                //doUpdateFormChangedData();
            } else if (sControlName.Equals("IDH_ORDQTY")) {
                //moDispRowDB.doCalcMarketingTotals();
                //doReadDBOBufferedValues();

                //doUpdateFormChangedData();
            } else if (sControlName.Equals("IDH_PURWGT")) {
                //moDispRowDB.doCalcMarketingTotals();
                //doReadDBOBufferedValues();

                doUpdateFormChangedData();
            } else if (sControlName.Equals("IDH_TIPCOS")) {
                //int iPrcSet = getFormDFValueAsInt( IDH_DISPROW._MANPRC );
                //iPrcSet = iPrcSet | Config.MASK_TIPCST;
                //setFormDFValue(IDH_DISPROW._MANPRC, iPrcSet);

                //moDispRowDB.doCalcMarketingTotals();
                //doReadDBOBufferedValues();

                //doUpdateFormChangedData();

                //Ask To Save to SIP
                if (moDisposalOrder.wasFormFieldSetByUser("IDH_TIPCOS"))
                    doRequestCIPSIPUpdate("ASKSIP", FixedValues.en_PriceTypes.DispCst);
            } else if (sControlName.Equals("IDH_ORDCOS")) {
                //int iPrcSet = getFormDFValueAsInt(IDH_DISPROW._MANPRC);
                //iPrcSet = iPrcSet | Config.MASK_HAULCST;
                //setFormDFValue(IDH_DISPROW._MANPRC, iPrcSet);

                //moDispRowDB.doCalcMarketingTotals();
                //doReadDBOBufferedValues();

                //doUpdateFormChangedData();

                //Ask To Save to SIP
                if (moDisposalOrder.wasFormFieldSetByUser("IDH_ORDCOS"))
                    doRequestCIPSIPUpdate("ASKSIP", FixedValues.en_PriceTypes.HaulCst);
            } else if (sControlName.Equals("IDH_PRCOST")) {
                //int iPrcSet = getFormDFValueAsInt(IDH_DISPROW._MANPRC);
                //iPrcSet = iPrcSet | Config.MASK_PRODUCERCST;
                //setFormDFValue(IDH_DISPROW._MANPRC, iPrcSet);

                //moDispRowDB.doCalcMarketingTotals();
                //doReadDBOBufferedValues();

                //doUpdateFormChangedData();

                //Ask To Save to SIP
                if (moDisposalOrder.wasFormFieldSetByUser("IDH_PRCOST"))
                    doRequestCIPSIPUpdate("ASKSIP", FixedValues.en_PriceTypes.ProCst);
            } else if (sControlName.Equals("IDH_CUSCHG")) {
                //int iPrcSet = getFormDFValueAsInt( IDH_DISPROW._MANPRC );
                //iPrcSet = iPrcSet | Config.MASK_TIPCHRG;
                //setFormDFValue( IDH_DISPROW._MANPRC, iPrcSet );

                //moDispRowDB.doCalcMarketingTotals();
                //doReadDBOBufferedValues();

                //doUpdateFormChangedData();

                //Ask To Save to CIP
                if (moDisposalOrder.wasFormFieldSetByUser("IDH_CUSCHG"))
                    doRequestCIPSIPUpdate("ASKCIP", FixedValues.en_PriceTypes.TChrg);
            } else if (sControlName.Equals("IDH_DISCNT")) {
                //moDispRowDB.doCalcMarketingTotals('P');
                //doReadDBOBufferedValues();

                //doUpdateFormChangedData();
            } else if (sControlName.Equals("IDH_DSCPRC")) {
                //moDispRowDB.doCalcMarketingTotals('A');
                //doReadDBOBufferedValues();

                //doUpdateFormChangedData();
            } else if (sControlName.Equals("IDH_BOOKDT")) {
                //if (moDisposalOrder.Mode != Mode_FIND)
                //    doItAll();
            } else if (sControlName.Equals("IDH_CUSCM")) {
                //if (moDisposalOrder.Mode != Mode_FIND)
                //    doItAll();
            } else if (sControlName.Equals("IDH_WGTDED")) {
                //if (moDisposalOrder.Mode != Mode_FIND)
                //    doItAll();
            } else if (sControlName.Equals("IDH_VALDED")) {
                //if (moDisposalOrder.Mode != Mode_FIND)
                //    doItAll();
            } else if (sControlName.Equals("IDH_ADJWGT")) {
                //if (moDisposalOrder.Mode != Mode_FIND)
                //    doItAll();
            } else if (sControlName.Equals("IDH_UOM")) {
                //if (moDisposalOrder.Mode != Mode_FIND)
                //    doItAll();

                //Ask To Save to CIP
                if (moDisposalOrder.wasFormFieldSetByUser("IDH_UOM"))
                    doRequestCIPSIPUpdate("ASKCIP", FixedValues.en_PriceTypes.TUOM);
            }
            //else if ( sFieldName.Equals("IDH_UOMC")) {
            //    if (moDisposalOrder.Mode != Mode_FIND)
            //        doItAll(false);
            //}
            else if (sControlName.Equals("IDH_PUOM")) {
                //if (moDisposalOrder.Mode != Mode_FIND)
                //    doItAll(false);

                //Ask To Save to SIP
                if (moDisposalOrder.wasFormFieldSetByUser("IDH_PUOM"))
                    doRequestCIPSIPUpdate("ASKSIP", FixedValues.en_PriceTypes.PUOM);
            } else if (sControlName.Equals("IDH_PURUOM")) {
                //if (moDisposalOrder.Mode != Mode_FIND)
                //    doItAll(false);

                //'Ask To Save to SIP
                if (moDisposalOrder.wasFormFieldSetByUser("IDH_PURUOM"))
                    doRequestCIPSIPUpdate("ASKSIP", FixedValues.en_PriceTypes.PRUOM);
            } else if (sControlName.Equals("IDH_BRANCH")) {
                if (moDisposalOrder.Mode != Mode_FIND)
                    doGetTZones();
            }

            return base.doPostUpdateDBFieldFromUser(sControlName, sValue);
        }

        private void doRequestCIPSIPUpdate(string sMessId, FixedValues.en_PriceTypes oPriceType) {
            //Skip this if the User can not update the SIP or CIP
            if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_CIP_SIP_FROM_DO_WO)) {
                if (Config.INSTANCE.getParameterAsBool("ITMPUOK", true)) {
                    if (mhPriceUpdateList == null) {
                        mhPriceUpdateList = new Hashtable();
                        mhPriceUpdateList.Add(oPriceType, sMessId);
                    } else {
                        if (!mhPriceUpdateList.Contains(oPriceType)) {
                            mhPriceUpdateList.Add(oPriceType, sMessId);
                        }
                    }
                } else {
                    ArrayList oPriceTypes = new ArrayList();
                    oPriceTypes.Add(oPriceType);
                    doActualCIPSIPUpdateRequest(sMessId, oPriceTypes);
                }
            }
        }

        public bool doRequestCIPSIPUpdateOnOK() {
            if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_CIP_SIP_FROM_DO_WO)) {
                if (Config.INSTANCE.getParameterAsBool("ITMPUOK", true)) {
                    if (mhPriceUpdateList != null && mhPriceUpdateList.Count > 0) {
                        ArrayList oaPriceTypeList = new ArrayList();

                        foreach (FixedValues.en_PriceTypes oPriceType in mhPriceUpdateList.Keys) {
                            oaPriceTypeList.Add(oPriceType);
                        }
                        if (oaPriceTypeList.Count > 0) {
                            doActualCIPSIPUpdateRequest("ASKMULTI", oaPriceTypeList);
                        }
                        mhPriceUpdateList.Clear();
                        mhPriceUpdateList = null;
                        return true;
                    }
                }
            }
            return false;
        }

        private void doActualCIPSIPUpdateRequest(string sMessId, ArrayList oPriceTypes) {
            //Skip this if the User can not update the SIP or CIP
            if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_CIP_SIP_FROM_DO_WO)) {
                //FixedValues.en_PriceTypes oPriceType;
                string[] sParams = null;
                if (oPriceTypes == null || oPriceTypes.Count == 0)
                    return;
                else if (oPriceTypes.Count == 1) {
                    sParams = new String[] { FixedValues.getPriceTypeName((FixedValues.en_PriceTypes)oPriceTypes[0]) };
                }

                if (Messages.INSTANCE.doResourceMessageYN(sMessId, sParams, 1) == 1) {

                    com.idh.wr1.PriceUpdateOption oPriceOption = new com.idh.wr1.PriceUpdateOption();

                    string sToDate = Config.Parameter("IPDFED");
                    DateTime oToDate;
                    if (sToDate == null || sToDate.Length == 0) {
                        sToDate = Dates.doDateToSBODateStr(DateTime.Now);
                        oToDate = DateTime.Now;
                    } else {
                        oToDate = Dates.doStrToDate(sToDate);
                    }

                    string sFromDate = Config.Parameter("IPDFST");
                    DateTime oFromDate;

                    if (sFromDate == null || sFromDate.Length == 0 || sFromDate.ToUpper().Equals("NOW")) {
                        //sFromDate = com.idh.utils.dates.doDateToSBODateStr(DateTime.Now);
                        oFromDate = DateTime.Now;
                    } else if (sFromDate.ToUpper().Equals("DOC")) {
                        //sFromDate = getFormDFValueAsString(IDH_DISPROW._BDate);
                        oFromDate = getFormDFValueAsDateTime(IDH_DISPROW._BDate);
                    } else {
                        oFromDate = Dates.doStrToDate(sFromDate);
                    }

                    oPriceOption.FromDate = oFromDate; // 'com.idh.utils.dates.doDateToSBODateStr(DateTime.Now) 'oPriceLUP.U_StDate_AsString
                    oPriceOption.ToDate = oToDate; //'oPriceLUP.U_EnDate_AsString

                    bool bDoAddress = true;

                    foreach (FixedValues.en_PriceTypes oPriceType in oPriceTypes) {
                        if (oPriceType == FixedValues.en_PriceTypes.TChrg ||
                             oPriceType == FixedValues.en_PriceTypes.HChrg ||
                            oPriceType == FixedValues.en_PriceTypes.TUOM) {
                            if (bDoAddress && moDispRowDB.CIP.U_StAddr != null && moDispRowDB.CIP.U_StAddr.Length > 0) {
                                bDoAddress = false;
                            }

                            oPriceOption.IDH_TChrg = oPriceType == FixedValues.en_PriceTypes.TChrg;
                            oPriceOption.IDH_HChrg = oPriceType == FixedValues.en_PriceTypes.HChrg;
                            oPriceOption.IDH_TUOM = oPriceType == FixedValues.en_PriceTypes.TUOM;

                        } else if (oPriceType == FixedValues.en_PriceTypes.DispCst ||
                                    oPriceType == FixedValues.en_PriceTypes.TUOM) {

                            if (bDoAddress && moDispRowDB.SIPT.U_StAddr != null && moDispRowDB.SIPT.U_StAddr.Length > 0) {
                                bDoAddress = false;
                            }

                            oPriceOption.IDH_DispCst = oPriceType == FixedValues.en_PriceTypes.DispCst;
                            if (!oPriceOption.IDH_TUOM) {
                                oPriceOption.IDH_TUOM = oPriceType == FixedValues.en_PriceTypes.TUOM;
                            }

                        } else if (oPriceType == FixedValues.en_PriceTypes.HaulCst) {

                            if (bDoAddress && moDispRowDB.SIPH.U_StAddr != null && moDispRowDB.SIPH.U_StAddr.Length > 0) {
                                bDoAddress = false;
                            }

                            oPriceOption.IDH_HaulCst = oPriceType == FixedValues.en_PriceTypes.HaulCst;

                        } else if (oPriceType == FixedValues.en_PriceTypes.ProCst ||
                                oPriceType == FixedValues.en_PriceTypes.PRUOM) {

                            if (bDoAddress && moDispRowDB.SIPP.U_StAddr != null && moDispRowDB.SIPP.U_StAddr.Length > 0) {
                                bDoAddress = false;
                            }

                            oPriceOption.IDH_ProCst = oPriceType == FixedValues.en_PriceTypes.ProCst;
                            oPriceOption.IDH_PRUOM = oPriceType == FixedValues.en_PriceTypes.PRUOM;
                        }
                    }

                    oPriceOption.AllSites = bDoAddress;

                    DialogResult oResult = oPriceOption.ShowDialog();
                    if (oResult == DialogResult.OK) {
                        doAddUpdatePrices(oPriceOption.FromDate, oPriceOption.ToDate, oPriceOption.AllSites,
                            oPriceTypes.Count > 1, oPriceOption.IDH_TChrg, oPriceOption.IDH_HChrg, oPriceOption.IDH_TUOM,
                            oPriceOption.IDH_DispCst, oPriceOption.IDH_PUOM, oPriceOption.IDH_HaulCst, oPriceOption.IDH_ProCst, oPriceOption.IDH_PRUOM);
                    } else {
                        //if (oPriceType == idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TChrg || oPriceType == idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HChrg || oPriceType == idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM) {
                        //    doGetChargePrices(oPForm.SBOParentForm, true, true);
                        //} else if (oPriceType == idh.forms.fr2.PriceUpdateOption.en_PriceTypes.DispCst || oPriceType == idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM) {
                        //    doGetTipCostPrices(oPForm.SBOParentForm);
                        //} else if (oPriceType == idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HaulCst) {
                        //    doGetHaulageCostPrices(oPForm.SBOParentForm);
                        //} else if (oPriceType == idh.forms.fr2.PriceUpdateOption.en_PriceTypes.ProCst || oPriceType == idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PRUOM) {
                        //    doGetProdCostPrices(oPForm.SBOParentForm);
                        //}
                    }
                }
            }
        }

        public bool doAddUpdatePrices(DateTime dFromDate, DateTime dToDate, bool bAllSites, bool bIsCombined,
            bool bTChrg, bool bHChrg, bool bTUOM, bool bDispCst, bool bPUOM, bool bHaulCst, bool bProCst, bool bPRUOM) {

            //System.DateTime dFromDate = com.idh.utils.dates.doStrToDate(sFromDate);
            //System.DateTime dToDate = com.idh.utils.dates.doStrToDate(sToDate);

            //string sBranch = "";
            //string sJobType = "";
            //string sZip = "";
            bool bResult = true;
            bool bDoUpdate = false;

            string sMessId = null;
            if (bIsCombined) {
                sMessId = "ASKMULOV";
            }

            if (bTChrg || bHChrg || bTUOM) {
                if (!bIsCombined) {
                    sMessId = "ASKCIPOV";
                }

                double dTPrice = getFormDFValueAsDouble(IDH_DISPROW._TCharge);
                double dHPrice = getFormDFValueAsDouble(IDH_DISPROW._CusChr);
                string sUOM = getFormDFValueAsString(IDH_DISPROW._UOM);

                string sTCalc = null;
                string sHCalc = null;
                double dFromWeight = -9999;
                double dToWeight = -9999;

                IDH_CSITPR oCIP = moDispRowDB.CIP;
                if (!oCIP.LookedUp) {
                    moDispRowDB.doLookupChargePrices();
                }

                if (oCIP.Count > 0 && (oCIP.Prices != null) && oCIP.Prices.Found) {
                    if (bDoUpdate || Messages.INSTANCE.doResourceMessageOC(sMessId, new string[] { "entry" }, 1) == 1) {
                        oCIP.U_EnDate = dFromDate.AddDays(-1);
                        bDoUpdate = true;
                    } else {
                        if (!bIsCombined) {
                            return false;
                        }
                    }

                    dFromWeight = oCIP.U_FTon;
                    dToWeight = oCIP.U_TTon;

                    if (!bTUOM)
                        sUOM = oCIP.U_UOM;

                    if (!bTChrg)
                        dTPrice = oCIP.U_TipTon;

                    if (!bHChrg)
                        dHPrice = oCIP.U_Haulge;

                    sTCalc = oCIP.U_ChrgCal;
                    sHCalc = oCIP.U_HaulCal;
                }

                oCIP.doAddEmptyRow(true, true, false);

                string sCardCode = getFormDFValueAsString(IDH_DISPROW._CustCd);
                string sCardName = getFormDFValueAsString(IDH_DISPROW._CustNm);
                string sItemCode = getFormDFValueAsString(IDH_DISPROW._ItemCd);
                string sItemName = getFormDFValueAsString(IDH_DISPROW._ItemDsc);
                string sItemGrp = getFormDFValueAsString(IDH_DISPROW._ItmGrp);
                string sWastCd = getFormDFValueAsString(IDH_DISPROW._WasCd);
                string sWastName = getFormDFValueAsString(IDH_DISPROW._WasDsc);
                string sDocDate = getFormDFValueAsString(IDH_JOBSHD._RDate);

                string sAddress = moDisposalOrder.getFormDFValueAsString(IDH_DISPORD._SAddress);

                System.DateTime dDocDate = Dates.doStrToDate(sDocDate);

                oCIP.doApplyDefaults();

                oCIP.Code = "";
                oCIP.Name = "";

                oCIP.U_CustCd = sCardCode;
                oCIP.U_ItemCd = sItemCode;
                oCIP.U_ItemDs = sItemName;

                oCIP.U_StDate = dFromDate;
                oCIP.U_EnDate = dToDate;

                oCIP.U_WasteCd = sWastCd;
                oCIP.U_WasteDs = sWastName;

                if (bAllSites) {
                    oCIP.U_StAddr = "";
                } else {
                    oCIP.U_StAddr = sAddress;
                }

                oCIP.U_JobTp = "";
                //sJobType
                oCIP.U_WR1ORD = "DO";

                oCIP.U_TipTon = dTPrice;
                if (sTCalc != null) {
                    oCIP.U_ChrgCal = sTCalc;
                    oCIP.U_ChrgCal = sTCalc; ;
                    oCIP.U_HaulCal = sHCalc; ;
                }

                oCIP.U_Haulge = dHPrice;
                if (sHCalc != null) {
                    oCIP.U_HaulCal = sHCalc;
                }

                oCIP.U_UOM = sUOM;

                oCIP.U_Branch = "";
                //sBranch
                oCIP.U_ZpCd = "";
                //sZip

                if (oCIP.doProcessData()) {
                    int iPrcSet = getFormDFValueAsInt(IDH_DISPROW._MANPRC);

                    iPrcSet = iPrcSet ^ Config.MASK_TIPCHRG;
                    iPrcSet = iPrcSet ^ Config.MASK_HAULCHRG;

                    setFormDFValue(IDH_DISPROW._MANPRC, iPrcSet);
                }
            }

            if (bDispCst || bPUOM) {
                if (!bIsCombined) {
                    sMessId = "ASKSIPOV";
                }

                IDH_SUITPR oSIPT = moDispRowDB.SIPT;
                double dTPrice = -9999;
                double dHPrice = -9999;
                string sUOM = null;
                bool bIsNew = true;

                if (oSIPT.LookedUp == false) {
                    //moDispRowDB.doGetTipCostPrice(false, false);
                    moDispRowDB.doLookupTipCostPrices();
                }

                if (oSIPT.Count > 0 && (oSIPT.Prices != null) && oSIPT.Prices.Found) {
                    if (bDoUpdate || Messages.INSTANCE.doResourceMessageOC(sMessId, new string[] { "entry" }, 1) == 1) {
                        oSIPT.U_EnDate = dFromDate.AddDays(-1);
                        bDoUpdate = true;
                        bIsNew = false;
                    } else {
                        if (!bIsCombined) {
                            return false;
                        }
                    }
                }

                if (bIsNew || bDispCst) {
                    dTPrice = getFormDFValueAsDouble(IDH_DISPROW._TipCost);
                }

                if (bIsNew || bPUOM) {
                    sUOM = getFormDFValueAsString(IDH_DISPROW._PUOM);
                }

                string sSuppCd = getFormDFValueAsString(IDH_DISPROW._Tip);
                if (doUpdateSIPDataBuffer(sSuppCd, dFromDate, dToDate, bAllSites, oSIPT, dTPrice, dHPrice, sUOM, bIsNew)) {
                    int iPrcSet = getFormDFValueAsInt(IDH_DISPROW._MANPRC);

                    iPrcSet = iPrcSet ^ Config.MASK_TIPCST;

                    setFormDFValue(IDH_DISPROW._MANPRC, iPrcSet);
                }
            }

            if (bHaulCst) {
                if (bIsCombined == false) {
                    sMessId = "ASKSIPOV";
                }

                IDH_SUITPR oSIPH = moDispRowDB.SIPH;
                double dTPrice = -9999;
                double dHPrice = -9999;
                string sUOM = null;
                bool bIsNew = true;

                if (oSIPH.LookedUp == false) {
                    //moDispRowDB.doGetHaulageCostPrice(false, false);
                    moDispRowDB.doLookupHaulageCostPrice(); ;
                }

                if (oSIPH.Count > 0 && (oSIPH.Prices != null) && oSIPH.Prices.Found) {
                    if (bDoUpdate || Messages.INSTANCE.doResourceMessageOC(sMessId, new string[] { "entry" }, 1) == 1) {
                        oSIPH.U_EnDate = dFromDate.AddDays(-1);
                        bDoUpdate = true;
                        bIsNew = false;
                    } else {
                        return false;
                    }
                }

                if (bIsNew || bHaulCst) {
                    dHPrice = getFormDFValueAsDouble(IDH_DISPROW._OrdCost);
                }

                //If bIsCombined = False OrElse oPriceForm.IDH_PUOM Then
                sUOM = getFormDFValueAsString(IDH_DISPROW._PUOM);
                //End If

                string sSuppCd = getFormDFValueAsString(IDH_DISPROW._CarrCd);
                if (doUpdateSIPDataBuffer(sSuppCd, dFromDate, dToDate, bAllSites, oSIPH, dTPrice, dHPrice, sUOM, bIsNew)) {
                    int iPrcSet = getFormDFValueAsInt(IDH_DISPROW._MANPRC);

                    iPrcSet = iPrcSet ^ Config.MASK_HAULCST;

                    setFormDFValue(IDH_DISPROW._MANPRC, iPrcSet);
                }
            }

            if (bProCst || bPRUOM) {
                if (bIsCombined == false) {
                    sMessId = "ASKSIPOV";
                }

                IDH_SUITPR oSIPP = moDispRowDB.SIPP;
                //Dim dPPrice As Double = getFormDFValue(oParentForm, IDH_JOBSHD._PCost)
                //Dim sUOM As String = getFormDFValue(oParentForm, IDH_JOBSHD._ProUOM)

                double dPPrice = -9999;
                double dHPrice = -9999;
                string sUOM = null;
                bool bIsNew = true;

                if (oSIPP.LookedUp == false) {
                    //moDispRowDB.doGetProducerCostPrice(false, false);
                    moDispRowDB.doLookupProducerCostPrices();
                }

                if (oSIPP.Count > 0 && (oSIPP.Prices != null) && oSIPP.Prices.Found) {
                    if (bDoUpdate || Messages.INSTANCE.doResourceMessageOC(sMessId, new string[] { "entry" }, 1) == 1) {
                        oSIPP.U_EnDate = dFromDate.AddDays(-1);
                        bDoUpdate = true;
                        bIsNew = false;
                    } else {
                        if (!bIsCombined) {
                            return false;
                        }
                    }
                }

                if (bIsNew || bProCst) {
                    dPPrice = getFormDFValueAsDouble(IDH_DISPROW._PCost);
                }

                if (bIsNew || bPRUOM) {
                    sUOM = getFormDFValueAsString(IDH_DISPROW._ProUOM);
                }

                string sSuppCd = getFormDFValueAsString(IDH_DISPROW._ProCd);
                if (doUpdateSIPDataBuffer(sSuppCd, dFromDate, dToDate, bAllSites, oSIPP, dPPrice, dHPrice, sUOM, bIsNew)) {
                    int iPrcSet = getFormDFValueAsInt(IDH_DISPROW._MANPRC);

                    iPrcSet = iPrcSet ^ Config.MASK_PRODUCERCST;

                    setFormDFValue(IDH_DISPROW._MANPRC, iPrcSet);
                }
            }
            return bResult;
        }

        private bool doUpdateSIPDataBuffer(string sSuppCd, DateTime dFromDate, DateTime dToDate, bool bAllSites, IDH_SUITPR oSIP, double dTPrice, double dHPrice, string sUOM, bool bIsNew) {

            if (sUOM == null && dTPrice == -9999 && dHPrice == -9999) {
                return true;
            }

            string sBranch = "";
            string sJobType = "";
            string sZip = "";

            if (oSIP.Count > 0 && (oSIP.Prices != null) && oSIP.Prices.Found) {
                sBranch = oSIP.U_Branch;
                sJobType = oSIP.U_JobTp;
                sZip = oSIP.U_ZpCd;
                if (sUOM == null || sUOM.Length == 0) {
                    sUOM = oSIP.U_UOM;
                }
            }

            oSIP.doAddEmptyRow(true, true, false);

            string sCustCd = getFormDFValueAsString(IDH_JOBSHD._CustCd);
            string sCardName = getFormDFValueAsString(IDH_JOBSHD._CustNm);
            string sItemCode = getFormDFValueAsString(IDH_JOBSHD._ItemCd);
            string sItemName = getFormDFValueAsString(IDH_JOBSHD._ItemDsc);
            string sItemGrp = getFormDFValueAsString(IDH_JOBSHD._ItmGrp);
            string sWastCd = getFormDFValueAsString(IDH_JOBSHD._WasCd);
            string sWastName = getFormDFValueAsString(IDH_JOBSHD._WasDsc);
            DateTime dDocDate = getFormDFValueAsDateTime(IDH_JOBSHD._BDate);
            //Dim sUOM As String = getFormDFValue(oForm, IDH_JOBSHD._PUOM)

            //System.DateTime dDocDate = com.idh.utils.dates.doStrToDate(sDocDate);
            //System.DateTime dFromDate = com.idh.utils.dates.doStrToDate(sFromDate);
            //System.DateTime dToDate = com.idh.utils.dates.doStrToDate(sToDate);

            string sAddress = moDisposalOrder.getFormDFValueAsString(IDH_DISPORD._SAddress);

            oSIP.doApplyDefaults();

            oSIP.Code = "";
            oSIP.Name = "";

            oSIP.U_CardCd = sSuppCd;
            oSIP.U_BP2CD = sCustCd;
            oSIP.U_ItemCd = sItemCode;
            oSIP.U_ItemDs = sItemName;

            oSIP.U_StDate = dFromDate;
            oSIP.U_EnDate = dToDate;

            if (dTPrice != -9999) {
                oSIP.U_TipTon = dTPrice;
            }

            if (dHPrice != -9999) {
                oSIP.U_Haulge = dHPrice;
            }

            oSIP.U_WasteCd = sWastCd;
            oSIP.U_WasteDs = sWastName;

            if (bAllSites) {
                oSIP.U_StAddr = "";
            } else {
                oSIP.U_StAddr = sAddress;
            }

            oSIP.U_JobTp = "";
            oSIP.U_WR1ORD = "DO";
            oSIP.U_UOM = sUOM;
            oSIP.U_Branch = "";
            oSIP.U_ZpCd = "";

            return oSIP.doProcessData();
        }

        #region vehicledata
        ///**
        // * Add the Vehicle
        // */
        //public void doCheckAndAddVehicle()
        //{
        //    string sWONR = getFormDFValueAsStringNZ( IDH_DISPROW._WROrd);
        //    string sWORow = getFormDFValueAsStringNZ(IDH_DISPROW._WRRow);
        //    //if (sWONR.Length > 0 && sWORow.Length > 0)
        //    //{
        //    //    return;
        //    //}

        //    string sCustCd = moDisposalOrder.getFormDFValueAsStringNZ( IDH_DISPORD._CardCd );
        //    string sCarrCd = moDisposalOrder.getFormDFValueAsStringNZ( IDH_DISPORD._CCardCd );

        //    string sReg = getFormDFValueAsString( IDH_DISPROW._Lorry);
        //    string sVehCode = getFormDFValueAsString( IDH_DISPROW._LorryCd);

        //    string sWhere;

        //    if ( !Config.ParameterAsBool("VMUSENEW", false) ) {
        //        if ((sVehCode != null) && sVehCode.Length > 0) {
        //            sWhere = "Code = '" + sVehCode + "' ";
        //            if (Config.INSTANCE.getParameterAsBool("DOVMUC", true))
        //                sWhere += " And U_CCrdCd = '" + sCustCd + "' ";
        //        } else {
        //            sWhere = "U_VehReg = '" + sReg + "' " + " And U_CCCrdCd = '" + sCarrCd + "' " + " And (U_WR1ORD Is NULL Or U_WR1ORD='' OR U_WR1ORD LIKE 'DO%')";
        //            if (Config.INSTANCE.getParameterAsBool("DOVMUC", true))
        //                sWhere += " And U_CCrdCd = '" + sCustCd + "' ";
        //        }

        //        IDH_VEHMAS oVehicle = new IDH_VEHMAS();
        //        bool bDoSave = false; 
        //        if ( oVehicle.getData( sWhere, null ) == 0 )
        //        {
        //            bDoSave = Config.INSTANCE.getParameterAsBool("MASVCR", false);
        //            if ( bDoSave )  {
        //                oVehicle.U_VehReg = sReg;
        //                oVehicle.U_VehDesc = getFormDFValueAsString(IDH_DISPROW._VehTyp);
        //                oVehicle.U_Driver = getFormDFValueAsString(IDH_DISPROW._Driver);
        //                oVehicle.U_TRLReg = getFormDFValueAsString(IDH_DISPROW._TRLReg);
        //                oVehicle.U_TRLNM = getFormDFValueAsString(IDH_DISPROW._TRLNM);
        //                oVehicle.U_WR1ORD = msVWR1Type;

        //                //Customer
        //                oVehicle.U_CCrdCd = sCustCd;
        //                oVehicle.U_CName = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._CardNM);
        //                oVehicle.U_CAddress = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._Address);

        //                //Carrier
        //                oVehicle.U_CCCrdCd = sCarrCd;
        //                oVehicle.U_CCName = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._CCardNM);

        //                //Disposal Site
        //                oVehicle.U_SCrdCd = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._SCardCd);
        //                oVehicle.U_SName = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._SCardNM);
        //                oVehicle.U_SAddress = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._SAddress);
        //                oVehicle.U_Origin = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._Origin);

        //                oVehicle.U_ItemCd = getFormDFValueAsString(IDH_DISPROW._ItemCd);
        //                oVehicle.U_ItemDsc = getFormDFValueAsString(IDH_DISPROW._ItemDsc);
        //                oVehicle.U_WasCd = getFormDFValueAsString(IDH_DISPROW._WasCd);
        //                oVehicle.U_WasDsc = getFormDFValueAsString(IDH_DISPROW._WasDsc);

        //                //*** The tare weights
        //                //oVehicle.U_Tarre = getUFValue(oForm, "IDH_TARWEI");
        //                //oVehicle.U_TRLTar = getUFValue(oForm, "IDH_TRLTar");

        //                string sItemGroup = getFormDFValueAsString(IDH_DISPROW._ItmGrp);
        //                //if (sItemGroup != null && sItemGroup.Length > 0)
        //                //    oVehicle.U_ItmGrp = sItemGroup;
        //                //else
        //                    oVehicle.U_ItmGrp = Config.Parameter("IGR-LOR");

        //                if (msVType!=null && msVType.Length > 0 )
        //                    oVehicle.U_VehType = msVType;
        //                else
        //                    oVehicle.U_VehType = "S";

        //                oVehicle.U_WFStat = "ReqAdd";

        //                if (oVehicle.doAddDataRow())
        //                {
        //                    setFormDFValue(IDH_DISPROW._LorryCd, oVehicle.Code);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            bDoSave = Config.INSTANCE.getParameterAsBool("MASVUP", false);
        //            if ( bDoSave ) 
        //            {
        //                oVehicle.U_WFStat = "ReqUpdate";
        //            }
        //        }

        //        if (bDoSave) {
        //            //'CUSTOMER
        //            oVehicle.U_CCrdCd = sCustCd;
        //            oVehicle.U_CName = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._CardNM);
        //            oVehicle.U_CAddress = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._Address);


        //            //'INCOMMING DISPOSAL
        //            //If getUFValue(oForm, "IDH_JOBTTP").Equals(idh.const.Lookup.INSTANCE.doGetJopTypeIncomming()) Then
        //            //    ''CARRIER
        //            //    '---> WC.Cus - WASTE CARRIER (Cus)
        //            //    '---> WC.Sup - WASTE CARRIER (Sup)
        //            //                    oAuditObj.setFieldValue("U_CCCrdCd", getDFValue(oForm, "@IDH_DISPORD", "U_CCardCd"))
        //            oVehicle.U_CCCrdCd = sCarrCd;
        //            oVehicle.U_CCName = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._CCardNM);

        //            //Else
        //            //    '---> WC.Cus - WASTE CARRIER (Cus)
        //            //    '---> WC.Sup - WASTE CARRIER (Sup)
        //            //    utTable.UserFields().Fields().Item("U_CCCrdCd").Value = getDFValue(oForm, "@IDH_DISPORD", "U_SCardCd")
        //            //    utTable.UserFields().Fields().Item("U_CCName").Value = getDFValue(oForm, "@IDH_DISPORD", "U_SCardNm")
        //            //End If

        //            //Disposal Site
        //            oVehicle.U_SCrdCd = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._SCardCd);
        //            oVehicle.U_SName = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._SCardNM);
        //            oVehicle.U_SAddress = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._SAddress);

        //            oVehicle.U_Origin = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._Origin);

        //            oVehicle.U_ItemCd = getFormDFValueAsString(IDH_DISPROW._ItemCd);
        //            oVehicle.U_ItemDsc = getFormDFValueAsString(IDH_DISPROW._ItemDsc);
        //            oVehicle.U_WasCd = getFormDFValueAsString(IDH_DISPROW._WasCd);
        //            oVehicle.U_WasDsc = getFormDFValueAsString(IDH_DISPROW._WasDsc);

        //            //*** The tare weights
        //            oVehicle.U_Tarre = com.idh.utils.Conversions.ToInt(getUFValue("IDH_TARWEI"));
        //            oVehicle.U_TRLTar = com.idh.utils.Conversions.ToInt(getUFValue("IDH_TRLTar"));

        //            //ONLY UPDATE IF THE 2nd WEIGHT CAPTURED
        //            double dWei2 = 0;
        //            dWei2 = getFormDFValueAsDouble(IDH_DISPROW._Wei2);
        //            if (dWei2 > 0) {
        //                string sMarkAct = null;
        //                if (getUFValue("IDH_FOC").Equals("Y")) {
        //                    sMarkAct = "DOFOC";
        //                } else if (getUFValue("IDH_DOORD").Equals("Y")) {
        //                    sMarkAct = "DOORD";
        //                } else if (getUFValue("IDH_DOARI").Equals("Y")) {
        //                    sMarkAct = "DOARI";
        //                } else if (getUFValue("IDH_AINV").Equals("Y")) {
        //                    sMarkAct = "DOAINV";
        //                } else if (getUFValue("IDH_DOARIP").Equals("Y")) {
        //                    sMarkAct = "DOARIP";
        //                } else {
        //                    sMarkAct = "";
        //                }
        //                oVehicle.U_MarkAc = sMarkAct;
        //            }

        //            oVehicle.doUpdateDataRow();
        //        }
        //    } else {
        //        if ((sVehCode != null) && sVehCode.Length > 0) {
        //            sWhere = "Code = '" + sVehCode + "' ";
        //            //if (Config.INSTANCE.getParameterAsBool("DOVMUC", true))
        //            //    sWhere += " And U_CCrdCd = '" + sCustCd + "' ";
        //        } else {
        //            sWhere = "U_VehReg = '" + sReg + "' ";
        //            //+" And U_CCCrdCd = '" + sCarrCd + "' " + " And (U_WR1ORD Is NULL Or U_WR1ORD='' OR U_WR1ORD LIKE 'DO%')";
        //            //if (Config.INSTANCE.getParameterAsBool("DOVMUC", true))
        //            //    sWhere += " And U_CCrdCd = '" + sCustCd + "' ";
        //        }

        //        IDH_VEHMAS2 oVehicle = new IDH_VEHMAS2();
        //        bool bDoSave = false;
        //        if (oVehicle.getData(sWhere, null) == 0) {
        //            bDoSave = Config.INSTANCE.getParameterAsBool("MASVCR", false);
        //            if (bDoSave) {
        //                oVehicle.U_VehReg = sReg;
        //                oVehicle.U_VehDesc = getFormDFValueAsString(IDH_DISPROW._VehTyp);
        //                oVehicle.U_Driver = getFormDFValueAsString(IDH_DISPROW._Driver);
        //                //oVehicle.U_TRLReg = getFormDFValueAsString(IDH_DISPROW._TRLReg);
        //                //oVehicle.U_TRLNM = getFormDFValueAsString(IDH_DISPROW._TRLNM);
        //                oVehicle.U_WR1ORD = msVWR1Type;

        //                //Customer
        //                oVehicle.U_CCrdCd = sCustCd;
        //                //oVehicle.U_CName = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._CardNM);
        //                ///oVehicle.U_CAddress = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._Address);

        //                //Carrier
        //                oVehicle.U_CCCrdCd = sCarrCd;
        //                //oVehicle.U_CCName = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._CCardNM);

        //                //Disposal Site
        //                //oVehicle.U_SCrdCd = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._SCardCd);
        //                //oVehicle.U_SName = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._SCardNM);
        //                //oVehicle.U_SAddress = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._SAddress);
        //                //oVehicle.U_Origin = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._Origin);

        //                //oVehicle.U_ItemCd = getFormDFValueAsString(IDH_DISPROW._ItemCd);
        //                //oVehicle.U_ItemDsc = getFormDFValueAsString(IDH_DISPROW._ItemDsc);
        //                //oVehicle.U_WasCd = getFormDFValueAsString(IDH_DISPROW._WasCd);
        //                //oVehicle.U_WasDsc = getFormDFValueAsString(IDH_DISPROW._WasDsc);

        //                //*** The tare weights
        //                //oVehicle.U_Tarre = getUFValue(oForm, "IDH_TARWEI");
        //                //oVehicle.U_TRLTar = getUFValue(oForm, "IDH_TRLTar");

        //                //string sItemGroup = getFormDFValueAsString(IDH_DISPROW._ItmGrp);
        //                //if (sItemGroup != null && sItemGroup.Length > 0)
        //                //    oVehicle.U_ItmGrp = sItemGroup;
        //                //else
        //                //    oVehicle.U_ItmGrp = Config.Parameter("IGR-LOR");

        //                DateTime oDate = DateTime.Now;
        //                oVehicle.U_CREAOn = oDate;
        //                oVehicle.U_LAMEND = oDate;

        //                if (msVType != null && msVType.Length > 0)
        //                    oVehicle.U_VehT = msVType;
        //                else
        //                    oVehicle.U_VehT = "S";

        //                oVehicle.U_WFStat = "ReqAdd";

        //                if (oVehicle.doAddDataRow()) {
        //                    setFormDFValue(IDH_DISPROW._LorryCd, oVehicle.Code);
        //                }
        //            }
        //        } else {
        //            bDoSave = Config.INSTANCE.getParameterAsBool("MASVUP", false);
        //            if (bDoSave) {
        //                oVehicle.U_WFStat = "ReqUpdate";

        //                DateTime oDate = DateTime.Now;
        //                oVehicle.U_LAMEND = oDate;

        //                //'CUSTOMER
        //                oVehicle.U_CCrdCd = sCustCd;
        //                //oVehicle.U_CName = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._CardNM);
        //                //oVehicle.U_CAddress = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._Address);


        //                //'INCOMMING DISPOSAL
        //                //If getUFValue(oForm, "IDH_JOBTTP").Equals(idh.const.Lookup.INSTANCE.doGetJopTypeIncomming()) Then
        //                //    ''CARRIER
        //                //    '---> WC.Cus - WASTE CARRIER (Cus)
        //                //    '---> WC.Sup - WASTE CARRIER (Sup)
        //                //                    oAuditObj.setFieldValue("U_CCCrdCd", getDFValue(oForm, "@IDH_DISPORD", "U_CCardCd"))
        //                oVehicle.U_CCCrdCd = sCarrCd;
        //                //oVehicle.U_CCName = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._CCardNM);

        //                //Else
        //                //    '---> WC.Cus - WASTE CARRIER (Cus)
        //                //    '---> WC.Sup - WASTE CARRIER (Sup)
        //                //    utTable.UserFields().Fields().Item("U_CCCrdCd").Value = getDFValue(oForm, "@IDH_DISPORD", "U_SCardCd")
        //                //    utTable.UserFields().Fields().Item("U_CCName").Value = getDFValue(oForm, "@IDH_DISPORD", "U_SCardNm")
        //                //End If

        //                //Disposal Site
        //                //oVehicle.U_SCrdCd = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._SCardCd);
        //                //oVehicle.U_SName = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._SCardNM);
        //                //oVehicle.U_SAddress = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._SAddress);

        //                //oVehicle.U_Origin = moDisposalOrder.getFormDFValueAsStringNZ(IDH_DISPORD._Origin);

        //                //oVehicle.U_ItemCd = getFormDFValueAsString(IDH_DISPROW._ItemCd);
        //                //oVehicle.U_ItemDsc = getFormDFValueAsString(IDH_DISPROW._ItemDsc);
        //                //oVehicle.U_WasCd = getFormDFValueAsString(IDH_DISPROW._WasCd);
        //                //oVehicle.U_WasDsc = getFormDFValueAsString(IDH_DISPROW._WasDsc);

        //                //*** The tare weights
        //                oVehicle.U_Tarre = com.idh.utils.Conversions.ToInt(getUFValue("IDH_TARWEI"));
        //                //oVehicle.U_TRLTar = com.idh.utils.Conversions.ToInt(getUFValue("IDH_TRLTar"));

        //                //ONLY UPDATE IF THE 2nd WEIGHT CAPTURED
        //                //double dWei2 = 0;
        //                //dWei2 = getFormDFValueAsDouble(IDH_DISPROW._Wei2);
        //                //if (dWei2 > 0) {
        //                //    string sMarkAct = null;
        //                //    if (getUFValue("IDH_FOC").Equals("Y")) {
        //                //        sMarkAct = "DOFOC";
        //                //    } else if (getUFValue("IDH_DOORD").Equals("Y")) {
        //                //        sMarkAct = "DOORD";
        //                //    } else if (getUFValue("IDH_DOARI").Equals("Y")) {
        //                //        sMarkAct = "DOARI";
        //                //    } else if (getUFValue("IDH_AINV").Equals("Y")) {
        //                //        sMarkAct = "DOAINV";
        //                //    } else if (getUFValue("IDH_DOARIP").Equals("Y")) {
        //                //        sMarkAct = "DOARIP";
        //                //    } else {
        //                //        sMarkAct = "";
        //                //    }
        //                //    oVehicle.U_MarkAc = sMarkAct;
        //                //}

        //                oVehicle.U_WR1ORD = msVWR1Type;

        //                oVehicle.doUpdateDataRow();
        //            }
        //        }
        //    }
        //}

        /**
		 * Returns true the rest of the data needs setting
		 */
        public bool doSetVehicleReg(string sCustomer, string sVehicleReg, string sVehicleCode, string sVehicleType, string sDriver, string sJobTp) {
            string sWOrd = (string)getFormDFValue(IDH_DISPROW._WROrd);
            string sWRow = (string)getFormDFValue(IDH_DISPROW._WRRow);

            if (!String.IsNullOrWhiteSpace(sWRow) && !sWRow.Equals("-1")) {
                setFormDFValue(IDH_DISPROW._Lorry, sVehicleReg);
                setFormDFValue(IDH_DISPROW._LorryCd, sVehicleCode);

                doUpdateTareFields();

                //double[] dTares;
                //if ( sVehicleCode != null && sVehicleCode.Length > 0 )
                //    dTares = moLookup.doGetLorryTareWeightsByCode(sVehicleCode);
                //else
                //    dTares = moLookup.doGetLorryTareWeightsByRegistration(sVehicleReg);

                //setUFValue("IDH_TARWEI", dTares[0]);
                //setUFValue("IDH_TRLTar", dTares[1]);

                return false;
            }

            if (doCheckForSecondWeigh(sWOrd, "", sVehicleReg, sVehicleCode, sJobTp)) {
                //if ( doCheckForSecondWeigh(sWOrd, sCustomer, sVehicleReg, sVehicleCode, sJobTp) ) {
                return false;
            } else {
                string sJobNr = moDisposalOrder.getFormDFValueAsString(IDH_DISPORD._Code);
                if (sJobNr == null || sJobNr.Length == 0) {
                    moDisposalOrder.Mode = Mode_ADD;
                }

                setFormDFValue(IDH_DISPROW._Lorry, sVehicleReg);
                setFormDFValue(IDH_DISPROW._LorryCd, sVehicleCode);
                setFormDFValue(IDH_DISPROW._VehTyp, sVehicleType);
                setFormDFValue(IDH_DISPROW._Driver, sDriver);
                setFormDFValue(IDH_DISPROW._JobTp, sJobTp);

                doUpdateTareFields();

                //double[] dTares;
                //if (sVehicleCode != null && sVehicleCode.Length > 0)
                //    dTares = moLookup.doGetLorryTareWeightsByCode(sVehicleCode);
                //else
                //    dTares = moLookup.doGetLorryTareWeightsByRegistration(sVehicleReg);

                //setUFValue("IDH_TARWEI", dTares[0]);
                //setUFValue("IDH_TRLTar", dTares[1]);

                string sTitle = Translation.getTranslated("IDH_DISPORD", "#TITLE", null, "Disposal Order") +
                                        " - " +
                                        Translation.getTranslatedWord("First Weigh");
                if (moSBOForm != null)
                    moSBOForm.Title = sTitle;
                else if (moWinForm != null)
                    moWinForm.Text = sTitle;

                return true;
            }
        }

        //Find the record containing the Reg, Jobtype and Order number that does not have a second weigh
        //If no record found go into a add mode
        //Returns True if it is a second weigh Job else False
        protected bool doCheckForSecondWeigh(string sOrder, string sCustomer, string sVehicleReg, string sVehicleCode, string sJobTp) {
            DataRecords oRecords = null;
            try {
                if (moDisposalOrder.Mode == FormBridge.Mode_FIND) {
                    string sQry = null;

                    if (sVehicleReg.Length == 0 &&
                        sOrder.Length == 0 &&
                        sCustomer.Length == 0) {
                        return false;
                    }

                    sQry = "SELECT Top 1 l." + IDH_DISPROW._JobNr + ", l." + IDH_DISPROW._Code +
                            " from [@IDH_DISPROW] l, [@IDH_DISPORD] h " +
                            " Where h." + IDH_DISPORD._Code + " = l." + IDH_DISPROW._JobNr;
                    if (sOrder.Length > 0) {
                        sQry = sQry + " AND l." + IDH_DISPROW._JobNr + " = '" + sOrder + "'";
                    } else {
                        if (sVehicleCode.Length > 0) {
                            sQry = sQry + " AND l." + IDH_DISPROW._LorryCd + " = '" + sVehicleCode + "'";
                        } else if (sVehicleReg.Length > 0) {
                            sQry = sQry + " AND l." + IDH_DISPROW._Lorry + " = '" + sVehicleReg + "'";
                        } else {
                            return false;
                        }

                        sQry = sQry + " AND ((l." + IDH_DISPROW._Wei2 + "= 0 AND l." + IDH_DISPROW._Wei1 + "!= 0) " +
                            " Or (l." + IDH_DISPROW._Wei1 + "= 0 AND l." + IDH_DISPROW._Wei2 + "!= 0)) " +
                            " AND " + " " + IDH_DISPROW._CstWgt + " = 0 " +
                            " AND l." + IDH_DISPROW._Status + "!= '" + FixedValues.getStatusOrdered() + '\'' +
                            " AND l." + IDH_DISPROW._Status + "!= '" + FixedValues.getStatusInvoiced() + '\'' +
                            " AND l." + IDH_DISPROW._Status + "!= '" + FixedValues.getStatusFoc() + '\'' +
                            " AND l." + IDH_DISPROW._RowSta + "!= '" + FixedValues.getStatusDeleted() + '\'';

                        if (moLookup.getParameterAsBool("DOFVPJ") == true) {
                            if (sJobTp.Length > 0) {
                                sQry = sQry + " AND l." + IDH_DISPROW._JobTp + "= '" + sJobTp + "'";
                            }
                        }

                        if (sCustomer.Length > 0) {
                            sCustomer = sCustomer.Replace('*', '%');
                            sQry = sQry + " AND h." + IDH_DISPORD._CardCd + " LIKE '" + sCustomer + "%'";
                        }
                    }

                    //sQry = sQry + " Order by CAST(l." + IDH_DISPROW._Code + " As Numeric) DESC";
                    sQry = sQry + " Order by l." + IDH_DISPROW._Code + " DESC";

                    oRecords = DataHandler.INSTANCE.doBufferedSelectQuery("doFindInternal", sQry);

                    if (oRecords != null && oRecords.RecordCount > 0) {
                        //This indicates a second weigh and will load the first weigh job
                        string sWOrder = oRecords.getValueAsString(IDH_DISPROW._JobNr);
                        string sWRow = oRecords.getValueAsString(IDH_DISPROW._Code);
                        moDisposalOrder.doLoad2ndWeigh(sWOrder, sWRow);
                        return true;
                    } else {
                        if (sOrder.Length != 0) {
                            //DataHandler.INSTANCE.doError("User: No Records were found: " + sOrder);
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("No Records were found: " + sOrder, "ERUSNRFR", new String[] { sOrder });
                        }
                        return false;
                    }
                }
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Exception finding vehicle.");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFVEH", null);
            } finally {
                DataHandler.INSTANCE.doReleaseDataRecords(ref oRecords);
            }
            return false;
        }
        #endregion

        #region doTheWeights
        public void doConfigWeighBridge(string sId) {
            DataHandler.INSTANCE.doDebug("DisposalRow.doConfigWeighBridge", "Bridge New Id - " + sId, 20);

            if (sId.Equals("-1")) {
                if (moWBridge != null) {
                    moWBridge.doClose();
                    moWBridge = null;
                }
                return;
            }

            bool bDoConfigure = false;

            string sPort = Config.Parameter("WBPORT" + sId);
            int iPort = -1;
            try {
                iPort = int.Parse(sPort);
            } catch (Exception) { }

            DataHandler.INSTANCE.doDebug("DisposalRow.doConfigWeighBridge", "Before Weighbridge set 001.11: IP[" + msCommServerIP + "] PORT[" + miCommServerPort + "] moWBridge [" + (moWBridge == null ? "NULL" : "SET - ID:" + moWBridge.getWBId() + " PRT:" + moWBridge.Port) + " NEW[ ID:" + sId + " PRT: " + iPort + "]", 20);

            if (moWBridge == null) {
                if (msCommServerIP != null && msCommServerIP.Length > 0 && miCommServerPort > 0) {
                    moWBridge = new Weighbridge(sId, msCommServerIP, miCommServerPort);
                } else {
                    moWBridge = new Weighbridge(sId);
                }
                bDoConfigure = true;
            } else if (!moWBridge.getWBId().Equals(sId) || moWBridge.Port != iPort) {
                DataHandler.INSTANCE.doDebug("DisposalRow.doConfigWeighBridge", "Bridge Id changed. OldId - " + moWBridge.getWBId() + ", New - " + sId, 20);
                if (!String.IsNullOrWhiteSpace(msCommServerIP) && miCommServerPort > 0) {
                    try {
                        moWBridge.doOpenSocket(msCommServerIP, miCommServerPort);
                    } catch (Exception e1) {
                        DataHandler.INSTANCE.doDebug("DisposalRow.doConfigWeighBridge", "Open Socket Exception", 20);
                        throw (e1);
                    }
                } else {
                    try {
                        if (moWBridge.WBSocket != null)
                            moWBridge.WBSocket.doCloseConnectoin(true);
                        moWBridge.WBSocket = null;

                        moWBridge.doClose();
                        //moWBridge.setWBId(sId);
                    } catch (Exception) {
                        DataHandler.INSTANCE.doDebug("DisposalRow.doConfigWeighBridge", "Close Comm Connection", 20);
                        //                        throw (e1);
                    }
                }
                moWBridge.setWBId(sId);
                bDoConfigure = true;
            }

            DataHandler.INSTANCE.doDebug("DisposalRow.doConfigWeighBridge", "Before Weighbridge set 002.11: moWBridge [" + (moWBridge == null ? "NULL" : "SET") + "] SOCKET[" + (moWBridge == null || moWBridge.WBSocket == null ? "NULL" : "SET") + "] - doConfig: " + (bDoConfigure ? "TRUE" : "FALSE"), 20);

            //doSetWeighBridgeLogo(oForm);

            if (bDoConfigure) {
                string sReadOnceCmd = Config.Parameter("WBFR" + sId);
                string sReadContinuesCmd = Config.Parameter("WBCR" + sId);

                int iWBFRSS = int.Parse(Config.Parameter("WBFRSS" + sId));
                int iWBFRSL = int.Parse(Config.Parameter("WBFRSL" + sId));

                int iWBFRWS = int.Parse(Config.Parameter("WBFRWS" + sId));
                int iWBFRWL = int.Parse(Config.Parameter("WBFRWL" + sId));

                int iWBCRWS = int.Parse(Config.Parameter("WBCRWS" + sId));
                int iWBCRWL = int.Parse(Config.Parameter("WBCRWL" + sId));

                string sBaud = Config.Parameter("WBBAUD" + sId);
                string sDataBit = Config.Parameter("WBDBIT" + sId);

                //N-None, O-Odd, E-Even, M-Mark
                string sParity = Config.Parameter("WBPARI" + sId);
                string sStop = Config.Parameter("WBSTOP" + sId);
                string sFlowControl = Config.Parameter("WBFLOW" + sId);
                string sInMode = Config.Parameter("WBIMOD" + sId);
                string sWBDelay = Config.Parameter("WBDELA" + sId);

                string sWBPattern = Config.Parameter("WBPAT" + sId);
                int iWBPatternOffset = com.idh.utils.Conversions.ToInt(Config.Parameter("WBPATO" + sId));

                if (sParity == null || sParity.Length == 0) {
                    sParity = "N";
                }

                int iWBDELA;
                if (sWBDelay == null || sWBDelay.Length == 0)
                    iWBDELA = 0;
                else
                    iWBDELA = int.Parse(sWBDelay);

                string sInfo =
                    "PortId: " + moWBridge.getWBId() + "; " +
                    "Port [WBPORT]: " + iPort + "; " +
                    "Baud [WBBAUD]: " + int.Parse(sBaud) + "; " +
                    "DataBit [WBDBIT]: " + int.Parse(sDataBit) + "; " +
                    "Parity [WBPARI]: " + sParity[0] + "; " +
                    "StopBit [WBSTOP]: " + int.Parse(sStop) + "; " +
                    "FlowControl [WBFLOW]: " + sFlowControl + "; " +
                    "InMode [WBIMOD]: " + sInMode + "; " +
                    "ReadDelay [WBDELA]: " + iWBDELA + "; " +
                    "ContinuesCmd [WBCR]: " + sReadContinuesCmd + "; " +
                    "C_WeightStart [WBCRWS]: " + iWBCRWS + "; " +
                    "C_WeightLen [WBCRWL]: " + iWBCRWL + "; " +
                    "ReadOnceCmd [WBFR]: " + sReadOnceCmd + "; " +
                    "O_WeightStart [WBFRWS]: " + iWBFRWS + "; " +
                    "O_WeightLen [WBFRSL]: " + iWBFRWL + "; " +
                    "O_SerialStart [WBFRSS]: " + iWBFRSS + "; " +
                    "O_SerialLength [WBFRSL]: " + iWBCRWL + "; " +
                    "O_Pattern [WBPAT]: " + sWBPattern + "; " +
                    "O_PatternOffset [WBPATO]: " + iWBPatternOffset + "; ";
                DataHandler.INSTANCE.doDebug("DisposalRow.doConfigWeighBridge", "Comm parameters: " + sInfo, 20);

                moWBridge.doConfigPort(int.Parse(sPort), int.Parse(sBaud), int.Parse(sDataBit), sParity[0], int.Parse(sStop), sFlowControl);
                moWBridge.setInMode(sInMode);
                moWBridge.setReadDelay(iWBDELA);
                moWBridge.doConfigReadCointinues(sReadContinuesCmd, iWBCRWS, iWBCRWL);
                moWBridge.doConfigReadOnce(sReadOnceCmd, iWBFRWS, iWBFRWL, iWBFRSS, iWBFRSL, sWBPattern, iWBPatternOffset);
            }
        }

        public void doReadWeight(string sReadType) {
            if (moWBridge == null) {
                return;
            }
            double dWeight = 0;
            try {

                DataHandler.INSTANCE.doDebug("DisposalRow.doReadWeight", "Keep WB Open: " + (mbKeepWBOpen ? "TRUE" : "FALSE"), 20);
                if (!moWBridge.IsOpen()) {
                    moWBridge.doOpen();
                }

                if (sReadType.Equals("CR")) {

                    DataHandler.INSTANCE.doDebug("DisposalRow.doReadWeight", "DoContinuesRead", 20);

                    if (!moWBridge.doReadContinues()) {
                        //DataHandler.INSTANCE.doError("System: " + sReadType + " - " + moWBridge.getError(), sReadType + ": " + moWBridge.getError());
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError(sReadType + " - " + moWBridge.getError(), "ERSYGEN", new String[] { idh.bridge.Translation.getTranslatedWord(sReadType) + " - " + idh.bridge.Translation.getTranslatedWord(moWBridge.getError()) });
                    }
                    dWeight = moWBridge.getWeight();
                    msWeight = dWeight.ToString();

                    setUFValue("IDH_WEIG", msWeight);
                } else if (sReadType.Equals("ACC1")) {

                    DataHandler.INSTANCE.doDebug("DisposalRow.doReadWeight", "DoReadOnce", 20);

                    if (!moWBridge.doReadOnce()) {
                        //DataHandler.INSTANCE.doError("System: " + sReadType + " - " + moWBridge.getError(), sReadType + ": " + moWBridge.getError());
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError(sReadType + " - " + moWBridge.getError(), "ERSYGEN", new String[] { idh.bridge.Translation.getTranslatedWord(sReadType) + " - " + idh.bridge.Translation.getTranslatedWord(moWBridge.getError()) });
                    }
                    dWeight = moWBridge.getWeight();
                    msWeight = dWeight.ToString();

                    setFormDFValue(IDH_DISPROW._Ser1, moWBridge.getSerial());
                    setFormDFValue(IDH_DISPROW._Wei1, dWeight);
                    setFormDFValue(IDH_DISPROW._WDt1, DateTime.Today);

                    setUFValue("IDH_WEIG", msWeight);

                    doCalcWBWeightTotal();
                    moDisposalOrder.Mode = Mode_UPDATE; //doSetUpdate();
                } else if (sReadType.Equals("ACC2")) {

                    DataHandler.INSTANCE.doDebug("DisposalRow.doReadWeight", "DoReadOnce", 20);

                    if (!moWBridge.doReadOnce()) {
                        //DataHandler.INSTANCE.doError("System: " + sReadType + " - " + moWBridge.getError(), sReadType + ": " + moWBridge.getError());
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError(sReadType + " - " + moWBridge.getError(), "ERSYGEN", new String[] { idh.bridge.Translation.getTranslatedWord(sReadType) + " - " + idh.bridge.Translation.getTranslatedWord(moWBridge.getError()) });
                    }
                    dWeight = moWBridge.getWeight();
                    msWeight = dWeight.ToString();

                    setFormDFValue(IDH_DISPROW._Ser2, moWBridge.getSerial());
                    setFormDFValue(IDH_DISPROW._Wei2, dWeight);
                    setFormDFValue(IDH_DISPROW._WDt2, DateTime.Today);

                    setUFValue("IDH_WEIG", msWeight);

                    doCalcWBWeightTotal();
                    moDisposalOrder.Mode = Mode_UPDATE; //doSetUpdate();            
                } else if (sReadType.Equals("ACCB")) {

                    DataHandler.INSTANCE.doDebug("DisposalRow.doReadWeight", "DoReadOnce", 20);

                    if (!moWBridge.doReadOnce()) {
                        //DataHandler.INSTANCE.doError("System: " + sReadType + " - " + moWBridge.getError(), sReadType + ": " + moWBridge.getError());
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError(sReadType + " - " + moWBridge.getError(), "ERSYGEN", new String[] { idh.bridge.Translation.getTranslatedWord(sReadType) + " - " + idh.bridge.Translation.getTranslatedWord(moWBridge.getError()) });
                    }
                    dWeight = moWBridge.getWeight();
                    msWeight = dWeight.ToString();

                    setUFValue("IDH_SERB", moWBridge.getSerial());
                    setUFValue("IDH_WEIG", msWeight);
                    setUFValue("IDH_WEIB", msWeight);
                    setUFValue("IDH_WDTB", DateTime.Today);
                }
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: WeighBridge Error - " + ex.ToString(), "WeighBridge Error");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXWGTB", null);
            } finally {
            }

            if (!mbKeepWBOpen)
                moWBridge.doClose();
        }

        public void doUseTareWei1(double dTar1, double dTar2) {
            setFormDFValue(IDH_DISPROW._Ser1, "NONE");
            setFormDFValue(IDH_DISPROW._Wei1, dTar1 + dTar2);
            setFormDFValue(IDH_DISPROW._WDt1, DateTime.Today);

            doCalcWBWeightTotal();
            moDisposalOrder.Mode = Mode_UPDATE; //doSetUpdate();
        }

        public void doUseTareWei2(double dTar1, double dTar2) {
            setFormDFValue(IDH_DISPROW._Ser2, "NONE");
            setFormDFValue(IDH_DISPROW._Wei2, dTar1 + dTar2);
            setFormDFValue(IDH_DISPROW._WDt2, DateTime.Today);

            doCalcWBWeightTotal();
            moDisposalOrder.Mode = Mode_UPDATE; //doSetUpdate();
        }
        #endregion

        #region fromSBO

        /*
		 * This is called after the Form has been updated from the DataSource
		 */
        protected override void doDataLoaded() {
            //			if ( Mode == FormBridge.Mode_OK || Mode == FormBridge.Mode_UPDATE ) {
            //				string sPONr = getFormDFValueAsString("U_ProPO");
            //				if ((sPONr != null) && sPONr.Length > 0 && sPONr.Equals("ERROR") == false) {
            //					moDisposalOrder.doSwitchToOrdered(com.idh.bridge.lookups.FixedValues.getStatusOrdered());
            //				}
            //				else {
            //					string sStatus = getFormDFValueAsString("U_Status");
            //					if (com.idh.bridge.lookups.FixedValues.canUpdate(sStatus)) {
            //						moDisposalOrder.doSwitchToUpdate();
            //					}
            //					else {
            //						moDisposalOrder.doSwitchToOrdered(sStatus);
            //					}
            //				}
            //			}
            double dFirstWeigh = getFormDFValueAsDouble(IDH_DISPROW._Wei1);
            double dSecondWeigh = getFormDFValueAsDouble(IDH_DISPROW._Wei2);
            if (dFirstWeigh > 0 && dSecondWeigh == 0) {
                moDisposalOrder.doSetAsSecondWeigh(true);
            }

            moDisposalOrder.doFillAlternativeDescription(moDispRowDB.U_WasCd);

            doUpdateTareFields();
        }

        public void doUpdateTareFields() {
            object[] oTares;
            string sVehicleReg = getFormDFValueAsString(IDH_DISPROW._Lorry);
            string sVehicleCode = getFormDFValueAsString(IDH_DISPROW._LorryCd);
            if (string.IsNullOrWhiteSpace(sVehicleCode))
                oTares = moLookup.doGetLorryTareWeightsByCode(sVehicleCode);
            else
                oTares = moLookup.doGetLorryTareWeightsByRegistration(sVehicleReg);

            //setUFValue("IDH_TARWEI", dTares[0]);
            //setUFValue("IDH_TRLTar", dTares[1]);
            if (oTares != null) {
                setUFValue("IDH_TARWEI", com.idh.utils.Conversions.ToDouble(oTares[0]));
                setUFValue("IDH_TRLTar", com.idh.utils.Conversions.ToDouble(oTares[1]));
                setUFValue("IDH_TRWTE1", (oTares[2] == null ? "" : oTares[2]));
                setUFValue("IDH_TRWTE2", (oTares[3] == null ? "" : oTares[3]));
            } else {
                setUFValue("IDH_TARWEI", 0);
                setUFValue("IDH_TRLTar", 0);
                setUFValue("IDH_TRWTE1", "");
                setUFValue("IDH_TRWTE2", "");
            }
        }

        //public void doSwitchAccountingCheckBox(string sID, bool bDoSetValue, bool bDoSetStatusValue){
        //    if (sID == null) {
        //        return;
        //    }

        //    string sAction = "";
        //    string sPStatus = null;
        //    string sStatus = null;

        //    if (bDoSetStatusValue == true) {
        //        string sDoMDDocs = Config.Parameter("MDAGED");
        //        if (sDoMDDocs.ToUpper().Equals("TRUE")) {
        //            sAction = "D"; //Do
        //        }
        //        else {
        //            sAction = "R"; //Request
        //        }
        //    }

        //    if (sID.Equals("IDH_DOPO")) {
        //        if (bDoSetValue) {
        //            try {
        //                setUFValue(sID, "Y");
        //            }
        //            catch (Exception) {
        //            }
        //        }
        //        if (bDoSetStatusValue == true) {
        //            if (getUFValue("IDH_DOPO").Equals("Y")) {
        //                if (sAction == "D") {
        //                    sPStatus = com.idh.bridge.lookups.FixedValues.getDoOrderStatus();
        //                    sStatus = com.idh.bridge.lookups.FixedValues.getDoFocStatus();
        //                }
        //                else {
        //                    sPStatus = com.idh.bridge.lookups.FixedValues.getReqOrderStatus();
        //                    sStatus = com.idh.bridge.lookups.FixedValues.getReqFocStatus();
        //                }

        //                setFormDFValue(IDH_DISPROW._PStat, sPStatus);
        //                setFormDFValue(IDH_DISPROW._Status, sStatus);
        //                setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethFoc());
        //                setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusFree());
        //                setFormDFValue(IDH_DISPROW._CCNum, "");
        //                setFormDFValue(IDH_DISPROW._CCType, "0");
        //                setFormDFValue(IDH_DISPROW._CCStat, "");
        //            }
        //            else {
        //                setFormDFValue(IDH_DISPROW._PStat, com.idh.bridge.lookups.FixedValues.getStatusOpen());
        //            }
        //        }
        //        else {
        //            setUFValue("IDH_FOC", "Y");
        //        }
        //    }
        //    else if (sID.Equals("IDH_DOORD") || 
        //             sID.Equals("IDH_DOARI") || 
        //             sID.Equals("IDH_AINV") || 
        //             sID.Equals("IDH_DOARIP") || 
        //             sID.Equals("IDH_FOC") || 
        //             sID.Equals("IDH")) {
        //        setUFValue("IDH_DOPO", "N");
        //        if ( !sID.Equals("IDH_FOC")) {
        //            setUFValue("IDH_FOC", "N" );
        //        }

        //        if ( !sID.Equals("IDH_DOORD")) {
        //            setUFValue("IDH_DOORD", "N" );
        //        }

        //        if ( !sID.Equals("IDH_DOARI") ) {
        //            setUFValue("IDH_DOARI", "N" );
        //        }

        //        if ( !sID.Equals("IDH_DOARIP") ) {
        //            setUFValue("IDH_DOARIP", "N" );
        //        }

        //        if ( !sID.Equals("IDH_AINV") ) {
        //            setUFValue( "IDH_AINV", "N" );
        //        }

        //        string sCVal = getUFValue(sID);
        //        if ( sCVal == "Y")
        //            return;

        //        if ( !sID.Equals("IDH") ) {
        //            if ( bDoSetValue ) {
        //                try {
        //                    setUFValue( sID, "Y");
        //                }
        //                catch (Exception) {
        //                }
        //            }

        //            //Dim sStatus As String
        //            if (bDoSetStatusValue == true) {
        //                if ( sID.Equals("IDH_DOARI") ) {
        //                    if (sAction.Equals("D")) 
        //                        sStatus = com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus();
        //                    else
        //                        sStatus = com.idh.bridge.lookups.FixedValues.getReqInvoiceStatus();

        //                    if ( !getFormDFValueAsString(IDH_DISPROW._Status).Equals( sStatus ) ) {
        //                        setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus());
        //                        setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethCash());
        //                        setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusUnPaid());
        //                        setFormDFValue(IDH_DISPROW._CCNum, "");
        //                        setFormDFValue(IDH_DISPROW._CCType, "0");
        //                        setFormDFValue(IDH_DISPROW._CCStat, "");							
        //                    }
        //                }
        //                else if (sID.Equals("IDH_AINV")) {
        //                    if (getUFValue( "IDH_AINV").Equals("Y") ) {
        //                        if (sAction.Equals("D")) 
        //                            sStatus = com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus();
        //                        else 
        //                            sStatus = com.idh.bridge.lookups.FixedValues.getReqInvoiceStatus();

        //                        if ( !getFormDFValueAsString(IDH_DISPROW._Status).Equals( sStatus ) ) {
        //                            setFormDFValue(IDH_DISPROW._Status, sStatus);
        //                            setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethPrePaid());
        //                            setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusPaid());
        //                            setFormDFValue(IDH_DISPROW._CCNum, "");
        //                            setFormDFValue(IDH_DISPROW._CCType, "0");
        //                            setFormDFValue(IDH_DISPROW._CCStat, "");
        //                        }
        //                    }
        //                }
        //                else if (sID.Equals( "IDH_DOARIP") ) {
        //                    if ( getUFValue("IDH_AINV").Equals("Y") ) {
        //                        if (sAction == "D") 
        //                            sStatus = com.idh.bridge.lookups.FixedValues.getDoPInvoiceStatus();
        //                        else 
        //                            sStatus = com.idh.bridge.lookups.FixedValues.getReqPInvoiceStatus();

        //                        if ( !getFormDFValueAsString(IDH_DISPROW._Status).Equals( sStatus ) ) {
        //                            setFormDFValue(IDH_DISPROW._Status, sStatus);
        //                            setFormDFValue(IDH_DISPROW._PayMeth, "Cash");
        //                            setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusUnPaid());
        //                            setFormDFValue(IDH_DISPROW._CCNum, "");
        //                            setFormDFValue(IDH_DISPROW._CCType, "0");
        //                            setFormDFValue(IDH_DISPROW._CCStat, "");
        //                        }
        //                    }
        //                }
        //                else if (sID.Equals("IDH_DOORD")) {
        //                    if (getUFValue("IDH_DOORD").Equals("Y")) {
        //                        if (sAction == "D") 
        //                            sStatus = com.idh.bridge.lookups.FixedValues.getDoOrderStatus();
        //                        else 
        //                            sStatus = com.idh.bridge.lookups.FixedValues.getReqOrderStatus();

        //                        if ( !getFormDFValueAsString(IDH_DISPROW._Status).Equals( sStatus ) ) {
        //                            setFormDFValue(IDH_DISPROW._Status, sStatus);
        //                            setFormDFValue(IDH_DISPROW._PayMeth, "Accounts");
        //                            setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusUnPaid());
        //                            setFormDFValue(IDH_DISPROW._CCNum, "");
        //                            setFormDFValue(IDH_DISPROW._CCType, "0");
        //                            setFormDFValue(IDH_DISPROW._CCStat, "");
        //                        }
        //                    }
        //                }
        //                else if (sID.Equals("IDH_FOC")) {
        //                    if ( getUFValue("IDH_FOC").Equals("Y")) {
        //                        if (sAction == "D") 
        //                            sStatus = com.idh.bridge.lookups.FixedValues.getDoFocStatus();
        //                        else 
        //                            sStatus = com.idh.bridge.lookups.FixedValues.getReqFocStatus();

        //                        if ( !getFormDFValueAsString(IDH_DISPROW._Status).Equals( sStatus ) ) {
        //                            setFormDFValue(IDH_DISPROW._Status, sStatus);
        //                            setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethFoc());
        //                            setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusFree());
        //                            setFormDFValue(IDH_DISPROW._CCNum, "");
        //                            setFormDFValue(IDH_DISPROW._CCType, "0");
        //                            setFormDFValue(IDH_DISPROW._CCStat, "");
        //                        }
        //                    }
        //                }
        //                else {
        //                    setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getStatusOpen());
        //                    setFormDFValue(IDH_DISPROW._PayMeth, "");
        //                    setFormDFValue(IDH_DISPROW._PayStat, "");
        //                    setFormDFValue(IDH_DISPROW._CCNum, "");
        //                    setFormDFValue(IDH_DISPROW._CCType, "0");
        //                    setFormDFValue(IDH_DISPROW._CCStat, "");
        //                }
        //            }
        //        }
        //        else {
        //            setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getStatusOpen());
        //            setFormDFValue(IDH_DISPROW._PayMeth, "");
        //            setFormDFValue(IDH_DISPROW._PayStat, "");
        //            setFormDFValue(IDH_DISPROW._CCNum, "");
        //            setFormDFValue(IDH_DISPROW._CCType, "0");
        //            setFormDFValue(IDH_DISPROW._CCStat, "");
        //        }
        //    }
        //}

        public void doCheckAccountingCheckBox(string sID, bool bSetChecked) {
            if (sID == null) {
                return;
            }

            if (sID.Equals("IDH_DOPO")) {
                if (bSetChecked) {
                    try {
                        setUFValue(sID, "Y");
                        setUFValue("IDH_FOC", "N");
                    } catch (Exception) {
                    }
                }
            } else if (sID.Equals("IDH_DOORD") ||
                       sID.Equals("IDH_DOARI") ||
                       sID.Equals("IDH_AINV") ||
                       sID.Equals("IDH_DOARIP") ||
                       sID.Equals("IDH_FOC") ||
                       sID.Equals("IDH")) {
                //setUFValue("IDH_DOPO", "N");
                if (!sID.Equals("IDH_FOC")) {
                    setUFValue("IDH_FOC", "N");
                }

                if (!sID.Equals("IDH_DOORD")) {
                    setUFValue("IDH_DOORD", "N");
                }

                if (!sID.Equals("IDH_DOARI")) {
                    setUFValue("IDH_DOARI", "N");
                } else {
                    //if (getUFValue("IDH_DOARI") != "Y") {
                    //    PaymentOption oPayMethForm = new PaymentOption();
                    //    bool bDoMDDocs = Config.ParameterAsBool("MDAGED", false);

                    //    System.Windows.Forms.DialogResult oResult = oPayMethForm.doShowDialog();
                    //    string sStatus;
                    //    if (oResult == System.Windows.Forms.DialogResult.OK) {
                    //        if (bDoMDDocs)
                    //            sStatus = com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus();
                    //        else
                    //            sStatus = com.idh.bridge.lookups.FixedValues.getReqInvoiceStatus();

                    //        moDispRowDB.U_Status = sStatus;
                    //        moDispRowDB.U_PayMeth = oPayMethForm.PaymentMethod;
                    //        moDispRowDB.U_PayStat = com.idh.bridge.lookups.FixedValues.getStatusUnPaid();
                    //        moDispRowDB.U_CCNum = "";
                    //        moDispRowDB.U_CCType = 0;
                    //        moDispRowDB.U_CCStat = "";

                    //        string sCC = Config.INSTANCE.getCreditCardName();
                    //        string sCC2 = oPayMethForm.PaymentMethod;

                    //        //if ( oPayMethForm.PaymentMethod.Equals( Config.INSTANCE.getCreditCardName() )) {
                    //        if (sCC.Equals(sCC2)) { //oPayMethForm.PaymentMethod.Equals(Config.INSTANCE.getCreditCardName())) {
                    //            if (!string.IsNullOrWhiteSpace(moDispRowDB.U_CustCd)) {
                    //                if (Config.INSTANCE.getParameterAsBool("CCONROW", false)) {
                    //                    //setSharedData(oForm, "CUSTCD", oDOR.U_CustCd)
                    //                    //setSharedData(oForm, "CUSTNM", oDOR.U_CustNm)
                    //                    //setSharedData(oForm, "AMOUNT", oDOR.U_Total)
                    //                    //goParent.doOpenModalForm("IDHCCPAY", oForm)

                    //                    CC oCC = new CC();
                    //                    oCC.CustomerCode = moDispRowDB.U_CustCd;
                    //                    oCC.CustomerName = moDispRowDB.U_CustNm;
                    //                    oCC.Amount = moDispRowDB.U_Total;
                    //                    oResult = oCC.doShowDialog();
                    //                    if (oResult == System.Windows.Forms.DialogResult.OK) {
                    //                        doUpdateCC(oCC);
                    //                    }
                    //                }
                    //            }
                    //        } else if (sStatus.Equals(com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()) &&
                    //                  (oPayMethForm.PaymentMethod.Equals("Cash") ||
                    //                    oPayMethForm.PaymentMethod.Equals("Check") ||
                    //                    oPayMethForm.PaymentMethod.Equals("Cheque"))) {
                    //            moDispRowDB.U_PayStat = com.idh.bridge.lookups.FixedValues.getStatusPaid();
                    //        } else if (sStatus.Equals(com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()) &&
                    //                oPayMethForm.PaymentMethod.Equals(com.idh.bridge.lookups.FixedValues.getPayMethFoc())) {
                    //            moDispRowDB.U_PayStat = com.idh.bridge.lookups.FixedValues.getStatusFree();
                    //        }

                    //        setUFValue("IDH_DOARI", "Y");
                    //        setUFValue("IDH_DOARIP", "N");
                    //        setUFValue("IDH_AINV", "N");

                    //        doUpdateFormChangedData();
                    //        return;
                    //    } else {
                    //        setUFValue("IDH_DOARI", "N");
                    //        setUFValue("IDH_DOARIP", "N");
                    //        setUFValue("IDH_AINV", "N");
                    //        return;
                    //    }
                    //}
                }

                if (!sID.Equals("IDH_DOARIP")) {
                    setUFValue("IDH_DOARIP", "N");
                }

                if (!sID.Equals("IDH_AINV")) {
                    setUFValue("IDH_AINV", "N");
                }

                string sCVal = getUFValue(sID);
                if (sCVal == "Y")
                    return;

                if (bSetChecked) {
                    if (!sID.Equals("IDH")) {
                        try {
                            setUFValue(sID, "Y");
                        } catch (Exception) {
                        }
                    }
                }
            }
        }

        public void doSetUseReadWeight() {
            setFormDFValue(IDH_DISPROW._UseWgt, "1");

            DBObject.setUserChanged(Config.MASK_TIPCSTWGT, false);
            DBObject.setUserChanged(Config.MASK_PRODUCERCSTWGT, false);
            DBObject.setUserChanged(Config.MASK_TIPCHRGWGT, false);

            DBObject.doGetAllUOM();
            doUpdateFormChangedData();
        }

        public void doSetUseAUOMWeight() {
            setFormDFValue(IDH_DISPROW._UseWgt, "2");

            DBObject.setUserChanged(Config.MASK_TIPCSTWGT, false);
            DBObject.setUserChanged(Config.MASK_PRODUCERCSTWGT, false);
            DBObject.setUserChanged(Config.MASK_TIPCHRGWGT, false);

            DBObject.doGetAllUOM();
            doUpdateFormChangedData();
        }

        public void doSetStockMovement() {
            if (moDisposalOrder.getItemValue("IDH_BOOKIN") == "Y")
                setFormDFValue(IDH_DISPROW._BookIn, "B");
            else
                setFormDFValue(IDH_DISPROW._BookIn, "N");
        }

        public void doCheckStockMovementCheckBox() {
            string sBookIn = getFormDFValueAsStringNZ(IDH_DISPROW._BookIn);
            if (sBookIn.Equals("B"))
                setUFValue("IDH_BOOKIN", "Y");
            else
                setUFValue("IDH_BOOKIN", "N");
        }

        public void doSwitchAccountingValues(string sID) {
            if (sID == null) {
                return;
            }

            string sAction = "";
            string sPStatus = null;
            string sStatus = null;

            string sPayMeth = getFormDFValueAsStringNZ(IDH_DISPROW._PayMeth);
            string sPayStatus = getFormDFValueAsStringNZ(IDH_DISPROW._PayStat);

            string sDoMDDocs = Config.Parameter("MDAGED");
            if (sDoMDDocs.ToUpper().Equals("TRUE") || !string.IsNullOrWhiteSpace(DBObject.U_TrnCode)) {
                sAction = "D"; //Do
            } else {
                sAction = "R"; //Request
            }

            if (sID.Equals("IDH_DOPO")) {
                if (getUFValue("IDH_DOPO").Equals("Y")) {
                    if (sAction == "D") {
                        sPStatus = com.idh.bridge.lookups.FixedValues.getDoOrderStatus();
                        sStatus = com.idh.bridge.lookups.FixedValues.getDoFocStatus();
                    } else {
                        sPStatus = com.idh.bridge.lookups.FixedValues.getReqOrderStatus();
                        sStatus = com.idh.bridge.lookups.FixedValues.getReqFocStatus();
                    }

                    setFormDFValue(IDH_DISPROW._PStat, sPStatus);
                    setFormDFValue(IDH_DISPROW._Status, sStatus);
                    setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethFoc());
                    setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusFree());
                    setFormDFValue(IDH_DISPROW._CCNum, "");
                    setFormDFValue(IDH_DISPROW._CCType, "0");
                    setFormDFValue(IDH_DISPROW._CCStat, "");
                } else {
                    setFormDFValue(IDH_DISPROW._PStat, com.idh.bridge.lookups.FixedValues.getStatusOpen());
                }
            } else if (sID.Equals("IDH_DOORD") ||
                       sID.Equals("IDH_DOARI") ||
                       sID.Equals("IDH_AINV") ||
                       sID.Equals("IDH_DOARIP") ||
                       sID.Equals("IDH_FOC") ||
                       sID.Equals("IDH")) {
                if (sID.Equals("IDH_DOARI")) {
                    if (sAction.Equals("D"))
                        sStatus = com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus();
                    else
                        sStatus = com.idh.bridge.lookups.FixedValues.getReqInvoiceStatus();

                    //if (!getFormDFValueAsString(IDH_DISPROW._Status).Equals(sStatus)) {
                    //    setFormDFValue(IDH_DISPROW._Status, sStatus);
                    //    setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethCash());
                    //    setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusUnPaid());
                    //    setFormDFValue(IDH_DISPROW._CCNum, "");
                    //    setFormDFValue(IDH_DISPROW._CCType, "0");
                    //    setFormDFValue(IDH_DISPROW._CCStat, "");
                    //}
                    //else {
                    //    if ( sPayMeth.Length == 0 ) 
                    //        setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethCash());

                    //    if ( sPayStatus.Length == 0 )
                    //        setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusUnPaid());
                    //}
                    if (moDispRowDB.isChangedRow()) {
                        PaymentOption oPayMethForm = new PaymentOption();
                        System.Windows.Forms.DialogResult oResult = oPayMethForm.doShowDialog();
                        if (oResult == System.Windows.Forms.DialogResult.OK) {
                            moDispRowDB.U_Status = sStatus;
                            moDispRowDB.U_CCNum = "";
                            moDispRowDB.U_CCType = 0;
                            moDispRowDB.U_CCStat = "";
                            moDispRowDB.U_PayStat = com.idh.bridge.lookups.FixedValues.getStatusUnPaid();
                            moDispRowDB.U_PayMeth = oPayMethForm.PaymentMethod;

                            string sCC = Config.INSTANCE.getCreditCardName();
                            string sCC2 = moDispRowDB.U_PayMeth;

                            ////if ( oPayMethForm.PaymentMethod.Equals( Config.INSTANCE.getCreditCardName() )) {
                            if (sCC.Equals(sCC2)) { //oPayMethForm.PaymentMethod.Equals(Config.INSTANCE.getCreditCardName())) {
                                //    if (!string.IsNullOrWhiteSpace(moDispRowDB.U_CustCd)) {
                                //        if (Config.INSTANCE.getParameterAsBool("CCONROW", false)) {
                                //            CC oCC = new CC();
                                //            oCC.CustomerCode = moDispRowDB.U_CustCd;
                                //            oCC.CustomerName = moDispRowDB.U_CustNm;
                                //            oCC.Amount = moDispRowDB.U_Total;
                                //            oResult = oCC.doShowDialog();
                                //            if (oResult == System.Windows.Forms.DialogResult.OK) {
                                //                moDispRowDB.U_CCNum = oCC.CCNUM; //setDFValue(oForm, msRowTable, "U_CCNum", sCCNum)
                                //                moDispRowDB.U_CCType = oCC.CCTYPE; //", sCCType)
                                //                moDispRowDB.U_CCStat = oCC.CCSTAT; //", sCCStatPre & getSharedData(oForm, "CCSTAT"))
                                //                moDispRowDB.U_PayStat = oCC.PayStatus; //", sPStat)
                                //                moDispRowDB.U_CCExp = oCC.CCEXP; //", sCCExp)
                                //                moDispRowDB.U_CCIs = oCC.CCIs; //", sCCIs)
                                //                moDispRowDB.U_CCSec = oCC.CCSec; //", sCCSec)
                                //                moDispRowDB.U_CCHNum = oCC.CCHNum; //", sCCHNm)
                                //                moDispRowDB.U_CCPCd = oCC.CCPCd; //, sCCPCd)
                                //            }
                                //        }
                                //    }
                            } else if (sStatus.Equals(com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()) &&
                                      (oPayMethForm.PaymentMethod.Equals("Cash") ||
                                        oPayMethForm.PaymentMethod.Equals("Check") ||
                                        oPayMethForm.PaymentMethod.Equals("Cheque"))) {
                                moDispRowDB.U_PayStat = com.idh.bridge.lookups.FixedValues.getStatusPaid();
                            } else if (sStatus.Equals(com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()) &&
                                    oPayMethForm.PaymentMethod.Equals(com.idh.bridge.lookups.FixedValues.getPayMethFoc())) {
                                moDispRowDB.U_PayStat = com.idh.bridge.lookups.FixedValues.getStatusFree();
                            }
                            doUpdateFormChangedData();
                            return;
                        }
                    }
                } else if (sID.Equals("IDH_AINV")) {
                    if (getUFValue("IDH_AINV").Equals("Y")) {
                        if (sAction.Equals("D"))
                            sStatus = com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus();
                        else
                            sStatus = com.idh.bridge.lookups.FixedValues.getReqInvoiceStatus();

                        if (!getFormDFValueAsString(IDH_DISPROW._Status).Equals(sStatus)) {
                            setFormDFValue(IDH_DISPROW._Status, sStatus);
                            setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethPrePaid());
                            setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusPaid());
                            setFormDFValue(IDH_DISPROW._CCNum, "");
                            setFormDFValue(IDH_DISPROW._CCType, "0");
                            setFormDFValue(IDH_DISPROW._CCStat, "");
                        } else {
                            if (sPayMeth.Length == 0)
                                setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethPrePaid());

                            if (sPayStatus.Length == 0)
                                setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusPaid());
                        }
                    }
                } else if (sID.Equals("IDH_DOARIP")) {
                    if (getUFValue("IDH_AINV").Equals("Y")) {
                        if (sAction == "D")
                            sStatus = com.idh.bridge.lookups.FixedValues.getDoPInvoiceStatus();
                        else
                            sStatus = com.idh.bridge.lookups.FixedValues.getReqPInvoiceStatus();

                        if (!getFormDFValueAsString(IDH_DISPROW._Status).Equals(sStatus)) {
                            setFormDFValue(IDH_DISPROW._Status, sStatus);
                            setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethCash());
                            setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusUnPaid());
                            setFormDFValue(IDH_DISPROW._CCNum, "");
                            setFormDFValue(IDH_DISPROW._CCType, "0");
                            setFormDFValue(IDH_DISPROW._CCStat, "");
                        } else {
                            if (sPayMeth.Length == 0)
                                setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethCash());

                            if (sPayStatus.Length == 0)
                                setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusUnPaid());
                        }
                    }
                } else if (sID.Equals("IDH_DOORD")) {
                    if (getUFValue("IDH_DOORD").Equals("Y")) {
                        if (sAction == "D")
                            sStatus = com.idh.bridge.lookups.FixedValues.getDoOrderStatus();
                        else
                            sStatus = com.idh.bridge.lookups.FixedValues.getReqOrderStatus();

                        if (!getFormDFValueAsString(IDH_DISPROW._Status).Equals(sStatus)) {
                            setFormDFValue(IDH_DISPROW._Status, sStatus);
                            setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethAccounts());
                            setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusUnPaid());
                            setFormDFValue(IDH_DISPROW._CCNum, "");
                            setFormDFValue(IDH_DISPROW._CCType, "0");
                            setFormDFValue(IDH_DISPROW._CCStat, "");
                        } else {
                            if (sPayMeth.Length == 0)
                                setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethAccounts());

                            if (sPayStatus.Length == 0)
                                setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusUnPaid());
                        }
                    }
                } else if (sID.Equals("IDH_FOC")) {
                    if (getUFValue("IDH_FOC").Equals("Y")) {
                        if (sAction == "D")
                            sStatus = com.idh.bridge.lookups.FixedValues.getDoFocStatus();
                        else
                            sStatus = com.idh.bridge.lookups.FixedValues.getReqFocStatus();

                        if (!getFormDFValueAsString(IDH_DISPROW._Status).Equals(sStatus)) {
                            setFormDFValue(IDH_DISPROW._Status, sStatus);
                            setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethFoc());
                            setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusFree());
                            setFormDFValue(IDH_DISPROW._CCNum, "");
                            setFormDFValue(IDH_DISPROW._CCType, "0");
                            setFormDFValue(IDH_DISPROW._CCStat, "");
                        } else {
                            if (sPayMeth.Length == 0)
                                setFormDFValue(IDH_DISPROW._PayMeth, com.idh.bridge.lookups.FixedValues.getPayMethFoc());

                            if (sPayStatus.Length == 0)
                                setFormDFValue(IDH_DISPROW._PayStat, com.idh.bridge.lookups.FixedValues.getStatusFree());
                        }
                    }
                } else {
                    setFormDFValue(IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getStatusOpen());
                    setFormDFValue(IDH_DISPROW._PayMeth, "");
                    setFormDFValue(IDH_DISPROW._PayStat, "");
                    setFormDFValue(IDH_DISPROW._CCNum, "");
                    setFormDFValue(IDH_DISPROW._CCType, "0");
                    setFormDFValue(IDH_DISPROW._CCStat, "");
                }
            }
        }

        public void doCreditCardPayment() {
            string sCC = Config.INSTANCE.getCreditCardName();
            string sCC2 = moDispRowDB.U_PayMeth;
            DialogResult oResult;

            if (sCC.Equals(sCC2)) {
                if (!string.IsNullOrWhiteSpace(moDispRowDB.U_CustCd)) {
                    if (Config.INSTANCE.getParameterAsBool("CCONROW", false)) {
                        CC oCC = new CC();
                        oCC.CustomerCode = moDispRowDB.U_CustCd;
                        oCC.CustomerName = moDispRowDB.U_CustNm;
                        oCC.Amount = moDispRowDB.U_Total;
                        oResult = oCC.doShowDialog();
                        if (oResult == System.Windows.Forms.DialogResult.OK) {
                            moDispRowDB.U_CCNum = oCC.CCNUM; //setDFValue(oForm, msRowTable, "U_CCNum", sCCNum)
                            moDispRowDB.U_CCType = oCC.CCTYPE; //", sCCType)
                            moDispRowDB.U_CCStat = oCC.CCSTAT; //", sCCStatPre & getSharedData(oForm, "CCSTAT"))
                            moDispRowDB.U_PayStat = oCC.PayStatus; //", sPStat)
                            moDispRowDB.U_CCExp = oCC.CCEXP; //", sCCExp)
                            moDispRowDB.U_CCIs = oCC.CCIs; //", sCCIs)
                            moDispRowDB.U_CCSec = oCC.CCSec; //", sCCSec)
                            moDispRowDB.U_CCHNum = oCC.CCHNum; //", sCCHNm)
                            moDispRowDB.U_CCPCd = oCC.CCPCd; //, sCCPCd)
                        }
                    }
                }
            }
        }

        private void doCalcWBWeightTotal() {
            double dWeight1 = 0;
            double dWeight2 = 0;
            double dWeightTotal = 0;
            try {
                dWeight1 = getFormDFValueAsDouble(IDH_DISPROW._Wei1);
                dWeight2 = getFormDFValueAsDouble(IDH_DISPROW._Wei2);
                if (dWeight1 > 0 && dWeight2 > 0) {
                    dWeightTotal = dWeight2 - dWeight1;
                    if (dWeightTotal < 0)
                        dWeightTotal *= -1;
                }
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error calculating Weight");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCWGT", null);
            }

            if (dWeightTotal > 0) {
                string sUOM = Config.INSTANCE.getWeighBridgeUOM(); //getCurrentUOM();
                moLookup.doCheckMaxWeight("DO", dWeightTotal, sUOM);
            }
            setFormDFValue(IDH_DISPROW._RdWgt, dWeightTotal);

            moDisposalOrder.doSetReportCheckBoxes();

            if (dWeightTotal != 0) {
                doItAll();
            } else {
                if ((dWeight1 == 0 && moDisposalOrder.wasFormFieldSetByUser("IDH_WEI1")) ||
                    (dWeight2 == 0 && moDisposalOrder.wasFormFieldSetByUser("IDH_WEI2"))) {
                    doItAll();
                }
            }
        }

        private string getCurrentUOM() {
            string sUOM;
            if (msRowMode.Equals("SO"))
                sUOM = (string)getFormDFValue(IDH_DISPROW._UOM);
            else
                sUOM = (string)getFormDFValue(IDH_DISPROW._PUOM);

            return sUOM;
        }

        //private void doDebugWB(string sMessage) {
        //    //MessageBox.Show(sMessage);
        //}

        private void doSetItemGroup() {
            string sVal = null;
            if (moWinForm != null) {
                Control[] oControl = moWinForm.Controls.Find("IDH_ITMGRC", true);
                if (oControl != null && oControl.Length > 0) {
                    sVal = oControl[0].Text;
                    oControl[0].Enabled = false;
                }
            } else if (moIDHForm != null) {
                SAPbouiCOM.Item oItem = moSBOForm.Items.Item("IDH_IGRP");
                SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;

                moIDHForm.doSetEnabled(ref oItem, true);

                SAPbouiCOM.ValidValues oVals = oCombo.ValidValues;
                if (oVals != null && oVals.Count > 0) {
                    sVal = oVals.Item(0).Value;
                    setUFValue("IDH_ITMGRN", oVals.Item(0).Description);
                    setFormDFValue(IDH_DISPROW._ItmGrp, sVal);
                }
                moIDHForm.doSetEnabled(ref oItem, false);
            }
            if (moDisposalOrder != null)
                moDisposalOrder.setFormDFValue(IDH_DISPORD._ItemGrp, sVal);
        }

        //public void doItAll() {
        //    doItAll(true);
        //}

        public void doItAll() {
            try {

                //DBObject.BlockChangeNotif = true;
                //try {
                DBObject.doGetAllUOM(true);
                //} catch ( Exception e ) {
                //}
                //DBObject.BlockChangeNotif = false;
                //DBObject.doGetAllPrices();

                //doReadDBOBufferedValues();
                doUpdateFormChangedData();
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Exception in doItAll. ");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { idh.bridge.Translation.getTranslatedWord("doItAll") });
            }
        }

        public void doItAll_Customer() {
            try {

                //DBObject.BlockChangeNotif = true;
                //try {
                DBObject.doGetCustomerUOM(true);
                //} catch (Exception e) {
                //}
                //DBObject.BlockChangeNotif = false;
                //DBObject.doGetChargePrices(true);

                //doReadDBOBufferedValues();
                doUpdateFormChangedData();
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Exception in doItAll_Customer. ");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { idh.bridge.Translation.getTranslatedWord("doItAll_Customer") });
            }
        }

        public void doItAll_Producer() {
            try {

                //DBObject.BlockChangeNotif = true;
                //try {
                DBObject.doGetProducerUOM(true);
                //} catch (Exception e) {
                //}
                //DBObject.BlockChangeNotif = false;
                //DBObject.doGetProducerCostPrice(true);

                //doReadDBOBufferedValues();
                doUpdateFormChangedData();
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Exception in doItAll_Producer. ");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { idh.bridge.Translation.getTranslatedWord("doItAll_Producer") });
            }
        }

        public void doItAll_DisposalSite() {
            try {

                //DBObject.BlockChangeNotif = true;
                //try {
                DBObject.doGetDisposalSiteUOM(true);
                //} catch (Exception e) {
                //}
                //DBObject.BlockChangeNotif = false;
                //DBObject.doGetTipCostPrice(true);

                //doReadDBOBufferedValues();
                doUpdateFormChangedData();
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Exception in doItAll_DisposalSite. ");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { idh.bridge.Translation.getTranslatedWord("doItAll_DisposalSite") });
            }
        }

        public void doItAll_Carrier() {
            try {

                DBObject.doGetHaulageCostPrice(true);

                //doReadDBOBufferedValues();
                doUpdateFormChangedData();
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Exception in doItAll_Carrier. ");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { idh.bridge.Translation.getTranslatedWord("doItAll_Carrier") });
            }
        }
        #endregion
        #region VatSettings
        public void doSwitchVatGroups() {
            System.DateTime dDocDate = getFormDFValueAsDateTime(IDH_DISPORD._BDate);
            DBObject.DoChargeVat = false;

            if (getItemValue("IDH_NOVAT") == "Y") {
                DBObject.U_TaxAmt = 0;
                //DBObject.U_VtCostAmt = 0;

                //DBObject.U_HCostVtGrp = Config.Parameter("MDVTEXGI");
                //DBObject.U_HCostVtRt = 0;

                //if ( moDisposalOrder.MMode == Config.MarketingMode.PO ) {
                //	setFormDFValue(IDH_DISPROW._TCostVtGrp, Config.Parameter("MDVTEXGI"));
                //	setFormDFValue(IDH_DISPROW._TCostVtRt, 0);
                //}
                //else {
                DBObject.U_TChrgVtGrp = Config.Parameter("MDVTEXGO");
                DBObject.U_TChrgVtRt = 0;

                DBObject.U_HChrgVtGrp = Config.Parameter("MDVTEXGO");
                DBObject.U_HChrgVtRt = 0;
                //}
            } else {
                DBObject.DoChargeVat = true;
                //    string sCardCode = getFormDFValueAsString(IDH_DISPROW._CustCd);
                //    string sWasteCode = getFormDFValueAsString(IDH_DISPROW._WasCd);

                //    if ( moDisposalOrder.MMode == Config.MarketingMode.PO ) {
                //        string sVatGroup = Config.INSTANCE.doGetPurchaceVatGroup( sCardCode, sWasteCode );
                //        double dVatRate = Config.INSTANCE.doGetVatRate( sVatGroup, dDocDate );

                //        setFormDFValue(IDH_DISPROW._TCostVtGrp, sVatGroup);
                //        setFormDFValue(IDH_DISPROW._TCostVtRt, dVatRate);
                //    }
                //    else {
                //        string sVatGroup = Config.INSTANCE.doGetSalesVatGroup( sCardCode, sWasteCode );
                //        double dVatRate = Config.INSTANCE.doGetVatRate( sVatGroup, dDocDate );

                //        setFormDFValue(IDH_DISPROW._TChrgVtGrp, sVatGroup);
                //        setFormDFValue(IDH_DISPROW._TChrgVtRt, dVatRate);
                //    }
            }
            //moDispRowDB.doCalcMarketingTotals();

            //LPV - incorrect function stick to the core process flow - moDispRowDB.doCalculateChargeTotal(true);
            moDispRowDB.doCalculateChargeVat('R');

            //doReadDBOBufferedValues();

            doUpdateFormData();
        }
        #endregion

        #region fillcombos
        public void doGetTZones() {
            string sBranch = (string)getFormDFValue(IDH_DISPROW._Branch);
            doFillCombo("IDH_TZONE", "[@IDH_TPZONES]", "Code", "U_Desc", " U_Branch = '" + sBranch + "'", "U_Desc", "None");
        }

        public void doGetBridges() {
            DataRecords oRecords = null;
            try {
                string sDesc;
                string sPort;
                string sID;
                //bool bIsFirst = true;
                string sQuery = "SELECT U_Val, U_Desc from [" + moLookup.msConfigurationTable + "] WHERE Code LIKE 'WBID%'";
                oRecords = DataHandler.INSTANCE.doBufferedSelectQuery("doGetBridges", sQuery);
                if (oRecords != null && oRecords.RecordCount > 0) {
                    object oCombo = null;

                    if (moWinForm != null) {
                        //oCombo = (ComboBox)moWinForm.Controls.Find("IDH_WEIBRG", true)[0];
                        //((ComboBox)oCombo).Items.Clear();

                        oCombo = moWinForm.Controls.Find("IDH_WEIBRG", true)[0];
                    } else if (moIDHForm != null) {
                        oCombo = moSBOForm.Items.Item("IDH_WEIBRG").Specific;

                        Combobox.doClearCombo(ref oCombo);
                        //SAPbouiCOM.ValidValues oVals = ((SAPbouiCOM.ComboBox)oCombo).ValidValues;
                        //IDHAddOns.idh.forms.Base.doClearValidValues( oVals );
                    }
                    Combobox.doClearCombo(ref oCombo);

                    string sFirstId = null;
                    if (oCombo != null) {
                        for (int i = 0; i < oRecords.RecordCount; i++) {
                            oRecords.gotoRow(i);
                            sID = oRecords.getValueAsString(0);
                            sDesc = oRecords.getValueAsString(1);
                            sPort = Config.Parameter("WBPORT" + sID);
                            if (moAvailablePorts == null || moAvailablePorts.Length == 0 || Array.IndexOf(moAvailablePorts, sPort) != -1) {
                                if ((msCommServerIP != null && msCommServerIP.Length > 0 && miCommServerPort > 0)
                                    || Weighbridge.IsPortAvailable(int.Parse(sPort))) {
                                    if (moWBridge == null) {
                                        if (sFirstId == null)
                                            sFirstId = sID;
                                        else if (moDisposalOrder.WeighBridge != null && moDisposalOrder.WeighBridge.Equals(sID))
                                            sFirstId = sID;
                                    }

                                    Combobox.doAddValueToCombo(ref oCombo, sID, sDesc);
                                    DataHandler.INSTANCE.doDebug("DisposalRow.doGetBridges", "Bridge used - " + sID, 20);
                                }
                            } else {
                                DataHandler.INSTANCE.doDebug("DisposalRow.doGetBridges", "Bridge not used - " + sID, 20);
                            }
                        }
                    }

                    if (sFirstId != null) {
                        DataHandler.INSTANCE.doDebug("DisposalRow.doGetBridges", "Setting FirstBridge - " + sFirstId, 20);
                        doConfigWeighBridge(sFirstId);
                    } else
                        DataHandler.INSTANCE.doDebug("DisposalRow.doGetBridges", "Not Setting FirstBridge.", 20);

                    //if ( oCombo != null ) {
                    //    for (int i = 0; i < oRecords.RecordCount; i++) {
                    //        oRecords.gotoRow(i);
                    //        sID = oRecords.getValueAsString(0);
                    //        sDesc = oRecords.getValueAsString(1);
                    //        sPort = Config.Parameter( "WBPORT" + sID);

                    //        if ( (msCommServerIP != null && msCommServerIP.Length > 0 && miCommServerPort > 0 ) 
                    //            || Weighbridge.IsPortAvailable(int.Parse(sPort) ) ){
                    //            if (moAvailablePorts == null || moAvailablePorts.Length == 0 || Array.IndexOf(moAvailablePorts, sPort) != -1) {
                    //                if (moWBridge == null && bIsFirst) {
                    //                    doConfigWeighBridge(sID);
                    //                    bIsFirst = false;
                    //                }

                    //                Combobox.doAddValueToCombo(ref oCombo, sID, sDesc);
                    //            }
                    //        }
                    //    }
                    //}
                }
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error retrieving bridges");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRWBS", null);
            }
            DataHandler.INSTANCE.doReleaseDataRecords(ref oRecords);
        }

        public void doGetObligated() {
            object oCombo = null;
            if (moWinForm != null) {
                oCombo = moWinForm.Controls.Find("IDH_OBLED", true)[0];
            } else if (moIDHForm != null) {
                oCombo = (SAPbouiCOM.ComboBox)moSBOForm.Items.Item("IDH_OBLED").Specific;
            }
            Combobox.doClearCombo(ref oCombo);

            //Combobox.doAddValueToCombo(ref oCombo, "", "Any");
            //Combobox.doAddValueToCombo(ref oCombo, "Obligated", "Obligated");
            //Combobox.doAddValueToCombo(ref oCombo, "Non-Obligated", "Non-Obligated");

            bool bDoTranslate = true;
            string sWord = Translation.getTranslatedWord("None");
            Combobox.doAddValueToCombo(ref oCombo, "", sWord);

            sWord = Config.ParameterWithDefault("OBTRUE", "Obligated");
            if (sWord == null || sWord.Length == 0) {
                sWord = Translation.getTranslatedWord("Obligated");
                bDoTranslate = true;
            } else
                bDoTranslate = false;
            Combobox.doAddValueToCombo(ref oCombo, sWord, sWord, bDoTranslate);

            sWord = Config.ParameterWithDefault("OBFALSE", "Non-Obligated");
            if (sWord == null || sWord.Length == 0) {
                sWord = Translation.getTranslatedWord("Non-Obligated");
                bDoTranslate = true;
            } else
                bDoTranslate = false;
            Combobox.doAddValueToCombo(ref oCombo, sWord, sWord, bDoTranslate);
        }

        public void doGetBranches() {
            doFillCombo("IDH_BRANCH", "OUBR", "Code", "Remarks");
        }

        //public void doFillTransactionCodes() {
        //    object oForm = (moWinForm != null ? (object)moWinForm : (object)moSBOForm);

        //}

        public void doGetJobTypes() {
            string sGrp = (string)getFormDFValue(IDH_DISPROW._ItmGrp);
            try {
                if (sGrp == null || sGrp.Length == 0) {
                    doSetItemGroup();
                    sGrp = (string)getFormDFValue(IDH_DISPROW._ItmGrp);
                }
                if (sGrp == null || sGrp.Length == 0) {
                    //DataHandler.INSTANCE.doError("User: Item Group must be set", "Item Group must be set");
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Item Group must be set", "ERUSGENS", new String[] { idh.bridge.Translation.getTranslatedWord("Item Group") });
                }
                //...				doFillCombo( "IDH_JOBTTP", "[@IDH_JOBTYPE] jt, OITB ig", "jt.U_JobTp", "jt.U_ItemGrp",  
                //				            "jt.U_ItemGrp = ig.ItmsGrpNam  AND ig.ItmsGrpCod = " + sGrp, "jt.U_ItemGrp, jt.U_JobTp", false);
                doFillCombo("IDH_JOBTTP", "[@IDH_JOBTYPE] jt, OITB ig", "jt.U_JobTp", "ig.ItmsGrpNam",
                            "jt.U_ItemGrp = ig.ItmsGrpCod  AND ig.ItmsGrpCod = " + sGrp, "ig.ItmsGrpNam, jt.U_JobTp", false);
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error getting job types");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGGEN", new String[] { idh.bridge.Translation.getTranslatedWord("Job Types") });
            }
        }

        public void doGetUOM(string sControl) {
            try {
                doFillCombo(sControl, "OWGT", "UnitDisply", "UnitName");
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error getting UOM");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGGEN", new String[] { idh.bridge.Translation.getTranslatedWord("UOM") });
            }
        }
        #endregion

        ///**
        // * Update the form with the DBO Object's buffered values
        // */
        //public void doReadDBOBufferedValues()
        //{
        //    setItemValue("IDH_SUBTOT", moDispRowDB.SubBeforeDiscount.ToString());
        //}

        #region "Handlers"
        public void doHandleChargePricesChanged(DBBase oCaller) {
            SAPbouiCOM.Form oForm = (SAPbouiCOM.Form)oCaller.SBOForm;
            if (((IDH_DISPROW)oCaller).CIP.Prices == null || ((IDH_DISPROW)oCaller).CIP.Prices.Count < 2) {
                ((SBOComboBox)moWinForm.Controls.Find("IDH_CUSCHG", true)[0]).HideDropDown = true;
            } else {
                if (moWinForm != null) {
                    ((SBOComboBox)moWinForm.Controls.Find("IDH_CUSCHG", true)[0]).HideDropDown = false;
                    FillCombos.TippingPriceCombo(FillCombos.getCombo(moWinForm, "IDH_CUSCHG"), ((IDH_DISPROW)oCaller).CIP.Prices);
                }
            }
        }

        //    moDispRowDB.Handler_CostHaulagePricesChanged = new IDH_DISPROW.et_ExternalTrigger(doHandleCostHaulagePricesChanged);
        public void doHandleCostHaulagePricesChanged(DBBase oCaller) {
            SAPbouiCOM.Form oForm = (SAPbouiCOM.Form)oCaller.SBOForm;
            if (((IDH_DISPROW)oCaller).SIPH.Prices == null || ((IDH_DISPROW)oCaller).SIPH.Prices.Count < 2) {
                ((SBOComboBox)moWinForm.Controls.Find("IDH_ORDCOS", true)[0]).HideDropDown = true;
            } else {
                if (moWinForm != null) {
                    ((SBOComboBox)moWinForm.Controls.Find("IDH_ORDCOS", true)[0]).HideDropDown = false;
                    FillCombos.HaulagePriceCombo(FillCombos.getCombo(moWinForm, "IDH_ORDCOS"), ((IDH_DISPROW)oCaller).SIPH.Prices);
                }
            }
        }

        //    moDispRowDB.Handler_CostProducerPricesChanged = new IDH_DISPROW.et_ExternalTrigger(doHandleCostProducerPricesChanged);
        public void doHandleCostProducerPricesChanged(DBBase oCaller) {
            SAPbouiCOM.Form oForm = (SAPbouiCOM.Form)oCaller.SBOForm;
            if (((IDH_DISPROW)oCaller).SIPP.Prices == null || ((IDH_DISPROW)oCaller).SIPP.Prices.Count < 2) {
                ((SBOComboBox)moWinForm.Controls.Find("IDH_PRCOST", true)[0]).HideDropDown = true;
            } else {
                if (moWinForm != null) {
                    ((SBOComboBox)moWinForm.Controls.Find("IDH_PRCOST", true)[0]).HideDropDown = false;
                    FillCombos.TippingPriceCombo(FillCombos.getCombo(moWinForm, "IDH_PRCOST"), ((IDH_DISPROW)oCaller).SIPP.Prices);
                }
            }
        }

        //    moDispRowDB.Handler_CostTippingPricesChanged = new IDH_DISPROW.et_ExternalTrigger(doHandleCostTippingPricesChanged);
        public void doHandleCostTippingPricesChanged(DBBase oCaller) {
            SAPbouiCOM.Form oForm = (SAPbouiCOM.Form)((IDH_DISPROW)oCaller).SBOForm;
            if (((IDH_DISPROW)oCaller).SIPT.Prices == null || ((IDH_DISPROW)oCaller).SIPT.Prices.Count < 2) {
                ((SBOComboBox)moWinForm.Controls.Find("IDH_TIPCOS", true)[0]).HideDropDown = true;
            } else {
                if (moWinForm != null) {
                    ((SBOComboBox)moWinForm.Controls.Find("IDH_TIPCOS", true)[0]).HideDropDown = false;
                    FillCombos.TippingPriceCombo(FillCombos.getCombo(moWinForm, "IDH_TIPCOS"), ((IDH_DISPROW)oCaller).SIPT.Prices);
                }
            }
        }

        public void doHandleChargeTotalsCalculated(DBBase oCaller) {
            //setUFValue(oForm, "U_BefDis", oCaller.SubBeforeDiscount)
            setItemValue("IDH_SUBTOT", moDispRowDB.SubBeforeDiscount.ToString());

            //doCalculateProfit(oCaller)
        }

        public void doHandleCostTotalsCalculated(DBBase oCaller) {
            //setUFValue(oForm, "U_BefDis", oCaller.SubBeforeDiscount)
            setItemValue("IDH_SUBTOT", moDispRowDB.SubBeforeDiscount.ToString());

            //doCalculateProfit(oCaller)
        }

        public int doHandlerYNOption(DBBase oCaller, string sMessageId, string[] sParams, int iDefButton) {
            return Messages.INSTANCE.doResourceMessageYN(sMessageId, sParams, iDefButton);
        }
        #endregion

    }
}
