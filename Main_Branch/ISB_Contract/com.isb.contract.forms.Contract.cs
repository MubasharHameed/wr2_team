﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
using com.idh.bridge;

using com.idh.bridge.lookups;
using com.uBC.utils;
using com.idh.controls;
using SAPbouiCOM;

using com.isb.contract.dbObjects.User;
using com.idh.forms.oo;


namespace com.isb.forms.contract {
    public class Contract : com.idh.forms.oo.Form {

        private IDH_CONTRACT moContract;
        public IDH_CONTRACT DBContract {
            get { return moContract; }
        }
        private DBOGrid moGrid;
        private string _ContractCode = "-1";
        //private string _EnqID = "";

        private bool bHandleValidateEvent = true;
        private bool mbAddUpdateOk = false;
        private bool mbLoadWOQById = false;
        private bool mbLoadReadOnly = false;
        private bool mbENQRESULT = false;
        ////private bool mbFromActivity = false;
        private com.idh.dbObjects.User.IDH_FORMSET moFormSettings;
        private bool doHandleDuplicate = false;
        //protected string msOrdCat = "Waste Order";//com.idh.bridge.lookups.FixedValues.getStatusOrder();

        public Contract(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doSetHandlers();
            doInitialize();
        }

        public Contract(string sParentId, string sContractCode)
            : base(null, sParentId, null) {
            doSetHandlers();

            _ContractCode = sContractCode;
            mbLoadWOQById = true;
            mbLoadReadOnly = false;

        }


        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuePos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDHCONTR", sParMenu, iMenuePos, "Enq_Contract.srf", true, true, false, "Contract",
                IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            owForm.doEnableHistory(IDH_CONTRACT.TableName);
            return owForm;
        }

        static string msMainTable = IDH_CONTRACT.TableName;
        static string msGridTable = IDH_CONTRTROW.TableName;

        protected void doInitialize() {
            moContract= new IDH_CONTRACT(IDHForm, SBOForm);
            moContract.MustLoadChildren = true;
            ////moWOQ.AutoAddEmptyLine = false;
            ////moWOQ.WOQItems.AutoAddEmptyLine = true;
            //moContract.Handler_ExternalTriggerWithData = new idh.dbObjects.DBBase.et_ExternalTriggerWithData(doHandlerWithData);
            //moContract.Handler_ExternalTriggerWithData_ShowYesNO = new idh.dbObjects.DBBase.et_ExternalYNOption(doHandlerYesNoMsgBox);
        }

        #region Properties

        //public string EnquiryID {
        //    get { return _EnqID; }
        //    set { _EnqID = value; }
        //}
        public string ContractID {
            get { return _ContractCode; }
            set { _ContractCode = value; }
        }

        public bool bLoadContract {
            get { return mbLoadWOQById; }
            set { mbLoadWOQById = value; }
        }
        public bool bAddUpdateOk {
            get { return mbAddUpdateOk; }
            set { mbAddUpdateOk = value; }
        }
        public bool bLoadReadOnly {
            get { return mbLoadReadOnly; }
            set { mbLoadReadOnly = value; }
        }
        public bool CONTRACTRESULT {
            get { return mbENQRESULT; }
            set { mbENQRESULT = value; }
        }

        //public bool bFromActivity {
        //    get { return moWOQ.FromActivity; }
        //    set { moWOQ.FromActivity = value; }


        //}

        private System.Collections.ArrayList moaSelectItems;
        public System.Collections.ArrayList SelectItems {
            get { return moaSelectItems; }
            set { moaSelectItems = value; }
        }

        #endregion      
        #region Override methods
        public override void doCompleteCreate(ref bool BubbleEvent) {
            {
                try {
                    DataBrowser.BrowseBy = "IDH_CODE";
                    //doAddUF("IDH_LBLTOT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, false, false);

                    if (base.IDHForm.goParent.doCheckModal(UniqueID)) {
                        SBOForm.EnableMenu("1281", false);
                        SBOForm.EnableMenu("1282", false);
                        SBOForm.EnableMenu("1290", false);
                        SBOForm.EnableMenu("1288", false);
                        SBOForm.EnableMenu("1289", false);
                        SBOForm.EnableMenu("1291", false);
                    }
                    //SBOForm.EnableMenu(IDHAddOns.idh.lookups.Base.DUPLICATE, true);
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                    BubbleEvent = false;
                }
            }
        }

        /** 
          * Do the final form steps to show before loaddata
          */
        public override void doBeforeLoadData() {
            try {
                if (moContract== null) {
                    moContract = new IDH_CONTRACT(IDHForm, SBOForm);
                    moContract.MustLoadChildren = true;
                    //moContract.Handler_ExternalTriggerWithData = new idh.dbObjects.DBBase.et_ExternalTriggerWithData(doHandlerWithData);
                    //moContract.Handler_ExternalTriggerWithData_ShowYesNO = new idh.dbObjects.DBBase.et_ExternalYNOption(doHandlerYesNoMsgBox);
                }
                Title = IDHForm.gsTitle;
                moGrid = new DBOGrid(IDHForm, SBOForm, "LINESGRID", moContract.ContractItems);
                moGrid.doSetDeleteActive(true);
                //setUFValue("IDH_LBLTOT", Translation.getTranslatedWord("Totals"));
                doTheGridLayout();
                doSetDBHandlers();
                doSetGridHandlers();
                doFillCombos();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }
        public override void doLoadData() {
            //base.doLoadData();
            try {
                
                //Items.Item("IDH_CODE").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable,
                //              (int)SAPbouiCOM.BoFormMode.fm_ADD_MODE + (int)SAPbouiCOM.BoFormMode.fm_EDIT_MODE +
                //              (int)SAPbouiCOM.BoFormMode.fm_OK_MODE + (int)SAPbouiCOM.BoFormMode.fm_PRINT_MODE +
                //              (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + (int)SAPbouiCOM.BoFormMode.fm_VIEW_MODE,
                //              SAPbouiCOM.BoModeVisualBehavior.mvb_False);
                //Items.Item("IDH_CODE").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, (int)SAPbouiCOM.BoFormMode.fm_FIND_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_True);

                //Items.Item("IDH_CPYENQ").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible,
                //      (int)SAPbouiCOM.BoFormMode.fm_ADD_MODE + (int)SAPbouiCOM.BoFormMode.fm_EDIT_MODE +
                //      (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + (int)SAPbouiCOM.BoFormMode.fm_OK_MODE,
                //      SAPbouiCOM.BoModeVisualBehavior.mvb_True);
                //Items.Item("IDH_CPYENQ").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, (int)SAPbouiCOM.BoFormMode.fm_FIND_MODE + (int)SAPbouiCOM.BoFormMode.fm_VIEW_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False);

                //Items.Item("IDH_DOCS").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible,
                //        (int)SAPbouiCOM.BoFormMode.fm_ADD_MODE + (int)SAPbouiCOM.BoFormMode.fm_EDIT_MODE +
                //        (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + (int)SAPbouiCOM.BoFormMode.fm_OK_MODE,
                //        SAPbouiCOM.BoModeVisualBehavior.mvb_True);
                //Items.Item("IDH_DOCS").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, (int)SAPbouiCOM.BoFormMode.fm_FIND_MODE + (int)SAPbouiCOM.BoFormMode.fm_VIEW_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False);
                ////doLinkWOQNumber();


                //SBOForm.AutoManaged = false;
                if (mbLoadWOQById) {
                    Freeze(true);
                    if (_ContractCode != "-1" && _ContractCode != "") {
                        if (moContract.getByKey(_ContractCode)) {
                            //FillCombos.FillState(Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);

                            //doSwitchToEdit();
                            moGrid.doApplyRules();
                            doFillGridCombos();
                            if (bLoadReadOnly)
                                Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE;
                            else
                          HandleFormStatus();
                        }
                    } 
                } else {
                    Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
            } finally {
                Freeze(false);
            }
            //doActivateTotalButton(false);
        }

        public override void doCloseForm(ref bool BubbleEvent) {
            DBOGrid.doRemoveGrid(SBOForm, "LINESGRID");
            base.doCloseForm(ref BubbleEvent);
        }

        public override void doFinalizeShow() {
            base.doFinalizeShow();
        }

        ////public override void doLoadData() {
        ////    doFillGridCombos();
        ////}
        #endregion
        
        #region General Methods
        //private void doSetHeaderByEnquiry(IDH_ENQUIRY oEnquiry) {
        //    moWOQ.doSetByEnquiry(oEnquiry);
        //    EnableItem(false, "IDH_DOCNUM");
        //}

        private void doSetHandlers() {
            doStartEventFilterBatch(true);
            Handler_Button_Add = doAddUpdateEvent;
            Handler_Button_Update = doAddUpdateEvent;
            //// Handler_VALIDATE_CHANGED += Handle_ValidateEvent;
            //Handler_Button_Find = doFindEvent;
            Handler_Button_Ok = doHandleOKEvent;
            //Handler_FORM_DATA_LOAD = doHandleFormDataLoad;
            ////addHandler_ITEM_PRESSED("2", doCancelButton);
            //addHandler_CLICK("IDH_VERSN", doShowVersionList);
            //addHandler_ITEM_PRESSED("IDH_CONREF", doRefeshContact);

            //addHandler_ITEM_PRESSED("IDH_DOCS", doHandleDocButton);
            //addHandler_ITEM_PRESSED("IDH_QUSTN", doShowQuestionnaire);
            //addHandler_ITEM_PRESSED("IDH_SNDAPR", doHandleApprovalButton);
            //addHandler_ITEM_PRESSED("IDH_CRTWO", doHandleCreateWO);
            //addHandler_ITEM_PRESSED("IDH_FINDAD", doFindAddress);
            //addHandler_ITEM_PRESSED("IDH_CRETBP", doCreateBP);
            //addHandler_ITEM_PRESSED("IDH_CPYENQ", doCopyFromENQ);
            //addHandler_ITEM_PRESSED("IDH_ADRCFL", doSearchBPAddressCFL);
            ////addHandler_VALIDATE("IDH_CardNM", dovalidateBPEvent);
            //addHandler_VALIDATE("IDH_ADR1", dovalidateFieldsForPriceChange);
            //addHandler_VALIDATE("IDH_PCODE", dovalidateFieldsForPriceChange);

            //addHandler_VALIDATE_CHANGED("IDH_STREET", doHandleAddressChanged);
            //addHandler_VALIDATE_CHANGED("IDH_BLOCK", doHandleAddressChanged);
            //addHandler_VALIDATE_CHANGED("IDH_CITY", doHandleAddressChanged);
            //addHandler_VALIDATE_CHANGED("IDH_COUNTY", doHandleAddressChanged);
            //addHandler_COMBO_SELECT("IDH_STATE", doHandleAddressChanged);


            //addHandler_VALIDATE("IDH_CardCD", dovalidateFieldsForPriceChange);
            //addHandler_VALIDATE("IDH_BRANCH", dovalidateFieldsForPriceChange);
            ////addHandler_VALIDATE("IDH_CardCD", dovalidateFieldsForPriceChange);
            //addHandler_COMBO_SELECT("IDH_BRANCH", doHandleBranchCombo);
            //addHandler_COMBO_SELECT("IDH_ITMGRP", doHandleItemGroupCombo);

            addHandler_VALIDATE_CHANGED("IDH_CARDCD", dovalidateBPChangeEvent);
            addHandler_VALIDATE_CHANGED("IDH_CARDNM", dovalidateBPChangeEvent);
            //addHandler_COMBO_SELECT("IDH_CUNTRY", doHandleCountryCombo);
            //addHandler_ITEM_PRESSED("IDH_NOVAT", doHanleNoVATCheckBox);
            //addHandler_ITEM_PRESSED("IDH_CALTOT", doHandleCalculateTotal);

            ////Menu Events
            Handler_Menu_NAV_ADD = doMenuAddEvent;
            Handler_Menu_NAV_FIRST = doMenuBrowseEvents;
            Handler_Menu_NAV_LAST = doMenuBrowseEvents;
            Handler_Menu_NAV_NEXT = doMenuBrowseEvents;
            Handler_Menu_NAV_PREV = doMenuBrowseEvents;
            Handler_Menu_NAV_FIND = doMenuFindBUttonEvent;
            Handler_FormMode_Changed = doHandleModeChange;

            doAddEvent(BoEventTypes.et_RIGHT_CLICK);
            doCommitEventFilters();

        }
        private void doSetDBHandlers() {
            //BO - Business Object
            moContract.Handler_RecordAdded += Handle_RecordProcessed;
            moContract.Handler_RecordUpdated += Handle_RecordProcessed;
            moContract.Handler_RecordDeleted += Handle_RecordProcessed;

            moContract.Handler_RowDataLoaded += Handle_DocLoaded;
            moContract.ContractItems.Handler_RowDataLoaded += Handle_DocItemsLoaded;
        }
        protected void doSetGridHandlers() {
            doStartEventFilterBatch(true);

            //moGrid.Handler_GRID_SEARCH = doHandleGridSearch;
            //moGrid.Handler_GRID_SEARCH_VALUESET = doHandleGridSearchValueSet;
            //moGrid.Handler_GRID_DOUBLE_CLICK = doHandleGridDoubleClick;
            //moGrid.Handler_GRID_RIGHT_CLICK = doHandleGridRightClick;
            //moGrid.Handler_GRID_MENU_EVENT = doHandleGridMenuEvent;
            //moGrid.Handler_GRID_SORT = doHandleGridSort;
            //moGrid.Handler_GRID_FIELD_CHANGED = doHandleGRIDFIELDCHANGED;

            doCommitEventFilters();

        }

        //private void doHandleGridColumns() {
        //    ListFields oListFields = moGrid.getListfields();
        //    for (int i = 0; i < oListFields.Count - 1; i++) {
        //        com.idh.controls.strct.ListField oField = oListFields.getListField(i);
        //        if (oField.miWidth != 0 && oField.mbCanEdit) {
        //            moGrid.doEnableColumn(oField.msFieldId, true);
        //        }
        //    }
        //}

        private void HandleFormStatus() {
            //    //  Freeze ( true);
            //    if (moWOQ.U_Status == ((int)IDH_WOQHD.en_WOQSTATUS.WaitingForApproval).ToString() || moWOQ.U_Status == ((int)IDH_WOQHD.en_WOQSTATUS.Rejected).ToString() ||
            //        moWOQ.U_Status == ((int)IDH_WOQHD.en_WOQSTATUS.Cancelled).ToString() || moWOQ.U_Status == ((int)IDH_WOQHD.en_WOQSTATUS.Closed).ToString()) {
            //        Mode = BoFormMode.fm_VIEW_MODE;
            //    } else if (moWOQ.U_Status == ((int)IDH_WOQHD.en_WOQSTATUS.Approved).ToString() || moWOQ.U_Status == ((int)IDH_WOQHD.en_WOQSTATUS.WOCreated).ToString()) {
            //        Mode = BoFormMode.fm_OK_MODE;
            //        moGrid.AddEditLine = false;
            //        string[] oExl = { "IDH_CRETBP", "IDH_CRTWO", "2", "1", "IDH_DOCS" };
            //        setFocus("IDHTEMP");

            //        //DisableAllEditItems(null);
            //        DisableAllEditAndButtonItems(ref oExl);
            //        //moGrid.doEnabled(false);
            //        doHandleGridColumns();
            //        for (int i = 0; i <= moGrid.getRowCount() - 1; i++) {
            //            if (moGrid.doGetFieldValue(IDH_WOQITEM._Status, i).ToString() == ((int)IDH_WOQITEM.en_WOQITMSTATUS.WOCreated).ToString() ||
            //                moGrid.doGetFieldValue(IDH_WOQITEM._Status, i).ToString() == ((int)IDH_WOQITEM.en_WOQITMSTATUS.Close).ToString() ||
            //                moGrid.doGetFieldValue(IDH_WOQITEM._Status, i).ToString() == ((int)IDH_WOQITEM.en_WOQITMSTATUS.Close).ToString())
            //                moGrid.Settings.SetRowEditable(i + 1, false);
            //            //else
            //            //    moGrid.Settings.SetRowEditable(i + 1, true);
            //        }
            //        //doTheGridLayout();



            //    } else {
            //        Mode = BoFormMode.fm_OK_MODE;

            //        moGrid.doEnabled(true);
            //    }
            //    //  Freeze( false);
        }

        //private void doActivateTotalButton(bool bEnable) {
        //    EnableItem(bEnable, "IDH_CALTOT");
        //}

        #endregion
        //#region ItemEventHandlers


        ///** 
        // * Update the Row Header Ref
        // */
        ////private void doUpdateLineHeaderRef() {
        ////    moWOQ.WOQItems.setValueForAllRows(IDH_WOQITEM._JobNr, moWOQ.Code, true);
        ////}

        public bool doHandleOKEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction)
                return false;
            return true;
        }
        public bool doAddUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction)
                return true;
            //if (!doValidate(SBOForm)) {
            //    BubbleEvent = false;
            //    return false;
            //}
            try {
                bool bDoContinue = true;
                BoFormMode CurrentMode = Mode;
                Freeze(true);
                DataHandler.INSTANCE.StartTransaction();
                //bDoContinue = doAddUpdateAddress();
                //if (bDoContinue)
                //    bDoContinue = doAutoSave(IDH_WOQHD.AUTONUMPREFIX, moWOQ.AutoNumKey);//doAutoSave(SBOForm, msMainTable, IDH_WOQHD.AUTONUMPREFIX, msHeaderKeyGen);
                //mbAddUpdateOk = bDoContinue;
                if (moContract.doProcessData()) {
                    //here update grid
                    //doUpdateLineHeaderRef();
                    bDoContinue = true;
                    //if (moGrid.getDeletedRowList() != null) {
                    //    ArrayList aDeletedRows = moGrid.getDeletedRowList();
                    //    for (int i = 0; i <= aDeletedRows.Count - 1; i++) {
                    //        IDHAddOns.idh.data.AuditObject oAuditObjGrid = new IDHAddOns.idh.data.AuditObject(IDHAddOns.idh.addon.Base.PARENT, IDH_WOQITEM.TableName);
                    //        if (oAuditObjGrid.setKeyPair(aDeletedRows[i].ToString(), IDHAddOns.idh.data.AuditObject.ac_Types.idh_REMOVE) == true) {
                    //            oAuditObjGrid.Commit();
                    //        }
                    //    }
                    //}
                    //int iwResult = 0;
                    //for (int i = 0; i <= moWOQ.WOQItems.Count - 1; i++) {
                    //    moWOQ.WOQItems.gotoRow(i);
                    //    //Start 
                    //    //Adding Lab Task for the WOQ Row 
                    //    if (CurrentMode == BoFormMode.fm_ADD_MODE) {
                    //        //Check if Sample Require is checked 
                    //        if (moWOQ.WOQItems.U_AdtnlItm != "A" && moWOQ.WOQItems.U_Sample == "Y") {
                    //            if (moWOQ.WOQItems.U_SampleRef.Trim() == string.Empty) {
                    //                bDoContinue = moWOQ.WOQItems.doCreateSampleTask(1,"");
                    //            } else {
                    //                moWOQ.WOQItems.doSaveSampleTask();
                    //            }
                    //        }
                    //    } else if (CurrentMode == BoFormMode.fm_UPDATE_MODE && moWOQ.WOQItems.U_Sample == "Y") {
                    //        if (moWOQ.WOQItems.U_SampleRef.Trim() == string.Empty) {
                    //            bDoContinue = moWOQ.WOQItems.doCreateSampleTask(1,"");
                    //        } else {

                    //            moWOQ.WOQItems.doSaveSampleTask();
                    //        }
                    //    }
                    //    //IDHAddOns.idh.data.AuditObject oAuditObjGrid = new IDHAddOns.idh.data.AuditObject(base.IDHForm.goParent, IDH_WOQITEM.TableName);
                    //    //if (oAuditObjGrid.setKeyPair(moWOQ.WOQItems.Code, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) == false) {
                    //    //    oAuditObjGrid.setKeyPair(moWOQ.WOQItems.Code, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD);
                    //    //}
                    //    if (moWOQ.WOQItems.U_WasFDsc.Trim() == string.Empty && moWOQ.WOQItems.U_WasDsc.Trim() == string.Empty && moWOQ.WOQItems.U_WasCd.Trim() == string.Empty) {
                    //        continue;
                    //    }
                    //    //oAuditObjGrid.setName((string)moWOQ.WOQItems.Name);//(IDH_WOQITEM._Name, i));
                    //    //for (int iCols = 0; iCols <= moWOQ.WOQItems.getColumnCount() - 1; iCols++) {
                    //    //    if (moWOQ.WOQItems.getFieldInfo(iCols).FieldName != "Code" && moWOQ.WOQItems.getFieldInfo(iCols).FieldName != "Name")
                    //    //        oAuditObjGrid.setFieldValue(moWOQ.WOQItems.getFieldInfo(iCols).FieldName, moWOQ.WOQItems.getValue(moWOQ.WOQItems.getFieldInfo(iCols).FieldName));
                    //    //}
                    //    //iwResult = oAuditObjGrid.Commit();
                    //    //if (iwResult != 0) {

                    //    //    bDoContinue = false;
                    //    //    break;
                    //    //}
                    //}
                    
                    //here update Enquiry
                } else
                    bDoContinue = false;
                //if (bDoContinue) {
                //    //moWOQ.WOQItems.AddedRows.Clear();
                //    //moWOQ.WOQItems.ChangedRows.Clear();
                //    IDH_ENQITEM moEnquiryItem = new IDH_ENQITEM();
                //    for (int i = 0; i <= moWOQ.WOQItems.Count - 1; i++) {
                //        moEnquiryItem.DoNotSetDefault = true;
                //        moEnquiryItem.DoNotSetParent = true;
                //        moWOQ.WOQItems.gotoRow(i);
                //        if (moWOQ.WOQItems.U_EnqLID == string.Empty)
                //            continue;
                //        if (moEnquiryItem.getByKey(moWOQ.WOQItems.U_EnqLID)) {
                //            moEnquiryItem.U_Status = "2";
                //            moEnquiryItem.U_WOQID = moWOQ.WOQItems.U_JobNr;
                //            moEnquiryItem.U_WstGpCd = moWOQ.WOQItems.U_WstGpCd;
                //            moEnquiryItem.U_WstGpNm = moWOQ.WOQItems.U_WstGpNm;
                //            moEnquiryItem.U_WOQLineID = moWOQ.WOQItems.Code;
                //            bDoContinue = moEnquiryItem.doUpdateDataRow("Linked with WOQ:" + moWOQ.WOQItems.U_JobNr + "." + moWOQ.WOQItems.Code);
                //            if (bDoContinue == false) {
                //                break;
                //            }
                //        }
                //    }
                //}
                if (bDoContinue) {
                    //if (DataHandler.INSTANCE.IsInTransaction()) {
                    //    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    //    moWOQ.DoAddressChanged = false;
                    //    com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
                    //    IDH_LABTASK.dosendAllAlerttoUser();
                    //} else {
                    //    //'Something went wrong...
                    //    com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
                    //    SBOForm.Update();
                    //    moGrid.doLoadLastChangeIndicators();
                    //    IDH_LABTASK.MessagesToSent.Clear();
                    //    return false;
                    //}

                    if (base.IDHForm.goParent.doCheckModal(SBOForm.UniqueID) || (this.ParentId != null && this.ParentId != "")) {
                        //mbENQRESULT = true;
                        Mode = BoFormMode.fm_OK_MODE;
                        BubbleEvent = false;
                        return true;
                    } else {
                        if (CurrentMode == BoFormMode.fm_ADD_MODE || CurrentMode == BoFormMode.fm_UPDATE_MODE) {
                            Mode = BoFormMode.fm_OK_MODE;
                        } //else
                          // Mode = BoFormMode.fm_OK_MODE;
                        BubbleEvent = false;
                    }
                } else {
                    if (DataHandler.INSTANCE.IsInTransaction()) {
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                        //com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
                        //SBOForm.Update();
                        //moGrid.doLoadLastChangeIndicators();
                        //if (base.IDHForm.goParent.doCheckModal(SBOForm.UniqueID) || (this.ParentId != null && this.ParentId != "")) {
                        //    return true;
                        //}
                        //return true;
                    }
                    com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
                    SBOForm.Update();
                    moGrid.doLoadLastChangeIndicators();
                    if (base.IDHForm.goParent.doCheckModal(SBOForm.UniqueID) || (this.ParentId != null && this.ParentId != "")) {
                        return true;
                    }
                    return true;
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "Error in saving WOQ." });
                if (DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
                    SBOForm.Update();
                    moGrid.doLoadLastChangeIndicators();
                    if (base.IDHForm.goParent.doCheckModal(SBOForm.UniqueID) || (this.ParentId != null && this.ParentId != "")) {
                        mbENQRESULT = false;
                    }
                    return true;
                }
            } finally {
                Freeze(false);
            }
            return true;
        }

        private bool doHandleGRIDFIELDCHANGED(ref IDHAddOns.idh.events.Base pVal) {
            try {
                if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED && !pVal.BeforeAction) {
                    //if (pVal.ColUID == IDH_WOQITEM._TCharge || pVal.ColUID == IDH_WOQITEM._TipCost || pVal.ColUID == IDH_WOQITEM._CusChr
                        //|| pVal.ColUID == IDH_WOQITEM._OrdCost) {
                        moContract.ContractItems.doMarkUserUpdate(pVal.ColUID);
                        //return false;
                    //}

                    //if user clicks Sample Check Box 
                    /*MA: 27-07-2016Moved to Db object 
                    if (pVal.ColUID == IDH_WOQITEM._Sample && SBOForm.Mode == BoFormMode.fm_UPDATE_MODE)
                    { 

                        //moGrid.doGetFieldValue(IDH_WOQITEM._WasFDsc, pVal.Row).ToString().Trim() != string.Empty

                        if (SBOForm.Mode == BoFormMode.fm_UPDATE_MODE && 
                            moGrid.doGetFieldValue(IDH_WOQITEM._SampleRef, pVal.Row).ToString().Length > 0 && 
                            moGrid.doGetFieldValue(IDH_WOQITEM._Sample, pVal.Row).ToString() == "N")
                        {
                            moGrid.doSetFieldValue(pVal.ColUID, "Y");
                            IDHAddOns.idh.addon.Base.PARENT.doMessage("Cannot untick the checkbox. There is a Lab Task linked with the row. Cancel the Lab Task if not required"); 
                            return false; 
                        }

                        //Following action required 
                        // 1 - Check Validations to proceed 
                        // 2 - Create a Sample Item 
                        // 3 - Created a Lab Task against the Sample Item 

                        // 2 - Creating Sample: doCreateLabItem(...) 
                        // 3 - Creating Lab Task: doCreateLabTask(...) 

                        //moved to general, but will moved to Lab task dbobject later on
                        //IDH_LABITM oLabItem = new IDH_LABITM();
                        //IDH_LITMDTL oLIDetail = new IDH_LITMDTL();
                        //bool bResult = false;
                        //IDHAddOns.idh.addon.Base.PARENT.goDICompany.StartTransaction();

                        //bResult = doCreateLabItem(ref pVal.Row, ref oLabItem, ref oLIDetail); //Adding new record in Lab Item and LI Details 
                        //if (bResult)
                        //    bResult = doCreateLabTask(ref pVal.Row, ref oLabItem);

                        bool bResult = false;
                      //  bResult = com.idh.bridge.utils.General.doCreateLabItemNTask(ref pVal.Row, ref moWOQ,true,0);
                        string sSampleRef = "", sSampleStatus = "";
                        bResult = com.idh.bridge.utils.General.doCreateLabItemNTask( moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WasCd),
                            moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WasDsc),
                            moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WasFDsc),
                            moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._UOM),
                            moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._Quantity), moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._Comment),
                            moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._Code), moWOQ.Code, "WOQ",
                            moWOQ.U_CardCd, moWOQ.U_CardNM, false, 0, ref sSampleRef, ref sSampleStatus);
                        if (bResult) {
                            moWOQ.WOQItems.setValue(IDH_WOQITEM._SampleRef, sSampleRef);
                            moWOQ.WOQItems.setValue(IDH_WOQITEM._SampStatus, sSampleStatus);
                        }
                        if (bResult)
                        {
                            //Need to update LabItem as the Object code has been updated by new Lab Task Code 
                            //oLabItem.doUpdateDataRow();

                            if (IDHAddOns.idh.addon.Base.PARENT.goDICompany.InTransaction)
                                IDHAddOns.idh.addon.Base.PARENT.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            IDHAddOns.idh.addon.Base.STATUSBAR.SetText("Process completed successfully.", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                        }
                        else
                            if (IDHAddOns.idh.addon.Base.PARENT.goDICompany.InTransaction)
                            {
                                IDHAddOns.idh.addon.Base.PARENT.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
                            }
                    }*/
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "Error in doGRIDFIELDCHANGED." });
            }
            return true;
        }

        ////private bool doAddUpdateAddress() {
        ////    SAPbobsCOM.BusinessPartners oBP = null;
        ////    SAPbobsCOM.BPAddresses oAddress = null;
        ////    try {

        ////        if (moWOQ.U_CardCd != string.Empty && moWOQ.U_Address != string.Empty) {
        ////            object oAddressLine = null;
        ////            oAddressLine = Config.INSTANCE.doLookupTableField("CRD1", "U_AddrsCd", "CardCode='" + moWOQ.U_CardCd.Replace("'", "''") + "' And Address='" + moWOQ.U_Address.Replace("'", "''") + "' And AdresType='S'", "", true);
        ////            if (moWOQ.DoAddressChanged || oAddressLine == null) {
        ////                //Add Address


        ////                //If sCardCode.Length > 0 Then
        ////                //  Try

        ////                oBP = (SAPbobsCOM.BusinessPartners)IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
        ////                if (oBP.GetByKey(moWOQ.U_CardCd)) {
        ////                    oAddress = oBP.Addresses;
        ////                    if (oAddressLine == null) {
        ////                        oAddress.Add();

        ////                        oAddress.SetCurrentLine(oAddress.Count - 1);
        ////                        oAddress.UserFields.Fields.Item("U_AddrsCd").Value = Config.INSTANCE.GetAddressNextCode();
        ////                    } else if (moWOQ.DoAddressChanged) {
        ////                        int i = 0;

        ////                        for (i = 0; i < oAddress.Count; i++) {
        ////                            oAddress.SetCurrentLine(i);
        ////                            if (oAddress.AddressName == moWOQ.U_Address && oAddress.AddressType == SAPbobsCOM.BoAddressType.bo_ShipTo)
        ////                                break;
        ////                        }


        ////                        if (i == oAddress.Count) {
        ////                            oAddress.Add();

        ////                            oAddress.SetCurrentLine(oAddress.Count - 1);
        ////                            oAddress.UserFields.Fields.Item("U_AddrsCd").Value = Config.INSTANCE.GetAddressNextCode();
        ////                        }
        ////                    }



        ////                    oAddress.AddressName = moWOQ.U_Address;
        ////                    oAddress.Street = moWOQ.U_Street;
        ////                    oAddress.Block = moWOQ.U_Block;
        ////                    oAddress.ZipCode = moWOQ.U_ZpCd;
        ////                    oAddress.City = moWOQ.U_City;
        ////                    oAddress.County = moWOQ.U_County;
        ////                    oAddress.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;

        ////                    oAddress.State = moWOQ.U_State;

        ////                    oAddress.Country = moWOQ.U_Country;


        ////                    int iwResult;
        ////                    string swResult;


        ////                    iwResult = oBP.Update();
        ////                    oAddressLine = Config.INSTANCE.doLookupTableField("CRD1", "U_AddrsCd", "CardCode Like '" + moWOQ.U_CardCd.Replace("'", "''") + "' And Address Like '" + moWOQ.U_Address.Replace("'", "''") + "' And AdresType='S'", "", true);
        ////                    if (oAddressLine != null)
        ////                        moWOQ.U_AddrssLN = Convert.ToString(oAddressLine);
        ////                    if (iwResult != 0) {

        ////                        IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetLastError(out iwResult, out swResult);
        ////                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Adding Address: " + swResult, "ERSYADDA", new string[] { swResult });
        ////                        return false;
        ////                    }
        ////                    return true;
        ////                }





        ////            }
        ////        }


        ////    } catch (Exception ex) {
        ////        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doAddUpdateAddress:Add/Update BP's Address" });
        ////        return false;
        ////    } finally {
        ////        if (oBP != null) {
        ////            object oObj = (object)oBP;
        ////            IDHAddOns.idh.data.Base.doReleaseObject(ref oObj);
        ////            oObj = (object)oAddress;
        ////            IDHAddOns.idh.data.Base.doReleaseObject(ref oObj);
        ////        }
        ////    }


        ////    return true;
        ////}

        //private bool doHandleGridSort(ref IDHAddOns.idh.events.Base pVal) {
        //    return false;
        //}
        //private bool doHandleGridMenuEvent(ref IDHAddOns.idh.events.Base pVal) {
        //    SAPbouiCOM.MenuEvent opVal = (SAPbouiCOM.MenuEvent)pVal.oData;

        //    if ((Mode == BoFormMode.fm_VIEW_MODE && (opVal.MenuUID.Equals("IDHGMADD") || opVal.MenuUID.Equals("IDHGMREML") || opVal.MenuUID.Equals("AddAdItm")))
        //        || ((opVal.MenuUID.Equals("IDHGMADD") || opVal.MenuUID.Equals("IDHGMREML") || opVal.MenuUID.Equals("AddAdItm")) &&
        //              (moWOQ.U_Status != ((int)IDH_WOQHD.en_WOQSTATUS.New).ToString() && moWOQ.U_Status != ((int)IDH_WOQHD.en_WOQSTATUS.ReviewAdvised).ToString()))
        //                        ) {
        //        setSharedData("ROWDELTED", "True");
        //        return false;
        //    }

        //    if (!pVal.BeforeAction && opVal.MenuUID.Equals("AddAdItm")) {
        //        SAPbouiCOM.SelectedRows oSelected = moGrid.getGrid().Rows.SelectedRows;
        //        int iSelectionCount = oSelected.Count;
        //        if (iSelectionCount > 0) {
        //            moWOQ.DoBlockUpdateTrigger = true;
        //            string sPCode = (string)moGrid.doGetFieldValue("Code", pVal.Row);
        //            int iSort = (int)moGrid.doGetFieldValue(IDH_WOQITEM._Sort, pVal.Row);
        //            Freeze(true);
        //            moWOQ.WOQItems.doAddEmptyRow(false, true);
        //            string iCode = (string)moGrid.doGetFieldValue("Code", moGrid.getRowCount() - 1);
        //            string sName = (string)moGrid.doGetFieldValue("Name", moGrid.getRowCount() - 1);
        //            for (int i = moGrid.getRowCount() - 1; i >= pVal.Row + 2; i--) {

        //                for (int icol = 0; icol <= moGrid.Columns.Count - 1; icol++) {
        //                    moGrid.doSetFieldValue(icol, i, moGrid.doGetFieldValue(icol, i - 1));
        //                }
        //            }
        //            for (int icol = 0; icol <= moGrid.Columns.Count - 1; icol++) {
        //                if ((moGrid.Columns.Item(icol).UniqueID == "Code"))
        //                    moGrid.doSetFieldValue(icol, pVal.Row + 1, iCode);
        //                else if ((moGrid.Columns.Item(icol).UniqueID == "Name"))
        //                    moGrid.doSetFieldValue(icol, pVal.Row + 1, sName);
        //                else
        //                    moGrid.doSetFieldValue(icol, pVal.Row + 1, moGrid.doGetFieldValue(icol, moGrid.getRowCount() - 1));
        //            }
        //            for (int i = pVal.Row + 1; i <= moGrid.getRowCount() - 1; i++) {
        //                moGrid.doSetFieldValue(IDH_WOQITEM._Sort, i, i + 1);
        //            }
        //            moGrid.doSetFieldValue(IDH_WOQITEM._PLineID, pVal.Row + 1, sPCode);
        //            moGrid.doSetFieldValue(IDH_WOQITEM._AdtnlItm, pVal.Row + 1, "A");
        //            if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
        //                IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");

        //            for (int i = 0; i <= moGrid.getRowCount() - 1; i++) {
        //                if (moGrid.doGetFieldValue(IDH_WOQITEM._Status, i).ToString() == "1")
        //                    moGrid.Settings.SetRowEditable(i + 1, true);
        //                else
        //                    moGrid.Settings.SetRowEditable(i + 1, false);
        //            }
        //            moWOQ.DoBlockUpdateTrigger = false;
        //            moGrid.doApplyRules();
        //            bool ceditable = moGrid.Columns.Item(IDH_WOQITEM._EnqID).Editable;

        //            Freeze(false);
        //        }
        //    } else if (pVal.BeforeAction && opVal.MenuUID.Equals("IDHGMADD") || opVal.MenuUID.Equals("IDHGMREML")) {
        //        setSharedData("ROWDELTED", "False");
        //        SAPbouiCOM.SelectedRows oSelected = moGrid.getGrid().Rows.SelectedRows;
        //        int iSelectionCount = oSelected.Count;
        //        if (iSelectionCount == 0)
        //            return true;
        //        if (moGrid.doGetFieldValue(IDH_WOQITEM._WasFDsc, pVal.Row).ToString().Trim() != string.Empty && (moGrid.doGetFieldValue(IDH_WOQITEM._Status, pVal.Row).ToString() != "1")) {
        //            return false;
        //        } else if (opVal.MenuUID.Equals("IDHGMREML") &&
        //          moGrid.doGetFieldValue(IDH_WOQITEM._WasFDsc, pVal.Row).ToString().Trim() != string.Empty
        //          && (moGrid.doGetFieldValue(IDH_WOQITEM._Status, pVal.Row).ToString() == "1")
        //          ) {
        //            bool bHasLinkedItems = false;
        //            string sLineCode = moGrid.doGetFieldValue(IDH_WOQITEM._Code, pVal.Row).ToString();
        //            for (int i = pVal.Row; i <= moGrid.getRowCount() - 1; i++) {
        //                if (moGrid.doGetFieldValue(IDH_WOQITEM._PLineID, i).ToString() == sLineCode) {
        //                    bHasLinkedItems = true;
        //                    break;
        //                }
        //            }
        //            if (!bHasLinkedItems)
        //                return true;
        //            string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ERVEQ003", null);
        //            int iRow = pVal.Row;
        //            if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) == 1) {
        //                Freeze(true);
        //                moWOQ.DoBlockUpdateTrigger = true;
        //                moGrid.doRemoveRow(iRow, false, true);
        //                for (int i = moGrid.getRowCount() - 1; i >= iRow; i--) {
        //                    if (moGrid.doGetFieldValue(IDH_WOQITEM._PLineID, i).ToString() == sLineCode) {
        //                        moGrid.doRemoveRow(i, false, true);
        //                        setSharedData("ROWDELTED", "True");
        //                    }
        //                }
        //                for (int i = 0; i <= moGrid.getRowCount() - 1; i++) {
        //                    moGrid.doSetFieldValue(IDH_WOQITEM._Sort, i, i + 1);
        //                    if (moGrid.doGetFieldValue(IDH_WOQITEM._Status, i).ToString() == "1")
        //                        moGrid.Settings.SetRowEditable(i + 1, true);
        //                    else
        //                        moGrid.Settings.SetRowEditable(i + 1, false);
        //                }
        //                moWOQ.DoBlockUpdateTrigger = false;
        //                moWOQ.WOQItems.doRefreashAllRowPrices(false, true);
        //                if (Mode == BoFormMode.fm_OK_MODE)
        //                    Mode = BoFormMode.fm_UPDATE_MODE;
        //                moGrid.doApplyRules();
        //                Freeze(false);
        //                return false;
        //            } else {
        //                setSharedData("ROWDELTED", "True");
        //                return false;
        //            }
        //        }
        //    }
        //    return true;
        //}
        //private bool doHandleGridRightClick(ref IDHAddOns.idh.events.Base pVal) {
        //    if (Mode != BoFormMode.fm_VIEW_MODE && pVal.BeforeAction && (moWOQ.U_Status == ((int)IDH_WOQHD.en_WOQSTATUS.New).ToString()) || moWOQ.U_Status == ((int)IDH_WOQHD.en_WOQSTATUS.ReviewAdvised).ToString()) {
        //        if (pVal.Row >= 0 && moGrid.getSBOGrid().Rows.IsLeaf(pVal.Row)) {
        //            SAPbouiCOM.SelectedRows oSelected = moGrid.getGrid().Rows.SelectedRows;
        //            int iSelectionCount = oSelected.Count;
        //            if (iSelectionCount > 0 && (moGrid.doGetFieldValue(IDH_WOQITEM._WasFDsc, pVal.Row).ToString() != string.Empty || moGrid.doGetFieldValue(IDH_WOQITEM._WasDsc, pVal.Row).ToString() != string.Empty || moGrid.doGetFieldValue(IDH_WOQITEM._WasCd, pVal.Row).ToString() != string.Empty)
        //                && moGrid.doGetFieldValue(IDH_WOQITEM._AdtnlItm, pVal.Row).ToString() == string.Empty
        //                && moGrid.doGetFieldValue(IDH_WOQITEM._Status, pVal.Row).ToString() == "1") {
        //                SAPbouiCOM.MenuItem oMenuItem;
        //                string sRMenuId = IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU;
        //                oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item(sRMenuId);
        //                if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
        //                    IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
        //                if (moGrid.CanEdit) {
        //                    SAPbouiCOM.Menus oMenus;
        //                    int iMenuPos = 1;
        //                    SAPbouiCOM.MenuCreationParams oCreationPackage;
        //                    oCreationPackage = (SAPbouiCOM.MenuCreationParams)IDHAddOns.idh.addon.Base.APPLICATION.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
        //                    oCreationPackage.Enabled = true;
        //                    oMenus = oMenuItem.SubMenus;
        //                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;

        //                    oCreationPackage.UniqueID = "AddAdItm";
        //                    oCreationPackage.String = Translation.getTranslatedWord("Add Additional Item");
        //                    oCreationPackage.Position = iMenuPos;
        //                    iMenuPos += 1;
        //                    oMenus.AddEx(oCreationPackage);
        //                }
        //            } else {
        //                if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
        //                    IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
        //                setWFValue("LASTJOBMENU", null);
        //            }

        //        } else {
        //            if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
        //                IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
        //            setWFValue("LASTJOBMENU", null);
        //        }
        //    } else if (Mode != BoFormMode.fm_VIEW_MODE && !pVal.BeforeAction && (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL || pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL_MULTI)) {
        //        Freeze(true);
        //        moWOQ.WOQItems.doCalculateAllRowsTotal(false);
        //        Freeze(false);
        //    }
        //    return true;
        //}
        //private bool doHandleGridSearch(ref IDHAddOns.idh.events.Base pVal) {
        //    if (!pVal.BeforeAction) {
        //        if (pVal.ColUID == IDH_WOQITEM._WasFDsc && moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WasCd) != string.Empty) {
        //            moWOQ.WOQItems.setValue(IDH_WOQITEM._WasCd, "", false);
        //            moWOQ.WOQItems.setValue(IDH_WOQITEM._WasDsc, "", false);
        //        } else if (pVal.ColUID == IDH_WOQITEM._WstGpNm && moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WstGpCd) != string.Empty) {
        //            moWOQ.WOQItems.setValue(IDH_WOQITEM._WstGpCd, "", false);
        //        }
        //    } else {
        //        if (pVal.ColUID == IDH_WOQITEM._RouteCd && moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._AdtnlItm) == "A") {
        //            moWOQ.WOQItems.setValue(IDH_WOQITEM._RouteCd, "", false);
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        //private bool doHandleGridSearchValueSet(ref IDHAddOns.idh.events.Base pVal) {
        //    if (pVal.BeforeAction) {
        //        if (pVal.ColUID == IDH_WOQITEM._WasFDsc && moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WasCd) != string.Empty) {
        //            string aValue = moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WasCd);
        //        }
        //    }
        //    if (!pVal.BeforeAction) {
        //        if (pVal.ColUID == IDH_WOQITEM._WasFDsc && moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WasCd) != string.Empty) {
        //            IDH_ADITVLD obj = new IDH_ADITVLD();
        //            string sWG = "";
        //            if (moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WstGpCd) != string.Empty) {
        //                sWG = " And U_WstGpCd='" + moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WstGpCd) + "'";
        //            }
        //            int ret = obj.getData("U_ItemCd='" + moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WasCd) + "' " + sWG, "");
        //            if (ret != 0) {
        //                moWOQ.WOQItems.setValue(IDH_WOQITEM._AdtnlItm, "A");
        //                moWOQ.WOQItems.setValue(IDH_WOQITEM._WstGpCd, obj.U_WstGpCd);
        //                if (obj.U_WstGpCd != string.Empty) {
        //                    object oVal = com.idh.bridge.lookups.Config.INSTANCE.doLookupTableField("@ISB_WASTEGRP", "U_WstGpNm", "U_WstGpCd = '" + obj.U_WstGpCd + '\'');
        //                    moWOQ.WOQItems.setValue(IDH_WOQITEM._WstGpNm, com.idh.utils.Conversions.ToString(oVal));
        //                }
        //            } else {
        //                string sAddGrp = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup();
        //                if (getSharedData("ITMSGRPCOD") != null && getSharedData("ITMSGRPCOD").ToString() == sAddGrp) {
        //                    moWOQ.WOQItems.setValue(IDH_WOQITEM._AdtnlItm, "A");
        //                }
        //            }
        //        } //else if (pVal.ColUID == IDH_WOQITEM._RouteCd && moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WasCd) != string.Empty) {
        //          //string sItemFName = (string)Config.INSTANCE.getValueFromOITM(moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._WasCd), "FrgnName");
        //          //if (sItemFName != null) {
        //          //    moWOQ.WOQItems.setValue(IDH_WOQITEM._WasFDsc, sItemFName);
        //          //}
        //          //}
        //    }
        //    return true;
        //}
        //private bool doHandleGridDoubleClick(ref IDHAddOns.idh.events.Base pVal) {
        //    if (pVal.BeforeAction) {
        //        if ((moGrid.doGetFieldValue(IDH_WOQITEM._WasCd, pVal.Row).ToString().Trim() != string.Empty || Mode != BoFormMode.fm_ADD_MODE) && (pVal.ColUID == IDH_WOQITEM._WasCd || pVal.ColUID == IDH_WOQITEM._WasFDsc) && (moGrid.doGetFieldValue(IDH_WOQITEM._WasFDsc, pVal.Row).ToString().Trim() != string.Empty || moGrid.doGetFieldValue(IDH_WOQITEM._WasDsc, pVal.Row).ToString().Trim() != string.Empty)) {//&& moEnquiry.EnquiryItems.getValueAsString(pVal.Row, IDH_ENQITEM._WasCd) == string.Empty) {

        //            com.isb.forms.Enquiry.admin.WasteItemValidations oOOForm = new com.isb.forms.Enquiry.admin.WasteItemValidations(null, SBOForm.UniqueID, null);
        //            oOOForm.iParentRow = pVal.Row;
        //            oOOForm.sEnquiryLineID = moGrid.doGetFieldValue(IDH_WOQITEM._EnqLID, pVal.Row).ToString();
        //            oOOForm.sWOQLineID = moGrid.doGetFieldValue(IDH_WOQITEM._Code, pVal.Row).ToString();

        //            oOOForm.setUFValue("IDH_ITEMCD", moGrid.doGetFieldValue(IDH_WOQITEM._WasCd, pVal.Row));
        //            oOOForm.setUFValue("IDH_ITEMNM", moGrid.doGetFieldValue(IDH_WOQITEM._WasDsc, pVal.Row));

        //            oOOForm.setUFValue("IDH_FRNNAM", moGrid.doGetFieldValue(IDH_WOQITEM._WasFDsc, pVal.Row));

        //            oOOForm.setUFValue("IDH_WSTGRP", moGrid.doGetFieldValue(IDH_WOQITEM._WstGpCd, pVal.Row));//Address ID 50
        //            oOOForm.WasteGroupCode = (string)moGrid.doGetFieldValue(IDH_WOQITEM._WstGpCd, pVal.Row);
        //            oOOForm.EnquiryID = (string)moGrid.doGetFieldValue(IDH_WOQITEM._EnqID, pVal.Row);
        //            oOOForm.WOQID = (string)moGrid.doGetFieldValue(IDH_WOQITEM._JobNr, pVal.Row);
        //            oOOForm.ItemCode = (string)moGrid.doGetFieldValue(IDH_WOQITEM._WasCd, pVal.Row);
        //            oOOForm.ItemFName = (string)moGrid.doGetFieldValue(IDH_WOQITEM._WasFDsc, pVal.Row);
        //            oOOForm.AddItm = (string)moGrid.doGetFieldValue(IDH_WOQITEM._AdtnlItm, pVal.Row);
        //            oOOForm.CardCode = getDFValue(msMainTable, IDH_WOQHD._CardCd).ToString();
        //            oOOForm.ParentFormMode = Mode;
        //            oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateWasteItemOKReturn);
        //            oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateWasteItemCancelReturn);
        //            oOOForm.doShowModal(SBOForm);

        //        } else if (pVal.BeforeAction && (pVal.ColUID == IDH_WOQITEM._EnqID || pVal.ColUID == IDH_WOQITEM._EnqLID) && moGrid.doGetFieldValue(IDH_WOQITEM._EnqID, pVal.Row).ToString().Trim() != string.Empty) {
        //            com.isb.forms.Enquiry.Enquiry oOOForm = new com.isb.forms.Enquiry.Enquiry(null, SBOForm.UniqueID, null);
        //            oOOForm.EnquiryID = moGrid.doGetFieldValue(IDH_WOQITEM._EnqID, pVal.Row).ToString().Trim();
        //            oOOForm.bLoadEnquiry = true;
        //            oOOForm.bLoadReadOnly = true;
        //            oOOForm.doShowModal(SBOForm);
        //            oOOForm.DBEnquiry.SBOForm = oOOForm.SBOForm;
        //        } else if (pVal.BeforeAction && pVal.ColUID == IDH_WOQITEM._SampleRef && moGrid.doGetFieldValue(IDH_WOQITEM._SampleRef, pVal.Row).ToString() != string.Empty) {
        //            ////open Lab Item Form here 
        //            ////IDHAddOns.idh.addon.Base.PARENT.doOpenForm("IDHLABTODO", oForm, false);
        //            //setSharedData("LABITEM", (string)moGrid.doGetFieldValue(IDH_WOQITEM._SampleRef, pVal.Row));
        //            //doOpenModalForm("IDHLABITM");

        //            //20160419: Open Lab Task instead 
        //            setSharedData("LABTASK", (string)moGrid.doGetFieldValue(IDH_WOQITEM._SampleRef, pVal.Row));
        //            doOpenModalForm("IDHLABTODO");

        //        } else if (pVal.BeforeAction && pVal.ColUID == IDH_WOQITEM._WOHID && moGrid.doGetFieldValue(IDH_WOQITEM._WOHID, pVal.Row).ToString() != string.Empty) {
        //            //open WOH Form 
        //            ArrayList oData = new ArrayList();
        //            oData.Add("DoUpdate");
        //            oData.Add(moGrid.doGetFieldValue(IDH_WOQITEM._WOHID, pVal.Row).ToString().Trim());
        //            doOpenModalForm("IDH_WASTORD", oData);
        //        } else if (pVal.BeforeAction && pVal.ColUID == IDH_WOQITEM._WORID && moGrid.doGetFieldValue(IDH_WOQITEM._WORID, pVal.Row).ToString() != string.Empty) {
        //            //Dim sRow As String = getDFValue(oForm, "OCLG", "U_IDHWOR")
        //            //If sRow.Length > 0 Then
        //            ArrayList oData = new ArrayList();
        //            oData.Add("DoUpdate");
        //            oData.Add(moGrid.doGetFieldValue(IDH_WOQITEM._WORID, pVal.Row).ToString().Trim());
        //            setSharedData("ROWCODE", moGrid.doGetFieldValue(IDH_WOQITEM._WORID, pVal.Row).ToString().Trim());
        //            doOpenModalForm("IDHJOBS", oData);
        //        }
        //    }

        //    return true;
        //}

        //private bool doHandleCalculateTotal(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    try {
        //        if (!pVal.BeforeAction)

        //            moWOQ.doCalculateAllRowsTotal(true);
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { ("doHandleCalculateTotal") });
        //    }
        //    return true;
        //}

        //#endregion
        #region Handlers
        /**
         * This will update the Underlying DataLayer and the update the form with the calculated values from the DataLayer
         */
        public void doSynchFormItemData(string sItemId) {
            Freeze(true);

            try {
                FormController.DataBindInfo oBindInfo = getBindInfo(sItemId);
                if (oBindInfo.msTableName != null && oBindInfo.msTableName.Equals(moContract.DBOTableName)) {
                    moContract.doMarkUserUpdate(oBindInfo.msField);
                    if (!moContract.IsActiveRow())
                        moContract.doActivateRow(moContract.CurrentRowIndex);
                }

            } catch (Exception e) {
                DataHandler.INSTANCE.doError(e.Message);
            } finally {
                Freeze(false);
            }
        }
        public bool Handle_ValidateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction && pVal.ActionSuccess) {
                doSynchFormItemData(pVal.ItemUID);
            }
            return true;
        }
        private void Handle_RecordProcessed(idh.dbObjects.DBBase oDBObject) {
            //if (Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE || Mode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE || Mode == SAPbouiCOM.BoFormMode.fm_EDIT_MODE) {
            //    if (Mode == BoFormMode.fm_ADD_MODE && mbFromActivity == false) {
            //        string msg = com.idh.bridge.resources.Messages.getGMessage("INFDOCNO", new string[] { "Enquiry", moEnquiry.Code });
            //        com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            //        doCreateActivity(false);
            //        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
            //        Mode = BoFormMode.fm_ADD_MODE;
            //    } else {
            //        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
            //        doCreateActivity(true);
            //        Mode = BoFormMode.fm_OK_MODE;
            //    }
            //}
        }



        //private bool doFindEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction) {
        //        Freeze(true);
        //        try {
        //            string sHeader = (string)getFormDFValue(IDH_WOQHD._Code, true);
        //            if (!moWOQ.getByKey(sHeader)) {
        //                moGrid.doAddEditLine(true);
        //                moGrid.doApplyRules();
        //                Mode = BoFormMode.fm_FIND_MODE;
        //                doSwitchToFind();
        //                doSetWidths();
        //                BubbleEvent = false;
        //                return false;
        //            } else {
        //                Mode = BoFormMode.fm_OK_MODE;

        //                FillCombos.FillState(Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);
        //                doSwitchToEdit();
        //                moGrid.doApplyRules();
        //                doFillGridCombos();
        //                doSetWidths();
        //                HandleFormStatus();
        //                BubbleEvent = false;
        //                return false;
        //            }
        //        } catch (Exception ex) {
        //            throw (ex);
        //        } finally {
        //            Freeze(false);
        //        }
        //    }

        //    return true;
        //}
        //private bool doSearchBPAddressCFL(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (!pVal.BeforeAction && moWOQ.U_CardCd.Trim() != string.Empty) {
        //        doChooseBPAddress();
        //    }
        //    return true;
        //}
        //private bool doHandleAddressChanged(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction) {
        //        //if (pVal.ItemChanged == false)
        //        //    return true;

        //        if (pVal.ItemUID == "IDH_STATE") {
        //            moWOQ.doMarkUserUpdate(IDH_WOQHD._State, true);
        //            return true;
        //        }
        //        SAPbouiCOM.Item oItem = SBOForm.Items.Item(pVal.ItemUID);
        //        SAPbouiCOM.EditText oTxt = (SAPbouiCOM.EditText)oItem.Specific;
        //        switch (pVal.ItemUID) {
        //            case "IDH_ADR1":
        //                moWOQ.doMarkUserUpdate(IDH_WOQHD._Address, true);
        //                break;
        //            case "IDH_STREET":
        //                moWOQ.doMarkUserUpdate(IDH_WOQHD._Street, true);
        //                break;
        //            case "IDH_BLOCK":
        //                moWOQ.doMarkUserUpdate(IDH_WOQHD._Block, true);
        //                break;
        //            case "IDH_CITY":
        //                moWOQ.doMarkUserUpdate(IDH_WOQHD._City, true);
        //                break;
        //            case "IDH_PCODE":
        //                moWOQ.doMarkUserUpdate(IDH_WOQHD._ZpCd, true);
        //                break;
        //            case "IDH_COUNTY":
        //                moWOQ.doMarkUserUpdate(IDH_WOQHD._County, true);
        //                break;
        //            case "IDH_STATE":
        //                moWOQ.doMarkUserUpdate(IDH_WOQHD._State, true);
        //                break;
        //        }
        //    }
        //    return true;
        //}
        //private bool doHandleCountryCombo(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (!pVal.BeforeAction) {

        //        FillCombos.FillState(Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);

        //    }
        //    return true;
        //}
        ////private bool dovalidateBPEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        ////    if (pVal.BeforeAction && (pVal.ItemUID== "IDH_CARDCD" && moContract.U_CardCd.ToString().Trim().IndexOf("*") > -1)&&(pVal.ItemUID == "IDH_CARDNM" && moContract.U_CardNm.ToString().Trim().IndexOf("*") > -1)) {
        ////        doChooseBPCode(pVal.ItemUID);
        ////    }
        ////    return true;
        ////}
        private bool dovalidateBPChangeEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction && (pVal.ItemUID == "IDH_CARDCD" && moContract.U_CardCd.ToString().Trim().IndexOf("*") > -1) && (pVal.ItemUID == "IDH_CARDNM" && moContract.U_CardNm.ToString().Trim().IndexOf("*") > -1)) {
                doChooseBPCode(pVal.ItemUID);
            } else if (pVal.BeforeAction && moContract.U_CardNm.ToString().Trim() == string.Empty) {
                moContract.doMarkUserUpdate(IDH_CONTRACT._CardNm);
                // moWOQ.U_CardCd= "";
            } else if (pVal.BeforeAction && moContract.U_CardCd.ToString().Trim() == string.Empty) {
                moContract.doMarkUserUpdate(IDH_CONTRACT._CardCd);
                // moWOQ.U_CardCd= "";
            }
            return false;
        }

        //public bool doCreateBP(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction) {
        //        if (Mode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
        //            com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage("UNSAVED");
        //            BubbleEvent = false;
        //        } else if (moWOQ.U_CardCd.Trim() != string.Empty &&
        //            Config.INSTANCE.doGetBPName(moWOQ.U_CardCd.Trim(), false) != string.Empty) {
        //            com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage(("ERPKEXIS"), new string[] { moWOQ.U_CardCd.Trim() });
        //            BubbleEvent = false;
        //        }
        //    } else {
        //        setFocus("IDH_ADR1");
        //        com.isb.forms.Enquiry.CreateBP oOOForm = new com.isb.forms.Enquiry.CreateBP(null, SBOForm.UniqueID, null);
        //        //string sAddress = (getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Address).ToString() + " " +
        //        //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Street).ToString() + " " +
        //        //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._City).ToString() + " " +
        //        //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Country).ToString() + " " +
        //        //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._State).ToString() + " " +
        //        //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._ZipCode).ToString()).Replace("  ", " ").Trim();
        //        oOOForm.WOQID = moWOQ.Code.Trim();
        //        oOOForm.setUFValue("IDH_CARDCD", moWOQ.U_CardCd.Trim());
        //        oOOForm.setUFValue("IDH_CARDNM", moWOQ.U_CardNM.Trim());

        //        oOOForm.setUFValue("IDH_CARDTY", Config.INSTANCE.getParameterWithDefault("DFEQBPTP", "C").ToString());

        //        oOOForm.setUFValue("IDH_ADRID", moWOQ.U_Address.Trim());//Address ID 50
        //        oOOForm.setUFValue("IDH_STREET", moWOQ.U_Street.Trim());//Street 100
        //        oOOForm.setUFValue("IDH_BLOCK", moWOQ.U_Block.Trim());//Block 100
        //        oOOForm.setUFValue("IDH_CITY", moWOQ.U_City.Trim());//City 100
        //        oOOForm.setUFValue("IDH_PCODE", moWOQ.U_ZpCd.Trim());//State 3
        //        oOOForm.setUFValue("IDH_COUNTY", moWOQ.U_County.Trim());//PostCode 20
        //        oOOForm.setUFValue("IDH_STATE", moWOQ.U_State.Trim());//County 100
        //        oOOForm.setUFValue("IDH_CONTRY", moWOQ.U_Country.Trim());//Country 3

        //        oOOForm.setUFValue("IDH_CNTPRS", moWOQ.U_Contact.Trim());//Contact Person 50
        //        oOOForm.setUFValue("IDH_EMAIL", moWOQ.U_E_Mail.Trim());//Email 100
        //        oOOForm.setUFValue("IDH_PHONE", moWOQ.U_Phone1.Trim());//Phone 20
        //        oOOForm.setUFValue("IDH_INFO", "Address:");//Phone 20
        //        oOOForm.setUFValue("IDH_INFO2", "Contact:");//Phone 20
        //        oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateBPOKReturn);
        //        oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateBPCancelReturn);
        //        oOOForm.doShowModal(SBOForm);
        //    }
        //    return true;
        //}
        //public bool doCopyFromENQ(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    setSharedData("IDH_BPNAME", moWOQ.U_CardNM);
        //    setSharedData("IDH_BPCODE", moWOQ.U_CardCd);
        //    setSharedData("IDH_SOURCE", "WOQ");
        //    if (moGrid.getRowCount() > 0) {
        //        string aExistingEnquiryRows = "";// new ArrayList();
        //        if (moGrid.doGetFieldValue(IDH_WOQITEM._EnqLID, 0).ToString() != string.Empty)
        //            aExistingEnquiryRows += (moGrid.doGetFieldValue(IDH_WOQITEM._EnqLID, 0));
        //        for (int i = 1; i <= moGrid.getRowCount() - 1; i++) {
        //            if (moGrid.doGetFieldValue(IDH_WOQITEM._EnqLID, i).ToString() != string.Empty)
        //                aExistingEnquiryRows += " ," + (moGrid.doGetFieldValue(IDH_WOQITEM._EnqLID, i));
        //        }

        //        setSharedData("IDH_EXISTINGENQLINES", aExistingEnquiryRows);
        //    }

        //    moaSelectItems = null;

        //    doOpenModalForm("IDHSRENQ");

        //    return true;
        //}
        //protected bool doHandleDocButton(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction)
        //        return true;
        //    string sReportName = Config.INSTANCE.getParameterWithDefault("WOQDOC", "");
        //    if (sReportName.ToLower().EndsWith(".rpt")) {
        //        Hashtable oParams = new Hashtable();
        //        //oParams.Add("OrdNum", sCode)
        //        oParams.Add("RowNum", moWOQ.Code);
        //        string sCallerForm = SBOForm.TypeEx;
        //        IDHAddOns.idh.report.Base.doCallReportDefaults(SBOForm, sReportName, "TRUE", "FALSE", "1", ref oParams, ref sCallerForm);
        //    } else if (sReportName != string.Empty) {
        //        setSharedData("WOQDOCCODES", sReportName);
        //        setSharedData("ROWNUM", moWOQ.Code);
        //        doOpenModalForm("IDHWOQRPTS");
        //    }
        //    return true;
        //}

        //protected bool doShowQuestionnaire(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    setSharedData("ENQID", 0);
        //    setSharedData("WOQID", moWOQ.Code);
        //    doOpenModalForm("IDH_ENQQS");

        //    return true;
        //}

        //protected bool doFindAddress(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (!pVal.BeforeAction) {
        //        com.uBC.forms.fr3.PostCode oOOForm = new com.uBC.forms.fr3.PostCode(null, SBOForm.UniqueID, null);
        //        string sAddress = (moWOQ.U_Address + " " +
        //                            moWOQ.U_Street + " " +
        //                            moWOQ.U_City + " " +
        //                            moWOQ.U_Country + " " +
        //                            moWOQ.U_State + " " +
        //                            moWOQ.U_ZpCd).Replace("  ", " ").Trim();

        //        oOOForm.setUFValue("IDH_ADDRES", sAddress);
        //        oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandlePostCodeOKReturn);
        //        oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandlePostCodeCancelReturn);
        //        oOOForm.doShowModal(SBOForm);
        //    }
        //    return true;
        //}
        //private bool doRefeshContact(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (!pVal.BeforeAction && moWOQ.U_CardCd.Trim() != string.Empty && (Mode == BoFormMode.fm_OK_MODE || Mode == BoFormMode.fm_UPDATE_MODE || Mode == BoFormMode.fm_ADD_MODE)) {
        //        if (moWOQ.U_Contact != "") {
        //            if (com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYNAsk("ENQCNREF", null, 2) != 1) {
        //                return true;
        //            }
        //        }
        //        ArrayList oContactData = Config.INSTANCE.doGetBPContactEmployee(moWOQ.U_CardCd, "DEFAULTCONTACT");
        //        if (oContactData != null) {
        //            moWOQ.U_Contact = oContactData[0].ToString();
        //            moWOQ.U_Phone1 = oContactData[4].ToString();
        //            moWOQ.U_E_Mail = oContactData[7].ToString();
        //            if (Mode != BoFormMode.fm_ADD_MODE)
        //                Mode = BoFormMode.fm_UPDATE_MODE;
        //        } else
        //            DataHandler.INSTANCE.doUserError("Failed to find default contact.");
        //    }
        //    return true;
        //}
        //public bool doCancelButton(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction && (Mode == BoFormMode.fm_UPDATE_MODE || (Mode == BoFormMode.fm_ADD_MODE && moWOQ.doFormHaveSomeData()))) {
        //        if (com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", null) == 2) {
        //            BubbleEvent = false;
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        ////public bool doCancelButton(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        ////    if (!pVal.BeforeAction) {
        ////        return false;
        ////    }
        ////    return true;
        ////}

        //private bool doHandleFormDataLoad(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.ActionSuccess && pVal.BeforeAction == false) {
        //        Mode = BoFormMode.fm_OK_MODE;
        //        setFocus("IDH_ADR1");
        //        EnableItem(false, "IDH_DOCNUM");
        //        doSetWidths();
        //    }
        //    return true;
        //}

        //private bool dovalidateFieldsForPriceChange(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    try {
        //        if (!bHandleValidateEvent)
        //            return true;
        //        if (pVal.BeforeAction && (pVal.ItemUID == "IDH_ADR1" || pVal.ItemUID == "IDH_PCODE")) {
        //            doHandleAddressChanged(ref pVal, ref BubbleEvent);
        //        }
        //        if (!pVal.BeforeAction && pVal.ItemChanged && moWOQ.WOQItems.Count > 0 && (moWOQ.U_CardCd != string.Empty || moWOQ.WOQItems.U_WasCd != string.Empty)) {
        //            Freeze(true);
        //            moWOQ.WOQItems.doRefreashAllRowPrices(false, true);
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("dovalidateFieldsForPriceChange") });
        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}
        //private bool doHanleNoVATCheckBox(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    try {
        //        if (!pVal.BeforeAction && moWOQ.WOQItems.Count > 0 && (moWOQ.U_CardCd != string.Empty || moWOQ.WOQItems.U_WasCd != string.Empty)) {
        //            Freeze(true);
        //            moWOQ.WOQItems.doCalculateAllRowsTotal(true);
        //        }

        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doHanleNoVATCheckBox") });
        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}

        //private bool doHandleBranchCombo(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    try {
        //        if (!pVal.BeforeAction && pVal.ItemChanged && moWOQ.WOQItems.Count > 0) {
        //            //IDH_BRANCH
        //            //moWOQ.WOQItems.doRefreashAllRowValueforField(sFieldname, oNewValue, false);
        //            //moWOQ.WOQItems.doRefreashAllRowValueforField(IDH_WOQITEM._Branch, moWOQ.U_Branch, false);
        //            Freeze(true);
        //            moWOQ.WOQItems.setValueForAllRows(IDH_WOQITEM._Branch, moWOQ.U_Branch);
        //            moWOQ.WOQItems.doRefreashAllRowPrices(false, true);
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doHandleBranchCombo") });
        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}
        //private bool doHandleItemGroupCombo(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    try {
        //        if (!pVal.BeforeAction && pVal.ItemChanged && moWOQ.WOQItems.Count > 0) {
        //            //IDH_BRANCH
        //            //moWOQ.WOQItems.doRefreashAllRowValueforField(sFieldname, oNewValue, false);
        //            Freeze(true);
        //            doFillJobTypeCombo();
        //            //string sContainerUID = "SRC*IDHISRC(CONTCD)[IDH_ITMCOD;IDH_ITMNAM=;IDH_GRPCOD=" + moWOQ.U_ItemGrp + "][ITEMCODE;" + IDH_WOQITEM._ItemDsc + "=ITEMNAME]";               
        //            //moGrid.doAddListField(IDH_WOQITEM._ItemCd, "Container Code", true, 100, sContainerUID, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
        //            moWOQ.WOQItems.setValueForAllRows(IDH_WOQITEM._ItmGrp, moWOQ.U_ItemGrp);
        //            // moWOQ.WOQItems.doRefreashAllRowValueforField(IDH_WOQITEM._ItmGrp, moWOQ.U_ItemGrp, false);
        //            moWOQ.WOQItems.doRefreashAllRowPrices(false, true);
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doHandleItemGroupCombo") });
        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}

        //public override bool doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
        //    if (!pVal.BeforeAction && pVal.MenuUID == IDHAddOns.idh.lookups.Base.DUPLICATE) {

        //        Freeze(true);
        //        string sCode = moWOQ.Code;
        //        moWOQ.DoNotSetDefault = true;
        //        doHandleDuplicate = true;
        //        Mode = BoFormMode.fm_ADD_MODE;
        //        moWOQ.DoNotSetDefault = false;
        //        Freeze(true);
        //        moGrid.AddEditLine = true;
        //        //moWOQ.getByKey(sCode);

        //        moWOQ.duplicateDocument(sCode);

        //        setFocus("IDH_CardNM");
        //        setVisible("IDH_SNDAPR", (moWOQ.U_AppRequired == "Y"));
        //        moGrid.doApplyRules();
        //        doFillGridCombos();
        //        Freeze(false);




        //    }
        //    if (getSharedData("ROWDELTED") != null && getSharedData("ROWDELTED").ToString() == "True") {
        //        BubbleEvent = false;
        //        setSharedData("ROWDELTED", "False");
        //        return false;
        //    }
        //    return true;
        //}

        private void Handle_DocLoaded(idh.dbObjects.DBBase oCaller) {
            //moContract.DoAddressChanged = false;
            EnableItem(false, "IDH_CODE");
            //string sCardName = "";
            //if (doValidateBPbyCode(ref sCardName) == true) {
            //    EnableItem(false, "IDH_CRETBP");
            //} else
            //    EnableItem(true, "IDH_CRETBP");
        }
        private void Handle_DocItemsLoaded(idh.dbObjects.DBBase oCaller) {
            //FillCombos.FillState(Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);
            //moEnquiry.doLoadChildren();
            if (moContract.ContractItems.Count == 0) {
                moGrid.doAddEditLine(true);
            }
            moGrid.doApplyRules();
            doFillGridCombos();

            //EnableItem(false, "IDH_DOCNUM");
            //string sCardName = "";
            //if (doValidateBPbyCode(ref sCardName) == true) {
            //    EnableItem(false, "IDH_CRETBP");
            //} else
            //    EnableItem(true, "IDH_CRETBP");
            //moEnquiry.DoAddressChanged = false;
            ////  doSetWidths();
        }
        //#endregion
        //#region CreateBP
        //public bool doHandleCreateBPOKReturn(com.idh.forms.oo.Form oDialogForm) {
        //    try {
        //        com.isb.forms.Enquiry.CreateBP oOOForm = (com.isb.forms.Enquiry.CreateBP)oDialogForm;
        //        SAPbouiCOM.Form oForm = oOOForm.SBOParentForm;
        //        Freeze(true);
        //        moWOQ.U_CardCd = oOOForm.CardCode;
        //        //doCreateActivity(true);
        //        if (moWOQ.U_ClgCode != 0) {
        //            moWOQ.doCreateActivity(true);
        //        }
        //        setFocus("IDH_ADR1");

        //        //oForm.Mode =SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
        //        EnableItem(false, "IDH_CRETBP");
        //        EnableItem(false, "IDH_CardNM");
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "Error in HandleCreateBP OK" });

        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}
        //public bool doHandleCreateBPCancelReturn(com.idh.forms.oo.Form oDialogForm) {
        //    com.isb.forms.Enquiry.CreateBP oOOForm = (com.isb.forms.Enquiry.CreateBP)oDialogForm;
        //    if (oOOForm.CardCode.Trim() != string.Empty) {
        //        SAPbouiCOM.Form oForm = oOOForm.SBOParentForm;
        //        moWOQ.U_CardCd = oOOForm.CardCode;
        //        if (moWOQ.U_ClgCode != 0) {
        //            moWOQ.doCreateActivity(true);
        //        }
        //        setFocus("IDH_ADR1");
        //        //oForm.Mode =SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
        //        EnableItem(false, "IDH_CRETBP");
        //        EnableItem(false, "IDH_CardNM");
        //    }
        //    return true;
        //}
        //#endregion
        //#region Postcode
        //public bool doHandlePostCodeOKReturn(com.idh.forms.oo.Form oDialogForm) {
        //    try {
        //        com.uBC.forms.fr3.PostCode oOOForm = (com.uBC.forms.fr3.PostCode)oDialogForm;
        //        string Company = oOOForm.Company;
        //        string SubBuilding = oOOForm.SubBuilding;
        //        string BuildingNumber = oOOForm.BuildingNumber;
        //        string BuildingName = oOOForm.BuildingName;
        //        string SecondaryStreet = oOOForm.SecondaryStreet;
        //        string Street = oOOForm.Street;
        //        string Block = oOOForm.Block;
        //        ////Neighbourhood
        //        string District = oOOForm.District;
        //        string City = oOOForm.City;
        //        string Line1 = oOOForm.Line1;
        //        string Line2 = oOOForm.Line2;
        //        string Line3 = oOOForm.Line3;
        //        string Line4 = oOOForm.Line4;
        //        string Line5 = oOOForm.Line5;
        //        string AdminAreaName = oOOForm.AdminAreaName;
        //        ////AdminAreaCode
        //        ////Province
        //        string ProvinceName = oOOForm.ProvinceName;
        //        string ProvinceCode = oOOForm.ProvinceCode;
        //        string PostalCode = oOOForm.PostalCode;
        //        string CountryName = oOOForm.CountryName;
        //        string CountryIso2 = oOOForm.CountryIso2;
        //        ////CountryIso3
        //        ////CountryIsoNumber
        //        ////SortingNumber1
        //        ////SortingNumber2
        //        ////Barcode
        //        string POBoxNumber = oOOForm.POBoxNumber;
        //        string Label = oOOForm.Label;
        //        string Type = oOOForm.Type;
        //        SAPbouiCOM.Form oForm = oOOForm.SBOParentForm;
        //        Freeze(true);
        //        moWOQ.DoBlockUpdateTrigger = true;
        //        moWOQ.U_Address = (SubBuilding + ' ' + BuildingNumber + ' ' + BuildingName).Replace("  ", " ");
        //        moWOQ.U_AddrssLN = "";
        //        moWOQ.U_Street = (SecondaryStreet + Street);
        //        moWOQ.U_Block = Block;
        //        moWOQ.U_City = City;
        //        moWOQ.U_ZpCd = PostalCode;
        //        moWOQ.U_County = AdminAreaName;


        //        moWOQ.U_State = ProvinceCode;
        //        moWOQ.U_Country = CountryIso2;

        //        com.idh.bridge.DataRecords oRecords = null;
        //        try {
        //            string sQry = "Select Code from OCRY where Code Like '" + CountryIso2 + "' ";
        //            if (CountryName != string.Empty) {
        //                oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("dogetcountry" + CountryName, sQry);
        //                if (oRecords != null && oRecords.RecordCount > 0) {
        //                    SAPbouiCOM.ComboBox oCOmbo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_CUNTRY").Specific;
        //                    if (oCOmbo.Selected == null || (oCOmbo.Selected != null && oCOmbo.Selected.Value != CountryIso2)) {
        //                        oCOmbo.SelectExclusive(CountryIso2, SAPbouiCOM.BoSearchKey.psk_ByValue);
        //                        FillCombos.FillState(oForm.Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);
        //                    }
        //                }
        //                if (ProvinceCode != string.Empty) {
        //                    sQry = "Select Code from OCST where Code Like '" + ProvinceCode + "' And Country='" + CountryIso2 + "' ";
        //                    oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("dogetstate" + ProvinceCode, sQry);
        //                    if (oRecords != null && oRecords.RecordCount > 0) {
        //                        SAPbouiCOM.ComboBox oCOmbo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_STATE").Specific;
        //                        if ((oCOmbo.Selected == null) || (oCOmbo.Selected != null && oCOmbo.Selected.Value != ProvinceCode)) {
        //                            oCOmbo.SelectExclusive(ProvinceCode, SAPbouiCOM.BoSearchKey.psk_ByValue);
        //                        }
        //                    }
        //                }
        //            }
        //        } catch (Exception ex) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
        //        } finally {

        //        }

        //        moWOQ.DoBlockUpdateTrigger = false;
        //        if (Mode == BoFormMode.fm_OK_MODE)
        //            Mode = BoFormMode.fm_UPDATE_MODE;

        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "HandlePostCodeOKReturn" });
        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}
        //public bool doHandlePostCodeCancelReturn(com.idh.forms.oo.Form oDeialogForm) {
        //    com.uBC.forms.fr3.PostCode oOOForm = (com.uBC.forms.fr3.PostCode)oDeialogForm;

        //    return true;
        //}
        //#endregion
        //#region CreateWasteItem
        //public bool doHandleCreateWasteItemOKReturn(com.idh.forms.oo.Form oDialogForm) {
        //    com.isb.forms.Enquiry.admin.WasteItemValidations oOOForm = (com.isb.forms.Enquiry.admin.WasteItemValidations)oDialogForm;

        //    moGrid.doSetFieldValue(IDH_WOQITEM._WasCd, oOOForm.iParentRow, oOOForm.ItemCode);
        //    moGrid.doSetFieldValue(IDH_WOQITEM._WasDsc, oOOForm.iParentRow, oOOForm.ItemName);
        //    if (oOOForm.ItemFName != moGrid.doGetFieldValue(IDH_WOQITEM._WasFDsc, oOOForm.iParentRow).ToString())
        //        moGrid.doSetFieldValue(IDH_WOQITEM._WasFDsc, oOOForm.iParentRow, oOOForm.ItemFName);
        //    mbENQRESULT = true;

        //    //if (Mode != BoFormMode.fm_ADD_MODE)
        //    //   Mode = BoFormMode.fm_UPDATE_MODE;
        //    return true;
        //}
        //public bool doHandleCreateWasteItemCancelReturn(com.idh.forms.oo.Form oDialogForm) {
        //    com.isb.forms.Enquiry.admin.WasteItemValidations oOOForm = (com.isb.forms.Enquiry.admin.WasteItemValidations)oDialogForm;

        //    if (oOOForm.bUpdateParent) {
        //        {
        //            moGrid.doSetFieldValue(IDH_WOQITEM._WasCd, oOOForm.iParentRow, oOOForm.ItemCode);
        //            moGrid.doSetFieldValue(IDH_WOQITEM._WasDsc, oOOForm.iParentRow, oOOForm.ItemName);
        //            if (oOOForm.ItemFName != moGrid.doGetFieldValue(IDH_WOQITEM._WasFDsc, oOOForm.iParentRow).ToString())
        //                moGrid.doSetFieldValue(IDH_WOQITEM._WasFDsc, oOOForm.iParentRow, oOOForm.ItemFName);

        //        }
        //        mbENQRESULT = true;
        //        //if (Mode != BoFormMode.fm_ADD_MODE)
        //        //    Mode = BoFormMode.fm_UPDATE_MODE;
        //    }
        //    return true;
        //}
        //#endregion
        //#region EventHandlers

        public bool doHandleModeChange(SAPbouiCOM.BoFormMode oSBOMode) {
            Freeze(true);
            if (oSBOMode == BoFormMode.fm_ADD_MODE) {
                if (doHandleDuplicate) {
                    Freeze(true);
                    bHandleValidateEvent = false;
                    doSwitchToEdit();
                    EnableItem(true, "IDH_CPYENQ");
                    doClearFormDFValues();
                    moGrid.doEnabled(true);
                    moContract.doClearBuffers();
                    //doSetWidths();
                    bHandleValidateEvent = true;
                    doHandleDuplicate = false;
                    //doActivateTotalButton(false);
                    return true;
                } else {
                    Freeze(true);
                    bHandleValidateEvent = false;
                    doSwitchToEdit();
                    EnableItem(true, "IDH_CPYENQ");
                    doClearFormDFValues();
                    moGrid.doEnabled(true);
                    moContract.doClearBuffers();
                    
                    //doHandleGridColumns();
                    moGrid.doApplyRules();
                    doFillGridCombos();
                    //FillCombos.FillState(Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);
                    //setFocus("IDH_CardNM");
                    //setVisible("IDH_SNDAPR", (moWOQ.U_AppRequired == "Y"));
                    //doSetWidths();
                    //doActivateTotalButton(false);
                    bHandleValidateEvent = true;
                }
                doHandleDuplicate = false;
            } else if (oSBOMode == BoFormMode.fm_FIND_MODE) {
                //Freeze(true); 
                //doSwitchToFind();
                setVisible("IDH_SNDAPR", false);
                doClearFormDFValues();
                //doSetWidths();
                //doActivateTotalButton(false);
                
            } else if (oSBOMode == BoFormMode.fm_OK_MODE || oSBOMode == BoFormMode.fm_UPDATE_MODE) {
                //Freeze(true); 
                if (moContract.U_CardCd != string.Empty) EnableItem(false, "IDH_CardNM");
                //EnableItem(false, "IDH_DOCNUM");


                EnableItem(true, "IDH_CPYENQ");
                //setVisible("IDH_SNDAPR", (moWOQ.U_AppRequired == "Y"));
            } else if (oSBOMode == BoFormMode.fm_VIEW_MODE) {
                //Freeze(true); 
                SBOForm.EnableMenu("1282", true);
            }
            Freeze(false);
            return true;
        }
        public bool doMenuAddEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction && (Mode == BoFormMode.fm_UPDATE_MODE || (Mode == BoFormMode.fm_ADD_MODE && moContract.doFormHaveSomeData())) && com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", null) == 2) {
                BubbleEvent = false;
                return false;
            } else if (!pVal.BeforeAction) {
                moContract.doClearBuffers();
                Mode = BoFormMode.fm_ADD_MODE;
            }
            return true;
        }

        public bool doMenuFindBUttonEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction && (Mode == BoFormMode.fm_UPDATE_MODE || (Mode == BoFormMode.fm_ADD_MODE && moContract.doFormHaveSomeData())) && com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", null) == 2) {
                BubbleEvent = false;
                return false;
            } else if (!pVal.BeforeAction) {
                Mode = BoFormMode.fm_FIND_MODE;
            }
            return true;
        }
        public bool doMenuBrowseEvents(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if ((Mode == BoFormMode.fm_UPDATE_MODE || (Mode == BoFormMode.fm_ADD_MODE && moContract.doFormHaveSomeData())) && com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", null) == 2) {
                    BubbleEvent = false;
                    return false;
                } else
                    Freeze(true);
            } else {
                //FillCombos.FillState(Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);
                moContract.doLoadChildren();
                if (moContract.ContractItems.Count == 0) {
                    moGrid.doAddEditLine(true);
                }
                moGrid.doApplyRules();
                doFillGridCombos();
                EnableItem(false, "IDH_CODE");
                //string sCardName = "";
                //doSetWidths();
                //HandleFormStatus();
                //moContract.DoAddressChanged = false;
                Freeze(false);
            }
            return true;
        }
        //public override void doHandleModalCanceled(string sModalFormType) {
        //    if (sModalFormType == "IDHCSRCH" && getSharedData("TRG") != null && getSharedData("TRG").ToString() == "BP")//BP Search
        //    {
        //        if (moWOQ.U_CardCd.Trim() != string.Empty) {
        //            string sCardName = Config.INSTANCE.doGetBPName(moWOQ.U_CardCd.Trim(), false);
        //            if (!moWOQ.U_CardNM.Trim().Equals(sCardName))
        //                moWOQ.U_CardCd = "";
        //        }

        //    } else if (sModalFormType == "IDHASRCH" || sModalFormType == "IDHASRCH3") {
        //        setFocus("IDH_ADR1");

        //    }
        //}
        public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {
            if (sModalFormType == "IDHCSRCH" && getSharedData("TRG").ToString() == "BP")//BP
            {
                moContract.U_CardCd = getSharedData("CARDCODE").ToString();
                moContract.U_CardNm = getSharedData("CARDNAME").ToString();

                //ArrayList oData = Config.INSTANCE.doGetBPContactEmployee(getSharedData("CARDCODE").ToString(), "DEFAULTCONTACT");
                //if (oData != null) {
                //    moWOQ.U_Contact = oData[0].ToString();
                //    moWOQ.U_Phone1 = oData[4].ToString();
                //    moWOQ.U_E_Mail = oData[7].ToString();
                //}
                if (Mode == BoFormMode.fm_OK_MODE)
                    Mode = BoFormMode.fm_UPDATE_MODE;

            } 
            //else if (sModalFormType == "IDHASRCH" || sModalFormType == "IDHASRCH3")//BP Address
            //  {
            //    moWOQ.DoBlockUpdateTrigger = true;
            //    moWOQ.DoAddressChanged = false;
            //    moWOQ.U_Address = (string)getSharedData("ADDRESS");
            //    moWOQ.U_AddrssLN = getSharedData("ADDRSCD").ToString();
            //    moWOQ.U_Block = (string)getSharedData("BLOCK");
            //    moWOQ.U_Street = (string)getSharedData("STREET");
            //    moWOQ.U_City = (string)getSharedData("CITY");
            //    moWOQ.U_State = (string)getSharedData("STATE");
            //    moWOQ.U_ZpCd = (string)getSharedData("ZIPCODE");
            //    moWOQ.U_Country = (string)getSharedData("COUNTRY");
            //    moWOQ.U_County = (string)getSharedData("COUNTY");
            //    moWOQ.DoBlockUpdateTrigger = false;
            //    if (Mode == BoFormMode.fm_OK_MODE)
            //        Mode = BoFormMode.fm_UPDATE_MODE;


            //} else if (sModalFormType == "IDHSRENQ")//Enquiry Search
            //  {
            //    System.Collections.ArrayList aSelectItems = (ArrayList)getSharedData("IDH_SELENQIDS");
            //    if (aSelectItems == null || aSelectItems.Count == 0)
            //        return;
            //    moaSelectItems = aSelectItems;

            //    doLoadBySelectedEnquiryLines();



            //    moaSelectItems = null;
            //}
        }
        //public void HandleFormDataEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, ref bool BubbleEvent) {
        //    try {
        //        if (BusinessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_LOAD && BusinessObjectInfo.ActionSuccess && BusinessObjectInfo.BeforeAction == false) {
        //            Mode = BoFormMode.fm_OK_MODE;
        //            setFocus("IDH_ADR1");

        //            EnableItem(false, "IDH_DOCNUM");
        //            if (moWOQ.U_CardCd != string.Empty) EnableItem(false, "IDH_CardNM");
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHFE", null);
        //    }

        //}

        //#endregion

        //private void doClearAll() {
        //    doClearFormDFValues();
        //}

        //#region "Form Save/Update"
        ////private bool doValidate(SAPbouiCOM.Form oForm) {
        ////    try {

        ////        if (moWOQ.U_CardNM.Trim() == string.Empty) {

        ////            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Business Partner is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Business Partner") });
        ////            return false;
        ////        }

        ////        if (moWOQ.U_Address.Trim() == string.Empty) {
        ////            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Address is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Address") });
        ////            return false;
        ////        }

        ////        if (moWOQ.U_ZpCd.Trim() == string.Empty) {
        ////            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Postcode is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Postcode") });
        ////            //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value for Address field.");
        ////            return false;
        ////        }

        ////        if (moWOQ.U_City.Trim() == string.Empty) {
        ////            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: City is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("City") });
        ////            //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value for City field.");
        ////            return false;
        ////        }


        ////        if (moWOQ.U_Country.Trim() == string.Empty) {
        ////            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Country is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Country") });
        ////            //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value for Country field.");
        ////            return false;
        ////        }

        ////        if (moWOQ.U_Contact.Trim() == string.Empty) {
        ////            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Contact person is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Contact person") });
        ////            //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value(s) for contact details.");
        ////            return false;
        ////        }

        ////        if (moWOQ.U_Phone1.Trim() == string.Empty && moWOQ.U_E_Mail.Trim() == string.Empty) {
        ////            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Contact detail(s) is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Contact detail(s)") });
        ////            //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value(s) for contact details.");
        ////            return false;
        ////        }


        ////        for (int i = 0; i <= moGrid.getRowCount() - 2; i++) {

        ////            if ((string)moGrid.doGetFieldValue(IDH_WOQITEM._WasFDsc, i) == string.Empty && (string)moGrid.doGetFieldValue(IDH_WOQITEM._WasDsc, i) == string.Empty) {
        ////                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Item is missing in row " + (i + 1).ToString(), "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Item") });
        ////                moGrid.getSBOGrid().SetCellFocus(i, moGrid.doFieldIndex(IDH_WOQITEM._WasFDsc));
        ////                return false;

        ////            } else if ((string)moGrid.doGetFieldValue(IDH_WOQITEM._UOM, i) == string.Empty) {
        ////                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: UOM in row " + (i + 1).ToString(), "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("UOM") });
        ////                moGrid.getSBOGrid().SetCellFocus(i, moGrid.doFieldIndex(IDH_WOQITEM._UOM));
        ////                return false;
        ////            }
        ////        }












        ////    } catch (Exception ex) {
        ////        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "Error in Validating WOQ." });
        ////        return false;
        ////    }



        ////    return true;
        ////}


        //#endregion
        //#region "Form specific methods"
        //private void doLoadBySelectedEnquiryLines() {
        //    try {
        //        if (Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE) {
        //            IDH_ENQUIRY oEnq = new IDH_ENQUIRY();
        //            oEnq.MustLoadChildren = false;
        //            if (oEnq.getByKey(moaSelectItems[0].ToString().Split(':')[0])) {
        //                doSetHeaderByEnquiry(oEnq);

        //            }
        //        }
        //        IDH_ENQITEM oEnqItems = new IDH_ENQITEM();
        //        oEnqItems.DoNotSetDefault = true;
        //        oEnqItems.DoNotSetParent = true;
        //        if (Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE) {
        //            Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
        //        }

        //        moGrid.AddEditLine = false;
        //        int iWOQRow = 0;
        //        string sPrvEnqRowCode = "", sPrvWOQRowCode = "";
        //        string sEnqLineID = "";
        //        int iRet = 0;
        //        moWOQ.WOQItems.doAddEmptyRow(true);
        //        //moWOQ.WOQItems.gotoRow(moWOQ.WOQItems.Count - 1);
        //        for (int iSelRecodrs = 0; iSelRecodrs <= moaSelectItems.Count - 1; iSelRecodrs++) {
        //            sEnqLineID = moaSelectItems[iSelRecodrs].ToString().Split(':')[1];
        //            iRet = oEnqItems.getData("(" + IDH_ENQITEM._Code + "='" + sEnqLineID + "' OR " + IDH_ENQITEM._PCode + "='" + sEnqLineID + "') And " + IDH_ENQITEM._Status + " ='1'", IDH_ENQITEM._Sort);
        //            if (iRet == 0)
        //                continue;
        //            while (oEnqItems.next()) {
        //                if (oEnqItems.U_EnqId == string.Empty)
        //                    continue;
        //                iWOQRow = moWOQ.WOQItems.Count - 1;
        //                moWOQ.WOQItems.DoBlockUpdateTrigger = true;
        //                moWOQ.WOQItems.U_AdtnlItm = oEnqItems.U_AddItm;
        //                moWOQ.WOQItems.U_EnqID = oEnqItems.U_EnqId;
        //                moWOQ.WOQItems.U_EnqLID = oEnqItems.Code;
        //                moWOQ.WOQItems.U_ExpLdWgt = oEnqItems.U_EstQty;
        //                moWOQ.WOQItems.U_WasCd = oEnqItems.U_ItemCode;
        //                moWOQ.WOQItems.U_WasDsc = oEnqItems.U_ItemDesc;
        //                moWOQ.WOQItems.U_WasFDsc = oEnqItems.U_ItemName;
        //                if (string.IsNullOrEmpty(moWOQ.WOQItems.U_WasDsc) && !string.IsNullOrEmpty(moWOQ.WOQItems.U_WasCd))
        //                    moWOQ.WOQItems.U_WasDsc = Config.INSTANCE.doGetItemDescriptionWithNoBuffer(moWOQ.WOQItems.U_WasCd);

        //                if (oEnqItems.U_AddItm == "A" && oEnqItems.U_PCode != "" && iWOQRow > 0) {
        //                    if (sPrvEnqRowCode == oEnqItems.U_PCode)
        //                        moWOQ.WOQItems.U_PLineID = sPrvWOQRowCode;

        //                } else {
        //                    moWOQ.WOQItems.U_PLineID = "";
        //                    sPrvEnqRowCode = oEnqItems.Code;
        //                    sPrvWOQRowCode = moWOQ.WOQItems.Code;
        //                }
        //                moWOQ.WOQItems.U_Status = "1";
        //                moWOQ.WOQItems.U_UOM = oEnqItems.U_UOM;
        //                moWOQ.WOQItems.U_WstGpCd = oEnqItems.U_WstGpCd;
        //                moWOQ.WOQItems.U_WstGpNm = oEnqItems.U_WstGpNm;
        //                moWOQ.WOQItems.DoBlockUpdateTrigger = false;
        //                moWOQ.WOQItems.doGetAllPrices();
        //                //moWOQ.WOQItems.doAddEmptyRow(true, true, false);
        //                iWOQRow++;
        //            }
        //            //moWOQ.WOQItems.doCalculateAllRowsTotal(false);
        //        }
        //        doFillGridCombos();
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doLoadBySelectedEnquiryLines" });
        //        return;
        //    } finally {
        //        // moGrid.AddEditLine = true;
        //    }
        //}

        //protected void doSetFilterFields() {
        //    moGrid.doAddFilterField("IDH_DOCNUM", IDH_WOQITEM._EnqID, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30);
        //}

        protected virtual void doTheGridLayout() {
            if (Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                if (moFormSettings == null) {
                    string sFormTypeId = SBOForm.TypeEx;
                    //Dim sGridID As String = sFormTypeId & "." & moDispRoutGrid.GridId
                    moFormSettings = new idh.dbObjects.User.IDH_FORMSET();
                    moFormSettings.getFormGridFormSettings(sFormTypeId, moGrid.GridId, "");
                    moFormSettings.doAddFieldToGrid(moGrid);

                    doSetListFields();
                    moFormSettings.doSyncDB(moGrid);
                } else {
                    if (moFormSettings.SkipFormSettings) {
                        doSetListFields();
                    } else {
                        moFormSettings.doAddFieldToGrid(moGrid);
                    }
                }
            } else {
                doSetListFields();
            }
            moGrid.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto;
        }

        protected virtual void doSetListFields() {

            string sWastGrp = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup();

            moGrid.doAddListField(IDH_CONTRTROW._Code, "Code", false, 20, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._Name, "Name", false, 0, null, null);

            moGrid.doAddListField(IDH_CONTRTROW._ContractNo, "Contract No", false, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._Qty, "Qty", true, -1, null, null);
            string forItem = "SRC*IDHISRC(ITM)[IDH_ITMCOD][ITEMCODE;U_ItemNm=ITEMNAME]";
            moGrid.doAddListField(IDH_CONTRTROW._ItemCd, "Container", true, -1, forItem, null);
            moGrid.doAddListField(IDH_CONTRTROW._ItemNm, "Container Name", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._Delivery, "Delivery", true, -1, "CHECKBOX", null);
            moGrid.doAddListField(IDH_CONTRTROW._DlvDate, "Delivery Date", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._Tip, "Disposal Site", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._TipName, "Disposal Name", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._TipAdr, "Disposal Address", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._TipAdrId, "Disposal Site Address Cd", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._WastCd, "Wast Code", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._WastNm, "Wast Name", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._JobType, "Order Type", true, -1, "COMBOBOX", null);
            moGrid.doAddListField(IDH_CONTRTROW._SDate, "Start Date", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._Freq, "Frequency", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._Mon, "Monday", true, -1, "CHECKBOX", null);
            moGrid.doAddListField(IDH_CONTRTROW._Tue, "Tuesday", true, -1, "CHECKBOX", null);
            moGrid.doAddListField(IDH_CONTRTROW._Wed, "Wednesday", true, -1, "CHECKBOX", null);
            moGrid.doAddListField(IDH_CONTRTROW._Thu, "Thursday", true, -1, "CHECKBOX", null);
            moGrid.doAddListField(IDH_CONTRTROW._Fri, "Friday", true, -1, "CHECKBOX", null);
            moGrid.doAddListField(IDH_CONTRTROW._Sat, "Saturday", true, -1, "CHECKBOX", null);
            moGrid.doAddListField(IDH_CONTRTROW._Sun, "Sunday", true, -1, "CHECKBOX", null);
            moGrid.doAddListField(IDH_CONTRTROW._HUOM, "Haulage UOM", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._HlgPrcTyp, "Haulage Price Type", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._HlgCIPID, "Haulage CIPID", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._HlgCon, "Haulage Contract Checkbox", true, -1, "CHECKBOX", null);
            moGrid.doAddListField(IDH_CONTRTROW._HlgPrice, "Haulage Price", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._TipUOM, "Disposal UOM", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._TipPrcTyp, "Disposal Price Type", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._TipCIPID, "Disposal CIPID", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._TipCon, "Disposal Contract Checkbox", true, -1, "CHECKBOX", null);
            moGrid.doAddListField(IDH_CONTRTROW._TipPrice, "Disposal Price", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._TranCd, "Transaction Code", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._PBI, "PBI Code", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._PBINxRDat, "PBI Next Run Date", true, -1, null, null);
            moGrid.doAddListField(IDH_CONTRTROW._WOR, "WOR Code", true, -1, null, null);




            //add rest of columns here 

            //string sCarrierUIP = "SRC*IDHCSRCH(WC)[IDH_BPCOD;IDH_TYPE=F-C][CARDCODE;" + IDH_CONTRTROW._CarrNm + "=CARDNAME]";
            //if (Config.ParameterAsBool("WOBPFLT", true) == false) {
            //    sCarrierUIP = "SRC*IDHCSRCH(WC)[IDH_BPCOD;][CARDCODE;" + IDH_CONTRTROW._CarrNm + "=CARDNAME]";
            //} else {
            //    sCarrierUIP = "SRC*IDHCSRCH(WC)[IDH_BPCOD;IDH_TYPE=S;IDH_GROUP=" + Config.Parameter("BGSWCarr") + "][CARDCODE;" + IDH_CONTRTROW._CarrNm + "=CARDNAME]";
            //}
            //moGrid.doAddListField(IDH_CONTRTROW._CarrCd, "Carrier Code", true, 80, sCarrierUIP, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);

            //sCarrierUIP = "SRC*IDHCSRCH(WC)[IDH_NAME;IDH_TYPE=F-C][CARDNAME;" + IDH_CONTRTROW._CarrCd + "=CARDCODE]";
            //if (Config.ParameterAsBool("WOBPFLT", true) == false) {
            //    sCarrierUIP = "SRC*IDHCSRCH(WC)[IDH_NAME;][CARDNAME;" + IDH_CONTRTROW._CarrCd + "=CARDCODE]";
            //} else {
            //    sCarrierUIP = "SRC*IDHCSRCH(WC)[IDH_NAME;IDH_TYPE=S;IDH_GROUP=" + Config.Parameter("BGSWCarr") + "][CARDNAME;" + IDH_CONTRTROW._CarrCd + "=CARDCODE]";
            //}
            //moGrid.doAddListField(IDH_CONTRTROW._CarrNm, "Carrier Name", true, 100, sCarrierUIP, null);

            //string sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_BPCOD;IDH_TYPE=F-C][CARDCODE;" + IDH_CONTRTROW._TipNm + "=CARDNAME]";
            //if (Config.ParameterAsBool("WOBPFLT", true) == false) {
            //    sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_BPCOD;][CARDCODE;" + IDH_CONTRTROW._TipNm + "=CARDNAME]";
            //} else {
            //    sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_BPCOD;IDH_TYPE=S;IDH_GROUP=" + Config.Parameter("BGSDispo") + "][CARDCODE;" + IDH_CONTRTROW._TipNm + "=CARDNAME]";
            //}
            //moGrid.doAddListField(IDH_CONTRTROW._Tip, "Disposal Code", true, 80, sDispUIP, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            //sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_NAME;IDH_TYPE=F-C][CARDNAME;" + IDH_CONTRTROW._Tip + "=CARDCODE]";
            //if (Config.ParameterAsBool("WOBPFLT", true) == false) {
            //    sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_NAME;][CARDNAME;" + IDH_CONTRTROW._Tip + "=CARDCODE]";
            //} else {
            //    sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_NAME;IDH_TYPE=S;IDH_GROUP=" + Config.Parameter("BGSDispo") + "][CARDNAME;" + IDH_CONTRTROW._Tip + "=CARDCODE]";
            //}
            //moGrid.doAddListField(IDH_CONTRTROW._TipNm, "Disposal Name", true, 100, sDispUIP, null);

            //moGrid.doAddListField(IDH_CONTRTROW._FnlTipCd, "Final Disposal Site", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            //moGrid.doAddListField(IDH_CONTRTROW._FnlTipNm, "Final Disposal Name", false, -1, null, null);

            //moGrid.doAddListField(IDH_CONTRTROW._WstGpCd, "Waste Group", true, 120, "SRC*IDH_WGSRCH(WGID)[IDH_WGCOD][WGCODE;" + IDH_CONTRTROW._WstGpNm + "=WGNAME]", null);
            //moGrid.doAddListField(IDH_CONTRTROW._WstGpNm, "Waste Group Name", true, 0, "SRC*IDH_WGSRCH(WGNM)[IDH_NAME][WGNAME;" + IDH_CONTRTROW._WstGpCd + "=WGCODE]", null);

            //moGrid.doAddListField(IDH_CONTRTROW._EnqID, "Enquiry", false, 50, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._EnqLID, "Enquiry Line", false, 50, null, null);

            //moGrid.doAddListField(IDH_CONTRTROW._JobTp, "Order Type", true, 100, "COMBOBOX", null);


            //string sContainerUID = "SRC*IDHISRC(CONTCD)[IDH_ITMCOD;IDH_ITMNAM=;IDH_GRPCOD=" + moWOQ.U_ItemGrp + "][ITEMCODE;" + IDH_CONTRTROW._ItemDsc + "=ITEMNAME]";
            //sContainerUID = "SRC*IDHISRC(CONTCD)[IDH_ITMCOD;IDH_ITMNAM=;IDH_GRPCOD=#" + IDH_CONTRTROW._ItmGrp + "][ITEMCODE;" + IDH_CONTRTROW._ItemDsc + "=ITEMNAME]";


            //moGrid.doAddListField(IDH_CONTRTROW._ItemCd, "Container Code", true, 100, sContainerUID, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);


            //sContainerUID = "SRC*IDHISRC(CONTCD)[IDH_ITMNAM;IDH_ITMCOD=;IDH_GRPCOD=#" + IDH_CONTRTROW._ItmGrp + "][ITEMNAME;" + IDH_CONTRTROW._ItemCd + "=ITEMCODE]";
            //moGrid.doAddListField(IDH_CONTRTROW._ItemDsc, "Container Name", true, 100, sContainerUID, null);

            ////-------------------
            //if (Config.INSTANCE.getParameterAsBool("USAREL", false) || Config.INSTANCE.getParameterAsBool("ENBWPITM", false)) {
            //    sContainerUID = "SRC*IDHWPSRCENQ(FROMWOQ)[IDH_ITMCOD;IDH_GRPCOD=" + sWastGrp + ";IDH_ADDITMFLG=#" + IDH_CONTRTROW._PLineID + ";IDH_ADDITMGRP=#" + IDH_CONTRTROW._WstGpCd + "][ITEMCODE;" + IDH_CONTRTROW._WasFDsc + "=FNAME;" + IDH_CONTRTROW._WasDsc + "=ITEMNAME;]";
            //} else {
            //    sContainerUID = "SRC*IDHWISRCENQ(FROMWOQ)[IDH_ITMCOD;IDH_GRPCOD=" + sWastGrp + ";IDH_ADDITMFLG=#" + IDH_CONTRTROW._PLineID + ";IDH_ADDITMGRP=#" + IDH_CONTRTROW._WstGpCd + "][ITEMCODE;" + IDH_CONTRTROW._WasFDsc + "=FNAME;" + IDH_CONTRTROW._WasDsc + "=ITEMNAME;]";
            //}
            ////
            //moGrid.doAddListField(IDH_CONTRTROW._WasCd, "Item Code", false, 120, sContainerUID, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            ////-------------------

            //if (Config.INSTANCE.getParameterAsBool("USAREL", false) || Config.INSTANCE.getParameterAsBool("ENBWPITM", false)) {
            //    //sContainerUID = "SRC*IDHWPSRCENQ(FROMWOQ)[IDH_ITMFNM;IDH_ITMCOD=#" + IDH_CONTRTROW._WasCd + ";IDH_GRPCOD=" + sWastGrp + ";IDH_ADDITMFLG=#" + IDH_CONTRTROW._PLineID + ";IDH_ADDITMGRP=#" + IDH_CONTRTROW._WstGpCd + "][FNAME;" + IDH_CONTRTROW._WasCd + "=ITEMCODE;" + IDH_CONTRTROW._WasDsc + "=ITEMNAME;]";
            //    sContainerUID = "SRC*IDHWPSRCENQ(FROMWOQ)[IDH_ITMFNM;IDH_GRPCOD=" + sWastGrp + ";IDH_ADDITMFLG=#" + IDH_CONTRTROW._PLineID + ";IDH_ADDITMGRP=#" + IDH_CONTRTROW._WstGpCd + "][FNAME;" + IDH_CONTRTROW._WasCd + "=ITEMCODE;" + IDH_CONTRTROW._WasDsc + "=ITEMNAME;]";
            //} else {
            //    //sContainerUID = "SRC*IDHWISRCENQ(FROMWOQ)[IDH_ITMFNM;IDH_ITMCOD=#" + IDH_CONTRTROW._WasCd + ";IDH_GRPCOD=" + sWastGrp + ";IDH_ADDITMFLG=#" + IDH_CONTRTROW._PLineID + ";IDH_ADDITMGRP=#" + IDH_CONTRTROW._WstGpCd + "][FNAME;" + IDH_CONTRTROW._WasCd + "=ITEMCODE;" + IDH_CONTRTROW._WasDsc + "=ITEMNAME;]";
            //    sContainerUID = "SRC*IDHWISRCENQ(FROMWOQ)[IDH_ITMFNM;IDH_GRPCOD=" + sWastGrp + ";IDH_ADDITMFLG=#" + IDH_CONTRTROW._PLineID + ";IDH_ADDITMGRP=#" + IDH_CONTRTROW._WstGpCd + "][FNAME;" + IDH_CONTRTROW._WasCd + "=ITEMCODE;" + IDH_CONTRTROW._WasDsc + "=ITEMNAME;]";
            //}
            //moGrid.doAddListField(IDH_CONTRTROW._WasFDsc, "Description", true, 140, sContainerUID, null);
            //if (moGrid.getGridControl().getListFields().Count > 0 && moGrid.doIndexFieldWC(IDH_CONTRTROW._WasFDsc) > 0) {
            //    int index = moGrid.doIndexFieldWC(IDH_CONTRTROW._WasFDsc);
            //    if (index > 0) {
            //        moGrid.getGridControl().getListField(index).msFieldType = sContainerUID;
            //    }
            //}
            ////moGrid.doAddListField(IDH_CONTRTROW._WasFDsc, "Description", true, 140, sContainerUID, null);

            //if (Config.INSTANCE.getParameterAsBool("USAREL", false) || Config.INSTANCE.getParameterAsBool("ENBWPITM", false)) {
            //    sContainerUID = "SRC*IDHWPSRCENQ(FROMENQUIRY)[IDH_ITMNAM;IDH_GRPCOD=" + sWastGrp + ";IDH_ADDITMFLG=#" + IDH_CONTRTROW._PLineID + ";IDH_ADDITMGRP=#" + IDH_CONTRTROW._WstGpCd + "][ITEMNAME;" + IDH_CONTRTROW._WasCd + "=ITEMCODE;" + IDH_CONTRTROW._WasFDsc + "=FNAME]";
            //} else {
            //    sContainerUID = "SRC*IDHWISRCENQ(FROMENQUIRY)[IDH_ITMNAM;IDH_GRPCOD=" + sWastGrp + ";IDH_ADDITMFLG=#" + IDH_CONTRTROW._PLineID + ";IDH_ADDITMGRP=#" + IDH_CONTRTROW._WstGpCd + "][ITEMNAME;" + IDH_CONTRTROW._WasCd + "=ITEMCODE;" + IDH_CONTRTROW._WasFDsc + "=FNAME]";
            //}
            //moGrid.doAddListField(IDH_CONTRTROW._WasDsc, "Item Name", true, 120, sContainerUID, null);
            //if (moGrid.getGridControl().getListFields().Count > 0 && moGrid.doIndexFieldWC(IDH_CONTRTROW._WasDsc) > 0) {
            //    int index = moGrid.doIndexFieldWC(IDH_CONTRTROW._WasDsc);
            //    if (index > 0) {
            //        moGrid.getGridControl().getListField(index).msFieldType = sContainerUID;
            //    }
            //}



            //moGrid.doAddListField(IDH_CONTRTROW._ExpLdWgt, "Est Qty", true, 40, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._UOM, "UOM", true, 40, "COMBOBOX", null);
            //moGrid.doAddListField(IDH_CONTRTROW._TipCost, "Disposal Cost", true, 70, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._MnMargin, "MU%", false, 40, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._LstPrice, "List Price", true, 60, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._TCharge, "Charge Price", true, 70, null, null);

            //moGrid.doAddListField(IDH_CONTRTROW._HlSQty, "Carrier Qty", true, 40, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._CusChr, "Carrier Charge", true, 70, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._Price, "Carrier Chg Total", false, 90, null, null);


            //moGrid.doAddListField(IDH_CONTRTROW._OrdCost, "Carrier Cost", true, 70, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._OrdTot, "Carrier Cost Total", false, 90, null, null);


            //moGrid.doAddListField(IDH_CONTRTROW._TipTot, "Disposal Total", false, 70, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._VtCostAmt, "Cost Tax", false, -1, null, null);

            //moGrid.doAddListField(IDH_CONTRTROW._TChrgVtGrp, "Tax Charge Group", false, 0, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._TaxAmt, "Tax Amount", false, 60, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._Total, "Total Charge", false, 60, null, null);

            //moGrid.doAddListField(IDH_CONTRTROW._JCost, "Total Cost", false, 60, null, null);


            //moGrid.doAddListField(IDH_CONTRTROW._GMargin, "GM %", false, 45, null, null);

            //moGrid.doAddListField(IDH_CONTRTROW._Sample, "Sample", true, 40, "CHECKBOX", null);
            //moGrid.doAddListField(IDH_CONTRTROW._MSDS, "MSDS", true, 40, "CHECKBOX", null);
            ////"SRC*IDH_WGSRCH(WGID)[IDH_WGCOD][WGCODE;" + IDH_CONTRTROW._WstGpNm + "=WGNAME]"

            ////DisposalRoute
            ////string sDispRouteUID = IDH_CONTRTROW._WasFDsc + "=FrgnName;" 
            ////    + IDH_CONTRTROW._Tip + "=" + IDH_DISPRTCD._TipCd + ";" + IDH_CONTRTROW._TipNm + "=" + IDH_DISPRTCD._TipNm + ";"
            ////    + IDH_CONTRTROW._ItemCd + "=" + IDH_DISPRTCD._ItemCd + ";"
            ////    + IDH_CONTRTROW._ItemDsc + "=" + IDH_DISPRTCD._ItemDsc + ";"
            ////    + IDH_CONTRTROW._WasCd + "=" + IDH_DISPRTCD._WasCd + ";"
            ////    + IDH_CONTRTROW._WasDsc + "=" + IDH_DISPRTCD._WasDsc + ";"
            ////    //+ IDH_CONTRTROW._Quantity + "=" + IDH_DISPRTCD._Qty + ";"
            ////    //+ IDH_CONTRTROW._OrdCost + "=" + IDH_DISPRTCD._HaulgCost + ";"
            ////    + IDH_CONTRTROW._WhsCode + "=" + IDH_DISPRTCD._WhsCode + ";"
            ////    //+ IDH_CONTRTROW._RtCdCode + "=" + IDH_DISPRTCD._Code + ";"                

            ////    + IDH_CONTRTROW._UOM + "=" + IDH_DISPRTCD._UOM;
            //string sDispRouteUID = IDH_CONTRTROW._RtCdCode + "=" + IDH_DISPRTCD._Code + ";";
            //string sParams = "";
            //moGrid.doAddListField(IDH_CONTRTROW._RouteCd, "Disposal Route", true, 60, "SRC*IDH_DRTSR(SRDPRTCD)[IDH_DISPRT;" + sParams + "][" + IDH_DISPRTCD._DisRCode + ";" + sDispRouteUID + "]", null);
            //moGrid.doAddListField(IDH_CONTRTROW._RtCdCode, "Disposal Route Code", false, 0, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._SampleRef, "Sample Ref.", false, 60, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._SampStatus, "Sample Status", false, 65, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._WhsCode, "Warehouse", false, 40, "COMBOBOX", null);


            //moGrid.doAddListField(IDH_CONTRTROW._Status, "Status", true, 50, "COMBOBOX", null);
            //moGrid.doAddListField(IDH_CONTRTROW._Sort, "Sort", false, 0, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._JobNr, "WOQ ID", false, 0, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._MANPRC, "Manual Price Change", false, 0, null, null);

            //moGrid.doAddListField(IDH_CONTRTROW._RDate, "Requested Date", true, -1, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._Obligated, "Weigh - needed", true, -1, "COMBOBOX", null);

            //moGrid.doAddListField(IDH_CONTRTROW._WOHID, "WOH", false, -1, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._WORID, "WOR", false, -1, null, null);

            //// moGrid.doAddListField(IDH_CONTRTROW._FnlTipCd, "Final Disposal Site", false, 0, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            ////  moGrid.doAddListField(IDH_CONTRTROW._FnlTipNm, "Final Disposal Name", false, 0, null, null);
            //moGrid.doAddListField(IDH_CONTRTROW._Comment, "Comments", true, -1, null, null);


            moGrid.doAutoListFields(false);

            //moGrid.DBObject.getFieldInfo(0).FieldName.in
            moGrid.doSynchDBandGridFieldPos();

            moGrid.doApplyRules();
            // moGrid.doSizeColumns();
            //moGrid.setOrderValue("Cast(Code as int) Asc");
        }

        //private void doHandleCountryCombo(ref SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal) {
        //    if (!pVal.BeforeAction) {
        //        SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_CUNTRY").Specific;
        //        if (oCombo.Selected == null)
        //            return;
        //        FillCombos.FillState(oForm.Items.Item("IDH_STATE").Specific, oCombo.Selected.Value, null);
        //    }
        //}

        private void doChooseBPCode(string sItemID ) {
            setSharedData("IDH_BPCOD", "");
            setSharedData("IDH_NAME", getItemValue(sItemID));
            setSharedData("TRG", "BP");
            setSharedData("SILENT", "SHOWMULTI");
            doOpenModalForm("IDHCSRCH");
        }
        private void doChooseBPAddress() {
            //string sCardCode = moContract.U_CardCd.ToString().Trim();
            //string sAddress = moContract.U_Address.Trim();
            //setSharedData("TRG", "IDH_ADR1");
            //setSharedData("IDH_CUSCOD", sCardCode);
            //setSharedData("IDH_ADRTYP", "S");
            //setSharedData("CALLEDITEM", "IDH_ADR1");
            //setSharedData("IDH_ADDRES", sAddress);
            //if (Config.ParameterAsBool("UNEWSRCH", false) == false) {
            //    doOpenModalForm("IDHASRCH");
            //} else {
            //    doOpenModalForm("IDHASRCH3");
            //}
        }

        //private void doSetWidths() {
        //    return;
        //    //if (moGrid == null || moGrid.Columns.Count == 0)

        //    //    return;
        //    //ListFields oFields = moGrid.getGridControl().getListFields();
        //    //foreach (com.idh.controls.strct.ListField oTField in oFields) {
        //    //    if (oTField.miWidth > 0)
        //    //        moGrid.Columns.Item(oTField.msFieldId).Width = oTField.miWidth;

        //    //}
        //}
        //private void doUpdateGridCells() {
        //    if (Mode == BoFormMode.fm_ADD_MODE || Mode == BoFormMode.fm_FIND_MODE)
        //        return;
        //    try {
        //        for (int i = 0; i <= moGrid.getRowCount() - 1; i++) {
        //            if (moGrid.doGetFieldValue(IDH_WOQITEM._Status, i).ToString() != "" && moGrid.doGetFieldValue(IDH_WOQITEM._Status, i).ToString() != "1") {
        //                moGrid.Settings.SetRowEditable(i + 1, false);
        //            }
        //        }
        //    } catch (Exception ex) {
        //        //com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doUpdateGridCells") });
        //    }

        //}
        //private bool doValidateBPbyCode(ref string sCardName) {
        //    if (moWOQ.U_CardCd.Trim() == string.Empty) {
        //        sCardName = "";
        //        return false;
        //    }
        //    sCardName = Config.INSTANCE.doGetBPName(moWOQ.U_CardCd, false);
        //    if (sCardName == string.Empty)
        //        return false;
        //    else
        //        return true;
        //}

        //private void doSwitchToFind() {
        //    Items.Item("IDH_DOCNUM").Enabled = true;
        //    setFocus("IDH_DOCNUM");
        //    EnableItem(false, "IDH_CRETBP");
        //    EnableItem(false, "IDH_CRTWO");
        //    EnableItem(false, "IDH_QUSTN");
        //    string[] oExl = { "IDH_DOCNUM" };
        //    DisableAllEditItems(oExl);
        //    SBOForm.DefButton = "1";

        //}

        private void doSwitchToEdit() {
            //setFocus("IDH_ADR1");
            //string[] oExl = { "IDH_DOCNUM", "IDH_ACTVTY", "IDH_STATUS", "IDH_USER", "IDH_CardCD",
            //                    "IDH_LBLTOT", "IDH_LPRICE", "IDH_COST", "IDH_LPPnL", "IDH_LPMRGN", "IDH_CHARGE", "IDH_COST2", "IDH_CHGPnL", "IDH_CHMRGN",
            //                "IDH_VERSN","IDH_CHARGE","IDH_CHGVAT","IDH_TCHARG"};
            //// concatinate
            //EnableAllEditItems(oExl);

            //EnableItem(false, "IDH_DOCNUM");
            //setFocus("IDH_ADR1");
            //EnableItem(true, "IDH_CRETBP");
            //EnableItem(true, "IDH_QUSTN");
        }
        
        #region Fill Combos
        private void doFillCombos() {
            try {
                //SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)SBOForm.Items.Item("IDH_ASGNTO").Specific;
                //doFillCombo(oCombo, "OUSR", "USERID", "U_NAME", "Locked='N' ", "U_NAME", false);

                //FillCombos.FillCountry(Items.Item("IDH_CUNTRY").Specific);


                //FillCombos.FillState(Items.Item("IDH_STATE").Specific, com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB"), null);
                //FillCombos.FillWR1StatusNew(Items.Item("IDH_STATUS").Specific, 106);
                //FillCombos.FillWR1StatusNew(Items.Item("IDH_SOURCE").Specific, 108);
                //doFillCombo("IDH_BRANCH", "OUBR", "Code", "Remarks", null, null, true);
                

                //oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_STATUS").Specific;
                //oCombo.SelectExclusive("1", SAPbouiCOM.BoSearchKey.psk_ByValue);


                //oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_CUNTRY").Specific;
                //oCombo.SelectExclusive(Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB"), SAPbouiCOM.BoSearchKey.psk_ByValue);

                //oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_ITMGRP").Specific;
                //doFillCombo(oCombo, "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpCod=jt.U_ItemGrp AND jt.U_OrdCat='" + msOrdCat + "'");
                
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }

        }
        private void doFillGridCombos() {
            try {
                //SAPbouiCOM.ComboBoxColumn oCombo;

                //int iIndex = moGrid.doIndexFieldWC(IDH_WOQITEM._UOM);
                //oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
                //oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                //FillCombos.UOMCombo(oCombo);

                //iIndex = moGrid.doIndexFieldWC(IDH_WOQITEM._Status);
                //oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
                //oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                //FillCombos.FillWR1StatusNew(oCombo, 107);

                //iIndex = moGrid.doIndexFieldWC(IDH_WOQITEM._WhsCode);
                //oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
                //oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                //doFillCombo(oCombo, "OWHS", "WhsCode", "WhsName", null, null, null, null);

                doFillJobTypeCombo();

                //iIndex = moGrid.doIndexFieldWC(IDH_WOQITEM._Obligated);
                //oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
                //oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                //FillCombos.AvailableCombo2(oCombo, "", "");
                ////oCombo.ValidValues.Remove(0, BoSearchKey.psk_Index);
                ////doFillCombo(oCombo, "OWHS", "WhsCode", "WhsName", null, null, null, null);


            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }

        private void doFillJobTypeCombo() {
            try {
                SAPbouiCOM.ComboBoxColumn oCombo;
                int iIndex = moGrid.doIndexFieldWC(IDH_CONTRTROW._JobType);
                oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                doClearValidValues(oCombo.ValidValues);

                string sItemGrp = "";// moWOQ.U_ItemGrp;
                FillCombos.doFillJobTypeCombo(ref oCombo, sItemGrp);

                // //string sJobNumber = moWOQ.Code;
                //// double dQty = 0;

                // ////JobEntries oJobs = getWFValue(oForm, "JOBENTRIES");
                // ArrayList oJobTypes = default(ArrayList);
                // IDH_JOBTYPE_SEQ oJobSeq = IDH_JOBTYPE_SEQ.getInstance();
                // //if (oJobs == null) {
                // //sLastJobType = getFormDFValue("U_JobTp");
                // oJobTypes = IDH_JOBTYPE_SEQ.getAvailJobs(Config.CAT_WO, sItemGrp, "");
                // //} 
                // //else {
                // //    sItemGrp = getFormDFValue(oForm, "U_ItmGrp");
                // //    sLastJobType = oJobs.LastJobType;
                // //    sJobNumber = getFormDFValue(oForm, "U_JobNr");
                // //    dQty = oJobs.getOnSiteQty(sJobNumber);

                // //    if (getWFValue(oForm, "ISNEW") == true) {
                // //        oJobTypes = IDH_JOBTYPE_SEQ.getNextAvailJobs(Config.CAT_WO, sItemGrp, sLastJobType, dQty);
                // //    } else {
                // //        oJobTypes = IDH_JOBTYPE_SEQ.getAvailJobs(Config.CAT_WO, sItemGrp, sLastJobType);
                // //    }
                // //}
                // oCombo.ValidValues.Add("", "");
                // if (oJobTypes.Count > 0) {
                //     iIndex = 0;
                //     for (iIndex = 0; iIndex <= oJobTypes.Count - 1; iIndex++) {
                //         try {
                //             oJobSeq.gotoRow((int)oJobTypes[iIndex]);
                //             oCombo.ValidValues.Add(oJobSeq.U_JobTp, oJobSeq.U_JobTp);
                //         } catch {
                //         }
                //     }
                // }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }

        #endregion

        //#endregion 
        //#region "Form Setup Methods"




        //public override bool doItemEvent(ref ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction && pVal.EventType == BoEventTypes.et_KEY_DOWN) {
        //        if (pVal.ItemUID == "IDHTEMP" && pVal.ItemChanged) {
        //            BubbleEvent = false;
        //            return false;
        //        }
        //    } else if (pVal.BeforeAction && IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
        //        IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
        //    //else if (pVal.BeforeAction && IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("1287"))
        //    //    IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("1287");
        //    return base.doItemEvent(ref pVal, ref BubbleEvent);
        //}
        //public override bool doRightClickEvent(ref ContextMenuInfo pVal, ref bool BubbleEvent) {
        //    if (pVal.ItemUID != "LINESGRID" && pVal.BeforeAction && IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm")) {
        //        IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
        //    } else if (pVal.BeforeAction && pVal.EventType == BoEventTypes.et_RIGHT_CLICK && pVal.ItemUID != "LINESGRID") {
        //        if (Mode == BoFormMode.fm_ADD_MODE || Mode == BoFormMode.fm_UPDATE_MODE || Mode == BoFormMode.fm_FIND_MODE) {
        //            SBOForm.EnableMenu(IDHAddOns.idh.lookups.Base.DUPLICATE, false);
        //        } else {
        //            SBOForm.EnableMenu(IDHAddOns.idh.lookups.Base.DUPLICATE, true);
        //        }
        //    }
        //    return base.doRightClickEvent(ref pVal, ref BubbleEvent);
        //}
        //public override bool doFormDataEvent(ref BusinessObjectInfo BusinessObjectInfo, ref bool BubbleEvent) {
        //    if (BusinessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_LOAD && BusinessObjectInfo.ActionSuccess) {
        //        if ((!base.IDHForm.goParent.doCheckModal(UniqueID)) && Mode != BoFormMode.fm_ADD_MODE && Mode != BoFormMode.fm_UPDATE_MODE && Mode != BoFormMode.fm_FIND_MODE) {
        //            SBOForm.EnableMenu(IDHAddOns.idh.lookups.Base.DUPLICATE, true);
        //        } else
        //            SBOForm.EnableMenu(IDHAddOns.idh.lookups.Base.DUPLICATE, false);
        //    }
        //    return base.doFormDataEvent(ref BusinessObjectInfo, ref BubbleEvent);
        //}
        //#endregion

        //#region Create WO
        //private bool doValidateNCreateAddressNContact() {
        //    try {
        //        bool bAddressExists = false;
        //        if (moWOQ.U_Address == "") {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Please provide customer address.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Container Address") });
        //            return false;
        //        }
        //        if (moWOQ.U_Contact == "" || (moWOQ.U_Phone1 == "" && moWOQ.U_E_Mail == "")) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Please provide customer contact.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Container Contact") });
        //            return false;
        //        }
        //        string sAddressLN = Config.INSTANCE.getValueFromBPShipToAddress(moWOQ.U_CardCd, moWOQ.U_Address, "U_AddrsCd").ToString();
        //        if ((sAddressLN == null || sAddressLN == string.Empty)) {
        //            //create address
        //            bAddressExists = false;
        //        } else {
        //            moWOQ.U_AddrssLN = sAddressLN;
        //            bAddressExists = true;
        //        }
        //        bool bContactExists = false;
        //        string sContact = (string)Config.INSTANCE.getValueFromTablebyKey("OCPR", "Name", "Name", moWOQ.U_Contact);
        //        if ((sContact == null || sContact == string.Empty)) {
        //            //create contact
        //            bContactExists = false;
        //        } else {
        //            bContactExists = true;
        //        }
        //        try {
        //            SAPbobsCOM.BusinessPartners oBP = (SAPbobsCOM.BusinessPartners)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
        //            if (oBP.GetByKey(moWOQ.U_CardCd) == false)
        //                return false;
        //            if (bAddressExists) {
        //                oBP.Addresses.SetCurrentLine(0);
        //                int iLine = 0;
        //                while (true) {
        //                    if (iLine >= oBP.Addresses.Count)
        //                        break;
        //                    oBP.Addresses.SetCurrentLine(iLine);
        //                    iLine++;
        //                    if (oBP.Addresses.AddressType == SAPbobsCOM.BoAddressType.bo_ShipTo && oBP.Addresses.AddressName == moWOQ.U_Address)
        //                        break;
        //                }
        //            }

        //            oBP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
        //            oBP.Addresses.AddressName = moWOQ.U_Address;
        //            oBP.Addresses.Street = moWOQ.U_SStreet;//Street 100
        //            oBP.Addresses.Block = moWOQ.U_Block;//Block 100
        //            oBP.Addresses.City = moWOQ.U_City;//City 100
        //            oBP.Addresses.ZipCode = moWOQ.U_ZpCd;//State 3
        //            oBP.Addresses.County = moWOQ.U_County;//PostCode 20
        //            oBP.Addresses.State = moWOQ.U_State;//County 100
        //            oBP.Addresses.Country = moWOQ.U_Country;//Country 3
        //            if (oBP.Addresses.UserFields.Fields.Item("U_AddrsCd").Value == null || oBP.Addresses.UserFields.Fields.Item("U_AddrsCd").Value.ToString() == string.Empty) {
        //                oBP.Addresses.UserFields.Fields.Item("U_AddrsCd").Value = Config.INSTANCE.GetAddressNextCode();
        //            }
        //            //oAddress.UserFields.Fields.Item("U_AddrsCd").Value = bridge.lookups.Config.INSTANCE.GetAddressNextCode();
        //            if (bContactExists) {
        //                oBP.ContactEmployees.SetCurrentLine(0);
        //                int iLine = 0;
        //                while (true) {
        //                    if (iLine >= oBP.ContactEmployees.Count)
        //                        break;
        //                    oBP.ContactEmployees.SetCurrentLine(iLine);
        //                    iLine++;
        //                    if (oBP.ContactEmployees.Name == moWOQ.U_Contact)
        //                        break;
        //                }
        //            }
        //            ////oBP.ContactEmployees.Add();
        //            oBP.ContactEmployees.Name = moWOQ.U_Contact;//Contact Person 50
        //            oBP.ContactEmployees.E_Mail = moWOQ.U_E_Mail;//Email 100
        //            oBP.ContactEmployees.Phone1 = moWOQ.U_Phone1;//Phone 20
        //            int iResult = oBP.Update();
        //            if (iResult != 0) {

        //                string sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
        //                //DataHandler.INSTANCE.doSystemError("Stocktransfer Error - " + sResult, "Error processing the Wharehouse to Wharehouse stock transfer.");
        //                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Business Partner Address Error - " + sResult + " Error while adding business partner aadress.", "ERSYBPAD",
        //                    new string[] { "Business Partner Address", sResult });
        //                return false;
        //            } else {
        //                return true;
        //            }
        //        } catch (Exception Ex) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
        //            return false;
        //        }

        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doValidateForWO") });
        //        return false;
        //    } finally {
        //        Freeze(false);
        //    }
        //    //return true;
        //}
        ////
        //public bool doValidateForWO() {
        //    try {
        //        if (Mode != BoFormMode.fm_OK_MODE) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Please save form before creating WO.", "ERSAVEFR", new string[] { com.idh.bridge.Translation.getTranslatedWord("Container Group") });
        //            return false;

        //        }
        //        if (moWOQ.U_AppRequired == "Y" && Convert.ToInt16(moWOQ.U_Status) < 5) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Quote need approval before creating WO.", "ERVEQ016", null);
        //            return false;
        //        }
        //        if (moWOQ.U_Status == "7" || moWOQ.U_Status == "8") {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Quote need approval before creating WO.", "ERVEQ017", null);
        //            return false;
        //        }
        //        string sCardName = "";
        //        if (doValidateBPbyCode(ref sCardName) == false) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: You must create customer.", "ERCRETBP", null);
        //            //com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: You must create customer.", "ERCRETBP", new string[] { com.idh.bridge.Translation.getTranslatedWord("Item") });
        //            return false;
        //        } else if (moWOQ.U_ItemGrp == string.Empty) {
        //            // com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: You must create customer.", "ERCRETBP", null);
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Please select Container Group.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Container Group") });
        //            return false;
        //        }
        //        string sBPType = Config.INSTANCE.doGetBPType(moWOQ.U_CardCd);
        //        if (!(sBPType == "C" || sBPType == "S")) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: You cannot create WOH with a Lead Business Partner.", "ERIVBPTP", null);
        //            //com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: You must create customer.", "ERCRETBP", new string[] { com.idh.bridge.Translation.getTranslatedWord("Item") });
        //            return false;
        //        }
        //        if (doValidateNCreateAddressNContact() == false) {
        //            return false;
        //        }
        //        bool ret = moWOQ.WOQItems.doValidateAllRowsforWO();
        //        return ret;

        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doValidateForWO") });
        //        return false;
        //    } finally {
        //        Freeze(false);
        //    }
        //    //return true;
        //}

        //private bool doCreateWO(WOQRequestDates oRDates) {
        //    try {
        //        Freeze(true);
        //        if (doValidateForWO()) {
        //            int iOption = 1;
        //            int iOptionoverride = 1;
        //            int iTotalRows = 0;
        //            for (int i = 0; i <= moWOQ.WOQItems.Count - 1; i++) {
        //                if (moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._WOHID) != string.Empty && moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._WORID) != string.Empty) {
        //                    iTotalRows++;
        //                }
        //            }
        //            if (iTotalRows > 0) {
        //                string sMsg = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ENQMG004", null);
        //                iOptionoverride = com.idh.bridge.resources.Messages.INSTANCE.doMessage(sMsg, 1, Translation.getTranslatedWord("Yes"), Translation.getTranslatedWord("No"), Translation.getTranslatedWord("Cancel"));
        //                if (iOptionoverride != 1 && iOptionoverride != 2)
        //                    return true;
        //            }
        //            iTotalRows = 0;
        //            for (int i = 0; i <= moWOQ.WOQItems.Count - 1; i++) {

        //                if ((moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._WasCd) == string.Empty && moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._WasFDsc) == string.Empty)
        //                             || (moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._AddItmID) == "A")
        //                    || ((moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._Status) == ((int)IDH_WOQITEM.en_WOQITMSTATUS.Cancel).ToString() || moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._Status) == ((int)IDH_WOQITEM.en_WOQITMSTATUS.Close).ToString()))
        //                    || (moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._Sample) == "Y" && (moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._SampStatus) != "Accepted" && moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._SampStatus) != "Cancelled"))
        //                    || (moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._MSDS) == "Y" && (Config.INSTANCE.getValueFromOITMWithNoBuffer(moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._WasCd), "U_LOFile").ToString() != "Y"))) {

        //                } else if (iOptionoverride == 2 && moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._WOHID) != string.Empty && moWOQ.WOQItems.getValueAsString(i, IDH_WOQITEM._WORID) != string.Empty) {
        //                } else
        //                    iTotalRows++;

        //            }
        //            if (iTotalRows == 0) {
        //                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: WO cannot create. There is no row available to convert into WOR.", "ERVEQ018", null);
        //                return true;
        //            } else if (iTotalRows > 1) {
        //                string sMsg = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ENQMG001", null);
        //                iOption = com.idh.bridge.resources.Messages.INSTANCE.doMessage(sMsg, 1, Translation.getTranslatedWord("One"), Translation.getTranslatedWord("Multiple"), Translation.getTranslatedWord("Cancel"));
        //                if (iOption == 3)
        //                    return true;
        //            }
        //            string sWOHNums = "";
        //            Freeze(true);
        //            if (moWOQ.doCreateWObyWOQ(iOption == 1 ? "One" : "Multiple", iOptionoverride, ref sWOHNums, ref moWOQ, oRDates)) {
        //                Freeze(false);
        //                string msg = com.idh.bridge.resources.Messages.getGMessage("INFDOCNO", new string[] { "WO(s)", sWOHNums.Trim().TrimEnd(',') });
        //                com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
        //                Freeze(true);
        //                //moWOQ.U_Status = ((int)IDH_WOQHD.en_WOQSTATUS.WOCreated).ToString();
        //                //this.doLoadData();
        //                moWOQ.getByKey(moWOQ.Code);
        //                moGrid.doApplyRules();
        //                //bool bDoContinue = doAutoSave(IDH_WOQHD.AUTONUMPREFIX, moWOQ.AutoNumKey);//doAutoSave(SBOForm, msMainTable, IDH_WOQHD.AUTONUMPREFIX, msHeaderKeyGen);
        //                HandleFormStatus();
        //                Freeze(false);
        //            }

        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doCreateWO") });

        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}

        //public bool doGetWORRequestDate(com.idh.forms.oo.Form oDialogForm) {
        //    com.isb.forms.Enquiry.WORREquestDate oOOForm = (com.isb.forms.Enquiry.WORREquestDate)oDialogForm;
        //    WOQRequestDates oRDates = oOOForm.RDates;
        //    //string sDate = oOOForm.IDH_RDate;
        //    //DateTime dRequestDate = com.idh.utils.Dates.doStrToDate(sDate);
        //    doCreateWO(oRDates);
        //    return true;
        //}


        //public bool doHandleCreateWO(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    try {
        //        if (pVal.BeforeAction)
        //            return true;
        //        bool mobCreateWO = true;
        //        if (moWOQ.WOQItems.Count > 0) {
        //            moWOQ.WOQItems.first();
        //            if (moWOQ.WOQItems.U_WORID != string.Empty) {
        //                mobCreateWO = false;
        //                com.isb.forms.Enquiry.WORREquestDate oOOForm = new com.isb.forms.Enquiry.WORREquestDate(null, SBOForm.UniqueID, null);
        //                oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doGetWORRequestDate);
        //                oOOForm.WOQItems = moWOQ.WOQItems;
        //                oOOForm.doShowModal(SBOForm);
        //                //oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doCancelAddUpdatePrices);
        //            }
        //        }
        //        if (mobCreateWO)
        //            doCreateWO(null);
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doHandleCreateWO") });

        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}
        //#endregion
        //#region Approvals And Versions
        //public bool doHandleApprovalButton(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {

        //    try {
        //        if (pVal.BeforeAction)
        //            return true;
        //        Freeze(true);
        //        if (Mode != BoFormMode.fm_OK_MODE) {
        //            com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage("UNSAVED");
        //            return true;
        //        }
        //        string sErrorMsgID = "";
        //        string ApproverUserNames = "";
        //        if (moWOQ.doCreateApprovalRequest(ref sErrorMsgID, ref ApproverUserNames)) {
        //            Mode = BoFormMode.fm_VIEW_MODE;
        //            string msg = com.idh.bridge.resources.Messages.getGMessage("WRNBPA12", null);
        //            com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
        //            if (ApproverUserNames.Trim() != string.Empty)
        //                doSendApprovalRequestMessage(ApproverUserNames.Trim().TrimEnd(','));
        //        } else if (sErrorMsgID != "") {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("You cannot send approval request as your name is not is not listed in approvals", sErrorMsgID, null);
        //        }

        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doHandleCreateWO") });

        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}

        //public bool doShowVersionList(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (!pVal.BeforeAction && moWOQ.U_Version > 1) {
        //        com.isb.forms.Enquiry.WOQVersionsList oOOForm = new com.isb.forms.Enquiry.WOQVersionsList(null, SBOForm.UniqueID, SBOForm);

        //        oOOForm.WOQID = Convert.ToInt32(moWOQ.Code);
        //        oOOForm.doOpenForm(oOOForm.IDHForm.gsType);
        //        return false;
        //    }
        //    return true;
        //}

        //public void doSendApprovalRequestMessage(string sAlertUsers) {
        //    string[] aReceipients = sAlertUsers.Split(',');
        //    if (aReceipients.Length == 0)
        //        return;
        //    string sHeaderMsg = Translation.getTranslatedWord("WOQ waiting for approval") + ": " + moWOQ.Code + " V." + moWOQ.U_Version.ToString();
        //    string sAlertMsg = Translation.getTranslatedWord("WOQ waiting for approval:");
        //    sAlertMsg += "\n";
        //    sAlertMsg += "Please review Work Order Quote " + moWOQ.Code + " and make your decision.\n";
        //    sAlertMsg += "From : " + idh.bridge.DataHandler.INSTANCE.User + "\n";
        //    sAlertMsg += "Cc: " + sAlertUsers;
        //    if (!sAlertUsers.Contains(idh.bridge.DataHandler.INSTANCE.User))
        //        sAlertUsers = sAlertUsers + "," + idh.bridge.DataHandler.INSTANCE.User;

        //    //base.IDHForm.goParent.doSendAlert(sAlertUsers.Split(','), sHeaderMsg + ": " + moWOQ.Code, sAlertMsg, BoLinkedObject.lf_BusinessPartner, "BP 163", true);

        //    SAPbobsCOM.Messages msg;
        //    msg = (SAPbobsCOM.Messages)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oMessages);
        //    msg.MessageText = sAlertMsg;
        //    msg.Subject = sHeaderMsg;
        //    bool bValidUsers = false;
        //    for (int i = 0; i <= aReceipients.Length - 1; i++) {
        //        if (aReceipients[i].Trim() == "") {
        //            continue;
        //        }
        //        bValidUsers = true;
        //        msg.Recipients.Add();
        //        msg.Recipients.SetCurrentLine(i);
        //        msg.Recipients.UserCode = aReceipients[i];
        //        msg.Recipients.NameTo = aReceipients[i];
        //        msg.Recipients.SendInternal = SAPbobsCOM.BoYesNoEnum.tYES;
        //        msg.Recipients.UserType = SAPbobsCOM.BoMsgRcpTypes.rt_InternalUser;
        //        object sEMail = Config.INSTANCE.getValueFromTablebyKey("OUSR", "E_Mail", "USER_CODE", aReceipients[i]);
        //        if (sEMail != null && sEMail.ToString() != string.Empty) {
        //            msg.Recipients.EmailAddress = sEMail.ToString();//Config.INSTANCE.getValueFromTablebyKey("OUSR", "E_Mail", "USER_CODE", sUserList[i]);
        //            msg.Recipients.SendEmail = SAPbobsCOM.BoYesNoEnum.tYES;
        //        }
        //    }
        //    if (bValidUsers == false)
        //        return;
        //    int iRet = msg.Add();
        //    if (iRet != 0) {
        //        string sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
        //        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("WOQ Approval request message Error - " + sResult + " Error sending the approval request.", "WRNBPA14",
        //            new string[] { sResult });
        //    }

        //}


        //public void doHandlerWithData(com.idh.dbObjects.DBBase oCaller, object sMessageId) {
        //    if (sMessageId != null && sMessageId.ToString().StartsWith("IDH_CALTOT")) {
        //        doActivateTotalButton(Convert.ToBoolean(sMessageId.ToString().Split(':')[1]));
        //    } else
        //        com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage(sMessageId.ToString());
        //}
        //public int doHandlerYesNoMsgBox(com.idh.dbObjects.DBBase oCaller, string sMessageId, string[] sParams, int iDefButton) {
        //    if (iDefButton == 3) {
        //        string sMsg = com.idh.bridge.resources.Messages.INSTANCE.getMessage(sMessageId, sParams);
        //        return com.idh.bridge.resources.Messages.INSTANCE.doMessage(sMsg.ToString(), iDefButton, "1", "2", "3");
        //    } else
        //        return com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN(sMessageId.ToString(), sParams, iDefButton);
        //}
        #endregion
    }
}