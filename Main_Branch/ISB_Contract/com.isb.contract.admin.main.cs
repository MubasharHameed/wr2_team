﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.bridge.lookups;
using IDHAddOns.idh.data;
using com.idh.bridge;

namespace com.isb.contract.admin {
    public class MainForm {

        public MainForm(IDHAddOns.idh.addon.Base oParent) {
            //IDHAddOns.idh.addon.Base oParent
            doAddForms(oParent);
        }
        //[System.Runtime.InteropServices.DllImport("User32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto, ExactSpelling = true)]
        //public static extern System.IntPtr SetParent(System.IntPtr hWndChild, System.IntPtr hWndParent);

        //[System.Runtime.InteropServices.DllImport("user32.dll")]
        //public static extern bool RedrawWindow(IntPtr hWnd, IntPtr rectUpdate, IntPtr hrgnUpdate, UInt32 flags);

        public static void doCreateMenu(string sMenuID) {
            SAPbouiCOM.Menus oMenus;
            SAPbouiCOM.MenuItem oMenuItem;
            SAPbouiCOM.MenuCreationParams oCreationPackage;

            // 
            if (true) {// Config.ParameterWithDefault("ENQMOD", "FALSE") == "ENQMOD_TRUE"
                try {
                    oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item("IDHCONMN");
                } catch (Exception ex) {
                    oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item(sMenuID);
                    oMenus = oMenuItem.SubMenus;

                    oCreationPackage = (SAPbouiCOM.MenuCreationParams)IDHAddOns.idh.addon.Base.APPLICATION.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                    oCreationPackage.UniqueID = "IDHCONMN";
                    oCreationPackage.String = Translation.getTranslatedWord("Contract Module");
                    oCreationPackage.Position = 6;
                    try {
                        oMenus.AddEx(oCreationPackage);
                    } catch (Exception exx) {
                    }
                }

                //try {
                //    oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item("IDHENQADM");
                //} catch (Exception ex) {
                //    oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item("IDHENQ");
                //    oMenus = oMenuItem.SubMenus;

                //    oCreationPackage = (SAPbouiCOM.MenuCreationParams)IDHAddOns.idh.addon.Base.APPLICATION.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
                //    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                //    oCreationPackage.UniqueID = "IDHENQADM";
                //    oCreationPackage.String = Translation.getTranslatedWord("Admin");
                //    oCreationPackage.Position = 1;

                //    try {
                //        oMenus.AddEx(oCreationPackage);
                //    } catch (Exception ex1) {
                //    }
                //}

                //try {
                //    oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item("ENQAPR");
                //} catch (Exception ex) {
                //    oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item("IDHENQADM");
                //    oMenus = oMenuItem.SubMenus;

                //    oCreationPackage = (SAPbouiCOM.MenuCreationParams)IDHAddOns.idh.addon.Base.APPLICATION.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
                //    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                //    oCreationPackage.UniqueID = "ENQAPR";
                //    oCreationPackage.String = Translation.getTranslatedWord("Approvals");
                //    oCreationPackage.Position = 2;

                //    try {
                //        oMenus.AddEx(oCreationPackage);
                //    } catch (Exception ex1) {

                //    }
                //}
            }
        }

        public static void doAddForms(IDHAddOns.idh.addon.Base oParent) {
            //com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.CreateBP));
             
            
            //com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.WasteItemValidations));
            //com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.WOQVersionsList));
            //com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.WOQuoteVersions));

            //com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.Enquiry), "IDHENQ", 1);//IDHORD

            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.contract.Contract), "IDHCONMN", 1);//IDHORD
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.EnqQuestions(oParent));

           // com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.WGValidationMaster), "IDHENQADM", 70);//IDHADM
           // com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.QuestionnaireMaster), "IDHENQADM", 71);//IDHADM
           // com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.DisposalProcess), "IDHENQADM", 72);//IDHADM
           // com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.DisposalTreatment), "IDHENQADM", 73);//IDHADM
           // com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.DisposalRoute), "IDHENQADM", 74);//IDHADM

           // //doAddForm

           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Search.WasteGroupSearch(oParent));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.admin.WasteGroupValidations(oParent, "IDHENQADM", 74));//IDHADM
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.admin.AdditionalItemValidations(oParent, "IDHENQADM", 75));//IDHADM

           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Search.SearchEnquiry(oParent));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Search.EnqWasteItemSearch(oParent));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Search.EnqWasteProfileItemSearch(oParent));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Search.DisposalRouteSearch(oParent));


           // //Approval
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.admin.Approval.ApprovalSetup(oParent, "ENQAPR", 1));//IDHADM
           // //com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.Approval.Approvals), "ENQAPR", 2);
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.admin.Approval.Approvals(oParent, "ENQAPR", 2));//

           // //  IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.admin.AdditionalItemSearch(oParent));

           // /*       com.idh.forms.oo.Form.doRegisterForm(GetType(com.isb.forms.Enquiry.admin.WGValidationMaster), "IDHADM", 70)
           //        doAddForm(New com.isb.forms.Enquiry.admin.WasteGroupValidations(Me, "IDHADM", 71))
           //    */


           // //Enquiry Lab Module Forms 
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.LabTaskAnalysis(oParent));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.ItemLabAnalysisTempt(oParent));
           // com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.LabStatus), "IDHENQ", 79);
           // com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.LabAnalysis), "IDHENQ", 80);

           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.LabItem(oParent, "IDHENQ", 81));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.LabPreProcess(oParent, "IDHENQ", 82));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.LabTask(oParent, "IDHENQ", 83));
           // //IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.LabResults(oParent, "IDHENQ", 84));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.LabResults(oParent));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.WORList(oParent));

           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Manager.EnquiryManager(oParent, "IDHENQ", 85));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Manager.WOQManager(oParent, "IDHENQ", 86));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Reports.ReportSelection(oParent, "IDHENQ", -1));

           // //Project DORA

           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.PJDORA.PBIMassSelector(oParent, "IDHPS", 87));
           // com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.PJDORA.admin.PBIMassSelectorConfig), "IDHADM", 80);
           // com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.PJDORA.admin.PBIMassUpdaterAmendmentList), "IDHADM", 81);

           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.PJDORA.PBIMassUpdater(oParent, "IDHPS", 88));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.PJDORA.AdHocWOMassUpdater(oParent, "IDHPS", 89));
           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.PJDORA.WORMassUpdater(oParent, "IDHPS", 90));

           //// IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.WORREquestDate(oParent, "IDHENQ", null));
           // com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.WORREquestDate));
           // //com.idh.win.forms.SBOForm.doRegisterForm(typeof(Form1), "IDHENQ", "Win - Disposal Order", 60);
           // //com.idh.form.SBO.Info oInfo = new com.idh.form.SBO.Info();
           // //oInfo.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
           // //oInfo.ControlBox = true;
           // //oInfo.MaximizeBox = true;
           // //oInfo.MinimizeBox = true;
           // //oInfo.doShowInSBO();

           // //com.uBC.form.Test1 oTest1 = new com.uBC.form.Test1();
           // //oTest1.Show();


           // //com.idh.controls.SBOWindow oSBOWin1 = com.idh.controls.SBOWindow.getInstanceByTitle(true);
           // //int iHandle = oSBOWin1.Handle;
           // //oInfo.set
           // ///doSBOInfoPanel(new com.isb.forms.Enquiry.Form1(), oSBOWin1);
           // // oSBOWin1.doSBOInfoPanel(new com.isb.forms.Enquiry.Form1());
           // //  com.idh.controls.WindowsAPIs.
           // //  iHandle = oSBOWin1.Handle;


        }

        //public static void doSBOInfoPanel(System.Windows.Forms.Form oForm, com.idh.controls.SBOWindow oSBOWin1) {


        //    SetParent(oForm.Handle, oSBOWin1.Handle);
        //    oForm.TopMost = true;
        //    oForm.Show();
        //    oForm.ForeColor = System.Drawing.Color.Red;
        //    oForm.Refresh();
        //    oForm.Activate();
        //}
    }
    public class DataStructures {
        public static bool FORCECREATE = false;
        #region Add Messages
        public static void doAddMessages(string sTableName, double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            //if (CurrentReleaseCount < 1054 || FORCECREATE) {
            //    object[,] oData32 = {
            //        {"Code","Name","Cat","Val","Desc"},
            //        {"ERVLDFLD","ERVLDFLD","Messages","Error: [%1] is missing.","Value for the field is missing."},
            //        {"INFDOCNO","INFDOCNO","Messages","[%1] document added with number: [%2]","Document added with number"},
            //        {"ERPKEXIS","ERPKEXIS","Messages","[%1] already exists.","Record already exists."},
            //        {"ERSYBPAD","ERSYBPAD","Messages","[%1] Add - [%2] Error while adding [%1].","Error while adding data."} };
            //    oBase.doUTAddValues(sTableName, oData32, true);
            //}
            
        }
        #endregion

        #region do WR Config Table
        public static void doConfigTableData(string sTableName, double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            //if (CurrentReleaseCount < 1033 || FORCECREATE) {
            //    object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
            //            {"WOQDOFS", "WOQDOFS", "Waste Order Quote", "TRUE", "Do use Offset prices (TRUE, FALSE)"}
            //    };
            //    oBase.doUTAddValues(sTableName, oData, false);
            //}
           
            
        }
        #endregion

        #region do UserTables
        public static void doSetupUserDefined(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            DataStructures moDataStructure = new DataStructures();
            moDataStructure.doAddContractTable(CurrentReleaseCount, oBase);
            moDataStructure.doAddContractDetailTable(CurrentReleaseCount, oBase);
        }
         
        #region "Contract Tables"
        private void doAddContractTable(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_CONTRACT";
            if (CurrentReleaseCount < 1081 || FORCECREATE) {

                if (oBase.doMDCreateTable(sTableName, "Contract Header")) {
                    oBase.doAddNumberSeq("SEQCONTH", "Service Header", 0);
                  
                    oBase.doMDCreateField(sTableName, "CardCd", "Customer Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "CardNm", "Customer name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

                    //oBase.doMDCreateField(sTableName, "CName", "Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);

                    oBase.doMDCreateField(sTableName, "SDate", "Start Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "EDate", "End Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "CDate", "Create Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);


                    oBase.doMDCreateField(sTableName, "BAddress", "Bill Address ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDUpdateField(sTableName, "BAddrssLN", "Bill Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "BStreet", "Bill Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "BBlock", "Bill Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "BCity", "Bill City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "BCounty", "Bill County", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "BState", "Bill State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
                    oBase.doMDCreateField(sTableName, "BZipCode", "Bill Post Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "BCountry", "Bill Country", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);

                    oBase.doMDCreateField(sTableName, "SAddress", "Ship Address ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDUpdateField(sTableName, "SAddrssLN", "Ship Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "SStreet", "Ship Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "SBlock", "Ship Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "SCity", "Ship City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "SCounty", "Ship County", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "SState", "Ship State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
                    oBase.doMDCreateField(sTableName, "SZipCode", "Ship Post Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "SCountry", "Ship Country", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);

                    oBase.doMDCreateField(sTableName, "TAddress", "Tip Address ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDUpdateField(sTableName, "TAddrssLN", "Tip Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "TStreet", "Tip Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "TBlock", "Tip Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "TCity", "Tip City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "TCounty", "Tip County", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "TState", "Tip State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
                    oBase.doMDCreateField(sTableName, "TZipCode", "Tip Post Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "TCountry", "Tip Country", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);


                    oBase.doMDCreateField(sTableName, "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "AsignTo", "Assigned To", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 155);
                    oBase.doMDCreateField(sTableName, "User", "Created By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 155);
                    oBase.doMDCreateField(sTableName, "Branch", "Branch", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 40);
                    oBase.doMDCreateField(sTableName, "Route", "Route", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                }
            }
        }
        private void doAddContractDetailTable(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_CONTRTROW";
            if (CurrentReleaseCount < 1081 || FORCECREATE) {

                sTableName = "@IDH_CONTRTROW";
                if (oBase.doMDCreateTable(sTableName, "Contract Detail")) {
                    oBase.doAddNumberSeq("SEQCONTR", "Contract Detail", 0);

                    oBase.doMDUpdateField(sTableName, "ContractNo", "Contract No", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDUpdateField(sTableName, "Qty", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
                    oBase.doMDCreateField(sTableName, "ItemCd", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "ItemNm", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDUpdateField(sTableName, "Delivery", "Delivery", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "DlvDate", "Delivery Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "Tip", "Disposal Site Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "TipName", "Disposal Site Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "TipAdr", "Disposal Site Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "TipAdrId", "Disposal Site Address Cd", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);

                    oBase.doMDCreateField(sTableName, "WastCd", "Wast Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "WastNm", "Wast Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);


                    oBase.doMDCreateField(sTableName, "JobType", "Order Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "SDate", "Task Start Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "Freq", "Frequency", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

                    oBase.doMDUpdateField(sTableName, "Mon", "Monday Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDUpdateField(sTableName, "Tue", "Tuesday Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDUpdateField(sTableName, "Wed", "Wednesday Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDUpdateField(sTableName, "Thu", "Thursday Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDUpdateField(sTableName, "Fri", "Friday Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDUpdateField(sTableName, "Sat", "Saturday Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDUpdateField(sTableName, "Sun", "Sunday Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    //HaulagePrice
                    oBase.doMDCreateField(sTableName, "HUOM", "Haulage UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "HlgPrcTyp", "Haulage Price Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "HlgCIPID", "Haulage CIPID", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDUpdateField(sTableName, "HlgCon", "Haulage Contract Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "HlgPrice", "Haulage Price", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //DisposalPrice
                    oBase.doMDCreateField(sTableName, "TipUOM", "Disposal UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "TipPrcTyp", "Disposal Price Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "TipCIPID", "Disposal CIPID", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDUpdateField(sTableName, "TipCon", "Disposal Contract Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "TipPrice", "Disposal Price", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);

                    oBase.doMDCreateField(sTableName, "TranCd", "Transaction Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "PBI", "PBI Code", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "PBINxRDat", "PBI Next Run Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "WOR", "WOR Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);

                }
            }
        }
        #endregion

        #endregion
    }
}
