/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 22/01/2018 07:38:40
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.contract.dbObjects.User {
    [Serializable]
    public class IDH_CONTRACT : com.isb.contract.dbObjects.Base.IDH_CONTRACT {

        public enum en_ContractSTATUS {
            New = 1,
            WaitingForApproval = 2,
            ReviewAdvised = 3,
            Rejected = 4,
            Approved = 5,
            WOCreated = 6,
            Cancelled = 7,
            Closed = 8
        };


        public IDH_CONTRACT() : base() {
            msAutoNumKey = "SEQCONTH";
        }

        public IDH_CONTRACT(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQCONTH";
        }
        public et_ExternalTriggerWithData Handler_ExternalTriggerWithData = null;
        //public et_ExternalTriggerWithDataAtRow Handler_ExternalTriggerWithData = null;
        public et_ExternalYNOption Handler_ExternalTriggerWithData_ShowYesNO = null;
        
        private IDH_CONTRTROW moContractItems;
        public IDH_CONTRTROW ContractItems {
            get { return moContractItems; }
            //set { moEnquiryItems = value; }
        }

        public bool hasRows {
            get { return moContractItems!= null && moContractItems.Count > 0; }
        }

        public IDH_CONTRTROW getRowDB() {
            return moContractItems;
        }

        #region overrides

        public override void doRegisterChildren() {
            if (moContractItems== null) {
                //if (ParentControl != null)
                //    moEnquiryItems = new IDH_ENQITEM(ParentControl, this);
                //else
                moContractItems = new IDH_CONTRTROW(this);
            }

            addChild("ContractItems", moContractItems, _Code, IDH_CONTRTROW._ContractNo);
        }

        #endregion

        public bool doFormHaveSomeData() {
            return true;
            //if (mbFormHasDatatoSave || U_CardCd != string.Empty || U_CardNM != string.Empty || U_Address != string.Empty || U_City != string.Empty || U_ZpCd != string.Empty) {
            //    return true;
            //} else
            //    return false;
        }
    }
}
