/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 22/01/2018 10:44:10
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;
using System.Windows.Forms;

namespace com.isb.contract.dbObjects.Base{
   [Serializable]
	public class IDH_CONTRTROW: com.idh.dbObjects.DBBase { 

		private Linker moLinker = null;
       public Linker ControlLinker {
           get { return moLinker; }
           set { moLinker = value; }
       }

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_CONTRTROW() : base("@IDH_CONTRTROW"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_CONTRTROW( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_CONTRTROW"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_CONTRTROW";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Contract No
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ContractNo
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ContractNo = "U_ContractNo";
		public string U_ContractNo { 
			get {
 				return getValueAsString(_ContractNo); 
			}
			set { setValue(_ContractNo, value); }
		}
           public string doValidate_ContractNo() {
               return doValidate_ContractNo(U_ContractNo);
           }
           public virtual string doValidate_ContractNo(object oValue) {
               return base.doValidation(_ContractNo, oValue);
           }

		/**
		 * Decription: Delivery
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Delivery
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Delivery = "U_Delivery";
		public string U_Delivery { 
			get {
 				return getValueAsString(_Delivery); 
			}
			set { setValue(_Delivery, value); }
		}
           public string doValidate_Delivery() {
               return doValidate_Delivery(U_Delivery);
           }
           public virtual string doValidate_Delivery(object oValue) {
               return base.doValidation(_Delivery, oValue);
           }

		/**
		 * Decription: Delivery Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DlvDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _DlvDate = "U_DlvDate";
		public DateTime U_DlvDate { 
			get {
 				return getValueAsDateTime(_DlvDate); 
			}
			set { setValue(_DlvDate, value); }
		}
		public void U_DlvDate_AsString(string value){
			setValue("U_DlvDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_DlvDate_AsString(){
			DateTime dVal = getValueAsDateTime(_DlvDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_DlvDate() {
               return doValidate_DlvDate(U_DlvDate);
           }
           public virtual string doValidate_DlvDate(object oValue) {
               return base.doValidation(_DlvDate, oValue);
           }

		/**
		 * Decription: Frequency
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Freq
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Freq = "U_Freq";
		public string U_Freq { 
			get {
 				return getValueAsString(_Freq); 
			}
			set { setValue(_Freq, value); }
		}
           public string doValidate_Freq() {
               return doValidate_Freq(U_Freq);
           }
           public virtual string doValidate_Freq(object oValue) {
               return base.doValidation(_Freq, oValue);
           }

		/**
		 * Decription: Friday Checkbox
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Fri
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Fri = "U_Fri";
		public string U_Fri { 
			get {
 				return getValueAsString(_Fri); 
			}
			set { setValue(_Fri, value); }
		}
           public string doValidate_Fri() {
               return doValidate_Fri(U_Fri);
           }
           public virtual string doValidate_Fri(object oValue) {
               return base.doValidation(_Fri, oValue);
           }

		/**
		 * Decription: Haulage CIPID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HlgCIPID
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _HlgCIPID = "U_HlgCIPID";
		public int U_HlgCIPID { 
			get {
 				return getValueAsInt(_HlgCIPID); 
			}
			set { setValue(_HlgCIPID, value); }
		}
           public string doValidate_HlgCIPID() {
               return doValidate_HlgCIPID(U_HlgCIPID);
           }
           public virtual string doValidate_HlgCIPID(object oValue) {
               return base.doValidation(_HlgCIPID, oValue);
           }

		/**
		 * Decription: Haulage Contract Checkbox
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HlgCon
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _HlgCon = "U_HlgCon";
		public string U_HlgCon { 
			get {
 				return getValueAsString(_HlgCon); 
			}
			set { setValue(_HlgCon, value); }
		}
           public string doValidate_HlgCon() {
               return doValidate_HlgCon(U_HlgCon);
           }
           public virtual string doValidate_HlgCon(object oValue) {
               return base.doValidation(_HlgCon, oValue);
           }

		/**
		 * Decription: Haulage Price Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HlgPrcTyp
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _HlgPrcTyp = "U_HlgPrcTyp";
		public string U_HlgPrcTyp { 
			get {
 				return getValueAsString(_HlgPrcTyp); 
			}
			set { setValue(_HlgPrcTyp, value); }
		}
           public string doValidate_HlgPrcTyp() {
               return doValidate_HlgPrcTyp(U_HlgPrcTyp);
           }
           public virtual string doValidate_HlgPrcTyp(object oValue) {
               return base.doValidation(_HlgPrcTyp, oValue);
           }

		/**
		 * Decription: Haulage Price
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HlgPrice
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _HlgPrice = "U_HlgPrice";
		public double U_HlgPrice { 
			get {
 				return getValueAsDouble(_HlgPrice); 
			}
			set { setValue(_HlgPrice, value); }
		}
           public string doValidate_HlgPrice() {
               return doValidate_HlgPrice(U_HlgPrice);
           }
           public virtual string doValidate_HlgPrice(object oValue) {
               return base.doValidation(_HlgPrice, oValue);
           }

		/**
		 * Decription: Haulage UOM
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HUOM
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _HUOM = "U_HUOM";
		public string U_HUOM { 
			get {
 				return getValueAsString(_HUOM); 
			}
			set { setValue(_HUOM, value); }
		}
           public string doValidate_HUOM() {
               return doValidate_HUOM(U_HUOM);
           }
           public virtual string doValidate_HUOM(object oValue) {
               return base.doValidation(_HUOM, oValue);
           }

		/**
		 * Decription: Item Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ItemCd = "U_ItemCd";
		public string U_ItemCd { 
			get {
 				return getValueAsString(_ItemCd); 
			}
			set { setValue(_ItemCd, value); }
		}
           public string doValidate_ItemCd() {
               return doValidate_ItemCd(U_ItemCd);
           }
           public virtual string doValidate_ItemCd(object oValue) {
               return base.doValidation(_ItemCd, oValue);
           }

		/**
		 * Decription: Item Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ItemNm = "U_ItemNm";
		public string U_ItemNm { 
			get {
 				return getValueAsString(_ItemNm); 
			}
			set { setValue(_ItemNm, value); }
		}
           public string doValidate_ItemNm() {
               return doValidate_ItemNm(U_ItemNm);
           }
           public virtual string doValidate_ItemNm(object oValue) {
               return base.doValidation(_ItemNm, oValue);
           }

		/**
		 * Decription: Order Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JobType
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _JobType = "U_JobType";
		public string U_JobType { 
			get {
 				return getValueAsString(_JobType); 
			}
			set { setValue(_JobType, value); }
		}
           public string doValidate_JobType() {
               return doValidate_JobType(U_JobType);
           }
           public virtual string doValidate_JobType(object oValue) {
               return base.doValidation(_JobType, oValue);
           }

		/**
		 * Decription: Monday Checkbox
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Mon
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Mon = "U_Mon";
		public string U_Mon { 
			get {
 				return getValueAsString(_Mon); 
			}
			set { setValue(_Mon, value); }
		}
           public string doValidate_Mon() {
               return doValidate_Mon(U_Mon);
           }
           public virtual string doValidate_Mon(object oValue) {
               return base.doValidation(_Mon, oValue);
           }

		/**
		 * Decription: PBI Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PBI
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _PBI = "U_PBI";
		public int U_PBI { 
			get {
 				return getValueAsInt(_PBI); 
			}
			set { setValue(_PBI, value); }
		}
           public string doValidate_PBI() {
               return doValidate_PBI(U_PBI);
           }
           public virtual string doValidate_PBI(object oValue) {
               return base.doValidation(_PBI, oValue);
           }

		/**
		 * Decription: PBI Next Run Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PBINxRDat
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _PBINxRDat = "U_PBINxRDat";
		public DateTime U_PBINxRDat { 
			get {
 				return getValueAsDateTime(_PBINxRDat); 
			}
			set { setValue(_PBINxRDat, value); }
		}
		public void U_PBINxRDat_AsString(string value){
			setValue("U_PBINxRDat", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_PBINxRDat_AsString(){
			DateTime dVal = getValueAsDateTime(_PBINxRDat);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_PBINxRDat() {
               return doValidate_PBINxRDat(U_PBINxRDat);
           }
           public virtual string doValidate_PBINxRDat(object oValue) {
               return base.doValidation(_PBINxRDat, oValue);
           }

		/**
		 * Decription: Quantity
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Qty
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Qty = "U_Qty";
		public double U_Qty { 
			get {
 				return getValueAsDouble(_Qty); 
			}
			set { setValue(_Qty, value); }
		}
           public string doValidate_Qty() {
               return doValidate_Qty(U_Qty);
           }
           public virtual string doValidate_Qty(object oValue) {
               return base.doValidation(_Qty, oValue);
           }

		/**
		 * Decription: Saturday Checkbox
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Sat
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Sat = "U_Sat";
		public string U_Sat { 
			get {
 				return getValueAsString(_Sat); 
			}
			set { setValue(_Sat, value); }
		}
           public string doValidate_Sat() {
               return doValidate_Sat(U_Sat);
           }
           public virtual string doValidate_Sat(object oValue) {
               return base.doValidation(_Sat, oValue);
           }

		/**
		 * Decription: Task Start Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _SDate = "U_SDate";
		public DateTime U_SDate { 
			get {
 				return getValueAsDateTime(_SDate); 
			}
			set { setValue(_SDate, value); }
		}
		public void U_SDate_AsString(string value){
			setValue("U_SDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_SDate_AsString(){
			DateTime dVal = getValueAsDateTime(_SDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_SDate() {
               return doValidate_SDate(U_SDate);
           }
           public virtual string doValidate_SDate(object oValue) {
               return base.doValidation(_SDate, oValue);
           }

		/**
		 * Decription: Sunday Checkbox
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Sun
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Sun = "U_Sun";
		public string U_Sun { 
			get {
 				return getValueAsString(_Sun); 
			}
			set { setValue(_Sun, value); }
		}
           public string doValidate_Sun() {
               return doValidate_Sun(U_Sun);
           }
           public virtual string doValidate_Sun(object oValue) {
               return base.doValidation(_Sun, oValue);
           }

		/**
		 * Decription: Thursday Checkbox
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Thu
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Thu = "U_Thu";
		public string U_Thu { 
			get {
 				return getValueAsString(_Thu); 
			}
			set { setValue(_Thu, value); }
		}
           public string doValidate_Thu() {
               return doValidate_Thu(U_Thu);
           }
           public virtual string doValidate_Thu(object oValue) {
               return base.doValidation(_Thu, oValue);
           }

		/**
		 * Decription: Disposal Site Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Tip
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Tip = "U_Tip";
		public string U_Tip { 
			get {
 				return getValueAsString(_Tip); 
			}
			set { setValue(_Tip, value); }
		}
           public string doValidate_Tip() {
               return doValidate_Tip(U_Tip);
           }
           public virtual string doValidate_Tip(object oValue) {
               return base.doValidation(_Tip, oValue);
           }

		/**
		 * Decription: Disposal Site Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipAdr
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TipAdr = "U_TipAdr";
		public string U_TipAdr { 
			get {
 				return getValueAsString(_TipAdr); 
			}
			set { setValue(_TipAdr, value); }
		}
           public string doValidate_TipAdr() {
               return doValidate_TipAdr(U_TipAdr);
           }
           public virtual string doValidate_TipAdr(object oValue) {
               return base.doValidation(_TipAdr, oValue);
           }

		/**
		 * Decription: Disposal Site Address Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipAdrId
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TipAdrId = "U_TipAdrId";
		public string U_TipAdrId { 
			get {
 				return getValueAsString(_TipAdrId); 
			}
			set { setValue(_TipAdrId, value); }
		}
           public string doValidate_TipAdrId() {
               return doValidate_TipAdrId(U_TipAdrId);
           }
           public virtual string doValidate_TipAdrId(object oValue) {
               return base.doValidation(_TipAdrId, oValue);
           }

		/**
		 * Decription: Disposal CIPID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipCIPID
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TipCIPID = "U_TipCIPID";
		public int U_TipCIPID { 
			get {
 				return getValueAsInt(_TipCIPID); 
			}
			set { setValue(_TipCIPID, value); }
		}
           public string doValidate_TipCIPID() {
               return doValidate_TipCIPID(U_TipCIPID);
           }
           public virtual string doValidate_TipCIPID(object oValue) {
               return base.doValidation(_TipCIPID, oValue);
           }

		/**
		 * Decription: Disposal Contract Checkbox
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipCon
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TipCon = "U_TipCon";
		public string U_TipCon { 
			get {
 				return getValueAsString(_TipCon); 
			}
			set { setValue(_TipCon, value); }
		}
           public string doValidate_TipCon() {
               return doValidate_TipCon(U_TipCon);
           }
           public virtual string doValidate_TipCon(object oValue) {
               return base.doValidation(_TipCon, oValue);
           }

		/**
		 * Decription: Disposal Site Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TipName = "U_TipName";
		public string U_TipName { 
			get {
 				return getValueAsString(_TipName); 
			}
			set { setValue(_TipName, value); }
		}
           public string doValidate_TipName() {
               return doValidate_TipName(U_TipName);
           }
           public virtual string doValidate_TipName(object oValue) {
               return base.doValidation(_TipName, oValue);
           }

		/**
		 * Decription: Disposal Price Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipPrcTyp
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TipPrcTyp = "U_TipPrcTyp";
		public string U_TipPrcTyp { 
			get {
 				return getValueAsString(_TipPrcTyp); 
			}
			set { setValue(_TipPrcTyp, value); }
		}
           public string doValidate_TipPrcTyp() {
               return doValidate_TipPrcTyp(U_TipPrcTyp);
           }
           public virtual string doValidate_TipPrcTyp(object oValue) {
               return base.doValidation(_TipPrcTyp, oValue);
           }

		/**
		 * Decription: Disposal Price
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipPrice
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TipPrice = "U_TipPrice";
		public double U_TipPrice { 
			get {
 				return getValueAsDouble(_TipPrice); 
			}
			set { setValue(_TipPrice, value); }
		}
           public string doValidate_TipPrice() {
               return doValidate_TipPrice(U_TipPrice);
           }
           public virtual string doValidate_TipPrice(object oValue) {
               return base.doValidation(_TipPrice, oValue);
           }

		/**
		 * Decription: Disposal UOM
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipUOM
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TipUOM = "U_TipUOM";
		public string U_TipUOM { 
			get {
 				return getValueAsString(_TipUOM); 
			}
			set { setValue(_TipUOM, value); }
		}
           public string doValidate_TipUOM() {
               return doValidate_TipUOM(U_TipUOM);
           }
           public virtual string doValidate_TipUOM(object oValue) {
               return base.doValidation(_TipUOM, oValue);
           }

		/**
		 * Decription: Transaction Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TranCd
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TranCd = "U_TranCd";
		public string U_TranCd { 
			get {
 				return getValueAsString(_TranCd); 
			}
			set { setValue(_TranCd, value); }
		}
           public string doValidate_TranCd() {
               return doValidate_TranCd(U_TranCd);
           }
           public virtual string doValidate_TranCd(object oValue) {
               return base.doValidation(_TranCd, oValue);
           }

		/**
		 * Decription: Tuesday Checkbox
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Tue
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Tue = "U_Tue";
		public string U_Tue { 
			get {
 				return getValueAsString(_Tue); 
			}
			set { setValue(_Tue, value); }
		}
           public string doValidate_Tue() {
               return doValidate_Tue(U_Tue);
           }
           public virtual string doValidate_Tue(object oValue) {
               return base.doValidation(_Tue, oValue);
           }

		/**
		 * Decription: Wast Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WastCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WastCd = "U_WastCd";
		public string U_WastCd { 
			get {
 				return getValueAsString(_WastCd); 
			}
			set { setValue(_WastCd, value); }
		}
           public string doValidate_WastCd() {
               return doValidate_WastCd(U_WastCd);
           }
           public virtual string doValidate_WastCd(object oValue) {
               return base.doValidation(_WastCd, oValue);
           }

		/**
		 * Decription: Wast Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WastNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WastNm = "U_WastNm";
		public string U_WastNm { 
			get {
 				return getValueAsString(_WastNm); 
			}
			set { setValue(_WastNm, value); }
		}
           public string doValidate_WastNm() {
               return doValidate_WastNm(U_WastNm);
           }
           public virtual string doValidate_WastNm(object oValue) {
               return base.doValidation(_WastNm, oValue);
           }

		/**
		 * Decription: Wednesday Checkbox
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Wed
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Wed = "U_Wed";
		public string U_Wed { 
			get {
 				return getValueAsString(_Wed); 
			}
			set { setValue(_Wed, value); }
		}
           public string doValidate_Wed() {
               return doValidate_Wed(U_Wed);
           }
           public virtual string doValidate_Wed(object oValue) {
               return base.doValidation(_Wed, oValue);
           }

		/**
		 * Decription: WOR Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WOR
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WOR = "U_WOR";
		public string U_WOR { 
			get {
 				return getValueAsString(_WOR); 
			}
			set { setValue(_WOR, value); }
		}
           public string doValidate_WOR() {
               return doValidate_WOR(U_WOR);
           }
           public virtual string doValidate_WOR(object oValue) {
               return base.doValidation(_WOR, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(38);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_ContractNo, "Contract No", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Contract No
			moDBFields.Add(_Delivery, "Delivery", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Delivery
			moDBFields.Add(_DlvDate, "Delivery Date", 4, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Delivery Date
			moDBFields.Add(_Freq, "Frequency", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Frequency
			moDBFields.Add(_Fri, "Friday Checkbox", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Friday Checkbox
			moDBFields.Add(_HlgCIPID, "Haulage CIPID", 7, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Haulage CIPID
			moDBFields.Add(_HlgCon, "Haulage Contract Checkbox", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Haulage Contract Checkbox
			moDBFields.Add(_HlgPrcTyp, "Haulage Price Type", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Haulage Price Type
			moDBFields.Add(_HlgPrice, "Haulage Price", 10, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Haulage Price
			moDBFields.Add(_HUOM, "Haulage UOM", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Haulage UOM
			moDBFields.Add(_ItemCd, "Item Code", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Item Code
			moDBFields.Add(_ItemNm, "Item Name", 13, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Item Name
			moDBFields.Add(_JobType, "Order Type", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Order Type
			moDBFields.Add(_Mon, "Monday Checkbox", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Monday Checkbox
			moDBFields.Add(_PBI, "PBI Code", 16, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //PBI Code
			moDBFields.Add(_PBINxRDat, "PBI Next Run Date", 17, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //PBI Next Run Date
			moDBFields.Add(_Qty, "Quantity", 18, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Quantity
			moDBFields.Add(_Sat, "Saturday Checkbox", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Saturday Checkbox
			moDBFields.Add(_SDate, "Task Start Date", 20, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Task Start Date
			moDBFields.Add(_Sun, "Sunday Checkbox", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Sunday Checkbox
			moDBFields.Add(_Thu, "Thursday Checkbox", 22, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Thursday Checkbox
			moDBFields.Add(_Tip, "Disposal Site Code", 23, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Disposal Site Code
			moDBFields.Add(_TipAdr, "Disposal Site Address", 24, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Disposal Site Address
			moDBFields.Add(_TipAdrId, "Disposal Site Address Cd", 25, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Disposal Site Address Cd
			moDBFields.Add(_TipCIPID, "Disposal CIPID", 26, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Disposal CIPID
			moDBFields.Add(_TipCon, "Disposal Contract Checkbox", 27, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Disposal Contract Checkbox
			moDBFields.Add(_TipName, "Disposal Site Description", 28, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Disposal Site Description
			moDBFields.Add(_TipPrcTyp, "Disposal Price Type", 29, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Disposal Price Type
			moDBFields.Add(_TipPrice, "Disposal Price", 30, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Disposal Price
			moDBFields.Add(_TipUOM, "Disposal UOM", 31, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Disposal UOM
			moDBFields.Add(_TranCd, "Transaction Code", 32, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Transaction Code
			moDBFields.Add(_Tue, "Tuesday Checkbox", 33, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Tuesday Checkbox
			moDBFields.Add(_WastCd, "Wast Code", 34, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Wast Code
			moDBFields.Add(_WastNm, "Wast Name", 35, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Wast Name
			moDBFields.Add(_Wed, "Wednesday Checkbox", 36, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Wednesday Checkbox
			moDBFields.Add(_WOR, "WOR Code", 37, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //WOR Code

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_ContractNo());
            doBuildValidationString(doValidate_Delivery());
            doBuildValidationString(doValidate_DlvDate());
            doBuildValidationString(doValidate_Freq());
            doBuildValidationString(doValidate_Fri());
            doBuildValidationString(doValidate_HlgCIPID());
            doBuildValidationString(doValidate_HlgCon());
            doBuildValidationString(doValidate_HlgPrcTyp());
            doBuildValidationString(doValidate_HlgPrice());
            doBuildValidationString(doValidate_HUOM());
            doBuildValidationString(doValidate_ItemCd());
            doBuildValidationString(doValidate_ItemNm());
            doBuildValidationString(doValidate_JobType());
            doBuildValidationString(doValidate_Mon());
            doBuildValidationString(doValidate_PBI());
            doBuildValidationString(doValidate_PBINxRDat());
            doBuildValidationString(doValidate_Qty());
            doBuildValidationString(doValidate_Sat());
            doBuildValidationString(doValidate_SDate());
            doBuildValidationString(doValidate_Sun());
            doBuildValidationString(doValidate_Thu());
            doBuildValidationString(doValidate_Tip());
            doBuildValidationString(doValidate_TipAdr());
            doBuildValidationString(doValidate_TipAdrId());
            doBuildValidationString(doValidate_TipCIPID());
            doBuildValidationString(doValidate_TipCon());
            doBuildValidationString(doValidate_TipName());
            doBuildValidationString(doValidate_TipPrcTyp());
            doBuildValidationString(doValidate_TipPrice());
            doBuildValidationString(doValidate_TipUOM());
            doBuildValidationString(doValidate_TranCd());
            doBuildValidationString(doValidate_Tue());
            doBuildValidationString(doValidate_WastCd());
            doBuildValidationString(doValidate_WastNm());
            doBuildValidationString(doValidate_Wed());
            doBuildValidationString(doValidate_WOR());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_ContractNo)) return doValidate_ContractNo(oValue);
            if (sFieldName.Equals(_Delivery)) return doValidate_Delivery(oValue);
            if (sFieldName.Equals(_DlvDate)) return doValidate_DlvDate(oValue);
            if (sFieldName.Equals(_Freq)) return doValidate_Freq(oValue);
            if (sFieldName.Equals(_Fri)) return doValidate_Fri(oValue);
            if (sFieldName.Equals(_HlgCIPID)) return doValidate_HlgCIPID(oValue);
            if (sFieldName.Equals(_HlgCon)) return doValidate_HlgCon(oValue);
            if (sFieldName.Equals(_HlgPrcTyp)) return doValidate_HlgPrcTyp(oValue);
            if (sFieldName.Equals(_HlgPrice)) return doValidate_HlgPrice(oValue);
            if (sFieldName.Equals(_HUOM)) return doValidate_HUOM(oValue);
            if (sFieldName.Equals(_ItemCd)) return doValidate_ItemCd(oValue);
            if (sFieldName.Equals(_ItemNm)) return doValidate_ItemNm(oValue);
            if (sFieldName.Equals(_JobType)) return doValidate_JobType(oValue);
            if (sFieldName.Equals(_Mon)) return doValidate_Mon(oValue);
            if (sFieldName.Equals(_PBI)) return doValidate_PBI(oValue);
            if (sFieldName.Equals(_PBINxRDat)) return doValidate_PBINxRDat(oValue);
            if (sFieldName.Equals(_Qty)) return doValidate_Qty(oValue);
            if (sFieldName.Equals(_Sat)) return doValidate_Sat(oValue);
            if (sFieldName.Equals(_SDate)) return doValidate_SDate(oValue);
            if (sFieldName.Equals(_Sun)) return doValidate_Sun(oValue);
            if (sFieldName.Equals(_Thu)) return doValidate_Thu(oValue);
            if (sFieldName.Equals(_Tip)) return doValidate_Tip(oValue);
            if (sFieldName.Equals(_TipAdr)) return doValidate_TipAdr(oValue);
            if (sFieldName.Equals(_TipAdrId)) return doValidate_TipAdrId(oValue);
            if (sFieldName.Equals(_TipCIPID)) return doValidate_TipCIPID(oValue);
            if (sFieldName.Equals(_TipCon)) return doValidate_TipCon(oValue);
            if (sFieldName.Equals(_TipName)) return doValidate_TipName(oValue);
            if (sFieldName.Equals(_TipPrcTyp)) return doValidate_TipPrcTyp(oValue);
            if (sFieldName.Equals(_TipPrice)) return doValidate_TipPrice(oValue);
            if (sFieldName.Equals(_TipUOM)) return doValidate_TipUOM(oValue);
            if (sFieldName.Equals(_TranCd)) return doValidate_TranCd(oValue);
            if (sFieldName.Equals(_Tue)) return doValidate_Tue(oValue);
            if (sFieldName.Equals(_WastCd)) return doValidate_WastCd(oValue);
            if (sFieldName.Equals(_WastNm)) return doValidate_WastNm(oValue);
            if (sFieldName.Equals(_Wed)) return doValidate_Wed(oValue);
            if (sFieldName.Equals(_WOR)) return doValidate_WOR(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_ContractNo Field to the Form Item.
		 */
		public void doLink_ContractNo(string sControlName){
			moLinker.doLinkDataToControl(_ContractNo, sControlName);
		}
		/**
		 * Link the U_Delivery Field to the Form Item.
		 */
		public void doLink_Delivery(string sControlName){
			moLinker.doLinkDataToControl(_Delivery, sControlName);
		}
		/**
		 * Link the U_DlvDate Field to the Form Item.
		 */
		public void doLink_DlvDate(string sControlName){
			moLinker.doLinkDataToControl(_DlvDate, sControlName);
		}
		/**
		 * Link the U_Freq Field to the Form Item.
		 */
		public void doLink_Freq(string sControlName){
			moLinker.doLinkDataToControl(_Freq, sControlName);
		}
		/**
		 * Link the U_Fri Field to the Form Item.
		 */
		public void doLink_Fri(string sControlName){
			moLinker.doLinkDataToControl(_Fri, sControlName);
		}
		/**
		 * Link the U_HlgCIPID Field to the Form Item.
		 */
		public void doLink_HlgCIPID(string sControlName){
			moLinker.doLinkDataToControl(_HlgCIPID, sControlName);
		}
		/**
		 * Link the U_HlgCon Field to the Form Item.
		 */
		public void doLink_HlgCon(string sControlName){
			moLinker.doLinkDataToControl(_HlgCon, sControlName);
		}
		/**
		 * Link the U_HlgPrcTyp Field to the Form Item.
		 */
		public void doLink_HlgPrcTyp(string sControlName){
			moLinker.doLinkDataToControl(_HlgPrcTyp, sControlName);
		}
		/**
		 * Link the U_HlgPrice Field to the Form Item.
		 */
		public void doLink_HlgPrice(string sControlName){
			moLinker.doLinkDataToControl(_HlgPrice, sControlName);
		}
		/**
		 * Link the U_HUOM Field to the Form Item.
		 */
		public void doLink_HUOM(string sControlName){
			moLinker.doLinkDataToControl(_HUOM, sControlName);
		}
		/**
		 * Link the U_ItemCd Field to the Form Item.
		 */
		public void doLink_ItemCd(string sControlName){
			moLinker.doLinkDataToControl(_ItemCd, sControlName);
		}
		/**
		 * Link the U_ItemNm Field to the Form Item.
		 */
		public void doLink_ItemNm(string sControlName){
			moLinker.doLinkDataToControl(_ItemNm, sControlName);
		}
		/**
		 * Link the U_JobType Field to the Form Item.
		 */
		public void doLink_JobType(string sControlName){
			moLinker.doLinkDataToControl(_JobType, sControlName);
		}
		/**
		 * Link the U_Mon Field to the Form Item.
		 */
		public void doLink_Mon(string sControlName){
			moLinker.doLinkDataToControl(_Mon, sControlName);
		}
		/**
		 * Link the U_PBI Field to the Form Item.
		 */
		public void doLink_PBI(string sControlName){
			moLinker.doLinkDataToControl(_PBI, sControlName);
		}
		/**
		 * Link the U_PBINxRDat Field to the Form Item.
		 */
		public void doLink_PBINxRDat(string sControlName){
			moLinker.doLinkDataToControl(_PBINxRDat, sControlName);
		}
		/**
		 * Link the U_Qty Field to the Form Item.
		 */
		public void doLink_Qty(string sControlName){
			moLinker.doLinkDataToControl(_Qty, sControlName);
		}
		/**
		 * Link the U_Sat Field to the Form Item.
		 */
		public void doLink_Sat(string sControlName){
			moLinker.doLinkDataToControl(_Sat, sControlName);
		}
		/**
		 * Link the U_SDate Field to the Form Item.
		 */
		public void doLink_SDate(string sControlName){
			moLinker.doLinkDataToControl(_SDate, sControlName);
		}
		/**
		 * Link the U_Sun Field to the Form Item.
		 */
		public void doLink_Sun(string sControlName){
			moLinker.doLinkDataToControl(_Sun, sControlName);
		}
		/**
		 * Link the U_Thu Field to the Form Item.
		 */
		public void doLink_Thu(string sControlName){
			moLinker.doLinkDataToControl(_Thu, sControlName);
		}
		/**
		 * Link the U_Tip Field to the Form Item.
		 */
		public void doLink_Tip(string sControlName){
			moLinker.doLinkDataToControl(_Tip, sControlName);
		}
		/**
		 * Link the U_TipAdr Field to the Form Item.
		 */
		public void doLink_TipAdr(string sControlName){
			moLinker.doLinkDataToControl(_TipAdr, sControlName);
		}
		/**
		 * Link the U_TipAdrId Field to the Form Item.
		 */
		public void doLink_TipAdrId(string sControlName){
			moLinker.doLinkDataToControl(_TipAdrId, sControlName);
		}
		/**
		 * Link the U_TipCIPID Field to the Form Item.
		 */
		public void doLink_TipCIPID(string sControlName){
			moLinker.doLinkDataToControl(_TipCIPID, sControlName);
		}
		/**
		 * Link the U_TipCon Field to the Form Item.
		 */
		public void doLink_TipCon(string sControlName){
			moLinker.doLinkDataToControl(_TipCon, sControlName);
		}
		/**
		 * Link the U_TipName Field to the Form Item.
		 */
		public void doLink_TipName(string sControlName){
			moLinker.doLinkDataToControl(_TipName, sControlName);
		}
		/**
		 * Link the U_TipPrcTyp Field to the Form Item.
		 */
		public void doLink_TipPrcTyp(string sControlName){
			moLinker.doLinkDataToControl(_TipPrcTyp, sControlName);
		}
		/**
		 * Link the U_TipPrice Field to the Form Item.
		 */
		public void doLink_TipPrice(string sControlName){
			moLinker.doLinkDataToControl(_TipPrice, sControlName);
		}
		/**
		 * Link the U_TipUOM Field to the Form Item.
		 */
		public void doLink_TipUOM(string sControlName){
			moLinker.doLinkDataToControl(_TipUOM, sControlName);
		}
		/**
		 * Link the U_TranCd Field to the Form Item.
		 */
		public void doLink_TranCd(string sControlName){
			moLinker.doLinkDataToControl(_TranCd, sControlName);
		}
		/**
		 * Link the U_Tue Field to the Form Item.
		 */
		public void doLink_Tue(string sControlName){
			moLinker.doLinkDataToControl(_Tue, sControlName);
		}
		/**
		 * Link the U_WastCd Field to the Form Item.
		 */
		public void doLink_WastCd(string sControlName){
			moLinker.doLinkDataToControl(_WastCd, sControlName);
		}
		/**
		 * Link the U_WastNm Field to the Form Item.
		 */
		public void doLink_WastNm(string sControlName){
			moLinker.doLinkDataToControl(_WastNm, sControlName);
		}
		/**
		 * Link the U_Wed Field to the Form Item.
		 */
		public void doLink_Wed(string sControlName){
			moLinker.doLinkDataToControl(_Wed, sControlName);
		}
		/**
		 * Link the U_WOR Field to the Form Item.
		 */
		public void doLink_WOR(string sControlName){
			moLinker.doLinkDataToControl(_WOR, sControlName);
		}
#endregion

	}
}
