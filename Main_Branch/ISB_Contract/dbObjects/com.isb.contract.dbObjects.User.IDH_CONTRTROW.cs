/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 22/01/2018 10:44:37
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.contract.dbObjects.User {
    [Serializable]
    public class IDH_CONTRTROW : com.isb.contract.dbObjects.Base.IDH_CONTRTROW {

        protected IDH_CONTRACT moParentContract;
        public IDH_CONTRACT ParentContract {
            get {
                if (moParentContract== null && U_ContractNo != null && U_ContractNo.Length > 0) {
                    moParentContract= new IDH_CONTRACT();
                    if (!moParentContract.getByKey(U_ContractNo)) {
                        moParentContract= null;
                    }

                }
                if (moParentContract == null) {
                    //DataHandler.INSTANCE.doError("The Parent Disposal Order [" + U_JobNr + "] could not be retrieved for Disposal Order Row [" + Code + "].");
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("The Parent WOQ [" + U_ContractNo + "] could not be retrieved for Contract Item [" + Code + "].",
                        "ERSYFPDO", new string[] { U_ContractNo, Code });
                }
                return moParentContract;
            }
        }

        public IDH_CONTRTROW() : base() {
            msAutoNumKey = "SEQCONTR";
        }

        public IDH_CONTRTROW(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQCONTR";
        }
        public IDH_CONTRTROW(IDH_CONTRACT oParent)
            : base() {
            moParentContract= oParent;
            msAutoNumKey = "SEQCONTR";
            //OrderByField = _Sort;
        }

    }
}
