/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 22/01/2018 07:38:14
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;
using System.Windows.Forms;

namespace com.isb.contract.dbObjects.Base {
   [Serializable]
	public class IDH_CONTRACT: com.idh.dbObjects.DBBase {

        private Linker moLinker = null;
        public Linker ControlLinker {
            get { return moLinker; }
            set { moLinker = value; }
        }

        private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

        private Control moParentControl;
        public Control ParentControl {
            get { return moParentControl; }
            set { moParentControl = value; }
        }

        private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_CONTRACT() : base("@IDH_CONTRACT"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_CONTRACT( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_CONTRACT"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_CONTRACT";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Assigned To
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AsignTo
		 * Size: 155
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AsignTo = "U_AsignTo";
		public string U_AsignTo { 
			get {
 				return getValueAsString(_AsignTo); 
			}
			set { setValue(_AsignTo, value); }
		}
           public string doValidate_AsignTo() {
               return doValidate_AsignTo(U_AsignTo);
           }
           public virtual string doValidate_AsignTo(object oValue) {
               return base.doValidation(_AsignTo, oValue);
           }

		/**
		 * Decription: Bill Address ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BAddress
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BAddress = "U_BAddress";
		public string U_BAddress { 
			get {
 				return getValueAsString(_BAddress); 
			}
			set { setValue(_BAddress, value); }
		}
           public string doValidate_BAddress() {
               return doValidate_BAddress(U_BAddress);
           }
           public virtual string doValidate_BAddress(object oValue) {
               return base.doValidation(_BAddress, oValue);
           }

		/**
		 * Decription: Bill Address LineNum
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BAddrssLN
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BAddrssLN = "U_BAddrssLN";
		public string U_BAddrssLN { 
			get {
 				return getValueAsString(_BAddrssLN); 
			}
			set { setValue(_BAddrssLN, value); }
		}
           public string doValidate_BAddrssLN() {
               return doValidate_BAddrssLN(U_BAddrssLN);
           }
           public virtual string doValidate_BAddrssLN(object oValue) {
               return base.doValidation(_BAddrssLN, oValue);
           }

		/**
		 * Decription: Bill Block
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BBlock
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BBlock = "U_BBlock";
		public string U_BBlock { 
			get {
 				return getValueAsString(_BBlock); 
			}
			set { setValue(_BBlock, value); }
		}
           public string doValidate_BBlock() {
               return doValidate_BBlock(U_BBlock);
           }
           public virtual string doValidate_BBlock(object oValue) {
               return base.doValidation(_BBlock, oValue);
           }

		/**
		 * Decription: Bill City
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BCity
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BCity = "U_BCity";
		public string U_BCity { 
			get {
 				return getValueAsString(_BCity); 
			}
			set { setValue(_BCity, value); }
		}
           public string doValidate_BCity() {
               return doValidate_BCity(U_BCity);
           }
           public virtual string doValidate_BCity(object oValue) {
               return base.doValidation(_BCity, oValue);
           }

		/**
		 * Decription: Bill Country
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BCountry
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BCountry = "U_BCountry";
		public string U_BCountry { 
			get {
 				return getValueAsString(_BCountry); 
			}
			set { setValue(_BCountry, value); }
		}
           public string doValidate_BCountry() {
               return doValidate_BCountry(U_BCountry);
           }
           public virtual string doValidate_BCountry(object oValue) {
               return base.doValidation(_BCountry, oValue);
           }

		/**
		 * Decription: Bill County
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BCounty
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BCounty = "U_BCounty";
		public string U_BCounty { 
			get {
 				return getValueAsString(_BCounty); 
			}
			set { setValue(_BCounty, value); }
		}
           public string doValidate_BCounty() {
               return doValidate_BCounty(U_BCounty);
           }
           public virtual string doValidate_BCounty(object oValue) {
               return base.doValidation(_BCounty, oValue);
           }

		/**
		 * Decription: Branch
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Branch
		 * Size: 40
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Branch = "U_Branch";
		public string U_Branch { 
			get {
 				return getValueAsString(_Branch); 
			}
			set { setValue(_Branch, value); }
		}
           public string doValidate_Branch() {
               return doValidate_Branch(U_Branch);
           }
           public virtual string doValidate_Branch(object oValue) {
               return base.doValidation(_Branch, oValue);
           }

		/**
		 * Decription: Bill State
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BState
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BState = "U_BState";
		public string U_BState { 
			get {
 				return getValueAsString(_BState); 
			}
			set { setValue(_BState, value); }
		}
           public string doValidate_BState() {
               return doValidate_BState(U_BState);
           }
           public virtual string doValidate_BState(object oValue) {
               return base.doValidation(_BState, oValue);
           }

		/**
		 * Decription: Bill Street
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BStreet
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BStreet = "U_BStreet";
		public string U_BStreet { 
			get {
 				return getValueAsString(_BStreet); 
			}
			set { setValue(_BStreet, value); }
		}
           public string doValidate_BStreet() {
               return doValidate_BStreet(U_BStreet);
           }
           public virtual string doValidate_BStreet(object oValue) {
               return base.doValidation(_BStreet, oValue);
           }

		/**
		 * Decription: Bill Post Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BZipCode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BZipCode = "U_BZipCode";
		public string U_BZipCode { 
			get {
 				return getValueAsString(_BZipCode); 
			}
			set { setValue(_BZipCode, value); }
		}
           public string doValidate_BZipCode() {
               return doValidate_BZipCode(U_BZipCode);
           }
           public virtual string doValidate_BZipCode(object oValue) {
               return base.doValidation(_BZipCode, oValue);
           }

		/**
		 * Decription: Call ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CardCd
		 * Size: 15
		 * Type: db_Alpha
		 * CType: Alpha
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _CardCd = "U_CardCd";
		public string U_CardCd { 
			get {
 				return getValueAsStringNZ(_CardCd); 
			}
			set { setValue(_CardCd, value); }
		}
           public string doValidate_CardCd() {
               return doValidate_CardCd(U_CardCd);
           }
           public virtual string doValidate_CardCd(object oValue) {
               return base.doValidation(_CardCd, oValue);
           }

        /**
		 * Decription: Service Call ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CardNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: db_Alpha
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _CardNm = "U_CardNm";
		public string U_CardNm { 
			get {
 				return getValueAsStringNZ(_CardNm); 
			}
			set { setValue(_CardNm, value); }
		}
           public string doValidate_CardNm() {
               return doValidate_CardNm(U_CardNm);
           }
           public virtual string doValidate_CardNm(object oValue) {
               return base.doValidation(_CardNm, oValue);
           }

		/**
		 * Decription: Create Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _CDate = "U_CDate";
		public DateTime U_CDate { 
			get {
 				return getValueAsDateTime(_CDate); 
			}
			set { setValue(_CDate, value); }
		}
		public void U_CDate_AsString(string value){
			setValue("U_CDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_CDate_AsString(){
			DateTime dVal = getValueAsDateTime(_CDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_CDate() {
               return doValidate_CDate(U_CDate);
           }
           public virtual string doValidate_CDate(object oValue) {
               return base.doValidation(_CDate, oValue);
           }

		/**
		 * Decription: Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CName
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CName = "U_CName";
		public string U_CName { 
			get {
 				return getValueAsString(_CName); 
			}
			set { setValue(_CName, value); }
		}
           public string doValidate_CName() {
               return doValidate_CName(U_CName);
           }
           public virtual string doValidate_CName(object oValue) {
               return base.doValidation(_CName, oValue);
           }

		/**
		 * Decription: End Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _EDate = "U_EDate";
		public DateTime U_EDate { 
			get {
 				return getValueAsDateTime(_EDate); 
			}
			set { setValue(_EDate, value); }
		}
		public void U_EDate_AsString(string value){
			setValue("U_EDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_EDate_AsString(){
			DateTime dVal = getValueAsDateTime(_EDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_EDate() {
               return doValidate_EDate(U_EDate);
           }
           public virtual string doValidate_EDate(object oValue) {
               return base.doValidation(_EDate, oValue);
           }

		/**
		 * Decription: Route
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Route
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Route = "U_Route";
		public string U_Route { 
			get {
 				return getValueAsString(_Route); 
			}
			set { setValue(_Route, value); }
		}
           public string doValidate_Route() {
               return doValidate_Route(U_Route);
           }
           public virtual string doValidate_Route(object oValue) {
               return base.doValidation(_Route, oValue);
           }

		/**
		 * Decription: Ship Address ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SAddress
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SAddress = "U_SAddress";
		public string U_SAddress { 
			get {
 				return getValueAsString(_SAddress); 
			}
			set { setValue(_SAddress, value); }
		}
           public string doValidate_SAddress() {
               return doValidate_SAddress(U_SAddress);
           }
           public virtual string doValidate_SAddress(object oValue) {
               return base.doValidation(_SAddress, oValue);
           }

		/**
		 * Decription: Ship Address LineNum
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SAddrssLN
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SAddrssLN = "U_SAddrssLN";
		public string U_SAddrssLN { 
			get {
 				return getValueAsString(_SAddrssLN); 
			}
			set { setValue(_SAddrssLN, value); }
		}
           public string doValidate_SAddrssLN() {
               return doValidate_SAddrssLN(U_SAddrssLN);
           }
           public virtual string doValidate_SAddrssLN(object oValue) {
               return base.doValidation(_SAddrssLN, oValue);
           }

		/**
		 * Decription: Ship Block
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SBlock
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SBlock = "U_SBlock";
		public string U_SBlock { 
			get {
 				return getValueAsString(_SBlock); 
			}
			set { setValue(_SBlock, value); }
		}
           public string doValidate_SBlock() {
               return doValidate_SBlock(U_SBlock);
           }
           public virtual string doValidate_SBlock(object oValue) {
               return base.doValidation(_SBlock, oValue);
           }

		/**
		 * Decription: Ship City
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SCity
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SCity = "U_SCity";
		public string U_SCity { 
			get {
 				return getValueAsString(_SCity); 
			}
			set { setValue(_SCity, value); }
		}
           public string doValidate_SCity() {
               return doValidate_SCity(U_SCity);
           }
           public virtual string doValidate_SCity(object oValue) {
               return base.doValidation(_SCity, oValue);
           }

		/**
		 * Decription: Ship Country
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SCountry
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SCountry = "U_SCountry";
		public string U_SCountry { 
			get {
 				return getValueAsString(_SCountry); 
			}
			set { setValue(_SCountry, value); }
		}
           public string doValidate_SCountry() {
               return doValidate_SCountry(U_SCountry);
           }
           public virtual string doValidate_SCountry(object oValue) {
               return base.doValidation(_SCountry, oValue);
           }

		/**
		 * Decription: Ship County
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SCounty
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SCounty = "U_SCounty";
		public string U_SCounty { 
			get {
 				return getValueAsString(_SCounty); 
			}
			set { setValue(_SCounty, value); }
		}
           public string doValidate_SCounty() {
               return doValidate_SCounty(U_SCounty);
           }
           public virtual string doValidate_SCounty(object oValue) {
               return base.doValidation(_SCounty, oValue);
           }

		/**
		 * Decription: Start Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _SDate = "U_SDate";
		public DateTime U_SDate { 
			get {
 				return getValueAsDateTime(_SDate); 
			}
			set { setValue(_SDate, value); }
		}
		public void U_SDate_AsString(string value){
			setValue("U_SDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_SDate_AsString(){
			DateTime dVal = getValueAsDateTime(_SDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_SDate() {
               return doValidate_SDate(U_SDate);
           }
           public virtual string doValidate_SDate(object oValue) {
               return base.doValidation(_SDate, oValue);
           }

		/**
		 * Decription: Ship State
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SState
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SState = "U_SState";
		public string U_SState { 
			get {
 				return getValueAsString(_SState); 
			}
			set { setValue(_SState, value); }
		}
           public string doValidate_SState() {
               return doValidate_SState(U_SState);
           }
           public virtual string doValidate_SState(object oValue) {
               return base.doValidation(_SState, oValue);
           }

		/**
		 * Decription: Ship Street
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SStreet
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SStreet = "U_SStreet";
		public string U_SStreet { 
			get {
 				return getValueAsString(_SStreet); 
			}
			set { setValue(_SStreet, value); }
		}
           public string doValidate_SStreet() {
               return doValidate_SStreet(U_SStreet);
           }
           public virtual string doValidate_SStreet(object oValue) {
               return base.doValidation(_SStreet, oValue);
           }

		/**
		 * Decription: Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Status
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Status = "U_Status";
		public int U_Status { 
			get {
 				return getValueAsInt(_Status); 
			}
			set { setValue(_Status, value); }
		}
           public string doValidate_Status() {
               return doValidate_Status(U_Status);
           }
           public virtual string doValidate_Status(object oValue) {
               return base.doValidation(_Status, oValue);
           }

		/**
		 * Decription: Ship Post Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SZipCode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SZipCode = "U_SZipCode";
		public string U_SZipCode { 
			get {
 				return getValueAsString(_SZipCode); 
			}
			set { setValue(_SZipCode, value); }
		}
           public string doValidate_SZipCode() {
               return doValidate_SZipCode(U_SZipCode);
           }
           public virtual string doValidate_SZipCode(object oValue) {
               return base.doValidation(_SZipCode, oValue);
           }

		/**
		 * Decription: Tip Address ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TAddress
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TAddress = "U_TAddress";
		public string U_TAddress { 
			get {
 				return getValueAsString(_TAddress); 
			}
			set { setValue(_TAddress, value); }
		}
           public string doValidate_TAddress() {
               return doValidate_TAddress(U_TAddress);
           }
           public virtual string doValidate_TAddress(object oValue) {
               return base.doValidation(_TAddress, oValue);
           }

		/**
		 * Decription: Tip Address LineNum
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TAddrssLN
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TAddrssLN = "U_TAddrssLN";
		public string U_TAddrssLN { 
			get {
 				return getValueAsString(_TAddrssLN); 
			}
			set { setValue(_TAddrssLN, value); }
		}
           public string doValidate_TAddrssLN() {
               return doValidate_TAddrssLN(U_TAddrssLN);
           }
           public virtual string doValidate_TAddrssLN(object oValue) {
               return base.doValidation(_TAddrssLN, oValue);
           }

		/**
		 * Decription: Tip Block
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TBlock
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TBlock = "U_TBlock";
		public string U_TBlock { 
			get {
 				return getValueAsString(_TBlock); 
			}
			set { setValue(_TBlock, value); }
		}
           public string doValidate_TBlock() {
               return doValidate_TBlock(U_TBlock);
           }
           public virtual string doValidate_TBlock(object oValue) {
               return base.doValidation(_TBlock, oValue);
           }

		/**
		 * Decription: Tip City
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TCity
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TCity = "U_TCity";
		public string U_TCity { 
			get {
 				return getValueAsString(_TCity); 
			}
			set { setValue(_TCity, value); }
		}
           public string doValidate_TCity() {
               return doValidate_TCity(U_TCity);
           }
           public virtual string doValidate_TCity(object oValue) {
               return base.doValidation(_TCity, oValue);
           }

		/**
		 * Decription: Tip Country
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TCountry
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TCountry = "U_TCountry";
		public string U_TCountry { 
			get {
 				return getValueAsString(_TCountry); 
			}
			set { setValue(_TCountry, value); }
		}
           public string doValidate_TCountry() {
               return doValidate_TCountry(U_TCountry);
           }
           public virtual string doValidate_TCountry(object oValue) {
               return base.doValidation(_TCountry, oValue);
           }

		/**
		 * Decription: Tip County
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TCounty
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TCounty = "U_TCounty";
		public string U_TCounty { 
			get {
 				return getValueAsString(_TCounty); 
			}
			set { setValue(_TCounty, value); }
		}
           public string doValidate_TCounty() {
               return doValidate_TCounty(U_TCounty);
           }
           public virtual string doValidate_TCounty(object oValue) {
               return base.doValidation(_TCounty, oValue);
           }

		/**
		 * Decription: Tip State
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TState
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TState = "U_TState";
		public string U_TState { 
			get {
 				return getValueAsString(_TState); 
			}
			set { setValue(_TState, value); }
		}
           public string doValidate_TState() {
               return doValidate_TState(U_TState);
           }
           public virtual string doValidate_TState(object oValue) {
               return base.doValidation(_TState, oValue);
           }

		/**
		 * Decription: Tip Street
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TStreet
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TStreet = "U_TStreet";
		public string U_TStreet { 
			get {
 				return getValueAsString(_TStreet); 
			}
			set { setValue(_TStreet, value); }
		}
           public string doValidate_TStreet() {
               return doValidate_TStreet(U_TStreet);
           }
           public virtual string doValidate_TStreet(object oValue) {
               return base.doValidation(_TStreet, oValue);
           }

		/**
		 * Decription: Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Type
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Type = "U_Type";
		public int U_Type { 
			get {
 				return getValueAsInt(_Type); 
			}
			set { setValue(_Type, value); }
		}
           public string doValidate_Type() {
               return doValidate_Type(U_Type);
           }
           public virtual string doValidate_Type(object oValue) {
               return base.doValidation(_Type, oValue);
           }

		/**
		 * Decription: Tip Post Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TZipCode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TZipCode = "U_TZipCode";
		public string U_TZipCode { 
			get {
 				return getValueAsString(_TZipCode); 
			}
			set { setValue(_TZipCode, value); }
		}
           public string doValidate_TZipCode() {
               return doValidate_TZipCode(U_TZipCode);
           }
           public virtual string doValidate_TZipCode(object oValue) {
               return base.doValidation(_TZipCode, oValue);
           }

		/**
		 * Decription: Created By
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_User
		 * Size: 155
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _User = "U_User";
		public string U_User { 
			get {
 				return getValueAsString(_User); 
			}
			set { setValue(_User, value); }
		}
           public string doValidate_User() {
               return doValidate_User(U_User);
           }
           public virtual string doValidate_User(object oValue) {
               return base.doValidation(_User, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(41);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_AsignTo, "Assigned To", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 155, EMPTYSTR, false, false); //Assigned To
			moDBFields.Add(_BAddress, "Bill Address ID", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Bill Address ID
			moDBFields.Add(_BAddrssLN, "Bill Address LineNum", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Bill Address LineNum
			moDBFields.Add(_BBlock, "Bill Block", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Bill Block
			moDBFields.Add(_BCity, "Bill City", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Bill City
			moDBFields.Add(_BCountry, "Bill Country", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Bill Country
			moDBFields.Add(_BCounty, "Bill County", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Bill County
			moDBFields.Add(_Branch, "Branch", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 40, EMPTYSTR, false, false); //Branch
			moDBFields.Add(_BState, "Bill State", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Bill State
			moDBFields.Add(_BStreet, "Bill Street", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Bill Street
			moDBFields.Add(_BZipCode, "Bill Post Code", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Bill Post Code
			moDBFields.Add(_CardCd, "Customer Code", 13, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Customer Code
            moDBFields.Add(_CardNm, "Customer name", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer name
            moDBFields.Add(_CDate, "Create Date", 15, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Create Date
			moDBFields.Add(_CName, "Name", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Name
			moDBFields.Add(_EDate, "End Date", 17, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //End Date
			moDBFields.Add(_Route, "Route", 18, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Route
			moDBFields.Add(_SAddress, "Ship Address ID", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Ship Address ID
			moDBFields.Add(_SAddrssLN, "Ship Address LineNum", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Ship Address LineNum
			moDBFields.Add(_SBlock, "Ship Block", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Ship Block
			moDBFields.Add(_SCity, "Ship City", 22, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Ship City
			moDBFields.Add(_SCountry, "Ship Country", 23, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Ship Country
			moDBFields.Add(_SCounty, "Ship County", 24, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Ship County
			moDBFields.Add(_SDate, "Start Date", 25, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Start Date
			moDBFields.Add(_SState, "Ship State", 26, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Ship State
			moDBFields.Add(_SStreet, "Ship Street", 27, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Ship Street
			moDBFields.Add(_Status, "Status", 28, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Status
			moDBFields.Add(_SZipCode, "Ship Post Code", 29, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Ship Post Code
			moDBFields.Add(_TAddress, "Tip Address ID", 30, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Tip Address ID
			moDBFields.Add(_TAddrssLN, "Tip Address LineNum", 31, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Tip Address LineNum
			moDBFields.Add(_TBlock, "Tip Block", 32, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Tip Block
			moDBFields.Add(_TCity, "Tip City", 33, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Tip City
			moDBFields.Add(_TCountry, "Tip Country", 34, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Tip Country
			moDBFields.Add(_TCounty, "Tip County", 35, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Tip County
			moDBFields.Add(_TState, "Tip State", 36, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Tip State
			moDBFields.Add(_TStreet, "Tip Street", 37, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Tip Street
			moDBFields.Add(_Type, "Type", 38, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Type
			moDBFields.Add(_TZipCode, "Tip Post Code", 39, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Tip Post Code
			moDBFields.Add(_User, "Created By", 40, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 155, EMPTYSTR, false, false); //Created By

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AsignTo());
            doBuildValidationString(doValidate_BAddress());
            doBuildValidationString(doValidate_BAddrssLN());
            doBuildValidationString(doValidate_BBlock());
            doBuildValidationString(doValidate_BCity());
            doBuildValidationString(doValidate_BCountry());
            doBuildValidationString(doValidate_BCounty());
            doBuildValidationString(doValidate_Branch());
            doBuildValidationString(doValidate_BState());
            doBuildValidationString(doValidate_BStreet());
            doBuildValidationString(doValidate_BZipCode());
            doBuildValidationString(doValidate_CardCd());
            doBuildValidationString(doValidate_CardNm());
            doBuildValidationString(doValidate_CDate());
            doBuildValidationString(doValidate_CName());
            doBuildValidationString(doValidate_EDate());
            doBuildValidationString(doValidate_Route());
            doBuildValidationString(doValidate_SAddress());
            doBuildValidationString(doValidate_SAddrssLN());
            doBuildValidationString(doValidate_SBlock());
            doBuildValidationString(doValidate_SCity());
            doBuildValidationString(doValidate_SCountry());
            doBuildValidationString(doValidate_SCounty());
            doBuildValidationString(doValidate_SDate());
            doBuildValidationString(doValidate_SState());
            doBuildValidationString(doValidate_SStreet());
            doBuildValidationString(doValidate_Status());
            doBuildValidationString(doValidate_SZipCode());
            doBuildValidationString(doValidate_TAddress());
            doBuildValidationString(doValidate_TAddrssLN());
            doBuildValidationString(doValidate_TBlock());
            doBuildValidationString(doValidate_TCity());
            doBuildValidationString(doValidate_TCountry());
            doBuildValidationString(doValidate_TCounty());
            doBuildValidationString(doValidate_TState());
            doBuildValidationString(doValidate_TStreet());
            doBuildValidationString(doValidate_Type());
            doBuildValidationString(doValidate_TZipCode());
            doBuildValidationString(doValidate_User());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AsignTo)) return doValidate_AsignTo(oValue);
            if (sFieldName.Equals(_BAddress)) return doValidate_BAddress(oValue);
            if (sFieldName.Equals(_BAddrssLN)) return doValidate_BAddrssLN(oValue);
            if (sFieldName.Equals(_BBlock)) return doValidate_BBlock(oValue);
            if (sFieldName.Equals(_BCity)) return doValidate_BCity(oValue);
            if (sFieldName.Equals(_BCountry)) return doValidate_BCountry(oValue);
            if (sFieldName.Equals(_BCounty)) return doValidate_BCounty(oValue);
            if (sFieldName.Equals(_Branch)) return doValidate_Branch(oValue);
            if (sFieldName.Equals(_BState)) return doValidate_BState(oValue);
            if (sFieldName.Equals(_BStreet)) return doValidate_BStreet(oValue);
            if (sFieldName.Equals(_BZipCode)) return doValidate_BZipCode(oValue);
            if (sFieldName.Equals(_CardCd)) return doValidate_CardCd(oValue);
            if (sFieldName.Equals(_CardNm)) return doValidate_CardNm(oValue);
            if (sFieldName.Equals(_CDate)) return doValidate_CDate(oValue);
            if (sFieldName.Equals(_CName)) return doValidate_CName(oValue);
            if (sFieldName.Equals(_EDate)) return doValidate_EDate(oValue);
            if (sFieldName.Equals(_Route)) return doValidate_Route(oValue);
            if (sFieldName.Equals(_SAddress)) return doValidate_SAddress(oValue);
            if (sFieldName.Equals(_SAddrssLN)) return doValidate_SAddrssLN(oValue);
            if (sFieldName.Equals(_SBlock)) return doValidate_SBlock(oValue);
            if (sFieldName.Equals(_SCity)) return doValidate_SCity(oValue);
            if (sFieldName.Equals(_SCountry)) return doValidate_SCountry(oValue);
            if (sFieldName.Equals(_SCounty)) return doValidate_SCounty(oValue);
            if (sFieldName.Equals(_SDate)) return doValidate_SDate(oValue);
            if (sFieldName.Equals(_SState)) return doValidate_SState(oValue);
            if (sFieldName.Equals(_SStreet)) return doValidate_SStreet(oValue);
            if (sFieldName.Equals(_Status)) return doValidate_Status(oValue);
            if (sFieldName.Equals(_SZipCode)) return doValidate_SZipCode(oValue);
            if (sFieldName.Equals(_TAddress)) return doValidate_TAddress(oValue);
            if (sFieldName.Equals(_TAddrssLN)) return doValidate_TAddrssLN(oValue);
            if (sFieldName.Equals(_TBlock)) return doValidate_TBlock(oValue);
            if (sFieldName.Equals(_TCity)) return doValidate_TCity(oValue);
            if (sFieldName.Equals(_TCountry)) return doValidate_TCountry(oValue);
            if (sFieldName.Equals(_TCounty)) return doValidate_TCounty(oValue);
            if (sFieldName.Equals(_TState)) return doValidate_TState(oValue);
            if (sFieldName.Equals(_TStreet)) return doValidate_TStreet(oValue);
            if (sFieldName.Equals(_Type)) return doValidate_Type(oValue);
            if (sFieldName.Equals(_TZipCode)) return doValidate_TZipCode(oValue);
            if (sFieldName.Equals(_User)) return doValidate_User(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_AsignTo Field to the Form Item.
		 */
		public void doLink_AsignTo(string sControlName){
			moLinker.doLinkDataToControl(_AsignTo, sControlName);
		}
		/**
		 * Link the U_BAddress Field to the Form Item.
		 */
		public void doLink_BAddress(string sControlName){
			moLinker.doLinkDataToControl(_BAddress, sControlName);
		}
		/**
		 * Link the U_BAddrssLN Field to the Form Item.
		 */
		public void doLink_BAddrssLN(string sControlName){
			moLinker.doLinkDataToControl(_BAddrssLN, sControlName);
		}
		/**
		 * Link the U_BBlock Field to the Form Item.
		 */
		public void doLink_BBlock(string sControlName){
			moLinker.doLinkDataToControl(_BBlock, sControlName);
		}
		/**
		 * Link the U_BCity Field to the Form Item.
		 */
		public void doLink_BCity(string sControlName){
			moLinker.doLinkDataToControl(_BCity, sControlName);
		}
		/**
		 * Link the U_BCountry Field to the Form Item.
		 */
		public void doLink_BCountry(string sControlName){
			moLinker.doLinkDataToControl(_BCountry, sControlName);
		}
		/**
		 * Link the U_BCounty Field to the Form Item.
		 */
		public void doLink_BCounty(string sControlName){
			moLinker.doLinkDataToControl(_BCounty, sControlName);
		}
		/**
		 * Link the U_Branch Field to the Form Item.
		 */
		public void doLink_Branch(string sControlName){
			moLinker.doLinkDataToControl(_Branch, sControlName);
		}
		/**
		 * Link the U_BState Field to the Form Item.
		 */
		public void doLink_BState(string sControlName){
			moLinker.doLinkDataToControl(_BState, sControlName);
		}
		/**
		 * Link the U_BStreet Field to the Form Item.
		 */
		public void doLink_BStreet(string sControlName){
			moLinker.doLinkDataToControl(_BStreet, sControlName);
		}
		/**
		 * Link the U_BZipCode Field to the Form Item.
		 */
		public void doLink_BZipCode(string sControlName){
			moLinker.doLinkDataToControl(_BZipCode, sControlName);
		}
		/**
		 * Link the U_CardCd Field to the Form Item.
		 */
		public void doLink_CardCd(string sControlName){
			moLinker.doLinkDataToControl(_CardCd, sControlName);
		}
		/**
		 * Link the U_CardNm Field to the Form Item.
		 */
		public void doLink_CardNm(string sControlName){
			moLinker.doLinkDataToControl(_CardNm, sControlName);
		}
		/**
		 * Link the U_CDate Field to the Form Item.
		 */
		public void doLink_CDate(string sControlName){
			moLinker.doLinkDataToControl(_CDate, sControlName);
		}
		/**
		 * Link the U_CName Field to the Form Item.
		 */
		public void doLink_CName(string sControlName){
			moLinker.doLinkDataToControl(_CName, sControlName);
		}
		/**
		 * Link the U_EDate Field to the Form Item.
		 */
		public void doLink_EDate(string sControlName){
			moLinker.doLinkDataToControl(_EDate, sControlName);
		}
		/**
		 * Link the U_Route Field to the Form Item.
		 */
		public void doLink_Route(string sControlName){
			moLinker.doLinkDataToControl(_Route, sControlName);
		}
		/**
		 * Link the U_SAddress Field to the Form Item.
		 */
		public void doLink_SAddress(string sControlName){
			moLinker.doLinkDataToControl(_SAddress, sControlName);
		}
		/**
		 * Link the U_SAddrssLN Field to the Form Item.
		 */
		public void doLink_SAddrssLN(string sControlName){
			moLinker.doLinkDataToControl(_SAddrssLN, sControlName);
		}
		/**
		 * Link the U_SBlock Field to the Form Item.
		 */
		public void doLink_SBlock(string sControlName){
			moLinker.doLinkDataToControl(_SBlock, sControlName);
		}
		/**
		 * Link the U_SCity Field to the Form Item.
		 */
		public void doLink_SCity(string sControlName){
			moLinker.doLinkDataToControl(_SCity, sControlName);
		}
		/**
		 * Link the U_SCountry Field to the Form Item.
		 */
		public void doLink_SCountry(string sControlName){
			moLinker.doLinkDataToControl(_SCountry, sControlName);
		}
		/**
		 * Link the U_SCounty Field to the Form Item.
		 */
		public void doLink_SCounty(string sControlName){
			moLinker.doLinkDataToControl(_SCounty, sControlName);
		}
		/**
		 * Link the U_SDate Field to the Form Item.
		 */
		public void doLink_SDate(string sControlName){
			moLinker.doLinkDataToControl(_SDate, sControlName);
		}
		/**
		 * Link the U_SState Field to the Form Item.
		 */
		public void doLink_SState(string sControlName){
			moLinker.doLinkDataToControl(_SState, sControlName);
		}
		/**
		 * Link the U_SStreet Field to the Form Item.
		 */
		public void doLink_SStreet(string sControlName){
			moLinker.doLinkDataToControl(_SStreet, sControlName);
		}
		/**
		 * Link the U_Status Field to the Form Item.
		 */
		public void doLink_Status(string sControlName){
			moLinker.doLinkDataToControl(_Status, sControlName);
		}
		/**
		 * Link the U_SZipCode Field to the Form Item.
		 */
		public void doLink_SZipCode(string sControlName){
			moLinker.doLinkDataToControl(_SZipCode, sControlName);
		}
		/**
		 * Link the U_TAddress Field to the Form Item.
		 */
		public void doLink_TAddress(string sControlName){
			moLinker.doLinkDataToControl(_TAddress, sControlName);
		}
		/**
		 * Link the U_TAddrssLN Field to the Form Item.
		 */
		public void doLink_TAddrssLN(string sControlName){
			moLinker.doLinkDataToControl(_TAddrssLN, sControlName);
		}
		/**
		 * Link the U_TBlock Field to the Form Item.
		 */
		public void doLink_TBlock(string sControlName){
			moLinker.doLinkDataToControl(_TBlock, sControlName);
		}
		/**
		 * Link the U_TCity Field to the Form Item.
		 */
		public void doLink_TCity(string sControlName){
			moLinker.doLinkDataToControl(_TCity, sControlName);
		}
		/**
		 * Link the U_TCountry Field to the Form Item.
		 */
		public void doLink_TCountry(string sControlName){
			moLinker.doLinkDataToControl(_TCountry, sControlName);
		}
		/**
		 * Link the U_TCounty Field to the Form Item.
		 */
		public void doLink_TCounty(string sControlName){
			moLinker.doLinkDataToControl(_TCounty, sControlName);
		}
		/**
		 * Link the U_TState Field to the Form Item.
		 */
		public void doLink_TState(string sControlName){
			moLinker.doLinkDataToControl(_TState, sControlName);
		}
		/**
		 * Link the U_TStreet Field to the Form Item.
		 */
		public void doLink_TStreet(string sControlName){
			moLinker.doLinkDataToControl(_TStreet, sControlName);
		}
		/**
		 * Link the U_Type Field to the Form Item.
		 */
		public void doLink_Type(string sControlName){
			moLinker.doLinkDataToControl(_Type, sControlName);
		}
		/**
		 * Link the U_TZipCode Field to the Form Item.
		 */
		public void doLink_TZipCode(string sControlName){
			moLinker.doLinkDataToControl(_TZipCode, sControlName);
		}
		/**
		 * Link the U_User Field to the Form Item.
		 */
		public void doLink_User(string sControlName){
			moLinker.doLinkDataToControl(_User, sControlName);
		}
#endregion

	}
}
