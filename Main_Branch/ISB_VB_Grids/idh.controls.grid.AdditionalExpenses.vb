Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.controls
Imports com.idh.controls.strct
Imports com.idh.bridge
Imports com.idh.bridge.data
Imports com.idh.bridge.lookups
Imports System
Imports com.idh.dbObjects.User
Imports com.idh.dbObjects.numbers
'
' Created by SharpDevelop.
' User: Louis Viljoen
' Date: 20/05/2008
' Time: 09:08
'

Namespace idh.controls.grid
	Public Class AdditionalExpenses
		Inherits IDHAddOns.idh.controls.UpdateGrid
        Private moDataHandler As WR1_Data.idh.data.AdditionalExpenses = Nothing
		Private mdDocDate As Date
        Private msWR1JobType As String = "WO"
        Private mbDoVat As Boolean = True

        Private msAutoNum As String = "ADDEXPKEY"
        Private msAutoNumPrefix As String = Nothing

        Private miBlockChangeTrigger As Integer = 0
        Public Property BlockChangeTrigger As Boolean
            Set(ByVal value As Boolean)
                If value = True Then
                    miBlockChangeTrigger = miBlockChangeTrigger + 1
                Else
                    miBlockChangeTrigger = miBlockChangeTrigger - 1
                End If
            End Set
            Get
                Return miBlockChangeTrigger > 0
            End Get
        End Property

        'Public

        Public Property DoVat() As Boolean
            Get
                Return mbDoVat
            End Get
            Set(ByVal value As Boolean)
                mbDoVat = value
                doCalculateLinesTotals()
            End Set
        End Property
		
		Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sGridID As String)
            MyBase.New(oIDHForm, oForm, sGridID)
        End Sub

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sGridID As String, _
                    ByVal iLeft As Integer, _
                    ByVal iTop As Integer, _
                    ByVal iWidth As Integer, _
                    ByVal iHeight As Integer)
            MyBase.New(oIDHForm, oForm, sGridID, iLeft, iTop, iWidth, iHeight)
        End Sub
		
        Public Sub doSetDataHandler(ByRef oDHandler As WR1_Data.idh.data.AdditionalExpenses)
            moDataHandler = oDHandler
        End Sub
		
		'** Use this if the item was already added to the Form
		Public Shared Function doAddToForm( _
				ByVal oIDHForm As IDHAddOns.idh.forms.Base, _
				ByVal oForm As SAPbouiCOM.Form, _
				ByVal sGridID As String, _
				ByVal iLeft As Integer, _
				ByVal iTop As Integer, _
				ByVal iWidth As Integer, _
				ByVal iHeight As Integer, _
				ByVal iFromPane As Integer, _
				ByVal iToPane As Integer) As UpdateGrid
		
			Dim oGridN As AdditionalExpenses = New AdditionalExpenses(oIDHForm, oForm, sGridID, iLeft, iTop, iWidth, iHeight)
            oGridN.getSBOItem.FromPane = iFromPane
            oGridN.getSBOItem.ToPane = iToPane
            oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
            oGridN.getItem.AffectsFormMode = False
			
			Return oGridN
		End Function
		
        Public Function doSetGridParameters( _
          ByVal sWR1JobType As String, _
          ByVal sJobCode As String, _
          ByVal sJobRow As String, _
          ByVal bCanEdit As Boolean, _
          ByVal dDocDate As Date, _
          ByVal bSetListFields As Boolean) As Boolean
            Return doSetGridParameters(sWR1JobType, sJobCode, sJobRow, bCanEdit, dDocDate, True, bSetListFields)
        End Function
         

        Public Overrides Sub doAddDataTable(ByVal sDataSrc As String)
            MyBase.doAddDataTable(sDataSrc)
            If sDataSrc = IDH_WOADDEXP.TableName Then
                msAutoNumPrefix = IDH_WOADDEXP.AUTONUMPREFIX
            ElseIf sDataSrc = IDH_DOADDEXP.TableName Then
                msAutoNumPrefix = IDH_DOADDEXP.AUTONUMPREFIX
            End If
        End Sub

		''** Here is where the Grid options get set
		'' WR1JobType can be set to WO or DO 
        Public Function doSetGridParameters( _
          ByVal sWR1JobType As String, _
          ByVal sJobCode As String, _
          ByVal sJobRow As String, _
          ByVal bCanEdit As Boolean, _
          ByVal dDocDate As Date, _
          ByVal bSetTable As Boolean, _
          ByVal bSetListFields As Boolean) As Boolean

            msWR1JobType = sWR1JobType

            'Dim sTable As String = "[@IDH_" & sWR1JobType & "ADDEXP]"
            'setTableValue(sTable)

            msKeyField = "Code"
            If bSetTable Then
                doAddGridTable(New GridTable("@IDH_" & sWR1JobType & "ADDEXP", Nothing, "Code", True, True), True)
            End If

            getItem.AffectsFormMode = False
            setOrderValue("Code")
            doSetDoCount(True)

            mdDocDate = dDocDate
            If mdDocDate = Nothing Then
                mdDocDate = Date.Now()
            End If

            'setRequiredFilter("U_JobNr = '" & sJobCode & "' AND U_RowNr = '" & sJobRow & "'")
            setRequiredFilter("U_RowNr = '" & sJobRow & "'")

            Dim sAddGrp As String = Config.INSTANCE.doGetAdditionalExpGroup()
            Dim sNotGroup As String = Config.ParameterWithDefault("IGNWSTGP", "")

            ''Set the grid list fields
            ''MA Start 27-01-2015
            If bSetListFields Then
                doAddListField("Code", "Code", False, 5, "IGNORE", Nothing)
                doAddListField("Name", "Name", False, 0, "IGNORE", Nothing)
                doAddListField("U_JobNr", "Order Number", False, 0, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_UserDefinedObject)
                doAddListField("U_RowNr", "Row Order Number", False, 0, "IGNORE", Nothing)

                'changed to avoid auto popup of Supplier search on change focus
                'instead this will work on TAB ONLY
                doAddListField("U_ItemCd", "Item Code", bCanEdit, -1, "SRC:IDHISRC(ADD)[IDH_ITMCOD;IDH_GRPCOD=" & sAddGrp & ";NOT_GRPCOD=" & sNotGroup & "][ITEMCODE;U_ItemDsc=ITEMNAME;U_UOM=SUOM]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
                'un-done the change because of the key press event doesnot take place
                'doAddListField("U_ItemCd", "Item Code", bCanEdit, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)

                doAddListField("U_ItemDsc", "Item Description", bCanEdit, -1, "SRC:IDHISRC(ADD)[IDH_ITMNAM;IDH_GRPCOD=" & sAddGrp & ";NOT_GRPCOD=" & sNotGroup & "][ITEMNAME;U_ItemCd=ITEMCODE;U_UOM=SUOM]", Nothing) '"IGNORE"

                'OnTime [#Ico000????] USA 
                'START
                doAddListField("U_EmpId", "Employee ID", bCanEdit, -1, "SRC:IDHEMPSCR(EMP)[IDH_EMPID][EMPID;U_EmpNm=FULLNM]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Employee)
                doAddListField("U_EmpNm", "Employee Name", False, -1, "IGNORE", Nothing)
                'END 
                'OnTime [#Ico000????] USA 

                'moGrid.doAddListField("U_CustCd", "Customer Code", False, -1, "SRC*IDHCSRCH(CUST)[IDH_BPCOD;IDH_TYPE=%F-C][CARDCODE]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
                doAddListField("U_CustCd", "Customer Code", False, 0, "IGNORE", Nothing)
                doAddListField("U_CustNm", "Customer Name", False, 0, "IGNORE", Nothing)
                doAddListField("U_CustCurr", "Customer Currency", False, 0, "IGNORE", Nothing)
                'changed to avoid auto popup of Supplier search on change focus
                'instead this will work on TAB ONLY
                doAddListField("U_SuppCd", "Supplier Code", bCanEdit, -1, "SRC*IDHCSRCH(SUP)[IDH_BPCOD;IDH_TYPE=SC][CARDCODE;U_SuppNm=CARDNAME;U_SuppCurr=CURRENCY]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
                'doAddListField("U_SuppCd", "Supplier Code", bCanEdit, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
                doAddListField("U_SuppNm", "Supplier Name", bCanEdit, -1, "SRC*IDHCSRCH(SUP)[IDH_NAME;IDH_TYPE=SC][CARDNAME;U_SuppCd=CARDCODE;U_SuppCurr=CURRENCY]", Nothing)

                'New column to display currency symbol of the supplier 
                doAddListField("U_SuppCurr", "Currency", False, -1, "IGNORE", Nothing)

                doAddListField("U_UOM", "Unit Of Measure", bCanEdit, -1, "COMBOBOX", Nothing)
                doAddListField("U_Quantity", "Order Quantity", bCanEdit, -1, "IGNORE", Nothing)

                doAddListField(IDH_WOADDEXP._ChrgCal, "Charge Calc Type", False, 0, "IGNORE", Nothing)
                doAddListField(IDH_WOADDEXP._CstCal, "Cost Calc Type", False, 0, "IGNORE", Nothing)

                ''Switch As Per Rows Start
                ''MA Start 30-10-2014 Issue#417 Issue#418
                Dim iWidth_Cost As Integer = 0, iWidth_Charge As Integer = 0
                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) Then
                    iWidth_Cost = -1
                End If

                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
                    iWidth_Charge = -1
                End If

                '** 20150226 - LPV -  BEGIN - Make sure this gets disabled when the grid is disabled Ie
                Dim bPCanEdit_Cost As Boolean = bCanEdit
                Dim bPCanEdit_Charge As Boolean = bCanEdit
                If bCanEdit Then
                    bPCanEdit_Cost = Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Cost_PRICES)
                    bPCanEdit_Charge = Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Charge_PRICES)
                End If
                doAddListField("U_ItmCost", "Cost", bPCanEdit_Cost, iWidth_Cost, "IGNORE", Nothing)
                doAddListField("U_ItmChrg", "Charge", bPCanEdit_Charge, iWidth_Charge, "IGNORE", Nothing)
                '** 20150226 - LPV -  START - Make sure this gets disabled when the grid is disabled

                doAddListField("U_ItmTCost", "Line Cost", False, iWidth_Cost, "IGNORE", Nothing)
                doAddListField("U_ItmTChrg", "Line Charge", False, iWidth_Charge, "IGNORE", Nothing)
                doAddListField("U_SONr", "Sales Order", False, -1, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Order)
                doAddListField("U_PONr", "Purchase Order", False, -1, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder)

                doAddListField("U_CostVatGrp", "Purchase Vat Group", False, iWidth_Cost, "COMBOBOX", Nothing)
                doAddListField("U_ItmCostVat", "Purchase Vat", False, iWidth_Cost, "IGNORE", Nothing)
                doAddListField("U_ChrgVatGrp", "Selling Vat Group", False, iWidth_Charge, "COMBOBOX", Nothing)
                doAddListField("U_ItmChrgVat", "Selling Vat", False, iWidth_Charge, "IGNORE", Nothing)
                'doAddListField("U_ItmTCost", "Line Cost", False, iWidth, "IGNORE", Nothing)
                'doAddListField("U_ItmTChrg", "Line Charge", False, iWidth, "IGNORE", Nothing)
                ''Switch As Per Rows End

                'doAddListField("U_SONr", "Sales Order", False, -1, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Order)
                'doAddListField("U_PONr", "Purchase Order", False, -1, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder)
                doAddListField("U_SINVNr", "AR Invoice", False, -1, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Invoice)
                doAddListField("U_PINVNr", "AP Invoice", False, -1, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseInvoice)
                doAddListField("U_FPCost", "Freeze Cost Price", bCanEdit, -1, "CHECKBOX", Nothing, -1)
                doAddListField("U_FPChrg", "Freeze Charge Price", bCanEdit, -1, "CHECKBOX", Nothing, -1)
                doAddListField("U_AutoAdd", "Auto Added", False, -1, "CHECKBOX", Nothing, -1)
                doAddListField("U_BPAutoAdd", "BP Auto Added", False, -1, "CHECKBOX", Nothing, -1)
            End If
            ''MA End 30-10-2014 Issue#417 Issue#418
            ''MA End 27-01-2015
            Return True
        End Function
        Public Function doUpdateColumns(ByVal bCanEdit As Boolean) As Boolean

            doEnableColumn("U_ItemCd", bCanEdit)
            doEnableColumn("U_ItemDsc", bCanEdit)

            doEnableColumn("U_EmpId", bCanEdit) ' "Employee ID", bCanEdit, -1, "SRC:IDHEMPSCR(EMP)[IDH_EMPID][EMPID;U_EmpNm=FULLNM]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Employee)
            doEnableColumn("U_SuppCd", bCanEdit) '"Supplier Code", bCanEdit, -1, "SRC*IDHCSRCH(SUP)[IDH_BPCOD;IDH_TYPE=SC][CARDCODE;U_SuppNm=CARDNAME;U_SuppCurr=CURRENCY]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            doEnableColumn("U_SuppNm", bCanEdit) '"Supplier Name", bCanEdit, -1, "IGNORE", Nothing)

            doEnableColumn("U_UOM", bCanEdit) ' "Unit Of Measure", bCanEdit, -1, "COMBOBOX", Nothing)
            doEnableColumn("U_Quantity", bCanEdit) ' "Order Quantity", bCanEdit, -1, "IGNORE", Nothing)

            Dim iWidth_Cost As Integer = 0, iWidth_Charge As Integer = 0
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) Then
                iWidth_Cost = -1
            End If

            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
                iWidth_Charge = -1
            End If
            Dim bPCanEdit_Cost As Boolean = Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Cost_PRICES)
            Dim bPCanEdit_Charge As Boolean = Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Charge_PRICES)
            doSetListFieldWidth("U_ItmCost", iWidth_Cost)
            doEnableColumn("U_ItmCost", bPCanEdit_Cost)
            'doAddListField("U_ItmCost", "Cost", bPCanEdit_Cost, iWidth_Cost, "IGNORE", Nothing)

            doSetListFieldWidth("U_ItmChrg", iWidth_Charge)
            doEnableColumn("U_ItmChrg", bPCanEdit_Charge)
            'doAddListField("U_ItmChrg", "Charge", bPCanEdit_Charge, iWidth_Charge, "IGNORE", Nothing)

            doSetListFieldWidth("U_ItmTCost", iWidth_Cost)
            'doAddListField("U_ItmTCost", "Line Cost", False, iWidth_Cost, "IGNORE", Nothing)

            doSetListFieldWidth("U_ItmTChrg", iWidth_Charge)
            'doAddListField("U_ItmTChrg", "Line Charge", False, iWidth_Charge, "IGNORE", Nothing)

            doSetListFieldWidth("U_CostVatGrp", iWidth_Cost)
            'doAddListField("U_CostVatGrp", "Purchase Vat Group", False, iWidth_Cost, "COMBOBOX", Nothing)

            doSetListFieldWidth("U_ItmCostVat", iWidth_Cost)
            'doAddListField("U_ItmCostVat", "Purchase Vat", False, iWidth_Cost, "IGNORE", Nothing)

            doSetListFieldWidth("U_ChrgVatGrp", iWidth_Charge)
            'doAddListField("U_ChrgVatGrp", "Selling Vat Group", False, iWidth_Charge, "COMBOBOX", Nothing)

            doSetListFieldWidth("U_ItmChrgVat", iWidth_Charge)
            'doAddListField("U_ItmChrgVat", "Selling Vat", False, iWidth_Charge, "IGNORE", Nothing)

            doEnableColumn("U_FPCost", bCanEdit) ', "Freeze Cost Price", bCanEdit, -1, "CHECKBOX", Nothing, -1)
            Dim i As Int32 = doFieldIndex("U_FPChrg")
            doEnableColumn(i, bCanEdit) ' "Freeze Charge Price", bCanEdit, -1, "CHECKBOX", Nothing, -1)
            doEnableColumn("U_FPChrg", bCanEdit) ' "Freeze Charge Price", bCanEdit, -1, "CHECKBOX", Nothing, -1)

            Return True
        End Function

        Public Function doIndexOfItem(ByVal sAddItemCd As String) As Integer
            Dim iRow As Integer = 0
            For iRow = 0 To getRowCount() - 1
                If doGetFieldValue("U_ItemCd", iRow).Equals(sAddItemCd) Then
                    Return iRow
                End If
            Next
            Return -1
        End Function

        Public Sub doAddExpenseDoPriceLookup( _
            ByVal sJobNr As String, _
            ByVal sRowNr As String, _
            ByVal sCustCd As String, _
            ByVal sCustNm As String, _
            ByVal sCustCurr As String, _
            ByVal sSuppCd As String, _
            ByVal sSuppNm As String, _
            ByVal sSuppCurr As String, _
            ByVal sEmpId As String, _
            ByVal sEmpNm As String, _
            ByVal sContainerCode As String, _
            ByVal sContainerGrp As String, _
            ByVal sAddItemCd As String, _
            ByVal sAddItemNm As String, _
            ByVal sWastCd As String, _
            ByVal sAddress As String, _
            ByVal sBranch As String, _
            ByVal sJobType As String, _
            ByVal dDocDate As Date, _
            ByVal sZipCode As String, _
            ByVal sUOM As String, _
            ByVal nQuantity As Double, _
            ByVal bCostFroozen As Boolean, _
            ByVal bChrgFroozen As Boolean)

            Dim dCharge As Double = 0
            Dim sChrgVatGrp As String = ""
            Dim sChrgCalc As String = "VARIABLE"
            Dim sCostCalc As String = "VARIABLE"
            'Dim aPrice As ArrayList
            'aPrices = Config.INSTANCE.doGetAdditionalChargePrices(getIDHForm().goParent, "Waste Order", sJobType, sContainerGrp, sContainerCode, sCustCd, nQuantity, sUOM, sWastCd, sAddress, dDocDate, sBranch, sAddItemCd, sZipCode)
            'If Not aPrices Is Nothing AndAlso aPrices.Count > 0 Then
            ' 	dCharge = aPrices(0)
            '	sChrgVatGrp = aPrices(2)
            'End If            

            Dim oPrice As com.idh.dbObjects.User.AdditionalPrice
            oPrice = Config.INSTANCE.doGetAdditionalChargePrices(msWR1JobType, sJobType, sContainerGrp, sContainerCode, sCustCd, nQuantity, sUOM, sWastCd, sAddress, dDocDate, sBranch, sAddItemCd, sZipCode)
            If Not oPrice Is Nothing Then
                dCharge = oPrice.mdPrice
                sChrgVatGrp = oPrice.msVatGroup
                sChrgCalc = oPrice.msCalcType
            End If

            Dim dCost As Double = 0
            Dim sCostVatGrp As String = ""
            oPrice = Config.INSTANCE.doGetAdditionalCostPrices(msWR1JobType, sJobType, sContainerGrp, sContainerCode, sSuppCd, nQuantity, sUOM, sWastCd, sAddress, dDocDate, sBranch, sAddItemCd, sZipCode)
            If Not oPrice Is Nothing Then
                dCost = oPrice.mdPrice
                sCostVatGrp = oPrice.msVatGroup
                sCostCalc = oPrice.msCalcType
            End If

            doAddExpense(sJobNr, sRowNr, sCustCd, sCustNm, sCustCurr, sSuppCd, sSuppNm, sSuppCurr, sEmpId, sEmpNm, sAddItemCd, sAddItemNm, sUOM, nQuantity, _
                 dCost, dCharge, sCostVatGrp, sChrgVatGrp, bCostFroozen, bChrgFroozen, True, False, sChrgCalc, sCostCalc)
        End Sub

        Public Sub doAddExpense( _
            ByVal sJobNr As String, _
            ByVal sRowNr As String, _
            ByVal sCustCd As String, _
            ByVal sCustNm As String, _
            ByVal sCustCurr As String, _
            ByVal sSuppCd As String, _
            ByVal sSuppNm As String, _
            ByVal sSuppCurr As String, _
            ByVal sEmpId As String, _
            ByVal sEmpNm As String, _
            ByVal sItemCd As String, _
            ByVal sItemNm As String, _
            ByVal sUOM As String, _
            ByVal nQuantity As Double, _
            ByVal nItmCost As Double, _
            ByVal nItmChrg As Double, _
            ByVal sCostVatGrp As String, _
            ByVal sChrgVatGrp As String, _
            ByVal bCostFroozen As Boolean, _
            ByVal bChrgFroozen As Boolean, _
            ByVal bAutoAdd As Boolean, _
            ByVal bBPAutoAdd As Boolean, _
            ByVal sChrgCalc As String, _
            ByVal sCostCalc As String)

            'comments to remove
            doAddNewLine()
            miCurrentDataRow = getLastRowIndex()
            'Dim sKey As String = DataHandler.doGenerateCode(msAutoNumPrefix, msAutoNum) 'com.idh.utils.Conversions.ToString(moIDHForm.goParent.goDB.doNextNumber("ADDEXPKEY"))
            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(msAutoNumPrefix, msAutoNum)
            doSetFieldValue("Code", oNumbers.CodeCode)
            doSetFieldValue("Name", oNumbers.NameCode)

            doSetFieldValue("U_JobNr", sJobNr)
            doSetFieldValue("U_RowNr", sRowNr)
            doSetFieldValue("U_CustCd", sCustCd)
            doSetFieldValue("U_CustNm", sCustNm)
            doSetFieldValue("U_CustCurr", sCustCurr)
            doSetFieldValue("U_SuppCd", sSuppCd)
            doSetFieldValue("U_SuppNm", sSuppNm)
            doSetFieldValue("U_SuppCurr", sSuppCurr)
            doSetFieldValue("U_EmpId", sEmpId)
            doSetFieldValue("U_EmpNm", sEmpNm)
            doSetFieldValue("U_ItemCd", sItemCd)
            doSetFieldValue("U_ItemDsc", sItemNm)
            doSetFieldValue("U_UOM", sUOM)
            doSetFieldValue("U_Quantity", nQuantity)
            doSetFieldValue("U_ItmCost", nItmCost)
            doSetFieldValue("U_ItmChrg", nItmChrg)
            doSetFieldValue("U_CostVatGrp", sCostVatGrp)
            doSetFieldValue("U_ChrgVatGrp", sChrgVatGrp)
            doSetFieldValue(IDH_WOADDEXP._ChrgCal, sChrgCalc)
            doSetFieldValue(IDH_WOADDEXP._CstCal, sCostCalc)
            If bCostFroozen Then
                doSetFieldValue("U_FPCost", "Y")
            Else
                doSetFieldValue("U_FPCost", "N")
            End If

            If bChrgFroozen Then
                doSetFieldValue("U_FPChrg", "Y")
            Else
                doSetFieldValue("U_FPChrg", "N")
            End If

            If bAutoAdd Then
                doSetFieldValue("U_AutoAdd", "Y")
            Else
                doSetFieldValue("U_AutoAdd", "N")
            End If

            If bBPAutoAdd Then
                doSetFieldValue("U_BPAutoAdd", "Y")
            Else
                doSetFieldValue("U_BPAutoAdd", "N")
            End If

            'doCalculateChargeLineTotals(getCurrentLine())
            doCalculateLineTotals(getCurrentDataRowIndex())
            doAddEditLine()
        End Sub

        Public Sub doUpdateExpense( _
          ByVal sCurrentCode As String, _
          ByVal sCustCd As String, _
          ByVal sCustNm As String, _
          ByVal sSuppCd As String, _
          ByVal sSuppNm As String, _
          ByVal sItemCd As String, _
          ByVal sItemNm As String _
         )

            Dim sCCode As String
            Dim iRow As Integer
            For iRow = 0 To getRowCount() - 1
                sCCode = CType(doGetFieldValue("U_ItemCd", iRow), String)
                If sCCode = sCurrentCode Then
                    If Not sCustCd Is Nothing AndAlso sCustCd.Length > 0 Then
                        doSetFieldValue("U_CustCd", iRow, sCustCd)
                    End If
                    If Not sCustNm Is Nothing AndAlso sCustNm.Length > 0 Then
                        doSetFieldValue("U_CustNm", iRow, sCustNm)
                    End If
                    If Not sSuppCd Is Nothing AndAlso sSuppCd.Length > 0 Then
                        doSetFieldValue("U_SuppCd", iRow, sSuppCd)
                    End If
                    If Not sSuppNm Is Nothing AndAlso sSuppNm.Length > 0 Then
                        doSetFieldValue("U_SuppNm", iRow, sSuppNm)
                    End If
                    If Not sItemCd Is Nothing AndAlso sItemCd.Length > 0 Then
                        doSetFieldValue("U_ItemCd", iRow, sItemCd)
                    End If
                    If Not sItemNm Is Nothing AndAlso sItemNm.Length > 0 Then
                        doSetFieldValue("U_ItemDsc", iRow, sItemNm)
                    End If
                    Exit Sub
                End If
            Next
        End Sub

        Public Sub doSetRowCode(ByVal sJobCode As String, ByVal sRowCode As String)
            Dim iRow As Integer
            For iRow = 0 To getRowCount() - 1
                doSetFieldValue(IDH_WOADDEXP._JobNr, iRow, sJobCode)
                doSetFieldValue(IDH_WOADDEXP._RowNr, iRow, sRowCode)
            Next
        End Sub

        Public Sub doUpdateAutoAddChargeCalculations(ByVal sBPCode As String, ByVal sJobType As String, ByVal dWORCharge As Double)
            Dim iRow As Integer
            For iRow = 0 To getRowCount() - 1
                Dim isAutoAdd As String = doGetFieldValue("U_AutoAdd", iRow)
                If isAutoAdd = "Y" Then
                    doSetFieldValue("U_ItmChrg", iRow, 100.0)
                End If
            Next
        End Sub

        Protected Overrides Function doSendGridEvent(ByVal iEventType As IDHAddOns.idh.events.Base.ev_Types, ByVal bBefore As Boolean, ByVal bActSuccess As Boolean, Optional ByVal sColID As String = Nothing, Optional ByVal iRow As Integer = 0, Optional ByRef oData As Object = Nothing) As Boolean
            If iEventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
                'If bBefore Then
                'Dim sKey As String = DataHandler.doGenerateCode(msAutoNumPrefix, msAutoNum) 'com.idh.utils.Conversions.ToString(getIDHForm.goParent.goDB.doNextNumber("ADDEXPKEY"))
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(msAutoNumPrefix, msAutoNum)
                doSetFieldValue("Code", iRow, oNumbers.CodeCode, False)
                doSetFieldValue("Name", iRow, oNumbers.CodeNumber, True)
                'End If
                '                Return True
                '			Else
                '				Return MyBase.doSendGridEvent(iEventType, bBefore, bActSuccess, sColID, iRow, oData)
            ElseIf iEventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                If bBefore = True Then
                    Dim oSelected As SAPbouiCOM.SelectedRows = getGrid().Rows.SelectedRows
                    Dim iSelectionCount As Integer = oSelected.Count

                    'If oGridN.getGrid().Rows.SelectedRows.Count > 0 Then
                    If iSelectionCount = 1 Then
                        Dim iiRow As Integer
                        iiRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)

                        Dim oMenuItem As SAPbouiCOM.MenuItem
                        oMenuItem = getIDHForm().goParent.goApplication.Menus.Item(IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU)

                        Dim oMenus As SAPbouiCOM.Menus
                        Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                        oCreationPackage = getIDHForm().goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)

                        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
                        oCreationPackage.UniqueID = "DUPLICATE"
                        oCreationPackage.String = getIDHForm().getTranslatedWord("Duplicate Row")
                        oCreationPackage.Enabled = True
                        oMenus = oMenuItem.SubMenus
                        oMenus.AddEx(oCreationPackage)

                        'Delete Option
                        oCreationPackage.UniqueID = "DELADDITM"
                        oCreationPackage.String = getIDHForm().getTranslatedWord("Delete Additional Item Row")
                        oCreationPackage.Enabled = True
                        oMenus = oMenuItem.SubMenus
                        oMenus.AddEx(oCreationPackage)
                    End If
                    Return True
                Else
                    If getIDHForm().goParent.goApplication.Menus.Exists("DUPLICATE") Then
                        getIDHForm().goParent.goApplication.Menus.RemoveEx("DUPLICATE")
                    End If
                    If getIDHForm().goParent.goApplication.Menus.Exists("DELADDITM") Then
                        getIDHForm().goParent.goApplication.Menus.RemoveEx("DELADDITM")
                    End If
                    Return True
                End If
            ElseIf iEventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                If bBefore = False Then
                    If oData.MenuUID.Equals("DUPLICATE") Then
                        'Check if this is a valid request
                        Dim oSelected As SAPbouiCOM.SelectedRows = getGrid().Rows.SelectedRows()
                        If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then
                            doDuplicateAdditionalItemRows()
                        End If
                    ElseIf oData.MenuUID.Equals("DELADDITM") Then
                        'Check if this is a valid request
                        Dim oSelected As SAPbouiCOM.SelectedRows = getGrid().Rows.SelectedRows()
                        If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then
                            doDeleteAdditionalItemRows()
                        End If
                    End If
                End If
            ElseIf iEventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH_VALUESET Then

            End If

            Return MyBase.doSendGridEvent(iEventType, bBefore, bActSuccess, sColID, iRow, oData)
        End Function

        '**
        ' Duplicate the selected rows
        '**
        Private Sub doDuplicateAdditionalItemRows()
            getSBOForm.Freeze(True)
            Try
                Dim iLastRow As Integer = getLastRowIndex()

                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                'Dim iCode As Integer
                'Dim sCode As String

                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    doAddRows(oSelected.Count)
                    'iCode = DataHandler.INSTANCE.getNextNumber(msAutoNum, oSelected.Count) 'getIDHForm.goParent.goDB.doNextNumber("ADDEXPKEY")
                    Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(msAutoNumPrefix, msAutoNum, oSelected.Count)
                    Dim iRow As Integer

                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iRow = GetDataTableRowIndex(oSelected, iIndex)

                        doCopyRow(iRow, iLastRow)

                        'sCode = DataHandler.doPackCode(msAutoNumPrefix, iCode)
                        'Reset the values to their defaults
                        Dim oDataTable As SAPbouiCOM.DataTable = getSBOGrid.DataTable
                        'oDataTable.SetValue(doIndexField("Code"), iLastRow, iCode.ToString())
                        'oDataTable.SetValue(doIndexField("Name"), iLastRow, iCode.ToString())
                        oDataTable.SetValue(doIndexField("Code"), iLastRow, oNumbers.doPackCodeCode(iIndex))
                        oDataTable.SetValue(doIndexField("Name"), iLastRow, oNumbers.doPackNameCode(iIndex))
                        'doCalculateLineTotals(iLastRow)
                        iLastRow = iLastRow + 1
                        'iCode = iCode + 1
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error duplicating the rows.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBD", {Nothing})
            Finally
                getSBOForm.Freeze(False)
            End Try
        End Sub

        Private Sub doDeleteAdditionalItemRows()
            getSBOForm.Freeze(True)
            Try
                'Step through the selected rows and delete
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    'Check weather can delete the Add. Item row. 
                    'Start
                    Dim bCanDeleteAddItemRow As Boolean = True
                    Dim sWOHNum As String = doGetFieldValue("U_JobNr")
                    Dim sWORNum As String = doGetFieldValue("U_RowNr")
                    If sWOHNum IsNot Nothing AndAlso sWORNum IsNot Nothing Then
                        Dim oRS As SAPbobsCOM.Recordset
                        oRS = DataHandler.INSTANCE.doSBOSelectQuery("doDeleteAdditionalItemRows", "select U_AEDate, U_Status, U_PStat, U_RowSta from [@idh_jobshd] where code = '" + sWORNum + "'")
                        If oRS.RecordCount > 0 Then
                            Dim dAEDate As DateTime = Convert.ToDateTime(oRS.Fields.Item("U_AEDate").Value.ToString())
                            Dim sSOStatus As String = oRS.Fields.Item("U_Status").Value.ToString()
                            Dim sPOStatus As String = oRS.Fields.Item("U_PStat").Value.ToString()
                            Dim sRowSta As String = oRS.Fields.Item("U_RowSta").Value.ToString()
                            Dim sAEDate As String
                            If dAEDate.Year < 1900 Then
                                sAEDate = String.Empty
                            Else
                                sAEDate = dAEDate.ToShortDateString()
                            End If

                            If sAEDate.Length > 0 OrElse _
                                sSOStatus.StartsWith(FixedValues.getStatusOrdered()) OrElse _
                                sSOStatus.StartsWith(FixedValues.getStatusInvoiced()) OrElse _
                                sPOStatus.StartsWith(FixedValues.getStatusOrdered()) OrElse _
                                sPOStatus.StartsWith(FixedValues.getStatusInvoiced()) OrElse _
                                com.idh.bridge.lookups.FixedValues.isDeleted(sRowSta) Then
                                bCanDeleteAddItemRow = False
                            End If
                        End If
                        DataHandler.INSTANCE.doReleaseRecordset(oRS)
                    End If
                    'End

                    If bCanDeleteAddItemRow Then
                        doRemoveSelectedRows()
                    Else
                        com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Can not delete additional item row for a complete/orderd/invoiced/deleted waste order.")
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error deleting the row.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBDR", {Nothing})
            Finally
                getSBOForm.Freeze(False)
            End Try
        End Sub

        Private Sub doCopyFromDataHandler(ByVal sRowCode As String)
            moDataTable.Clear()
            Dim oDCol As SAPbouiCOM.DataColumns = moDataTable.Columns
            Dim idx As Integer
            Dim iColIndex As Integer
            For idx = 0 To moGridControl.getListFields().Count - 1
                Dim oField As ListField = moGridControl.getListFields().Item(idx)
                iColIndex = moDataHandler.getColIndex(oField.msFieldName)
                If iColIndex > -1 Then
                    Dim sColName As String = moDataHandler.getColName(iColIndex)
                    Dim oType As SAPbouiCOM.BoFieldsType = moDataHandler.getColType(iColIndex)
                    If oType = SAPbouiCOM.BoFieldsType.ft_NotDefined Then
                        oType = SAPbouiCOM.BoFieldsType.ft_Text
                    End If
                    Dim iSize As Integer = moDataHandler.getColSize(iColIndex)
                    oDCol.Add(sColName, oType, iSize)
                Else
                    oDCol.Add(oField.msFieldName, SAPbouiCOM.BoFieldsType.ft_Text, 10)
                End If
            Next

            Dim oValue As String
            If moDataHandler.getRowCount() > 0 Then
                Dim iRow As Integer = 0
                For r As Integer = 0 To moDataHandler.getRowCount() - 1
                    If moDataHandler.wasDeleted(r) = False Then
                        Dim sRowCodeW As String = moDataHandler.getFieldValue("U_RowNr", r)
                        If Not sRowCodeW Is Nothing AndAlso sRowCodeW.Equals(sRowCode) Then
                            'doAddDataLines(1)
                            doAddDataLine(False)
                            For idx = 0 To moGridControl.getListFields().Count - 1
                                iColIndex = moDataHandler.getColIndex(moGridControl.getListFields().Item(idx).msFieldName)
                                If iColIndex > -1 Then
                                    oValue = moDataHandler.getFieldValue(iColIndex, r)
                                    If Not oValue Is Nothing Then
                                        moDataTable.SetValue(idx, iRow, oValue)
                                    End If
                                    'Else
                                    '	moDataTable.SetValue(idx, iRow, iRow)
                                End If
                            Next
                            iRow = iRow + 1
                        End If
                    End If
                Next
            End If
            If moDataTable.Rows.Count = 0 Then
                Dim oDRows As SAPbouiCOM.DataRows
                oDRows = moDataTable.Rows
                oDRows.Add(1)
            End If

        End Sub

        Public Function getDataHandler() As WR1_Data.idh.data.AdditionalExpenses
            Return moDataHandler
        End Function

        '      Public Overrides Sub doReloadData()
        '          doReloadDataFH(msOrderDirection, True, True, Nothing)
        '      End Sub

        '      Public Overrides Sub doReloadData(ByVal bDoRules As Boolean)
        '          doReloadDataFH(msOrderDirection, True, bDoRules, Nothing)
        '      End Sub

        'Public Overrides Sub doReloadData(ByVal sDirection As String, ByVal bAddBlankLine As Boolean )
        '      	doReloadDataFH(sDirection, bAddBlankLine, True, Nothing)
        '      End Sub

        Public Overrides Sub doReloadData(ByVal sDirection As String, ByVal bAddBlankLine As Boolean, ByVal bDoRules As Boolean)
            doReloadDataFH(sDirection, bAddBlankLine, bDoRules, Nothing)
        End Sub

        Public Sub doReloadDataFH(Optional ByVal sRow As String = Nothing)
            doReloadDataFH(msOrderDirection, True, True, sRow)
        End Sub

        Public Sub doReloadDataFH(ByVal sDirection As String, ByVal bAddBlankLine As Boolean, Optional ByVal bDoRules As Boolean = True, Optional ByVal sRow As String = Nothing)
            moGridForm.Freeze(True)
            Dim sQry As String = "NONE"
            Try
                'IDHAddOns.idh.addon.Base.doDebug("Prepare Query")
                DataHandler.INSTANCE.DebugTickGRID = "Prepare Query"

                Dim bDoCount As Boolean = False
                If mbDoCount Then
                    If mcExpandedState = "N" Then
                        bDoCount = True
                        If doIndexField(COUNTCOLTITLE) < 0 Then
                            doInsertListField("'1000000'", COUNTCOLTITLE, False, COUNTWCOL, "IGNORE", COUNTCOLTITLE, 0)
                        End If
                    Else
                        doRemoveListField(COUNTCOLTITLE)
                    End If
                End If

                If Not moDataHandler Is Nothing Then
                    doCopyFromDataHandler(sRow)
                Else
                    moGridControl.doCreateFilterString()
                    sQry = moGridControl.getQueryString(sDirection, False)

                    'IDHAddOns.idh.addon.Base.doDebug("Do Query")
                    DataHandler.INSTANCE.DebugTickGRID = "Do Query"

                    ExecuteQuery(sQry, bDoCount)
                End If

                doAddEditLine()

                Dim iCount As Integer = moDataTable.Rows.Count
                If bDoRules Then
                    'IDHAddOns.idh.addon.Base.doDebug("Set Rules")
                    DataHandler.INSTANCE.DebugTickGRID = "Set Rules"

                    doApplyRules()
                Else
                    If iCount > 0 Then
                        moGridObj.AutoResizeColumns()
                    End If
                End If

                'IDHAddOns.idh.addon.Base.doDebug("Done")
                DataHandler.INSTANCE.DebugTickGRID = "Done"

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString() & " - " & sQry, "Error reloading the Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", {Nothing})
            End Try
            moGridForm.Freeze(False)
        End Sub

        ''Public Overrides Function doC()

        Public Overrides Function doItemEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim bResult As Boolean = MyBase.doItemEvent(pVal, BubbleEvent)
            If pVal.ItemUID.Equals(msGridId) = False And (pVal.EventType <> SAPbouiCOM.BoEventTypes.et_KEY_DOWN) Then
                Return False
            End If

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    Dim iCurRow As Integer = getSBOGrid.GetDataTableRowIndex(pVal.Row)
                    If pVal.ItemChanged Then
                        If doCheckIsSameCol(pVal.ColUID, "U_ItmCost") Then
                            doSetFieldValue("U_FPCost", iCurRow, "Y")
                        ElseIf doCheckIsSameCol(pVal.ColUID, "U_ItmChrg") Then
                            doSetFieldValue("U_FPChrg", iCurRow, "Y")
                        End If
                    End If
                End If
            End If
            Return bResult
        End Function

        Public Overrides Sub doHandleSearchResults(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            MyBase.doHandleSearchResults(oForm, sModalFormType)
        End Sub

        ''## MA Start 16-06-2014
        Public Overrides Sub doHandleSearchCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            Dim strColumn As String
            strColumn = (IDHAddOns.idh.forms.Base.getSharedData(oForm, "#LASTFLD#"))
            'MyBase.doHandleSearchCancled(oForm, sModalFormType)
            If strColumn = "U_ItemCd" AndAlso doGetFieldValue("U_ItemCd", miCurrentDataRow).ToString.Trim <> "" Then
                Dim objItem As SAPbobsCOM.Items = Nothing
                Try
                    objItem = IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems)
                    If Not objItem.GetByKey(doGetFieldValue("U_ItemCd", miCurrentDataRow).ToString.Trim) Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Error: Invalid item code.")
                        doSetFieldValue("U_ItemDsc", miCurrentDataRow, "")
                        doSetFieldValue("U_ItemCd", miCurrentDataRow, "")
                        Exit Sub
                    End If
                Catch ex As Exception
                Finally
                    IDHAddOns.idh.data.Base.doReleaseObject(objItem)
                    objItem = Nothing
                End Try
                ''##MA Start 23-09-2014 Issue#458
            ElseIf strColumn = "U_EmpId" AndAlso doGetFieldValue("U_EmpId", miCurrentDataRow).ToString.Trim <> "" Then
                Dim objEmps As SAPbobsCOM.EmployeesInfo = Nothing
                Try
                    objEmps = IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oEmployeesInfo)
                    If Not objEmps.GetByKey(doGetFieldValue("U_EmpId", miCurrentDataRow).ToString.Trim) Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Error: Invalid item code.")
                        doSetFieldValue("U_EmpId", miCurrentDataRow, "")
                        doSetFieldValue("U_EmpNm", miCurrentDataRow, "")
                        Exit Sub
                    End If
                Catch ex As Exception
                Finally
                    IDHAddOns.idh.data.Base.doReleaseObject(objEmps)
                    objEmps = Nothing
                End Try
                ''##MA End 23-09-2014 Issue#458
            ElseIf strColumn = "U_SuppCd" Then
                Dim objBP As SAPbobsCOM.BusinessPartners = Nothing
                Try
                    objBP = IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                    If Not objBP.GetByKey(doGetFieldValue("U_SuppCd", miCurrentDataRow).ToString.Trim) Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Error: Invalid item code.")
                        doSetFieldValue("U_SuppNm", miCurrentDataRow, "")
                        doSetFieldValue("U_SuppCd", miCurrentDataRow, "")
                        doSetFieldValue("U_SuppCurr", miCurrentDataRow, "")
                        Exit Sub
                    End If
                Catch ex As Exception
                Finally
                    IDHAddOns.idh.data.Base.doReleaseObject(objBP)
                    objBP = Nothing
                End Try
            End If
        End Sub
        ''## MA End 16-06-2014
        'Protected Overrides Function doAddFieldChanged(ByVal iRow As Integer, ByVal oField As Object, Optional ByVal bDoEvent As Boolean = False) As Boolean
        '	If MyBase.doAddFieldChanged(iRow, oField, bDoEvent) Then
        '		Dim sWrkField As String
        '    	sWrkField = doFindDBField(oField, True).Trim()
        '    	If Not sWrkField Is Nothing AndAlso sWrkField.Length > 0 Then
        '    		If sWrkField.Equals("U_Quantity") Then
        '    		   doCalculateLineTotals(iRow)
        '    		Else If sWrkField.Equals("U_ItmCost") Then
        '    			'doSetFieldValue("U_FPCost", iRow, "Y")
        '    			'doCalculateLineTotals(iRow)
        '    			doCalculateCostLineTotals(iRow)
        '    		Else If sWrkField.Equals("U_ItmChrg") Then
        '    			'doSetFieldValue("U_FPChrg", iRow, "Y")
        '    			'doCalculateLineTotals(iRow)
        '    			doCalculateChargeLineTotals(iRow)
        '    		End If
        '    	End If
        '		Return True	
        '	End If
        '	Return False
        'End Function

        'Replaces the above
        Protected Overrides Sub doSetFieldChanged(ByVal iRow As Integer, ByVal sFieldName As String, ByVal oValue As Object, ByVal bBeforeAction As Boolean)
            MyBase.doSetFieldChanged(iRow, sFieldName, oValue, bBeforeAction)

            If BlockChangeTrigger = False Then
                Dim sWrkField As String
                sWrkField = doFindDBField(sFieldName, True).Trim()
                If Not sWrkField Is Nothing AndAlso sWrkField.Length > 0 Then
                    If sWrkField.Equals("U_Quantity") Then
                        'Handle on the row as we need new price lookups
                        'doCalculateLineTotals(iRow)
                    ElseIf sWrkField.Equals("U_ItmCost") Then
                        doCalculateCostLineTotals(iRow)
                    ElseIf sWrkField.Equals("U_ItmChrg") Then
                        doCalculateChargeLineTotals(iRow)
                    ElseIf sWrkField.Equals("U_SuppCd") Then
                        'doCalculateCostLineTotals(iRow)
                    End If
                End If
            End If
        End Sub

        Public Sub doCalculateLinesTotals()
            Dim iRow As Integer
            For iRow = 0 To getRowCount() - 1
                doCalculateLineTotals(iRow)
            Next
        End Sub

        Public Sub doCalculateLineTotals(ByVal iRow As Integer)
            doCalculateChargeLineTotals(iRow)
            doCalculateCostLineTotals(iRow)
        End Sub

        ''** look for the Line containing the Customer linked to the carrier with the same waste code and update the UOM
        Public Sub doUpdateRebateLineUOM(ByVal sCarrierCode As String, ByVal sWasteCode As String, ByVal sUOM As String)
            Dim oCustomerSupplier As LinkedBP = Config.INSTANCE.doGetLinkedBP(sCarrierCode)
            If oCustomerSupplier IsNot Nothing Then
                Dim iRow As Integer = 0
                Dim sCustomerSupplier As String
                Dim sItemCode As String
                For iRow = 0 To getRowCount() - 1
                    sCustomerSupplier = doGetFieldValue(IDH_WOADDEXP._SuppCd, iRow)
                    sItemCode = doGetFieldValue(IDH_WOADDEXP._ItemCd, iRow)
                    If sCustomerSupplier = oCustomerSupplier.LinkedBPCardCode AndAlso sWasteCode = sItemCode Then
                        doSetFieldValue(IDH_WOADDEXP._UOM, iRow, sUOM)
                        Exit For
                    End If
                Next
            End If
        End Sub

        ''** look for the Line containing the Customer linked to the carrier with the same waste code and update the weight and recalc
        Public Sub doUpdateRebateLineWeight(ByVal sCarrierCode As String, ByVal sWasteCode As String, ByVal dWeight As Double, ByVal sUOM As String)
            Dim oCustomerSupplier As LinkedBP = Config.INSTANCE.doGetLinkedBP(sCarrierCode)
            If oCustomerSupplier IsNot Nothing Then
                Dim iRow As Integer = 0
                Dim sCustomerSupplier As String
                Dim sItemCode As String
                For iRow = 0 To getRowCount() - 1
                    sCustomerSupplier = doGetFieldValue(IDH_WOADDEXP._SuppCd, iRow)
                    sItemCode = doGetFieldValue(IDH_WOADDEXP._ItemCd, iRow)
                    If sCustomerSupplier = oCustomerSupplier.LinkedBPCardCode AndAlso sWasteCode = sItemCode Then
                        doSetFieldValue(IDH_WOADDEXP._UOM, iRow, sUOM)
                        doSetFieldValue(IDH_WOADDEXP._Quantity, iRow, dWeight)
                        doCalculateLineTotals(iRow)
                        Exit For
                    End If
                Next
            End If
        End Sub

        Public Sub doCalculateChargeLineTotals(ByVal iRow As Integer)
            Dim dQty As Double = doGetFieldValue("U_Quantity", iRow)
            Dim dCharge As Double = doGetFieldValue("U_ItmChrg", iRow)
            Dim sChrgVatGrp As String = doGetFieldValue("U_ChrgVatGrp", iRow)
            Dim dChrgT As Double = doGetFieldValue("U_ItmTChrg", iRow)
            Dim sChrgCalc As String = doGetFieldValue(IDH_WOADDEXP._ChrgCal, iRow)

            If String.IsNullOrWhiteSpace(sChrgCalc) = False AndAlso sChrgCalc = "FIXED" Then
                dQty = 1
            End If

            Dim dChrgVat As Double = 0
            Dim dSub As Double = dQty * dCharge

            If mbDoVat Then
                dChrgVat = Config.INSTANCE.doGetVatRate(sChrgVatGrp, mdDocDate)
                dChrgVat = dSub * (dChrgVat / 100)
            End If

            '** Calculate the Charge
            dChrgT = dSub + dChrgVat

            doSetFieldValue("U_ItmChrgVat", iRow, dChrgVat, False, False, False)
            doSetFieldValue("U_ItmTChrg", iRow, dChrgT, False, False, True)
        End Sub

        Public Sub doCalculateCostLineTotals(ByVal iRow As Integer)
            Dim dQty As Double = doGetFieldValue("U_Quantity", iRow)
            Dim dCost As Double = doGetFieldValue("U_ItmCost", iRow)
            Dim sCostVatGrp As String = doGetFieldValue("U_CostVatGrp", iRow)
            Dim dCostT As Double = doGetFieldValue("U_ItmTCost", iRow)
            Dim sCstCalc As String = doGetFieldValue(IDH_WOADDEXP._CstCal, iRow)

            If String.IsNullOrWhiteSpace(sCstCalc) = False AndAlso sCstCalc = "FIXED" Then
                dQty = 1
            End If

            Dim dCostVat As Double = 0
            Dim dSub As Double = dQty * dCost

            If mbDoVat Then
                dCostVat = Config.INSTANCE.doGetVatRate(sCostVatGrp, mdDocDate)
                dCostVat = dSub * (dCostVat / 100)
            End If

            '** Calculate the Cost
            dCostT = dSub + dCostVat

            doSetFieldValue("U_ItmCostVat", iRow, dCostVat, False, False, False)
            doSetFieldValue("U_ItmTCost", iRow, dCostT, False, False, True)
        End Sub

        Public Function doCalculateProfit() As Double
            Dim dCost As Double
            Dim dSell As Double

            Dim iRow As Integer = 0
            Dim dQty As Double
            Dim dChrgQty As Double = 0
            Dim dCstQty As Double = 0

            Dim sChrgCalc As String = CType(doGetFieldValue(IDH_WOADDEXP._ChrgCal, iRow), String)
            Dim sCstCalc As String = CType(doGetFieldValue(IDH_WOADDEXP._CstCal, iRow), String)

            For iRow = 0 To getRowCount() - 1
                dQty = CType(doGetFieldValue("U_Quantity", iRow), Double)
                If String.IsNullOrWhiteSpace(sChrgCalc) = False AndAlso sChrgCalc = "FIXED" Then
                    dChrgQty = 1
                Else
                    dChrgQty = dQty
                End If
                dCost = dCost + (dChrgQty * CType(doGetFieldValue("U_ItmCost", iRow), Double))

                If String.IsNullOrWhiteSpace(sCstCalc) = False AndAlso sCstCalc = "FIXED" Then
                    dCstQty = 1
                Else
                    dCstQty = dQty
                End If
                dSell = dSell + (dCstQty * CType(doGetFieldValue("U_ItmChrg", iRow), Double))
            Next
            Return dSell - dCost
        End Function

        Public Function doCalculateCostTotals() As Double
            Dim dTotal As Double = 0
            Dim iRow As Integer = 0
            For iRow = 0 To getRowCount() - 1
                dTotal = dTotal + CType(doGetFieldValue("U_ItmTCost", iRow), Double)
            Next
            Return dTotal
        End Function

        Public Function doCalculateChargeTotals() As Double
            Dim dTotal As Double = 0
            Dim iRow As Integer = 0
            For iRow = 0 To getRowCount() - 1
                dTotal = dTotal + CType(doGetFieldValue("U_ItmTChrg", iRow), Double)
            Next
            Return dTotal
        End Function

        ''MA Start 07-04-2014
        Public Function doCalculateCostVAT() As Double
            Dim dTotal As Double = 0
            Dim iRow As Integer = 0
            For iRow = 0 To getRowCount() - 1
                dTotal = dTotal + CType(doGetFieldValue("U_ItmCostVat", iRow), Double)
            Next
            Return dTotal
        End Function

        Public Function doCalculateChargeVAT() As Double
            Dim dTotal As Double = 0
            Dim iRow As Integer = 0
            For iRow = 0 To getRowCount() - 1
                dTotal = dTotal + CType(doGetFieldValue("U_ItmChrgVat", iRow), Double)
            Next
            Return dTotal
        End Function

        Public Function doCalculateCostTotalsExcVAT() As Double
            Dim dTotal As Double = 0
            Dim iRow As Integer = 0
            For iRow = 0 To getRowCount() - 1
                dTotal = dTotal + (CType(doGetFieldValue("U_Quantity", iRow), Double) * CType(doGetFieldValue("U_ItmCost", iRow), Double))
            Next
            Return dTotal
        End Function

        Public Function doCalculateChargeTotalsExcVAT() As Double
            Dim dTotal As Double = 0
            Dim iRow As Integer = 0
            For iRow = 0 To getRowCount() - 1
                dTotal = dTotal + (CType(doGetFieldValue("U_Quantity", iRow), Double) * CType(doGetFieldValue("U_ItmChrg", iRow), Double))
            Next
            Return dTotal
        End Function

        ''MA End 07-04-2014

        '        Public Overrides Sub IN_doSetFieldValue(ByVal sFieldName As String, ByVal iRow As Integer, ByVal oData As Object, Optional ByVal bDoBlockNewLine As Boolean = False, Optional ByVal bDoForceUpdate As Boolean = False)
        '    		MyBase.IN_doSetFieldValue(sFieldName, iRow, oData, bDoBlockNewLine, bDoForceUpdate)
        '        End Sub

        Public Sub doCalculateLinesPrices( _
                ByVal sCardCode As String, _
                ByVal sContainerCode As String, _
                ByVal sContainerGrp As String, _
                ByVal sWastCd As String, _
                ByVal sAddress As String, _
                ByVal sBranch As String, _
                ByVal sJobType As String, _
                ByVal dDocDate As Date, _
                ByVal dReadWeight As Double, _
                ByVal sUOM As String, _
                ByVal sZipCode As String)
            doCalculateLinesChargePrices(sCardCode, sContainerCode, sContainerGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dReadWeight, sUOM, sZipCode)
            doCalculateLinesCostPrices(sCardCode, sContainerCode, sContainerGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dReadWeight, sUOM, sZipCode)
        End Sub

        Public Sub doCalculateLinesChargePrices( _
                ByVal sCardCode As String, _
                ByVal sContainerCode As String, _
                ByVal sContainerGrp As String, _
                ByVal sWastCd As String, _
                ByVal sAddress As String, _
                ByVal sBranch As String, _
                ByVal sJobType As String, _
                ByVal dDocDate As Date, _
                ByVal dReadWeight As Double, _
                ByVal sUOM As String, _
                ByVal sZipCode As String)

            Dim iRow As Integer
            For iRow = 0 To getRowCount() - 1
                doCalculateLineChargePrices(sCardCode, sContainerCode, sContainerGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dReadWeight, sUOM, iRow, sZipCode)
            Next
        End Sub

        Public Sub doCalculateLineChargePrices( _
                ByVal sCardCode As String, _
                ByVal sContainerCode As String, _
                ByVal sContainerGrp As String, _
                ByVal sWastCd As String, _
                ByVal sAddress As String, _
                ByVal sBranch As String, _
                ByVal sJobType As String, _
                ByVal dDocDate As Date, _
                ByVal dReadWeight As Double, _
                ByVal sUOM As String, _
                ByVal iRow As Integer, _
                ByVal sZipCode As String)

            Dim sFrozen As String = CType(doGetFieldValue("U_FPChrg", iRow), String)
            If sFrozen.Equals("Y") Then
                doCalculateChargeLineTotals(iRow)
                Exit Sub
            End If

            Dim sSuppier As String = CType(doGetFieldValue("U_SuppCd", iRow), String)
            Dim sAddItem As String = CType(doGetFieldValue("U_ItemCd", iRow), String)
            Dim dCharge As Double = 0 'doGetFieldValue("U_ItmChrg", iRow)
            Dim dChrgVat As Double = 0 'doGetFieldValue("U_ItmChrgVat", iRow)

            If Config.INSTANCE.getParameterAsBool("PLFUOM", False) = True Then
                sUOM = CType(doGetFieldValue("U_UOM", iRow), String)
            End If

            If sCardCode Is Nothing Then
                sCardCode = ""
            End If

            BlockChangeTrigger = True
            Try
                'If sCardCode.Length > 0 AndAlso _
                '	sAddItem.Length > 0 Then
                If sAddItem.Length > 0 Then

                    Dim dQty As Double = doGetFieldValue("U_Quantity", iRow)
                    Dim oPrice As com.idh.dbObjects.User.AdditionalPrice
                    oPrice = Config.INSTANCE.doGetAdditionalChargePrices(msWR1JobType, sJobType, sContainerGrp, sContainerCode, sCardCode, dReadWeight, sUOM, sWastCd, sAddress, dDocDate, sBranch, sAddItem, sZipCode)

                    ''FIXED

                    If Not oPrice Is Nothing Then
                        dCharge = oPrice.mdPrice
                        dChrgVat = oPrice.mdVat

                        'now lookup the additional expenses price
                        '** Calculate the Charge
                        dChrgVat = (dQty * dCharge) * (dChrgVat / 100)

                        doSetFieldValue(IDH_CSITPR._ChrgCal, iRow, oPrice.msCalcType, False, False, False)
                        doSetFieldValue("U_ChrgVatGrp", iRow, oPrice.msVatGroup, False, False, False)
                    End If
                End If
                doSetFieldValue("U_ItmChrgVat", iRow, dChrgVat, False, False, False)
                doSetFieldValue("U_ItmChrg", iRow, dCharge, False, False, False)

                doCalculateChargeLineTotals(iRow)
            Catch ex As Exception
                Throw ex
            End Try
            BlockChangeTrigger = False
        End Sub

        Public Sub doCalculateLinesCostPrices( _
                ByVal sCardCode As String, _
                ByVal sContainerCode As String, _
                ByVal sContainerGrp As String, _
                ByVal sWastCd As String, _
                ByVal sAddress As String, _
                ByVal sBranch As String, _
                ByVal sJobType As String, _
                ByVal dDocDate As Date, _
                ByVal dReadWeight As Double, _
                ByVal sUOM As String, _
                ByVal sZipCode As String)

            Dim iRow As Integer
            For iRow = 0 To getRowCount() - 1
                doCalculateLineCostPrices(sCardCode, sContainerCode, sContainerGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dReadWeight, sUOM, iRow, sZipCode)
            Next
        End Sub

        Public Sub doCalculateLineCostPrices( _
                ByVal sCardCode As String, _
                ByVal sContainerCode As String, _
                ByVal sContainerGrp As String, _
                ByVal sWastCd As String, _
                ByVal sAddress As String, _
                ByVal sBranch As String, _
                ByVal sJobType As String, _
                ByVal dDocDate As Date, _
                ByVal dReadWeight As Double, _
                ByVal sUOM As String, _
                ByVal iRow As Integer, _
                ByVal sZipCode As String)

            Dim sFrozen As String = doGetFieldValue("U_FPCost", iRow)
            If sFrozen.Equals("Y") Then
                doCalculateCostLineTotals(iRow)
                Exit Sub
            End If

            Dim sSuppier As String = doGetFieldValue("U_SuppCd", iRow)
            Dim sAddItem As String = doGetFieldValue("U_ItemCd", iRow)
            Dim dCost As Double = 0 'doGetFieldValue("U_ItmCost", iRow)
            Dim dCostVat As Double = 0 'doGetFieldValue("U_ItmCostVat", iRow)

            If Config.INSTANCE.getParameterAsBool("PLFUOM", False) = True Then
                sUOM = doGetFieldValue("U_UOM", iRow)
            End If

            If sCardCode Is Nothing Then
                sCardCode = ""
            End If

            If sSuppier Is Nothing Then
                sSuppier = ""
            End If

            BlockChangeTrigger = True
            Try
                'If sCardCode.Length > 0 AndAlso _
                '	sAddItem.Length > 0 AndAlso _
                '	sSuppier.Length > 0 Then
                If sAddItem.Length > 0 Then
                    Dim dQty As Double = doGetFieldValue("U_Quantity", iRow)
                    'Dim dCostT As Double

                    Dim oPrice As com.idh.dbObjects.User.AdditionalPrice
                    oPrice = Config.INSTANCE.doGetAdditionalCostPrices(msWR1JobType, sJobType, sContainerGrp, sContainerCode, sSuppier, dReadWeight, sUOM, sWastCd, sAddress, dDocDate, sBranch, sAddItem, sZipCode)

                    If Not oPrice Is Nothing Then
                        dCost = oPrice.mdPrice
                        dCostVat = oPrice.mdVat

                        '** Calculate the Cost
                        dCostVat = (dQty * dCost) * (dCostVat / 100)

                        doSetFieldValue("U_CostVatGrp", iRow, oPrice.msVatGroup, False, False, False)
                        doSetFieldValue(IDH_WOADDEXP._CstCal, iRow, oPrice.msCalcType, False, False, False)
                    End If
                End If
                doSetFieldValue("U_ItmCostVat", iRow, dCostVat, False, False, False)
                doSetFieldValue("U_ItmCost", iRow, dCost, False, False, False)
                doCalculateCostLineTotals(iRow)
            Catch ex As Exception
                Throw ex
            End Try
            BlockChangeTrigger = False

        End Sub

        Public Overrides Function doProcessData() As Boolean
            If Not moDataHandler Is Nothing Then
                Return doSetData()
            Else
                Return MyBase.doProcessData()
            End If
        End Function

        'This will Add a row to the DataHandler if the row is not in the Handlers buffer and then set the Handler's data to that of the Grid
        Private Function doSetData() As Boolean
            Dim iRow As Integer
            Dim iWrkRow As Integer
            Dim oData(8) As Object
            Dim iColIndex As Integer
            Dim idx As Integer

            If hasChangedRows() OrElse hasAddedRows() Then
                For iRow = 0 To moDataTable.Rows.Count - 1
                    Dim sCode As String = doGetFieldValue("Code", iRow)
                    Dim sItemCd As String = doGetFieldValue("U_ItemCd", iRow)
                    If sCode.Length > 0 Then
                        If IsAddedRow(iRow) OrElse IsChangedRow(iRow) Then
                            If sItemCd.Length > 0 Then
                                iWrkRow = moDataHandler.doSetRow(sCode)
                                For idx = 0 To moGridControl.getListFields().Count - 1
                                    Dim sFieldName As String = moGridControl.getListFields().Item(idx).msFieldName
                                    Dim oValue As Object

                                    iColIndex = moDataHandler.getColIndex(sFieldName)
                                    If iColIndex > -1 Then
                                        oValue = moDataTable.GetValue(idx, iRow)
                                        moDataHandler.setFieldValue(iColIndex, iWrkRow, oValue)
                                    End If
                                Next
                            End If
                        End If
                    End If
                Next
            End If

            If hasDeletedRows() Then
                Dim sCode As String
                For iRow = 0 To maDeletedRows.Count - 1
                    sCode = maDeletedRows.Item(iRow)
                    moDataHandler.doRemoveRow(sCode)
                Next
            End If
            Return True
        End Function

        Sub doAddExpense(sJobNr As String, sRowNr As String, sCustCd As String, sCustNm As String, sCustCurr As String, sSuppCd As String, sSuppNm As String, sSuppCurr As String, sEmpId As String, sEmpNm As String, sItemCd As String, sItemNm As String, sUOMForAddItemRow As String, dManQty As Double, nItmCost As Double, nItmChrg As Double, sCostVatGrp As String, sChrgVatGrp As String, p18 As Boolean, p19 As Boolean)
            Throw New NotImplementedException
        End Sub

#Region "BP Additional Charges"
        'Public Sub getBPAdditionalCharges(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
        '    Dim sBPCode As String = oWOR.U_CustCd
        '    Dim oBPAC As IDH_BPADCHG
        '    Dim oList As ArrayList
        '    oBPAC = New IDH_BPADCHG()
        '    oList = oBPAC.getBPAdditionalCharges(sBPCode, oWOR.U_JobTp)

        '    If Not oList Is Nothing AndAlso oList.Count > 0 Then
        '        Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
        '        For Each oBPAC In oList
        '            If Not oBPAC.U_ItemCode Is Nothing AndAlso oBPAC.U_ItemCode.Length > 0 Then
        '                'Check if the item is allready in the Grid and only adds it if it does not
        '                If oGrid.doIndexOfItem(oBPAC.U_ItemCode) < 0 Then
        '                    Dim sCustCd As String = oWOR.F_CustomerCode
        '                    Dim sCustNm As String = oWOR.F_CustomerName
        '                    Dim sSuppCd As String = "" 'getDFValue(oForm, msRowTable, "U_CongCd")
        '                    Dim sSuppNm As String = "" 'getDFValue(oForm, msRowTable, "U_SCngNm")
        '                    Dim sSuppCurr As String = ""
        '                    Dim sEmpId As String = ""
        '                    Dim sEmpNm As String = ""
        '                    Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, oBPAC.U_ItemCode)
        '                    Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, oBPAC.U_ItemCode)
        '                    Dim dCost As Double = 0

        '                    oGrid.doAddExpense(oWOR.U_JobNr, oWOR.Code, _
        '                        sCustCd, sCustNm, _
        '                        sSuppCd, sSuppNm, sSuppCurr, _
        '                        sEmpId, sEmpNm, _
        '                        oBPAC.U_ItemCode, oBPAC.U_ItemName, _
        '                        "ea", 1, _
        '                        dCost, 0, _
        '                        sCostVatGrp, _
        '                        sChrgVatGrp, _
        '                        True, _
        '                        True, _
        '                        True)
        '                End If
        '            End If

        '        Next
        '        'doWarnMess("BPs Additional charges added. Please review Additional Tab and update Ordered Quantity.", SAPbouiCOM.BoMessageTime.bmt_Short)
        '    End If
        '    oBPAC = Nothing
        '    oList = Nothing
        'End Sub

        'Public Sub removeAutoAddBPAddCharges(ByVal oForm As SAPbouiCOM.Form)
        '    'Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
        '    'Dim sBPCode As String = oWOR.U_CustCd

        '    Dim sBPCode As String = getFormDFValue(oForm, "U_CustCd")
        '    Dim oBPAC As IDH_BPADCHG
        '    Dim oList As ArrayList
        '    oBPAC = New IDH_BPADCHG()
        '    oList = oBPAC.getBPAdditionalCharges(sBPCode)

        '    If Not oList Is Nothing AndAlso oList.Count > 0 Then
        '        Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
        '        For Each oBPAC In oList
        '            If Not oBPAC.U_ItemCode Is Nothing AndAlso oBPAC.U_ItemCode.Length > 0 Then
        '                'Remove Row
        '                Dim iSearchIndx As Integer
        '                iSearchIndx = oGrid.doIndexOfItem(oBPAC.U_ItemCode)
        '                If iSearchIndx > -1 Then
        '                    oGrid.doRemoveRow(iSearchIndx)
        '                End If
        '            End If

        '        Next
        '        'doWarnMess("Existing BPs Additional charges removed.", SAPbouiCOM.BoMessageTime.bmt_Short)
        '    End If
        '    oBPAC = Nothing
        '    oList = Nothing
        'End Sub

        'Public Sub doUpdateAutoAddBPAddCharges(ByVal oForm As SAPbouiCOM.Form, ByVal sDateE As String)
        '    Try
        '        If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
        '            If Not sDateE Is Nothing AndAlso sDateE.Length > 0 Then
        '                Dim sBPCode As String = getFormDFValue(oForm, "U_CustCd")
        '                Dim sJobType As String = getFormDFValue(oForm, "U_JobTp")
        '                Dim dTCharge As Double = getFormDFValue(oForm, "U_TCharge")

        '                Dim oBPAC As IDH_BPADCHG
        '                Dim oList As ArrayList
        '                oBPAC = New IDH_BPADCHG()
        '                oList = oBPAC.getBPAdditionalCharges(sBPCode, sJobType)

        '                If Not oList Is Nothing AndAlso oList.Count > 0 Then
        '                    Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
        '                    For Each oBPAC In oList
        '                        If Not oBPAC.U_ItemCode Is Nothing AndAlso oBPAC.U_ItemCode.Length > 0 Then
        '                            Try
        '                                Dim iSearchIndx As Integer
        '                                iSearchIndx = oGrid.doIndexOfItem(oBPAC.U_ItemCode)
        '                                If iSearchIndx > -1 Then
        '                                    'Parse U_Formula 
        '                                    Dim sExpression As String = String.Empty
        '                                    For Each sKey As String In oBPAC.U_Formula.Split("#")
        '                                        If Not (sKey.Contains("+") Or sKey.Contains("-") Or sKey.Contains("*") Or sKey.Contains("/") Or sKey.Contains("(") Or sKey.Contains(")")) Then
        '                                            If sKey.StartsWith("U_") Then
        '                                                sExpression = sExpression + getFormDFValue(oForm, sKey).ToString()
        '                                            Else
        '                                                sExpression = sExpression + sKey
        '                                            End If
        '                                        Else
        '                                            sExpression = sExpression + sKey
        '                                        End If
        '                                    Next
        '                                    Dim result = New DataTable().Compute(sExpression, Nothing)
        '                                    Dim dCharge As Double = 0
        '                                    dCharge = result
        '                                    oGrid.doSetFieldValue("U_ItmChrg", iSearchIndx, dCharge)
        '                                End If
        '                            Catch ex As Exception
        '                                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doUpdateAutoAddBPAddCharges: Error evaluating the additional charges expression!")
        '                            End Try
        '                        End If

        '                    Next
        '                    'doWarnMess("Existing BPs Additional charges removed.", SAPbouiCOM.BoMessageTime.bmt_Short)
        '                End If
        '                oBPAC = Nothing
        '                oList = Nothing
        '            ElseIf sDateE Is Nothing OrElse sDateE.Equals(String.Empty) Then
        '                'TBD: Clear AutoAdded Rows ??
        '                'removeAutoAddBPAddCharges(oForm)
        '            End If
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doUpdateAutoAddBPAddCharges: Error updating additional item row charges.")
        '    End Try

        'End Sub

#End Region


    End Class
End Namespace
