Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.controls
Imports com.idh.controls.strct
Imports com.idh.bridge
Imports com.idh.bridge.data
Imports com.idh.bridge.lookups
Imports System
Imports com.idh.dbObjects.User
Imports com.idh.dbObjects.numbers

'
' Created by SharpDevelop.
' User: Louis Viljoen
' Date: 20/05/2008
' Time: 09:08
'

Namespace idh.controls.grid
    Public Class WPChemicals
        Inherits IDHAddOns.idh.controls.UpdateGrid

        'Protected Overrides Function doAddFieldChanged(ByVal iRow As Integer, ByVal oField As Object, Optional ByVal bDoEvent As Boolean = False) As Boolean
        '    If MyBase.doAddFieldChanged(iRow, oField, bDoEvent) Then
        '        Dim sWrkField As String
        '        sWrkField = doFindDBField(oField, True).Trim()
        '        If Not sWrkField Is Nothing AndAlso sWrkField.Length > 0 Then
        '            If sWrkField.Equals("U_WgtDed") Then
        '                doCalculateWeightTotals()
        '            ElseIf sWrkField.Equals("U_ValDed") Then
        '                doCalculateValueTotals()
        '            ElseIf sWrkField.Equals("U_Qty") Then
        '                ''doCalcWeight()
        '                'doCalculateWeightTotals()
        '            ElseIf sWrkField.Equals("U_ItemCd") Then
        '                'doCalculateWeightTotals()
        '                ''doCalcWeight()
        '            End If
        '        End If
        '        Return True
        '    End If
        '    Return False
        'End Function

        Public Sub doCalculateLineDeductions(ByVal iRow As Integer)
            doCalculateWeightTotals()
            doCalculateValueTotals()
        End Sub

        Public Sub doSetQty()
            '			..
            '       		Dim iRow As Integer = pVal.Row
            '    		Dim sItemCode As String = getDFValue(oForm, msRowTable, "U_ItemCd")
            '    		Dim dQty As Double = getDFValue(oForm, msRowTable, "U_Qty")
            '    		Dim dDefaultWeight As Double = Config.INSTANCE.doGetItemDefaultWeight( sItemCode )
            '    		Dim dWeight As Double = dQty * dDefaultWeight
        End Sub

        Public Function doCalculateWeightTotals() As Double
            Dim iRow As Integer = 0
            Dim dTotal As Double
            For iRow = 0 To getRowCount() - 1
                dTotal = dTotal + doGetFieldValue("U_WgtDed", iRow)
            Next
            Return dTotal
        End Function

        Public Function doCalculateValueTotals() As Double
            Dim dTotal As Double = 0
            Dim iRow As Integer = 0
            For iRow = 0 To getRowCount() - 1
                dTotal = dTotal + doGetFieldValue("U_ValDed", iRow)
            Next
            Return dTotal
        End Function

        ''** Here is where the Grid options get set
        '' WR1JobType can be set to WO or DO 
        Public Function doSetGridParameters( _
          ByVal sTableName As String, _
          ByVal sChmCode As String, _
          ByVal sItemCd As String, _
          ByVal bCanEdit As Boolean, _
          ByVal dDocDate As Date, _
          ByVal bSetTable As Boolean) As Boolean
            sTableName = sTableName

            msKeyField = "Code"
            If bSetTable Then
                doAddGridTable(New GridTable(sTableName, Nothing, "Code", True), True)
            End If
            msNumberKey = "CASSEQ"

            getItem.AffectsFormMode = False
            setOrderValue("Code")
            doSetDoCount(True)

            mdDocDate = dDocDate
            If mdDocDate = Nothing Then
                mdDocDate = Date.Now()
            End If

            setRequiredFilter("U_ChemicalCd = '" & sChmCode & "' AND U_ItemCd = '" & sItemCd & "'")

            ''Set the grid list fields
            doAddListField("Code", "Code", False, 5, "IGNORE", Nothing)
            doAddListField("Name", "Name", False, 0, "IGNORE", Nothing)
            doAddListField("U_ChemicalCd", "Chemical Code", False, 0, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_UserDefinedObject)
            doAddListField("U_Quantity", "Quantity", bCanEdit, 10, "IGNORE", Nothing)
            doAddListField("U_MinQty", "Minimum Quantity", bCanEdit, 10, Nothing, Nothing)
            doAddListField("U_MaxQty", "Maximum Quantity", bCanEdit, 10, Nothing, Nothing)
            doAddListField("U_ItemCd", "Item Code", False, 0, "IGNORE", Nothing)

            'doAddListField("U_ShpNo", "Shipment No", bCanEdit, 10, "IGNORE", Nothing)
            'doAddListField("U_ShpDt", "Shipment Date", bCanEdit, 10, Nothing, Nothing)
            'doAddListField("U_CarCd", "Carrier Code", bCanEdit, -1, "SRC:IDHCSRCH(CUS)[IDH_BPCOD;IDH_TYPE=%F-S][CARDCODE;U_CarNm=CARDNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            'doAddListField("U_CarNm", "Carrier Name", False, 10, "IGNORE", Nothing)
            'doAddListField("U_Shipd", "Shipped", bCanEdit, 10, "COMBOBOX", Nothing)
            'doAddListField("U_UOM", "UOM", bCanEdit, 10, "COMBOBOX", Nothing)
            'doAddListField("U_Bal", "Balance", bCanEdit, 10, "IGNORE", Nothing)
            'doAddListField("U_Status", "Status", bCanEdit, 10, "COMBOBOX", Nothing)

            'doAddListField("U_Qty", "Qty", bCanEdit, 10, "IGNORE", Nothing)
            'doAddListField("U_ItemCd", "Item Code", bCanEdit, -1, "SRC:IDHISRC(DED)[IDH_ITMCOD][ITEMCODE;U_ItemDsc=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            'doAddListField("U_ItemDsc", "Item Description", bCanEdit, -1, "IGNORE", Nothing)
            'doAddListField("U_WgtDed", "Weight Deduction", bCanEdit, -1, "IGNORE", Nothing)
            'doAddListField("U_ValDed", "Value Deduction", bCanEdit, -1, "IGNORE", Nothing)
            'doAddListField("U_WRRow", "Waste Order Row", True, 10, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_UserDefinedObject)
            Return True
        End Function

        Public Function doIndexOfItem(ByVal sAddItemCd As String) As Integer
            Dim iRow As Integer = 0
            For iRow = 0 To getRowCount() - 1
                If doGetFieldValue("U_ItemCd", iRow).Equals(sAddItemCd) Then
                    Return iRow
                End If
            Next
            Return -1
        End Function

        Public Sub doAddCASChemical( _
          ByVal sChmCd As String, _
          ByVal sChmNm As String, _
          ByVal iQty As Integer, _
          ByVal iMinQty As Integer, _
          ByVal iMaxQty As Integer, _
          ByVal sItemCd As String, _
          ByVal sItemNm As String)

            doAddNewLine()
            miCurrentDataRow = getLastRowIndex()
            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, msNumberKey)

            doSetFieldValue("Code", oNumbers.CodeCode)
            doSetFieldValue("Name", oNumbers.NameCode)
            doSetFieldValue("U_ChemicalCd", sChmCd)
            doSetFieldValue("U_ChemicalNm", sChmNm)
            doSetFieldValue("U_Quantity", iQty)
            doSetFieldValue("U_MinQty", iMinQty)
            doSetFieldValue("U_MaxQty", iMaxQty)
            doSetFieldValue("U_ItemCd", sItemCd)
            doSetFieldValue("U_ItemNm", sItemNm)
            doAddEditLine()
        End Sub

        'Public Sub doUpdateTFSMovement( _
        '  ByVal sCurrentCode As String, _
        '  ByVal sItemCd As String, _
        '  ByVal sItemNm As String, _
        '  ByVal dQty As Double, _
        '  ByVal dWgtDed As Double, _
        '  ByVal dValDed As Double _
        ' )

        '    Dim sCCode As String
        '    Dim iRow As Integer
        '    For iRow = 0 To getRowCount() - 1
        '        sCCode = doGetFieldValue("U_ItemCd", iRow)
        '        If sCCode = sCurrentCode Then
        '            If Not sItemCd Is Nothing AndAlso sItemCd.Length > 0 Then
        '                doSetFieldValue("U_ItemCd", iRow, sItemCd)
        '            End If
        '            If Not sItemNm Is Nothing AndAlso sItemNm.Length > 0 Then
        '                doSetFieldValue("U_ItemDsc", iRow, sItemNm)
        '            End If

        '            doSetFieldValue("U_Qty", iRow, dQty)
        '            doSetFieldValue("U_WgtDed", iRow, dWgtDed)
        '            doSetFieldValue("U_ValDed", iRow, dValDed)
        '            Exit Sub
        '        End If
        '    Next
        'End Sub


#Region "GridFunctions"
        Private moDataHandler As WR1_Data.idh.data.Chemicals = Nothing
        Private mdDocDate As Date
        Private sTableName As String = "@ISB_CASTBL"
        Private msNumberKey As String = "CASSEQ"

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sGridID As String)
            MyBase.New(oIDHForm, oForm, sGridID)
        End Sub

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sGridID As String, _
                    ByVal iLeft As Integer, _
                    ByVal iTop As Integer, _
                    ByVal iWidth As Integer, _
                    ByVal iHeight As Integer)
            MyBase.New(oIDHForm, oForm, sGridID, iLeft, iTop, iWidth, iHeight)
        End Sub

        Public Sub doSetDataHandler(ByRef oDHandler As WR1_Data.idh.data.Chemicals)
            moDataHandler = oDHandler
        End Sub

        '** Use this if the item was already added to the Form
        Public Shared Function doAddToForm( _
          ByVal oIDHForm As IDHAddOns.idh.forms.Base, _
          ByVal oForm As SAPbouiCOM.Form, _
          ByVal sGridID As String, _
          ByVal iLeft As Integer, _
          ByVal iTop As Integer, _
          ByVal iWidth As Integer, _
          ByVal iHeight As Integer, _
          ByVal iFromPane As Integer, _
          ByVal iToPane As Integer) As UpdateGrid

            Dim oGridN As WPChemicals = New WPChemicals(oIDHForm, oForm, sGridID, iLeft, iTop, iWidth, iHeight)
            oGridN.getSBOItem.FromPane = iFromPane
            oGridN.getSBOItem.ToPane = iToPane
            oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
            oGridN.getItem.AffectsFormMode = False

            Return oGridN
        End Function

        Public Function doSetGridParameters( _
          ByVal sTableName As String, _
          ByVal sChmCode As String, _
          ByVal sItemCd As String, _
          ByVal bCanEdit As Boolean, _
          ByVal dDocDate As Date) As Boolean
            'Return doSetGridParameters(sWR1JobType, sJobCode, sJobRow, bCanEdit, dDocDate, True)
            'Return doSetGridParameters("@IDH_TFSMAST", sChmCode, sItemCd, bCanEdit, dDocDate, True)
            Return doSetGridParameters("OITM", sChmCode, sItemCd, bCanEdit, dDocDate, True)
        End Function

        Public Sub doSetCASCodeAndNumber(ByVal sChmCode As String, ByVal sItemCd As String)
            Dim iRow As Integer
            For iRow = 0 To getRowCount() - 1
                doSetFieldValue("U_ChemicalCd", iRow, sChmCode)
                doSetFieldValue("U_ItemCd", iRow, sItemCd)
            Next
        End Sub

        Protected Overrides Function doSendGridEvent(ByVal iEventType As IDHAddOns.idh.events.Base.ev_Types, ByVal bBefore As Boolean, ByVal bActSuccess As Boolean, Optional ByVal sColID As String = Nothing, Optional ByVal iRow As Integer = 0, Optional ByRef oData As Object = Nothing) As Boolean
            If iEventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
                ''Kashif's Code
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, msNumberKey)
                doSetFieldValue("Code", iRow, oNumbers.CodeCode, True)
                doSetFieldValue("Name", iRow, oNumbers.NameCode, True)
            End If
            Return MyBase.doSendGridEvent(iEventType, bBefore, bActSuccess, sColID, iRow, oData)
        End Function

        Private Sub doCopyFromDataHandler(ByVal sChmCode As String)
            moDataTable.Clear()
            Dim oDCol As SAPbouiCOM.DataColumns = moDataTable.Columns
            Dim idx As Integer
            Dim iColIndex As Integer
            For idx = 0 To moGridControl.getListFields().Count - 1
                Dim oField As ListField = moGridControl.getListFields().Item(idx)
                iColIndex = moDataHandler.getColIndex(oField.msFieldName)
                If iColIndex > -1 Then
                    Dim sColName As String = moDataHandler.getColName(iColIndex)
                    Dim oType As SAPbouiCOM.BoFieldsType = moDataHandler.getColType(iColIndex)
                    If oType = SAPbouiCOM.BoFieldsType.ft_NotDefined Then
                        oType = SAPbouiCOM.BoFieldsType.ft_Text
                    End If
                    Dim iSize As Integer = moDataHandler.getColSize(iColIndex)
                    oDCol.Add(sColName, oType, iSize)
                Else
                    oDCol.Add(oField.msFieldName, SAPbouiCOM.BoFieldsType.ft_Text, 10)
                End If
            Next

            'Dim oValue As String
            'Dim gridFieldName As String
            'Dim dataFieldName As String

            'If moDataHandler.getRowCount() > 0 Then
            '    Dim iRow As Integer = 0
            '    For r As Integer = 0 To moDataHandler.getRowCount() - 1
            '        If moDataHandler.wasDeleted(r) = False Then
            '            Dim sChmCodeW As String = moDataHandler.getFieldValue("U_ChemicalCd", r)
            '            If Not sChmCodeW Is Nothing AndAlso sChmCodeW.Equals(sChmCode) Then
            '                moDataTable.Rows.Add(1)
            '                For idx = 0 To moGridControl.getListFields().Count - 1
            '                    gridFieldName = moGridControl.getListFields().Item(idx).msFieldName
            '                    dataFieldName = moDataHandler.getColName(idx)
            '                    iColIndex = moDataHandler.getColIndex(moGridControl.getListFields().Item(idx).msFieldName)
            '                    If iColIndex > -1 Then
            '                        oValue = moDataHandler.getFieldValue(iColIndex, r)
            '                        If Not oValue Is Nothing Then
            '                            If idx = 6 Then 'If moDataHandler.getColIndex("U_ShpDt") = idx Then
            '                                'moDataTable.SetValue(idx, iRow, com.idh.utils.dates.doStrToDate(oValue))
            '                                moDataTable.SetValue(idx, iRow, Convert.ToDateTime(oValue))
            '                            Else
            '                                moDataTable.SetValue(idx, iRow, oValue)
            '                            End If
            '                        End If
            '                    End If
            '                Next
            '                iRow = iRow + 1
            '            End If
            '        End If
            '    Next
            'End If

            'modified code version 
            If moDataHandler.getRowCount() > 0 Then
                Dim iRow As Integer = 0
                For r As Integer = 0 To moDataHandler.getRowCount() - 1
                    If moDataHandler.wasDeleted(r) = False Then
                        Dim sChmCodeW As String = moDataHandler.getFieldValue("U_ChemicalCd", r)
                        If Not sChmCodeW Is Nothing AndAlso sChmCodeW.Equals(sChmCode) Then
                            moDataTable.Rows.Add(1)

                            moDataTable.SetValue("Code", r, moDataHandler.getFieldValue("Code", r))
                            moDataTable.SetValue("Name", r, moDataHandler.getFieldValue("Name", r))
                            moDataTable.SetValue("U_ChemicalCd", r, moDataHandler.getFieldValue("U_ChemicalCd", r))
                            moDataTable.SetValue("U_ChemicalNm", r, moDataHandler.getFieldValue("U_ChemicalNm", r))
                            moDataTable.SetValue("U_Quantity", r, moDataHandler.getFieldValue("U_Quantity", r))
                            moDataTable.SetValue("U_MinQty", r, moDataHandler.getFieldValue("U_MinQty", r))
                            moDataTable.SetValue("U_MaxQty", r, moDataHandler.getFieldValue("U_MaxQty", r))
                            moDataTable.SetValue("U_ItemCd", r, moDataHandler.getFieldValue("U_ItemCd", r))
                            moDataTable.SetValue("U_ItemNm", r, moDataHandler.getFieldValue("U_ItemNm", r))

                            'For idx = 0 To moGridControl.getListFields().Count - 1
                            '    gridFieldName = moGridControl.getListFields().Item(idx).msFieldName
                            '    dataFieldName = moDataHandler.getColName(idx)
                            '    iColIndex = moDataHandler.getColIndex(moGridControl.getListFields().Item(idx).msFieldName)
                            '    If iColIndex > -1 Then
                            '        oValue = moDataHandler.getFieldValue(iColIndex, r)
                            '        If Not oValue Is Nothing Then
                            '            If idx = 6 Then 'If moDataHandler.getColIndex("U_ShpDt") = idx Then
                            '                'moDataTable.SetValue(idx, iRow, com.idh.utils.dates.doStrToDate(oValue))
                            '                moDataTable.SetValue(idx, iRow, Convert.ToDateTime(oValue))
                            '            Else
                            '                moDataTable.SetValue(idx, iRow, oValue)
                            '            End If
                            '        End If
                            '    End If
                            'Next
                            iRow = iRow + 1
                        End If
                    End If
                Next
            End If

            If moDataTable.Rows.Count = 0 Then
                Dim oDRows As SAPbouiCOM.DataRows
                oDRows = moDataTable.Rows
                oDRows.Add(1)
            End If

        End Sub

        Public Function getDataHandler() As WR1_Data.idh.data.Chemicals
            Return moDataHandler
        End Function

        Public Overrides Sub doReloadData()
            doReloadDataFH(msOrderDirection, True, True, Nothing)
        End Sub

        Public Overrides Sub doReloadData(ByVal bDoRules As Boolean)
            doReloadDataFH(msOrderDirection, True, bDoRules, Nothing)
        End Sub

        Public Overrides Sub doReloadData(ByVal sDirection As String, ByVal bAddBlankLine As Boolean)
            doReloadDataFH(sDirection, bAddBlankLine, True, Nothing)
        End Sub

        Public Overrides Sub doReloadData(ByVal sDirection As String, ByVal bAddBlankLine As Boolean, ByVal bDoRules As Boolean)
            doReloadDataFH(sDirection, bAddBlankLine, bDoRules, Nothing)
        End Sub

        Public Sub doReloadDataFH(Optional ByVal sRow As String = Nothing)
            doReloadDataFH(msOrderDirection, True, True, sRow)
        End Sub

        Public Sub doReloadDataFH(ByVal sDirection As String, ByVal bAddBlankLine As Boolean, Optional ByVal bDoRules As Boolean = True, Optional ByVal sRow As String = Nothing)
            moGridForm.Freeze(True)
            Dim sQry As String = "NONE"
            Try
                'IDHAddOns.idh.addon.Base.doDebug("Prepare Query")
                DataHandler.INSTANCE.DebugTickGRID = "Prepare Query"

                Dim bDoCount As Boolean = False
                If mbDoCount Then
                    If mcExpandedState = "N" Then
                        bDoCount = True
                        If doIndexField(COUNTCOLTITLE) < 0 Then
                            doInsertListField("'1000000'", COUNTCOLTITLE, False, COUNTWCOL, "IGNORE", COUNTCOLTITLE, 0)
                        End If
                    Else
                        doRemoveListField(COUNTCOLTITLE)
                    End If
                End If

                If Not moDataHandler Is Nothing Then
                    doCopyFromDataHandler(sRow)
                Else
                    moGridControl.doCreateFilterString()
                    sQry = moGridControl.getQueryString(sDirection, False)

                    'IDHAddOns.idh.addon.Base.doDebug("Do Query")
                    DataHandler.INSTANCE.DebugTickGRID = "Do Query"
                    ExecuteQuery(sQry, bDoCount)
                End If

                doAddEditLine()

                Dim iCount As Integer = moDataTable.Rows.Count
                If bDoRules Then
                    'IDHAddOns.idh.addon.Base.doDebug("Set Rules")
                    DataHandler.INSTANCE.DebugTickGRID = "Set Rules"
                    doApplyRules()
                Else
                    If iCount > 0 Then
                        moGridObj.AutoResizeColumns()
                    End If
                End If

                'IDHAddOns.idh.addon.Base.doDebug("Done")
                DataHandler.INSTANCE.DebugTickGRID = "Done"
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString() & " - " & sQry, "Error reloading the Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", {sQry})
            End Try
            moGridForm.Freeze(False)
        End Sub

        Public Overrides Function doItemEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim bResult As Boolean = MyBase.doItemEvent(pVal, BubbleEvent)
            If pVal.ItemUID.Equals(msGridId) = False Then
                Return False
            End If

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    Dim iCurRow As Integer = getSBOGrid.GetDataTableRowIndex(pVal.Row)
                    If pVal.ItemChanged Then
                    End If
                End If
            End If
            Return bResult
        End Function

        Public Overrides Function doProcessData() As Boolean
            If Not moDataHandler Is Nothing Then
                Return doSetData()
            Else
                Return MyBase.doProcessData()
            End If
        End Function

        Public Overrides Sub doHandleSearchResults(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            MyBase.doHandleSearchResults(oForm, sModalFormType)

            'If sModalFormType = "IDHISRC" Then
            '    Dim sTrg As String = IDHAddOns.idh.forms.Base.getSharedData(getSBOForm(), "TRG")
            '    If sTrg.Equals("DED") Then
            '        doCalcWeight()
            '    End If
            'End If
        End Sub

        'Private Sub doCalcWeight()
        '    Dim sItem As String = doGetFieldValue("U_ItemCd", miCurrentRow)
        '    Dim bUseDefaultWeight As Boolean = Config.INSTANCE.getItemUseDefaultWeight(sItem)
        '    If bUseDefaultWeight Then
        '        Dim dDefWeight As Double = Config.INSTANCE.getItemDefaultWeight(sItem)
        '        Dim dQty As Double = doGetFieldValue("U_Qty", miCurrentRow)
        '        If dQty = 0 Then
        '            doSetFieldValue("U_Qty", miCurrentRow, 1, False, False, True)
        '            dQty = 1
        '        End If

        '        Dim dWeight As Double = dQty * dDefWeight
        '        doSetFieldValue("U_WgtDed", miCurrentRow, dWeight, False, False, True)
        '        doCalculateWeightTotals()
        '    End If
        'End Sub

        'This will Add a row to the DataHandler if the row is not in the Handlers buffer and then set the Handler's data to that of the Grid
        Private Function doSetData() As Boolean
            Dim iRow As Integer
            Dim iWrkRow As Integer
            Dim oData(8) As Object
            Dim iColIndex As Integer
            Dim idx As Integer

            If hasChangedRows() OrElse hasAddedRows() Then
                For iRow = 0 To moDataTable.Rows.Count - 1
                    Dim sCode As String = doGetFieldValue("Code", iRow)
                    'Dim sItemCd As String = doGetFieldValue("U_ItemCd", iRow)
                    If sCode.Length > 0 Then
                        If wasAdded(iRow) OrElse hasChanged(iRow) Then
                            'If sItemCd.Length > 0 Then
                            iWrkRow = moDataHandler.doSetRow(sCode)
                            For idx = 0 To moGridControl.getListFields().Count - 1
                                Dim sFieldName As String = moGridControl.getListFields().Item(idx).msFieldName
                                Dim oValue As Object

                                iColIndex = moDataHandler.getColIndex(sFieldName)
                                If iColIndex > -1 Then
                                    oValue = moDataTable.GetValue(idx, iRow)
                                    moDataHandler.setFieldValue(iColIndex, iWrkRow, oValue)
                                End If
                            Next
                            'End If
                        End If
                    End If
                Next
            End If

            If hasDeletedRows() Then
                Dim sCode As String
                For iRow = 0 To maDeletedRows.Count - 1
                    sCode = maDeletedRows.Item(iRow)
                    moDataHandler.doRemoveRow(sCode)
                Next
            End If
            Return True
        End Function

        Public Function wasAdded(ByVal iRow As Integer) As Boolean
            Return maAddedRows.Contains(iRow)
        End Function

        'Public Function wasDeleted(ByVal iRow As Integer) As Boolean
        '    Return maDeletedRows.Contains(iRow)
        'End Function

        Public Function hasChanged(ByVal iRow As Integer) As Boolean
            If maAddedRows.Count > 0 OrElse mhChangedRowNr.Count > 0 OrElse maDeletedRows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function
#End Region
    End Class
End Namespace
