'
' Created by SharpDevelop.
' User: Louis Viljoen
' Date: 2010/05/26
' Time: 04:17 PM
'
'
Imports IDHAddOns.idh.controls
Imports com.idh.bridge
Imports com.idh.utils
Imports com.idh.bridge.data
Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User
Imports com.idh.dbObjects.numbers
Imports com.idh.bridge.resources

Namespace idh.controls.grid
    Public Class OrderRowGrid
        Inherits IDHAddOns.idh.controls.UpdateGrid
        Private msRowTableAlias As String

        Public msWR1OrdType As String = "WO" 'DO, WO
        Private msHeadTable As String '@..... without the brackets
        Private msRowTable As String '@..... without the brackets

        Private moMarkedForDeleteList As ArrayList

        Public ReadOnly Property JobEntries() As JobEntries
            Get
                Return moJobEntries
            End Get
        End Property
        Private moJobEntries As JobEntries

        Public Property LastJobNumber() As String
            Get
                Return msLastJobNumber
            End Get
            Set
                msLastJobNumber = Value
            End Set
        End Property
        Private msLastJobNumber As String = ""

        Public mbForceRealTimeBilling As Boolean = False

        Public Class DelWOR
            Public sWOR As String
            Public sComment As String
        End Class

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base,
                    ByVal oForm As SAPbouiCOM.Form,
                    ByVal sGridID As String,
                    ByVal sRowTableAlias As String)
            MyBase.New(oIDHForm, oForm, sGridID)
            doSetAlias(sRowTableAlias)
            'doSetRequiredListFields()
        End Sub

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base,
                    ByVal oForm As SAPbouiCOM.Form,
                    ByVal sGridID As String,
                    ByVal iLeft As Integer,
                    ByVal iTop As Integer,
                    ByVal iWidth As Integer,
                    ByVal iHeight As Integer,
                    ByVal sRowTableAlias As String)
            MyBase.New(oIDHForm, oForm, sGridID, iLeft, iTop, iWidth, iHeight)
            doSetAlias(sRowTableAlias)
            'doSetRequiredListFields()
        End Sub

        Public Sub doFinalize()
            doSetRequiredListFields()
        End Sub

        Public Overrides Sub doReloadData(ByVal sDirection As String, ByVal bAddBlankLine As Boolean, ByVal bDoRules As Boolean)
            MyBase.doReloadData(sDirection, bAddBlankLine, bDoRules)
            moMarkedForDeleteList = New ArrayList()
            mbForceRealTimeBilling = False
        End Sub

        Protected Overrides Function doProcessChangedRows() As Boolean
            If MyBase.doProcessChangedRows() Then
                Return doDeletePBIDetails()
            End If
            Return False
        End Function

        Public Function doDeletePBIDetails() As Boolean
            Dim bResult As Boolean = True
            If Not moMarkedForDeleteList Is Nothing AndAlso moMarkedForDeleteList.Count > 0 Then
                Dim oRow As DelWOR
                Dim oPBIDetail As IDH_PBIDTL = New IDH_PBIDTL()
                For iCount As Integer = 0 To moMarkedForDeleteList.Count - 1
                    oRow = moMarkedForDeleteList.Item(iCount)

                    If Not oRow.sWOR Is Nothing AndAlso oRow.sWOR.Length > 0 Then
                        bResult = (bResult And oPBIDetail.doDeleteByWOR(oRow.sWOR, oRow.sComment, False, False))
                    End If
                Next
                IDHAddOns.idh.data.Base.doReleaseObject(oPBIDetail)
            End If
            Return True
        End Function

        Private Sub doSetAlias(ByVal sRowTableAlias As String)
            If Not sRowTableAlias Is Nothing AndAlso
             sRowTableAlias.Length > 0 Then
                msRowTableAlias = sRowTableAlias & "."
            Else
                msRowTableAlias = ""
            End If
        End Sub

        Public Sub doAddImportedWORow(ByVal oWO As IDH_JOBENTR, ByVal sJobNr As String, ByVal sCustCd As String, ByVal sCustNM As String, ByVal sAddress As String, ByVal sZipCode As String, ByVal oRDate As Date, ByVal sRFTime As String, ByVal sTTime As String,
                            ByVal sContainerGrp As String,
                            ByVal sSLicNm As String, ByVal sCustRef As String, ByVal sSupRef As String, ByVal sProRef As String, ByVal sSiteRef As String,
                            ByVal sOrigin As String, ByVal sCarrCd As String, ByVal sCarrNm As String, ByVal sPCardCd As String, ByVal sPCardNM As String, ByVal sSCardCd As String, ByVal sSCardNM As String, ByVal sSAddress As String,
                            ByVal sCntrNo As String,
                            ByVal oImportRow As com.idh.bridge.import.DrumList.ImportRow
                    )


            miCurrentDataRow = getLastRowIndex()
            Dim iNewRow As Integer = doAddDataLine(True)
            doSetRowCountValue(iNewRow)

            Dim bIsCost As Boolean = False
            If oImportRow.CostCharge Is Nothing OrElse
             oImportRow.CostCharge.Trim().Length = 0 OrElse
             oImportRow.CostCharge = "Charge" Then
                bIsCost = False
            Else
                bIsCost = True
            End If

            Dim oWOR As IDH_JOBSHD = New IDH_JOBSHD(oWO)
            oWOR.doApplyDefaults()

            oWOR.BlockChangeNotif = True
            Try

                DataHandler.INSTANCE.doInfo(">>>>> ADDING THE ROW")

                'Dim sLineID As String = oWOR.getNewKey() 'com.idh.utils.Conversions.ToString(DataHandler.INSTANCE.doNextNumber("JOBSCHE"))
                Dim oNumbers As NumbersPair = oWOR.getNewKey()
                oWOR.Code = oNumbers.CodeCode
                oWOR.Name = oNumbers.NameCode
                oWOR.U_BDate = Date.Now()
                oWOR.U_BTime = com.idh.utils.Dates.doTimeStrToIntStr(Nothing)
                oWOR.U_RDate = oRDate

                Dim sTimeF As String = Nothing
                sTimeF = com.idh.utils.Dates.doStrToTimeStr(sTimeF)
                Dim sTimeT As String = Nothing
                sTimeT = com.idh.utils.Dates.doStrToTimeStr(sTimeT)

                If sTimeF.Length < 4 OrElse sTimeF.Equals("00:00") Then
                    sTimeF = Config.ParameterWithDefault("WOTMFR", "8:00")
                End If
                If sTimeT.Length < 4 OrElse sTimeT.Equals("00:00") Then
                    sTimeT = Config.ParameterWithDefault("WOTMTO", "17:00")
                End If

                Dim iTimeF As Integer = com.idh.utils.Dates.doTimeStrToInt(sTimeF)
                Dim iTimeT As Integer = com.idh.utils.Dates.doTimeStrToInt(sTimeT)

                oWOR.U_RTime = iTimeF
                oWOR.U_RTimeT = iTimeT

                oWOR.U_SLicSp = ""
                oWOR.U_SLicNr = ""

                oWOR.U_SLicCh = 0
                oWOR.U_SLicCst = 0
                oWOR.U_SLicCTo = 0

                oWOR.U_SLicNm = sSLicNm
                oWOR.U_SLicCQt = 1
                oWOR.U_IntComm = ""
                oWOR.U_UseWgt = "0"
                oWOR.U_User = IDHAddOns.idh.addon.Base.PARENT.gsUserName
                oWOR.U_Branch = Config.INSTANCE.doGetCurrentBranch()

                oWOR.U_ExpLdWgt = oImportRow.ExpLoad
                oWOR.U_CustRef = sCustRef
                oWOR.U_SupRef = sSupRef
                oWOR.U_ProRef = sProRef
                oWOR.U_SiteRef = sSiteRef
                oWOR.U_WRRow = ""
                oWOR.U_WROrd = ""
                oWOR.U_LnkPBI = ""
                oWOR.U_DPRRef = ""
                oWOR.U_Sched = "N"
                oWOR.U_USched = "Y"
                oWOR.U_Status = FixedValues.getStatusOpen()
                oWOR.U_PStat = FixedValues.getStatusOpen()
                oWOR.U_RowSta = Config.INSTANCE.getImportRowStatus()
                oWOR.U_PayMeth = ""
                oWOR.U_PayStat = com.idh.bridge.lookups.FixedValues.getStatusUnPaid()
                oWOR.U_CCNum = ""
                oWOR.U_CCType = "0"
                oWOR.U_CCStat = ""
                oWOR.U_Obligated = Config.INSTANCE.getBPOligated(sCustCd)
                oWOR.U_WastTNN = ""
                oWOR.U_ExtWeig = ""
                oWOR.U_ConNum = ""
                oWOR.U_CarrReb = "N"
                oWOR.U_CustReb = "N"
                oWOR.U_CustCs = Config.INSTANCE.getBPCs(sCustCd)
                oWOR.U_ItmGrp = sContainerGrp
                oWOR.U_Origin = sOrigin

                'oWOR.U_CustCd = sCustCd
                'oWOR.U_CustNm = Config.INSTANCE.doGetBPName(sCustCd)

                'NOW START WITH THE CALCULATION VALUES
                oWOR.U_WasCd = oImportRow.ItemCode
                oWOR.U_WasDsc = oImportRow.Description

                oWOR.U_UOM = oImportRow.UOM
                oWOR.doMarkUserUpdate(IDH_JOBSHD._UOM)

                oWOR.U_PUOM = oImportRow.UOM
                oWOR.doMarkUserUpdate(IDH_JOBSHD._PUOM)

                oWOR.U_ProUOM = oImportRow.UOM
                oWOR.doMarkUserUpdate(IDH_JOBSHD._ProUOM)

                Dim dWeightFactor As Double = Config.INSTANCE.doGetBaseWeightFactors(oImportRow.UOM)
                If (dWeightFactor <= 0) Then
                    oWOR.U_UseWgt = "2"
                Else
                    oWOR.U_UseWgt = "1"
                End If

                If oImportRow.WasteCarrier IsNot Nothing AndAlso oImportRow.WasteCarrier.Length > 0 Then
                    oWOR.U_CarrCd = oImportRow.WasteCarrier
                    oWOR.U_CarrNm = Config.INSTANCE.doGetBPName(oImportRow.WasteCarrier)
                End If

                If bIsCost Then
                    Dim oLinkedBP As LinkedBP = Config.INSTANCE.doGetLinkedBP(sCustCd)
                    If oLinkedBP IsNot Nothing Then
                        oWOR.U_ProCd = oLinkedBP.LinkedBPCardCode
                        oWOR.U_ProNm = oLinkedBP.LinkedBPName
                    End If
                End If

                If oImportRow.DisposalSite IsNot Nothing AndAlso oImportRow.DisposalSite.Length > 0 Then
                    oWOR.U_Tip = oImportRow.DisposalSite
                    oWOR.U_TipNm = Config.INSTANCE.doGetBPName(oImportRow.DisposalSite)
                End If
                oWOR.U_SAddress = sSAddress

                oWOR.U_CntrNo = sCntrNo
                oWOR.U_JobNr = sJobNr

                Dim sJobTp As String = oImportRow.OrderType
                If sJobTp.Length = 0 Then
                    sJobTp = Config.INSTANCE.getWOImportJobType()
                End If
                oWOR.U_JobTp = sJobTp

                Dim sContainerCd As String = oImportRow.Container
                If sContainerCd.Length = 0 Then
                    sContainerCd = Config.INSTANCE.getWOImportContainer()
                End If
                If sContainerCd.Length = 0 Then
                    sContainerCd = Config.INSTANCE.getDefaultWOContainer()
                End If

                Dim sContainerDesc As String = ""
                If sContainerCd.Length > 0 Then
                    sContainerDesc = com.idh.bridge.lookups.Config.INSTANCE.doGetItemDescription(sContainerCd)
                End If
                oWOR.U_ItemCd = sContainerCd
                oWOR.U_ItemDsc = sContainerDesc

                Dim dContQty As Double = 1 'oImportRow.Qty
                oWOR.U_CusQty = dContQty
                oWOR.doMarkUserUpdate(IDH_JOBSHD._CusQty)

                oWOR.U_HlSQty = dContQty
                oWOR.doMarkUserUpdate(IDH_JOBSHD._HlSQty)

                oWOR.U_OrdWgt = dContQty
                oWOR.doMarkUserUpdate(IDH_JOBSHD._OrdWgt)

                Dim dWeightValue As Double = 0
                Dim dAUOMQty As Double = 0

                If oWOR.U_UseWgt = "2" Then
                    oWOR.U_AUOM = oWOR.U_UOM
                    oWOR.doMarkUserUpdate(IDH_JOBSHD._AUOM)

                    dWeightValue = 0
                    dAUOMQty = oImportRow.Number
                Else
                    oWOR.U_AUOM = Config.INSTANCE.getDefaultAUOM()

                    dWeightValue = oImportRow.Number
                    dAUOMQty = 0
                End If

                If bIsCost = True Then
                    oWOR.U_PAUOMQt = dAUOMQty
                    oWOR.doMarkUserUpdate(IDH_JOBSHD._PAUOMQt)

                    oWOR.U_PRdWgt = dWeightValue
                    oWOR.doMarkUserUpdate(IDH_JOBSHD._PRdWgt)

                    ''oWOR.U_TAUOMQt = dAUOMQty
                    ''oWOR.doMarkUserUpdate(IDH_JOBSHD._TAUOMQt)

                    ''oWOR.U_TRdWgt = dWeightValue
                    ''oWOR.doMarkUserUpdate(IDH_JOBSHD._TRdWgt)

                    'oWOR.U_AUOMQt = 0
                    'oWOR.U_RdWgt = 0
                    'oWOR.U_CstWgt = 0
                    'oWOR.U_TCharge = 0
                Else
                    'oWOR.U_PAUOMQt = 0
                    'oWOR.U_PRdWgt = 0
                    'oWOR.U_PRdWgt = 0
                    'oWOR.U_PCost = 0

                    ''oWOR.U_TAUOMQt = 0
                    ''oWOR.U_TRdWgt = 0

                    oWOR.U_AUOMQt = dAUOMQty
                    oWOR.doMarkUserUpdate(IDH_JOBSHD._AUOMQt)

                    oWOR.U_RdWgt = dWeightValue
                    oWOR.doMarkUserUpdate(IDH_JOBSHD._RdWgt)
                End If

                'The Qty field use to be the AUOM value..
                oWOR.U_TAUOMQt = dAUOMQty
                oWOR.doMarkUserUpdate(IDH_JOBSHD._TAUOMQt)

                oWOR.U_TRdWgt = dWeightValue
                oWOR.doMarkUserUpdate(IDH_JOBSHD._TRdWgt)

                'Ok they confirmed - Do this always - We just have to check the charge when the cost flag is set... as far as I know it should not do a purcase and sell at the same time
                'oWOR.U_AUOMQt = dAUOMQty
                'oWOR.doMarkUserUpdate(IDH_JOBSHD._AUOMQt)

                'oWOR.U_RdWgt = dWeightValue
                'oWOR.doMarkUserUpdate(IDH_JOBSHD._RdWgt)
            Catch ex As Exception
                Throw ex
            Finally
                oWOR.BlockChangeNotif = False
            End Try

            Dim bDoTip As Boolean = False
            Dim bDoHaulage As Boolean = True
            If bIsCost = True Then
                'If oImportRow.DispCost <> -9999 Then
                '    oWOR.U_TipCost = oImportRow.DispCost
                '    oWOR.doMarkUserUpdate(IDH_JOBSHD._TipCost)

                '    oWOR.doCalculateTipCostTotals("S")
                'Else
                '    oWOR.doGetTipCostPrice(True)
                'End If

                If oImportRow.DispCharge <> -9999 Then
                    oWOR.U_PCost = oImportRow.DispCharge
                    oWOR.doMarkUserUpdate(IDH_JOBSHD._PCost)

                    oWOR.doCalculateProducerCostTotals("S"c)
                Else
                    oWOR.doGetProducerCostPrice(True)
                End If

                'If oImportRow.HaulCost <> -9999 Then
                '    oWOR.U_OrdCost = oImportRow.HaulCost
                '    oWOR.doMarkUserUpdate(IDH_JOBSHD._OrdCost)

                '    oWOR.doCalculateHaulageCostTotals("S")
                'Else
                '    oWOR.doGetHaulageCostPrice(True)
                'End If
            Else
                If oImportRow.DispCharge <> -9999 Then
                    oWOR.U_TCharge = oImportRow.DispCharge
                    oWOR.doMarkUserUpdate(IDH_JOBSHD._TCharge)
                    oWOR.doCalculateTipChargeTotals("S"c)
                    bDoTip = False
                Else
                    bDoTip = True
                End If

                'If oImportRow.HaulCharge <> -9999 Then
                '    oWOR.U_CusChr = oImportRow.HaulCharge
                '    oWOR.doMarkUserUpdate(IDH_JOBSHD._CusChr)
                '    oWOR.doCalculateHaulageChargeTotals("S")
                '    bDoHaulage = False
                'End If

                'If bDoTip AndAlso bDoHaulage Then
                '    oWOR.doGetChargePrices(bDoTip, bDoHaulage, True)
                'End If
            End If

            'Do this always
            If oImportRow.DispCost <> -9999 Then
                oWOR.U_TipCost = oImportRow.DispCost
                oWOR.doMarkUserUpdate(IDH_JOBSHD._TipCost)

                oWOR.doCalculateTipCostTotals("S"c)
            Else
                oWOR.doGetTipCostPrice(True)
            End If

            If oImportRow.HaulCost <> -9999 Then
                oWOR.U_OrdCost = oImportRow.HaulCost
                oWOR.doMarkUserUpdate(IDH_JOBSHD._OrdCost)

                oWOR.doCalculateHaulageCostTotals("S"c)
            Else
                oWOR.doGetHaulageCostPrice(True)
            End If

            'If oImportRow.DispCharge <> -9999 Then
            '    oWOR.U_TCharge = oImportRow.DispCharge
            '    oWOR.doMarkUserUpdate(IDH_JOBSHD._TCharge)
            '    oWOR.doCalculateTipChargeTotals("S")
            '    bDoTip = False
            'End If

            If oImportRow.HaulCharge <> -9999 Then
                oWOR.U_CusChr = oImportRow.HaulCharge
                oWOR.doMarkUserUpdate(IDH_JOBSHD._CusChr)
                oWOR.doCalculateHaulageChargeTotals("S"c)
                bDoHaulage = False
            End If

            If bDoTip OrElse bDoHaulage Then
                oWOR.doGetChargePrices(bDoTip, bDoHaulage, True)
            End If

            'Now Add this Row to the Grid
            Dim iIndex As Integer
            Dim iCol As Integer
            Dim oVal As Object = Nothing
            Dim iCols As Integer = moGridControl.getListFields().Count()
            Dim sFieldName As String = Nothing

            For iCol = 0 To iCols - 1
                Try
                    sFieldName = moGridControl.getListFields().Item(iCol).msFieldName
                    iIndex = oWOR.getFieldIndex(sFieldName)
                    If iIndex > -1 Then
                        oVal = oWOR.getValue(sFieldName)
                        If oVal IsNot Nothing Then
                            doSetFieldValue(sFieldName, oVal)
                        End If
                    End If
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the value - " & sFieldName & "=" & oVal)
                    If Not String.IsNullOrEmpty(sFieldName) Or String.IsNullOrEmpty(oVal.ToString()) Then
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSVS", {sFieldName, oVal.ToString()})
                    Else : com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSV", {Nothing})
                    End If
                End Try
            Next
        End Sub

        'Public Sub doAddImportedWORow_Old(ByVal sJobNr As String, ByVal sCustCd As String, ByVal sCustNM As String, ByVal sAddress As String, ByVal sZipCode As String, ByVal oRDate As Date, ByVal sRFTime As String, ByVal sTTime As String, _
        '                    ByVal sContainerGrp As String, _
        '                    ByVal sSLicNm As String, ByVal sCustRef As String, ByVal sSupRef As String, ByVal sProRef As String, ByVal sSiteRef As String, _
        '                    ByVal sOrigin As String, ByVal sCarrCd As String, ByVal sCarrNm As String, ByVal sPCardCd As String, ByVal sPCardNM As String, ByVal sSCardCd As String, ByVal sSCardNM As String, ByVal sSAddress As String, _
        '                    ByVal sCntrNo As String, _
        '                    ByVal oImportRow As com.idh.bridge.import.DrumList.ImportRow _
        '            )

        '    miCurrentRow = getLastRowIndex()
        '    Dim iNewRow As Integer = doAddDataLine(True)
        '    Dim bIsWeight As Boolean = True
        '    doSetRowCountValue(iNewRow)

        '    Dim bIsCost As Boolean = False
        '    If oImportRow.CostCharge Is Nothing OrElse _
        '     oImportRow.CostCharge.Trim().Length = 0 OrElse _
        '     oImportRow.CostCharge = "Charge" Then
        '        bIsCost = False
        '    Else
        '        bIsCost = True
        '    End If

        '    Dim sLineID As String = com.idh.utils.Conversions.ToString(DataHandler.INSTANCE.doNextNumber("JOBSCHE"))
        '    doSetFieldValue(IDH_JOBSHD._Code, sLineID)
        '    doSetFieldValue(IDH_JOBSHD._Name, sLineID)

        '    doSetFieldValue(IDH_JOBSHD._BDate, Date.Now())
        '    doSetFieldValue(IDH_JOBSHD._BTime, com.idh.utils.dates.doTimeStrToIntStr(Nothing))

        '    doSetFieldValue("U_RDate", oRDate)

        '    Dim sTimeF As String = com.idh.utils.dates.doStrToTimeStr(sTimeF)
        '    Dim sTimeT As String = com.idh.utils.dates.doStrToTimeStr(sTimeT)

        '    If sTimeF.Length < 4 OrElse sTimeF.Equals("00:00") Then
        '        sTimeF = Config.ParameterWithDefault("WOTMFR", "8:00")
        '    End If
        '    If sTimeT.Length < 4 OrElse sTimeT.Equals("00:00") Then
        '        sTimeT = Config.ParameterWithDefault("WOTMTO", "17:00")
        '    End If

        '    Dim iTimeF As Integer = com.idh.utils.dates.doTimeStrToInt(sTimeF)
        '    Dim iTimeT As Integer = com.idh.utils.dates.doTimeStrToInt(sTimeT)

        '    doSetFieldValue(IDH_JOBSHD._RTime, iTimeF)
        '    doSetFieldValue(IDH_JOBSHD._RTimeT, iTimeT)

        '    doSetFieldValue(IDH_JOBSHD._SLicSp, "") 'getFormDFValue(oForm, "U_SLicSp"))
        '    doSetFieldValue(IDH_JOBSHD._SLicNr, "") 'getFormDFValue(oForm, "U_SLicNr"))
        '    'doSetFieldValue("U_SLicExp", com.idh.utils.dates.doStrToDate(getFormDFValue(oForm, "U_SLicExp")))

        '    If getRowCount() > 1 Then
        '        doSetFieldValue(IDH_JOBSHD._SLicCh, "0") '.GetValue("U_SLicCh", .Offset).Trim())
        '        doSetFieldValue(IDH_JOBSHD._SLicCst, "0") '.GetValue("U_SLicCh", .Offset).Trim())
        '        doSetFieldValue(IDH_JOBSHD._SLicCTo, "0") '.GetValue("U_SLicCh", .Offset).Trim())
        '    Else
        '        doSetFieldValue(IDH_JOBSHD._SLicCh, 0)
        '        doSetFieldValue(IDH_JOBSHD._SLicCst, 0)
        '        doSetFieldValue(IDH_JOBSHD._SLicCTo, 0)

        '        'Dim iJobIndex As Integer = User.IDH_JOBTYPE_SEQ.getFirstJob(Config.CAT_WO, sIGrp)
        '        'If iJobIndex > -1 Then
        '        '    Dim oJobSEQ As User.IDH_JOBTYPE_SEQ = User.IDH_JOBTYPE_SEQ.getInstance()
        '        '    oJobSEQ.gotoRow(iJobIndex)
        '        '    doSetFieldValue("U_JobTp", oJobSEQ.U_JobTp)
        '        'End If
        '    End If

        '    doSetFieldValue(IDH_JOBSHD._SLicNm, sSLicNm)
        '    doSetFieldValue(IDH_JOBSHD._SLicCQt, 1)
        '    doSetFieldValue(IDH_JOBSHD._IntComm, "")
        '    doSetFieldValue(IDH_JOBSHD._UseWgt, "0")

        '    ''Set the congestion charge
        '    'Dim dCongCharge As Double = Config.INSTANCE.doGetCongestion(goParent, sCustomer, getFormDFValue(oForm, "U_ZpCd"), getFormDFValue(oForm, "U_Address"))
        '    'If dCongCharge <> 0 Then
        '    '    Dim sCongSupplier As String = Config.Parameter("SPCONG")
        '    '    Dim sCongName As String = Config.INSTANCE.doGetBPName(sCongSupplier)

        '    '    doSetFieldValue(IDH_JOBSHD._CongCd, sCongSupplier)
        '    '    doSetFieldValue(IDH_JOBSHD._SCngNm, sCongName)
        '    'End If
        '    'doSetFieldValue(IDH_JOBSHD._CongCh, dCongCharge)

        '    doSetFieldValue(IDH_JOBSHD._User, IDHAddOns.idh.addon.Base.PARENT.gsUserName)

        '    Dim sBranch As String = Config.INSTANCE.doGetCurrentBranch()
        '    doSetFieldValue(IDH_JOBSHD._Branch, sBranch)

        '    doSetFieldValue(IDH_JOBSHD._UOM, oImportRow.UOM)
        '    doSetFieldValue(IDH_JOBSHD._PUOM, oImportRow.UOM)
        '    doSetFieldValue(IDH_JOBSHD._ProUOM, oImportRow.UOM)
        '    If Config.INSTANCE.doGetBaseWeightFactors(oImportRow.UOM) < 0 Then
        '        bIsWeight = False
        '    End If

        '    doSetFieldValue(IDH_JOBSHD._ExpLdWgt, oImportRow.ExpLoad)

        '    doSetFieldValue(IDH_JOBSHD._WasCd, oImportRow.ItemCode)
        '    doSetFieldValue(IDH_JOBSHD._WasDsc, oImportRow.Description)

        '    doSetFieldValue(IDH_JOBSHD._CustRef, sCustRef)
        '    doSetFieldValue(IDH_JOBSHD._SupRef, sSupRef)

        '    doSetFieldValue(IDH_JOBSHD._ProRef, sProRef)
        '    doSetFieldValue(IDH_JOBSHD._SiteRef, sSiteRef)

        '    doSetFieldValue(IDH_JOBSHD._WRRow, "")
        '    doSetFieldValue(IDH_JOBSHD._WROrd, "")

        '    doSetFieldValue(IDH_JOBSHD._LnkPBI, "")
        '    doSetFieldValue(IDH_JOBSHD._DPRRef, "")

        '    doSetFieldValue(IDH_JOBSHD._Sched, "N")
        '    doSetFieldValue(IDH_JOBSHD._USched, "Y")

        '    doSetFieldValue(IDH_JOBSHD._Status, FixedValues.getStatusOpen())
        '    doSetFieldValue(IDH_JOBSHD._PStat, FixedValues.getStatusOpen())
        '    doSetFieldValue(IDH_JOBSHD._RowSta, Config.INSTANCE.getImportRowStatus())

        '    doSetFieldValue(IDH_JOBSHD._PayMeth, "")
        '    doSetFieldValue(IDH_JOBSHD._PayStat, com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
        '    doSetFieldValue(IDH_JOBSHD._CCNum, "")
        '    doSetFieldValue(IDH_JOBSHD._CCType, "0")
        '    doSetFieldValue(IDH_JOBSHD._CCStat, "")

        '    'Get the BP Oligation
        '    Dim sOlig As String = Config.INSTANCE.getBPOligated(sCustCd)
        '    doSetFieldValue("U_Obligated", sOlig)

        '    doSetFieldValue(IDH_JOBSHD._WastTNN, "")
        '    doSetFieldValue(IDH_JOBSHD._ExtWeig, "")

        '    doSetFieldValue(IDH_JOBSHD._ConNum, "")
        '    doSetFieldValue(IDH_JOBSHD._CarrReb, "N")
        '    doSetFieldValue(IDH_JOBSHD._CustReb, "N")

        '    Dim sCustCs As String = Config.INSTANCE.getBPCs(sCustCd)
        '    doSetFieldValue(IDH_JOBSHD._CustCs, sCustCs)

        '    doSetFieldValue(IDH_JOBSHD._ItmGrp, sContainerGrp)

        '    doSetFieldValue(IDH_JOBSHD._Origin, sOrigin)
        '    doSetFieldValue(IDH_JOBSHD._CustCd, sCustCd)
        '    doSetFieldValue(IDH_JOBSHD._CustNm, sCustCd)

        '    If oImportRow.WasteCarrier Is Nothing OrElse oImportRow.WasteCarrier.Length = 0 Then
        '        doSetFieldValue(IDH_JOBSHD._CarrCd, sCarrCd)
        '        doSetFieldValue(IDH_JOBSHD._CarrNm, sCarrNm)
        '    Else
        '        doSetFieldValue(IDH_JOBSHD._CarrCd, oImportRow.WasteCarrier)
        '        doSetFieldValue(IDH_JOBSHD._CarrNm, Config.INSTANCE.doGetBPName(oImportRow.WasteCarrier))
        '    End If

        '    If bIsCost Then
        '        Dim oLinkedBP As LinkedBP = Config.INSTANCE.doGetLinkedBP(sCustCd)
        '        If oLinkedBP Is Nothing Then
        '            doSetFieldValue(IDH_JOBSHD._ProCd, sPCardCd)
        '            doSetFieldValue(IDH_JOBSHD._ProNm, sPCardNM)
        '            doSetFieldValue(IDH_JOBSHD._ProUOM, Config.INSTANCE.getDefaultUOM())
        '        Else
        '            doSetFieldValue(IDH_JOBSHD._ProCd, oLinkedBP.LinkedBPCardCode)
        '            doSetFieldValue(IDH_JOBSHD._ProNm, oLinkedBP.LinkedBPName)
        '            doSetFieldValue(IDH_JOBSHD._ProUOM, oLinkedBP.UOM)
        '        End If
        '    Else
        '        doSetFieldValue(IDH_JOBSHD._ProCd, sPCardCd)
        '        doSetFieldValue(IDH_JOBSHD._ProNm, sPCardNM)
        '        doSetFieldValue(IDH_JOBSHD._ProUOM, Config.INSTANCE.getDefaultUOM())
        '    End If

        '    If oImportRow.DisposalSite Is Nothing OrElse oImportRow.DisposalSite.Length = 0 Then
        '        doSetFieldValue(IDH_JOBSHD._Tip, sSCardCd)
        '        doSetFieldValue(IDH_JOBSHD._TipNm, sSCardNM)
        '        doSetFieldValue(IDH_JOBSHD._SAddress, sSAddress)
        '    Else
        '        doSetFieldValue(IDH_JOBSHD._Tip, oImportRow.DisposalSite)
        '        doSetFieldValue(IDH_JOBSHD._TipNm, Config.INSTANCE.doGetBPName(oImportRow.DisposalSite))
        '    End If
        '    doSetFieldValue(IDH_JOBSHD._SAddress, sSAddress)

        '    doSetFieldValue(IDH_JOBSHD._CntrNo, sCntrNo)
        '    doSetFieldValue(IDH_JOBSHD._JobNr, sJobNr)

        '    Dim sJobTp As String = ""
        '    Dim sContainerCd As String = ""
        '    Dim sContainerDesc As String = ""

        '    sJobTp = oImportRow.OrderType
        '    If sJobTp.Length = 0 Then
        '        sJobTp = Config.INSTANCE.getWOImportJobType()
        '    End If
        '    doSetFieldValue(IDH_JOBSHD._JobTp, sJobTp)

        '    sContainerCd = oImportRow.Container
        '    If sContainerCd.Length = 0 Then
        '        sContainerCd = Config.INSTANCE.getWOImportContainer()
        '    End If
        '    If sContainerCd.Length = 0 Then
        '        sContainerCd = Config.INSTANCE.getDefaultWOContainer()
        '    End If
        '    If sContainerCd.Length > 0 Then
        '        sContainerDesc = com.idh.bridge.lookups.Config.INSTANCE.doGetItemDescription(sContainerCd)
        '    End If
        '    doSetFieldValue(IDH_JOBSHD._ItemCd, sContainerCd)
        '    doSetFieldValue(IDH_JOBSHD._ItemDsc, sContainerDesc)

        '    doSetFieldValue(IDH_JOBSHD._AUOM, Config.INSTANCE.getDefaultUOM())

        '    Dim dDisposalChrgTotal As Double = 0
        '    Dim dDisposalCstTotal As Double = 0
        '    Dim dProducerTotal As Double = 0
        '    '            Dim sVatGroup As String
        '    '            Dim dVatRate As Double

        '    Dim dVat As Double
        '    Dim sVatTChrgGroup As String = ""
        '    Dim dVatTChrgRate As Double = 0
        '    Dim sVatHChrgGroup As String = ""
        '    Dim dVatHChrgRate As Double = 0

        '    Dim sVatTCstGroup As String = ""
        '    Dim dVatTCstRate As Double = 0
        '    Dim sVatHCstGroup As String = ""
        '    Dim dVatHCstRate As Double = 0
        '    Dim sVatPCstGroup As String = ""
        '    Dim dVatPCstRate As Double = 0

        '    Dim dHaulageChrgTotal As Double = 0
        '    Dim dHaulageCstTotal As Double = 0
        '    Dim dVatChrgTotal As Double = 0
        '    Dim dVatCstTotal As Double = 0

        '    Dim dContQty As Double = 1
        '    Dim dHaulageCost As Double = oImportRow.HaulCost

        '    If bIsCost = False Then
        '        doSetFieldValue(IDH_JOBSHD._PAUOMQt, 0)
        '        doSetFieldValue(IDH_JOBSHD._PRdWgt, 0)
        '        doSetFieldValue(IDH_JOBSHD._ProWgt, 0)

        '        doSetFieldValue(IDH_JOBSHD._AUOMQt, oImportRow.Qty)
        '        doSetFieldValue(IDH_JOBSHD._RdWgt, oImportRow.Number) 'ExpLoad)

        '        Dim dCustWgt As Double = oImportRow.Number
        '        Dim sTipChargCalcType As String = ""
        '        If oImportRow.DispCharge = -9999 OrElse oImportRow.HaulCharge = -9999 Then
        '            Dim oPrices As com.idh.dbObjects.User.Price 'ArrayList
        '            If sCustCd.Length > 0 AndAlso sContainerCd.Length > 0 Then
        '                oPrices = Config.INSTANCE.doGetJobChargePrices("WO", sJobTp, sContainerGrp, sContainerCd, sCustCd, dCustWgt, oImportRow.UOM, oImportRow.ItemCode, sAddress, oRDate, sBranch, sZipCode)

        '                If oImportRow.HaulCharge = -9999 Then
        '                    oImportRow.HaulCharge = oPrices.mdHaulPrice
        '                End If

        '                If oImportRow.DispCharge = -9999 Then
        '                    oImportRow.DispCharge = oPrices.mdTipPrice
        '                End If

        '                dVatTChrgRate = oPrices.mdTipVat
        '                sVatTChrgGroup = oPrices.msTipVatGroup

        '                dVatHChrgRate = oPrices.mdHaulageVat
        '                sVatHChrgGroup = oPrices.msHaulageVatGroup

        '                sTipChargCalcType = oPrices.msTipChargeCalc.ToUpper()
        '                If sTipChargCalcType.Equals("OFFSET") Then
        '                    If oPrices.mdTipOffsetWeight < dCustWgt Then
        '                        dCustWgt = dCustWgt - oPrices.mdTipOffsetWeight
        '                    End If
        '                End If
        '            End If
        '        End If
        '        doSetFieldValue(IDH_JOBSHD._CstWgt, dCustWgt)
        '        doSetFieldValue(IDH_JOBSHD._TCharge, oImportRow.DispCharge)

        '        If sTipChargCalcType.Equals("FIXED") Then
        '            If dCustWgt = 0 Then
        '                dDisposalChrgTotal = 0
        '            Else
        '                dDisposalChrgTotal = oImportRow.DispCharge
        '            End If
        '        Else
        '            dDisposalChrgTotal = oImportRow.Number * oImportRow.DispCharge
        '        End If

        '        doSetFieldValue(IDH_JOBSHD._PCost, 0)
        '        dProducerTotal = 0
        '    Else
        '        doSetFieldValue(IDH_JOBSHD._AUOMQt, 0)
        '        doSetFieldValue(IDH_JOBSHD._RdWgt, 0)
        '        doSetFieldValue(IDH_JOBSHD._CstWgt, 0)

        '        doSetFieldValue(IDH_JOBSHD._TCharge, 0)
        '        dDisposalChrgTotal = 0

        '        Dim sPCostCalcType As String = ""
        '        Dim dProdWgt As Double = oImportRow.Number

        '        doSetFieldValue(IDH_JOBSHD._PAUOMQt, oImportRow.Qty)
        '        doSetFieldValue(IDH_JOBSHD._PRdWgt, dProdWgt)
        '        If oImportRow.DispCharge = -9999 Then
        '            Dim oPrices As com.idh.dbObjects.User.Price
        '            If sPCardCd.Length > 0 AndAlso sContainerCd.Length > 0 Then
        '                oPrices = Config.INSTANCE.doGetJobCostPrice("WO", sJobTp, sContainerGrp, sContainerCd, sPCardCd, dProdWgt, oImportRow.UOM, oImportRow.ItemCode, sCustCd, sAddress, oRDate, sBranch, sZipCode)

        '                oImportRow.DispCharge = oPrices.mdTipPrice

        '                dVatPCstRate = oPrices.mdTipVat
        '                sVatPCstGroup = oPrices.msTipVatGroup

        '                sPCostCalcType = oPrices.msTipChargeCalc.ToUpper()
        '                If sPCostCalcType.Equals("OFFSET") Then
        '                    If oPrices.mdTipOffsetWeight < dProdWgt Then
        '                        dProdWgt = dProdWgt - oPrices.mdTipOffsetWeight
        '                    End If
        '                End If
        '            End If
        '        End If
        '        doSetFieldValue(IDH_JOBSHD._ProWgt, dProdWgt)
        '        doSetFieldValue(IDH_JOBSHD._PCost, oImportRow.DispCharge)

        '        If sPCostCalcType.Equals("FIXED") Then
        '            If dProdWgt = 0 Then
        '                dProducerTotal = 0
        '            Else
        '                dProducerTotal = oImportRow.DispCharge
        '            End If
        '        Else
        '            dProducerTotal = dProdWgt * oImportRow.DispCharge
        '        End If
        '    End If

        '    Dim sTCostCalcType As String = ""
        '    Dim dTipWgt As Double = oImportRow.Number

        '    doSetFieldValue(IDH_JOBSHD._TRdWgt, dTipWgt)
        '    'doSetFieldValue(IDH_JOBSHD._TAUOMQt, 
        '    If oImportRow.DispCost = -9999 Then
        '        Dim oPrices As com.idh.dbObjects.User.Price
        '        If sSCardCd.Length > 0 AndAlso sContainerCd.Length > 0 Then
        '            oPrices = Config.INSTANCE.doGetJobCostPrice("WO", sJobTp, sContainerGrp, sContainerCd, sSCardCd, dTipWgt, oImportRow.UOM, oImportRow.ItemCode, sCustCd, sAddress, oRDate, sBranch, sZipCode)

        '            oImportRow.DispCost = oPrices.mdTipPrice

        '            dVatTCstRate = oPrices.mdTipVat
        '            sVatTCstGroup = oPrices.msTipVatGroup

        '            sTCostCalcType = oPrices.msTipChargeCalc.ToUpper()
        '            If sTCostCalcType.Equals("OFFSET") Then
        '                If oPrices.mdTipOffsetWeight < dTipWgt Then
        '                    dTipWgt = dTipWgt - oPrices.mdTipOffsetWeight
        '                End If
        '            End If
        '        End If
        '    End If
        '    doSetFieldValue(IDH_JOBSHD._TipWgt, dTipWgt)
        '    doSetFieldValue(IDH_JOBSHD._TipCost, oImportRow.DispCost)

        '    If sTCostCalcType.Equals("FIXED") Then
        '        If dTipWgt = 0 Then
        '            dDisposalCstTotal = 0
        '        Else
        '            dDisposalCstTotal = oImportRow.DispCost
        '        End If
        '    Else
        '        dDisposalCstTotal = dTipWgt * oImportRow.DispCost
        '    End If

        '    '           	doSetFieldValue(IDH_JOBSHD._TRdWgt, oImportRow.Number)
        '    '            If oImportRow.DispCost = -9999 Then
        '    '            	oImportRow.DispCost = 0
        '    '            	doSetFieldValue(IDH_JOBSHD._TipWgt, 0)
        '    '            Else
        '    '            	doSetFieldValue(IDH_JOBSHD._TRdWgt, oImportRow.Number)
        '    '            	doSetFieldValue(IDH_JOBSHD._TipWgt, oImportRow.Number)
        '    '            End If
        '    '            doSetFieldValue(IDH_JOBSHD._TipCost, oImportRow.DispCost)
        '    '            dDisposalCstTotal = oImportRow.Number * oImportRow.DispCost

        '    'Haulage Charge
        '    doSetFieldValue(IDH_JOBSHD._CusQty, dContQty)
        '    doSetFieldValue(IDH_JOBSHD._HlSQty, dContQty)
        '    doSetFieldValue(IDH_JOBSHD._CusChr, oImportRow.HaulCharge)
        '    dHaulageChrgTotal = dContQty * oImportRow.HaulCharge

        '    'Haulage Cost Values
        '    doSetFieldValue(IDH_JOBSHD._OrdWgt, dContQty)
        '    'Dim sHChargeCalcType As String = ""
        '    If oImportRow.HaulCost = -9999 Then
        '        Dim oPrices As com.idh.dbObjects.User.Price 'ArrayList
        '        If sCarrCd.Length > 0 AndAlso sContainerCd.Length > 0 Then
        '            oPrices = Config.INSTANCE.doGetJobCostPrice("WO", sJobTp, sContainerGrp, sContainerCd, sCarrCd, dContQty, oImportRow.UOM, oImportRow.ItemCode, sCustCd, sAddress, oRDate, sBranch, sZipCode)

        '            oImportRow.HaulCost = oPrices.mdHaulPrice

        '            dVatHCstRate = oPrices.mdHaulageVat
        '            sVatHCstGroup = oPrices.msHaulageVatGroup

        '            'sHChargeCalcType = oPrices.msHaulageChargeCalc.ToUpper()
        '        End If
        '    End If
        '    doSetFieldValue(IDH_JOBSHD._OrdCost, oImportRow.HaulCost)
        '    dHaulageCstTotal = dContQty * oImportRow.HaulCost

        '    doSetFieldValue(IDH_JOBSHD._TipTot, dDisposalCstTotal)
        '    doSetFieldValue(IDH_JOBSHD._OrdTot, dHaulageCstTotal)
        '    doSetFieldValue(IDH_JOBSHD._PCTotal, dProducerTotal)

        '    'The Disposal Cost Vat Settings
        '    If dDisposalCstTotal = 0 AndAlso dProducerTotal = 0 Then
        '        doSetFieldValue(IDH_JOBSHD._TCostVtGrp, "")
        '        doSetFieldValue(IDH_JOBSHD._TCostVtRt, 1)
        '        dVatCstTotal = 0
        '    Else
        '        If sVatTCstGroup.Length = 0 Then
        '            sVatTCstGroup = Config.INSTANCE.doGetPurchaceVatGroup(sSCardCd, oImportRow.ItemCode)
        '        End If
        '        doSetFieldValue(IDH_JOBSHD._TCostVtGrp, sVatTCstGroup)

        '        If sVatTCstGroup.Length > 0 AndAlso dVatTCstRate = 0 Then
        '            dVatTCstRate = Config.INSTANCE.doGetVatRate(sVatTCstGroup, DateTime.Now)
        '        End If
        '        doSetFieldValue(IDH_JOBSHD._TCostVtRt, dVatTCstRate)

        '        If dVatTCstRate = 0 Then
        '            dVat = 1
        '        Else
        '            dVat = (dVatTCstRate / 100)
        '        End If
        '        dVatCstTotal = ((dDisposalCstTotal + dProducerTotal) * dVat)
        '    End If

        '    'The Haulage Cost Vat Settings
        '    If dHaulageCstTotal = 0 Then
        '        doSetFieldValue(IDH_JOBSHD._HCostVtGrp, "")
        '        doSetFieldValue(IDH_JOBSHD._HCostVtRt, 1)
        '    Else
        '        If sVatHCstGroup.Length = 0 Then
        '            sVatHCstGroup = Config.INSTANCE.doGetPurchaceVatGroup(sCarrCd, sContainerCd)
        '        End If
        '        doSetFieldValue(IDH_JOBSHD._HCostVtGrp, sVatHCstGroup)

        '        If sVatHCstGroup.Length > 0 AndAlso dVatHCstRate = 0 Then
        '            dVatHCstRate = Config.INSTANCE.doGetVatRate(sVatHCstGroup, DateTime.Now)
        '        End If
        '        doSetFieldValue(IDH_JOBSHD._HCostVtRt, dVatHCstRate)

        '        If dVatHCstRate = 0 Then
        '            dVat = 1
        '        Else
        '            dVat = (dVatHCstRate / 100)
        '        End If
        '        dVatCstTotal = dVatCstTotal + (dHaulageCstTotal * dVat)
        '    End If

        '    doSetFieldValue(IDH_JOBSHD._VtCostAmt, dVatCstTotal)
        '    doSetFieldValue(IDH_JOBSHD._JCost, dDisposalCstTotal + dHaulageCstTotal + dProducerTotal + dVatCstTotal)


        '    doSetFieldValue(IDH_JOBSHD._TCTotal, dDisposalChrgTotal)
        '    doSetFieldValue(IDH_JOBSHD._Price, dHaulageChrgTotal)
        '    doSetFieldValue(IDH_JOBSHD._DisAmt, 0)

        '    'The Tipping Charge Vat Settings
        '    If dDisposalChrgTotal = 0 Then
        '        doSetFieldValue(IDH_JOBSHD._TChrgVtGrp, "")
        '        doSetFieldValue(IDH_JOBSHD._TChrgVtRt, 1)
        '        dVatChrgTotal = 0
        '    Else
        '        If sVatTChrgGroup.Length = 0 Then
        '            sVatTChrgGroup = Config.INSTANCE.doGetSalesVatGroup(sCustCd, oImportRow.ItemCode)
        '        End If
        '        doSetFieldValue(IDH_JOBSHD._TChrgVtGrp, sVatTChrgGroup)

        '        If sVatTChrgGroup.Length > 0 AndAlso dVatTChrgRate = 0 Then
        '            dVatTChrgRate = Config.INSTANCE.doGetVatRate(sVatTChrgGroup, DateTime.Now)
        '        End If
        '        doSetFieldValue(IDH_JOBSHD._TChrgVtRt, dVatTChrgRate)

        '        If dVatTChrgRate = 0 Then
        '            dVat = 1
        '        Else
        '            dVat = (dVatTChrgRate / 100)
        '        End If
        '        dVatChrgTotal = (dDisposalChrgTotal * dVat)
        '    End If

        '    'The Haulage Charge Vat Settings
        '    If dHaulageChrgTotal = 0 Then
        '        doSetFieldValue(IDH_JOBSHD._HChrgVtGrp, "")
        '        doSetFieldValue(IDH_JOBSHD._HChrgVtRt, 1)
        '    Else
        '        If sVatHChrgGroup.Length = 0 Then
        '            sVatHChrgGroup = Config.INSTANCE.doGetSalesVatGroup(sCustCd, sContainerCd)
        '        End If
        '        doSetFieldValue(IDH_JOBSHD._HChrgVtGrp, sVatHChrgGroup)

        '        If sVatHChrgGroup.Length > 0 AndAlso dVatHChrgRate = 0 Then
        '            dVatHChrgRate = Config.INSTANCE.doGetVatRate(sVatHChrgGroup, DateTime.Now)
        '        End If
        '        doSetFieldValue(IDH_JOBSHD._HChrgVtRt, dVatHChrgRate)

        '        If dVatHChrgRate = 0 Then
        '            dVat = 1
        '        Else
        '            dVat = (dVatHChrgRate / 100)
        '        End If
        '        dVatChrgTotal = dVatChrgTotal + (dHaulageChrgTotal * dVat)
        '    End If

        '    doSetFieldValue(IDH_JOBSHD._TaxAmt, dVatChrgTotal)
        '    doSetFieldValue(IDH_JOBSHD._Total, dDisposalChrgTotal + dHaulageChrgTotal + dVatChrgTotal)
        'End Sub

        '**
        ' This is the fields that must always be included to the ListFields
        '	The placement, width +++ will be overriden if the are added in the doSetListFields Function
        '**
        Private Sub doSetRequiredListFields()
            doAddListFieldNotPinned(msRowTableAlias & "U_JobNr", "Order No.", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "Code", "Row No.", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_RowSta", "Row Status", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_Status", "Sales Status", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_PStat", "Purchase Status", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_WROrd", "WO No.", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_WRRow", "WO Row No.", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_ActComm", "Active Status", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned("CAST(((" & msRowTableAlias & "U_TCTotal + " & msRowTableAlias & "U_Price) - " & msRowTableAlias & "U_DisAmt) As Numeric(19,2))", "Sub After Discount", False, 0, Nothing, "SubAftDisc")

            'Need this to calculate the totals
            doAddListFieldNotPinned(msRowTableAlias & "U_CstWgt", "Waste Qty", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_Total", "Total Charge", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_AddEx", "Additional Expenses", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_TCTotal", "Tip Total", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_Price", "Haulage Total", False, 0, Nothing, Nothing)

            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._TipTot, "Disposal Cost Total", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._OrdTot, "Haulage Cost Total", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._PCTotal, "Producer Cost Total", False, 0, Nothing, Nothing)

            'Needed for the job sequencing
            doAddListFieldNotPinned(msRowTableAlias & "U_RDate", "Request Date", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_ItmGrp", "Item Group", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_JobTp", "Job Type", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_CusQty", "Container Qty", False, 0, Nothing, Nothing)

            doAddListFieldNotPinned(msRowTableAlias & "U_ItmGrp", "Container Grp", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_ItemCd", "Container", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & "U_ItemDsc", "Desc", False, 0, Nothing, Nothing)

            'PBI Details
            doAddListFieldNotPinned(msRowTableAlias & "U_LnkPBI", "Linked PBI", False, 0, Nothing, Nothing)

            'Stock
            doAddListFieldNotPinned(msRowTableAlias & "U_BookIn", "Do Stock Movement", False, 0, Nothing, Nothing)

            'Marketing Documents
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._SAINV, "Sales Invoice", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._SLPO, "Skip licence PO", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._SAORD, "Sales Order", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._GRIn, "Goods Receipt Incomming", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._TIPPO, "Tipping PO", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._JOBPO, "Haulage PO", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._ProPO, "Producer PO", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._CustGR, "Customer Goods Receipt", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._ProGRPO, "Producer Goods Receipt PO", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._SODlvNot, "Do Stock Movement", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._Jrnl, "Journal Entry", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._JStat, "Journal Status", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._TPCN, "Disposal Purchase Credit Note", False, 0, Nothing, Nothing)
            doAddListFieldNotPinned(msRowTableAlias & IDH_JOBSHD._TCCN, "Customer Credit Note", False, 0, Nothing, Nothing)
        End Sub

        '**
        ' Build the List of Job entries
        '*
        Public Sub doBuildJobEntries(ByVal sOrderType As String)
            If sOrderType = "WO" Then
                sOrderType = Config.CAT_WO
            ElseIf sOrderType = "DO" Then
                sOrderType = Config.CAT_DO
            End If
            moJobEntries = New JobEntries()

            Dim iCount As Integer = getRowCount()

            If iCount > 0 Then
                For iRow As Integer = 0 To iCount - 1
                    Dim sJobNumber As String = doGetFieldValue(msRowTableAlias & "U_JobNr", iRow) 'moDataTable.GetValue(doIndexField(msRowTableAlias & "U_JobNr"), iRow)
                    If sJobNumber.Length > 0 Then
                        Dim sRowStatus As String = doGetFieldValue((msRowTableAlias & "U_RowSta"), iRow)
                        If sRowStatus.Equals(FixedValues.getStatusDeleted()) = False Then
                            Dim sRowNumber As String = doGetFieldValue((msRowTableAlias & "Code"), iRow)
                            Dim oReqDate As DateTime = doGetFieldValue((msRowTableAlias & "U_RDate"), iRow) 'moDataTable.GetValue(doIndexField(msRowTableAlias & "U_RDate"), iRow)
                            Dim oAEDate As DateTime = doGetFieldValue((msRowTableAlias & "U_AEDate"), iRow)
                            Dim sItemGroup As String = doGetFieldValue(msRowTableAlias & "U_ItmGrp", iRow) 'moDataTable.GetValue(doIndexField(msRowTableAlias & "U_ItmGrp"), iRow)
                            Dim sContainerCode As String = doGetFieldValue(msRowTableAlias & "U_ItemCd", iRow) 'moDataTable.GetValue(doIndexField(msRowTableAlias & "U_ItemCd"), iRow)
                            Dim sContainerName As String = doGetFieldValue(msRowTableAlias & "U_ItemDsc", iRow) 'moDataTable.GetValue(doIndexField(msRowTableAlias & "U_ItemDsc"), iRow)
                            Dim sJobType As String = doGetFieldValue(msRowTableAlias & "U_JobTp", iRow) ' moDataTable.GetValue(doIndexField(msRowTableAlias & "U_JobTp"), iRow)
                            'Dim dQty As Double = doGetFieldValue(msRowTableAlias & "U_CusQty", iRow) 'moDataTable.GetValue(doIndexField(msRowTableAlias & "U_CusQty"), iRow)
                            Dim dQty As Double = doGetFieldValue(msRowTableAlias & "U_CusQty", iRow) 'moDataTable.GetValue(doIndexField(msRowTableAlias & "U_CusQty"), iRow)

                            moJobEntries.doAddEntry(sJobNumber, sRowNumber, oReqDate, oAEDate, sOrderType,
                                    sItemGroup, sJobType, sContainerCode, sContainerName, dQty)

                            'Capture the Last Record values
                            msLastJobNumber = sJobNumber
                        End If
                    End If
                Next
            End If
        End Sub

        '**
        ' Check to see if the last row can close the Header
        '**
        Public Function doCheckCloseOrder() As Boolean
            If moJobEntries Is Nothing Then
                Return False
            End If

            Dim bCheckOnsite As Boolean = Config.INSTANCE.getParameterAsBool("WFCHKONS", True)
            If bCheckOnsite = False Then
                Return False
            Else
                Return moJobEntries.checkIfClosed()
            End If
        End Function

        '**
        ' Build the List of Job entries from possible multiple Orders
        '*
        Public Sub doBuildJobEntriesFromMultiOrders(ByVal sOrderType As String)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim iRow As Integer
                    Dim iSRow As Integer
                    Dim oaRemove As ArrayList = Nothing
                    Dim bFound As Boolean = False
                    Dim sStatus As String

                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iSRow = oSelected.Item(iIndex, SAPbouiCOM.BoOrderType.ot_RowOrder)
                        iRow = getSBOGrid().GetDataTableRowIndex(iSRow)

                        sStatus = doGetFieldValue(msRowTableAlias & "U_RowSta", iRow)
                        If com.idh.bridge.lookups.FixedValues.isDeleted(sStatus) Then
                            oaRemove.Add(iSRow)
                        Else
                            Dim sJobNumber As String = doGetFieldValue(msRowTableAlias & "U_JobNr", iRow)
                            If sJobNumber.Length > 0 Then

                            End If
                        End If
                    Next

                    If oaRemove.Count > 0 Then
                        For i As Integer = 0 To oaRemove.Count - 1
                            oSelected.Remove(oaRemove.Item(i))
                        Next
                    End If

                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error marking as Deleted.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBMD", {Nothing})
            End Try
        End Sub


        '		'**
        '		' Get the Job Entries
        '		'**
        '		Public Function getJobEntries( ) As JobEntries
        '			Return getJobEntries( msLastJobNumber )
        '		End Function
        '		Public Function getJobEntries( ByVal sJobNumber As String ) As JobEntries
        '			Return moJobEntries
        '		End Function

        '**
        ' Get the calculated Onsite Quantities
        '**
        Public Function getOnSiteQty() As Double
            Return getOnSiteQty(msLastJobNumber)
        End Function
        Public Function getOnSiteQty(ByVal sJobNumber As String) As Double
            Dim dTotalQty As Double = 0
            If Not moJobEntries Is Nothing Then
                dTotalQty = moJobEntries.getOnSiteQty(sJobNumber)
            End If
            Return dTotalQty
        End Function

        Public Overloads Shared Function getInstance(ByVal oForm As SAPbouiCOM.Form, ByVal sGridID As String) As OrderRowGrid
            Return CType(UpdateGrid.getInstance(oForm, sGridID), OrderRowGrid)
        End Function

        Public Sub doCommentDeletion()
            Dim oSelected As SAPbouiCOM.SelectedRows
            oSelected = getSBOGrid().Rows.SelectedRows()
            If Not oSelected Is Nothing Then
                'Get the Deletion comment
                IDHAddOns.idh.addon.Base.PARENT.doOpenModalForm("IDH_COMMFRM", getSBOForm())
            End If
        End Sub

        Public Overridable Function doCheckAndDeleteSelection(ByVal sComment As String) As Boolean
            If doCheckCanDeleteSelection() Then
                doMarkSelectionAsDeleted(sComment)
                Return True
            End If
            Return False
        End Function

        '**
        ' Check for Marketing Documents linked to the Waste Order Row
        '**
        Public Function doCheckForLinkedMarkDocs(ByVal iRow As Integer) As Boolean
            Dim sValue As String
            For Each sField As String In IDH_JOBSHD.MarkDocFields
                sValue = doGetFieldValue(msRowTableAlias & sField, iRow)
                If Not sValue Is Nothing AndAlso sValue.Length > 0 Then
                    Return True
                End If
            Next
            Return False
        End Function

        Public Overridable Function doCheckCanDeleteSelection() As Boolean
            Dim iSRow As Integer
            Try
                Dim oSelectedRows As SAPbouiCOM.SelectedRows
                oSelectedRows = getSBOGrid().Rows.SelectedRows()

                Dim iIndex As Integer
                Dim iRow As Integer
                Dim iSize As Integer = oSelectedRows.Count
                If iSize = 0 Then Return True

                Dim oaRemove As New ArrayList
                Dim oaPBIList As ArrayList = Nothing
                Dim sSOList As String = ""
                Dim sPOList As String = ""
                Dim sSInvList As String = ""
                Dim sPInvList As String = ""
                Dim sLinkedList As String = ""
                Dim sDeletedList As String = ""
                Dim sMarkDocs As String = ""
                Dim sPBIList As String = ""

                For iIndex = 0 To iSize - 1
                    iSRow = oSelectedRows.Item(iIndex, SAPbouiCOM.BoOrderType.ot_RowOrder)
                    iRow = getSBOGrid().GetDataTableRowIndex(iSRow)

                    Dim sCode As String = doGetFieldValue(msRowTableAlias & "Code", iRow)
                    Dim sSOStatus As String = doGetFieldValue(msRowTableAlias & "U_Status", iRow)
                    Dim sPOStatus As String = doGetFieldValue(msRowTableAlias & "U_PStat", iRow)
                    Dim sRowSta As String = doGetFieldValue(msRowTableAlias & "U_RowSta", iRow)
                    Dim sWROrd As String = doGetFieldValue(msRowTableAlias & "U_WROrd", iRow)
                    Dim sWRRow As String = doGetFieldValue(msRowTableAlias & "U_WRRow", iRow)
                    Dim sPBI As String = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._LnkPBI, iRow)

                    If com.idh.bridge.lookups.FixedValues.isDeleted(sRowSta) Then
                        sDeletedList = sDeletedList & "," & sCode
                        oaRemove.Add(iSRow)
                    Else

                        If sSOStatus.StartsWith(FixedValues.getStatusOrdered()) Then
                            sSOList = sSOList & "," & sCode
                            oaRemove.Add(iSRow)
                        ElseIf sSOStatus.StartsWith(FixedValues.getStatusInvoiced()) Then
                            sSInvList = sSInvList & "," & sCode
                            oaRemove.Add(iSRow)
                        End If

                        If sPOStatus.StartsWith(FixedValues.getStatusOrdered()) Then
                            sPOList = sPOList & "," & sCode
                            oaRemove.Add(iSRow)
                        ElseIf sPOStatus.StartsWith(FixedValues.getStatusInvoiced()) Then
                            sPInvList = sPInvList & "," & sCode
                            oaRemove.Add(iSRow)
                        End If

                        If (Not sWROrd Is Nothing AndAlso sWROrd.Length > 0) OrElse
                            (Not sWRRow Is Nothing AndAlso sWRRow.Length > 0) Then
                            sLinkedList = sLinkedList & "," & sCode
                            oaRemove.Add(iSRow)
                        End If

                        If (Not sPBI Is Nothing AndAlso sPBI.Length > 0) Then
                            sPBIList = sPBIList & "," & sCode
                            If oaPBIList Is Nothing Then
                                oaPBIList = New ArrayList()
                            End If
                            oaPBIList.Add(iSRow)
                            'oaRemove.Add(iSRow)
                        End If

                        If doCheckForLinkedMarkDocs(iRow) Then
                            sMarkDocs = sMarkDocs & "," & sCode
                            oaRemove.Add(iSRow)
                        End If
                    End If
                Next

                If sPBIList.Length > 0 Then
                    '"The Waste Order Row%1 [%2] %3 linked to %4PBI%5. "
                    Dim sParams As String() = {"", sPBIList.Substring(1), "is", "a "}
                    If sPBIList.IndexOf(",", 1) > -1 Then
                        sParams(0) = "s"
                        sParams(2) = "are"
                        sParams(3) = ""
                    End If
                    If Messages.INSTANCE.doResourceMessageYN("WORPBID", sParams, 1) = 1 Then
                        sPBIList = ""
                    Else
                        oaRemove.AddRange(oaPBIList)
                    End If
                End If

                If oaRemove.Count > 0 Then
                    For i As Integer = 0 To oaRemove.Count - 1
                        oSelectedRows.Remove(oaRemove.Item(i))
                    Next

                    Dim sFinalMessage As String = " "
                    If sSOList.Length > 0 Then
                        sFinalMessage = sFinalMessage & ": " & Translation.getTranslatedWord("Sales Order") & " [" & sSOList.Substring(1) & "]"
                    End If
                    If sPOList.Length > 0 Then
                        sFinalMessage = sFinalMessage & ": " & Translation.getTranslatedWord("Purchase Order") & " [" & sPOList.Substring(1) & "]"
                    End If
                    If sSInvList.Length > 0 Then
                        sFinalMessage = sFinalMessage & ": " & Translation.getTranslatedWord("Sales Invoice") & " [" & sSInvList.Substring(1) & "]"
                    End If
                    If sPInvList.Length > 0 Then
                        sFinalMessage = sFinalMessage & ": " & Translation.getTranslatedWord("Purchase Invoice") & " [" & sPInvList.Substring(1) & "]"
                    End If
                    If sLinkedList.Length > 0 Then
                        sFinalMessage = sFinalMessage & ": " & Translation.getTranslatedWord("Linked") & " [" & sLinkedList.Substring(1) & "]"
                    End If
                    If sPBIList.Length > 0 Then
                        sFinalMessage = sFinalMessage & ": " & Translation.getTranslatedWord("Linked PBI") & " [" & sPBIList.Substring(1) & "]"
                    End If
                    If sDeletedList.Length > 0 Then
                        sFinalMessage = sFinalMessage & ": " & Translation.getTranslatedWord("Already Deleted") & " [" & sDeletedList.Substring(1) & "]"
                    End If
                    If sMarkDocs.Length > 0 Then
                        sFinalMessage = sFinalMessage & ": " & Translation.getTranslatedWord("Marketing Docs's") & " [" & sMarkDocs.Substring(1) & "]"
                    End If
                    'com.idh.bridge.DataHandler.INSTANCE.doError(Messages.getGMessage("ROWRDEL", Nothing) + sFinalMessage)
                    DataHandler.INSTANCE.doResUserError(Messages.getGMessage("ROWRDEL", Nothing) + sFinalMessage, "ERUSGEN", {Messages.getGMessage("ROWRDEL", Nothing) + sFinalMessage})
                End If
                Return oSelectedRows.Count > 0
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error checking for delete - " & iSRow)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBCD", {iSRow})
            Finally
            End Try
            Return False
        End Function

        '**
        ' Mark the selected rows as deleted
        '**
        Protected Overridable Function doMarkSelectionAsDeleted(ByVal sComment As String) As Boolean
            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = getGrid().Rows.SelectedRows()
                Dim sDeleted As String = com.idh.bridge.lookups.FixedValues.getStatusDeleted()
                If Not oSelected Is Nothing Then
                    Dim iRow As Integer
                    'Dim bFound As Boolean = False

                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iRow = getDataTableRowIndex(oSelected, iIndex)

                        Dim sWOR As String = doGetFieldValue(msRowTableAlias & "Code", iRow)
                        Dim sLinkedPBI As String = doGetFieldValue(msRowTableAlias & "U_LnkPBI", iRow)

                        doSetFieldValue(msRowTableAlias & "U_RowSta", iRow, sDeleted)
                        doSetFieldValue(msRowTableAlias & "U_ActComm", iRow, sComment)

                        If Not sLinkedPBI Is Nothing AndAlso sLinkedPBI.Length > 0 Then
                            ''Delete all the PBI Details linked to this WOR
                            '	Dim oPBIDetail As IDH_PBIDTL = New IDH_PBIDTL()
                            '	Dim bDeletePBI As Boolean = oPBIDetail.doDeleteByPBI( sWOR, sComment )
                            '	IDHAddOns.idh.data.Base.doReleaseObject(oPBIDetail)
                            Dim oWOR As New DelWOR
                            oWOR.sWOR = sWOR
                            oWOR.sComment = sComment

                            If moMarkedForDeleteList Is Nothing Then
                                moMarkedForDeleteList = New ArrayList()
                            End If
                            moMarkedForDeleteList.Add(oWOR)
                        End If

                        'Dim sStatus As String
                        'sStatus = doGetFieldValue(msRowTableAlias & "U_RowSta", iRow)



                        'If com.idh.bridge.lookups.FixedValues.isOpen(sStatus) OrElse _
                        '        com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse _
                        '        com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) OrElse _
                        '        com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) OrElse _
                        '        com.idh.bridge.lookups.FixedValues.isError(sStatus) Then
                        '    doSetFieldValue(msRowTableAlias & "U_RowSta", iRow, sDeleted)
                        '    doSetFieldValue(msRowTableAlias & "U_ActComm", iRow, sComment)

                        '    If doCheckForLinkedMarkDocs(iRow) = False AndAlso Not sLinkedPBI Is Nothing AndAlso sLinkedPBI.Length > 0 Then
                        '        ''Delete all the PBI Details linked to this WOR
                        '        '	Dim oPBIDetail As IDH_PBIDTL = New IDH_PBIDTL()
                        '        '	Dim bDeletePBI As Boolean = oPBIDetail.doDeleteByPBI( sWOR, sComment )
                        '        '	IDHAddOns.idh.data.Base.doReleaseObject(oPBIDetail)
                        '        Dim oWOR As New DelWOR
                        '        oWOR.sWOR = sWOR
                        '        oWOR.sComment = sComment
                        '        moMarkedForDeleteList.Add(oWOR)
                        '    End If
                        'End If
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error marking as Deleted.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBMD", {Nothing})
            Finally
                moGridForm.Freeze(False)
            End Try
            Return True
        End Function

        '**
        ' Do Undelete the rows
        '**
        Public Overridable Function doReactivateSelection() As Boolean
            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = getGrid().Rows.SelectedRows()
                Dim sOpen As String = com.idh.bridge.lookups.FixedValues.getStatusOpen()
                If Not oSelected Is Nothing Then
                    Dim iRow As Integer
                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iRow = getDataTableRowIndex(oSelected, iIndex)

                        Dim sStatus As String
                        Dim sActComm As String
                        sStatus = doGetFieldValue(msRowTableAlias & "U_RowSta", iRow)
                        sActComm = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._ActComm, iRow)

                        If com.idh.bridge.lookups.FixedValues.isDeleted(sStatus) Then
                            If Not sActComm.Contains(Config.INSTANCE.getParameterWithDefault("PBIDELCM", "PBI Deleted no longer available")) Then
                                doSetFieldValue(msRowTableAlias & "U_RowSta", iRow, sOpen)
                                doSetFieldValue(msRowTableAlias & "U_ActComm", iRow, "ReOpened")
                            Else
                                Messages.INSTANCE.doResourceMessage("EREPBRW", Nothing)
                                Return False
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error ReActivating the row.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXAROW", {Nothing})
            Finally
                moGridForm.Freeze(False)
            End Try
            Return True
        End Function

        '**
        ' Do Request Archive
        '**
        Public Overridable Function doRequestArchive() As Boolean
            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim iRow As Integer
                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iRow = getDataTableRowIndex(oSelected, iIndex)

                        doSetFieldValue(msRowTableAlias & IDH_JOBSHD._ReqArch, iRow, "Y")
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Requesting Archive.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBRA", {Nothing})
            Finally
                moGridForm.Freeze(False)
            End Try
            Return True
        End Function

        '**
        ' Do Request ReOpen
        '**
        Public Function doRequestReOpen() As Boolean
            If msWR1OrdType = "DO" Then
                Return doRequestDORReOpen()
            Else
                Return doRequestWORReOpen()
            End If
        End Function

        Public Overridable Function doRequestWORReOpen() As Boolean
            Return True
        End Function

        Public Overridable Function doRequestDORReOpen() As Boolean
            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim oDOR As IDH_DISPROW = New IDH_DISPROW()
                    Dim iRow As Integer
                    Dim sCode As String
                    Dim bResult As Boolean = True

                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iRow = getDataTableRowIndex(oSelected, iIndex)
                        sCode = doGetFieldValue(msRowTableAlias & IDH_DISPROW._Code, iRow)

                        If sCode IsNot Nothing AndAlso sCode.Length > 0 Then
                            If oDOR.getByKey(sCode) Then
                                DataHandler.INSTANCE.StartTransaction()
                                Try
                                    bResult = oDOR.doReOpen("ReOpen Request from Order Manager Grid.")
                                    If bResult = True Then
                                        bResult = oDOR.doUpdateDataRow()
                                    End If
                                Catch ex As Exception
                                    'DataHandler.INSTANCE.doError("Exception: [Code:" & sCode & "]" + ex.ToString(), "Error ReOpening the Marketing Documents. [Code:" & sCode & "].")
                                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXROMD", {sCode})
                                    bResult = False
                                Finally
                                    If (bResult) Then
                                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                    Else
                                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                    End If
                                End Try
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error ReOpening the Marketing Documents.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXROMD", {String.Empty})
            Finally
                moGridForm.Freeze(False)
            End Try
            Return True
        End Function

        '**
        ' Do Undelete the rows
        '**
        Public Overridable Function doRequestShipmentCancelation() As Boolean
            Dim bResult As Boolean = False
            If msWR1OrdType = "DO" Then
                bResult = doRequestDORShipmentCancelation()
            Else
                bResult = doRequestWORShipmentCancelation()
            End If
            If bResult Then
                doReloadData()
            End If
            Return bResult
        End Function

        '**
        ' Do Cancel DO Shipment on the selected Rows
        '**
        Public Overridable Function doRequestDORShipmentCancelation() As Boolean
            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim oDOR As IDH_DISPROW = New IDH_DISPROW()
                    Dim iRow As Integer
                    Dim sCode As String
                    Dim bResult As Boolean = True
                    Dim sCanceled As String = com.idh.bridge.lookups.FixedValues.getStatusCanceled()

                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iRow = getDataTableRowIndex(oSelected, iIndex)
                        sCode = doGetFieldValue(msRowTableAlias & IDH_DISPROW._Code, iRow)

                        If sCode IsNot Nothing AndAlso sCode.Length > 0 Then
                            If oDOR.getByKey(sCode) Then
                                If oDOR.doCancelShipment("Marketing Document Cancelation due to Shipment Cancelation from the DOM.") Then
                                    oDOR.doUpdateDataRow()
                                End If
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCMD", {String.Empty})
            Finally
                moGridForm.Freeze(False)
            End Try
            Return True
        End Function

        '**
        ' Do Cancel WO Shipment on the selected Rows
        '**
        Public Overridable Function doRequestWORShipmentCancelation() As Boolean
            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim oWOR As IDH_JOBSHD = New IDH_JOBSHD()
                    Dim iRow As Integer
                    Dim sCode As String
                    Dim bResult As Boolean = True
                    Dim sCanceled As String = com.idh.bridge.lookups.FixedValues.getStatusCanceled()

                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iRow = getDataTableRowIndex(oSelected, iIndex)
                        sCode = doGetFieldValue(msRowTableAlias & IDH_DISPROW._Code, iRow)

                        If sCode IsNot Nothing AndAlso sCode.Length > 0 Then
                            If oWOR.getByKey(sCode) Then
                                oWOR.doCancelShipment("Marketing Document Cancelation due to Shipment Cancelation from the BM.")
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCMD", {String.Empty})
            Finally
                moGridForm.Freeze(False)
            End Try
            Return True
        End Function

        '**
        ' Do Request Cancelation of Marketing Documents
        '**
        '**
        ' Do Request ReOpen
        '**
        Public Function doRequestCancelMarketingDocs() As Boolean
            If msWR1OrdType = "DO" Then
                Return doRequestDORCancelMarketingDocs()
            Else
                Return doRequestWORCancelMarketingDocs()
            End If
        End Function
        Public Overridable Function doRequestWORCancelMarketingDocs() As Boolean

            Return True
        End Function
        Public Overridable Function doRequestDORCancelMarketingDocs() As Boolean
            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim oDOR As IDH_DISPROW = New IDH_DISPROW()
                    Dim iRow As Integer
                    Dim sCode As String
                    Dim bResult As Boolean = True

                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iRow = getDataTableRowIndex(oSelected, iIndex)
                        sCode = doGetFieldValue(msRowTableAlias & IDH_DISPROW._Code, iRow)

                        If sCode IsNot Nothing AndAlso sCode.Length > 0 Then
                            If oDOR.getByKey(sCode) Then
                                DataHandler.INSTANCE.StartTransaction()
                                Try
                                    bResult = oDOR.doResetMarketingDocuments("Marketing Document Cancelation Request from Order Manager Grid.")
                                    If bResult = True Then
                                        bResult = oDOR.doUpdateDataRow()
                                    End If
                                Catch ex As Exception
                                    'DataHandler.INSTANCE.doError("Exception: [Code:" & sCode & "]" + ex.ToString(), "Error Canceling Marketing Documents. [Code:" & sCode & "].")
                                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCMD", {sCode})
                                    bResult = False
                                Finally
                                    If (bResult) Then
                                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                    Else
                                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                    End If
                                End Try
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Canceling Marketing Documents.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCMD", {String.Empty})
            Finally
                moGridForm.Freeze(False)
            End Try
            Return True
        End Function

        '**
        ' Mark the selected rows as Foc
        '**
        Public Sub doMarkSelectionAsFoc()
            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim iRow As Integer
                    Dim bFound As Boolean = False
                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iRow = getDataTableRowIndex(oSelected, iIndex)

                        Dim sStatus As String
                        sStatus = doGetFieldValue(msRowTableAlias & "U_Status", iRow)
                        If com.idh.bridge.lookups.FixedValues.isOpen(sStatus) OrElse
                                com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse
                                com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) OrElse
                                com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) OrElse
                                com.idh.bridge.lookups.FixedValues.isError(sStatus) Then
                            sStatus = com.idh.bridge.lookups.FixedValues.getDoFocStatus()

                            If com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) Then
                                doSetFieldValue(msRowTableAlias & "U_Status", iRow, "")
                            End If

                            doSetFieldValue(msRowTableAlias & "U_Status", iRow, sStatus)
                        End If
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error marking as FOC.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXMFOC", {Nothing})
            Finally
                moGridForm.Freeze(False)
            End Try
        End Sub

        '**
        ' Mark the selected rows as ToPay
        '**
        Public Sub doMarkSelectionAsPay()
            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                Dim oToRemove As New ArrayList
                Dim oToSelectPayment As New ArrayList
                Dim oToCCError As New ArrayList
                Dim iIndex As Integer
                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim iRow As Integer
                    Dim bFound As Boolean = False
                    For iIndex = 0 To oSelected.Count - 1
                        iRow = getDataTableRowIndex(oSelected, iIndex)

                        Dim sLinkedWO As String = doGetFieldValue(msRowTableAlias & IDH_DISPROW._WRRow, iRow)
                        If msWR1OrdType = "DO" AndAlso sLinkedWO IsNot Nothing AndAlso sLinkedWO.Length > 0 Then
                            oToRemove.Add(iRow)
                        Else

                            Dim sStatus As String = doGetFieldValue(msRowTableAlias & "U_Status", iRow)
                            Dim sPayMethod As String = doGetFieldValue(msRowTableAlias & "U_PayMeth", iRow)
                            Dim sPayStatus As String = doGetFieldValue(msRowTableAlias & "U_PayStat", iRow)
                            Dim sPayRcpt As String = doGetFieldValue(msRowTableAlias & "U_PARCPT", iRow)

                            If sStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusInvoiced()) = True AndAlso
                               (sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusPaid()) OrElse
                                sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusErrPaid()) OrElse
                                sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusUnPaid())) AndAlso
                               (sPayRcpt Is Nothing OrElse sPayRcpt.Length = 0) Then
                                If sPayMethod.Equals("Cash") OrElse sPayMethod.Equals("Check") OrElse sPayMethod.Equals("Cheque") Then
                                    doForceFieldValue(msRowTableAlias & "U_PayStat", iRow, com.idh.bridge.lookups.FixedValues.getStatusPaid())
                                ElseIf sPayMethod.Equals(Config.INSTANCE.getCreditCardName()) Then
                                    If sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusErrPaid()) Then
                                        oToCCError.Add(iRow)
                                    Else
                                        doForceFieldValue(msRowTableAlias & "U_PayStat", iRow, com.idh.bridge.lookups.FixedValues.getStatusPaid())
                                    End If
                                ElseIf sPayMethod.Length = 0 Then
                                    oToSelectPayment.Add(iRow)
                                End If
                            Else
                                oToRemove.Add(iRow)
                            End If
                        End If
                    Next

                    Dim sMessageId As String = ""
                    Dim sParams As String() = Nothing

                    'Remove the rows from the selection
                    If oToRemove.Count() > 0 Then
                        If oToRemove.Count() > 1 Then
                            sMessageId = "MANAIPRS"
                            sParams = New String() {oToRemove.Count().ToString(), General.doCreateIntCSL(oToRemove, True)}
                        Else
                            sMessageId = "MANAIPRS"
                            sParams = New String() {General.doCreateIntCSL(oToRemove, True)}
                        End If
                        Messages.INSTANCE.doResourceMessage(sMessageId, sParams, 1, com.idh.bridge.Translation.getTranslatedWord("Ok"))
                        For iIndex = 0 To oToRemove.Count - 1
                            oSelected.Remove(oToRemove(iIndex))
                        Next
                    End If

                    'Show the Credit card Error option
                    Dim bFromCC As Boolean = False
                    If oToCCError.Count() > 0 Then
                        If oToSelectPayment.Count() > 1 Then
                            sMessageId = "MANCCERS"
                            sParams = New String() {oToCCError.Count().ToString(), General.doCreateIntCSL(oToCCError, True)}
                        Else
                            sMessageId = "MANCCER"
                            sParams = New String() {General.doCreateIntCSL(oToCCError, True)}
                        End If
                        If Messages.INSTANCE.doResourceMessageYNAsk(sMessageId, sParams, 1) = 2 Then
                            For iIndex = 0 To oToCCError.Count - 1
                                doForceFieldValue(msRowTableAlias & "U_PayStat", oToCCError(iIndex), com.idh.bridge.lookups.FixedValues.getStatusPaid())
                            Next
                        Else
                            For iIndex = 0 To oToCCError.Count - 1
                                oToSelectPayment.Add(oToCCError(iIndex))
                            Next
                            bFromCC = True
                        End If
                    End If

                    'Show the payment selection option
                    If oToSelectPayment.Count() > 0 Then
                        If bFromCC = False Then
                            If oToSelectPayment.Count() > 1 Then
                                sMessageId = "MANPMRS"
                                sParams = New String() {oToSelectPayment.Count().ToString(), General.doCreateIntCSL(oToSelectPayment, True)}
                            Else
                                sMessageId = "MANPMR"
                                sParams = New String() {General.doCreateIntCSL(oToSelectPayment, True)}
                            End If
                        End If
                        If bFromCC OrElse Messages.INSTANCE.doResourceMessageYNAsk(sMessageId, sParams, 1) = 1 Then
                            IDHAddOns.idh.forms.Base.setSharedData(moGridForm, "ACTION", "D")
                            IDHAddOns.idh.forms.Base.setSharedData(moGridForm, "ROWS", oToSelectPayment)
                            IDHAddOns.idh.addon.Base.PARENT.doOpenModalForm("IDHPAYMET", moGridForm)
                        End If
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error marking to pay.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXMTOP", {Nothing})
            Finally
                moGridForm.Freeze(False)
            End Try
        End Sub

        '**
        ' Mark the selected rows as Invoice
        '**
        Public Sub doMarkSelectionAsInvoice()
            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows

                Dim oToRemove As New ArrayList
                Dim oToPayCash As New ArrayList
                Dim oToPayCheque As New ArrayList
                Dim oToPayCC As New ArrayList
                Dim oToSelectPayment As New ArrayList
                Dim iIndex As Integer
                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim iRow As Integer
                    Dim bFound As Boolean = False
                    For iIndex = 0 To oSelected.Count - 1
                        iRow = getDataTableRowIndex(oSelected, iIndex)

                        Dim sLinkedWO As String = doGetFieldValue(msRowTableAlias & IDH_DISPROW._WRRow, iRow)
                        If msWR1OrdType = "DO" AndAlso sLinkedWO IsNot Nothing AndAlso sLinkedWO.Length > 0 Then
                            oToRemove.Add(iRow)
                        Else
                            Dim sStatus As String = doGetFieldValue(msRowTableAlias & "U_Status", iRow)
                            Dim sPayMethod As String = doGetFieldValue(msRowTableAlias & "U_PayMeth", iRow)
                            Dim sPayStatus As String = doGetFieldValue(msRowTableAlias & "U_PayStat", iRow)
                            Dim sPayRcpt As String = doGetFieldValue(msRowTableAlias & "U_PARCPT", iRow)

                            Dim dValue As Double
                            Dim bCanDoZero As Boolean = Config.INSTANCE.getParameterAsBool("MDALLZE", False)

                            dValue = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._Total, iRow)
                            If dValue <> 0 OrElse bCanDoZero Then
                                If com.idh.bridge.lookups.FixedValues.isOpen(sStatus) OrElse
                                    com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse
                                    com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) OrElse
                                    com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) OrElse
                                    com.idh.bridge.lookups.FixedValues.isError(sStatus) Then
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()
                                    doForceFieldValue(msRowTableAlias & "U_Status", iRow, sStatus)

                                    'Check the rows to be marked as to be paid
                                    If sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusUnPaid()) OrElse
                                        sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusErrPaid()) AndAlso
                                       (sPayRcpt Is Nothing OrElse sPayRcpt.Length = 0) Then
                                        If sPayMethod.Equals("Cash") Then
                                            oToPayCash.Add(iRow)
                                        ElseIf sPayMethod.Equals("Check") OrElse sPayMethod.Equals("Cheque") Then
                                            oToPayCheque.Add(iRow)
                                        ElseIf sPayMethod.Equals(Config.INSTANCE.getCreditCardName()) Then
                                            oToPayCC.Add(iRow)
                                        ElseIf sPayMethod.Length = 0 Then
                                            oToSelectPayment.Add(iRow)
                                        End If
                                    End If
                                Else
                                    oToRemove.Add(iRow)
                                End If
                            Else
                                'oToRemove.Add(iRow)
                            End If
                        End If
                    Next

                    Dim sParams() As String
                    Dim sMessageId As String = ""

                    'Remove to rows that need to be skipped
                    If oToRemove.Count() > 0 Then
                        If oToRemove.Count() > 1 Then
                            sMessageId = "MANAIRS"
                            sParams = New String() {oToRemove.Count().ToString(), General.doCreateIntCSL(oToRemove, True)}
                        Else
                            sMessageId = "MANAIR"
                            sParams = New String() {General.doCreateIntCSL(oToRemove, True)}
                        End If
                        Messages.INSTANCE.doResourceMessage(sMessageId, sParams, 1, com.idh.bridge.Translation.getTranslatedWord("Ok"))
                        For iIndex = 0 To oToRemove.Count - 1
                            oSelected.Remove(oToRemove(iIndex))
                        Next
                    End If

                    'Check the Cash
                    If oToPayCash.Count() > 0 Then
                        If oToPayCash.Count() > 1 Then
                            sMessageId = "MANRMRS"
                            sParams = New String() {oToPayCash.Count().ToString(), General.doCreateIntCSL(oToPayCash, True)}
                        Else
                            sParams = New String() {General.doCreateIntCSL(oToPayCash, True)}
                            sMessageId = "MANRMR"
                        End If
                        If Messages.INSTANCE.doResourceMessageYN(sMessageId, sParams, 1) = 1 Then
                            For iIndex = 0 To oToPayCash.Count - 1
                                doForceFieldValue(msRowTableAlias & "U_PayStat", oToPayCash(iIndex), com.idh.bridge.lookups.FixedValues.getStatusPaid())
                            Next
                        End If
                    End If

                    'Check the Cheque
                    If oToPayCheque.Count() > 0 Then
                        If oToPayCheque.Count() > 1 Then
                            sMessageId = "MANRCRS"
                            sParams = New String() {oToPayCheque.Count().ToString(), General.doCreateIntCSL(oToPayCheque, True)}
                        Else
                            sMessageId = "MANRCR"
                            sParams = New String() {General.doCreateIntCSL(oToPayCheque, True)}
                        End If
                        If Messages.INSTANCE.doResourceMessageYN(sMessageId, sParams, 1) = 1 Then
                            For iIndex = 0 To oToPayCheque.Count - 1
                                doForceFieldValue(msRowTableAlias & "U_PayStat", oToPayCheque(iIndex), com.idh.bridge.lookups.FixedValues.getStatusPaid())
                            Next
                        End If
                    End If

                    'Check the CreditCard
                    If oToPayCC.Count() > 0 Then
                        If oToPayCC.Count() > 1 Then
                            sMessageId = "MANPCRS"
                            sParams = New String() {oToPayCC.Count().ToString(), General.doCreateIntCSL(oToPayCC, True)}
                        Else
                            sMessageId = "MANPCR"
                            sParams = New String() {General.doCreateIntCSL(oToPayCC, True)}
                        End If
                        If Messages.INSTANCE.doResourceMessageYN(sMessageId, sParams, 1) = 1 Then
                            For iIndex = 0 To oToPayCC.Count - 1
                                doForceFieldValue(msRowTableAlias & "U_PayStat", oToPayCC(iIndex), com.idh.bridge.lookups.FixedValues.getStatusPaid())
                            Next
                        End If
                    End If

                    'Show the payment selection option
                    If oToSelectPayment.Count() > 0 Then
                        If oToSelectPayment.Count() > 1 Then
                            sMessageId = "MANPMRS"
                            sParams = New String() {oToSelectPayment.Count().ToString(), General.doCreateIntCSL(oToSelectPayment, True)}
                        Else
                            sMessageId = "MANPMR"
                            sParams = New String() {General.doCreateIntCSL(oToSelectPayment, True)}
                        End If
                        If Messages.INSTANCE.doResourceMessageYNAsk(sMessageId, sParams, 1) = 1 Then
                            IDHAddOns.idh.forms.Base.setSharedData(moGridForm, "ACTION", "D")
                            IDHAddOns.idh.forms.Base.setSharedData(moGridForm, "ROWS", oToSelectPayment)
                            IDHAddOns.idh.addon.Base.PARENT.doOpenModalForm("IDHPAYMET", moGridForm)
                        End If
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error martking to invoice.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXMTOI", {Nothing})
            Finally
                moGridForm.Freeze(False)
            End Try
        End Sub


        'Dim dValue As Double
        'Dim bCanDoZero As Boolean = Config.INSTANCE.getParameterAsBool("MDALLZE", False)
        'If bDoSO Then
        '    dValue = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._Total)
        '    If dValue <> 0 OrElse bCanDoZero Then
        '    End If
        'End If
        'If bDoPO Then
        '    dValue = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._JCost)
        '    If dValue <> 0 OrElse bCanDoZero Then
        '    End If
        'End If

        '**
        ' Mark the selected rows as Order
        '**
        Public Sub doMarkSelectionAsOrder(ByVal bDoSO As Boolean, ByVal bDoPO As Boolean)
            If bDoSO = False AndAlso bDoPO = False Then
                Exit Sub
            End If

            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oToRemove As New ArrayList
                Dim oSelected As SAPbouiCOM.SelectedRows
                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim iRow As Integer
                    Dim bFound As Boolean = False
                    Dim iIndex As Integer
                    Dim bContinueWithSO As Boolean
                    Dim bContinueWithPO As Boolean
                    Dim bHaveZeroValue As Boolean
                    Dim bCanDoZero As Boolean
                    For iIndex = 0 To oSelected.Count - 1
                        bContinueWithSO = False
                        bContinueWithPO = False
                        bHaveZeroValue = False
                        iRow = getDataTableRowIndex(oSelected, iIndex)

                        Dim sLinkedWO As String = doGetFieldValue(msRowTableAlias & IDH_DISPROW._WRRow, iRow)
                        If msWR1OrdType = "DO" AndAlso sLinkedWO IsNot Nothing AndAlso sLinkedWO.Length > 0 Then
                            oToRemove.Add(iRow)
                        Else
                            Dim sStatus As String
                            Dim sStockMove As String = doGetFieldValue(msRowTableAlias & "U_BookIn", iRow)
                            Dim bStockMove As Boolean = False
                            Dim sTipPO As String = Nothing
                            Dim sHaulPO As String = Nothing
                            Dim sProPO As String = Nothing
                            Dim sSO As String = Nothing

                            If Not sStockMove Is Nothing AndAlso sStockMove.Length > 0 AndAlso sStockMove.Equals("B") = True Then
                                bStockMove = True
                            End If

                            Dim dValue As Double
                            bCanDoZero = Config.INSTANCE.getParameterAsBool("MDALLZE", False)
                            If bDoSO Then
                                sStatus = doGetFieldValue(msRowTableAlias & "U_Status", iRow)
                                dValue = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._Total, iRow)

                                If dValue <> 0 OrElse bCanDoZero Then

                                    Dim bCanDoSO As Boolean = False

                                    If com.idh.bridge.lookups.FixedValues.isOpen(sStatus) OrElse
                                        com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse
                                        com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) OrElse
                                        com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) OrElse
                                        com.idh.bridge.lookups.FixedValues.isError(sStatus) OrElse
                                        com.idh.bridge.lookups.FixedValues.isNoOrder(sStatus) Then
                                        bCanDoSO = True
                                    ElseIf com.idh.bridge.lookups.FixedValues.getStatusOrdered() = sStatus Then
                                        Dim idx As Integer
                                        idx = doIndexField(msRowTableAlias & "U_SAORD")
                                        If idx > -1 Then
                                            sSO = doGetFieldValue(idx, iRow)
                                            If sSO Is Nothing OrElse sSO.Length = 0 Then
                                                bCanDoSO = True
                                            End If
                                        End If
                                    End If

                                    If bCanDoSO Then
                                        'com.idh.bridge.lookups.FixedValues.isOpen(sStatus) OrElse _
                                        'com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse _
                                        'com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) OrElse _
                                        'com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) OrElse _
                                        'com.idh.bridge.lookups.FixedValues.isError(sStatus) OrElse _
                                        'com.idh.bridge.lookups.FixedValues.isNoOrder(sStatus) Then
                                        doForceFieldValue(msRowTableAlias & "U_Status", iRow, com.idh.bridge.lookups.FixedValues.getDoOrderStatus())
                                        doForceFieldValue(msRowTableAlias & "U_PayMeth", iRow, "Accounts")
                                        doForceFieldValue(msRowTableAlias & "U_PayStat", iRow, com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
                                        doForceFieldValue(msRowTableAlias & "U_CCNum", iRow, "")
                                        doForceFieldValue(msRowTableAlias & "U_CCType", iRow, "0")
                                        doForceFieldValue(msRowTableAlias & "U_CCStat", iRow, "")
                                        bContinueWithSO = True
                                    ElseIf Not sStockMove Is Nothing AndAlso sStockMove.Length > 0 AndAlso
                                        sStockMove.Equals("D") = False AndAlso
                                        com.idh.bridge.lookups.FixedValues.getStatusInvoiced <> sStatus Then
                                        doForceFieldValue(msRowTableAlias & "U_Status", iRow, com.idh.bridge.lookups.FixedValues.getDoOrderStatus())
                                        bContinueWithSO = True
                                    End If
                                Else
                                    bHaveZeroValue = True
                                End If
                            End If

                            If bDoPO Then
                                sStatus = doGetFieldValue(msRowTableAlias & "U_PStat", iRow)
                                dValue = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._JCost, iRow)

                                If dValue <> 0 OrElse bCanDoZero Then
                                    If sStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) = True Then
                                        Dim idx As Integer
                                        Dim bCanDoPO As Boolean = False
                                        idx = doIndexField(msRowTableAlias & "U_TIPPO")
                                        If idx > -1 Then
                                            sTipPO = doGetFieldValue(idx, iRow)
                                            bCanDoPO = True
                                        End If

                                        idx = doIndexField(msRowTableAlias & "U_JOBPO")
                                        If idx > -1 Then
                                            sHaulPO = doGetFieldValue(msRowTableAlias & "U_JOBPO", iRow)
                                            bCanDoPO = True
                                        End If

                                        idx = doIndexField(msRowTableAlias & "U_ProPO")
                                        If idx > -1 Then
                                            sProPO = doGetFieldValue(msRowTableAlias & "U_ProPO", iRow)
                                            bCanDoPO = True
                                        End If

                                        If bStockMove Then
                                            bCanDoPO = True
                                        End If

                                        If bCanDoPO AndAlso
                                            ((sTipPO Is Nothing OrElse sTipPO.Length = 0) OrElse
                                              (sHaulPO Is Nothing OrElse sHaulPO.Length = 0) OrElse
                                              (sProPO Is Nothing OrElse sProPO.Length = 0) OrElse
                                              bStockMove) Then
                                            doForceFieldValue(msRowTableAlias & "U_PStat", iRow, com.idh.bridge.lookups.FixedValues.getDoOrderStatus())
                                            bContinueWithPO = True
                                        End If
                                    Else
                                        If com.idh.bridge.lookups.FixedValues.isOpen(sStatus) OrElse
                                            com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse
                                            com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) OrElse
                                            com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) OrElse
                                            com.idh.bridge.lookups.FixedValues.isError(sStatus) OrElse
                                            bStockMove Then
                                            doForceFieldValue(msRowTableAlias & "U_PStat", iRow, com.idh.bridge.lookups.FixedValues.getDoOrderStatus())
                                            bContinueWithPO = True
                                        End If
                                    End If
                                Else
                                    bHaveZeroValue = True
                                End If
                            End If
                            If bContinueWithSO = False AndAlso bContinueWithPO = False Then
                                oToRemove.Add(iRow)
                            End If
                        End If
                    Next

                    Dim sParams() As String
                    Dim sMessageId As String = ""

                    'Remove to rows that need to be skipped
                    If oToRemove.Count() > 0 Then
                        If oToRemove.Count() > 1 Then
                            If bHaveZeroValue AndAlso bCanDoZero = False Then
                                sMessageId = "MANZORDS"
                            Else
                                sMessageId = "MANORDS"
                            End If
                            sParams = New String() {oToRemove.Count().ToString(), General.doCreateIntCSL(oToRemove, True)}
                        Else
                            If bHaveZeroValue AndAlso bCanDoZero = False Then
                                sMessageId = "MANZORD"
                            Else
                                sMessageId = "MANORD"
                            End If
                            sParams = New String() {General.doCreateIntCSL(oToRemove, True)}
                        End If
                        Messages.INSTANCE.doResourceMessage(sMessageId, sParams, 1, com.idh.bridge.Translation.getTranslatedWord("Ok"))
                        For iIndex = 0 To oToRemove.Count - 1
                            oSelected.Remove(oToRemove(iIndex))
                        Next
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error marking to order.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXMTOO", {Nothing})
            Finally
                moGridForm.Freeze(False)
            End Try
        End Sub

        '**
        ' Mark the selected rows as Rebate
        '**
        Public Sub doMarkSelectionAsRebate(ByVal bDoARRebate As Boolean, ByVal bDoAPRebate As Boolean)
            If bDoARRebate = False AndAlso bDoAPRebate = False Then
                Exit Sub
            End If

            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                Dim oToRemove As New ArrayList

                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim iRow As Integer
                    Dim bFound As Boolean = False
                    Dim iIndex As Integer
                    For iIndex = 0 To oSelected.Count - 1
                        iRow = getDataTableRowIndex(oSelected, iIndex)

                        Dim sLinkedWO As String = doGetFieldValue(msRowTableAlias & IDH_DISPROW._WRRow, iRow)
                        If msWR1OrdType = "DO" AndAlso sLinkedWO IsNot Nothing AndAlso sLinkedWO.Length > 0 Then
                            oToRemove.Add(iRow)
                        Else
                            Dim sStatus As String
                            Dim dValue As Double
                            Dim bCanDoZero As Boolean = Config.INSTANCE.getParameterAsBool("MDALLZE", False)
                            'If bDoSO Then
                            '    dValue = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._Total)
                            '    If dValue <> 0 OrElse bCanDoZero Then
                            '    End If
                            'End If
                            'If bDoPO Then
                            '    dValue = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._JCost)
                            '    If dValue <> 0 OrElse bCanDoZero Then
                            '    End If
                            'End If
                            If bDoARRebate Then
                                sStatus = doGetFieldValue(msRowTableAlias & "U_Status", iRow)
                                dValue = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._Total, iRow)
                                If dValue <> 0 OrElse bCanDoZero Then
                                    If com.idh.bridge.lookups.FixedValues.isWaitingForRebate(sStatus) Then
                                        doForceFieldValue(msRowTableAlias & "U_Status", iRow, com.idh.bridge.lookups.FixedValues.getDoRebateStatus())
                                        doForceFieldValue(msRowTableAlias & "U_PayMeth", iRow, "")
                                        doForceFieldValue(msRowTableAlias & "U_PayStat", iRow, "")
                                        doForceFieldValue(msRowTableAlias & "U_CCNum", iRow, "")
                                        doForceFieldValue(msRowTableAlias & "U_CCType", iRow, "0")
                                        doForceFieldValue(msRowTableAlias & "U_CCStat", iRow, "")
                                    End If
                                End If
                            End If

                            If bDoAPRebate Then
                                sStatus = doGetFieldValue(msRowTableAlias & "U_PStat", iRow)
                                dValue = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._JCost, iRow)
                                If dValue <> 0 OrElse bCanDoZero Then
                                    If com.idh.bridge.lookups.FixedValues.isWaitingForRebate(sStatus) Then
                                        doForceFieldValue(msRowTableAlias & "U_PStat", iRow, com.idh.bridge.lookups.FixedValues.getDoRebateStatus())
                                    End If
                                End If
                            End If

                            If bDoARRebate = False AndAlso bDoAPRebate = False Then
                                oToRemove.Add(iRow)
                            End If
                        End If
                    Next

                    Dim sParams() As String
                    Dim sMessageId As String = ""

                    'Remove to rows that need to be skipped
                    If oToRemove.Count() > 0 Then
                        If oToRemove.Count() > 1 Then
                            sMessageId = "MANREBS"
                            sParams = New String() {oToRemove.Count().ToString(), General.doCreateIntCSL(oToRemove, True)}
                        Else
                            sMessageId = "MANREB"
                            sParams = New String() {General.doCreateIntCSL(oToRemove, True)}
                        End If
                        Messages.INSTANCE.doResourceMessage(sMessageId, sParams, 1, com.idh.bridge.Translation.getTranslatedWord("Ok"))
                        For iIndex = 0 To oToRemove.Count - 1
                            oSelected.Remove(oToRemove(iIndex))
                        Next
                    End If

                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error marking for rebate.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXMTOR", {Nothing})
            Finally
                moGridForm.Freeze(False)
            End Try
        End Sub

        Public Sub doMarkSelectionAsJournal()
            moGridForm.Freeze(True)
            Try
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                Dim oToRemove As New ArrayList

                oSelected = getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    Dim iRow As Integer
                    Dim bFound As Boolean = False
                    Dim iIndex As Integer
                    For iIndex = 0 To oSelected.Count - 1
                        iRow = getDataTableRowIndex(oSelected, iIndex)

                        Dim sLinkedWO As String = doGetFieldValue(msRowTableAlias & IDH_DISPROW._WRRow, iRow)
                        If msWR1OrdType = "DO" AndAlso sLinkedWO IsNot Nothing AndAlso sLinkedWO.Length > 0 Then
                            oToRemove.Add(iRow)
                        Else
                            Dim sStatus As String
                            'Dim dValue As Double
                            'Dim bCanDoZero As Boolean = Config.INSTANCE.getParameterAsBool("MDALLZE", False)
                            'If bDoSO Then
                            '    dValue = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._Total)
                            '    If dValue <> 0 OrElse bCanDoZero Then
                            '    End If
                            'End If
                            'If bDoPO Then
                            '    dValue = doGetFieldValue(msRowTableAlias & IDH_JOBSHD._JCost)
                            '    If dValue <> 0 OrElse bCanDoZero Then
                            '    End If
                            'End If

                            sStatus = doGetFieldValue(msRowTableAlias & "U_JStat", iRow)
                            If com.idh.bridge.lookups.FixedValues.isOpen(sStatus) OrElse _
                                com.idh.bridge.lookups.FixedValues.isWaitingForJournal(sStatus) OrElse _
                                com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse _
                                com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) OrElse _
                                com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) OrElse _
                                com.idh.bridge.lookups.FixedValues.isError(sStatus) Then
                                sStatus = com.idh.bridge.lookups.FixedValues.getDoJrnlStatus()
                                doSetFieldValue(msRowTableAlias & "U_JStat", iRow, sStatus)
                            Else
                                oToRemove.Add(iRow)
                            End If
                        End If
                    Next

                    Dim sParams() As String
                    Dim sMessageId As String = ""

                    'Remove to rows that need to be skipped
                    If oToRemove.Count() > 0 Then
                        If oToRemove.Count() > 1 Then
                            sMessageId = "MANJRNLS"
                            sParams = New String() {oToRemove.Count().ToString(), General.doCreateIntCSL(oToRemove, True)}
                        Else
                            sMessageId = "MANJRNL"
                            sParams = New String() {General.doCreateIntCSL(oToRemove, True)}
                        End If
                        Messages.INSTANCE.doResourceMessage(sMessageId, sParams, 1, com.idh.bridge.Translation.getTranslatedWord("Ok"))
                        For iIndex = 0 To oToRemove.Count - 1
                            oSelected.Remove(oToRemove(iIndex))
                        Next
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error marking for Journals.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXMFJL", {Nothing})
            Finally
                moGridForm.Freeze(False)
            End Try
        End Sub

        '**
        ' Calculate the totals for the rows in the grid
        '**
        Public Function doTotals(Optional ByVal bDoZero As Boolean = False) As RowGridTotals
            Dim oRowTotals As New RowGridTotals()
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) OrElse _
                         Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) OrElse _
                         Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
                If bDoZero = False Then
                    Dim iCount As Integer = getRowCount()
                    Dim iIndex As Integer
                    Dim oObject As Object
                    Dim dSubAftDisc As Double = 0
                    If iCount > 0 Then
                        For iRow As Integer = 0 To iCount - 1
                            oRowTotals.CstWgtAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._CstWgt, True), iRow))
                            'oRowTotals.RdWgtAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._RdWgt), iRow))
                            oRowTotals.RdWgtAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._RdWgt, True), iRow))

                            oRowTotals.TotalAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._Total, True), iRow))
                            iIndex = doIndexField("SubAftDisc")
                            oObject = moDataTable.GetValue(iIndex, iRow)
                            dSubAftDisc = com.idh.utils.Conversions.ToDouble(oObject)
                            oRowTotals.AftDiscAdd(dSubAftDisc)
                            oRowTotals.DisAmtAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._DisAmt, True), iRow))
                            oRowTotals.AddExAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._AddEx, True), iRow))
                            oRowTotals.TxAmtAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._TaxAmt, True), iRow)) 'Tax on Change 

                            oRowTotals.TCTotalAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._TCTotal, True), iRow))
                            oRowTotals.PriceAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._Price, True), iRow))

                            oRowTotals.HaulageCostAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._OrdTot, True), iRow))
                            oRowTotals.ProducerCostAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._PCTotal, True), iRow))
                            oRowTotals.DisposalCostAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._TipTot, True), iRow))

                            oRowTotals.TotalJCostAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._JCost, True), iRow))
                            oRowTotals.AdditionalCostAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._AddCost, True), iRow))
                            oRowTotals.AdditionalChargeAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._AddCharge, True), iRow))
                            oRowTotals.TxAmtCostAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._VtCostAmt, True), iRow))

                            oRowTotals.ChargeDedAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._ValDed, True), iRow))

                        Next
                    End If
                End If
            End If
            Return oRowTotals
        End Function

        Public Function doTotalsNew(Optional ByVal bDoZero As Boolean = False) As RowGridTotals
            Dim oRowTotals As New RowGridTotals()
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
                If bDoZero = False Then
                    Dim sLastSQL As String = moGridControl.getLastGeneratedQuery
                    Dim sLastColIndex As Int16 = sLastSQL.ToUpper.LastIndexOf("As Col")
                    If sLastColIndex > -1 AndAlso sLastSQL.ToUpper.LastIndexOf(" FROM ") > -1 Then
                        sLastSQL = sLastSQL.Substring(sLastSQL.ToUpper.LastIndexOf(" FROM ", sLastColIndex))

                        If sLastSQL.ToUpper.LastIndexOf(" ORDER BY ") > -1 Then
                            sLastSQL = sLastSQL.Substring(0, sLastSQL.ToUpper.LastIndexOf(" ORDER BY "))
                        End If

                        Dim msRowTableAlias_local As String = msRowTableAlias.Replace(".", "")
                        Dim sQry As String = "Select Top " & moGridControl.CurrentMaxRows & "  (" & msRowTableAlias & IDH_JOBSHD._CstWgt & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._CstWgt & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._RdWgt & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._RdWgt & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._Total & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._Total & ", " _
                                             & " (CAST(((U_TCTotal + U_Price) - r.U_DisAmt) As Numeric(19,2))) As SubAftDisc, " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._DisAmt & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._DisAmt & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._AddEx & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._AddEx & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._TaxAmt & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._TaxAmt & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._TCTotal & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._TCTotal & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._Price & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._Price & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._OrdTot & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._OrdTot & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._PCTotal & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._PCTotal & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._TipTot & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._TipTot & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._JCost & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._JCost & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._AddCost & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._AddCost & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._AddCharge & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._AddCharge & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._VtCostAmt & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._VtCostAmt & ", " _
                                             & " (" & msRowTableAlias & IDH_JOBSHD._ValDed & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._ValDed & " " _
                                        & sLastSQL
                        sQry = "Select Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._CstWgt & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._CstWgt & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._RdWgt & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._RdWgt & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._Total & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._Total & ", " _
                                             & " Sum(SubAftDisc) As SubAftDisc, " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._DisAmt & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._DisAmt & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._AddEx & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._AddEx & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._TaxAmt & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._TaxAmt & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._TCTotal & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._TCTotal & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._Price & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._Price & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._OrdTot & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._OrdTot & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._PCTotal & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._PCTotal & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._TipTot & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._TipTot & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._JCost & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._JCost & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._AddCost & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._AddCost & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._AddCharge & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._AddCharge & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._VtCostAmt & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._VtCostAmt & ", " _
                                             & " Sum(" & msRowTableAlias_local & "_" & IDH_JOBSHD._ValDed & ") As " & msRowTableAlias_local & "_" & IDH_JOBSHD._ValDed & " " _
                                             & " From (" & sQry & ")T"
                        Dim oRecordset As SAPbobsCOM.Recordset = Nothing
                        oRecordset = IDHAddOns.idh.addon.Base.PARENT.goDB.doSelectQuery(sQry)
                        If Not oRecordset Is Nothing AndAlso oRecordset.RecordCount > 0 Then
                            oRowTotals.CstWgtAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._CstWgt).Value)
                            oRowTotals.RdWgtAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._RdWgt).Value)


                            oRowTotals.TotalAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._Total).Value)

                            oRowTotals.AftDiscAdd(oRecordset.Fields.Item("SubAftDisc").Value)
                            oRowTotals.DisAmtAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._DisAmt).Value)
                            oRowTotals.AddExAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._AddEx).Value)
                            oRowTotals.TxAmtAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._TaxAmt).Value) 'Tax on Change 

                            oRowTotals.TCTotalAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._TCTotal).Value)
                            oRowTotals.PriceAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._Price).Value)

                            oRowTotals.HaulageCostAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._OrdTot).Value)
                            oRowTotals.ProducerCostAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._PCTotal).Value)
                            oRowTotals.DisposalCostAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._TipTot).Value)

                            oRowTotals.TotalJCostAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._JCost).Value)
                            oRowTotals.AdditionalCostAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._AddCost).Value)
                            oRowTotals.AdditionalChargeAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._AddCharge).Value)
                            oRowTotals.TxAmtCostAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._VtCostAmt).Value)

                            oRowTotals.ChargeDedAdd(oRecordset.Fields.Item(msRowTableAlias_local & "_" & IDH_JOBSHD._ValDed).Value)
                            Return oRowTotals
                        End If
                    End If
                    ''This will remove after final testing/( or will keep as in case of failer this part can be use( to be final)
                    Dim iCount As Integer = getRowCount()
                    Dim iIndex As Integer
                    Dim oObject As Object
                    Dim dSubAftDisc As Double = 0
                    If iCount > 0 Then
                        For iRow As Integer = 0 To iCount - 1
                            oRowTotals.CstWgtAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._CstWgt, True), iRow))
                            'oRowTotals.RdWgtAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._RdWgt), iRow))
                            oRowTotals.RdWgtAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._RdWgt, True), iRow))


                            oRowTotals.TotalAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._Total, True), iRow))
                            iIndex = doIndexField("SubAftDisc")
                            oObject = moDataTable.GetValue(iIndex, iRow)
                            dSubAftDisc = com.idh.utils.Conversions.ToDouble(oObject)
                            oRowTotals.AftDiscAdd(dSubAftDisc)
                            oRowTotals.DisAmtAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._DisAmt, True), iRow))
                            oRowTotals.AddExAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._AddEx, True), iRow))
                            oRowTotals.TxAmtAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._TaxAmt, True), iRow)) 'Tax on Change 

                            oRowTotals.TCTotalAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._TCTotal, True), iRow))
                            oRowTotals.PriceAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._Price, True), iRow))

                            oRowTotals.HaulageCostAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._OrdTot, True), iRow))
                            oRowTotals.ProducerCostAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._PCTotal, True), iRow))
                            oRowTotals.DisposalCostAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._TipTot, True), iRow))

                            oRowTotals.TotalJCostAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._JCost, True), iRow))
                            oRowTotals.AdditionalCostAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._AddCost, True), iRow))
                            oRowTotals.AdditionalChargeAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._AddCharge, True), iRow))
                            oRowTotals.TxAmtCostAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._VtCostAmt, True), iRow))

                            oRowTotals.ChargeDedAdd(moDataTable.GetValue(doIndexField(msRowTableAlias & IDH_JOBSHD._ValDed, True), iRow))

                        Next
                    End If
                End If
            End If
            Return oRowTotals
        End Function

        '***
        '#MA WO billing Managers Processing for PO. If BP is Intra Company. 
        ' issue:324 20170426
        '***
        Public Function doProcessPOForIntraComapny() As Boolean
            Dim sParams() As String
            Dim oToRemove As New ArrayList
            Dim oBPLists As New ArrayList
            Dim oSelected As SAPbouiCOM.SelectedRows
            oSelected = getGrid().Rows.SelectedRows()
            If Not oSelected Is Nothing Then
                Dim iRow As Integer
                Dim iIndex As Integer
                For iIndex = 0 To oSelected.Count - 1
                    iRow = getDataTableRowIndex(oSelected, iIndex)
                    Dim sProducder As String = doGetFieldValue("r.U_ProCd", iRow) 'msRowTableAlias
                    Dim sDosposalSite As String = doGetFieldValue("r.U_Tip", iRow) 'msRowTableAlias
                    Dim sHaulier As String = doGetFieldValue("r.U_CarrCd", iRow) 'msRowTableAlias
                    'Dim val As String = doGetFieldValue(msRowTableAlias & IDH_JOBENTR._CardCd, iRow)
                    If (Not String.IsNullOrWhiteSpace(sProducder) AndAlso Config.INSTANCE.doCheckIntraCompany(sProducder)) _
                        AndAlso (Not String.IsNullOrWhiteSpace(sDosposalSite) AndAlso Config.INSTANCE.doCheckIntraCompany(sDosposalSite)) _
                        AndAlso (Not String.IsNullOrWhiteSpace(sHaulier) AndAlso Config.INSTANCE.doCheckIntraCompany(sHaulier)) Then
                        'andalso Config.INSTANCE.doCheckIntraCompany(sProducder) AndAlso Config.INSTANCE.doCheckIntraCompany(sDosposalSite) AndAlso Config.INSTANCE.doCheckIntraCompany(sHaulier) Then
                        If (Not oBPLists.Contains(sProducder)) Then
                            oBPLists.Add(sProducder)
                        End If
                        If (Not oBPLists.Contains(sDosposalSite)) Then
                            oBPLists.Add(sDosposalSite)
                        End If
                        If (Not oBPLists.Contains(sHaulier)) Then
                            oBPLists.Add(sHaulier)
                        End If
                        oToRemove.Add(iRow)
                    ElseIf (Not String.IsNullOrWhiteSpace(sProducder) AndAlso Not Config.INSTANCE.doCheckIntraCompany(sProducder)) _
                    OrElse (Not String.IsNullOrWhiteSpace(sDosposalSite) AndAlso Not Config.INSTANCE.doCheckIntraCompany(sDosposalSite)) _
                    OrElse (Not String.IsNullOrWhiteSpace(sHaulier) AndAlso Not Config.INSTANCE.doCheckIntraCompany(sHaulier)) Then
                        'andalso Config.INSTANCE.doCheckIntraCompany(sProducder) AndAlso Config.INSTANCE.doCheckIntraCompany(sDosposalSite) AndAlso Config.INSTANCE.doCheckIntraCompany(sHaulier) Then
                        If (Not String.IsNullOrWhiteSpace(sProducder) AndAlso Not oBPLists.Contains(sProducder) AndAlso Config.INSTANCE.doCheckIntraCompany(sProducder)) Then
                            oBPLists.Add(sProducder)
                        End If
                        If (Not String.IsNullOrWhiteSpace(sDosposalSite) AndAlso Not oBPLists.Contains(sDosposalSite) AndAlso Config.INSTANCE.doCheckIntraCompany(sDosposalSite)) Then
                            oBPLists.Add(sDosposalSite)
                        End If
                        If (Not String.IsNullOrWhiteSpace(sHaulier) AndAlso Not oBPLists.Contains(sHaulier) AndAlso Config.INSTANCE.doCheckIntraCompany(sHaulier)) Then
                            oBPLists.Add(sHaulier)
                        End If
                    Else
                        If (Not String.IsNullOrWhiteSpace(sProducder) AndAlso Not oBPLists.Contains(sProducder) AndAlso Config.INSTANCE.doCheckIntraCompany(sProducder)) Then
                            oBPLists.Add(sProducder)
                        End If
                        If (Not String.IsNullOrWhiteSpace(sDosposalSite) AndAlso Not oBPLists.Contains(sDosposalSite) AndAlso Config.INSTANCE.doCheckIntraCompany(sDosposalSite)) Then
                            oBPLists.Add(sDosposalSite)
                        End If
                        If (Not String.IsNullOrWhiteSpace(sHaulier) AndAlso Not oBPLists.Contains(sHaulier) AndAlso Config.INSTANCE.doCheckIntraCompany(sHaulier)) Then
                            oBPLists.Add(sHaulier)
                        End If
                        oToRemove.Add(iRow)
                    End If
                Next
                If oBPLists.Count > 0 Then
                    Dim i As Int32 = 0
                    Dim sBP As String = ""
                    For i = 0 To oBPLists.Count - 2
                        sBP &= oBPLists(i).ToString() & ","
                    Next
                    sBP &= oBPLists(i).ToString()
                    Dim sMessage As String = Messages.INSTANCE.getMessage("ERRJEINT", New String() {sBP}) '"Can not update closed WOrders: " + sOrder + "." + sOrderRow;
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Please check BP(s) intra company configuration.", "ERRJEINT", New String() {sBP})

                End If
                If oToRemove.Count > 0 Then
                    ' Dim sMessage As String = com.idh.bridge.Messages.INSTANCE.getMessage("ERRJEINT", New String() {"Suppliers"}) '"Can not update closed WOrders: " + sOrder + "." + sOrderRow;
                    'com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Please check BP(s) intra company configuration.", "ERRJEINT", New String() {"Suppliers"})

                    'sParams = New String() {General.doCreateIntCSL(oToRemove, True)}
                    'Messages.INSTANCE.doResourceMessage("ERRWOBLM", sParams, 1, com.idh.bridge.Translation.getTranslatedWord("Ok"))
                    For iIndex = 0 To oToRemove.Count - 1
                        oSelected.Remove(oToRemove(iIndex))
                    Next
                End If
            End If
            If oSelected.Count > 0 Then
                Return True
            End If
            Return False
        End Function

        '        Protected Overridable Function doSaveRowData() As Boolean
        '            Dim aRows As ArrayList = doGetChangedRows()
        '            If aRows Is Nothing OrElse aRows.Count() = 0 Then
        '            	Return False
        '            End If
        '
        '            Dim sField As String = ""
        '            Dim oValue As Object = ""
        '            Dim sKey1 As String = ""
        '            Dim sKey2 As String = ""
        '
        '            Dim oAuditObj1 As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_JOBSHD")
        '            Dim oAuditObj2 As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_JOBENTR")
        '            Try
        '                Dim iResult As Integer
        '                Dim sResult As String = Nothing
        '
        '                Dim aChangedFields As ArrayList
        '                If Not aRows Is Nothing Then
        '                    For iIndex As Integer = 0 To aRows.Count - 1
        '                        setCurrentLine(aRows(iIndex))
        '                        sKey1 = doGetFieldValue("r.Code")
        '                        sKey2 = doGetFieldValue("r.U_JobNr")
        '
        '                        If oAuditObj1.setKeyPair(sKey1, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) AndAlso _
        '                           oAuditObj2.setKeyPair(sKey2, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then
        '                            aChangedFields = doGetChangedFields(aRows(iIndex))
        '                            For iField As Integer = 0 To aChangedFields.Count - 1
        '                                sField = aChangedFields.Item(iField)
        '                                oValue = doGetFieldValue(sField)
        '
        '                                If sField.StartsWith("r.") Then
        '                                    sField = sField.Substring(2)
        '                                    oAuditObj1.setFieldValue(sField, oValue)
        '                                ElseIf sField.StartsWith("e.") Then
        '                                    sField = sField.Substring(2)
        '                                    oAuditObj2.setFieldValue(sField, oValue)
        '                                End If
        '                            Next
        '
        '                            iResult = oAuditObj1.Commit()
        '                            If iResult <> 0 Then
        '                                goParent.goDICompany.GetLastError(iResult, sResult)
        '                                com.idh.bridge.DataHandler.INSTANCE.doError("Error saving the Order rows: " + sResult, "Error saving the Order rows - " & sKey1)
        '                            End If
        '                            iResult = oAuditObj2.Commit()
        '                            If iResult <> 0 Then
        '                                goParent.goDICompany.GetLastError(iResult, sResult)
        '                                com.idh.bridge.DataHandler.INSTANCE.doError("Error saving the Order rows: " + sResult, "Error saving the Order rows - " & sKey2)
        '                            End If
        '                        End If
        '                    Next
        '                End If
        '            Catch ex As Exception
        '                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error saving the Order rows - " & sKey1 & "." & sField & "." & oValue)
        '            Finally
        '                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj1)
        '            End Try
        '            Return True
        '        End Function

        ''** Lookup Selected Row prics and Recalculate values
        ''** sHeadTable - @....
        ''** sRowTable - @...
        ''** sWR1OrdType - WO/DO
        'Public Function doRecalcValues(ByVal sHeadTable As String, _
        '        ByVal sRowTable As String, _
        '        ByVal sWR1OrdType As String) As Boolean

        '    msHeadTable = sHeadTable
        '    msRowTable = sRowTable
        '    msWR1OrdType = sWR1OrdType

        '    Try
        '        'Step through the selected rows and update
        '        Dim oSelected As SAPbouiCOM.SelectedRows
        '        oSelected = getGrid().Rows.SelectedRows()
        '        If oSelected Is Nothing OrElse oSelected.Count = 0 Then
        '            'com.idh.bridge.DataHandler.INSTANCE.doError("No rows selected.")
        '            com.idh.bridge.DataHandler.INSTANCE.doResUserError("No rows selected.", "ERUSROWS", {Nothing})
        '            Return False
        '        Else
        '            Dim iRow As Integer
        '            Dim sRow As String
        '            Dim iSelected As Integer = oSelected.Count
        '            Dim sRowList As String = ""
        '            For iIndex As Integer = 0 To iSelected - 1
        '                iRow = GetDataTableRowIndex(oSelected, iIndex)

        '                sRow = doGetFieldValue(msRowTableAlias & "Code", iRow)
        '                If sRowList.Length > 0 Then
        '                    sRowList = sRowList & "'"
        '                End If
        '                sRowList = sRowList & "'" & sRow & "'"
        '                'doRecalcValues(sRow)
        '            Next

        '            If sWR1OrdType = "DO" Then
        '                Dim oDOR As IDH_DISPROW = New IDH_DISPROW()
        '                If oDOR.getData(IDH_DISPROW._Code & " IN (" & sRowList & ")", Nothing) > 0 Then
        '                    While oDOR.next
        '                        oDOR.doGetAllPrices()
        '                    End While
        '                End If
        '                oDOR.doProcessData()
        '            Else
        '                Dim oWOR As IDH_JOBSHD = New IDH_JOBSHD()
        '                If oWOR.getData(IDH_JOBSHD._Code & " IN (" & sRowList & ")", Nothing) > 0 Then
        '                    While oWOR.next
        '                        oWOR.doGetAllPrices()
        '                    End While
        '                End If
        '                oWOR.doProcessData()
        '            End If
        '        End If
        '    Catch ex As Exception
        '        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error recalculating the selected rows.")
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRROW", {Nothing})
        '    End Try
        '    Return True
        'End Function

        ''** Lookup Selected Row prics and Recalculate values
        'Private Function doRecalcValues(ByVal sRowCode As String) As Boolean
        '    Dim oTableObj As New IDHAddOns.idh.data.AuditObject(IDHAddOns.idh.addon.Base.PARENT, msRowTable)
        '    Try
        '        Dim iwResult As Integer
        '        Dim swResult As String = Nothing

        '        If oTableObj.setKeyPair(sRowCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then
        '            oTableObj.setName(sRowCode)

        '            doGetCosts(oTableObj)
        '            doGetCharges(oTableObj)

        '            iwResult = oTableObj.Commit()
        '            If iwResult <> 0 Then
        '                IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetLastError(iwResult, swResult)
        '                'com.idh.bridge.DataHandler.INSTANCE.doError("Error Updating the Row Values: " + swResult)
        '                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Updating the Row Values: " + swResult, "ERSYUROW", {swResult})
        '            End If
        '        End If

        '    Catch ex As Exception
        '        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error recalculating the values.")
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRROW", {Nothing})
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oTableObj)
        '    End Try
        '    Return True
        'End Function

        'Private Sub doGetCosts(ByRef oRowAuditObj As IDHAddOns.idh.data.AuditObject)
        '    Try
        '        Dim sTipCode As String = oRowAuditObj.getFieldValue("U_Tip")
        '        Dim sCarrierCode As String = oRowAuditObj.getFieldValue("U_CarrCd")
        '        Dim sItemCode As String = oRowAuditObj.getFieldValue("U_ItemCd")
        '        Dim sItemGrp As String = oRowAuditObj.getFieldValue("U_ItmGrp")
        '        Dim sJobType As String = oRowAuditObj.getFieldValue("U_JobTp")
        '        Dim dTipWeight As Double = oRowAuditObj.getFieldValue("U_TipWgt")
        '        Dim dHaulWeight As Double = oRowAuditObj.getFieldValue("U_OrdWgt")
        '        Dim sWastCd As String = oRowAuditObj.getFieldValue("U_WasCd")
        '        Dim sCustCd As String = oRowAuditObj.getFieldValue("U_CustCd")
        '        Dim sPUOM As String = oRowAuditObj.getFieldValue("U_PUOM")

        '        Dim sProducer As String = oRowAuditObj.getFieldValue(IDH_DISPROW._ProCd)
        '        'Dim sProducer As String = oRowAuditObj.getFieldValue("U_ProCd")

        '        '** Get Customer Address
        '        Dim sAddress As String = Nothing
        '        Dim sZipCode As String = Nothing
        '        Dim sBranch As String

        '        Dim sJobNr As String = oRowAuditObj.getFieldValue("U_JobNr")
        '        Dim sQry As String = "Select U_ADDRESS, U_ZpCd, U_PCardCd FROM [" & msHeadTable & "] WHERE CODE = '" & sJobNr & "'"

        '        Dim iPrcSet As Integer = oRowAuditObj.getFieldValue("U_MANPRC")

        '        Dim oRecordset As SAPbobsCOM.Recordset = Nothing
        '        oRecordset = IDHAddOns.idh.addon.Base.PARENT.goDB.doSelectQuery(sQry)
        '        If Not oRecordset Is Nothing AndAlso oRecordset.RecordCount > 0 Then
        '            sAddress = oRecordset.Fields.Item(0).Value.Trim()
        '            sZipCode = oRecordset.Fields.Item(1).Value.Trim()
        '            'sProducer = oRecordset.Fields.Item(2).Value.Trim()
        '        End If
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordset)

        '        sBranch = oRowAuditObj.getFieldValue("U_Branch")

        '        Dim dDocDate As Date
        '        Dim dTipCost As Double
        '        Dim dProcucerCost As Double
        '        Dim dHaulCost As Double
        '        Dim dTipTotal As Double = 0
        '        Dim dProducerTotal As Double = 0
        '        Dim dHaulTotal As Double = 0
        '        Dim sCalcType As String = ""
        '        Dim sHCalcType As String = ""
        '        Dim dLicTotal As Double = 0
        '        Dim bTPriceFound As Boolean = True
        '        Dim bHPriceFound As Boolean = True
        '        Dim dTipVatRate As Double = 0
        '        Dim dProducerVatRate As Double = 0
        '        Dim dHaulageVatRate As Double = 0

        '        If msWR1OrdType = "DO" Then
        '            dDocDate = oRowAuditObj.getFieldValue("U_BDate")
        '        Else
        '            dDocDate = oRowAuditObj.getFieldValue("U_RDate")
        '        End If

        '        Dim oPrices As com.idh.dbObjects.User.Prices 'ArrayList
        '        'Producer price
        '        If sTipCode.Length > 0 AndAlso sItemCode.Length > 0 Then
        '            'If msWR1OrdType = "DO" Then
        '            oPrices = Config.INSTANCE.doGetJobCostPrice(msWR1OrdType, sJobType, sItemGrp, sItemCode, sProducer, dTipWeight, sPUOM, sWastCd, sCustCd, sAddress, dDocDate, sBranch, sZipCode)
        '            'Else
        '            '    oPrices = Config.INSTANCE.doGetJobCostPrice(msWR1OrdType, sJobType, sItemGrp, sItemCode, sTipCode, dTipWeight, sPUOM, sWastCd, sCustCd, sAddress, dDocDate, sBranch, sZipCode)
        '            'End If
        '            bTPriceFound = oPrices.Found

        '            dTipCost = oPrices.TipPrice
        '            sCalcType = oPrices.TipChargeCalc

        '            dTipVatRate = oPrices.TipVat

        '            oRowAuditObj.setFieldValue("U_TCostVtRt", dTipVatRate)
        '            oRowAuditObj.setFieldValue("U_TCostVtGrp", oPrices.TipVatGroup)
        '        Else
        '            dTipCost = 0
        '            sCalcType = "VARIABLE"

        '            oRowAuditObj.setFieldValue("U_TCostVtRt", 0)
        '            oRowAuditObj.setFieldValue("U_TCostVtGrp", "")
        '        End If

        '        If sCalcType.ToUpper().Equals("FIXED") Then
        '            If dTipWeight = 0 Then
        '                dTipTotal = 0
        '            Else
        '                dTipTotal = dTipCost
        '            End If
        '        Else
        '            dTipTotal = dTipCost * dTipWeight
        '        End If

        '        'Disposal cost
        '        If sTipCode.Length > 0 AndAlso sItemCode.Length > 0 Then
        '            'If msWR1OrdType = "DO" Then
        '            oPrices = Config.INSTANCE.doGetJobCostPrice(msWR1OrdType, sJobType, sItemGrp, sItemCode, sProducer, dTipWeight, sPUOM, sWastCd, sCustCd, sAddress, dDocDate, sBranch, sZipCode)
        '            'Else
        '            '   oPrices = Config.INSTANCE.doGetJobCostPrice(msWR1OrdType, sJobType, sItemGrp, sItemCode, sTipCode, dTipWeight, sPUOM, sWastCd, sCustCd, sAddress, dDocDate, sBranch, sZipCode)
        '            'End If
        '            bTPriceFound = oPrices.Found

        '            dTipCost = oPrices.TipPrice
        '            sCalcType = oPrices.TipChargeCalc

        '            dTipVatRate = oPrices.TipVat

        '            oRowAuditObj.setFieldValue("U_TCostVtRt", dTipVatRate)
        '            oRowAuditObj.setFieldValue("U_TCostVtGrp", oPrices.TipVatGroup)
        '        Else
        '            dTipCost = 0
        '            sCalcType = "VARIABLE"

        '            oRowAuditObj.setFieldValue("U_TCostVtRt", 0)
        '            oRowAuditObj.setFieldValue("U_TCostVtGrp", "")
        '        End If

        '        If sCalcType.ToUpper().Equals("FIXED") Then
        '            If dTipWeight = 0 Then
        '                dTipTotal = 0
        '            Else
        '                dTipTotal = dTipCost
        '            End If
        '        Else
        '            dTipTotal = dTipCost * dTipWeight
        '        End If

        '        If sCarrierCode.Length > 0 AndAlso sItemCode.Length > 0 Then
        '            oPrices = Config.INSTANCE.doGetJobCostPrice(msWR1OrdType, sJobType, sItemGrp, sItemCode, sCarrierCode, dHaulWeight, sPUOM, sWastCd, sCustCd, sAddress, dDocDate, sBranch, sZipCode)
        '            bHPriceFound = oPrices.Found

        '            dHaulCost = oPrices.HaulPrice
        '            sHCalcType = oPrices.HaulageChargeCalc

        '            dHaulageVatRate = oPrices.HaulageVat
        '            oRowAuditObj.setFieldValue("U_HCostVtRt", dHaulageVatRate)
        '            oRowAuditObj.setFieldValue("U_HCostVtGrp", oPrices.HaulageVatGroup)
        '        Else
        '            dHaulCost = 0
        '            sHCalcType = "STANDARD"

        '            oRowAuditObj.setFieldValue("U_HCostVtRt", 0)
        '            oRowAuditObj.setFieldValue("U_HCostVtGrp", "")
        '        End If

        '        If bHPriceFound Then
        '            If sHCalcType.ToUpper().Equals("FIXED") Then
        '                If dHaulWeight = 0 Then
        '                    dHaulTotal = 0
        '                Else
        '                    dHaulTotal = dHaulCost
        '                End If
        '            Else
        '                dHaulTotal = dHaulCost * dHaulWeight
        '            End If
        '        End If

        '        Dim dTAddCost As Double = oRowAuditObj.getFieldValue("U_TAddCost")
        '        Dim dTipVat As Double = dTipTotal * (dTipVatRate / 100)
        '        Dim dHaulageVat As Double = dHaulTotal * (dHaulageVatRate / 100)

        '        If bTPriceFound Then
        '            oRowAuditObj.setFieldValue("U_TipCost", dTipCost)
        '            iPrcSet = iPrcSet And (Not Config.MASK_TIPCST)
        '        Else
        '            dTipCost = oRowAuditObj.getFieldValue("U_TipCost")
        '        End If

        '        If bHPriceFound Then
        '            oRowAuditObj.setFieldValue("U_OrdCost", dHaulCost)
        '            iPrcSet = iPrcSet And (Not Config.MASK_HAULCST)
        '        Else
        '            dHaulCost = oRowAuditObj.getFieldValue("U_OrdCost")
        '        End If

        '        oRowAuditObj.setFieldValue("U_MANPRC", iPrcSet)
        '        oRowAuditObj.setFieldValue("U_TipTot", dTipTotal)
        '        oRowAuditObj.setFieldValue("U_OrdTot", dHaulTotal)
        '        oRowAuditObj.setFieldValue("U_JCost", (dTipTotal + dHaulTotal + dLicTotal + dTAddCost + dTipVat + dHaulageVat))

        '    Catch ex As Exception
        '        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error get the costs.")
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGCST", {Nothing})
        '    End Try
        'End Sub

        'Private Sub doGetCharges(ByRef oRowAuditObj As IDHAddOns.idh.data.AuditObject)
        '    Try
        '        Dim sCardCode As String = oRowAuditObj.getFieldValue("U_CustCd")
        '        Dim sCarrierCode As String = oRowAuditObj.getFieldValue("U_CarrCd")
        '        Dim sItemCode As String = oRowAuditObj.getFieldValue("U_ItemCd")
        '        Dim sItemGrp As String = oRowAuditObj.getFieldValue("U_ItmGrp")
        '        Dim sWastCd As String = oRowAuditObj.getFieldValue("U_WasCd")
        '        Dim sJobType As String = oRowAuditObj.getFieldValue("U_JobTp")
        '        'Dim sDocDate As String = oRowAuditObj.getFieldValue("U_RDate")
        '        Dim dDocDate As Date ' = oRowAuditObj.getFieldValue("U_RDate") 'com.idh.utils.dates.doStrToDate(sDocDate)

        '        '** Get Customer Address
        '        Dim sAddress As String = Nothing
        '        Dim sZipCode As String = Nothing
        '        Dim sBranch As String = oRowAuditObj.getFieldValue("U_Branch") 'com.idh.utils.dates.doStrToDate(sDocDate)
        '        Dim sJobNr As String = oRowAuditObj.getFieldValue("U_JobNr")
        '        Dim sQry As String = "Select U_ADDRESS, U_ZpCd FROM [" & msHeadTable & "] WHERE CODE = '" & sJobNr & "'"

        '        Dim oRecordset As SAPbobsCOM.Recordset = Nothing
        '        oRecordset = IDHAddOns.idh.addon.Base.PARENT.goDB.doSelectQuery(sQry)
        '        If Not oRecordset Is Nothing AndAlso oRecordset.RecordCount > 0 Then
        '            sAddress = oRecordset.Fields.Item(0).Value.Trim()
        '            sZipCode = oRecordset.Fields.Item(1).Value.Trim()
        '        End If
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordset)

        '        If msWR1OrdType = "DO" Then
        '            If Config.Parameter("MDDODI").ToUpper().Equals("TRUE") Then
        '                sItemCode = Config.Parameter("MDDODE")
        '            Else
        '                If sItemCode.Length = 0 Then
        '                    sItemCode = Config.Parameter("MDDODE")
        '                End If
        '            End If
        '            dDocDate = oRowAuditObj.getFieldValue("U_BDate")
        '        Else
        '            dDocDate = oRowAuditObj.getFieldValue("U_RDate")
        '        End If

        '        Dim sCustUOM As String = Config.INSTANCE.getBPUOM(sCardCode)
        '        Dim sDefUOM As String = Config.INSTANCE.getDefaultUOM()
        '        Dim sCIPUom As String
        '        Dim sUOM As String

        '        sCIPUom = Config.INSTANCE.getCIPAnyUOM(msWR1OrdType, sJobType, sItemGrp, sItemCode, sCardCode, sWastCd, sAddress, dDocDate, sBranch, sZipCode)
        '        If Not sCIPUom Is Nothing Then
        '            oRowAuditObj.setFieldValue("U_UOM", sCIPUom)
        '            sUOM = sCIPUom
        '        Else
        '            sUOM = oRowAuditObj.getFieldValue("U_UOM")
        '        End If

        '        Dim dReadWeight As Double = 0
        '        If sCustUOM.Equals(sDefUOM) = False OrElse Not sCIPUom Is Nothing OrElse sUOM.Equals(sDefUOM) = False Then
        '            dReadWeight = oRowAuditObj.getFieldValue("U_AUOMQt")
        '        End If
        '        If dReadWeight = 0 Then
        '            dReadWeight = oRowAuditObj.getFieldValue("U_RdWgt")

        '            If dReadWeight <> 0 AndAlso msWR1OrdType.Equals("DO") = True AndAlso sUOM.ToUpper().Equals("KG") = False Then
        '                dReadWeight = Config.INSTANCE.doConvertWeights("kg", sUOM, dReadWeight)
        '            End If
        '        End If

        '        Dim iPrcSet As Integer = oRowAuditObj.getFieldValue("U_MANPRC")
        '        Dim dHaulChrg As Double
        '        Dim dTipCharge As Double
        '        Dim sTipChargCalcType As String
        '        Dim sHaulageChargCalcType As String
        '        Dim dTipOffset As Double
        '        Dim dHaulageOffset As Double

        '        Dim dTipVatRate As Double = 0
        '        Dim sTipVatGrp As String = ""
        '        Dim dHaulageVatRate As Double = 0
        '        Dim sHaulageVatGrp As String = ""
        '        Dim dAddCharges As Double = oRowAuditObj.getFieldValue("U_TAddChrg")

        '        Dim oPrices As com.idh.dbObjects.User.Prices 'ArrayList
        '        If sCardCode.Length > 0 AndAlso sItemCode.Length > 0 Then
        '            oPrices = Config.INSTANCE.doGetJobChargePrices(msWR1OrdType, sJobType, sItemGrp, sItemCode, sCardCode, dReadWeight, sUOM, sWastCd, sCarrierCode, sAddress, dDocDate, sBranch, sZipCode)

        '            dHaulChrg = oPrices.HaulPrice
        '            dTipCharge = oPrices.TipPrice
        '            dTipVatRate = oPrices.TipVat
        '            sTipVatGrp = oPrices.TipVatGroup

        '            dHaulageVatRate = oPrices.HaulageVat
        '            sHaulageVatGrp = oPrices.HaulageVatGroup

        '            sTipChargCalcType = oPrices.TipChargeCalc
        '            sHaulageChargCalcType = oPrices.HaulageChargeCalc
        '            dTipOffset = oPrices.TipOffsetWeight
        '            dHaulageOffset = oPrices.HaulageOffsetWeight
        '        Else
        '            dHaulChrg = 0
        '            dTipCharge = 0

        '            sTipChargCalcType = "VARIABLE"
        '            sHaulageChargCalcType = "STANDARD"
        '            dTipOffset = 0
        '            dHaulageOffset = 0
        '        End If

        '        Dim dTotal As Double = 0
        '        Dim dCustWeight As Double
        '        Dim dTipCustTotal As Double
        '        Dim dPrice As Double
        '        Dim dCusQty As Double
        '        Dim dBefDiscTotal As Double
        '        Dim dDiscountAmt As Double
        '        Dim dDiscnt As Double
        '        Dim dVatAmnt As Double
        '        Dim dDefWeight As Double = 0
        '        Dim sHaulAC As String

        '        If sHaulageChargCalcType.Equals("WEIGHT") Then
        '            sHaulAC = "Y"
        '        Else
        '            sHaulAC = "N"
        '        End If
        '        oRowAuditObj.setFieldValue("U_HaulAC", sHaulAC)

        '        dDiscnt = oRowAuditObj.getFieldValue("U_Discnt")
        '        dDiscountAmt = oRowAuditObj.getFieldValue("U_DisAmt")

        '        If sUOM.Length = 0 Then
        '            sUOM = Config.INSTANCE.getDefaultUOM()
        '        End If

        '        If dReadWeight <> 0 Then
        '            If sTipChargCalcType.ToUpper().Equals("OFFSET") Then
        '                If dTipOffset < dReadWeight Then
        '                    dCustWeight = dReadWeight - dTipOffset
        '                Else
        '                    dCustWeight = 0
        '                End If
        '            Else
        '                dCustWeight = dReadWeight
        '            End If
        '        Else
        '            dCustWeight = 0
        '        End If

        '        If sTipChargCalcType.ToUpper().Equals("FIXED") Then
        '            If dCustWeight = 0 Then
        '                dTipCustTotal = 0
        '            Else
        '                dTipCustTotal = dTipCharge
        '            End If
        '        Else
        '            dTipCustTotal = dTipCharge * dCustWeight
        '        End If

        '        'Haulage
        '        If sHaulAC = "Y" Then
        '            dCusQty = oRowAuditObj.getFieldValue("U_RdWgt")
        '        Else
        '            dCusQty = oRowAuditObj.getFieldValue("U_CusQty")
        '        End If
        '        oRowAuditObj.setFieldValue("U_HlSQty", dCusQty)

        '        dPrice = dCusQty * dHaulChrg
        '        dBefDiscTotal = dTipCustTotal + dPrice

        '        If dDiscnt = 0 AndAlso dDiscountAmt <> 0 Then
        '            dDiscnt = dDiscountAmt / dBefDiscTotal * 100
        '        ElseIf dDiscnt <> 0 Then
        '            dDiscountAmt = (dDiscnt / 100) * dBefDiscTotal
        '        End If

        '        Dim TSub As Double = dTipCustTotal - (dTipCustTotal * (dDiscnt / 100))
        '        Dim HSub As Double = dPrice - (dPrice * (dDiscnt / 100))
        '        Dim TVat As Double = TSub * (dTipVatRate / 100)
        '        Dim HVat As Double = HSub * (dHaulageVatRate / 100)

        '        dTotal = TSub + HSub
        '        dVatAmnt = TVat + HVat

        '        dTotal = dTotal + dVatAmnt + dAddCharges

        '        oRowAuditObj.setFieldValue("U_CstWgt", dCustWeight)
        '        oRowAuditObj.setFieldValue("U_Price", dPrice)
        '        oRowAuditObj.setFieldValue("U_Discnt", CStr(dDiscnt))
        '        oRowAuditObj.setFieldValue("U_DisAmt", CStr(dDiscountAmt))
        '        oRowAuditObj.setFieldValue("U_TaxAmt", dVatAmnt)
        '        oRowAuditObj.setFieldValue("U_TCTotal", CStr(dTipCustTotal))
        '        oRowAuditObj.setFieldValue("U_Total", CStr(dTotal))

        '        oRowAuditObj.setFieldValue("U_CusChr", dHaulChrg)
        '        iPrcSet = iPrcSet And (Not Config.MASK_HAULCHRG)

        '        oRowAuditObj.setFieldValue("U_TCharge", dTipCharge)
        '        iPrcSet = iPrcSet And (Not Config.MASK_TIPCHRG)

        '        oRowAuditObj.setFieldValue("U_MANPRC", iPrcSet)

        '        oRowAuditObj.setFieldValue("U_TChrgVtRt", dTipVatRate)
        '        oRowAuditObj.setFieldValue("U_TChrgVtGrp", sTipVatGrp)

        '        oRowAuditObj.setFieldValue("U_HChrgVtRt", dHaulageVatRate)
        '        oRowAuditObj.setFieldValue("U_HChrgVtGrp", sHaulageVatGrp)
        '    Catch ex As Exception
        '        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error getting the charges.")
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGCHG", {Nothing})
        '    End Try
        'End Sub

        '		Private Sub doGetCharges(ByVal sAddress As String, ByVal sZipCode As String)
        '            Try
        '                Dim sCardCode As String = doGetFieldValue(IDH_JOBSHD._CustCd)
        '                Dim sItemCode As String = doGetFieldValue(IDH_JOBSHD._ItemCd)
        '                Dim sItemGrp As String = doGetFieldValue(IDH_JOBSHD._ItmGrp)
        '                Dim sWastCd As String = doGetFieldValue(IDH_JOBSHD._WasCd)
        '                Dim sJobType As String = doGetFieldValue(IDH_JOBSHD._JobTp)
        '                Dim dDocDate As Date ' = oRowAuditObj.getFieldValue("U_RDate") 'com.idh.utils.dates.doStrToDate(sDocDate)
        '
        '                '** Get Customer Address
        '                Dim sBranch As String = doGetFieldValue(IDH_JOBSHD._Branch)
        '                Dim sJobNr As String = doGetFieldValue(IDH_JOBSHD._JobNr)
        '
        '                If msWR1OrdType = "DO" Then
        '                    If Config.Parameter("MDDODI").ToUpper().Equals("TRUE") OrElse sItemCode.Length = 0 Then
        '                        sItemCode = Config.Parameter("MDDODE")
        '                    End If
        '                    dDocDate = doGetFieldValue(IDH_JOBSHD._BDate)
        '                Else
        '                    dDocDate = doGetFieldValue(IDH_JOBSHD._RDate)
        '                End If
        '
        '                Dim sCustUOM As String = Config.INSTANCE.getBPUOM(IDHAddOns.idh.addon.Base.PARENT, sCardCode)
        '                Dim sDefUOM As String = Config.INSTANCE.getDefaultUOM(IDHAddOns.idh.addon.Base.PARENT)
        '                Dim sCIPUom As String
        '                Dim sUOM As String
        '
        '                sCIPUom = Config.INSTANCE.getCIPOverridingUOM(msWR1OrdType, sJobType, sItemGrp, sItemCode, sCardCode, sWastCd, sAddress, dDocDate, sBranch, sZipCode)
        '                If Not sCIPUom Is Nothing Then
        '                    oRowAuditObj.setFieldValue("U_UOM", sCIPUom)
        '                    sUOM = sCIPUom
        '                Else
        '                    sUOM = doGetFieldValue(IDH_JOBSHD._UOM)
        '                End If
        '
        '                Dim dReadWeight As Double = 0
        '                If sCustUOM.Equals(sDefUOM) = False OrElse Not sCIPUom Is Nothing OrElse sUOM.Equals(sDefUOM) = False Then
        '                    dReadWeight = doGetFieldValue(IDH_JOBSHD._AUOMQt)
        '                End If
        '                If dReadWeight = 0 Then
        '                    dReadWeight = doGetFieldValue(IDH_JOBSHD._RdWgt)
        '
        '                    If dReadWeight <> 0 AndAlso msWR1OrdType.Equals("DO") = True AndAlso sUOM.ToUpper().Equals("KG") = False Then
        '                        dReadWeight = Config.INSTANCE.doConvertWeights(IDHAddOns.idh.addon.Base.PARENT, "kg", sUOM, dReadWeight)
        '                    End If
        '                End If
        '
        '                Dim iPrcSet As Integer = doGetFieldValue(IDH_JOBSHD._MANPRC)
        '                Dim dHaulChrg As Double
        '                Dim dTipCharge As Double
        '                Dim sTipChargCalcType As String
        '                Dim sHaulageChargCalcType As String
        '                Dim dTipOffset As Double
        '                Dim dHaulageOffset As Double
        '
        '                Dim dTipVatRate As Double = 0
        '                Dim sTipVatGrp As String = ""
        '                Dim dHaulageVatRate As Double = 0
        '                Dim sHaulageVatGrp As String = ""
        '                Dim dAddCharges As Double = doGetFieldValue(IDH_JOBSHD._TAddChrg)
        '
        '                Dim oPrices As com.idh.dbObjects.User.Price 'ArrayList
        '                If sCardCode.Length > 0 AndAlso sItemCode.Length > 0 Then
        '                    oPrices = Config.INSTANCE.doGetJobChargePrices(msWR1OrdType, sJobType, sItemGrp, sItemCode, sCardCode, dReadWeight, sUOM, sWastCd, sAddress, dDocDate, sBranch, sZipCode)
        '
        '                    dHaulChrg = oPrices.mdHaulPrice
        '                    dTipCharge = oPrices.mdTipPrice
        '                    dTipVatRate = oPrices.mdTipVat
        '                    sTipVatGrp = oPrices.msTipVatGroup
        '
        '                    dHaulageVatRate = oPrices.mdHaulageVat
        '                    sHaulageVatGrp = oPrices.msHaulageVatGroup
        '
        '                    sTipChargCalcType = oPrices.msTipChargeCalc
        '                    sHaulageChargCalcType = oPrices.msHaulageChargeCalc
        '                    dTipOffset = oPrices.mdTipOffsetWeight
        '                    dHaulageOffset = oPrices.mdHaulageOffsetWeight
        '                Else
        '                    dHaulChrg = 0
        '                    dTipCharge = 0
        '
        '                    sTipChargCalcType = "VARIABLE"
        '                    sHaulageChargCalcType = "STANDARD"
        '                    dTipOffset = 0
        '                    dHaulageOffset = 0
        '                End If
        '
        '                Dim dTotal As Double = 0
        '                Dim dCustWeight As Double
        '                Dim dTipCustTotal As Double
        '                Dim dPrice As Double
        '                Dim dCusQty As Double
        '                Dim dBefDiscTotal As Double
        '                Dim dDiscountAmt As Double
        '                Dim dDiscnt As Double
        '                Dim dVatAmnt As Double
        '                Dim dDefWeight As Double = 0
        '                Dim sHaulAC As String
        '
        '                If sHaulageChargCalcType.Equals("WEIGHT") Then
        '                    sHaulAC = "Y"
        '                Else
        '                    sHaulAC = "N"
        '                End If
        '                oRowAuditObj.setFieldValue("U_HaulAC", sHaulAC)
        '
        '                dDiscnt = doGetFieldValue(IDH_JOBSHD._Discnt)
        '                dDiscountAmt = doGetFieldValue(IDH_JOBSHD._DisAmt)
        '
        '                If sUOM.Length = 0 Then
        '                    sUOM = Config.INSTANCE.getDefaultUOM(IDHAddOns.idh.addon.Base.PARENT)
        '                End If
        '
        '                If dReadWeight <> 0 Then
        '                    If sTipChargCalcType.ToUpper().Equals("OFFSET") Then
        '                        If dTipOffset < dReadWeight Then
        '                            dCustWeight = dReadWeight - dTipOffset
        '                        Else
        '                            dCustWeight = 0
        '                        End If
        '                    Else
        '                        dCustWeight = dReadWeight
        '                    End If
        '                Else
        '                    dCustWeight = 0
        '                End If
        '
        '                If sTipChargCalcType.ToUpper().Equals("FIXED") Then
        '                    If dCustWeight = 0 Then
        '                        dTipCustTotal = 0
        '                    Else
        '                        dTipCustTotal = dTipCharge
        '                    End If
        '                Else
        '                    dTipCustTotal = dTipCharge * dCustWeight
        '                End If
        '
        '                'Haulage
        '                If sHaulAC = "Y" Then
        '                    dCusQty = doGetFieldValue(IDH_JOBSHD._RdWgt)
        '                Else
        '                    dCusQty = doGetFieldValue(IDH_JOBSHD._CusQty)
        '                End If
        '                oRowAuditObj.setFieldValue("U_HlSQty", dCusQty)
        '
        '                dPrice = dCusQty * dHaulChrg
        '                dBefDiscTotal = dTipCustTotal + dPrice
        '
        '                If dDiscnt = 0 AndAlso dDiscountAmt <> 0 Then
        '                    dDiscnt = dDiscountAmt / dBefDiscTotal * 100
        '                ElseIf dDiscnt <> 0 Then
        '                    dDiscountAmt = (dDiscnt / 100) * dBefDiscTotal
        '                End If
        '
        '                Dim TSub As Double = dTipCustTotal - (dTipCustTotal * (dDiscnt / 100))
        '                Dim HSub As Double = dPrice - (dPrice * (dDiscnt / 100))
        '                Dim TVat As Double = TSub * (dTipVatRate / 100)
        '                Dim HVat As Double = HSub * (dHaulageVatRate / 100)
        '
        '                dTotal = TSub + HSub
        '                dVatAmnt = TVat + HVat
        '
        '                dTotal = dTotal + dVatAmnt + dAddCharges
        '
        '                oRowAuditObj.setFieldValue("U_CstWgt", dCustWeight)
        '                oRowAuditObj.setFieldValue("U_Price", dPrice)
        '                oRowAuditObj.setFieldValue("U_Discnt", CStr(dDiscnt))
        '                oRowAuditObj.setFieldValue("U_DisAmt", CStr(dDiscountAmt))
        '                oRowAuditObj.setFieldValue("U_TaxAmt", dVatAmnt)
        '                oRowAuditObj.setFieldValue("U_TCTotal", CStr(dTipCustTotal))
        '                oRowAuditObj.setFieldValue("U_Total", CStr(dTotal))
        '
        '                oRowAuditObj.setFieldValue("U_CusChr", dHaulChrg)
        '                iPrcSet = iPrcSet And (Not idh.const.Lookup.MASK_HAULCHRG)
        '
        '                oRowAuditObj.setFieldValue("U_TCharge", dTipCharge)
        '                iPrcSet = iPrcSet And (Not idh.const.Lookup.MASK_TIPCHRG)
        '
        '                oRowAuditObj.setFieldValue("U_MANPRC", iPrcSet)
        '
        '                oRowAuditObj.setFieldValue("U_TChrgVtRt", dTipVatRate)
        '                oRowAuditObj.setFieldValue("U_TChrgVtGrp", sTipVatGrp)
        '
        '                oRowAuditObj.setFieldValue("U_HChrgVtRt", dHaulageVatRate)
        '                oRowAuditObj.setFieldValue("U_HChrgVtGrp", sHaulageVatGrp)
        '            Catch ex As Exception
        '                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error getting the charges.")
        '            End Try
        '        End Sub


    End Class
End Namespace
