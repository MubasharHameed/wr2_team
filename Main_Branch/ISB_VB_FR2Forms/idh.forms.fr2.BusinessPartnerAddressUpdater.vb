﻿Imports System.IO
Imports System.Collections
Imports System

Imports IDHAddOns.idh.controls
Imports IDHAddOns
Imports com.idh.utils
Imports com.idh.bridge
Imports com.idh.bridge.data
Imports com.idh.dbObjects.User

Namespace idh.forms.fr2
    Public Class BusinessPartnerAddressUpdater
        Inherits com.idh.forms.oo.Form

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByRef oSBOParentForm As SAPbouiCOM.Form, ByRef oSBOForm As SAPbouiCOM.Form)
            MyBase.New(oIDHForm, oSBOParentForm, oSBOForm)
            doSetHandlers()
        End Sub
        Private Sub doSetHandlers()
            'doStartEventFilterBatch(True)
            'Handler_GOT_FOCUS = New ev_Item_Event(AddressOf FormActivated)
            'Handler_FORM_LOAD = New ev_Item_Event(AddressOf FormActivated)
            'doCommitEventFilters()
            'Handler_Button_Update = New ev_Item_Event(AddressOf doOKButton)

        End Sub
        'Public Overrides Function doItemEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        '    If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_LOAD AndAlso pVal.BeforeAction = False Then
        '        FormActivated(pVal, BubbleEvent)
        '    End If
        '    Return MyBase.doEvents(pVal, BubbleEvent)
        'End Function
        Public Overloads Shared Function doRegisterFormClass() As IDHAddOns.idh.forms.Base
            ''IDH_BPADRUP
            Dim owForm As com.idh.forms.oo.FormController = New com.idh.forms.oo.FormController( _
                "IDH_BPADRUP", _
                0, _
                "BP Address updater.srf", _
                False)
            Return owForm
        End Function

        ' Public Overrides 
        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef BubbleEvent As Boolean)
            doAddUF("IDH_STATUS", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 2000)
            SBOForm.AutoManaged = True
            SBOForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_OK_MODE
        End Sub
        
        Public Overrides Sub doLoadData()
         
        End Sub
        Public Overrides Sub doFinalizeShow()
            MyBase.doFinalizeShow()
            setUFValue("IDH_STATUS", "")
            setSharedData("IDH_AddressDone", False)
            FormActivated()
        End Sub

        Public Function FormActivated() As Boolean 'ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Try
                If getSharedData("IDH_AddressDone") = True Then
                    Return True
                End If
                setSharedData("IDH_AddressDone", True)

                Dim sOldAddress As String, sNewAddress As String
                Dim sOldStreet As String, sNewStreet As String
                Dim sOldBlock As String, sNewBlock As String
                Dim sOldCity As String, sNewCity As String
                Dim sOldCounty As String, sNewCounty As String
                Dim sOldState As String, sNewState As String
                Dim sOldPostcode As String, sNewPostcode As String
                Dim sAddrsCd As String
                Dim sExcludeStatus As Boolean = lookups.Config.INSTANCE.getParameterAsBool("UPDATAAL", False)

                SBOForm.Freeze(False)
                Dim strStatus As String = com.idh.bridge.resources.Messages.getGMessage("WRNBPA1", Nothing)
                setUFValue("IDH_STATUS", strStatus)
                'doWarnMess(com.idh.bridge.resources.Messages.getGMessage("WRNBPA1", Nothing), SAPbouiCOM.BoMessageTime.bmt_Short)
                SBOForm.Freeze(True)
                'SBOForm.Refresh()
                'doWarnMess("Please wait while system is updating Business Partner Address(s)", SAPbouiCOM.BoMessageTime.bmt_Medium)
                Dim oRecordSetOldAddress As SAPbobsCOM.Recordset
                Dim oRecordSet As SAPbobsCOM.Recordset = getParentSharedData("oRecordSet")
                Dim sSQL As String = ""
                Dim sCardCode As String = getParentSharedData("sCardCode")
                While Not oRecordSet.EoF
                    sNewAddress = oRecordSet.Fields.Item("Address").Value
                    sNewStreet = oRecordSet.Fields.Item("Street").Value
                    sNewBlock = oRecordSet.Fields.Item("Block").Value
                    sNewCity = oRecordSet.Fields.Item("City").Value
                    sNewCounty = oRecordSet.Fields.Item("County").Value
                    sNewState = oRecordSet.Fields.Item("State").Value
                    sNewPostcode = oRecordSet.Fields.Item("ZipCode").Value

                    sAddrsCd = oRecordSet.Fields.Item("U_AddrsCd").Value
                    sSQL = "select top 1 ACR1.Address,Street,Block,City, County,[State], ZipCode " _
                                & " From ACR1 WITH(NOLOCK)" _
                                & " Where ACR1.cardCode='" & sCardCode.Replace("'", "''") & "' And ACR1.ObjType=2 " _
                                & " And ACR1.U_AddrsCd=" & sAddrsCd & " And AdresType='S' " _
                                & "	Order by LogInstanc Desc "
                    oRecordSetOldAddress = DataHandler.INSTANCE.doSBOSelectQuery("doGetBPAllAddresses", sSQL)
                    If oRecordSetOldAddress IsNot Nothing AndAlso oRecordSetOldAddress.RecordCount > 0 Then
                        sOldAddress = oRecordSetOldAddress.Fields.Item("Address").Value
                        sOldStreet = oRecordSetOldAddress.Fields.Item("Street").Value
                        sOldBlock = oRecordSetOldAddress.Fields.Item("Block").Value
                        sOldCity = oRecordSetOldAddress.Fields.Item("City").Value
                        sOldCounty = oRecordSetOldAddress.Fields.Item("County").Value
                        sOldState = oRecordSetOldAddress.Fields.Item("State").Value
                        sOldPostcode = oRecordSetOldAddress.Fields.Item("ZipCode").Value

                        If sOldAddress <> sNewAddress OrElse sOldStreet <> sNewStreet OrElse sOldBlock <> sNewBlock OrElse sOldCity <> sNewCity _
                            OrElse sOldCounty <> sNewCounty OrElse sOldState <> sNewState OrElse sOldPostcode <> sNewPostcode Then 'Update address
                            'doWarnMess(getGMessage("WRNBPA2", Nothing), SAPbouiCOM.BoMessageTime.bmt_Short)
                            strStatus &= vbCrLf & com.idh.bridge.resources.Messages.getGMessage("WRNBPA2", Nothing)
                            setUFValue("IDH_STATUS", strStatus)
                            '--Update Customer Price Table Address
                            sSQL = "UPDATE [@idh_csitpr] " _
                            & "SET    u_staddr = '" & sNewAddress.Replace("'", "''") & "',U_ZpCd='" & sNewPostcode.Replace("'", "''") & "' " _
                            & "WHERE  [@idh_csitpr].u_custcd = '" & sCardCode.Replace("'", "''") & "' " _
                            & "       AND Isnull(u_staddr, '') = '" & sOldAddress.Replace("'", "''") & "' " _
                            & "       AND Isnull(u_stadln, '') = '" & sAddrsCd & "'"
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)

                            'doWarnMess(getGMessage("WRNBPA3", Nothing), SAPbouiCOM.BoMessageTime.bmt_Short)
                            'doWarnMess("Updating Supplier Price", SAPbouiCOM.BoMessageTime.bmt_Long)
                            SBOForm.Freeze(False)
                            strStatus &= vbCrLf & com.idh.bridge.resources.Messages.getGMessage("WRNBPA3", Nothing)
                            setUFValue("IDH_STATUS", strStatus)
                            SBOForm.Freeze(True)
                            '--Start of Supplier Price
                            sSQL = "UPDATE [@idh_suitpr] " _
                            & "SET u_supaddr = '" & sNewAddress.Replace("'", "''") & "',U_SupZipCd='" & sNewPostcode.Replace("'", "''") & "' " _
                            & "WHERE  ( [@idh_suitpr].u_cardcd = '" & sCardCode.Replace("'", "''") & "' " _
                            & "         AND Isnull(u_supadln, '') = '" & sAddrsCd & "' " _
                            & "         AND Isnull(u_supaddr, '') = '" & sOldAddress.Replace("'", "''") & "' )"
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)

                            sSQL = "Update [@IDH_SUITPR] " _
                               & " Set U_StAddr='" & sNewAddress.Replace("'", "''") & "',U_ZpCd='" & sNewPostcode.Replace("'", "''") & "' " _
                               & " where ([@IDH_SUITPR].U_BP2CD='" & sCardCode.Replace("'", "''") & "' " _
                               & " And " _
                               & " isnull(U_StAdLN,'')='" & sAddrsCd & "' " _
                               & " and isnull(U_StAddr,'')='" & sOldAddress.Replace("'", "''") & "') "
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)

                            '--Start BP Forecast
                            'doWarnMess(getGMessage("WRNBPA4", Nothing), SAPbouiCOM.BoMessageTime.bmt_Short)
                            'doWarnMess("Updating BP Forecast", SAPbouiCOM.BoMessageTime.bmt_Medium)
                            SBOForm.Freeze(False)
                            strStatus &= vbCrLf & com.idh.bridge.resources.Messages.getGMessage("WRNBPA4", Nothing)
                            setUFValue("IDH_STATUS", strStatus)
                            SBOForm.Freeze(True)
                            sSQL = "Update [@IDH_BPFORECST] Set U_Address='" & sNewAddress.Replace("'", "''") & "' " _
                               & " where " _
                               & " [@IDH_BPFORECST].U_CardCd='" & sCardCode.Replace("'", "''") & "'  " _
                               & " And isnull(U_AddrssLN,'')='" & sAddrsCd & "'  " _
                               & " and isnull(U_Address,'')='" & sOldAddress.Replace("'", "''") & "'  "
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)

                            '--Start of PBI
                            'update PBI with end date checked and their end date>getdate or no checkbox is checked
                            'doWarnMess(getGMessage("WRNBPA5", Nothing), SAPbouiCOM.BoMessageTime.bmt_Short)
                            'doWarnMess("Updating PBI", SAPbouiCOM.BoMessageTime.bmt_Medium)
                            SBOForm.Freeze(False)
                            strStatus &= vbCrLf & com.idh.bridge.resources.Messages.getGMessage("WRNBPA5", Nothing)
                            setUFValue("IDH_STATUS", strStatus)
                            SBOForm.Freeze(True)

                            sSQL = "Update [@IDH_PBI] Set U_IDHSADDR='" & sNewAddress.Replace("'", "''") & "' " _
                            & "where 	 [@IDH_PBI].U_IDHCCODE='" & sCardCode.Replace("'", "''") & "' " _
                            & "And isnull(U_IDHSADLN,'')='" & sAddrsCd & "' " _
                            & "and isnull(U_IDHSADDR,'')='" & sOldAddress.Replace("'", "''") & "' " _
                            & (IIf(sExcludeStatus, "", ("And	((( U_IDHRECEN=2 and " _
                            & "(U_IDHRECSD is not  Null) " _
                            & "ANd (U_IDHRECED is not  Null) " _
                            & "And " _
                            & "CONVERT(VARCHAR, U_IDHRECED , 112)>=CONVERT(VARCHAR, GETDATE() , 112) ) " _
                            & "or " _
                            & "( U_IDHRECEN=0 ) " _
                            & ") " _
                            & "or " _
                            & "(( U_IDHRECEN=1 " _
                            & "ANd (U_IDHRECCE is not  Null) " _
                            & "And " _
                            & "U_IDHRECCE>0 ) " _
                            & "))")))
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)


                            sSQL = "Update [@IDH_PBI] Set U_IDHDADDR='" & sNewAddress.Replace("'", "''") & "' " _
                            & "where 	 [@IDH_PBI].U_IDHDISCD='" & sCardCode.Replace("'", "''") & "' " _
                            & "And isnull(U_IDHDADLN,'')='" & sAddrsCd & "' " _
                            & "and isnull(U_IDHDADDR,'')='" & sOldAddress.Replace("'", "''") & "' " _
                            & (IIf(sExcludeStatus, "", ("And	((( U_IDHRECEN=2 and " _
                            & "(U_IDHRECSD is not  Null) " _
                            & "ANd (U_IDHRECED is not  Null) " _
                            & "And " _
                            & "CONVERT(VARCHAR, U_IDHRECED , 112)>=CONVERT(VARCHAR, GETDATE() , 112) ) " _
                            & "or " _
                            & "( U_IDHRECEN=0 ) " _
                            & ") " _
                            & "or " _
                            & "(( U_IDHRECEN=1 " _
                            & "ANd (U_IDHRECCE is not  Null) " _
                            & "And " _
                            & "U_IDHRECCE>0 ) " _
                            & "))")))
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)

                            'doWarnMess(getGMessage("WRNBPA6", Nothing), SAPbouiCOM.BoMessageTime.bmt_Long)
                            'doWarnMess("Updating Disposal Order Row(s)", SAPbouiCOM.BoMessageTime.bmt_Medium)
                            SBOForm.Freeze(False)
                            strStatus &= vbCrLf & com.idh.bridge.resources.Messages.getGMessage("WRNBPA6", Nothing)
                            setUFValue("IDH_STATUS", strStatus)
                            SBOForm.Freeze(True)
                            '--Start of DO Row
                            sSQL = "Update [@IDH_DISPROW] Set U_SAddress='" & sNewAddress.Replace("'", "''") & "' " _
                                & " where " _
                                & " [@IDH_DISPROW].U_Tip='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_SAddrsLN,'')='" & sAddrsCd & "' " _
                                & " and isnull(U_SAddress,'')='" & sOldAddress.Replace("'", "''") & "' " _
                                & (IIf(sExcludeStatus, "", (" And U_Status in('Open','DoOrder','ReqOrder','ReqFoc','DoFoc','ReqInvoice','DoInvoice') ")))
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)
                            '--End of DO Row
                            '----Start of DOH
                            'doWarnMess(getGMessage("WRNBPA7", Nothing), SAPbouiCOM.BoMessageTime.bmt_Long)
                            'doWarnMess("Updating Disposal Order(s)", SAPbouiCOM.BoMessageTime.bmt_Medium)
                            SBOForm.Freeze(False)
                            strStatus &= vbCrLf & com.idh.bridge.resources.Messages.getGMessage("WRNBPA7", Nothing)
                            setUFValue("IDH_STATUS", strStatus)
                            SBOForm.Freeze(True)
                            sSQL = "Update [@IDH_DISPORD] Set U_Address='" & sNewAddress.Replace("'", "''") & "', " _
                                & "U_Street='" & sNewStreet.Replace("'", "''") & "',U_Block='" & sNewBlock.Replace("'", "''") & "',U_City='" & sNewCity.Replace("'", "''") & "',U_County='" & sNewCounty.Replace("'", "''") & "',U_State='" & sNewState.Replace("'", "''") & "',U_ZpCd='" & sNewPostcode.Replace("'", "''") & "'" _
                                & " where " _
                                & " [@IDH_DISPORD].U_CardCd='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_AddrssLN,'')='" & sAddrsCd & "' " _
                                & " and isnull(U_Address,'')='" & sOldAddress.Replace("'", "''") & "' " _
                                & (IIf(sExcludeStatus, "", (" And ( Code in ( " _
                                & " Select T0.U_JobNr from [@IDH_DISPROW] T0 WITH(noLOCK) Inner Join " _
                                & " [@IDH_DISPORD] T1 with(nolock ) on T0.U_JobNr=T1.Code " _
                                & " Where  T1.U_CardCd='" & sCardCode.Replace("'", "''") & "' and T0.U_Status in('Open','DoOrder','ReqOrder','ReqFoc','DoFoc','ReqInvoice','DoInvoice')) " _
                                & " or (Select Count(1) from [@IDH_DISPROW] T0 WITH(noLOCK)  Where T0.U_JobNr=[@IDH_DISPORD].Code)=0)  ")))
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)

                            sSQL = "Update [@IDH_DISPORD] Set U_CAddress='" & sNewAddress.Replace("'", "''") & "', " _
                                & "U_CStreet='" & sNewStreet.Replace("'", "''") & "',U_CBlock='" & sNewBlock.Replace("'", "''") & "',U_CCity='" & sNewCity.Replace("'", "''") & "',U_CState='" & sNewState.Replace("'", "''") & "',U_CZpCd='" & sNewPostcode.Replace("'", "''") & "'" _
                                & " where " _
                                & " [@IDH_DISPORD].U_CCardCd='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_CAddrsLN,'')='" & sAddrsCd & "' " _
                                & " and isnull(U_CAddress,'')='" & sOldAddress.Replace("'", "''") & "' " _
                                & (IIf(sExcludeStatus, "", (" And ( Code in ( " _
                                & " Select T0.U_JobNr from [@IDH_DISPROW] T0 WITH(noLOCK) Inner Join " _
                                & " [@IDH_DISPORD] T1 with(nolock ) on T0.U_JobNr=T1.Code " _
                                & " Where  T1.U_CCardCd='" & sCardCode.Replace("'", "''") & "' and T0.U_Status in('Open','DoOrder','ReqOrder','ReqFoc','DoFoc','ReqInvoice','DoInvoice')) " _
                                & " or (Select Count(1) from [@IDH_DISPROW] T0 WITH(noLOCK)  Where T0.U_JobNr=[@IDH_DISPORD].Code)=0)  ")))
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)

                            'doWarnMess(getGMessage("WRNBPA7", Nothing), SAPbouiCOM.BoMessageTime.bmt_Long)
                            'doWarnMess("Updating Disposal Order(s)", SAPbouiCOM.BoMessageTime.bmt_Medium)
                            sSQL = "Update [@IDH_DISPORD] Set U_PAddress='" & sNewAddress.Replace("'", "''") & "', " _
                                & " U_PStreet='" & sNewStreet.Replace("'", "''") & "',U_PBlock='" & sNewBlock.Replace("'", "''") & "',U_PCity='" & sNewCity.Replace("'", "''") & "',U_PState='" & sNewState.Replace("'", "''") & "',U_PZpCd='" & sNewPostcode.Replace("'", "''") & "'" _
                                & " where " _
                                & " [@IDH_DISPORD].U_PCardCd='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_PAddrsLN,'')='" & sAddrsCd & "' " _
                                & " and isnull(U_PAddress,'')='" & sOldAddress.Replace("'", "''") & "' " _
                                & (IIf(sExcludeStatus, "", (" And ( Code in ( " _
                                & " Select T0.U_JobNr from [@IDH_DISPROW] T0 WITH(noLOCK) Inner Join " _
                                & " [@IDH_DISPORD] T1 with(nolock ) on T0.U_JobNr=T1.Code " _
                                & " Where  T1.U_PCardCd='" & sCardCode.Replace("'", "''") & "' and T0.U_Status in('Open','DoOrder','ReqOrder','ReqFoc','DoFoc','ReqInvoice','DoInvoice')) " _
                                & " or (Select Count(1) from [@IDH_DISPROW] T0 WITH(noLOCK)  Where T0.U_JobNr=[@IDH_DISPORD].Code)=0)  ")))
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)

                            sSQL = "Update [@IDH_DISPORD] Set U_SAddress='" & sNewAddress.Replace("'", "''") & "', " _
                                & " U_SStreet='" & sNewStreet.Replace("'", "''") & "',U_SBlock='" & sNewBlock.Replace("'", "''") & "',U_SCity='" & sNewCity.Replace("'", "''") & "',U_SState='" & sNewState.Replace("'", "''") & "',U_SZpCd='" & sNewPostcode.Replace("'", "''") & "'" _
                                & " where " _
                                & " [@IDH_DISPORD].U_SCardCd='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_SAddrsLN,'')='" & sAddrsCd & "' " _
                                & " and isnull(U_SAddress,'')='" & sOldAddress.Replace("'", "''") & "' " _
                                & (IIf(sExcludeStatus, "", (" And ( Code in ( " _
                                & " Select T0.U_JobNr from [@IDH_DISPROW] T0 WITH(noLOCK) Inner Join " _
                                & " [@IDH_DISPORD] T1 with(nolock ) on T0.U_JobNr=T1.Code " _
                                & " Where  T1.U_SCardCd='" & sCardCode.Replace("'", "''") & "' and T0.U_Status in('Open','DoOrder','ReqOrder','ReqFoc','DoFoc','ReqInvoice','DoInvoice')) " _
                                & " or (Select Count(1) from [@IDH_DISPROW] T0 WITH(noLOCK)  Where T0.U_JobNr=[@IDH_DISPORD].Code)=0)  ")))
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)
                            '----End of DOH
                            ''----Start of DO Row
                            'doWarnMess(getGMessage("WRNBPA9", Nothing), SAPbouiCOM.BoMessageTime.bmt_Long)
                            'doWarnMess("Updating Disposal Order(s)", SAPbouiCOM.BoMessageTime.bmt_Medium)
                            'sSQL = "Update [@IDH_DISPROW] Set U_SAddress='" & sNewAddress & "' " _
                            '    & " where " _
                            '    & " [@IDH_DISPROW].U_Tip='" & sCardCode & "' " _
                            '    & " And " _
                            '    & " isnull(U_SAddrsLN,'')='" & iLineNum & "' " _
                            '    & " and isnull(U_SAddress,'')='" & sOldAddress & "' " _
                            '    & " And U_Status in('Open','DoOrder','ReqOrder','ReqFoc','DoFoc','ReqInvoice','DoInvoice') "
                            'DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)
                            ''----End of DO Row

                            'doWarnMess(getGMessage("WRNBPA8", Nothing), SAPbouiCOM.BoMessageTime.bmt_Long)
                            'doWarnMess("Updating Work Order Row(s)", SAPbouiCOM.BoMessageTime.bmt_Medium)
                            SBOForm.Freeze(False)
                            strStatus &= vbCrLf & com.idh.bridge.resources.Messages.getGMessage("WRNBPA8", Nothing)
                            setUFValue("IDH_STATUS", strStatus)
                            SBOForm.Freeze(True)
                            '----Start of WO Row
                            sSQL = "Update [@IDH_JOBSHD] Set U_SAddress='" & sNewAddress.Replace("'", "''") & "' " _
                                & " where " _
                                & " [@IDH_JOBSHD].U_Tip='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_SAddrsLN,'')='" & sAddrsCd & "' " _
                                & " and isnull(U_SAddress,'')='" & sOldAddress.Replace("'", "''") & "' " _
                                & (IIf(sExcludeStatus, "", (" And U_Status in('Open','DoOrder','ReqOrder','ReqFoc','DoFoc','ReqInvoice','DoInvoice') ")))
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)
                            '----End of WO Row
                            ''----Start of WO Row
                            'doWarnMess(getGMessage("WRNBPA9", Nothing), SAPbouiCOM.BoMessageTime.bmt_Long)
                            'doWarnMess("Updating Work Order Row(s)", SAPbouiCOM.BoMessageTime.bmt_Medium)
                            'sSQL = "Update [@IDH_JOBSHD] Set U_SAddress='" & sNewAddress & "' " _
                            '    & " where " _
                            '    & " [@IDH_JOBSHD].U_Tip='" & sCardCode & "' " _
                            '    & " And " _
                            '    & " isnull(U_SAddrsLN,'')='" & iLineNum & "' " _
                            '    & " and isnull(U_SAddress,'')='" & sOldAddress & "' " _
                            '    & " And U_Status in('Open','DoOrder','ReqOrder','ReqFoc','DoFoc','ReqInvoice','DoInvoice') "
                            'DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)
                            ''----End of WO Row
                            '----Start of WOH
                            SBOForm.Freeze(False)
                            strStatus &= vbCrLf & com.idh.bridge.resources.Messages.getGMessage("WRNBPA9", Nothing)
                            setUFValue("IDH_STATUS", strStatus)
                            SBOForm.Freeze(True)
                            'doWarnMess(getGMessage("WRNBPA9", Nothing), SAPbouiCOM.BoMessageTime.bmt_Long)
                            'doWarnMess("Updating Work Order Header(s)", SAPbouiCOM.BoMessageTime.bmt_Medium)

                            sSQL = "Update [@IDH_JOBENTR] Set U_Address='" & sNewAddress.Replace("'", "''") & "', " _
                                & "U_Street='" & sNewStreet.Replace("'", "''") & "',U_Block='" & sNewBlock.Replace("'", "''") & "',U_City='" & sNewCity.Replace("'", "''") & "',U_County='" & sNewCounty.Replace("'", "''") & "',U_State='" & sNewState.Replace("'", "''") & "',U_ZpCd='" & sNewPostcode.Replace("'", "''") & "'" _
                                & " where " _
                                & " [@IDH_JOBENTR].U_CardCd='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_AddrssLN,'')='" & sAddrsCd & "' " _
                                & " and isnull(U_Address,'')='" & sOldAddress.Replace("'", "''") & "' " _
                                & (IIf(sExcludeStatus, "", (" And ( Code in ( " _
                                & " Select T0.U_JobNr from [@IDH_JOBSHD] T0 WITH(noLOCK) Inner Join " _
                                & " [@IDH_JOBENTR] T1 with(nolock ) on T0.U_JobNr=T1.Code" _
                                & " Where  T1.U_CardCd='" & sCardCode.Replace("'", "''") & "' and  T0.U_Status in('Open','DoOrder','ReqOrder','ReqFoc','DoFoc','ReqInvoice','DoInvoice')) " _
                                & " or (Select Count(1) from [@IDH_JOBSHD] T0 WITH(noLOCK)  Where T0.U_JobNr=[@IDH_JOBENTR].Code)=0)  ")))
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)

                            sSQL = "Update [@IDH_JOBENTR] Set U_CAddress='" & sNewAddress.Replace("'", "''") & "', " _
                                & " U_CStreet='" & sNewStreet.Replace("'", "''") & "',U_CBlock='" & sNewBlock.Replace("'", "''") & "',U_CCity='" & sNewCity.Replace("'", "''") & "',U_CState='" & sNewState.Replace("'", "''") & "',U_CZpCd='" & sNewPostcode.Replace("'", "''") & "'" _
                                & " where " _
                                & " [@IDH_JOBENTR].U_CCardCd='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_CAddrsLN,'')='" & sAddrsCd & "' " _
                                & " and isnull(U_CAddress,'')='" & sOldAddress.Replace("'", "''") & "' " _
                                & (IIf(sExcludeStatus, "", (" And ( Code in ( " _
                                & " Select T0.U_JobNr from [@IDH_JOBSHD] T0 WITH(noLOCK) Inner Join " _
                                & " [@IDH_JOBENTR] T1 with(nolock ) on T0.U_JobNr=T1.Code " _
                                & " Where  T1.U_CCardCd='" & sCardCode.Replace("'", "''") & "' and  T0.U_Status in('Open','DoOrder','ReqOrder','ReqFoc','DoFoc','ReqInvoice','DoInvoice')) " _
                                & " or (Select Count(1) from [@IDH_JOBSHD] T0 WITH(noLOCK)  Where T0.U_JobNr=[@IDH_JOBENTR].Code)=0)  ")))
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)

                            'doWarnMess(getGMessage("WRNBPA8", Nothing), SAPbouiCOM.BoMessageTime.bmt_Long)
                            'doWarnMess("Updating Work Order Header(s)", SAPbouiCOM.BoMessageTime.bmt_Medium)
                            sSQL = "Update [@IDH_JOBENTR] Set U_PAddress='" & sNewAddress.Replace("'", "''") & "', " _
                                & " U_PStreet='" & sNewStreet.Replace("'", "''") & "',U_PBlock='" & sNewBlock.Replace("'", "''") & "',U_PCity='" & sNewCity.Replace("'", "''") & "',U_PState='" & sNewState.Replace("'", "''") & "',U_PZpCd='" & sNewPostcode.Replace("'", "''") & "'" _
                                & " where " _
                                & " [@IDH_JOBENTR].U_PCardCd='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_PAddrsLN,'')='" & sAddrsCd & "' " _
                                & " and isnull(U_PAddress,'')='" & sOldAddress.Replace("'", "''") & "' " _
                                & (IIf(sExcludeStatus, "", (" And ( Code in ( " _
                                & " Select T0.U_JobNr from [@IDH_JOBSHD] T0 WITH(noLOCK) Inner Join " _
                                & " [@IDH_JOBENTR] T1 with(nolock ) on T0.U_JobNr=T1.Code " _
                                & " Where  T1.U_PCardCd='" & sCardCode.Replace("'", "''") & "' and  T0.U_Status in('Open','DoOrder','ReqOrder','ReqFoc','DoFoc','ReqInvoice','DoInvoice')) " _
                                & " or (Select Count(1) from [@IDH_JOBSHD] T0 WITH(noLOCK)  Where T0.U_JobNr=[@IDH_JOBENTR].Code)=0)  ")))
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)

                            sSQL = "Update [@IDH_JOBENTR] Set U_SAddress='" & sNewAddress.Replace("'", "''") & "', " _
                                & " U_SStreet='" & sNewStreet.Replace("'", "''") & "',U_SBlock='" & sNewBlock.Replace("'", "''") & "',U_SCity='" & sNewCity.Replace("'", "''") & "',U_SState='" & sNewState.Replace("'", "''") & "',U_SZpCd='" & sNewPostcode.Replace("'", "''") & "'" _
                                & " where " _
                                & " [@IDH_JOBENTR].U_SCardCd='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_SAddrsLN,'')='" & sAddrsCd & "' " _
                                & " and isnull(U_SAddress,'')='" & sOldAddress.Replace("'", "''") & "' " _
                                & (IIf(sExcludeStatus, "", (" And ( Code in ( " _
                                & " Select T0.U_JobNr from [@IDH_JOBSHD] T0 WITH(noLOCK) Inner Join  " _
                                & " [@IDH_JOBENTR] T1 with(nolock ) on T0.U_JobNr=T1.Code " _
                                & " Where  T1.U_SCardCd='" & sCardCode.Replace("'", "''") & "' and  T0.U_Status in('Open','DoOrder','ReqOrder','ReqFoc','DoFoc','ReqInvoice','DoInvoice')) " _
                                & " or (Select Count(1) from [@IDH_JOBSHD] T0 WITH(noLOCK)  Where T0.U_JobNr=[@IDH_JOBENTR].Code)=0)  ")))
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)
                            '----End of WOH

                            ''Start of Waste Profile
                            SBOForm.Freeze(False)
                            strStatus &= vbCrLf & Translation.getTranslatedWord("Updating Address in Waste Profile.") ''& com.idh.bridge.resources.Messages.getGMessage("WRNBPA8", Nothing)
                            setUFValue("IDH_STATUS", strStatus)
                            SBOForm.Freeze(True)
                            sSQL = "Update [@ISB_WSTPROFILE] Set U_AddCd='" & sNewAddress.Replace("'", "''") & "' " _
                                & " where " _
                                & " [@ISB_WSTPROFILE].U_CardCd='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_AddCd,'')='" & sOldAddress.Replace("'", "''") & "' "
                            DataHandler.INSTANCE.doUpdateQuery("BusinessPartner", sSQL)
                            ''End of Waste Profile
                            ''Start Update ENQUIRY
                            SBOForm.Freeze(False)
                            strStatus &= vbCrLf & com.idh.bridge.resources.Messages.getGMessage("WRNBPA11", Nothing)
                            setUFValue("IDH_STATUS", strStatus)
                            SBOForm.Freeze(True)
                            sSQL = "Update [@IDH_ENQUIRY] Set U_Address='" & sNewAddress.Replace("'", "''") & "' " _
                                & " where " _
                                & " [@IDH_ENQUIRY].U_CardCode='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_AddrssLN,'')='" & sAddrsCd & "' " _
                                & " and isnull(U_Address,'')='" & sOldAddress.Replace("'", "''") & "' "

                            DataHandler.INSTANCE.doUpdateQuery("Enquiry", sSQL)
                            ''End Update ENQUIRY
                            ''Start Update WOQ
                            SBOForm.Freeze(False)
                            strStatus &= vbCrLf & com.idh.bridge.resources.Messages.getGMessage("WRNBPA12", Nothing)
                            setUFValue("IDH_STATUS", strStatus)
                            SBOForm.Freeze(True)
                            'sSQL = "Update [@IDH_WOQITEM] Set U_Address='" & sNewAddress.Replace("'", "''") & "', " _
                            '    & "U_Street='" & sNewStreet.Replace("'", "''") & "',U_Block='" & sNewBlock.Replace("'", "''") & "',U_City='" & sNewCity.Replace("'", "''") & "',U_County='" & sNewCounty.Replace("'", "''") & "',U_State='" & sNewState.Replace("'", "''") & "',U_ZpCd='" & sNewPostcode.Replace("'", "''") & "'" _
                            '    & " where " _
                            '    & " [@IDH_WOQITEM].U_CardCd='" & sCardCode.Replace("'", "''") & "' " _
                            '    & " And " _
                            '    & " isnull(U_AddrssLN,'')='" & sAddrsCd & "' " _
                            '    & " and isnull(U_Address,'')='" & sOldAddress.Replace("'", "''") & "' "
                            'DataHandler.INSTANCE.doUpdateQuery("WOQAddress", sSQL)

                            'sSQL = "Update [@IDH_WOQITEM] Set U_CAddress='" & sNewAddress.Replace("'", "''") & "', " _
                            '    & " U_CStreet='" & sNewStreet.Replace("'", "''") & "',U_CBlock='" & sNewBlock.Replace("'", "''") & "',U_CCity='" & sNewCity.Replace("'", "''") & "',U_CState='" & sNewState.Replace("'", "''") & "',U_CZpCd='" & sNewPostcode.Replace("'", "''") & "'" _
                            '    & " where " _
                            '    & " [@IDH_WOQITEM].U_CCardCd='" & sCardCode.Replace("'", "''") & "' " _
                            '    & " And " _
                            '    & " isnull(U_CAddrsLN,'')='" & sAddrsCd & "' " _
                            '    & " and isnull(U_CAddress,'')='" & sOldAddress.Replace("'", "''") & "' "
                            'DataHandler.INSTANCE.doUpdateQuery("WOQCAddress", sSQL)

                            'sSQL = "Update [@IDH_WOQITEM] Set U_PAddress='" & sNewAddress.Replace("'", "''") & "', " _
                            '    & " U_PStreet='" & sNewStreet.Replace("'", "''") & "',U_PBlock='" & sNewBlock.Replace("'", "''") & "',U_PCity='" & sNewCity.Replace("'", "''") & "',U_PState='" & sNewState.Replace("'", "''") & "',U_PZpCd='" & sNewPostcode.Replace("'", "''") & "'" _
                            '    & " where " _
                            '    & " [@IDH_WOQITEM].U_PCardCd='" & sCardCode.Replace("'", "''") & "' " _
                            '    & " And " _
                            '    & " isnull(U_PAddrsLN,'')='" & sAddrsCd & "' " _
                            '    & " and isnull(U_PAddress,'')='" & sOldAddress.Replace("'", "''") & "' "
                            'DataHandler.INSTANCE.doUpdateQuery("WOQPAddress", sSQL)

                            'sSQL = "Update [@IDH_WOQITEM] Set U_SAddress='" & sNewAddress.Replace("'", "''") & "', " _
                            '    & " U_SStreet='" & sNewStreet.Replace("'", "''") & "',U_SBlock='" & sNewBlock.Replace("'", "''") & "',U_SCity='" & sNewCity.Replace("'", "''") & "',U_SState='" & sNewState.Replace("'", "''") & "',U_SZpCd='" & sNewPostcode.Replace("'", "''") & "'" _
                            '    & " where " _
                            '    & " [@IDH_WOQITEM].U_SCardCd='" & sCardCode.Replace("'", "''") & "' " _
                            '    & " And " _
                            '    & " isnull(U_SAddrsLN,'')='" & sAddrsCd & "' " _
                            '    & " and isnull(U_SAddress,'')='" & sOldAddress.Replace("'", "''") & "' "
                            'DataHandler.INSTANCE.doUpdateQuery("WOQSAddress", sSQL)
                            ''End Update WOQ
                            '----Start of WOQ Row
                            sSQL = "Update [@IDH_WOQHD] Set U_Address='" & sNewAddress.Replace("'", "''") & "' " _
                                & " where " _
                                & " [@IDH_WOQHD].U_CardCd='" & sCardCode.Replace("'", "''") & "' " _
                                & " And " _
                                & " isnull(U_AddrssLN,'')='" & sAddrsCd & "' " _
                                & " and isnull(U_Address,'')='" & sOldAddress.Replace("'", "''") & "' "
                            DataHandler.INSTANCE.doUpdateQuery("WOQRow", sSQL)
                            '----End of WO Row

                        End If
                    End If
                    oRecordSet.MoveNext()
                End While
                SBOForm.Freeze(False)
                strStatus &= vbCrLf & com.idh.bridge.resources.Messages.getGMessage("WRNBPA10", Nothing)
                setUFValue("IDH_STATUS", strStatus)

                'SBOForm.Freeze(True)
                IDHForm.goParent.goApplication.StatusBar.SetText(com.idh.bridge.resources.Messages.getGMessage("WRNBPA10", Nothing), SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                'doWarnMess("Successfully updated the Address on WR1 documents(s)", SAPbouiCOM.BoMessageTime.bmt_Medium)
                'SBOForm.Items.Item("1").Click()
                ' SBOForm.Close()
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREUPADR", Nothing)
            End Try
            Return True
        End Function

    End Class
End Namespace