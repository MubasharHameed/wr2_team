﻿'
' Created by SharpDevelop.
' User: Louis
' Date: 2011/10/31
' Time: 07:19 PM
'
' Only the uBCoded team are authorized to use this new object to create new Forms.
'
Imports System.IO
Imports System.Collections
Imports System

Imports IDHAddOns.idh.controls
Imports com.idh.bridge.data
Imports com.idh.dbObjects

Namespace idh.forms.fr2.popup
	Public Class AdditionalMarkDocs
		
		'This is the Base Form object from the Framework
        Inherits com.idh.forms.oo.Form
	
		Private moMDGrid As FilterGrid
		Private msWOR As String 
		
		'**
		' Register the form controller
		'**
        Public Overloads Shared Function doRegisterFormClass() As IDHAddOns.idh.forms.Base
            Dim owForm As com.idh.forms.oo.FormController = New com.idh.forms.oo.FormController("IDH_ADDMD", 0, "AdditionalMDs.srf", False)
            Return owForm
        End Function
		
		Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal sParentId As String, ByRef oSBOForm As SAPbouiCOM.Form)
			MyBase.New(oIDHForm, sParentId, oSBOForm)
			
			doSetHandlers()
		End Sub
		
		Private Sub doSetHandlers()
		End Sub

		Public Overrides Sub doBeforeLoadData()
			msWOR = getParentSharedData("IDH_WOR")
	
			If moMDGrid Is Nothing Then
				moMDGrid = New FilterGrid(IDHForm, SBOForm, "IDHMDGRD", False)
                moMDGrid.doAddGridTable(New GridTable("@IDH_WOADDEXP", Nothing, Nothing, False, True), True)
			End If
			doSetListFields()
		End Sub

		Protected Sub doSetListFields()
			moMDGrid.doAddListField("U_ItemCd", "Item Code", False, 20, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
			moMDGrid.doAddListField("U_CustCd", "Customer Code", False, -1, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
			moMDGrid.doAddListField("U_SuppCd", "Supplier Code", False, -1, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
			moMDGrid.doAddListField("U_SINVNr", "Invoice", False, 50, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Invoice )
			moMDGrid.doAddListField("U_SONr", "Sales Order", False, 50, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Order)
			moMDGrid.doAddListField("U_PONr", "Purchase Order", False, 50, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder)
		End Sub

        '** The Initializer
        Public Overrides Sub doLoadData()
        	moMDGrid.setRequiredFilter("U_RowNr='" & msWOR & "'")
        	moMDGrid.doReloadData()
        End Sub
	End Class
End Namespace
