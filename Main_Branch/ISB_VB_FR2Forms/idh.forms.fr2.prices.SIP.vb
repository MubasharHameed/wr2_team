﻿'
' Created by SharpDevelop.
' User: Louis
' Date: 2012/01/28
' Time: 01:50 PM
'
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Imports System.IO
Imports System.Collections
Imports System

Imports IDHAddOns.idh.controls
'Imports com.idh.bridge
Imports com.idh.bridge.data
Imports com.idh.dbObjects
Imports com.idh.controls
Imports com.idh.dbObjects.User
Imports com.idh.bridge
Imports com.idh.bridge.lookups

Imports com.uBC.utils
Imports com.idh.dbObjects.numbers

Namespace idh.forms.fr2.prices
    Public Class SIP

        'This is the Base Form object from the Framework
        Inherits com.idh.forms.oo.Form

        Private moFormSettings As IDH_FORMSET

        Private moPricesGrid As DBOGrid 'The Result Grid
        Private moDBObject As IDH_SUITPR 'The SIP Data Object

        Private miNormalColor As Integer = &HFFFFFF
        'Private miLastEditRowIndex As Integer = -1

        Private miDefaultColor As Integer = &HFFFFFF

        Private miSelectedLineColor As Integer = &HBFF5CD
        Private miEditLineColor As Integer = &HA6F0F8

        Private mbCalledFromChanged As Boolean = False

        Private moMarkedEditLines As ArrayList = New ArrayList()

        Private mbDelayedLoad As Boolean = False
        Public Property DelayedLoad() As Boolean
            Get
                Return mbDelayedLoad
            End Get
            Set(ByVal value As Boolean)
                mbDelayedLoad = value
            End Set
        End Property

#Region "Form Item Setters"
        Public ItemGroup As String = ""

        Public Property SupplierCode() As String
            Get
                Return getUFValue("IDH_CARDCD").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_CARDCD", value)
                setUFValue("IDH_CARDNM", com.idh.bridge.lookups.Config.INSTANCE.doGetBPName(value))
            End Set
        End Property

        Public Property CustomerCode() As String
            Get
                Return getUFValue("IDH_BP2CD").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_BP2CD", value)
            End Set
        End Property

        Public Property BrancheCode() As String
            Get
                Return getUFValue("IDH_BRANCH").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_BRANCH", value)
            End Set
        End Property

        Public Property ContainerCode() As String
            Get
                Return getUFValue("IDH_ITMCD").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_ITMCD", value)
                setUFValue("IDH_ITMNM", com.idh.bridge.lookups.Config.INSTANCE.doGetItemDescription(value))
            End Set
        End Property

        Public Property JobType() As String
            Get
                Return getUFValue("IDH_JOBTP").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_JOBTP", value)
            End Set
        End Property

        Public Property ZipCode() As String
            Get
                Return getUFValue("IDH_POSCOD").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_POSCOD", value)
            End Set
        End Property

        Public Property Address() As String
            Get
                Return getUFValue("IDH_ADDR").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_ADDR", value)
            End Set
        End Property
        Public Property AddressLineNum() As String
            Get
                Return getUFValue("IDH_ADLN").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_ADLN", value)
            End Set
        End Property
        Public Property SupplierAddress() As String
            Get
                Return getUFValue("IDH_SPADDR").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_SPADDR", value)
            End Set
        End Property
        Public Property SupplierAddressLineNum() As String
            Get
                Return getUFValue("IDH_SPADLN").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_SPADLN", value)
            End Set
        End Property
        Public Property SupplierZipCode() As String
            Get
                Return getUFValue("IDH_SZIPCD").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_SZIPCD", value)
            End Set
        End Property

        Public Property WasteCode() As String
            Get
                Return getUFValue("IDH_WASTCD").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_WASTCD", value)
                setUFValue("IDH_WASTNM", com.idh.bridge.lookups.Config.INSTANCE.doGetItemDescription(value))
            End Set
        End Property

        Public Property WR1OrderType() As String
            Get
                Return getUFValue("IDH_WR1ORD").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_WR1ORD", value)
            End Set
        End Property

        Public Property Weight() As String
            Get
                Return getUFValue("IDH_WEIGHT").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_WEIGHT", value)
            End Set
        End Property

        Public Property UOM() As String
            Get
                Return getUFValue("IDH_UOM").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_UOM", value)
            End Set
        End Property

        Public Property DocDate() As String
            Get
                Return getUFValue("IDH_DOCDAT").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_DOCDAT", value)
            End Set
        End Property

        Public Property FilterMode() As Integer
            Get
                Return getUFValue("IDH_SWFLT").ToString()
            End Get
            Set(ByVal value As Integer)
                setUFValue("IDH_SWFLT", value)
            End Set
        End Property

        ''    doAddUF("IDH_CARDNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = False
        ''    doAddUF("IDH_ITMNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = False
        ''    doAddUF("IDH_WASTNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = False
#End Region

        '**
        ' Register the form controller
        '**
        Public Overloads Shared Function doRegisterFormClass(ByVal sParMenu As String) As IDHAddOns.idh.forms.Base
            Dim owForm As com.idh.forms.oo.FormController = New com.idh.forms.oo.FormController( _
              "IDH_SIP", _
              sParMenu, _
              5, _
              "CIP_SIP.srf", _
              True, _
              True, _
              False, _
              "Supplier Item Price", _
              IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL)

            ''Add the item to the list of items to get their filepaths fixed.
            'owForm.doAddImagesToFix("IDH_CRDL")
            'owForm.doAddImagesToFix("IDH_ITMCH")
            'owForm.doAddImagesToFix("IDH_ADDRCH")
            'owForm.doAddImagesToFix("IDH_SPADCH")
            'owForm.doAddImagesToFix("IDH_WASTSE")
            'owForm.doAddImagesToFix("IDH_CSTSL")
            'owForm.doAddImagesToFix("IDH_ADDSE")

            'IDHAddOns.idh.addon.Base.PARENT.doAddEvent( SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED, owForm.gsType )

            Return owForm
        End Function

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal sParentId As String, ByVal oSBOForm As SAPbouiCOM.Form)
            MyBase.New(oIDHForm, sParentId, oSBOForm)

            moDBObject = New IDH_SUITPR(IDHForm, SBOForm)
            moDBObject.ApplyDefaultsOnNewRows = False
            moDBObject.IgnoreCache = True

            doSetHandlers()
            '## Start  if CIP called from any other form as pop-up then here oSBOForm can be set nothing and we can use this
            If oSBOForm IsNot Nothing Then
                setSharedData("IDH_BPSRCHOPEND", "")
                setSharedData("IDH_ITM1SRCHOPEND", "")
                setSharedData("IDH_ITMSRCHOPEND", "")
                setSharedData("IDH_ADRSRCHOPEND", "")
            End If
            '## End
        End Sub

        '**
        '** This will be called by the base constructure
        '**
        Public Sub doSetHandlers()
            doStartEventFilterBatch(True)

            addHandler_ITEM_PRESSED("IDH_CRDL", New ev_Item_Event(AddressOf doSupplierLookup))
            addHandler_ITEM_PRESSED("IDH_BP2SL", New ev_Item_Event(AddressOf doCustomerLookup))
            addHandler_ITEM_PRESSED("IDH_ITMCH", New ev_Item_Event(AddressOf doItemLookup))
            addHandler_ITEM_PRESSED("IDH_ADDRCH", New ev_Item_Event(AddressOf doCustomerAddressLookup))
            addHandler_ITEM_PRESSED("IDH_SPADCH", New ev_Item_Event(AddressOf doSupplierAddressLookup))
            addHandler_ITEM_PRESSED("IDH_WASTSE", New ev_Item_Event(AddressOf doWasteCodeLookup))
            addHandler_ITEM_PRESSED("IDH_ADDSE", New ev_Item_Event(AddressOf doAdditionalCodeLookup))

            addHandler_ITEM_PRESSED("IDH_SWFLT", New ev_Item_Event(AddressOf doSwitchFilterType))
            addHandler_ITEM_PRESSED("IDH_SWSIM", New ev_Item_Event(AddressOf doSwitchFilterType))
            addHandler_ITEM_PRESSED("IDH_SWNFLT", New ev_Item_Event(AddressOf doSwitchFilterType))

            addHandler_COMBO_SELECT("IDH_BRANCH", New ev_Item_Event(AddressOf doSelectBranch))
            addHandler_COMBO_SELECT("IDH_JOBTP", New ev_Item_Event(AddressOf doSelectJobType))
            addHandler_COMBO_SELECT("IDH_WR1ORD", New ev_Item_Event(AddressOf doSelectWR1OrdType))
            addHandler_COMBO_SELECT("IDH_UOM", New ev_Item_Event(AddressOf doSelectUOM))
            addHandler_COMBO_SELECT("IDH_PRCGRP", New ev_Item_Event(AddressOf doSelectPriceGroup))

            addHandler_VALIDATE_CHANGED("IDH_POSCOD", New ev_Item_Event(AddressOf doZipCodeChange))

            addHandler_VALIDATE_CHANGED("IDH_CARDCD", New ev_Item_Event(AddressOf doSupplierChanged))
            addHandler_VALIDATE_CHANGED("IDH_BP2CD", New ev_Item_Event(AddressOf doCustomerChanged))
            addHandler_VALIDATE_CHANGED("IDH_ITMCD", New ev_Item_Event(AddressOf doItemChanged))
            addHandler_VALIDATE_CHANGED("IDH_ADDR", New ev_Item_Event(AddressOf doCustomerAddressChanged))
            addHandler_VALIDATE_CHANGED("IDH_SPADDR", New ev_Item_Event(AddressOf doSupplierAddressChanged))
            addHandler_VALIDATE_CHANGED("IDH_WASTCD", New ev_Item_Event(AddressOf doWasteCodeChanged))
            addHandler_VALIDATE_CHANGED("IDH_ADDCD", New ev_Item_Event(AddressOf doAdditionalCodeChanged))

            'Handler_ITEM_PRESSED = New ev_Item_Event( AddressOf doHandleItemPressedEvents )
            'Handler_Button_Ok = New et_Button_Ok( AddressOf doDoOkButtonEvent )
            Handler_VALIDATE_CHANGED = New ev_Item_Event(AddressOf doTriggerLookup)
            'Handler_COMBO_SELECT = New et_COMBO_SELECT( AddressOf doTriggerLookup )
            addHandler_ITEM_PRESSED("IDH_ACTPRC", New ev_Item_Event(AddressOf doShowOnlyActivePrices))

            'IDHAddOns.idh.addon.Base.PARENT.doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED, IDHForm.gsType)
            'IDHAddOns.idh.addon.Base.PARENT.doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK, IDHForm.gsType)
            'IDHAddOns.idh.addon.Base.PARENT.doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS, IDHForm.gsType)
            'IDHAddOns.idh.addon.Base.PARENT.doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS, IDHForm.gsType)
            'IDHAddOns.idh.addon.Base.PARENT.doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE, IDHForm.gsType)
            Handler_Button_Add = New ev_Item_Event(AddressOf doAddUpdateEvent)
            Handler_Button_Update = New ev_Item_Event(AddressOf doAddUpdateEvent)

            doCommitEventFilters()
        End Sub

#Region "FormEvents"
        Public Function doAddUpdateEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                Freeze(True)
                If moPricesGrid.doProcessData() Then
                    doLoadData()
                    moPricesGrid.doApplyRules()
                End If
                Freeze(False)
            End If
            Return True
        End Function

        '** Switch the Filter type
        Public Function doSwitchFilterType(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sSwitch As String = getUFValue("IDH_SWFLT")

                If sSwitch.Equals("1") Then
                    PaneLevel = 0
                    If SupplierCode.Length = 0 Then
                        SupplierCode = "FALLBACK"
                    End If
                ElseIf sSwitch.Equals("3") Then
                    PaneLevel = 0
                    doClearFormUFValues()
                    setUFValue("IDH_SWFLT", "3")
                Else
                    PaneLevel = 1

                    ItemGroup = ""
                    ContainerCode = ""
                    WasteCode = ""
                    WR1OrderType = ""

                    JobType = ""
                    DocDate = ""
                    Address = ""
                    ZipCode = ""
                    BrancheCode = ""
                    UOM = ""
                    Weight = ""
                End If
                doLoadData()
            End If
            Return True
        End Function

        '** The Fallback ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                End If

            Else
                ''## Start 08-07-2013
                ''Try to handle the customer code lookup in case if we paste full customer code and press tab
                ''Earlier it was not trigering the event and nothing happened
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE AndAlso pVal.BeforeAction = False Then
                    'If pVal.ItemUID = "IDH_CARDCD" Then 'AndAlso pVal.ItemChanged 
                    '    Dim sCardCode As String = getUFValue("IDH_CARDCD")
                    '    If sCardCode.Trim = "" Then
                    '        Return False
                    '    End If
                    '    doSupplierLookup(pVal, BubbleEvent)
                    '    Return False
                    'ElseIf pVal.ItemUID = "IDH_CSTCD" Then 'AndAlso pVal.ItemChanged 
                    '    Dim sCardCode As String = getUFValue("IDH_CSTCD")
                    '    If sCardCode.Trim = "" Then
                    '        Return False
                    '    End If
                    '    doCustomerLookup(pVal, BubbleEvent)
                    '    Return False
                    'ElseIf pVal.ItemUID = "IDH_ITMCD" Then
                    '    Dim sItemCode As String = getUFValue("IDH_ITMCD")
                    '    If sItemCode.Trim = "" Then
                    '        Return False
                    '    End If
                    '    doItemLookup(pVal, BubbleEvent)
                    '    Return False
                    'ElseIf pVal.ItemUID = "IDH_WASTCD" Then
                    '    Dim sItemCode As String = getUFValue("IDH_WASTCD")
                    '    If sItemCode.Trim = "" Then
                    '        Return False
                    '    End If
                    '    doWasteCodeLookup(pVal, BubbleEvent)
                    '    Return False
                    'ElseIf pVal.ItemUID = "IDH_SPADDR" Then
                    '    Dim sSupplierAddress As String = getUFValue("IDH_SPADDR")
                    '    If sSupplierAddress.Trim = "" Then
                    '        Return False
                    '    End If
                    '    doSupplierAddressLookup(pVal, BubbleEvent)
                    '    Return False
                    'ElseIf pVal.ItemUID = "IDH_ADDR" Then
                    '    Dim sCustomerAddress As String = getUFValue("IDH_ADDR")
                    '    If sCustomerAddress.Trim = "" Then
                    '        Return False
                    '    End If
                    '    doCustomerAddressChanged(pVal, BubbleEvent)
                    '    Return False
                    'ElseIf pVal.ItemUID = "IDH_ADDCD" Then
                    '    Dim sAdditionalCode As String = getUFValue("IDH_ADDCD")
                    '    If sAdditionalCode.Trim = "" Then
                    '        Return False
                    '    End If
                    '    doAdditionalCodeLookup(pVal, BubbleEvent)
                    '    Return False
                    'End If
                End If
                ''End
            End If
            Return True
        End Function

        '** The Postal Code change event
        '** Return True if the Event must be handled by the other Objects
        Public Function doZipCodeChange(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sZipCode As String = getUFValue("IDH_POSCOD")
                moDBObject.doAddDefaultValue(IDH_SUITPR._ZpCd, sZipCode)
                doLoadData()
            End If
            Return False
        End Function

        '** The do the Branch selection handler
        '** Return True if the Event must be handled by the other Objects
        Public Function doSelectBranch(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sBranch As String = getUFValue("IDH_BRANCH")
                moDBObject.doAddDefaultValue(IDH_SUITPR._Branch, sBranch)
                doLoadData()
            End If
            Return False
        End Function

        '** The do the Price Group selection handler
        '** Return True if the Event must be handled by the other Objects
        Public Function doSelectPriceGroup(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sPriceGroup As String = getUFValue("IDH_PRCGRP")
                setUFValue("IDH_CARDCD", sPriceGroup)
                doLoadData()
            End If
            Return False
        End Function


        '** The do the Job Type selection handler
        '** Return True if the Event must be handled by the other Objects
        Public Function doSelectJobType(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sJob As String = getUFValue("IDH_JOBTP")
                moDBObject.doAddDefaultValue(IDH_SUITPR._JobTp, sJob)
                doLoadData()
            End If
            Return False
        End Function

        '** The do the WR1Order Type selection handler
        '** Return True if the Event must be handled by the other Objects
        Public Function doSelectWR1OrdType(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sOrderType As String = getUFValue("IDH_WR1ORD")
                moDBObject.doAddDefaultValue(IDH_SUITPR._WR1ORD, sOrderType)
                doLoadData()
            End If
            Return False
        End Function

        '** The do the UOM selection handler
        '** Return True if the Event must be handled by the other Objects
        Public Function doSelectUOM(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sUOM As String = getUFValue("IDH_UOM")
                moDBObject.doAddDefaultValue(IDH_SUITPR._UOM, sUOM)
                doLoadData()
            End If
            Return False
        End Function

        '** The doTriggerLookup handler
        '** Return True if the Event must be handled by the other Objects
        Public Function doTriggerLookup(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If Not pVal.ItemUID.Equals("LINESGRID") AndAlso pVal.ItemChanged Then
                    doLoadData()
                End If
            End If
            Return False
        End Function

        '** The Supplier Choose Button
        '** Return True if the Event must be handled by the other Objects
        Public Function doSupplierLookup(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.Before_Action = False Then
                Dim sCardCode As String = getUFValue("IDH_CARDCD")
                '##Start 10-07-2013
                If sCardCode = "FALLBACK" Then
                    Return False
                End If
                '##
                setUFValue("IDH_CARDNM", "")

                setSharedData("TRG", "SUP")
                setSharedData("IDH_BPCOD", sCardCode)
                '##Start 03-07-2013
                setSharedData("IDH_NAME", "")
                '## End
                setSharedData("IDH_TYPE", "S")
                setSharedData("SILENT", "SHOWMULTI")

                'doOpenModalForm("IDHCSRCH")
                '##Start 03-07-2013
                'doOpenModalForm("IDHCSRCH")
                If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then

                    doOpenModalForm("IDHCSRCH") 'Actual BP Search Form 
                Else
                    If getSharedData("IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData("IDH_BPSRCHOPEND").ToString.Trim = "" Then
                        doOpenModalForm("IDHNCSRCH", moDBObject.SBOForm) 'NEW BP Search Form 
                    End If
                End If
                '## End
            End If
            Return False
        End Function

        '** The Supplier Changed
        '** Return True if the Event must be handled by the other Objects
        Public Function doSupplierChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sCardCode As String = getUFValue("IDH_CARDCD")

                mbCalledFromChanged = True
                If sCardCode.Length = 0 Then
                    setUFValue("IDH_CARDNM", "")
                    doLoadData()
                Else
                    Return doSupplierLookup(pVal, BubbleEvent)
                End If
            End If

            Return False
        End Function

        '** The customer Choose Button
        '** Return True if the Event must be handled by the other Objects
        Public Function doCustomerLookup(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.Before_Action = False Then
                Dim sCardCode As String = getUFValue("IDH_BP2CD")

                setSharedData("TRG", "CUS")
                setSharedData("IDH_BPCOD", sCardCode)
                '##Start 03-07-2013
                setSharedData("IDH_NAME", "")
                '## End
                setSharedData("IDH_TYPE", "C")
                setSharedData("SILENT", "SHOWMULTI")

                '##Start 03-07-2013
                'doOpenModalForm("IDHCSRCH")
                If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then

                    doOpenModalForm("IDHCSRCH") 'Actual BP Search Form 
                Else
                    If getSharedData("IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData("IDH_BPSRCHOPEND").ToString.Trim = "" Then
                        doOpenModalForm("IDHNCSRCH", moDBObject.SBOForm) 'NEW BP Search Form 
                    End If
                End If
                '## End
            End If
            Return False
        End Function

        '** The Customer Changed
        '** Return True if the Event must be handled by the other Objects
        Public Function doCustomerChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sCardCode As String = getUFValue("IDH_BP2CD")

                mbCalledFromChanged = True
                If sCardCode.Length = 0 Then
                    '## MA Start 16-07-2014' add check to execute code only once
                    If pVal.Before_Action = False Then
                        doLoadData()
                    End If
                    '## MA End 16-07-2014
                Else
                    Return doCustomerLookup(pVal, BubbleEvent)
                End If
            End If

            Return False
        End Function

        '** The Container Choose Button
        '** Return True if the Event must be handled by the other Objects
        Public Function doItemLookup(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.Before_Action = False Then
                Dim sItemCd As String = getUFValue("IDH_ITMCD")
                setUFValue("IDH_ITMNM", "")

                setSharedData("TRG", "ITM")
                setSharedData("IDH_ITMCOD", sItemCd)
                setSharedData("SILENT", "SHOWMULTI")

                ''## Start 05-07-2013
                'doOpenModalForm("IDHISRC")
                If getSharedData("IDH_ITM1SRCHOPEND") Is Nothing OrElse getSharedData("IDH_ITM1SRCHOPEND").ToString.Trim = "" Then

                    doOpenModalForm("IDHISRC")
                End If
                '## End
            End If
            Return False
        End Function

        '** The Container Changed
        '** Return True if the Event must be handled by the other Objects
        Public Function doItemChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sItemCd As String = getUFValue("IDH_ITMCD")

                mbCalledFromChanged = True
                If sItemCd.Length = 0 Then
                    setUFValue("IDH_ITMNM", "")
                    doLoadData()
                Else
                    Return doItemLookup(pVal, BubbleEvent)
                End If
            End If

            Return False
        End Function

        '** The Customer Address Choose Button
        '** Return True if the Event must be handled by the other Objects
        Public Function doCustomerAddressLookup(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.Before_Action = False Then
                Dim sCardCode As String = getUFValue("IDH_BP2CD")
                Dim sAddress As String = getUFValue("IDH_ADDR")

                If Not sCardCode Is Nothing AndAlso sCardCode.Length > 0 Then
                    setSharedData("TRG", "SA")
                    setSharedData("IDH_CUSCOD", sCardCode)
                    setSharedData("IDH_ADRTYP", "S")
                    setSharedData("SILENT", "SHOWMULTI")

                    setSharedData("IDH_ADDRES", sAddress)
                    '##Start 05-07-2013
                    'doOpenModalForm("IDHASRCH")
                    If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                        doOpenModalForm("IDHASRCH") 'Actual Address Search Form 
                    Else
                        If getSharedData("IDH_ADRSRCHOPEND") Is Nothing OrElse getSharedData("IDH_ADRSRCHOPEND").ToString.Trim = "" Then
                            doOpenModalForm("IDHASRCH3", moDBObject.SBOForm) 'NEW Address Search Form 
                        End If
                    End If
                    '## End
                End If
            End If
            Return False
        End Function

        '** The Customer Address Changed
        '** Return True if the Event must be handled by the other Objects
        Public Function doCustomerAddressChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sAddress As String = getUFValue("IDH_ADDR")

                mbCalledFromChanged = True
                If sAddress.Length = 0 Then
                    ZipCode = ""
                    AddressLineNum = ""
                    doLoadData()
                Else
                    Return doCustomerAddressLookup(pVal, BubbleEvent)
                End If
            End If

            Return False
        End Function

        '** The Supplier Address Choose Button
        '** Return True if the Event must be handled by the other Objects
        Public Function doSupplierAddressLookup(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.Before_Action = False Then
                Dim sCardCode As String = getUFValue("IDH_CARDCD")
                Dim sAddress As String = getUFValue("IDH_SPADDR")

                If Not sCardCode Is Nothing AndAlso sCardCode.Length > 0 Then
                    setSharedData("TRG", "SPA")
                    setSharedData("IDH_CUSCOD", sCardCode)
                    setSharedData("IDH_ADRTYP", "S")
                    setSharedData("SILENT", "SHOWMULTI")

                    setSharedData("IDH_ADDRES", sAddress)
                    '##Start 05-07-2013
                    'doOpenModalForm("IDHASRCH")
                    If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                        doOpenModalForm("IDHASRCH") 'Actual Address Search Form 
                    Else
                        If getSharedData("IDH_ADRSRCHOPEND") Is Nothing OrElse getSharedData("IDH_ADRSRCHOPEND").ToString.Trim = "" Then
                            doOpenModalForm("IDHASRCH3", moDBObject.SBOForm) 'NEW Address Search Form 
                        End If
                    End If
                    '## End
                End If
            End If
            Return False
        End Function

        '** The Supplier Address Changed
        '** Return True if the Event must be handled by the other Objects
        Public Function doSupplierAddressChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sAddress As String = getUFValue("IDH_SPADDR")

                mbCalledFromChanged = True
                If sAddress.Length = 0 Then
                    SupplierZipCode = ""
                    SupplierAddressLineNum = ""
                    doLoadData()
                Else
                    Return doSupplierAddressLookup(pVal, BubbleEvent)
                End If
            End If

            Return False
        End Function

        '** The Waste Code Choose Button
        '** Return True if the Event must be handled by the other Objects
        Public Function doWasteCodeLookup(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.Before_Action = False Then
                Dim sWasteCode As String = getUFValue("IDH_WASTCD")
                setUFValue("IDH_WASTNM", "")

                setSharedData("TRG", "WC")
                setSharedData("IDH_GRPCOD", com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup())
                setSharedData("IDH_ITMCOD", sWasteCode)
                setSharedData("IDH_ITMNAM", "")
                setSharedData("IDH_INVENT", "")
                setSharedData("SILENT", "SHOWMULTI")

                ''##Start 05-07-2013
                'doOpenModalForm("IDHWISRC")
                If getSharedData("IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedData("IDH_ITMSRCHOPEND").ToString.Trim = "" Then

                    doOpenModalForm("IDHWISRC")
                End If
                ''End
            End If
            Return False
        End Function

        '** The Waste Code Changed
        '** Return True if the Event must be handled by the other Objects
        Public Function doWasteCodeChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sWasteCode As String = getUFValue("IDH_WASTCD")

                If sWasteCode.Length = 0 Then
                    setUFValue("IDH_WASTNM", "")
                    doLoadData()
                Else
                    mbCalledFromChanged = True
                    Return doWasteCodeLookup(pVal, BubbleEvent)
                End If
            End If

            Return False
        End Function

        '** The Additional Code Choose Button
        '** Return True if the Event must be handled by the other Objects
        Public Function doAdditionalCodeLookup(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.Before_Action = False Then
                Dim sAdditionalCode As String = getUFValue("IDH_ADDCD")
                setUFValue("IDH_ADDNM", "")

                setSharedData("TRG", "ADD")
                setSharedData("IDH_GRPCOD", com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup())
                setSharedData("NOT_GRPCOD", com.idh.bridge.lookups.Config.ParameterWithDefault("IGNWSTGP", ""))
                setSharedData("IDH_ITMCOD", sAdditionalCode)
                setSharedData("IDH_ITMNAM", "")
                setSharedData("IDH_INVENT", "N")
                setSharedData("SILENT", "SHOWMULTI")

                'doOpenModalForm("IDHISRC")
                ''## Start 05-07-2013
                'doOpenModalForm("IDHISRC")
                If getSharedData("IDH_ITM1SRCHOPEND") Is Nothing OrElse getSharedData("IDH_ITM1SRCHOPEND").ToString.Trim = "" Then

                    doOpenModalForm("IDHISRC")
                End If
                '## End
            End If
            Return False
        End Function

        '** The Additional Code Changed
        '** Return True if the Event must be handled by the other Objects
        Public Function doAdditionalCodeChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sAdditionalCode As String = getUFValue("IDH_ADDCD")

                mbCalledFromChanged = True
                If sAdditionalCode.Length = 0 Then
                    setUFValue("IDH_ADDNM", "")
                    doLoadData()
                Else
                    Return doAdditionalCodeLookup(pVal, BubbleEvent)
                End If
            End If

            Return False
        End Function

        Public Function doShowOnlyActivePrices(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then

                mbCalledFromChanged = True
                doLoadData()
            End If
            Return False
        End Function
#End Region

#Region "Dialog Handler"
        'Public Overrides Function doCustomItemEvent(ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
        '    If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SORT Then
        '        If pVal.BeforeAction = True Then
        '            If SupplierCode.Equals("FALLBACK") Then
        '                SupplierCode = ""
        '            End If
        '        Else
        '            If String.IsNullOrWhiteSpace(SupplierCode) Then
        '                SupplierCode = "FALLBACK"
        '            End If
        '        End If
        '    End If
        '    Return True
        'End Function
        '## MA Start 07-07-2014
        'Public Overrides Function doCustomItemEvent(ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
        '    'Clear the description field if code is empty
        '    'If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED AndAlso pVal.BeforeAction = False Then
        '    '    If pVal.ColUID = IDH_SUITPR._StAddr Then
        '    '        Dim sAddress As String = pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_SUITPR._StAddr), pVal.Row)
        '    '        If sAddress.Trim = "" Then
        '    '            pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_SUITPR._StAdLN), pVal.Row, "")
        '    '        Else
        '    '            Dim sCardCode As String = IDH_SUITPR._BP2CD
        '    '            If sCardCode.Trim <> "" AndAlso sAddress.Trim <> "" Then
        '    '                Dim sAddressLineNum As String = com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCardCode, sAddress, "LineNum")
        '    '                pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_SUITPR._StAdLN), pVal.Row, sAddressLineNum)
        '    '            Else
        '    '                pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_SUITPR._StAdLN), pVal.Row, "")
        '    '            End If
        '    '        End If
        '    '    ElseIf pVal.ColUID = IDH_SUITPR._SupAddr Then
        '    '        Dim sAddress As String = pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_SUITPR._SupAddr), pVal.Row)
        '    '        If sAddress.Trim = "" Then
        '    '            pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_SUITPR._SupAdLN), pVal.Row, "")
        '    '        Else
        '    '            Dim sCardCode As String = IDH_SUITPR._CardCd
        '    '            If sCardCode.Trim <> "" AndAlso sAddress.Trim <> "" Then
        '    '                Dim sAddressLineNum As String = com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCardCode, sAddress, "LineNum")
        '    '                pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_SUITPR._SupAdLN), pVal.Row, sAddressLineNum)
        '    '            Else
        '    '                pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_SUITPR._SupAdLN), pVal.Row, "")
        '    '            End If
        '    '        End If
        '    '    End If
        '    'End If
        '    '    'Clear the description field if code is empty
        '    '    If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED AndAlso pVal.BeforeAction = False Then
        '    '        If (pVal.ColUID = "U_WasteDs" OrElse pVal.ColUID = "U_WasteCd") Then 'AndAlso pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED AndAlso pVal.BeforeAction = False Then
        '    '            Dim sWasteCode As String = pVal.oGrid.doGetFieldValue("U_WasteCd", pVal.Row)
        '    '            Dim sWasteDesc As String = pVal.oGrid.doGetFieldValue("U_WasteDs", pVal.Row)
        '    '            If sWasteCode.Trim = "" AndAlso sWasteDesc.Trim <> "" Then
        '    '                pVal.oGrid.doSetFieldValue("U_WasteDs", pVal.Row, "")
        '    '            End If
        '    '        ElseIf (pVal.ColUID = "U_AItmCd" OrElse pVal.ColUID = "U_AItmDs") Then 'AndAlso pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED AndAlso pVal.BeforeAction = False Then
        '    '            Dim sAItemCode As String = pVal.oGrid.doGetFieldValue("U_AItmCd", pVal.Row)
        '    '            Dim sAItemDesc As String = pVal.oGrid.doGetFieldValue("U_AItmDs", pVal.Row)
        '    '            If sAItemCode.Trim = "" AndAlso sAItemDesc.Trim <> "" Then
        '    '                pVal.oGrid.doSetFieldValue("U_AItmDs", pVal.Row, "")
        '    '            End If
        '    '        ElseIf (pVal.ColUID = "U_ItemCd" OrElse pVal.ColUID = "U_ItemDs") Then
        '    '            Dim sItemCode As String = pVal.oGrid.doGetFieldValue("U_ItemCd", pVal.Row)
        '    '            Dim sItemDesc As String = pVal.oGrid.doGetFieldValue("U_ItemDs", pVal.Row)
        '    '            If sItemCode.Trim = "" AndAlso sItemDesc.Trim <> "" Then
        '    '                pVal.oGrid.doSetFieldValue("U_ItemDs", pVal.Row, "")
        '    '            End If
        '    '        End If
        '    '    End If
        '    Return MyBase.doCustomItemEvent(pVal)
        'End Function
        '## MA End 07-07-2014        

        Public Overrides Sub doHandleModalCanceled(ByVal sModalFormType As String)
            MyBase.doHandleModalCanceled(sModalFormType)
            If mbCalledFromChanged AndAlso _
                           (sModalFormType.Equals("IDHCSRCH") OrElse _
                             sModalFormType.Equals("IDHNCSRCH") OrElse _
                            sModalFormType.Equals("IDHISRC") OrElse _
                            sModalFormType.Equals("IDHASRCH") OrElse _
                            sModalFormType.Equals("IDHWISRC")) OrElse _
                            sModalFormType.Equals("IDHASRCH3") Then

                doLoadData()
            End If
            If sModalFormType.Equals("IDHCSRCH") OrElse sModalFormType.Equals("IDHNCSRCH") Then 'Customer
                Dim sTarget As String
                sTarget = getSharedData("TRG")
                If sTarget Is Nothing Then Exit Sub
                If sTarget.Equals("SUP") Then
                    If Not SBOForm.ActiveItem.Equals("IDH_CARDCD") Then
                        setFocus("IDH_CARDCD")
                        setUFValue("IDH_CARDCD", "")
                        setUFValue("IDH_CARDNM", "")
                        doLoadData()
                    End If
                ElseIf sTarget.Equals("CUS") Then
                    If Not SBOForm.ActiveItem.Equals("IDH_BP2CD") Then
                        setFocus("IDH_BP2CD")
                        setUFValue("IDH_BP2CD", "")
                        doLoadData()
                    End If
                End If
            ElseIf sModalFormType.Equals("IDHASRCH") OrElse sModalFormType.Equals("IDHASRCH3") Then 'Address
                Dim sTarget As String
                sTarget = getSharedData("TRG")
                If sTarget.Equals("SA") Then
                    If Not SBOForm.ActiveItem.Equals("IDH_ADDR") Then
                        setFocus("IDH_ADDR")
                        setUFValue("IDH_ADDR", "")
                        setUFValue("IDH_ADLN", "")
                        setUFValue("IDH_POSCOD", "")
                        doLoadData()
                    End If
                ElseIf sTarget.Equals("SPA") Then
                    If Not SBOForm.ActiveItem.Equals("IDH_SPADDR") Then
                        setFocus("IDH_SPADDR")
                        setUFValue("IDH_SPADDR", "")
                        setUFValue("IDH_SPADLN", "")
                        setUFValue("IDH_SZIPCD", "")
                        doLoadData()
                    End If
                End If
            ElseIf sModalFormType.Equals("IDHISRC") Then
                Dim sTarget As String = getSharedData("TRG")
                If sTarget.Equals("ITM") Then 'Container Code (ItemLookup)
                    If Not SBOForm.ActiveItem.Equals("IDH_ITMCD") Then
                        setFocus("IDH_ITMCD")
                        setUFValue("IDH_ITMCD", "")
                        setUFValue("IDH_ITMNM", "")
                        doLoadData()
                    End If
                ElseIf sTarget.Equals("ADD") Then ' Additional Code Looup
                    If Not SBOForm.ActiveItem.Equals("IDH_ADDCD") Then
                        setFocus("IDH_ADDCD")
                        setUFValue("IDH_ADDCD", "")
                        setUFValue("IDH_ADDNM", "")
                        doLoadData()
                    End If
                End If
            ElseIf sModalFormType.Equals("IDHWISRC") Then 'WasteCodeLooup
                If Not SBOForm.ActiveItem.Equals("IDH_WASTCD") Then
                    setFocus("IDH_WASTCD")
                    setUFValue("IDH_WASTCD", "")
                    setUFValue("IDH_WASTNM", "")
                    doLoadData()
                End If
            End If
            ''## End
            mbCalledFromChanged = False
        End Sub

        'CLOSING MODAL RECEIVING DATA
        Public Overrides Sub doHandleModalResultShared(ByVal sModalFormType As String, ByVal sLastButton As String)
            mbCalledFromChanged = False
            Try
                If sModalFormType.Equals("IDHCSRCH") OrElse sModalFormType.Equals("IDHNCSRCH") Then
                    Dim sTarget, sCardCode, sCardName As String
                    sTarget = getSharedData("TRG")
                    If sTarget.Equals("SUP") Then
                        sCardCode = getSharedData("CARDCODE")
                        sCardName = getSharedData("CARDNAME")

                        setUFValue("IDH_CARDCD", sCardCode)
                        setUFValue("IDH_CARDNM", sCardName)

                        moDBObject.doAddDefaultValue(IDH_SUITPR._CardCd, sCardCode)

                        doLoadData()
                    ElseIf sTarget.Equals("CUS") Then
                        sCardCode = getSharedData("CARDCODE")
                        sCardName = getSharedData("CARDNAME")

                        setUFValue("IDH_BP2CD", sCardCode)

                        moDBObject.doAddDefaultValue(IDH_SUITPR._BP2CD, sCardCode)

                        doLoadData()
                    End If
                ElseIf sModalFormType.Equals("IDHISRC") Then
                    Dim sTarget, sItemCode, sItemName As String
                    sTarget = getSharedData("TRG")
                    If sTarget.Equals("ITM") Then
                        sItemCode = getSharedData("ITEMCODE")
                        sItemName = getSharedData("ITEMNAME")

                        setUFValue("IDH_ITMCD", sItemCode)
                        setUFValue("IDH_ITMNM", sItemName)

                        moDBObject.doAddDefaultValue(IDH_SUITPR._ItemCd, sItemCode)
                        moDBObject.doAddDefaultValue(IDH_SUITPR._ItemDs, sItemName)

                        doLoadData()
                    ElseIf sTarget.Equals("ADD") Then
                        sItemCode = getSharedData("ITEMCODE")
                        sItemName = getSharedData("ITEMNAME")

                        setUFValue("IDH_ADDCD", sItemCode)
                        setUFValue("IDH_ADDNM", sItemName)

                        moDBObject.doAddDefaultValue(IDH_CSITPR._AItmCd, sItemCode)
                        moDBObject.doAddDefaultValue(IDH_CSITPR._AItmDs, sItemName)

                        doLoadData()
                    End If
                ElseIf sModalFormType.Equals("IDHASRCH") OrElse sModalFormType.Equals("IDHASRCH3") Then
                    Dim sTarget As String
                    sTarget = getSharedData("TRG")
                    If sTarget.Equals("SA") Then
                        '                        sAddr = getSharedData("ADDRESS")
                        '
                        Address = getSharedData("ADDRESS")
                        ZipCode = getSharedData("ZIPCODE")
                        AddressLineNum = getSharedData("ADDRSCD")
                        moDBObject.doAddDefaultValue(IDH_SUITPR._StAddr, Address)
                        moDBObject.doAddDefaultValue(IDH_SUITPR._StAdLN, AddressLineNum)
                        moDBObject.doAddDefaultValue(IDH_CSITPR._ZpCd, ZipCode)

                        doLoadData()
                    ElseIf sTarget.Equals("SPA") Then
                        '                        sAddr = getSharedData("ADDRESS")
                        '
                        SupplierAddress = getSharedData("ADDRESS")
                        SupplierZipCode = getSharedData("ZIPCODE")
                        SupplierAddressLineNum = getSharedData("ADDRSCD")

                        moDBObject.doAddDefaultValue(IDH_SUITPR._SupAddr, SupplierAddress)
                        moDBObject.doAddDefaultValue(IDH_SUITPR._SupAdLN, SupplierAddressLineNum)
                        moDBObject.doAddDefaultValue(IDH_SUITPR._SupZipCd, SupplierZipCode)

                        doLoadData()
                    End If
                ElseIf sModalFormType.Equals("IDHWISRC") Then
                    Dim sTarget, sWasteCd, sWasteName, sUOM As String
                    sTarget = getSharedData("TRG")
                    If sTarget.Equals("WC") Then
                        sWasteCd = getSharedData("ITEMCODE")
                        sWasteName = getSharedData("ITEMNAME")
                        sUOM = getSharedData("PUOM")
                        If sUOM Is Nothing OrElse sUOM.Length = 0 Then
                            sUOM = lookups.Config.INSTANCE.getDefaultUOM()
                        End If

                        setUFValue("IDH_WASTCD", sWasteCd)
                        setUFValue("IDH_WASTNM", sWasteName)

                        moDBObject.doAddDefaultValue(IDH_SUITPR._WasteCd, sWasteCd)
                        moDBObject.doAddDefaultValue(IDH_SUITPR._WasteDs, sWasteName)
                        moDBObject.doAddDefaultValue(IDH_CSITPR._UOM, sUOM)

                        doLoadData()
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal result - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub
#End Region

#Region "Form Initializers"
        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(BubbleEvent)

            Dim oItem As SAPbouiCOM.Item = Items.Item("IDH_CONTA")
            moPricesGrid = New DBOGrid(IDHForm, SBOForm, "LINESGRID", _
                                 oItem.Left, oItem.Top, oItem.Width, oItem.Height, moDBObject)

            Dim fSettings As SAPbouiCOM.FormSettings
            fSettings = SBOForm.Settings
            fSettings.Enabled = False

            doAddUF("IDH_SWFLT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 5).AffectsFormMode = False

            oItem = doAddUF("IDH_PRCGRP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 8)
            oItem.AffectsFormMode = False

            Dim oOpt As SAPbouiCOM.OptionBtn = Nothing
            oItem = Items.Item("IDH_SWFLT")
            oItem.AffectsFormMode = False
            oOpt = oItem.Specific
            oOpt.ValOn = "1"
            oOpt.Selected = True

            oItem = Items.Item("IDH_SWSIM")
            oItem.AffectsFormMode = False
            oOpt = oItem.Specific
            oOpt.ValOn = "2"
            oOpt.GroupWith("IDH_SWFLT")

            oItem = Items.Item("IDH_SWNFLT")
            oItem.AffectsFormMode = False
            oOpt = oItem.Specific
            oOpt.ValOn = "3"
            oOpt.GroupWith("IDH_SWFLT")

            Items.Item("42").Visible = True
            Items.Item("IDH_BP2LK").Visible = True
            Items.Item("IDH_BP2CD").Visible = True
            Items.Item("IDH_BP2SL").Visible = True

            Items.Item("46").Visible = True
            Items.Item("IDH_SPADDR").Visible = True
            Items.Item("IDH_SPADCH").Visible = True

            Items.Item("54").Visible = True
            Items.Item("IDH_SZIPCD").Visible = True

            EnableItem(True, "IDH_SZIPCD")
            EnableItem(True, "IDH_POSCOD")

            Dim oLink As SAPbouiCOM.LinkedButton
            oLink = Items.Item("IDH_LINKCR").Specific
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

            oLink = Items.Item("IDH_ITMLK").Specific
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items

            oLink = Items.Item("IDH_WASTLK").Specific
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items

            oLink = Items.Item("IDH_ADDLK").Specific
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items

            oLink = Items.Item("IDH_BP2LK").Specific
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

            Dim oChk As SAPbouiCOM.CheckBox = Nothing
            oChk = Items.Item("IDH_ACTPRC").Specific
            oChk.ValOff = "N"
            oChk.ValOn = "Y"

            SupportedModes = SAPbouiCOM.BoFormMode.fm_OK_MODE Or SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            AutoManaged = False
        End Sub

        ''
        ' ! - Negate the value - NOT - WHERE NOT x = Value
        ' ^ - Null Or value - OR - WHERE x Is NULL Or x='' Or x = value
        ' EQUEM - use equal and considerempty strings
        ' REPLACE - replace the [@] with the input value
        ' AUTO - this will make any "*" filter a LIKE and any "," filter a IN
        ' MLIKE - Multiple Likes => (x  LIKE y* OR z LINE u*)
        ' IN - will use the "in" option => in ()
        ' FNZ_LIKE - The firsttime NULL and Zero Length must be included => IS NULL OR = '' OR LIKE val%
        ''
        Protected Sub doSetFilterFields()
            moPricesGrid.doAddFilterField("IDH_CARDCD", IDH_SUITPR._CardCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "EQUEM", 30)
            doAddUF("IDH_CARDNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = False

            moPricesGrid.doAddFilterField("IDH_BP2CD", IDH_SUITPR._BP2CD, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            moPricesGrid.doAddFilterField("IDH_BRANCH", IDH_SUITPR._Branch, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            moPricesGrid.doAddFilterField("IDH_ITMCD", IDH_SUITPR._ItemCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            doAddUF("IDH_ITMNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = False

            moPricesGrid.doAddFilterField("IDH_JOBTP", IDH_SUITPR._JobTp, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            moPricesGrid.doAddFilterField("IDH_POSCOD", IDH_SUITPR._ZpCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            moPricesGrid.doAddFilterField("IDH_ADDR", IDH_SUITPR._StAddr, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)
            doAddUF("IDH_ADLN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10).AffectsFormMode = False

            moPricesGrid.doAddFilterField("IDH_SPADDR", IDH_SUITPR._SupAddr, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)
            moPricesGrid.doAddFilterField("IDH_SZIPCD", IDH_SUITPR._SupZipCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            doAddUF("IDH_SPADLN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10).AffectsFormMode = False

            moPricesGrid.doAddFilterField("IDH_WASTCD", IDH_SUITPR._WasteCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            doAddUF("IDH_WASTNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = False

            moPricesGrid.doAddFilterField("IDH_ADDCD", IDH_CSITPR._AItmCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            doAddUF("IDH_ADDNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = False

            moPricesGrid.doAddFilterField("IDH_WR1ORD", IDH_SUITPR._WR1ORD, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            doAddUF("IDH_WEIGHT", SAPbouiCOM.BoDataType.dt_QUANTITY, 30).AffectsFormMode = False

            moPricesGrid.doAddFilterField("IDH_UOM", IDH_SUITPR._UOM, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            doAddUF("IDH_DOCDAT", SAPbouiCOM.BoDataType.dt_DATE, 30).AffectsFormMode = False

            doAddUF("IDH_INFO", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 400).AffectsFormMode = False

            doAddUFCheck("IDH_ACTPRC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")

        End Sub

        ''MA Start 11-11-2014 Issue#479
        Protected Overridable Sub doTheGridLayout()
            If Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                If moFormSettings Is Nothing Then
                    Dim sFormTypeId As String = SBOForm.TypeEx
                    'Dim sGridID As String = sFormTypeId & "." & moPricesGrid.GridId
                    moFormSettings = New IDH_FORMSET()
                    moFormSettings.getFormGridFormSettings(sFormTypeId, moPricesGrid.GridId, "")
                    moFormSettings.doAddFieldToGrid(moPricesGrid)

                    doSetListFields()
                    moFormSettings.doSyncDB(moPricesGrid)
                Else
                    If moFormSettings.SkipFormSettings Then
                        doSetListFields()
                    Else
                        moFormSettings.doAddFieldToGrid(moPricesGrid)
                    End If
                End If
            Else
                doSetListFields()
            End If
        End Sub
        ''MA End 11-11-2014 Issue#479

        '		SRC:IDHISRC(PROD)[ITEMCODE=U_ItemCd;IDH_GRPCOD=#Test][U_ItemCd=ITEMCODE;U_ItemDs=ITEMNAME]
        '		   :FORMTPE(TARGET)[INPUT][OUTPUT]
        '					 INPUT - %USE VALUE AS IS
        '							 #USE GRID VALUE
        '							 >USE ITEM VALUE
        '					 OUTPUT - SRCFIELDVAL - use this value on as the new value of the current field
        '							  COLID=SRCFIELDVAL - Use the result of the search field val as the new value for the
        '												- Noted Col
        '							  (EQVAL?NEWVAL:VAL) - If the Value = to EQVAL Then Use NEWVAL else use Val
        Protected Overridable Sub doSetListFields()
            Dim sAddGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup()
            Dim sWastGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
            
            moPricesGrid.doAddListField(IDH_SUITPR._Code, "Code", False, 0, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._Name, "Name", False, 0, Nothing, Nothing)

            moPricesGrid.doAddListField(IDH_SUITPR._AItmPr, "Additional Price", True, -1, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._AAddExp, "Auto Add", True, -1, "COMBOBOX", Nothing)
            'moPricesGrid.doAddListField(IDH_SUITPR._Haulge, "Haul Price", True, -1, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._Haulge, "Haulage Cost", True, -1, Nothing, Nothing) '#MA issue:400 20170503
            moPricesGrid.doAddListField(IDH_SUITPR._HaulCal, "Haul. Charge Calc", True, -1, "COMBOBOX", Nothing)
            'moPricesGrid.doAddListField(IDH_SUITPR._TipTon, "Tip Price", True, -1, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._TipTon, "Disposal Cost", True, -1, Nothing, Nothing) '#MA issue:400 20170503
            moPricesGrid.doAddListField(IDH_SUITPR._ChrgCal, "Charge Calc", True, -1, "COMBOBOX", Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._FTon, "From (Unit)", True, -1, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._TTon, "To (Unit)", True, -1, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._UOM, "UOM", True, -1, "COMBOBOX", Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._StDate, "Start", True, -1, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._EnDate, "Finish", True, -1, Nothing, Nothing)
            '## MA Start 16-07-2014' remove trigger for UOM change to default value, as it was changing UOM to default when ever user change Waste code 
            'moPricesGrid.doAddListField(IDH_SUITPR._WasteCd, "Waste Code", True, -1, "SRC*IDHWISRC(WST01)[IDH_ITMCOD;IDH_INVENT=;IDH_GRPCOD=" & sWastGrp & "][ITEMCODE;U_WasteDs=ITEMNAME;U_UOM=PUOM(?" & sDefaultUOM & ")]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            moPricesGrid.doAddListField(IDH_SUITPR._WasteCd, "Waste Code", True, -1, "SRC*IDHWISRC(WST01)[IDH_ITMCOD;IDH_INVENT=;IDH_GRPCOD=" & sWastGrp & "][ITEMCODE;U_WasteDs=ITEMNAME;U_UOM=PUOM]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            '## MA End 16-07-2014
            moPricesGrid.doAddListField(IDH_SUITPR._WasteDs, "Waste Desc.", True, -1, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._AItmCd, "Additional Code", True, -1, "SRC*IDHISRC(GADDX)[IDH_ITMCOD;IDH_GRPCOD=" & sAddGrp & "][ITEMCODE;U_AItmDs=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            moPricesGrid.doAddListField(IDH_SUITPR._AItmDs, "Additional Desc", True, -1, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._ItemCd, "Container Code", True, -1, "SRC*IDHISRC(ITM01)[IDH_ITMCOD][ITEMCODE;U_ItemDs=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            moPricesGrid.doAddListField(IDH_SUITPR._ItemDs, "Container Desc.", True, -1, Nothing, Nothing)
            'moPricesGrid.doAddListField(IDH_SUITPR._StAddr, "Site Addr.", True, -1, "SRC*IDHASRCH(CADDR)[IDH_CUSCOD=#" + IDH_SUITPR._CardCd + ";IDH_ADRTYP=%S][ADDRESS;U_ZpCd=ZIPCODE]", Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._StAddr, "Customer Addr.", True, -1, "SRC*IDHASRCH(GCAD)[IDH_CUSCOD=#" + IDH_SUITPR._BP2CD + ";IDH_ADRTYP=%S][ADDRESS;" + IDH_SUITPR._ZpCd + "=ZIPCODE;" + IDH_SUITPR._StAdLN + "=ADDRSCD]", Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._ZpCd, "Cust. Postal Code", True, -1, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._StAdLN, "Address Line Num", True, 0, Nothing, Nothing)
            'moPricesGrid.doAddListField(IDH_SUITPR._Distance, "Milage", True, -1, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._SupAddr, "Supplier Addr.", True, -1, "SRC*IDHASRCH(GSAD)[IDH_CUSCOD=#" + IDH_SUITPR._CardCd + ";IDH_ADRTYP=%S][ADDRESS;" + IDH_SUITPR._SupZipCd + "=ZIPCODE;" + IDH_SUITPR._SupAdLN + "=ADDRSCD]", Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._SupZipCd, "Supp. Postal Code", True, -1, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._SupAdLN, "Supp. Address Line Num", True, 0, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._WR1ORD, "WR1 Order", True, -1, "COMBOBOX", Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._JobTp, "Order Type", True, -1, "COMBOBOX", Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._Branch, "Branch", True, -1, "COMBOBOX", Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._CardCd, "Supplier", True, -1, "SRC*IDHCSRCH(SUP01)[IDH_BPCOD;IDH_TYPE=F-S][CARDCODE]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            moPricesGrid.doAddListField(IDH_SUITPR._BP2CD, "Customer", True, -1, "SRC*IDHCSRCH(CUS01)[IDH_BPCOD;IDH_TYPE=F-C][CARDCODE]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            moPricesGrid.doAddListField(IDH_CSITPR._Currency, "Currency", True, -1, "COMBOBOX", Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._ItmPLs, "Item Price List", False, 0, "COMBOBOX", Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._ItemPr, "Item Price", False, 0, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._ItemTPr, "Item Tip. Price", False, 0, Nothing, Nothing)

            moPricesGrid.doAddListField(IDH_SUITPR._FrmBsWe, "Weight Base From", False, -1, Nothing, Nothing)
            moPricesGrid.doAddListField(IDH_SUITPR._ToBsWe, "Weight Base To", False, -1, Nothing, Nothing)
        End Sub

        Public Overrides Sub doBeforeLoadData()
            Dim oOpt As SAPbouiCOM.OptionBtn = Nothing
            oOpt = Items.Item("IDH_SWFLT").Specific
            oOpt.Selected = True

            setUFValue("IDH_SWFLT", "1")
            PaneLevel = 0

            If moPricesGrid Is Nothing Then
                moPricesGrid = DBOGrid.getInstance(SBOForm, "LINESGRID")
                If moPricesGrid Is Nothing Then
                    moPricesGrid = New DBOGrid(IDHForm, SBOForm, "LINESGRID", moDBObject)
                End If
            End If
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_CIP_SIP_DELETE) = False OrElse _
                Config.INSTANCE.doGetIsSupperUser() Then
                moPricesGrid.doSetDeleteActive(True)
            Else
                moPricesGrid.doSetDeleteActive(False)
            End If

            ''MA Start 11-11-2014 Issue#479
            'doSetListFields()
            'Set the required List Fields
            doTheGridLayout()
            ''MA End 11-11-2014 Issue#479
            doSetFilterFields()

            If SupplierCode.Length = 0 Then
                SupplierCode = "FALLBACK"
            End If

            moPricesGrid.Handler_GRID_ROW_ADD_EMPTY = New IDHAddOns.idh.controls.IDHGrid.ev_GRID_EVENTS(AddressOf doSetLastLine)
            moPricesGrid.Handler_GRID_DATA_KEY_EMPTY = New IDHAddOns.idh.controls.IDHGrid.ev_GRID_EVENTS(AddressOf doCreateKey)
            moPricesGrid.Handler_GRID_SORT = New IDHAddOns.idh.controls.IDHGrid.ev_GRID_EVENTS(AddressOf doSort)
            'moPricesGrid.Handler_GRID_SEARCH_VALUESET = New IDHAddOns.idh.controls.IDHGrid.ev_GRID_EVENTS(AddressOf doValidateUOMFromSearch)
            moPricesGrid.Handler_GRID_ROW_DEL = New IDHAddOns.idh.controls.IDHGrid.ev_GRID_EVENTS(AddressOf doHandleGridDelete)
            doFillCombos(True)
        End Sub
#End Region

#Region "LoadCombo"
        Protected Sub doFillCombos(ByVal bDoFormCombo As Boolean)
            doCalcTypeCombo(bDoFormCombo)
            doCalcTypeHaulCombo(bDoFormCombo)
            doJobTypeCombo(bDoFormCombo)
            doWR1OrdTypeCombo(bDoFormCombo)
            doUOMCombo(bDoFormCombo)
            doBranchCombo(bDoFormCombo)
            ''            doPriceListCombo()
            doAddAddCombo(bDoFormCombo)

            If bDoFormCombo Then
                FillCombos.PriceGroupsCombo(Items.Item("IDH_PRCGRP").Specific, "S", "FALLBACK", "FALLBACK")
            End If

            If bDoFormCombo = False Then
                Dim oCombo As SAPbouiCOM.ComboBoxColumn
                oCombo = moPricesGrid.Columns.Item(moPricesGrid.doIndexFieldWC(IDH_SUITPR._Currency))
                com.uBC.utils.FillCombos.CurrencyCombo(oCombo)
            End If

        End Sub

        Private Sub doCalcTypeCombo(ByVal bDoFormCombo As Boolean)
            Try
                If bDoFormCombo Then
                    ''FORM COMBO

                Else
                    ''GRID COMBO
                    Dim oCombo As SAPbouiCOM.ComboBoxColumn
                    Dim oValidValues As SAPbouiCOM.ValidValues

                    oCombo = moPricesGrid.Columns.Item(moPricesGrid.doIndexFieldWC("U_ChrgCal"))
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                    oValidValues = oCombo.ValidValues

                    doClearValidValues(oValidValues)
                    oValidValues.Add("VARIABLE", "VARIABLE")
                    oValidValues.Add("FIXED", "FIXED")
                    oValidValues.Add("OFFSET", "OFFSET")
                    oValidValues.Add("WEIGHT", "Use Weight")
                    oValidValues.Add("QTY1", "Use Quantity 1")
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Calculation type Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Calculation type")})
            End Try
        End Sub

        Private Sub doCalcTypeHaulCombo(ByVal bDoFormCombo As Boolean)
            Try
                If bDoFormCombo Then
                    ''FORM COMBO

                Else
                    ''GRID COMBO
                    Dim oCombo As SAPbouiCOM.ComboBoxColumn
                    Dim oValidValues As SAPbouiCOM.ValidValues

                    oCombo = moPricesGrid.Columns.Item(moPricesGrid.doIndexFieldWC("U_HaulCal"))
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                    oValidValues = oCombo.ValidValues

                    doClearValidValues(oValidValues)
                    oValidValues.Add("STANDARD", "STANDARD")
                    oValidValues.Add("WEIGHT", "Use Weight")
                    oValidValues.Add("QTY1", "Use Quantity 1")
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Haulage Calculation type Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Haulage Calculation type")})
            End Try
        End Sub

        Private Sub doWR1OrdTypeCombo(ByVal bDoFormCombo As Boolean)
            Try
                If bDoFormCombo Then
                    ''FORM COMBO

                    Dim oFormCombo As SAPbouiCOM.ComboBox
                    Dim oFormValidValues As SAPbouiCOM.ValidValues

                    oFormCombo = Items.Item("IDH_WR1ORD").Specific
                    oFormValidValues = oFormCombo.ValidValues
                    doClearValidValues(oFormValidValues)

                    oFormValidValues.Add("", Translation.getTranslatedWord("Any"))
                    oFormValidValues.Add("DO", Translation.getTranslatedWord("Disposal Order"))
                    oFormValidValues.Add("WO", Translation.getTranslatedWord("Waste Order"))
                Else
                    ''GRID COMBO

                    Dim oCombo As SAPbouiCOM.ComboBoxColumn
                    Dim oValidValues As SAPbouiCOM.ValidValues

                    oCombo = moPricesGrid.Columns.Item(moPricesGrid.doIndexFieldWC("U_WR1ORD"))
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                    oValidValues = oCombo.ValidValues
                    doClearValidValues(oValidValues)

                    oValidValues.Add("", Translation.getTranslatedWord("Any"))
                    oValidValues.Add("DO", Translation.getTranslatedWord("Disposal Order"))
                    oValidValues.Add("WO", Translation.getTranslatedWord("Waste Order"))
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Order Type Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Order Type")})
            End Try
        End Sub

        Private Sub doUOMCombo(ByVal bDoFormCombo As Boolean)
            Try
                If bDoFormCombo Then
                    ''FORM COMBO

                    doFillCombo("IDH_UOM", "OWGT", "UnitDisply", "UnitName", Nothing, Nothing, "Any", "")
                Else
                    ''GRID COMBO

                    Dim oCombo As SAPbouiCOM.ComboBoxColumn
                    oCombo = moPricesGrid.Columns.Item(moPricesGrid.doIndexFieldWC("U_UOM"))
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                    doFillCombo(oCombo, "OWGT", "UnitDisply", "UnitName", Nothing, Nothing, Nothing, "t") ''"Any", "")
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            End Try
        End Sub

        Private Sub doJobTypeCombo(ByVal bDoFormCombo As Boolean)
            Try
                If bDoFormCombo Then
                    ''FORM COMBO

                    doFillCombo("IDH_JOBTP", "[@IDH_JOBTYPE]", "U_JobTp", Nothing, Nothing, Nothing, "Any", "")
                Else
                    ''GRID COMBO

                    Dim oCombo As SAPbouiCOM.ComboBoxColumn
                    oCombo = moPricesGrid.Columns.Item(moPricesGrid.doIndexFieldWC("U_JobTp"))
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                    doFillCombo(oCombo, "[@IDH_JOBTYPE]", "U_JobTp", Nothing, Nothing, Nothing, "Any", "")
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Job Type Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Job Type")})
            End Try
        End Sub

        Public Sub doBranchCombo(ByVal bDoFormCombo As Boolean)
            Try

                If bDoFormCombo Then
                    ''FORM COMBO

                    doFillCombo("IDH_BRANCH", "OUBR", "Code", "Remarks", Nothing, Nothing, "Any", "")
                Else
                    ''FORM COMBO

                    Dim oCombo As SAPbouiCOM.ComboBoxColumn
                    oCombo = moPricesGrid.Columns.Item(moPricesGrid.doIndexFieldWC("U_Branch"))
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                    doFillCombo(oCombo, "OUBR", "Code", "Remarks", Nothing, Nothing, "Any", "")
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Branch Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Branch")})
            End Try
        End Sub

        Public Sub doAddAddCombo(ByVal bDoFormCombo As Boolean)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim oValidValues As SAPbouiCOM.ValidValues
            Try
                If bDoFormCombo Then
                    ''FORM COMBO

                Else
                    ''FORM COMBO

                    oCombo = moPricesGrid.Columns.Item(moPricesGrid.doIndexFieldWC("U_AAddExp"))
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                    oValidValues = oCombo.ValidValues

                    doClearValidValues(oValidValues)
                    oValidValues.Add(" ", "IGNORE")
                    oValidValues.Add("A", "Add")
                    oValidValues.Add("N", "Do Not Add")
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Auto Add Additional Item.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Auto Add Additional Item")})
            End Try
        End Sub

        '        Public Sub doPriceListCombo()
        '            Dim oCombo As SAPbouiCOM.ComboBoxColumn
        '            Try
        '            	oCombo = moPricesGrid.Columns.Item(moPricesGrid.doIndexFieldWC("U_ItmPLs"))
        '            	oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
        '
        '        		doFillCombo(oCombo, "OPLN", "ListNum", "ListName", Nothing, Nothing, Nothing, Nothing)
        '            Catch ex As Exception
        '               com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Branch Combo.")
        '            End Try
        '        End Sub
#End Region

        Private Sub doUpDateDefaults()
            'Dim sSwitch As String = getUFValue("IDH_SWFLT")

            Dim sCardCode As String = getUFValue("IDH_CARDCD")
            Dim sCustomerCd As String = getUFValue("IDH_BP2CD")
            Dim sItemCode As String = getUFValue("IDH_ITMCD")
            Dim sItemDesc As String = getUFValue("IDH_ITMNM")
            Dim sWastCd As String = getUFValue("IDH_WASTCD")
            Dim sWastDsc As String = getUFValue("IDH_WASTNM")
            Dim sAddCd As String = getUFValue("IDH_ADDCD")
            Dim sAddDsc As String = getUFValue("IDH_ADDNM")
            Dim sAddress As String = getUFValue("IDH_ADDR")
            Dim sAddressLineNum As String = getUFValue("IDH_ADLN")
            Dim sSupAddress As String = getUFValue("IDH_SPADDR")
            Dim sSupZipCode As String = getUFValue("IDH_SZIPCD")
            Dim sSupAddressLineNum As String = getUFValue("IDH_SPADLN")

            Dim sBranch As String = getUFValue("IDH_BRANCH")
            Dim sZipCode As String = getUFValue("IDH_POSCOD")
            Dim sUOM As String = getUFValue("IDH_UOM")
            Dim sOrderType As String = getUFValue("IDH_WR1ORD")
            Dim sJob As String = getUFValue("IDH_JOBTP")
            'Customer
            If (sZipCode Is Nothing OrElse sZipCode.Trim = "") AndAlso sAddress IsNot Nothing AndAlso sAddress.Trim <> "" Then
                sZipCode = com.idh.utils.Conversions.ToString(com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCustomerCd, Address, "ZipCode"))
            End If
            If sZipCode <> ZipCode Then
                ZipCode = sZipCode
            End If

            'Supplier
            If (sSupZipCode Is Nothing OrElse sSupZipCode.Trim = "") AndAlso sSupAddress IsNot Nothing AndAlso sSupAddress.Trim <> "" Then
                sSupZipCode = com.idh.utils.Conversions.ToString(com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCardCode, sSupAddress, "ZipCode"))
            End If
            If sSupZipCode <> SupplierZipCode Then
                SupplierZipCode = sSupZipCode
            End If
            'Dim sItemGrp As String = "" 'getUFValue("IDH_ITMCD")
            'Dim dWeight As Double = getUFValue("IDH_WEIGHT")
            'Dim sDocDate As String = getUFValue("IDH_DOCDAT")
            'Dim dDocDate As DateTime = DateTime.Now()

            'moDBObject.doAddDefaultValue(IDH_SUITPR._CardCd, sCardCode)
            If sCardCode.Equals("FALLBACK") Then
                moDBObject.doAddDefaultValue(IDH_SUITPR._CardCd, "")
            Else
                moDBObject.doAddDefaultValue(IDH_SUITPR._CardCd, sCardCode)
            End If
            moDBObject.doAddDefaultValue(IDH_SUITPR._BP2CD, sCustomerCd)
            moDBObject.doAddDefaultValue(IDH_SUITPR._ItemCd, sItemCode)
            moDBObject.doAddDefaultValue(IDH_SUITPR._ItemDs, sItemDesc)
            moDBObject.doAddDefaultValue(IDH_SUITPR._WasteCd, sWastCd)
            moDBObject.doAddDefaultValue(IDH_SUITPR._WasteDs, sWastDsc)
            moDBObject.doAddDefaultValue(IDH_SUITPR._AItmCd, sAddCd)
            moDBObject.doAddDefaultValue(IDH_SUITPR._AItmDs, sAddDsc)
            moDBObject.doAddDefaultValue(IDH_SUITPR._StAddr, sAddress)
            moDBObject.doAddDefaultValue(IDH_SUITPR._StAdLN, sAddressLineNum)
            moDBObject.doAddDefaultValue(IDH_SUITPR._SupAddr, sSupAddress)
            moDBObject.doAddDefaultValue(IDH_SUITPR._SupAdLN, sSupAddressLineNum)
            moDBObject.doAddDefaultValue(IDH_SUITPR._SupZipCd, sSupZipCode)
            moDBObject.doAddDefaultValue(IDH_SUITPR._Branch, sBranch)
            moDBObject.doAddDefaultValue(IDH_SUITPR._ZpCd, sZipCode)

            If sUOM Is Nothing OrElse sUOM.Length = 0 Then
                sUOM = moDBObject.doGetDefaultValue(IDH_CSITPR._UOM)
                If sUOM Is Nothing OrElse sUOM.Length = 0 Then
                    sUOM = com.idh.bridge.lookups.Config.INSTANCE.doGetItemSalesUOM(sCardCode, sWastCd)
                    moDBObject.doAddDefaultValue(IDH_CSITPR._UOM, sUOM)
                End If
            Else
                moDBObject.doAddDefaultValue(IDH_CSITPR._UOM, sUOM)
            End If

            moDBObject.doAddDefaultValue(IDH_SUITPR._WR1ORD, sOrderType)
            moDBObject.doAddDefaultValue(IDH_SUITPR._JobTp, sJob)

        End Sub

        Private Function doCreateKey(ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.BeforeAction = False Then
                Dim oNumbers As NumbersPair = moDBObject.getNewKey()
                moDBObject.Code = oNumbers.CodeCode
                moDBObject.Name = oNumbers.NameCode
            End If
            Return True
        End Function

        Private Sub UnMarkEditLines()
            Dim iEmptyRow As Integer = moDBObject.LastEmptyRowIndex
            Dim iRow As Integer

            Try
                If iEmptyRow >= 0 AndAlso moMarkedEditLines.Count > 0 Then
                    For iIndex As Integer = 0 To moMarkedEditLines.Count - 1
                        iRow = moMarkedEditLines.Item(iIndex)
                        If iRow >= 0 AndAlso
                            iRow < iEmptyRow Then
                            moPricesGrid.doSetRowColor(iRow + 1, miNormalColor)
                            moMarkedEditLines.RemoveAt(iIndex)
                        End If
                    Next
                End If
            Catch ex As Exception

            End Try
        End Sub

        Private Function doSetLastLine(ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.BeforeAction = False Then
                Dim iEmptyRow As Integer = moDBObject.LastEmptyRowIndex
                UnMarkEditLines()

                If iEmptyRow >= 0 Then
                    Dim sKey As String = moDBObject.Code
                    sKey = com.idh.utils.Conversions.ToString(moDBObject.getValue(iEmptyRow, moDBObject.RowKeyField))
                    If sKey Is Nothing OrElse sKey.Length = 0 Then
                        If moMarkedEditLines.Contains(iEmptyRow) = False Then
                            moPricesGrid.doSetRowColor(iEmptyRow + 1, miEditLineColor)
                            moMarkedEditLines.Add(iEmptyRow)
                        End If
                    End If
                    moDBObject.doApplyDefaults(iEmptyRow)
                End If
            End If
            Return True
        End Function
        'Private Function doValidateUOMFromSearch(ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
        '    If (pVal.BeforeAction - False) Then
        '        If (pVal.ColUID = IDH_SUITPR._UOM AndAlso moPricesGrid.doGetFieldValue(IDH_SUITPR._UOM, pVal.Row).ToString().Trim = String.Empty) Then
        '            Dim sDefaultUOM As String = com.idh.bridge.lookups.Config.INSTANCE.getDefaultUOM()
        '            If sDefaultUOM <> "" Then
        '                moPricesGrid.doSetFieldValue(IDH_SUITPR._UOM, pVal.Row, sDefaultUOM)
        '            End If
        '        End If
        '    End If
        'End Function
        Private Function doSort(ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.BeforeAction = True Then
                If SupplierCode.Equals("FALLBACK") Then
                    SupplierCode = ""
                End If
            Else
                If String.IsNullOrWhiteSpace(SupplierCode) Then
                    SupplierCode = "FALLBACK"
                End If
            End If
            Return True
        End Function

        Private Function doHandleGridDelete(ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.BeforeAction = False Then
                moDBObject.doAddAuditTrail("REMOVE", String.Concat(IDH_CSITPR._Code, ": ", moDBObject.Code), "DBO-[N C]")
            End If
            Return True
        End Function

        Public Sub doLoadNow(ByVal iMode As Integer)
            'doUpDateDefaults()

            mbDelayedLoad = False
            setUFValue("IDH_SWFLT", iMode)
            PaneLevel = iMode - 1

            mbDelayedLoad = False
            doLoadData()
        End Sub

        '** The Initializer
        Public Overrides Sub doLoadData()
            Dim iMode As Integer = getUFValue("IDH_SWFLT")
            If PaneLevel <> (iMode - 1) Then
                PaneLevel = iMode - 1
            End If

            doUpDateDefaults()

            If mbDelayedLoad = False Then
                doLoadGridData()
                doFillCombos(False)
            End If
        End Sub

        Public Sub doLoadGridData()
            SBOForm.Freeze(True)

            Try
                'IDHAddOns.idh.addon.Base.doDebug("Prepare Query", 1)
                DataHandler.INSTANCE.DebugTickGRID = "Prepare Query"

                moPricesGrid.doAutoListFields(True)

                Dim sSwitch As String = getUFValue("IDH_SWFLT")
                Dim oPrice As com.idh.dbObjects.User.Prices = Nothing
                If getUFValue("IDH_ACTPRC") = "Y" Then
                    moPricesGrid.setRequiredFilter("DATEDIFF(DD, U_StDate, CAST((CONVERT(NVARCHAR, GETDATE(), 12) ) AS DATETIME)) >= 0 And " _
                     & "DATEDIFF(DD, U_EnDate, CAST((CONVERT(NVARCHAR, GETDATE(), 12) ) AS DATETIME)) <= 0")
                Else
                    moPricesGrid.setRequiredFilter("")
                End If
                If sSwitch.Equals("1") Then
                    moPricesGrid.setOrderValue(IDH_SUITPR._CardCd)
                    moPricesGrid.setDoFilter(True)

                    If SupplierCode.Equals("FALLBACK") Then
                        SupplierCode = ""
                        moPricesGrid.doReloadData()
                        SupplierCode = "FALLBACK"
                    Else
                        moPricesGrid.doReloadData()
                    End If
                ElseIf sSwitch.Equals("3") Then
                    moPricesGrid.setOrderValue(IDH_SUITPR._CardCd)
                    moPricesGrid.setDoFilter(False)
                    'moPricesGrid.doReloadDataNoFilter()
                    moPricesGrid.doReloadData()
                Else
                    moPricesGrid.setDoFilter(True)

                    Dim sOrderType As String = getUFValue("IDH_WR1ORD")
                    Dim sJob As String = getUFValue("IDH_JOBTP")
                    Dim sItemGrp As String = ItemGroup 'getUFValue("IDH_ITMGRP")
                    Dim sItemCode As String = getUFValue("IDH_ITMCD")
                    Dim sCardCode As String = getUFValue("IDH_CARDCD")
                    Dim sCustCode As String = getUFValue("IDH_BP2CD")
                    Dim dWeight As Double = getUFValue("IDH_WEIGHT")
                    Dim sUOM As String = getUFValue("IDH_UOM")
                    Dim sWastCd As String = getUFValue("IDH_WASTCD")
                    Dim sAddress As String = getUFValue("IDH_ADDR")

                    Dim sDocDate As String = getUFValue("IDH_DOCDAT")
                    Dim dDocDate As DateTime
                    If sDocDate.Length > 0 Then
                        dDocDate = com.idh.utils.Dates.doStrToDate(sDocDate)
                    Else
                        dDocDate = DateTime.Now()
                    End If

                    Dim sBranch As String = getUFValue("IDH_BRANCH")
                    Dim sZipCode As String = getUFValue("IDH_POSCOD")

                    If sCardCode.Equals("FALLBACK") Then
                        sCardCode = ""
                    End If

                    oPrice = moDBObject.doGetJobCostPrice(sOrderType, sJob, sItemGrp, sItemCode, _
                                                      sCardCode, dWeight, sUOM, sWastCd, sCustCode, sAddress, _
                                                      dDocDate, sBranch, sZipCode)

                    moPricesGrid.doPostReloadData(True)

                    Dim sInfo As String = moDBObject.Info
                    If Not oPrice Is Nothing Then
                        If sInfo.Length > 0 Then
                            sInfo = sInfo & Chr(13)
                        End If

                        If (moDBObject.TipPriceFound) Then
                            sInfo = sInfo & Chr(13) & " Tip Price is not set."
                        End If
                        If (moDBObject.HaulPriceFound) Then
                            sInfo = sInfo & Chr(13) & " Haulage Price is not set."
                        End If

                        sInfo = sInfo & Chr(13) & " Tip Price: " & oPrice.TipPrice
                        sInfo = sInfo & Chr(13) & " Haulage Price: " & oPrice.HaulPrice
                        sInfo = sInfo & Chr(13) & " Tip Calc Type: " & oPrice.TipChargeCalc
                        sInfo = sInfo & Chr(13) & " Haulage Calc Type: " & oPrice.HaulageChargeCalc
                        sInfo = sInfo & Chr(13) & " Offset Weight: " & oPrice.TipOffsetWeight
                        sInfo = sInfo & Chr(13) & " Band: " & oPrice.Band
                        sInfo = sInfo & Chr(13) & " UOM: " & oPrice.UOM
                        sInfo = sInfo & Chr(13) & " Tip Vat: " & oPrice.TipVat & "%"
                        sInfo = sInfo & Chr(13) & " Tip Vat Group: " & oPrice.HaulageVatGroup
                        sInfo = sInfo & Chr(13) & " Haul Vat: " & oPrice.HaulageVat & "%"
                        sInfo = sInfo & Chr(13) & " Haul Vat Group: " & oPrice.HaulageVatGroup

                        sInfo = sInfo & Chr(13) & Chr(13)
                        sInfo = sInfo & Chr(13) & "["
                        sInfo = sInfo & Chr(13) & moDBObject.LastQuery
                        sInfo = sInfo & Chr(13) & "]"

                        If oPrice.Found = True Then
                            moPricesGrid.doSetRowColor(1, miSelectedLineColor)
                        End If
                    End If
                    setUFValue("IDH_INFO", sInfo)
                End If

                'Block Currency Column for Customers that are not Multi-Ccy
                If SupplierCode <> "FALLBACK" Then
                    Try
                        Dim sSuppCcy As String = com.idh.bridge.lookups.Config.INSTANCE.doGetBPCurrency(SupplierCode)
                        If Not sSuppCcy.Equals("##") Then
                            moPricesGrid.getSBOGrid.Columns.Item(moPricesGrid.doIndexFieldWC(IDH_SUITPR._Currency)).Editable = False
                        End If
                    Catch ex As Exception
                    End Try
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error reloading the Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", {Nothing})
            End Try
            SBOForm.Freeze(False)
        End Sub
        Public Overrides Function doCustomItemEvent(ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            'com.idh.bridge.DataHandler.INSTANCE.
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SORT AndAlso pVal.BeforeAction = False Then
                'IDHForm.goParent.goApplication.StatusBar.SetText(pVal.EventType.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                doFillCombos(False)
            End If
            Return MyBase.doCustomItemEvent(pVal)
        End Function
    End Class
End Namespace
