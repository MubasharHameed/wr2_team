Imports System.IO
Imports System.Collections
Imports System

'Needed for the Framework controls
Imports IDHAddOns.idh.controls
Imports IDHAddOns
Imports com.idh.bridge
Imports com.idh.bridge.data
Imports com.idh.dbObjects.User

Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.numbers

Namespace idh.forms.fr2
    Public Class Training02
    	'This is the Base Form object from the Framework
        Inherits com.idh.forms.oo.Form
        
        Private moIDH_Training AS IDH_TRAIN01
		
		Private moGridN As UpdateGrid
		
		Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal sParentId As String, ByRef oSBOForm As SAPbouiCOM.Form)
			MyBase.New(oIDHForm, sParentId, oSBOForm)
			
            addHandler_ITEM_PRESSED("IDHBPCFL", New ev_Item_Event(AddressOf doCustomerPressed))
			
            Handler_ITEM_PRESSED = New ev_Item_Event(AddressOf doHandleItemPressedEvents)
            Handler_Button_Ok = New ev_Item_Event(AddressOf doDoOkButtonEvent)
			
            moIDH_Training = New IDH_TRAIN01(oIDHForm, oSBOForm)
		End Sub
		
		'** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Function doHandleItemPressedEvents(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
				If pVal.ItemUID.Equals("IDHBPCFL") Then
					doChooseCustomer()
				end If
            End If
            Return False
        End Function
        
        Public Function doCustomerPressed(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
				Dim sCardCode as String = moIDH_Training.U_CardCode 'getFormDFValue("U_CardCode")
	            setSharedData("IDH_BPCOD", sCardCode)
				setSharedData("IDH_WOH", "Louis")
	            
            	Dim oOOForm As idh.forms.fr2.Training03 = New idh.forms.fr2.Training03(Nothing, FormId, Nothing)
                oOOForm.Handler_DialogOkReturn = New DialogReturn(AddressOf doHandleCustomerSearchResult)
                oOOForm.Handler_DialogButton1Return = New DialogReturn(AddressOf doHandleOtherCustomerSearchResult)
            	oOOForm.doShowModal(Me)

            End If
            Return False
        End Function
		
        Public Overloads Shared Function doRegisterFormClass() As IDHAddOns.idh.forms.Base
            Dim owForm As com.idh.forms.oo.FormController = New com.idh.forms.oo.FormController( _
              "IDH_Train02", _
              "IDHORD", _
              101, _
              "training01_1.srf", _
              False, _
              True, _
              False, _
              "Training Form VB", _
              IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL)

            ''Add the item to the list of items to get their filepaths fixed.
            'owForm.doAddImagesToFix("IDHBPCFL")

            'Sets the History table and enables the ChangeLog Menu option
            'This will also Add the DBDataSource when the form gets loaded
            owForm.doEnableHistory("@IDH_TRAIN01")

            Return owForm
        End Function

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef BubbleEvent As Boolean)
        	'OLD - WAY - this will automatically be done when the Form History or Main table was set in the contructor
        	'oForm.DataSources.DBDataSources.Add("@IDH_TRAIN01")
        	
        	'Dim oItem As SAPbouiCOM.Item
        	'Dim oEdit As SAPbouiCOM.EditText
        	'oItem = oForm.Items.Item("IDHCardCod")
        	'oEdit = oItem.Specific
        	'oEdit.DataBind.SetBound(True, "@IDH_TRAIN01", "U_CardCode")

			'Using a table other than the Form's main table        	
        	'doAddDF(oForm, "@IDH_TRAIN01", "IDHCode", "Code")
        	'doAddDF(oForm, "@IDH_TRAIN01", "IDHCardCod", "U_CardCode")
        	'doAddDF(oForm, "@IDH_TRAIN01", "IDHCardNam", "U_CardName")
        	'doAddDF(oForm, "@IDH_TRAIN01", "IDHInv", "U_InvID")
	       	'doAddDF(oForm, "@IDH_TRAIN01", "IDHInvDate", "U_InvDate")
        	'doAddDF(oForm, "@IDH_TRAIN01", "IDHTotal", "U_DocTotal")

			'Using the Form's main table
        	'doAddDF("IDHCode", "Code")
        	'doAddDF("IDHCardCod", "U_CardCode")
        	''doAddDF("IDHCardNam", "U_CardName")
        	'doAddDF("IDHInv", "U_InvID")
        	'doAddDF("IDHInvDate", "U_InvDate")
        	'doAddDF("IDHTotal", "U_DocTotal")
        	
        	moIDH_Training.doLink_Code("IDHCode")
        	moIDH_Training.doLink_CardCode("IDHCardCod")
        	moIDH_Training.doLink_InvID("IDHInv")
        	moIDH_Training.doLink_InvDate("IDHInvDate")
        	moIDH_Training.doLink_DocTotal("IDHTotal")

			'oForm.DataSources.UserDataSources.Add("DS01", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30)
	      	'Dim oEdit As SAPbouiCOM.EditText
        	'oItem = oForm.Items.Item("IDHCardCod")
        	'oEdit = oItem.Specific
			'oEdit.DataBind.SetBound(True, "", "DS01")
			
        	'doAddUF(oForm, "IDHCardCod", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
        	
        	'doAddUFCheck(oForm, "IDH_ASNEW", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
        	
        	'Dim oRadio As SAPbouiCOM.OptionBtn
            'oRadio = oForm.Items.Item("IDH_APPROV").Specific
            'oRadio.DataBind.SetBound(True, "", "U_APPR")

            'oRadio = oForm.Items.Item("IDH_DECLIN").Specific
            'oRadio.DataBind.SetBound(True, "", "U_DECL")
            'oRadio.GroupWith("IDH_APPROV")

			moGridN = UpdateGrid.getInstance(SBOForm, "LINESGRID")
			If moGridN Is Nothing Then
				moGridN = New UpdateGrid(IDHForm, SBOForm, "LINESGRID")
			End If
			moGridN.getSBOItem().AffectsFormMode = True

			SBOForm.DataBrowser.BrowseBy = "IDHCode"
			SBOForm.SupportedModes = -1
            SBOForm.AutoManaged = True

            SBOForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
        End Sub

		Public Overrides Sub doBeforeLoadData()
			If moGridN Is Nothing Then
				moGridN = UpdateGrid.getInstance(SBOForm, "LINESGRID")
				If moGridN Is Nothing Then
					moGridN = New UpdateGrid(IDHForm, SBOForm, "LINESGRID")
				End If				
			End If
			doSetFilterFields()
			doSetListFields()
			
			moGridN.doAddGridTable( new GridTable( "@IDH_TRAIN01_LNS", Nothing, "Code", True ), True)
			moGridN.doSetDoCount(True)
			moGridN.doSetHistory("@IDH_TRAIN01_LNS", "Code")
		End Sub
		
		Protected Overridable Sub doSetFilterFields()
			'oGridN.doAddFilterField("IDHCode", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30 )
			
			'** Can Optionally set a default value **/
			'oGridN.doAddFilterField("IDHCode", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "XYZ")
		End Sub

		Protected Overridable Sub doSetListFields()
			moGridN.doAddListField("Code", "Code", true, 20, Nothing, Nothing)
			moGridN.doAddListField("U_HDRCode", "HDRCode", true, 20, Nothing, Nothing)
			moGridN.doAddListField("U_ItemCode", "Item Code", true, 50, Nothing, Nothing)
			moGridN.doAddListField("U_ItemDesc", "Description", true, 150, Nothing, Nothing)
			moGridN.doAddListField("U_QTY", "QTY", true, 20, Nothing, Nothing)
			moGridN.doAddListField("U_Price", "Price", true, 20, Nothing, Nothing)
			moGridN.doAddListField("U_Total", "Total", true, 20, Nothing, Nothing)
		End Sub
		
        '** The Initializer
        Public Overrides Sub doLoadData()
       	    Dim sCode As String = getFormDFValue("Code")
       	    moGridN.setRequiredFilter("U_HDRCode = '" & sCode & "'")
        	moGridN.doReloadData()
        	
        	Dim sName As String = getFormDFValue("U_CardName")
        End Sub

        Public Overrides Sub doCloseForm(ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(BubbleEvent)
        End Sub

'        Public Overrides Sub doClose()
'        End Sub
'
'        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, _
'        		ByVal pVal As SAPbouiCOM.ItemEvent, _
'        		ByRef BubbleEvent As Boolean )
'            If pVal.BeforeAction = True Then   
'            	If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
'                   oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
'					If doUpdateHeader(oForm) Then
'						dim oGrid as Updategrid = Updategrid.getInstance(oForm, "LINESGRID")
'	        			If oGrid.doProcessData() Then
'	        			End If  
'	        			
'	       				oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
'	                    BubbleEvent = False
'					End If
'				End If
'            End If
'        End Sub

        Public Function doDoOkButtonEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
 			If pVal.BeforeAction = True Then   
'					If doUpdateHeader(oForm) Then
'						dim oGrid as Updategrid = Updategrid.getInstance(oForm, "LINESGRID")
'	        			If oGrid.doProcessData() Then
'	        			End If  
'	        			
'	       				oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
'	                    BubbleEvent = False
'					End If
            End If
            Return True
        End Function


'        '** The ItemEvent handler
'        '** Return True if the Event must be handled by the other Objects 
'        Public Overrides Function doItemEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
'            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
'                If pVal.BeforeAction = False Then
'					If pVal.ItemUID.Equals("IDHBPCFL") Then
'						doChooseCustomer()
'					end If
'                End If
'            End If
'            Return False
'        End Function

		'OPENING MODAL MOVING DATA UP
		Protected Overridable Sub doChooseCustomer()
			'Using a table other than the Form's main table        	
			'dim sCardCode as String = getDFValue(oForm, "@IDH_TRAIN01", "U_CardCode")
			
			'Using the Form's main table
			'Dim sCardCode as String = getFormDFValue("U_CardCode")
            setSharedData("IDH_BPCOD", moIDH_Training.U_CardCode)
            'goParent.doOpenModalForm("IDHCSRCH", oForm)
'            goParent.doOpenModalForm("134", oForm)
'            goParent.doOpenModalForm("150", oForm)
			setSharedData("IDH_WOH", "Louis")
            'doOpenModalForm("651")
'            doOpenModalForm("IDH_Train03")
            
'            Dim oOOForm As idh.forms.fr2.Training03 = New idh.forms.fr2.Training03(Nothing, FormId, Nothing)
'            oOOForm.Handler_DialogOkReturn = New DialogOkReturn( AddressOf doHandleCustomerSearchResult ) 
'            oOOForm.doShowModal(Me)
        End Sub

		Public Function doHandleCustomerSearchResult( ByVal oOForm As com.idh.forms.oo.Form ) As Boolean
			Dim sCardCode, sCardName As String
			sCardCode = DirectCast(oOForm, Training03).sVal1 ' getSharedData("CARDCODE")
			sCardName = DirectCast(oOForm, Training03).sVal2 ' getSharedData("CARDNAME")
			
			'Using a table other than the Form's main table        	
			'setDFValue(oForm,"@IDH_TRAIN01","U_CardCode",sCardCode,True)
			'setDFValue(oForm,"@IDH_TRAIN01","U_CardName",sCardName,True)
			
			'Using the form's main table
			moIDH_Training.U_CardCode = sCardCode
			moIDH_Training.U_CardName = sCardName
			
			'setFormDFValue( "U_CardCode", sCardCode, True )
			'setFormDFValue( "U_CardName", sCardName, True )
			
			Return True
		End Function

		Public Function doHandleOtherCustomerSearchResult( ByVal oOForm As com.idh.forms.oo.Form ) As Boolean
			Dim sCardCode, sCardName As String
			sCardCode = DirectCast(oOForm, Training03).sVal1 ' getSharedData("CARDCODE")
			sCardName = DirectCast(oOForm, Training03).sVal2 ' getSharedData("CARDNAME")
			
			'Using the form's main table
			'setFormDFValue( "U_CardCode", sCardCode )
			'setFormDFValue( "U_CardName", sCardName )
			
			moIDH_Training.U_CardCode = sCardCode
			moIDH_Training.U_CardName = sCardName
			Return True
		End Function


		'CLOSING MODAL RECEIVING DATA
		Public Overrides Sub doHandleModalResultShared(ByVal sModalFormType As String, ByVal sLastButton As String )
			Try
				If sModalFormType.Equals("IDHCSRCH") Then
					Dim sCardCode, sCardName As String
					sCardCode = getSharedData("CARDCODE")
					sCardName = getSharedData("CARDNAME")
					
					'Using a table other than the Form's main table        	
					'setDFValue(oForm,"@IDH_TRAIN01","U_CardCode",sCardCode,True)
					'setDFValue(oForm,"@IDH_TRAIN01","U_CardName",sCardName,True)
					
					'Using the form's main table
					'setFormDFValue( "U_CardCode", sCardCode )
					'setFormDFValue( "U_CardName", sCardName )
					
					moIDH_Training.U_CardCode = sCardCode
					moIDH_Training.U_CardName = sCardName
				End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal result - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

'        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
'        	'This will now Automatically update all the Form's Main table fields and 
'        	'generate and set the Code value if the code was not set
'        	Return doAutoUpdate(oForm, "TRN1KEY") 
'        End Function
'
''        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
''            Dim iwResult As Integer = 0
''            Dim swResult As String = Nothing
''
''            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@" & getHistoryTable())
''            Try
''            	dim sCode as String = getDFValue(oform,"@IDH_TRAIN01","Code")
''				If sCode.Length > 0 Then
''					oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE)
''				Else
''					sCode = goParent.goDB.doNextNumber("TRN1KEY")
''					oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
''					oAuditObj.setName(sCode)
''				End If
''			
''				oAuditObj.setFieldValue("U_CardCode", getDFValue(oform,"@IDH_TRAIN01","U_CardCode"))
''				oAuditObj.setFieldValue("U_CardName", getDFValue(oform,"@IDH_TRAIN01","U_CardName"))
''        		oAuditObj.setFieldValue("U_InvID", getDFValue(oForm, "@IDH_TRAIN01", "U_InvID"))
''        		oAuditObj.setFieldValue("U_InvDate", getDFValue(oForm, "@IDH_TRAIN01", "U_InvDate"))
''        		oAuditObj.setFieldValue("U_DocTotal", getDFValue(oForm, "@IDH_TRAIN01", "U_DocTotal"))
''				
''				iwResult = oAuditObj.Commit()
''				
''				If iwResult <> 0 Then
''					goParent.goDICompany.GetLastError(iwResult, swResult)
''					com.idh.bridge.DataHandler.INSTANCE.doError("doUpdateHeader", "Error Adding: " + swResult, "")
''					Return False
''				End If
''				
''				Return True
''            Catch ex As Exception
''               com.idh.bridge.DataHandler.INSTANCE.doError("doUpdateHeader", "Exception: " & ex.ToString, "")
''                Return False
''            Finally
''                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
''            End Try
''        End Function
        
        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doMenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.MenuUID = Config.NAV_ADD Then
                    Freeze(True)
                    Try
                        doClearAll(True)
                        doLoadData()
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & FormId & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {FormId, pVal.MenuUID})
                    Finally
                        Freeze(False)
                    End Try
                ElseIf pVal.MenuUID = Config.NAV_FIND OrElse _
                       pVal.MenuUID = Config.NAV_FIRST OrElse _
                       pVal.MenuUID = Config.NAV_LAST OrElse _
                       pVal.MenuUID = Config.NAV_NEXT OrElse _
                       pVal.MenuUID = Config.NAV_PREV Then
                    Freeze(True)
                    Try
                        If pVal.MenuUID = Config.NAV_FIND Then
                            doClearAll(False)
                            doLoadData()
                        Else
                            doLoadData()
                        End If
                    Catch ex As Exception
                    Finally
                        Freeze(False)
                    End Try
                End If
            End If
            Return False
        End Function
        
'        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByVal pVal As IDHAddOns.idh.events.Base) As Boolean
'            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
'                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
'                Dim sKey As String = com.idh.utils.Conversions.ToString(goParent.goDB.doNextNumber("TRN1KEYL"))
'                oGridN.doSetFieldValue("Code", pVal.Row, sKey, True)
'                oGridN.doSetFieldValue("Name", pVal.Row, sKey, True)
'                
'                'Using a table other than the Form's main table        	
'                'Dim sCode As String = getDFValue(oForm, "@IDH_TRAIN01", "Code" )
'                
'                'Using the Form's Main Table
'                Dim sCode As String = getFormDFValue(oForm, "Code" )
'                oGridN.doSetFieldValue("U_HDRCode", pVal.Row, sCode, True) 
'            End If
'            Return MyBase.doCustomItemEvent(oForm, pVal)
'        End Function        
     
     	Private Sub doClearAll(ByVal bDoNewCode As Boolean )
     		'Using a table other than the Form's main table        	
        	'setDFValue(oForm, "@IDH_TRAIN01", "U_CardCode", "")
        	'setDFValue(oForm, "@IDH_TRAIN01", "U_CardName", "")
        	'setDFValue(oForm, "@IDH_TRAIN01", "U_InvID", "")
        	'setDFValue(oForm, "@IDH_TRAIN01", "U_InvDate", "")
        	'setDFValue(oForm, "@IDH_TRAIN01", "U_DocTotal", "")
        	
        	'Using the Form's main table
        	setFormDFValue("U_CardCode", "")
        	setFormDFValue("U_CardName", "")
        	setFormDFValue("U_InvID", "")
        	setFormDFValue("U_InvDate", "")
        	setFormDFValue("U_DocTotal", "")
        	
            If bDoNewCode = True Then
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_TRAIN01.AUTONUMPREFIX, "TRN1KEY")
                setFormDFValue("Code", oNumbers.CodeCode)
                setFormDFValue("Name", oNumbers.NameCode)
            Else
                'Using the Form's main table
                setFormDFValue("Code", "")
                setFormDFValue("Name", "")
            End If
        	
        	'Using a table other than the Form's main table        	
        	'setDFValue(oForm, "@IDH_TRAIN01", "Code", sCode)    		
        End Sub
        
    End Class
End Namespace
