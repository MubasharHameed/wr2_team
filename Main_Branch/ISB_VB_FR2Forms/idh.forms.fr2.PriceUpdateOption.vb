﻿Imports System.IO
Imports System.Collections
Imports System

'Needed for the Framework controls
Imports IDHAddOns.idh.controls
Imports IDHAddOns
Imports com.idh.bridge.data
Imports SAPbouiCOM
Imports IDHAddOns.idh.events
'Imports idh.const.Lookup

Namespace idh.forms.fr2
    Public Class PriceUpdateOption
        Inherits com.idh.forms.oo.Form

        Enum en_PriceTypes
            TUOM = 10
            PUOM = 15
            PRUOM = 20
            TChrg = 100
            HChrg = 200
            DispCst = 300
            HaulCst = 400
            ProCst = 500
        End Enum

        Public Shared Function getPriceTypeName(ByVal oPriceType As en_PriceTypes) As String
            Select Case oPriceType
                Case en_PriceTypes.TUOM
                    Return "Tipping UOM"
                Case en_PriceTypes.PUOM
                    Return "Purchase UOM"
                Case en_PriceTypes.PRUOM
                    Return "Producer UOM"
                Case en_PriceTypes.TChrg
                    Return "Tipping Charge"
                Case en_PriceTypes.HChrg
                    Return "Haulage Charge"
                Case en_PriceTypes.DispCst
                    Return "Disposal Cost"
                Case en_PriceTypes.HaulCst
                    Return "Haulage Cost"
                Case en_PriceTypes.ProCst
                    Return "Producer Cost"
                Case Else
                    Return "UnKNown"
            End Select
        End Function

        Public PriceType As en_PriceTypes 'TChrg, HChrg, DispCst, HaulCst, ProCst
        Public PriceTypes As ArrayList = New ArrayList() 'Contains Multiple Types
        Public Property FromDate() As String
            Get
                Return getUFValue("IDH_FRMDAT").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_FRMDAT", value)
            End Set
        End Property

        Public Property ToDate() As String
            Get
                Return getUFValue("IDH_TODAT").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_TODAT", value)
            End Set
        End Property

        Public Property AllSites() As String
            Get
                Return getUFValue("IDH_APPTS").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_APPTS", value)
            End Set
        End Property

        'TUOM
        Public Property IDH_TUOM() As Boolean
            Get
                Return getUFValue("IDH_10").ToString() = "Y"
            End Get
            Set(ByVal value As Boolean)
                setUFValue("IDH_10", If(value, "Y", "N"))
            End Set
        End Property

        'PUOM
        Public Property IDH_PUOM() As Boolean
            Get
                Return getUFValue("IDH_15").ToString() = "Y"
            End Get
            Set(ByVal value As Boolean)
                setUFValue("IDH_15", If(value, "Y", "N"))
            End Set
        End Property

        'PRUOM = 20
        Public Property IDH_PRUOM() As Boolean
            Get
                Return getUFValue("IDH_20").ToString() = "Y"
            End Get
            Set(ByVal value As Boolean)
                setUFValue("IDH_20", If(value, "Y", "N"))
            End Set
        End Property

        'TChrg = 100
        Public Property IDH_TChrg() As Boolean
            Get
                Return getUFValue("IDH_100").ToString() = "Y"
            End Get
            Set(ByVal value As Boolean)
                setUFValue("IDH_100", If(value, "Y", "N"))
            End Set
        End Property

        'HChrg = 200
        Public Property IDH_HChrg() As Boolean
            Get
                Return getUFValue("IDH_200").ToString() = "Y"
            End Get
            Set(ByVal value As Boolean)
                setUFValue("IDH_200", If(value, "Y", "N"))
            End Set
        End Property

        'DispCst = 300
        Public Property IDH_DispCst() As Boolean
            Get
                Return getUFValue("IDH_300").ToString() = "Y"
            End Get
            Set(ByVal value As Boolean)
                setUFValue("IDH_300", If(value, "Y", "N"))
            End Set
        End Property

        'HaulCst = 400
        Public Property IDH_HaulCst() As Boolean
            Get
                Return getUFValue("IDH_400").ToString() = "Y"
            End Get
            Set(ByVal value As Boolean)
                setUFValue("IDH_400", If(value, "Y", "N"))
            End Set
        End Property

        'ProCst = 500
        Public Property IDH_ProCst() As Boolean
            Get
                Return getUFValue("IDH_500").ToString() = "Y"
            End Get
            Set(ByVal value As Boolean)
                setUFValue("IDH_500", If(value, "Y", "N"))
            End Set
        End Property

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByRef oSBOParentForm As SAPbouiCOM.Form, ByRef oSBOForm As SAPbouiCOM.Form)
            MyBase.New(oIDHForm, oSBOParentForm, oSBOForm)
        End Sub

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal sParentId As String, ByRef oSBOForm As SAPbouiCOM.Form)
            MyBase.New(oIDHForm, sParentId, oSBOForm)
        End Sub

        Public Overloads Shared Function doRegisterFormClass() As IDHAddOns.idh.forms.Base
            Dim owForm As com.idh.forms.oo.FormController = New com.idh.forms.oo.FormController(
                "IDH_PRUPD",
                0,
                "PriceUpdateOption.srf",
                False)

            'Dim owForm As com.idh.forms.oo.FormController = New com.idh.forms.oo.FormController( _
            '  "IDH_PRUPD", _
            '  "IDHORD", _
            '  101, _
            '  "PriceUpdateOption.srf", _
            '  False, _
            '  True, _
            '  False, _
            '  "Price Update Option", _
            '  IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL)

            Return owForm
        End Function

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef BubbleEvent As Boolean)
            doAddUF("IDH_FRMDAT", SAPbouiCOM.BoDataType.dt_DATE, 10, False, False)
            doAddUF("IDH_TODAT", SAPbouiCOM.BoDataType.dt_DATE, 10, False, False)
            doAddUFCheck("IDH_APPTS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")

            SBOForm.AutoManaged = False
        End Sub

        Private Sub doLoadCheckBox(ByVal sId As String, ByVal iTop As Integer, ByVal sTypeName As String)
            doAddUFCheck(sId, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N", 0, 5, iTop, 270, 14, sTypeName, "1").Enabled = False
            setUFValue(sId, "N")
        End Sub

        Private Sub doActivateCheckBox(ByVal sId As String)
            Dim oCheckItem As SAPbouiCOM.Item = SBOForm.Items.Item(sId)
            oCheckItem.Enabled = True

            setUFValue(sId, "Y")
        End Sub

        Public Overrides Sub doBeforeLoadData()
            Dim sString As String = ""
            If PriceTypes.Count > 0 Then
                Dim sTypeName As String

                'Show the option checkboxes
                Dim iSizeChange As Integer = 120 '8 * 15
                Dim iTop As Integer = 55
                SBOForm.Height = SBOForm.Height + iSizeChange + 10
                SBOForm.Items.Item("1").Top = SBOForm.Items.Item("1").Top + 10 + iSizeChange
                SBOForm.Items.Item("2").Top = SBOForm.Items.Item("2").Top + 10 + iSizeChange

                doLoadCheckBox("IDH_10", 55, getPriceTypeName(en_PriceTypes.TUOM))
                doLoadCheckBox("IDH_15", 70, getPriceTypeName(en_PriceTypes.PUOM))
                doLoadCheckBox("IDH_20", 85, getPriceTypeName(en_PriceTypes.PRUOM))
                doLoadCheckBox("IDH_100", 100, getPriceTypeName(en_PriceTypes.TChrg))
                doLoadCheckBox("IDH_200", 115, getPriceTypeName(en_PriceTypes.HChrg))
                doLoadCheckBox("IDH_300", 130, getPriceTypeName(en_PriceTypes.DispCst))
                doLoadCheckBox("IDH_400", 145, getPriceTypeName(en_PriceTypes.HaulCst))
                doLoadCheckBox("IDH_500", 160, getPriceTypeName(en_PriceTypes.ProCst))

                For Each oType As en_PriceTypes In PriceTypes
                    sTypeName = getPriceTypeName(oType)
                    If sString.Length > 0 Then
                        sString = sString & "," & sTypeName
                    End If
                    'doAddUFCheck("IDH_" & oType, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N", 0, 5, iTop, 270, 14, sTypeName, "1")
                    'setUFValue("IDH_" & oType, "Y")
                    'iTop = iTop + 15

                    doActivateCheckBox("IDH_" & oType)
                    Select Case oType
                        Case en_PriceTypes.TChrg '100
                            doActivateCheckBox("IDH_10")
                        Case en_PriceTypes.ProCst '500
                            doActivateCheckBox("IDH_20")
                        Case en_PriceTypes.DispCst '300
                            doActivateCheckBox("IDH_15")
                    End Select

                Next
            Else
                sString = getPriceTypeName(PriceType)
            End If
            SBOForm.Title = sString & " Price Update / Create Option"
        End Sub

    End Class
End Namespace
