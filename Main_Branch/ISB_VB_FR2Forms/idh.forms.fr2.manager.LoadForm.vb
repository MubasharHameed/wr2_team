﻿'
' Created by SharpDevelop.
' User: Louis
' Date: 2011/10/31
' Time: 07:19 PM
'
' Only the IDHSBO team are authorized to use this new object to create new Forms.
'
Imports System.IO
Imports System.Collections
Imports System

Imports com.idh.dbObjects.numbers

Imports IDHAddOns.idh.controls
'Imports com.idh.bridge
Imports com.idh.bridge.data
Imports com.idh.dbObjects
Imports com.idh.bridge

Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User
Imports com.idh.utils

Namespace idh.forms.fr2.manager
	Public Class LoadForm
		
		'This is the Base Form object from the Framework
        Inherits com.idh.forms.oo.Form
	
		Private moAvailableGrid As FilterGrid
		Private moLockedGrid As FilterGrid
		
		'**
		' Register the form controller
		'**
        Public Overloads Shared Function doRegisterFormClass(ByVal sParMenu As String) As IDHAddOns.idh.forms.Base
            Dim owForm As com.idh.forms.oo.FormController = New com.idh.forms.oo.FormController( _
              "IDH_ManLoad", _
              sParMenu, _
              80, _
              "ManualLoadingForm.srf", _
              False, _
              True, _
              False, _
              "Manual Loading Form", _
              IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL)

            ''Add the item to the list of items to get their filepaths fixed.
            'owForm.doAddImagesToFix("IDHWTCFL")

            'The History and main Form Table
            owForm.doEnableHistory("@IDH_MANBALD")

            IDHAddOns.idh.addon.Base.PARENT.doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN, owForm.gsType)
            IDHAddOns.idh.addon.Base.PARENT.doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT, owForm.gsType)
            IDHAddOns.idh.addon.Base.PARENT.doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED, owForm.gsType)

            Return owForm
        End Function

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal sParentId As String, ByRef oSBOForm As SAPbouiCOM.Form)
            MyBase.New(oIDHForm, sParentId, oSBOForm)

            doSetHandlers()
        End Sub

        Private Sub doSetHandlers()
            addHandler_LOST_FOCUS_CHANGED("IDHWCICD", New ev_Item_Event(AddressOf doWasteCodeLostFocus))
            addHandler_ITEM_PRESSED("IDHWTCFL", New ev_Item_Event(AddressOf doWasteLookup))
            addHandler_ITEM_PRESSED("IDHPRNBTN", New ev_Item_Event(AddressOf doPrintEvent))

            Handler_Button_Ok = New ev_Item_Event(AddressOf doOkEvent)
            Handler_Button_Add = New ev_Item_Event(AddressOf doAddEvent_)
            Handler_Button_Update = New ev_Item_Event(AddressOf doUpdateEvent)
            Handler_Button_Find = New ev_Item_Event(AddressOf doFindButtonEvent)
            Handler_PRINT = New ev_Item_Event(AddressOf doPrintEvent)
            Handler_FORM_RESIZE = New ev_Item_Event(AddressOf doResizeEvent)

            Handler_Menu_NAV_ADD = New et_Menu_Event(AddressOf doMenuAddEvent)
            Handler_Menu_NAV_FIND = New et_Menu_Event(AddressOf doMenuFindEvent)

            Handler_Menu_NAV_FIRST = New et_Menu_Event(AddressOf doNavigationEvent)
            Handler_Menu_NAV_LAST = New et_Menu_Event(AddressOf doNavigationEvent)
            Handler_Menu_NAV_NEXT = New et_Menu_Event(AddressOf doNavigationEvent)
            Handler_Menu_NAV_PREV = New et_Menu_Event(AddressOf doNavigationEvent)
        End Sub
#Region "Menu Events"
        Public Function doMenuAddEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Freeze(True)
                Try
                    doClearAll(True)
                    doLoadData()
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & FormId & "." & pVal.MenuUID)
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {FormId, pVal.MenuUID})
                Finally
                    Freeze(False)
                End Try
            End If
            Return True
        End Function

        Public Function doMenuFindEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Freeze(True)
                Try
                    doClearAll(False)
                    doLoadData()
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & FormId & "." & pVal.MenuUID)
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {FormId, pVal.MenuUID})
                Finally
                    Freeze(False)
                End Try
            End If
            Return True
        End Function

        '**
        ' Navigation took place
        '
        Public Function doNavigationEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                doLoadData()
            End If
            Return True
        End Function
#End Region

#Region "Other Event"
        '**
        ' Handle the Ok Button Event
        '**
        Public Function doOkEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
            End If
            Return True
        End Function

        '**
        ' Handle the Add Event
        '**
        Class Batch
            Public BatchNumber As String
            Public Weight As Double
            Public AbsEntry As String
            Public Sub New(ByVal sAbsEntry As String, ByVal sBatchNumber As String, ByVal dWeight As Double)
                AbsEntry = sAbsEntry
                BatchNumber = sBatchNumber
                Weight = dWeight
            End Sub
        End Class
        Public Function doAddEvent_(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim bResult As Boolean = False
            If pVal.BeforeAction = False Then
                Dim sLoadNr As String = getFormDFValue("Code")
                Dim sItemCode As String = ""
                Dim sCheckValue As String
                Dim sAbsEntry As String
                Dim aoBatches As ArrayList = New ArrayList()
                ''Dim aoWeight As ArrayList = New ArrayList()
                If moAvailableGrid.getRowCount > 0 Then
                    sItemCode = CType(moAvailableGrid.doGetFieldValue("b.ItemCode", 0), String)
                    For iRow As Integer = 0 To moAvailableGrid.getRowCount() - 1
                        sCheckValue = Conversions.ToString(moAvailableGrid.doGetFieldValue(4, iRow))
                        If sCheckValue = "Y" Then
                            sAbsEntry = CType(moAvailableGrid.doGetFieldValue("e.AbsEntry", iRow), String)
                            Dim oBatch As Batch = New Batch(sAbsEntry, _
                                                            Conversions.ToString(moAvailableGrid.doGetFieldValue("b.BatchNum", iRow)), _
                                                            CType(moAvailableGrid.doGetFieldValue("b.Quantity", iRow), Double))
                            aoBatches.Add(oBatch)
                        End If
                    Next
                End If

                If aoBatches.Count > 0 Then
                    Dim oLoadHead As User.IDH_MANBALD
                    Dim oLoadRow As User.IDH_MANBALDB = Nothing

                    oLoadHead = New User.IDH_MANBALD()
                    oLoadHead.Code = sLoadNr
                    oLoadHead.Name = oLoadHead.Code
                    oLoadHead.U_ItemCd = sItemCode
                    oLoadHead.U_DOR = ""
                    oLoadHead.U_BALDDT = DateTime.Now()

                    If oLoadHead.doAddDataRow() Then
                        oLoadRow = New User.IDH_MANBALDB()

                        Dim oBatch As Batch
                        For Each oBatch In aoBatches
                            oLoadRow.doAddEmptyRow(True)
                            oLoadRow.Code = oBatch.AbsEntry.ToString()
                            oLoadRow.Name = oLoadRow.Code
                            oLoadRow.U_BALDNR = oLoadHead.Code
                            oLoadRow.U_BANR = oBatch.BatchNumber
                            oLoadRow.U_Weight = oBatch.Weight
                        Next

                        bResult = oLoadRow.doProcessData()

                    End If
                    com.idh.bridge.DataHandler.INSTANCE.doReleaseObject(oLoadHead)
                    com.idh.bridge.DataHandler.INSTANCE.doReleaseObject(oLoadRow)

                    doClearAll(False)
                    Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                End If
            End If
            Return True
        End Function

        '**
        ' Handle the Update Event
        '**
        Public Function doUpdateEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
            End If
            Return True
        End Function

        '**
        ' Handle the Find Button Event
        '**
        Public Function doFindButtonEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            ' 			If pVal.BeforeAction = False Then
            doLoadData()
            '            End If
            Return True
        End Function

        '**
        ' Do the Code change
        '
        Public Function doWasteCodeLostFocus(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                'doLoadData()
                Dim sWasteCode As String = getFormDFValue("U_ItemCd")

                Dim sGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
                setSharedData("IDH_ITMCOD", sWasteCode)
                'setSharedData("IDH_GRPCOD", sGrp)
                setSharedData("IDH_INVENT", "Y")
                setSharedData("SILENT", "SHOWMULTI")
                doOpenModalForm("IDHWISRC", Nothing)
            End If
            Return True
        End Function

        '        Public Function doPrintButtonEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        ' 			If pVal.BeforeAction = True Then
        '            End If
        '        End Function

        Public Function doWasteLookup(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sWasteCode As String = getFormDFValue("U_ItemCd")

                Dim sGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
                setSharedData("IDH_ITMCOD", sWasteCode)
                'setSharedData("IDH_GRPCOD", sGrp)
                setSharedData("IDH_INVENT", "Y")
                setSharedData("SILENT", "SHOWMULTI")
                doOpenModalForm("IDHWISRC", Nothing)
            End If
            Return False
        End Function

        Public Function doPrintEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim sReportFile As String = Config.Parameter("LDFRM")

                Dim sLoadNr As String = getFormDFValue("Code")
                If Not sLoadNr Is Nothing AndAlso _
                 sLoadNr.Length > 0 AndAlso _
                 Not sReportFile Is Nothing AndAlso _
                 sReportFile.Length > 0 Then
                    Dim oParams As New Hashtable
                    oParams.Add("LoadNR", sLoadNr)
                    If (Config.INSTANCE.getParameterAsBool("UNEWRPTV", False) = True) Then
                        IDHAddOns.idh.forms.Base.setSharedData(SBOForm, "IDH_RPTPARMS", oParams)
                        IDHAddOns.idh.forms.Base.setSharedData(SBOForm, "IDH_RPTNAME", sReportFile)
                        IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", SBOForm, False)
                    Else
                        IDHAddOns.idh.report.Base.doCallReportDefaults(SBOForm, sReportFile, "TRUE", "FALSE", "1", oParams, IDHForm.gsType)
                    End If
                End If
            End If
            Return True
        End Function

        Public Function doResizeEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim oItem As SAPbouiCOM.Item
                Dim iTopSpace As Integer = 42
                Dim iMidSpace As Integer = 24
                Dim iBotSpace As Integer = 37
                Dim iWorkableHeight As Integer

                oItem = Items.Item("1000001")
                iTopSpace = oItem.Top + oItem.Height

                oItem = Items.Item("1")

                iWorkableHeight = (oItem.Top - 6) - (iTopSpace + iMidSpace)

                Dim iGridHeight As Integer = CType(Int(iWorkableHeight / 2), Integer)

                oItem = moAvailableGrid.getItem()
                oItem.Height = iGridHeight
                oItem.Width = Width - 10

                oItem = Items.Item("11")
                oItem.Top = iTopSpace + iGridHeight + 10

                oItem = moLockedGrid.getItem()
                oItem.Top = iTopSpace + iGridHeight + iMidSpace
                oItem.Height = iGridHeight
                oItem.Width = Width - 10
            End If
            Return True
        End Function

#End Region

        Private Sub doClearAll(ByVal bDoNewCode As Boolean)
            'Using the Form's main table
            setFormDFValue("U_DOR", "")
            setFormDFValue("U_ItemCd", "")
            setFormDFValue("U_BALDDT", DateTime.Now())

            If bDoNewCode = True Then
                'sCode = DataHandler.doGenerateCode(IDH_MANBALD.AUTONUMPREFIX, "MANBALD")
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_MANBALD.AUTONUMPREFIX, "MANBALD")
                setFormDFValue("Code", oNumbers.CodeCode)
                setFormDFValue("Name", oNumbers.NameCode)
            Else
                'Using the Form's main table
                setFormDFValue("Code", "")
                setFormDFValue("Name", "")
            End If
        End Sub

        Public Overrides Sub doHandleModalResultShared(ByVal sModalFormType As String, ByVal sLastButton As String)
            If sModalFormType = "IDHWISRC" Then
                setFormDFValue("U_ItemCd", getSharedData("ITEMCODE"), False, True)
                doReloadGrids()
            End If
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef BubbleEvent As Boolean)
            doAddDF("IDHWCICD", "U_ItemCd")
            doAddDF("IDHBALDDT", "U_BALDDT")
            doAddDF("IDHBADOR", "U_DOR")
            doAddDF("IDHBALDNR", "Code")

            DataBrowser.BrowseBy = "IDHBALDNR"
            SupportedModes = -1
            AutoManaged = True
            Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE

            'Hide the Rectangles - we don't need them
            Items.Item("IDHGRDAVB").Visible = False
            Items.Item("IDHGRIDUAV").Visible = False
        End Sub

        Public Overrides Sub doBeforeLoadData()
            Dim sLoadCode As String = getParentSharedData("IDH_LOADSHT")
            If isModal() AndAlso Not sLoadCode Is Nothing AndAlso sLoadCode.Length > 0 Then
                setFormDFValue("Code", sLoadCode, True)
            End If

            If moAvailableGrid Is Nothing Then
                moAvailableGrid = New FilterGrid(IDHForm, SBOForm, "IDHGRDAVB", False)
                moAvailableGrid.doAddGridTable(New GridTable("OIBT", "b", "BatchNum", True, True), True)
                moAvailableGrid.doAddGridTable(New GridTable("OBTN", "e", "BatchNum", False, True), False)
            End If

            If moLockedGrid Is Nothing Then
                moLockedGrid = New FilterGrid(IDHForm, SBOForm, "IDHGRIDUAV", False)
                moLockedGrid.doAddGridTable(New GridTable("OIBT", "b", "BatchNum", True, True), True)
                moLockedGrid.doAddGridTable(New GridTable("OBTN", "e", "BatchNum", False, True), False)
            End If

            doSetListFields()
        End Sub

        Private Sub doFindLoadSheet()
            'IDH_LOADSHT
            Dim oCons As New SAPbouiCOM.Conditions
            Dim oCon As SAPbouiCOM.Condition
            Dim sCode As String = getFormDFValue("Code", True)

            oCon = oCons.Add
            oCon.Alias = "Code"
            oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
            oCon.CondVal = sCode
            FormMainDataSource.Query(oCons)

            doLoadData()
        End Sub

        Protected Sub doSetListFields()
            'Available Batches
            moAvailableGrid.doAddListField("e.AbsEntry", "System Number", False, 20, "IGNORE", Nothing, -1, Nothing)
            'moAvailableGrid.doAddListField("b.SysNumber", "System Number", False, 20, "IGNORE", Nothing, 0, Nothing)
            moAvailableGrid.doAddListField("b.BatchNum", "Batch Number", False, 100, "IGNORE", Nothing, -1, Nothing)
            moAvailableGrid.doAddListField("b.ItemCode", "Waste Code", False, 100, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            moAvailableGrid.doAddListField("b.Quantity", "Weight", False, 50, "IGNORE", Nothing, -1, Nothing)
            moAvailableGrid.doAddListField("'N'", "Load", True, 30, "CHECKBOX", Nothing, -1, Nothing)

            'Locked Batches
            Dim sStatusField As String = " Case When b.Status = 0 THEN 'Released' When b.Status = 1 THEN 'Not Accessible' ELSE 'Locked' End "
            moLockedGrid.doAddListField("e.AbsEntry", "System Number", False, 20, "IGNORE", Nothing, -1, Nothing)
            'moLockedGrid.doAddListField("b.SysNumber", "System Number", False, 20, "IGNORE", Nothing, 0, Nothing)
            moLockedGrid.doAddListField("b.BatchNum", "Batch Number", False, 100, "IGNORE", Nothing, -1, Nothing)
            moLockedGrid.doAddListField("b.ItemCode", "Waste Code", False, 100, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            moLockedGrid.doAddListField("b.Quantity", "Weight", False, 50, "IGNORE", Nothing, -1, Nothing)
            moLockedGrid.doAddListField(sStatusField, "Reason", False, 100, "IGNORE", Nothing, -1, Nothing)
        End Sub

        '** The Initializer
        Public Overrides Sub doLoadData()
            doReloadGrids()

            Dim sLoadNr As String = getFormDFValue("Code")
            If sLoadNr Is Nothing OrElse sLoadNr.Length = 0 Then
                Items.Item("IDHPRNBTN").Enabled = False
            Else
                Items.Item("IDHPRNBTN").Enabled = True
            End If
        End Sub

        Private Sub doReloadGrids()
            Dim sLoadNumber As String = getFormDFValue("Code", True)
            Dim sItemCode As String = getFormDFValue("U_ItemCd", True)
            Dim sRequiredString As String = " b.SysNumber = e.SysNumber AND b.ItemCode = e.ItemCode And "
            Dim sRequiredString2 As String

            If sItemCode.Length > 0 Then
                sRequiredString = sRequiredString & " b.ItemCode = '" & sItemCode & "' And "
            End If

            '			SELECT  TOP 2000 e.AbsEntry As Col1,b.BatchNum As Col2,b.ItemCode As Col3,b.Quantity As Col4 
            '			FROM [OIBT] b,[OBTN] e 
            '			WHERE  b.SysNumber = e.SysNumber 
            '				AND b.ItemCode = e.ItemCode 
            '				And e.AbsEntry In ('2','4','7') 
            '			--	And b.Status = 0 
            '			--	And b.Quantity > 0 
            '				And e.AbsEntry Not In (SELECT Code From [@IDH_MANBALDB] WHERE U_BALDNR != '27') 

            Dim oLoadRow As User.IDH_MANBALDB = New User.IDH_MANBALDB()
            Dim sBatchNumbers As String = ""
            Dim aoBatches As ArrayList = New ArrayList()
            Dim bIsReload As Boolean = False
            If sLoadNumber.Length > 0 AndAlso _
             oLoadRow.getData(com.idh.dbObjects.Base.IDH_MANBALDB._BALDNR & " = '" & sLoadNumber & "'", Nothing) > 0 Then
                bIsReload = True

                While oLoadRow.next()
                    If sBatchNumbers.Length > 0 Then
                        sBatchNumbers = sBatchNumbers & ","
                    End If
                    sBatchNumbers = sBatchNumbers & "'" & oLoadRow.Code & "'"
                End While

                EnableItem(False, "IDHWCICD")
                EnableItem(False, "IDHWTCFL")
                EnableItem(False, "IDHBALDDT")
                EnableItem(False, "IDHBALDNR")
                'setEnableItem(False, "IDHBADOR")

                moAvailableGrid.doRemoveListField("'N'")
                sRequiredString2 = sRequiredString & " e.AbsEntry In (" & sBatchNumbers & ") "
            Else
                sRequiredString2 = sRequiredString & " b.Status = 0 And b.Quantity > 0 And e.AbsEntry Not In (SELECT Code From [@IDH_MANBALDB] WHERE U_BALDNR != '" & sLoadNumber & "') "
            End If

            moAvailableGrid.setRequiredFilter(sRequiredString2)
            moAvailableGrid.doReloadData()

            'Now click the Batches that was clicked before
            '        	Dim oLoadRow As User.IDH_MANBALDB = New User.IDH_MANBALDB()
            '        	Dim aoBatches As ArrayList = New ArrayList()
            '        	If oLoadRow.getData( oLoadRow._BALDNR & " = '" & sLoadNumber & "'", Nothing ) > 0 Then
            '				While oLoadRow.Next()
            '					aoBatches.Add( oLoadRow.Code )
            '				End While
            '
            ' 				Dim sAbsEntry As String
            ' 				If moAvailableGrid.getRowCount() > 0 Then
            '					For iRow As Integer = 0 To moAvailableGrid.getRowCount() - 1
            '						sAbsEntry = moAvailableGrid.doGetFieldValue("e.AbsEntry", iRow).ToString()
            '						If aoBatches.Contains( sAbsEntry ) Then
            '							moAvailableGrid.doSetFieldValue(4, iRow, "Y" )
            '						Else
            '							moAvailableGrid.doSetFieldValue(4, iRow, "N" )
            '						End If
            '					Next
            '				End If
            '
            '        		setEnableItem(False, "IDHWCICD")
            '				setEnableItem(False, "IDHWTCFL")
            '				setEnableItem(False, "IDHBALDDT")
            '				setEnableItem(False, "IDHBALDNR")
            '				'setEnableItem(False, "IDHBADOR")
            '        	Else
            If bIsReload = False Then
                If Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    EnableItem(True, "IDHBALDNR")
                    setFocus("IDHBALDNR")

                    EnableItem(False, "IDHWCICD")
                    EnableItem(False, "IDHWTCFL")
                    EnableItem(False, "IDHBALDDT")
                    'setEnableItem(True, "IDHBADOR")
                Else
                    EnableItem(True, "IDHWCICD")
                    EnableItem(True, "IDHWTCFL")
                    EnableItem(True, "IDHBALDDT")
                    EnableItem(True, "IDHBALDNR")
                    'setEnableItem(True, "IDHBADOR")
                End If
            End If
            '			com.idh.bridge.DataHandler.INSTANCE.doReleaseObject(oLoadRow)


            moLockedGrid.setRequiredFilter(sRequiredString & " b.Status != 0 ")
            moLockedGrid.doReloadData()
        End Sub

    End Class
End Namespace
