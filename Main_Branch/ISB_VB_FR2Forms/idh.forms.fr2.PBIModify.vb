﻿Imports System.IO
Imports System.Collections
Imports System

Imports IDHAddOns.idh.controls
Imports IDHAddOns
Imports com.idh.utils
Imports com.idh.bridge
Imports com.idh.bridge.data
Imports com.idh.dbObjects.User

Namespace idh.forms.fr2
    Public Class PBIModify
        Inherits com.idh.forms.oo.Form
        
        Private mbDaysChanged As Boolean = False
        Private mbIsAll As Boolean = True
        
#Region "Properties"        
		Public ReadOnly Property ActionDate As DateTime
			Get
				Dim sDate As String = getUFValue("IDH_ACDATE")
                Dim dActDate As DateTime
                If Not sDate Is Nothing AndAlso sDate.Length >= 6 Then
                	sDate = com.idh.utils.dates.doSBODateToSQLDate(sDate)
                    dActDate = Conversions.ToDateTime(sDate)
               	Else
                	dActDate = DateTime.Now
                End If
                Return dActDate
			End Get
		End Property
		
		Private moPBINumbers As ArrayList
        Public Property PBINumbers() As ArrayList
            Get
                Return moPBINumbers
            End Get
            Set(ByVal value As ArrayList)
                moPBINumbers = value
            End Set
        End Property        
        
        Public Property DOREAL As String
        	Get 
        		Return getUFValue("IDH_DOREAL")
        	End Get
        	Set 
        		setUFValue("IDH_DOREAL", value)
        	End Set
        End Property
        
        '** SET ALL PBI DAY VALUES
        Public Property AROUTE As String
        	Get 
        		Return getUFValue("IDH_AROUTE")
        	End Get
        	Set 
        		setUFValue("IDH_AROUTE", value)
        	End Set
        End Property
        
        Public Property ASEQ As String
        	Get 
        		Return getUFValue("IDH_ASEQ")
        	End Get
        	Set 
        		setUFValue("IDH_ASEQ", value)
        	End Set
        End Property
        
        '** SET ONLY THE SELECTED PBI DAY VALUES
        '****** DAYS
        Public Property DMON As String
        	Get 
        		Return getUFValue("IDH_DMON")
        	End Get
        	Set 
        		setUFValue("IDH_DMON", value)
        	End Set
        End Property
        
        Public Property DTUE As String
        	Get 
        		Return getUFValue("IDH_DTUE")
        	End Get
        	Set 
        		setUFValue("IDH_DTUE", value)
        	End Set
        End Property
		
        Public Property DWED As String
        	Get 
        		Return getUFValue("IDH_DWED")
        	End Get
        	Set 
        		setUFValue("IDH_DWED", value)
        	End Set
        End Property
        
        Public Property DTHU As String
        	Get 
        		Return getUFValue("IDH_DTHU")
        	End Get
        	Set 
        		setUFValue("IDH_DTHU", value)
        	End Set
        End Property
        
        Public Property DFRI As String
        	Get 
        		Return getUFValue("IDH_DFRI")
        	End Get
        	Set 
        		setUFValue("IDH_DFRI", value)
        	End Set
        End Property
        
        Public Property DSAT As String
        	Get 
        		Return getUFValue("IDH_DSAT")
        	End Get
        	Set 
        		setUFValue("IDH_DSAT", value)
        	End Set
        End Property
        
        Public Property DSUN As String
        	Get 
        		Return getUFValue("IDH_DSUN")
        	End Get
        	Set 
        		setUFValue("IDH_DSUN", value)
        	End Set
        End Property
        
        '****** DAY ROUTES
        Public Property DMONR As String
        	Get 
        		Return getUFValue("IDH_DMONR")
        	End Get
        	Set 
        		setUFValue("IDH_DMONR", value)
        	End Set
        End Property
        
        Public Property DTUER As String
        	Get 
        		Return getUFValue("IDH_DTUER")
        	End Get
        	Set 
        		setUFValue("IDH_DTUER", value)
        	End Set
        End Property
		
        Public Property DWEDR As String
        	Get 
        		Return getUFValue("IDH_DWEDR")
        	End Get
        	Set 
        		setUFValue("IDH_DWEDR", value)
        	End Set
        End Property
        
        Public Property DTHUR As String
        	Get 
        		Return getUFValue("IDH_DTHUR")
        	End Get
        	Set 
        		setUFValue("IDH_DTHUR", value)
        	End Set
        End Property
        
        Public Property DFRIR As String
        	Get 
        		Return getUFValue("IDH_DFRIR")
        	End Get
        	Set 
        		setUFValue("IDH_DFRIR", value)
        	End Set
        End Property
        
        Public Property DSATR As String
        	Get 
        		Return getUFValue("IDH_DSATR")
        	End Get
        	Set 
        		setUFValue("IDH_DSATR", value)
        	End Set
        End Property
        
        Public Property DSUNR As String
        	Get 
        		Return getUFValue("IDH_DSUNR")
        	End Get
        	Set 
        		setUFValue("IDH_DSUNR", value)
        	End Set
        End Property
        
        '****** DAY SEQ
        Public Property DMONS As String
        	Get 
        		Return getUFValue("IDH_DMONS")
        	End Get
        	Set 
        		setUFValue("IDH_DMONS", value)
        	End Set
        End Property
        
        Public Property DTUES As String
        	Get 
        		Return getUFValue("IDH_DTUES")
        	End Get
        	Set 
        		setUFValue("IDH_DTUES", value)
        	End Set
        End Property
		
        Public Property DWEDS As String
        	Get 
        		Return getUFValue("IDH_DWEDS")
        	End Get
        	Set 
        		setUFValue("IDH_DWEDS", value)
        	End Set
        End Property
        
        Public Property DTHUS As String
        	Get 
        		Return getUFValue("IDH_DTHUS")
        	End Get
        	Set 
        		setUFValue("IDH_DTHUS", value)
        	End Set
        End Property
        
        Public Property DFRIS As String
        	Get 
        		Return getUFValue("IDH_DFRIS")
        	End Get
        	Set 
        		setUFValue("IDH_DFRIS", value)
        	End Set
        End Property
        
        Public Property DSATS As String
        	Get 
        		Return getUFValue("IDH_DSATS")
        	End Get
        	Set 
        		setUFValue("IDH_DSATS", value)
        	End Set
        End Property
        
        Public Property DSUNS As String
        	Get 
        		Return getUFValue("IDH_DSUNS")
        	End Get
        	Set 
        		setUFValue("IDH_DSUNS", value)
        	End Set
        End Property
#End Region
        
        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByRef oSBOParentForm As SAPbouiCOM.Form, ByRef oSBOForm As SAPbouiCOM.Form)
        	MyBase.New(oIDHForm, oSBOParentForm, oSBOForm)
        	doSetHandlers()
        End Sub

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal sParentId As String, ByRef oSBOForm As SAPbouiCOM.Form)
        	MyBase.New(oIDHForm, sParentId, oSBOForm)
        	doSetHandlers()
        End Sub
        
        Private Sub doSetHandlers()
            Handler_Button_Update = New ev_Item_Event(AddressOf doOKButton)
        	
            addHandler_ITEM_PRESSED("IDH_SETALL", New ev_Item_Event(AddressOf doSwitchToAll))
            addHandler_ITEM_PRESSED("IDH_SETDAY", New ev_Item_Event(AddressOf doSwitchToDay))
        	
        	'Use The switch the Day Change flag
            addHandler_ITEM_PRESSED("IDH_DMON", New ev_Item_Event(AddressOf doSetDayChanged))
            addHandler_ITEM_PRESSED("IDH_DTUE", New ev_Item_Event(AddressOf doSetDayChanged))
            addHandler_ITEM_PRESSED("IDH_DWED", New ev_Item_Event(AddressOf doSetDayChanged))
            addHandler_ITEM_PRESSED("IDH_DTHU", New ev_Item_Event(AddressOf doSetDayChanged))
            addHandler_ITEM_PRESSED("IDH_DFRI", New ev_Item_Event(AddressOf doSetDayChanged))
            addHandler_ITEM_PRESSED("IDH_DSAT", New ev_Item_Event(AddressOf doSetDayChanged))
            addHandler_ITEM_PRESSED("IDH_DSUN", New ev_Item_Event(AddressOf doSetDayChanged))
        End Sub        
        
        Public Overloads Shared Function doRegisterFormClass() As IDHAddOns.idh.forms.Base
            Dim owForm As com.idh.forms.oo.FormController = New com.idh.forms.oo.FormController( _
                "IDH_PBIMOD", _
                0, _
                "PBI Modify.srf", _
                False)
            Return owForm
        End Function

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef BubbleEvent As Boolean)
        	
        	doAddUF("IDH_AROUTE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_ASEQ", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_ACDATE", SAPbouiCOM.BoDataType.dt_DATE, 10)
        	
        	doAddUFCheck("IDH_DMON", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
        	doAddUFCheck("IDH_DTUE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
        	doAddUFCheck("IDH_DWED", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
        	doAddUFCheck("IDH_DTHU", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
        	doAddUFCheck("IDH_DFRI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
        	doAddUFCheck("IDH_DSAT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
        	doAddUFCheck("IDH_DSUN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
        	
        	doAddUF("IDH_DMONR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_DTUER", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_DWEDR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_DTHUR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_DFRIR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_DSATR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_DSUNR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	
        	doAddUF("IDH_DMONS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_DTUES", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_DWEDS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_DTHUS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_DFRIS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_DSATS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	doAddUF("IDH_DSUNS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
        	
        	doAddUFCheck("IDH_DOREAL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N").AffectsFormMode = False
        	setUFValue("IDH_DOREAL", "Y")
        	doAddUF("IDH_SETALL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 5).AffectsFormMode = False
        	
        	Dim oOpt As SAPbouiCOM.OptionBtn = Nothing
            oOpt = Items.Item("IDH_SETALL").Specific
            oOpt.ValOn = "1"
            oOpt.Selected = True

            oOpt = Items.Item("IDH_SETDAY").Specific
            oOpt.ValOn = "2"
            oOpt.GroupWith("IDH_SETALL")
        	
            SBOForm.AutoManaged = TRue
        End Sub
        
		'***
		'*** This function will only update the Routes and Sequences it the Length is bigger than 0
		'*** If any of the days are changed all the days will be updated along with the Routes and Seq 
		'**			Unselected days will be cleared along with their routes and seq
		'***
		Public Function doUpdatePBI() As Boolean
            Dim oPBI As IDH_PBI = Nothing
			If Not moPBINumbers Is Nothing AndAlso moPBINumbers.Count > 0 Then
				Dim sSelectedPBIs As String = com.idh.utils.General.doCreateStringCSLSQL(moPBINumbers)
				oPBI = New IDH_PBI()
				oPBI.getData( IDH_PBI._Code & " IN (" & sSelectedPBIs & ")", Nothing)
			End If
			
			If Not oPBI Is Nothing AndAlso oPBI.Count > 0 Then
				While oPBI.Next()
					If mbIsAll Then
						If AROUTE.Length > 0 Then
							If oPBI.U_IDHRDMON2 = "Y" Then oPBI.U_IDHRCDM = AROUTE
							If oPBI.U_IDHRDTUE2 = "Y" Then oPBI.U_IDHRCDTU = AROUTE
							If oPBI.U_IDHRDWED2 = "Y" Then oPBI.U_IDHRCDW = AROUTE
							If oPBI.U_IDHRDTHU2 = "Y" Then oPBI.U_IDHRCDTH = AROUTE
							If oPBI.U_IDHRDFRI2 = "Y" Then oPBI.U_IDHRCDF = AROUTE
							If oPBI.U_IDHRDSAT2 = "Y" Then oPBI.U_IDHRCDSA = AROUTE
							If oPBI.U_IDHRDSUN2= "Y" Then oPBI.U_IDHRCDSU = AROUTE
						End If
					
						If ASEQ.Length > 0 Then
							If oPBI.U_IDHRDMON2 = "Y" Then oPBI.U_IDHRSQM = ASEQ
							If oPBI.U_IDHRDTUE2 = "Y" Then oPBI.U_IDHRSQTU = ASEQ
							If oPBI.U_IDHRDWED2 = "Y" Then oPBI.U_IDHRSQW = ASEQ
							If oPBI.U_IDHRDTHU2 = "Y" Then oPBI.U_IDHRSQTH = ASEQ
							If oPBI.U_IDHRDFRI2 = "Y" Then oPBI.U_IDHRSQF = ASEQ
							If oPBI.U_IDHRDSAT2 = "Y" Then oPBI.U_IDHRSQSA = ASEQ
							If oPBI.U_IDHRDSUN2 = "Y" Then oPBI.U_IDHRSQSU = ASEQ
						End If
					Else
						If mbDaysChanged = False Then
							If oPBI.U_IDHRDMON2 = "Y" Then
								If DMONR.Length > 0 Then oPBI.U_IDHRCDM = DMONR
								If DMONS.Length > 0 Then oPBI.U_IDHRSQM = DMONS
							End If
							
							If oPBI.U_IDHRDTUE2 = "Y" Then
								If DTUER.Length > 0 Then oPBI.U_IDHRCDTU = DTUER
								If DTUES.Length > 0 Then oPBI.U_IDHRSQTU = DTUES
							End If
							
							If oPBI.U_IDHRDWED2 = "Y" Then
								If DWEDR.Length > 0 Then oPBI.U_IDHRCDW = DWEDR
								If DWEDS.Length > 0 Then oPBI.U_IDHRSQW = DWEDS
							End If
							
							If oPBI.U_IDHRDTHU2 = "Y" Then
								If DTHUR.Length > 0 Then oPBI.U_IDHRCDTH = DTHUR
								If DTHUS.Length > 0 Then oPBI.U_IDHRSQTH = DTHUS
							End If
							
							If oPBI.U_IDHRDFRI2 = "Y" Then
								If DFRIR.Length > 0 Then oPBI.U_IDHRCDF = DFRIR
								If DFRIS.Length > 0 Then oPBI.U_IDHRSQF = DFRIS
							End If
							
							If oPBI.U_IDHRDSAT2 = "Y" Then
								If DSATR.Length > 0 Then oPBI.U_IDHRCDSA = DSATR
								If DSATS.Length > 0 Then oPBI.U_IDHRSQSA = DSATS
							End If
							
							If oPBI.U_IDHRDSUN2 = "Y" Then
								If DSUNR.Length > 0 Then oPBI.U_IDHRCDSU = DSUNR
								If DSUNS.Length > 0 Then oPBI.U_IDHRSQSU = DSUNS
							End If
						Else
							If DMON = "Y" Then
								oPBI.U_IDHRDMON2 = "Y"
								If DMONR.Length > 0 Then oPBI.U_IDHRCDM = DMONR
								If DMONS.Length > 0 Then oPBI.U_IDHRSQM = DMONS
							Else
								oPBI.U_IDHRDMON2 = "N"
								oPBI.U_IDHRCDM = ""
								oPBI.U_IDHRSQM = 0
							End If
							
							If DTUE = "Y" Then
								oPBI.U_IDHRDTUE2 = "Y"
								If DTUER.Length > 0 Then oPBI.U_IDHRCDTU = DTUER
								If DTUES.Length > 0 Then oPBI.U_IDHRSQTU = DTUES
							Else
								oPBI.U_IDHRDTUE2 = "N"
								oPBI.U_IDHRCDTU = ""
								oPBI.U_IDHRSQTU = 0
							End If
							
							If DWED = "Y" Then
								oPBI.U_IDHRDWED2 = "Y"
								If DWEDR.Length > 0 Then oPBI.U_IDHRCDW = DWEDR
								If DWEDS.Length > 0 Then oPBI.U_IDHRSQW = DWEDS
							Else
								oPBI.U_IDHRDWED2 = "N"
								oPBI.U_IDHRCDW = ""
								oPBI.U_IDHRSQW = 0
							End If
							
							If DTHU = "Y" Then
								oPBI.U_IDHRDTHU2 = "Y"
								If DTHUR.Length > 0 Then oPBI.U_IDHRCDTH = DTHUR
								If DTHUS.Length > 0 Then oPBI.U_IDHRSQTH = DTHUS
							Else
								oPBI.U_IDHRDTHU2 = "N"
								oPBI.U_IDHRCDTH = ""
								oPBI.U_IDHRSQTH = 0
							End If
							
							If DFRI = "Y" Then 
								oPBI.U_IDHRDFRI2 = "Y"
								If DFRIR.Length > 0 Then oPBI.U_IDHRCDF = DFRIR
								If DFRIS.Length > 0 Then oPBI.U_IDHRSQF = DFRIS
							Else
								oPBI.U_IDHRDFRI2 = "N"
								oPBI.U_IDHRCDF = ""
								oPBI.U_IDHRSQF = 0
							End If
							
							If DSAT = "Y" Then
								oPBI.U_IDHRDSAT2 = "Y"
								If DSATR.Length > 0 Then oPBI.U_IDHRCDSA = DSATR
								If DSATS.Length > 0 Then oPBI.U_IDHRSQSA = DSATS
							Else
								oPBI.U_IDHRDSAT2 = "N"
								oPBI.U_IDHRCDSA = ""
								oPBI.U_IDHRSQSA = 0
							End If
							
							If DSUN = "Y" Then 
								oPBI.U_IDHRDSUN2 = "Y"
								If DSUNR.Length > 0 Then oPBI.U_IDHRCDSU = DSUNR
								If DSUNS.Length > 0 Then oPBI.U_IDHRSQSU = DSUNS
							Else
								oPBI.U_IDHRDSUN2 = "N"
								oPBI.U_IDHRCDSU = ""
								oPBI.U_IDHRSQSU = 0
							End If
						End If
					End If
					oPBI.U_IDHRECOM = oPBI.doDescribeCoverage()
				End While

                ''Save and process the PBI
                If oPBI.doProcessData() Then
                    oPBI.first()
                    While oPBI.next()
                        If DOREAL = "Y" Then
                            Dim oWOM As WR1_PBI.IDH_PBI_Handler = New WR1_PBI.IDH_PBI_Handler(DataHandler.INSTANCE.SBOCompany)

                            Dim dSetNextRun As Date = oPBI.U_IDHNEXTR
                            Dim dCalculatedNextRun As Date = ActionDate

                            While Date.Compare(dCalculatedNextRun, dSetNextRun) < 0
                                If oWOM.ProcessOrderRun(oPBI.Code, dCalculatedNextRun) = False Then
                                    Exit While
                                Else
                                    dCalculatedNextRun = oWOM.CalculatedNextRun
                                End If
                            End While
                        Else
                            'INSERT INTO SCHEDULER
                            Dim oSched As IDH_Scheduler = New IDH_Scheduler()

                            oSched.Description = "PBI Manager change"
                            oSched.Class = "com.idh.processor.PBI"
                            oSched.Processor = "doProcess"

                            Dim sParam As String = "CODES=[" & oPBI.Code & "];" & Chr(13) & "TOEND=[Y];" & Chr(13)
                            oSched.Parameters = sParam

                            oSched.StartDate = oPBI.U_IDHRECSD
                            'oSched.EndDate_AsString(tbEndDate.Text);

                            oSched.CurrCount = 0
                            oSched.MaxCount = -1
                            oSched.MonthDay = 0
                            oSched.PMonth = 0

                            oSched.Su = Nothing
                            oSched.Mo = Nothing
                            oSched.Tu = Nothing
                            oSched.We = Nothing
                            oSched.Th = Nothing
                            oSched.Fr = Nothing
                            oSched.Sa = Nothing

                            oSched.ProcDate = ActionDate

                            oSched.ProcTime = Nothing

                            oSched.LastRun = DateTime.Now
                            oSched.Status = "Added"

                            If oSched.doAddDataRow() = False Then
                                'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Scheduling the Update.", "Error Scheduling the Update.")
                                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Scheduling the Update.", "ERSYESUP", {Nothing})
                            End If
                        End If
                    End While
                End If
            End If
        	Return True
        End Function
        
#Region "FormEvents"        
		'***
		'*** This function will only update the Routes and Sequences it the Length is bigger than 0
		'*** If any of the days are changed all the days will be updated along with the Routes and Seq 
		'**			Unselected days will be cleared along with their routes and seq
		'***
		Public Function doOKButton(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
				doUpdatePBI()
				doClear()
	        	Return True
        End Function
        '        
        Private Sub doClear() 
        	AROUTE = ""
        	ASEQ = ""
        	
        	DMON = "N"
        	DTUE = "N"
        	DWED = "N"
        	DTHU = "N"
        	DFRI = "N"
        	DSAT = "N"
        	DSUN = "N"
        	
        	DMONR = ""
        	DTUER = ""
        	DWEDR = ""
        	DTHUR = ""
        	DFRIR = ""
        	DSATR = ""
        	DSUNR = ""
        	
        	DMONS = ""
        	DTUES = ""
        	DWEDS = ""
        	DTHUS = ""
        	DFRIS = ""
        	DSATS = ""
        	DSUNS = ""
        	
        	setUFValue("IDH_SETALL", "Y")
        End Sub
        
        Public Function doSetDayChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If  DMON = "Y" OrElse _
        		DTUE = "Y" OrElse _
        		DWED = "Y" OrElse _
        		DTHU = "Y" OrElse _
        		DFRI = "Y" OrElse _
        		DSAT = "Y" OrElse _
        		DSUN = "Y" Then
        		mbDaysChanged = True
        	Else
        		mbDaysChanged = False
            End If
            Return True
        End Function
        
        Public Function doSwitchToAll(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	mbIsAll = True
        	
            EnableItem(True, "IDH_AROUTE")
        	EnableItem( True, "IDH_ASEQ")
            setFocus("IDH_AROUTE")

            EnableItem(False, "IDH_DMON")
            EnableItem(False, "IDH_DTUE")
            EnableItem(False, "IDH_DWED")
            EnableItem(False, "IDH_DTHU")
            EnableItem(False, "IDH_DFRI")
            EnableItem(False, "IDH_DSAT")
        	EnableItem( False, "IDH_DSUN")
        	
            EnableItem(False, "IDH_DMONR")
            EnableItem(False, "IDH_DTUER")
            EnableItem(False, "IDH_DWEDR")
            EnableItem(False, "IDH_DTHUR")
            EnableItem(False, "IDH_DFRIR")
            EnableItem(False, "IDH_DSATR")
            EnableItem(False, "IDH_DSUNR")
        	
            EnableItem(False, "IDH_DMONS")
            EnableItem(False, "IDH_DTUES")
            EnableItem(False, "IDH_DWEDS")
            EnableItem(False, "IDH_DTHUS")
            EnableItem(False, "IDH_DFRIS")
            EnableItem(False, "IDH_DSATS")
            EnableItem(False, "IDH_DSUNS")


            setUFValue("IDH_DMON", "")
            setUFValue("IDH_DTUE", "")
            setUFValue("IDH_DWED", "")
            setUFValue("IDH_DTHU", "")
            setUFValue("IDH_DFRI", "")
            setUFValue("IDH_DSAT", "")
            setUFValue("IDH_DSUN", "")

            setUFValue("IDH_DMONR", "")
            setUFValue("IDH_DTUER", "")
            setUFValue("IDH_DWEDR", "")
            setUFValue("IDH_DTHUR", "")
            setUFValue("IDH_DFRIR", "")
            setUFValue("IDH_DSATR", "")
            setUFValue("IDH_DSUNR", "")

            setUFValue("IDH_DMONS", "")
            setUFValue("IDH_DTUES", "")
            setUFValue("IDH_DWEDS", "")
            setUFValue("IDH_DTHUS", "")
            setUFValue("IDH_DFRIS", "")
            setUFValue("IDH_DSATS", "")
            setUFValue("IDH_DSUNS", "")

            Return True
        End Function
        
        Public Function doSwitchToDay(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	mbIsAll = False
        	
            EnableItem(True, "IDH_DMON")
            EnableItem(True, "IDH_DTUE")
            EnableItem(True, "IDH_DWED")
            EnableItem(True, "IDH_DTHU")
            EnableItem(True, "IDH_DFRI")
            EnableItem(True, "IDH_DSAT")
            EnableItem(True, "IDH_DSUN")
        	
            EnableItem(True, "IDH_DMONR")
            EnableItem(True, "IDH_DTUER")
            EnableItem(True, "IDH_DWEDR")
            EnableItem(True, "IDH_DTHUR")
            EnableItem(True, "IDH_DFRIR")
            EnableItem(True, "IDH_DSATR")
            EnableItem(True, "IDH_DSUNR")
        	
            EnableItem(True, "IDH_DMONS")
            EnableItem(True, "IDH_DTUES")
            EnableItem(True, "IDH_DWEDS")
            EnableItem(True, "IDH_DTHUS")
            EnableItem(True, "IDH_DFRIS")
            EnableItem(True, "IDH_DSATS")
            EnableItem(True, "IDH_DSUNS")
            setFocus("IDH_DMONR")

            EnableItem(False, "IDH_AROUTE")
            EnableItem(False, "IDH_ASEQ")

            setUFValue("IDH_AROUTE", "")
            setUFValue("IDH_ASEQ", "")

            Return True
        End Function
#End Region        
    End Class
End Namespace
