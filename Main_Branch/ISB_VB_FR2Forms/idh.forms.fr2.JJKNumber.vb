﻿Imports System.IO
Imports System.Collections
Imports System

'Needed for the Framework controls
Imports IDHAddOns.idh.controls
Imports IDHAddOns
Imports com.idh.bridge.data
Imports com.idh.bridge
Imports com.idh.utils

Namespace idh.forms.fr2
    Public Class JJKNumber
        Inherits com.idh.forms.oo.Form

        Public Property JJKNumber() As String
            Get
                Return getUFValue("IDH_JJKNUM").ToString()
            End Get
            Set(ByVal value As String)
                setUFValue("IDH_JJKNUM", value)
            End Set
        End Property

        'Public RowCodes As ArrayList
        Private msTempConNum As String
        Public Property TempConNum() As String
            Get
                Return msTempConNum
            End Get
            Set(ByVal value As String)
                msTempConNum = value
                setUFValue("IDH_JJKNUM", value)
            End Set
        End Property

        Public RowTable As String = Nothing

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal oSBOParentForm As SAPbouiCOM.Form, ByVal oSBOForm As SAPbouiCOM.Form)
            MyBase.New(oIDHForm, oSBOParentForm, oSBOForm)
            doSetHandlers()
        End Sub

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal sParentId As String, ByRef oSBOForm As SAPbouiCOM.Form)
            MyBase.New(oIDHForm, sParentId, oSBOForm)
            doSetHandlers()
        End Sub

        Private Sub doSetHandlers()
            Handler_Button_Ok = New ev_Item_Event(AddressOf doOkEvent)
        End Sub

        '**
        ' Handle the Ok Button Event
        '**
        Public Function doOkEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                Dim sWrkJJK As String = JJKNumber
                If Not RowTable Is Nothing AndAlso RowTable.Length > 0 _
                    AndAlso Not msTempConNum Is Nothing AndAlso msTempConNum.Length > 0 _
                    AndAlso sWrkJJK.Length > 0 Then

                    DataHandler.INSTANCE.doUpdateQuery("UPDATE [" & RowTable & "] Set U_ConNum = '" & Formatter.doFixForSQL(sWrkJJK) & "' WHERE U_ConNum = '" & msTempConNum & "'")
                End If
            End If
            Return True
        End Function

        Public Overloads Shared Function doRegisterFormClass() As IDHAddOns.idh.forms.Base
            Dim owForm As com.idh.forms.oo.FormController = New com.idh.forms.oo.FormController("IDHJJKNU", 0, "JJKNumber.srf", False)
            Return owForm
        End Function

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef BubbleEvent As Boolean)
            doAddUF("IDH_JJKNUM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
            JJKNumber = "JJK"
            SBOForm.AutoManaged = False
        End Sub

    End Class
End Namespace

