﻿'
' Created by SharpDevelop.
' User: Louis
' Date: 2011/10/31
' Time: 07:19 PM
'
' Only the IDHSBO team are authorized to use this new object to create new Forms.
'
Imports System.IO
Imports System.Collections
Imports System

Imports IDHAddOns.idh.controls
Imports com.idh.bridge.data
Imports com.idh.dbObjects
Imports com.idh.dbObjects.User
Imports com.idh.utils

Imports com.idh.bridge.lookups

Namespace idh.forms.fr2.prices
    Public Class ItemProfileCIPSIP

        'This is the Base Form object from the Framework
        Inherits com.idh.forms.oo.Form

        Private moMDGrid As FilterGrid
        Private moCIPObject As IDH_CSITPR
        Private moSIPObject As IDH_SUITPR
        Private moDataTable As SAPbouiCOM.DataTable
        Private msLastUOM As String

        Private msItemCode As String
        Private msItemDesc As String

        Private mbIsWaste As Boolean = True

        Public Property ItemCode() As String
            Get
                Return msItemCode
            End Get
            Set(ByVal value As String)
                msItemCode = value
            End Set
        End Property

        Public Property ItemDesc() As String
            Get
                Return msItemDesc
            End Get
            Set(ByVal value As String)
                msItemDesc = value
            End Set
        End Property

        Private msItemGroup As String
        Public Property ItemGroup() As String
            Get
                Return msItemGroup
            End Get
            Set(ByVal value As String)
                msItemGroup = value
            End Set
        End Property

        Private msSupplier As String
        Public Property Supplier() As String
            Get
                Return msSupplier
            End Get
            Set(ByVal value As String)
                msSupplier = value
            End Set
        End Property


        Private msCustomer As String = ""
        Public Property Customer() As String
            Get
                Return msCustomer
            End Get
            Set(ByVal value As String)
                msCustomer = value
            End Set
        End Property

        Private msAddress As String = ""
        Public Property Address() As String
            Get
                Return msAddress
            End Get
            Set(ByVal value As String)
                msAddress = value
            End Set
        End Property

        '**
        ' Register the form controller
        '**
        Public Overloads Shared Function doRegisterFormClass() As IDHAddOns.idh.forms.Base
            Dim owForm As com.idh.forms.oo.FormController = New com.idh.forms.oo.FormController( _
                    "IDH_ITMPRPR", _
                    0, _
                    "ItemProfilePrices.srf", _
                    False)
            Return owForm
        End Function

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal sParentId As String, ByRef oSBOForm As SAPbouiCOM.Form)
            MyBase.New(oIDHForm, sParentId, oSBOForm)

            moCIPObject = New IDH_CSITPR(IDHForm, SBOForm)
            moSIPObject = New IDH_SUITPR(IDHForm, SBOForm)

            doSetHandlers()
        End Sub
        Public Sub New(ByVal sParentId As String)
            MyBase.New(Nothing, sParentId, Nothing)

            moCIPObject = New IDH_CSITPR(IDHForm, SBOForm)
            moSIPObject = New IDH_SUITPR(IDHForm, SBOForm)

            doSetHandlers()
        End Sub
        Private Sub doSetHandlers()
            Handler_Button_Add = New ev_Item_Event(AddressOf doAddUpdateEvent)
            Handler_Button_Update = New ev_Item_Event(AddressOf doAddUpdateEvent)
        End Sub

#Region "FormEvents"
        Public Function doAddUpdateEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                moCIPObject.doProcessData()
                moSIPObject.doProcessData()
            End If
            Return True
        End Function
#End Region

        Public Overrides Sub doBeforeLoadData()
            'Fallback on old way
            Dim sVal As String = getParentSharedData("IDH_ITMCD")
            If Not sVal Is Nothing AndAlso msItemCode Is Nothing Then
                msItemCode = sVal
            End If

            If moMDGrid Is Nothing Then
                moMDGrid = New FilterGrid(IDHForm, SBOForm, "IDH_CONTA", True)
                'moMDGrid.AddEditLine = True

                moMDGrid.doAddGridTable(New GridTable(IDH_CSITPR.TableName, "c", IDH_CSITPR._LinkNr, True, True), True)
                moMDGrid.doAddGridTable(New GridTable(IDH_SUITPR.TableName, "s", IDH_SUITPR._Code, True, True), False)

                moMDGrid.Handler_GRID_FIELD_CHANGED = New IDHGrid.ev_GRID_EVENTS(AddressOf doSetCharge)
                moMDGrid.Handler_GRID_DATA_KEY_EMPTY = New IDHGrid.ev_GRID_EVENTS(AddressOf doNewEntry)
                'moMDGrid.Handler_GRID_ROW_ADD = New IDHGrid.ev_GRID_EVENTS(AddressOf doNewRow)
                'moMDGrid.Handler_GRID_ROW_ADD_EMPTY = New IDHGrid.ev_GRID_EVENTS(AddressOf doNewRowEmpty)
            End If
            moDataTable = moMDGrid.getDataTable()

            If msItemGroup Is Nothing OrElse msItemGroup.Length = 0 Then
                mbIsWaste = False
            Else
                If com.idh.bridge.lookups.Config.INSTANCE.isWasteGroup(msItemGroup) = False Then
                    mbIsWaste = False
                End If
            End If

            doSetListFields()
        End Sub

#Region "Grid Events"
        'Private Function doNewRow(ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
        '    If pVal.BeforeAction = False Then
        '        Dim sLinkCode As String = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._LinkNr)
        '        Dim sCIPCode As String = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._Code)
        '        Dim sSIPCode As String = pVal.oGrid.doGetFieldValue("s." & IDH_SUITPR._Code)
        '    End If
        '    Return True
        'End Function
        'Private Function doNewRowEmpty(ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
        '    If pVal.BeforeAction = False Then
        '        Dim sLinkCode As String = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._LinkNr)
        '        Dim sCIPCode As String = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._Code)
        '        Dim sSIPCode As String = pVal.oGrid.doGetFieldValue("s." & IDH_SUITPR._Code)
        '    End If
        '    Return True
        'End Function
        Private Function doNewEntry(ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.BeforeAction = False Then
                Dim iLastRow As Integer = pVal.oGrid.getLastRowIndex()
                msLastUOM = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._UOM, iLastRow)
                If msLastUOM Is Nothing OrElse msLastUOM.Length = 0 Then
                    msLastUOM = "kg"
                End If

                moCIPObject.doAddProfileEntry(-1, msItemCode, msItemDesc, msItemGroup, msLastUOM, 0, Now(), msCustomer, msAddress)
                moSIPObject.doAddProfileEntry(moCIPObject.U_LinkNr, msItemCode, msItemDesc, msItemGroup, moCIPObject.U_UOM, 0, moCIPObject.U_StDate, msSupplier, msCustomer, msAddress)

                moCIPObject.last()
                Dim iRow As Integer = moDataTable.Rows.Count - 1

                moDataTable.SetValue("c." & IDH_CSITPR._LinkNr, iRow, moCIPObject.U_LinkNr)
                moDataTable.SetValue("c." & IDH_CSITPR._Code, iRow, moCIPObject.Code)
                moDataTable.SetValue("s." & IDH_SUITPR._Code, iRow, moSIPObject.Code)
                moDataTable.SetValue("c." & IDH_CSITPR._UOM, iRow, moCIPObject.U_UOM)

                If mbIsWaste Then
                    moDataTable.SetValue("c." & IDH_CSITPR._WasteCd, iRow, moCIPObject.U_WasteCd)
                    moDataTable.SetValue("c." & IDH_CSITPR._TipTon, iRow, moCIPObject.U_TipTon)
                    moDataTable.SetValue("s." & IDH_SUITPR._TipTon, iRow, moSIPObject.U_TipTon)
                Else
                    moDataTable.SetValue("c." & IDH_CSITPR._AItmCd, iRow, moCIPObject.U_AItmCd)
                    moDataTable.SetValue("c." & IDH_CSITPR._AItmPr, iRow, moCIPObject.U_AItmPr)
                    moDataTable.SetValue("s." & IDH_SUITPR._AItmPr, iRow, moSIPObject.U_AItmPr)
                End If

                moDataTable.SetValue("c." & IDH_CSITPR._StDate, iRow, moCIPObject.U_StDate)

                moDataTable.Rows.Add(1)
            End If
            Return True
        End Function

        Private Function doSetCharge(ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.oData.Equals("c." & IDH_CSITPR._TipTon) Then
                    Dim sCIPCode As String = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._Code)
                    Dim sValue As String = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._TipTon)

                    Dim iRowIndex As Integer = moCIPObject.doFindFisrt(IDH_CSITPR._Code, sCIPCode, True)
                    If iRowIndex <> -1 Then
                        moCIPObject.U_TipTon = Conversions.ToDouble(sValue)
                    End If
                ElseIf pVal.oData.Equals("s." & IDH_SUITPR._TipTon) Then
                    Dim sSIPCode As String = pVal.oGrid.doGetFieldValue("s." & IDH_SUITPR._Code)
                    Dim sValue As String = pVal.oGrid.doGetFieldValue("s." & IDH_SUITPR._TipTon)

                    Dim iRowIndex As Integer = moSIPObject.doFindFisrt(IDH_SUITPR._Code, sSIPCode, True)
                    If iRowIndex <> -1 Then
                        moSIPObject.U_TipTon = Conversions.ToDouble(sValue)
                    End If
                ElseIf pVal.oData.Equals("c." & IDH_CSITPR._AItmPr) Then
                    Dim sCIPCode As String = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._Code)
                    Dim sValue As String = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._AItmPr)

                    Dim iRowIndex As Integer = moCIPObject.doFindFisrt(IDH_CSITPR._Code, sCIPCode, True)
                    If iRowIndex <> -1 Then
                        moCIPObject.U_AItmPr = Conversions.ToDouble(sValue)
                    End If
                ElseIf pVal.oData.Equals("s." & IDH_SUITPR._AItmPr) Then
                    Dim sSIPCode As String = pVal.oGrid.doGetFieldValue("s." & IDH_SUITPR._Code)
                    Dim sValue As String = pVal.oGrid.doGetFieldValue("s." & IDH_SUITPR._AItmPr)

                    Dim iRowIndex As Integer = moSIPObject.doFindFisrt(IDH_SUITPR._Code, sSIPCode, True)
                    If iRowIndex <> -1 Then
                        moSIPObject.U_AItmPr = Conversions.ToDouble(sValue)
                    End If
                ElseIf pVal.oData.Equals("c." & IDH_CSITPR._StDate) Then
                    Dim oValue As Object = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._StDate)

                    Dim sCIPCode As String = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._Code)
                    Dim iRowIndex As Integer = moCIPObject.doFindFisrt(IDH_SUITPR._Code, sCIPCode, True)
                    If iRowIndex <> -1 Then
                        moCIPObject.U_StDate = oValue
                    End If

                    Dim sSIPCode As String = pVal.oGrid.doGetFieldValue("s." & IDH_SUITPR._Code)
                    iRowIndex = moSIPObject.doFindFisrt(IDH_SUITPR._Code, sSIPCode, True)
                    If iRowIndex <> -1 Then
                        moSIPObject.U_StDate = oValue
                    End If
                ElseIf pVal.oData.Equals("c." & IDH_CSITPR._UOM) Then
                    msLastUOM = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._UOM)

                    Dim sCIPCode As String = pVal.oGrid.doGetFieldValue("c." & IDH_CSITPR._Code)
                    Dim iRowIndex As Integer = moCIPObject.doFindFisrt(IDH_CSITPR._Code, sCIPCode, True)
                    If iRowIndex <> -1 Then
                        moCIPObject.U_UOM = msLastUOM
                    End If

                    Dim sSIPCode As String = pVal.oGrid.doGetFieldValue("s." & IDH_SUITPR._Code)
                    iRowIndex = moSIPObject.doFindFisrt(IDH_SUITPR._Code, sSIPCode, True)
                    If iRowIndex <> -1 Then
                        moSIPObject.U_UOM = msLastUOM
                    End If
                End If
            End If
            Return True
        End Function
#End Region

        Protected Sub doSetListFields()
            moMDGrid.doAddListField("c." & IDH_CSITPR._LinkNr, "CIP/SIP LinkNr", False, 0, "IGNORE", Nothing, 0, Nothing)

            moMDGrid.doAddListField("c." & IDH_CSITPR._Code, "CIP Code", False, -1, "IGNORE", Nothing, -1, Nothing)
            moMDGrid.doAddListField("s." & IDH_CSITPR._Code, "SIP Code", False, -1, "IGNORE", Nothing, -1, Nothing)

            moMDGrid.doAddListField("c." & IDH_CSITPR._UOM, "UOM", True, -1, "COMBOBOX", Nothing)

            If mbIsWaste Then
                moMDGrid.doAddListField("c." & IDH_CSITPR._WasteCd, "Waste Code", False, -1, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
                moMDGrid.doAddListField("c." & IDH_CSITPR._TipTon, "Charge", True, -1, "IGNORE", Nothing)
                moMDGrid.doAddListField("s." & IDH_SUITPR._TipTon, "Cost", True, -1, "IGNORE", Nothing)
            Else
                moMDGrid.doAddListField("c." & IDH_CSITPR._AItmCd, "Additional Item", False, -1, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
                moMDGrid.doAddListField("c." & IDH_CSITPR._AItmPr, "Charge", True, -1, "IGNORE", Nothing)
                moMDGrid.doAddListField("s." & IDH_SUITPR._AItmPr, "Cost", True, -1, "IGNORE", Nothing)
            End If
            
            moMDGrid.doAddListField("c." & IDH_CSITPR._StDate, "Start", True, -1, Nothing, Nothing)
        End Sub

        Private Sub doLoadUOMCombo()
            Try
                ''GRID COMBO
                Dim oCombo As SAPbouiCOM.ComboBoxColumn
                oCombo = moMDGrid.Columns.Item(moMDGrid.doIndexFieldWC("c." & IDH_CSITPR._UOM))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doFillCombo(oCombo, "OWGT", "UnitDisply", "UnitName", Nothing, Nothing, Nothing, "t") '"Any", "")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            End Try
        End Sub

        '** The Initializer
        Public Overrides Sub doLoadData()
            Dim sUOMs As String = Config.ParameterWithDefault("ITMPRUOM", "kg,t")
            Dim saUOMList As String() = sUOMs.Split(","c)
            Dim sInList As String = Formatter.doFormatStringList(saUOMList)
            Dim soUOMList As ArrayList = New ArrayList(saUOMList)

            'moMDGrid.setRequiredFilter("c." & IDH_CSITPR._WasteCd & " = '" & msItemCode & "' AND s." & IDH_SUITPR._WasteCd & " = '" & msItemCode & "' AND c." & IDH_CSITPR._UOM & " IN (" & sInList & ")")

            Dim sWhere As String
            If mbIsWaste Then
                sWhere = IDH_CSITPR._WasteCd & " = '" & msItemCode & "'"
            Else
                sWhere = IDH_CSITPR._AItmCd & " = '" & msItemCode & "'"
            End If

            moCIPObject.AutoAddEmptyLine = False
            If moCIPObject.getData(sWhere & " AND " & IDH_CSITPR._LinkNr & " > 0 ", IDH_CSITPR._UOM) > 0 Then
                Dim sList As String = ""
                moCIPObject.first()
                While moCIPObject.next()
                    If sList.Length > 0 Then
                        sList = sList & ","
                    End If
                    sList = sList & moCIPObject.U_LinkNr

                    ''Now remove the UOM from the List
                    If soUOMList.Contains(moCIPObject.U_UOM) Then
                        soUOMList.Remove(moCIPObject.U_UOM)
                    End If
                End While

                moSIPObject.AutoAddEmptyLine = False
                moSIPObject.getData(sWhere & " AND " & IDH_CSITPR._LinkNr & " In (" & sList & ")", IDH_SUITPR._UOM)
            End If

            moDataTable.Columns.Add("c." & IDH_CSITPR._LinkNr, SAPbouiCOM.BoFieldsType.ft_Integer)

            moDataTable.Columns.Add("c." & IDH_CSITPR._Code, SAPbouiCOM.BoFieldsType.ft_AlphaNumeric)
            moDataTable.Columns.Add("s." & IDH_CSITPR._Code, SAPbouiCOM.BoFieldsType.ft_AlphaNumeric)

            moDataTable.Columns.Add("c." & IDH_CSITPR._UOM, SAPbouiCOM.BoFieldsType.ft_AlphaNumeric)

            If mbIsWaste Then
                moDataTable.Columns.Add("c." & IDH_CSITPR._WasteCd, SAPbouiCOM.BoFieldsType.ft_AlphaNumeric)
                moDataTable.Columns.Add("c." & IDH_CSITPR._TipTon, SAPbouiCOM.BoFieldsType.ft_Price)
                moDataTable.Columns.Add("s." & IDH_SUITPR._TipTon, SAPbouiCOM.BoFieldsType.ft_Price)
            Else
                moDataTable.Columns.Add("c." & IDH_CSITPR._AItmCd, SAPbouiCOM.BoFieldsType.ft_AlphaNumeric)
                moDataTable.Columns.Add("c." & IDH_CSITPR._AItmPr, SAPbouiCOM.BoFieldsType.ft_Price)
                moDataTable.Columns.Add("s." & IDH_SUITPR._AItmPr, SAPbouiCOM.BoFieldsType.ft_Price)
            End If

            moDataTable.Columns.Add("c." & IDH_CSITPR._StDate, SAPbouiCOM.BoFieldsType.ft_Date)

            ''Now addd the remaining UOM to the Grid
            'moDataTable.Rows.Add(soUOMList.Count)
            For Each sUOM As String In soUOMList
                moCIPObject.doAddProfileEntry(-1, msItemCode, msItemDesc, msItemGroup, sUOM, 0, Now(), msCustomer, msAddress)
                moSIPObject.doAddProfileEntry(moCIPObject.U_LinkNr, msItemCode, msItemDesc, msItemGroup, sUOM, 0, moCIPObject.U_StDate, msSupplier, msCustomer, msAddress)
            Next

            ''Now step through the found Rows and Add the missing UOM to the DBObjects
            ''Use the CIP as the referance object - the ItemCode and UOM of the CIP will be used
            moCIPObject.first()
            moDataTable.Rows.Add(moCIPObject.Count)
            Dim iRow As Integer = 0
            While moCIPObject.next()
                moDataTable.SetValue("c." & IDH_CSITPR._LinkNr, iRow, moCIPObject.U_LinkNr)
                moDataTable.SetValue("c." & IDH_CSITPR._Code, iRow, moCIPObject.Code)

                Dim iRowIndex As Integer = moSIPObject.doFindFisrt(IDH_CSITPR._LinkNr, moCIPObject.U_LinkNr, True)
                If iRowIndex = -1 Then
                    moSIPObject.doAddProfileEntry(moCIPObject.U_LinkNr, msItemCode, msItemDesc, msItemGroup, moCIPObject.U_UOM, 0, moCIPObject.U_StDate, msSupplier, msCustomer, msAddress)
                End If
                moDataTable.SetValue("s." & IDH_SUITPR._Code, iRow, moSIPObject.Code)

                moDataTable.SetValue("c." & IDH_CSITPR._UOM, iRow, moCIPObject.U_UOM)

                If mbIsWaste Then
                    moDataTable.SetValue("c." & IDH_CSITPR._WasteCd, iRow, moCIPObject.U_WasteCd)
                    moDataTable.SetValue("c." & IDH_CSITPR._TipTon, iRow, moCIPObject.U_TipTon)
                    moDataTable.SetValue("s." & IDH_SUITPR._TipTon, iRow, moSIPObject.U_TipTon)
                Else
                    moDataTable.SetValue("c." & IDH_CSITPR._AItmCd, iRow, moCIPObject.U_AItmCd)
                    moDataTable.SetValue("c." & IDH_CSITPR._AItmPr, iRow, moCIPObject.U_AItmPr)
                    moDataTable.SetValue("s." & IDH_SUITPR._AItmPr, iRow, moSIPObject.U_AItmPr)
                End If

                moDataTable.SetValue("c." & IDH_CSITPR._StDate, iRow, moCIPObject.U_StDate)

                iRow = iRow + 1
            End While
            moDataTable.Rows.Add(1)

            moMDGrid.doApplyRules()
            doLoadUOMCombo()
        End Sub
    End Class
End Namespace
