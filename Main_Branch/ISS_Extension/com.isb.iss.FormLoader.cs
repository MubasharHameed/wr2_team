using System.IO;
using System.Collections;
using com.idh.bridge;
using System;

using SAPbobsCOM;
using com.idh.dbObjects.User;
using com.idh.dbObjects.numbers;
using com.isb.bridge.lookups;
using com.isb.core.controller;

namespace com.isb.iss {     
    public class FormLoader : WR1_Forms.idh.main.MainForm {
        public FormLoader(string sDesc, int iDebugLevel = 0) : base(sDesc, iDebugLevel) {
        }

        protected override bool doSetupDB() {
            return base.doSetupDB();
        }

        //*** Instanciate the lookup Object
        public override void doCreateLookup() {
            base.doCreateLookup();
        }

        protected override void doRegisterForms() {
            doLoadCurrentForms();

            //Core Forms
            if (Config.INSTANCE.getParameterAsBool("WFWOPRE", false)) {
                com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.core.forms.WasteOrder), "IDHORD", 32);
            }
            if (Config.INSTANCE.getParameterAsBool("WFWOPRE", false)) {
                com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.core.forms.DisposalOrder), "IDHORD", 32);
            }

            if (Config.ParameterWithDefault("ENQMOD", "FALSE") == "ENQMOD_TRUE") {
                com.isb.forms.Enquiry.admin.MainForm.doAddForms(this);
            }

            doSearchForms_();
        }

        protected void doSearchForms_() {
            //com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.search.form.sbo.BP)); //, "NONE");

            doAddForm(Loader.getSearchForm("BPSearch", new object[] { this }));
            doAddForm(Loader.getSearchForm("DisposalSite", new object[] { this }));
            doAddForm(Loader.getSearchForm("BPSearch2", new object[] { this }));
            doAddForm(Loader.getSearchForm("AddressSearch3", new object[] { this }));

            doAddForm(Loader.getSearchForm("BPSearchAlternate", new object[] { this }));
            doAddForm(Loader.getSearchForm("AddressSearch", new object[] { this }));

            if (!Config.ParameterAsBool("UNEWSRCH", false)) {
                doAddForm(Loader.getSearchForm("WasteItemSearch", new object[] { this }));
            } else {
                doAddForm(Loader.getSearchForm("WasteItemSearch2", new object[] { this }));
            }

            doAddForm(Loader.getSearchForm("AlternateWasteItemSearch", new object[] { this }));
            doAddForm(Loader.getSearchForm("Route", new object[] { this }));

            doAddForm(Loader.getSearchForm("LinkWO", new object[] { this }));

            if (!Config.ParameterAsBool("UNEWSRCH", false)) {
                doAddForm(Loader.getSearchForm("ItemSearch", new object[] { this }));
            } else {
                doAddForm(Loader.getSearchForm("ItemSearch2", new object[] { this }));
            }

            doAddForm(Loader.getSearchForm("Lorry", new object[] { this }));
            doAddForm(Loader.getSearchForm("JobType", new object[] { this }));
            doAddForm(Loader.getSearchForm("Driver", new object[] { this }));
            doAddForm(Loader.getSearchForm("GlAccnt", new object[] { this }));
            doAddForm(Loader.getSearchForm("Project", new object[] { this }));
            doAddForm(Loader.getSearchForm("ProfitCentre", new object[] { this }));
            doAddForm(Loader.getSearchForm("UN", new object[] { this }));
            doAddForm(Loader.getSearchForm("EWC", new object[] { this }));
            doAddForm(Loader.getSearchForm("HAZ", new object[] { this }));
            doAddForm(Loader.getSearchForm("DRCodes", new object[] { this }));
            doAddForm(Loader.getSearchForm("Origin", new object[] { this }));
            doAddForm(Loader.getSearchForm("Forecast", new object[] { this }));
            doAddForm(Loader.getSearchForm("MAddressSearch", new object[] { this }));
            doAddForm(Loader.getSearchForm("CustRef", new object[] { this }));
            doAddForm(Loader.getSearchForm("Zones", new object[] { this }));

            doAddForm(Loader.getSearchForm("TFSNumbers", new object[] { this }));
            doAddForm(Loader.getSearchForm("TFSAnnex", new object[] { this }));
            doAddForm(Loader.getSearchForm("BPContact", new object[] { this }));
            doAddForm(Loader.getSearchForm("HCode", new object[] { this }));
            doAddForm(Loader.getSearchForm("YCode", new object[] { this }));
            doAddForm(Loader.getSearchForm("TFSCompAuthority", new object[] { this }));
            doAddForm(Loader.getSearchForm("BASEL", new object[] { this }));
            doAddForm(Loader.getSearchForm("OECD", new object[] { this }));
            doAddForm(Loader.getSearchForm("ShipName", new object[] { this }));
            doAddForm(Loader.getSearchForm("WasteProfile", new object[] { this }));
            doAddForm(Loader.getSearchForm("WPTemplateItemSearch", new object[] { this }));
            doAddForm(Loader.getSearchForm("CAS", new object[] { this }));
            doAddForm(Loader.getSearchForm("WPWasteItemSearch", new object[] { this }));
            doAddForm(Loader.getSearchForm("EmployeeSearch", new object[] { this }));
            doAddForm(Loader.getSearchForm("AddressSearch2", new object[] { this }));
            doAddForm(Loader.getSearchForm("USACounty", new object[] { this }));
            doAddForm(Loader.getSearchForm("SalesContact", new object[] { this }));
            doAddForm(Loader.getSearchForm("EPA", new object[] { this }));
            doAddForm(Loader.getSearchForm("ProperShipNm", new object[] { this }));

            //Draft Document List 
            doAddForm(Loader.getSearchForm("DraftSearch", new object[] { this }));
            doAddForm(Loader.getSearchForm("DraftSearchSI", new object[] { this }));
            doAddForm(Loader.getSearchForm("Warehouse", new object[] { this }));

            //EVN Numbers Search 
            doAddForm(Loader.getSearchForm("EVNNumbers", new object[] { this }));
        }

        protected void doLoadCurrentForms() {
            //BP Additional Charges Formula Editor 
            doAddForm(Loader.getISBForm("FormulaEditor", new object[] { this }));

            //doAddForm(New WR1_Admin.idh.forms.lists.Container(Me))
            doAddForm(Loader.getAdminForm("Container", new object[] { this }));

            doAddForm(Loader.getISBForm("OSMEdit", new object[] { this }));
            doAddForm(Loader.getISBForm("Comment", new object[] { this }));
            doAddForm(Loader.getISBForm("ChooseDay", new object[] { this }));

            doAddForm(Loader.getISBForm("ShipNameGen", new object[] { this, "IDHADM", 61 }));
            doAddForm(Loader.getISBForm("DuplicateOpts", new object[] { this }));

            doAddForm(Loader.getISBForm("PayMethod", new object[] { this }));
            doAddForm(Loader.getISBForm("CCPay", new object[] { this }));

            doAddForm(Loader.getISBForm("ENGenOption", new object[] { this }));
            //OPTIONS
            doAddForm(Loader.getISBForm("options.Worksheet", new object[] { this }));

            doAddForm(Loader.getFormInstance("ISB_TFS.dll", "WR1_TFS.idh.forms.TFS", "PhysicalCharacteristics", new object[] { this }));
            doAddForm(Loader.getFormInstance("ISB_TFS.dll", "WR1_TFS.idh.forms.TFS", "PackagingType", new object[] { this }));
            doAddForm(Loader.getFormInstance("ISB_TFS.dll", "WR1_TFS.idh.forms.TFS", "TransportationMode", new object[] { this }));

            //com.idh.forms.oo.Form.doRegisterForm(typeof(WR1_FR2Forms.idh.forms.fr2.JJKNumber));
            Loader.doRegisterFR2Form("JJKNumber", new object[] { });

            //PopUps
            //com.idh.forms.oo.Form.doRegisterForm(typeof(WR1_FR2Forms.idh.forms.fr2.popup.AdditionalMarkDocs));
            Loader.doRegisterFR2Form("popup.AdditionalMarkDocs", new object[] { });
            //com.idh.forms.oo.Form.doRegisterForm(typeof(WR1_FR2Forms.idh.forms.fr2.prices.ItemProfileCIPSIP));
            Loader.doRegisterFR2Form("prices.ItemProfileCIPSIP", new object[] { });

            doAddForm(Loader.getISBForm("Attachment", new object[] { this }));

            string sExtra = goDB.Data().Extra;
            //GENERAL - SUBS
            if (goDB.Data().Extra.Equals("ECO")) {
                doAddForm(Loader.getISBForm("SerialNrSearch", new object[] { this }));
            } else {
                doAddForm(Loader.getSBOForm("BusinessPartner", new object[] { this }));
                doAddForm(Loader.getSBOForm("Activity", new object[] { this }));
                doAddForm(Loader.getSBOForm("AlertOverview", new object[] { this }));

                doAddForm(Loader.getISBForm("SerialNrSearch", new object[] { this }));
                doAddForm(Loader.getISBForm("Map", new object[] { this }));
                    
                //doAddForm(Loader.getSBOForm("ServiceCall", new object[] { this }));
                doAddForm((IDHAddOns.idh.forms.Base)Loader.getObjectInstance("ISS_Extension.dll", "com.isb.iss.forms.SBO", "ServiceCall", new object[] { this }));
                    
                doAddForm(Loader.getSBOForm("Messages", new object[] { this }));
                doAddForm(Loader.getSBOForm("ApprovalRequest", new object[] { this }));
                doAddForm(Loader.getSBOForm("ApprovalDecisionReport", new object[] { this }));

                //Add Goods Receipt/Issue SBO Forms 
                doAddForm(Loader.getSBOForm("GoodsReceipt", new object[] { this }));
                doAddForm(Loader.getSBOForm("GoodsIssue", new object[] { this }));
            }

            doAddForm(Loader.getSBOForm("ItemMaster", new object[] { this }));
            doAddForm(Loader.getSBOForm("SalesOrder", new object[] { this }));
            doAddForm(Loader.getSBOForm("PurchaseOrder", new object[] { this }));
            doAddForm(Loader.getSBOForm("APInvoice", new object[] { this }));

            //ADMIN
            if (goDB.Data().Extra.Equals("ECO")) {
                doAddForm(Loader.getAdminForm("JobTypes", new object[] { this, "IDHADM", 1 }));
            } else {
                doAddForm(Loader.getAdminForm("JobTypes", new object[] { this, "IDHADM", 1 }));
                doAddForm(Loader.getAdminForm("EWC", new object[] { this, "IDHADM", 47 }));
                doAddForm(Loader.getAdminForm("HAZ", new object[] { this, "IDHADM", 48 }));
                doAddForm(Loader.getAdminForm("ENCategories", new object[] { this, "IDHADM", 49 }));
                doAddForm(Loader.getAdminForm("WEEE", new object[] { this, "IDHADM", 50 }));
                doAddForm(Loader.getAdminForm("Origin", new object[] { this, "IDHADM", 51 }));
                doAddForm(Loader.getAdminForm("Printers", new object[] { this, "IDHADM", 52 }));
                doAddForm(Loader.getAdminForm("Translate", new object[] { this, "IDHADM", 53 }));
                doAddForm(Loader.getAdminForm("SCReason", new object[] { this, "IDHADM", 54 }));
                doAddForm(Loader.getAdminForm("SCValidation", new object[] { this, "IDHADM", 55 }));
                doAddForm(Loader.getAdminForm("Status", new object[] { this, "IDHADM", 56 }));
                doAddForm(Loader.getAdminForm("WasteGroup", new object[] { this, "IDHADM", 58 })); //USA Requirement: Waste Group

                //OnTime [#Ico000????] USA 
                if (Config.ParameterAsBool("USAREL", false)) {
                    doAddForm(Loader.getAdminForm("CountyPlus", new object[] { this, "IDHADM", 57 })); //USA Requirement: USA County (from UFD1 table)
                    doAddForm(Loader.getAdminForm("PortMaster", new object[] { this, "IDHADM", 59 })); //USA Requirement: Port Master 
                    doAddForm(Loader.getAdminForm("CountyTariff", new object[] { this, "IDHADM", 60 })); //USA Requirement: County Tariff
                    doAddForm(Loader.getAdminForm("FuelSurcharge", new object[] { this, "IDHADM", 61 })); //USA Requirement: Fuel Surcharge
                }

                if (Config.ParameterAsBool("WFUSETRC", true)) {
                    Loader.doRegisterFR3Form("admin.TransactionCodes", new object[] { "IDHADM", 65 });
                    Loader.doRegisterFR3Form("admin.BPTransactionCodesLink", new object[] { "IDHADM", 66 });
                }

                if (Config.ParameterAsBool("OSMUSCB", false)) {
                    Loader.doRegisterFR3Form("admin.OSM", new object[] { "IDHADM", 67 });
                    //com.idh.forms.oo.Form.doRegisterForm(typeof(com.uBC.utils.FieldChangedView), "IDHADM", 68);
                }

                //BP Additional Charges
                if (Config.ParameterAsBool("USEBPACH", true)) {
                    Loader.doRegisterFR3Form("admin.BPAdditionalCharges", new object[] { "IDHADM", 69 });
                }
            }

            //MASTER DATA
            if (goDB.Data().Extra.Equals("ECO")) {
                doAddForm(Loader.getAdminForm("VehicleMaster", new object[] { this, "IDHMAST", 3 }));
            } else {
                if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) && Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES)) {
                    Loader.doRegisterFR2Form("prices.CIP", new object[] { "IDHMAST" });
                }

                if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) && Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES)) {
                    Loader.doRegisterFR2Form("prices.SIP", new object[] { "IDHMAST" });
                }
                Loader.doRegisterFR2Form("PriceUpdateOption", new object[] { });
                Loader.doRegisterFR3Form("admin.PriceGroup", new object[] { "IDHMAST", 21 });

                doAddForm(Loader.getAdminForm("MassPrice", new object[] { this, "IDHMAST", 22 }));
                doAddForm(Loader.getAdminForm("MassPriceSIP", new object[] { this, "IDHMAST", 23 }));

                if (!Config.ParameterAsBool("VMUSENEW", false)) {
                    doAddForm(Loader.getAdminForm("VehicleMaster", new object[] { this, "IDHMAST", 24 }));
                } else {
                    Loader.doRegisterFR3Form("VehicleM", new object[] { "IDHMAST", 24 });
                    Loader.doRegisterFR3Form("VehicleMaster", new object[] { "IDHMAST" });
                    Loader.doRegisterFR3Form("admin.VMReason", new object[] { "IDHADM" });
                }
                Loader.doRegisterFR3Form("admin.Reason", new object[] { "IDHADM" });

                string sRouteparm = Config.Parameter("ROUENABL");
                if (sRouteparm != "TRUE") {
                    doAddForm(Loader.getAdminForm("Routes", new object[] { this, "IDHMAST", 25 }));
                } else {
                    doAddForm(Loader.getAdminForm("Routes2", new object[] { this, "IDHMAST", 25 }));
                }

                doAddForm(Loader.getAdminForm("Zones", new object[] { this, "IDHMAST", 26 }));
                doAddForm(Loader.getAdminForm("TipZones", new object[] { this, "IDHADM", 27 }));

                doAddForm(Loader.getISBForm("admin.Config", new object[] { this, "IDHADM", 28 }));

                doAddForm(Loader.getAdminForm("GlAccnts", new object[] { this, "IDHMAST", 29 }));
                doAddForm(Loader.getAdminForm("BandFactors", new object[] { this, "IDHMAST", 28 }));
                doAddForm(Loader.getAdminForm("CCDetails", new object[] { this, "IDHMAST", 29 }));
                //doAddForm(Loader.getAdminForm("TakeOns", new object[] { this, "IDHMAST", 30 }));
                doAddForm(Loader.getAdminForm("WorkFlow", new object[] { this, "IDHMAST", 47 }));
                doAddForm(Loader.getAdminForm("SelfBill", new object[] { this, "IDHMAST", 48 }));
                doAddForm(Loader.getAdminForm("WasteGrpJobLink", new object[] { this, "IDHMAST", 50 }));
                doAddForm(Loader.getAdminForm("AddressRestriction", new object[] { this, "IDHMAST", 51 }));

                doAddForm(Loader.getSearchForm("Branch", new object[] { this }));

                doAddForm(Loader.getAdminForm("AltItemDescription", new object[] { this, "IDHMAST", 53 }));
                doAddForm(Loader.getAdminForm("ContainerDetails", new object[] { this, "IDHMAST", 54 }));

                doAddForm(Loader.getISBForm("RouteClosure", new object[] { this, "IDHMAST", 55 }));
            }

            //ORDERS
            if (goDB.Data().Extra.Equals("ECO")) {
                doAddForm(Loader.getISBForm("orders.Disposal", new object[] { this, 33 }));
            } else {
                doAddForm(Loader.getAdminForm("BPForecasting", new object[] { this, "IDHORD", 29 }));

                doAddForm(Loader.getISBForm("EVNMaster", new object[] { this, "IDHORD", 30 }));
                doAddForm(Loader.getISBForm("WasteContract", new object[] { this, 31 }));
                doAddForm(Loader.getISBForm("OrderRow", new object[] { this, 36 }));

                if (Config.INSTANCE.getParameterAsBool("PBIENABL", true)) {
                    doAddForm(Loader.getISBForm("PreBookInstruction", new object[] { this, 37 }));
                }
                Loader.doRegisterFR3Form("Import", new object[] { "IDHORD" });
                doAddForm(Loader.getISBForm("ImportIncomingPayments", new object[] { this, -1 }));
            }

            //PLANNING AND SCHEDULING
            if (goDB.Data().Extra.Equals("ECO")) {
                doAddForm(Loader.getManagerForm("Disposal", new object[] { this, 50, "IDHDOM", "Disposal Manager.srf", "Disposal Order Manager" }));
            } else {
                doAddForm(Loader.getManagerForm("Order", new object[] { this, 50 })); //, null, null, null }));
                doAddForm(Loader.getManagerForm("OrderL2", new object[] { this, 51 }));
                doAddForm(Loader.getManagerForm("OrderL3", new object[] { this, 52 }));
                if (Config.INSTANCE.getParameterAsBool("PBIENABL", true)) {
                    doAddForm(Loader.getManagerForm("OrderL4", new object[] { this, 53 }));
                }
                doAddForm(Loader.getManagerForm("OrderL5", new object[] { this, 55 }));
                doAddForm(Loader.getManagerForm("OrderL6", new object[] { this, 53 }));
                doAddForm(Loader.getManagerForm("OrderL7", new object[] { this, 54 }));
                doAddForm(Loader.getManagerForm("OrderL8", new object[] { this, 55 }));

                if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_FORMS1)) {
                    doAddForm(Loader.getManagerForm("WOBilling", new object[] { this, 56 }));
                    doAddForm(Loader.getManagerForm("WOBillingL2", new object[] { this, 64 }));
                    doAddForm(Loader.getManagerForm("WOBillingL3", new object[] { this, 67 }));
                    doAddForm(Loader.getManagerForm("WOBillingL4", new object[] { this, 68 }));
                    doAddForm(Loader.getManagerForm("WOBillingL5", new object[] { this, 69 }));
                }

                if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_FORMS1)) {
                    doAddForm(Loader.getManagerForm("BPForecast", new object[] { this, 69 }));
                }

                doAddForm(Loader.getManagerForm("EVN", new object[] { this, 69 }));

                string sRouteparm = Config.Parameter("ROUENABL");
                if (sRouteparm == "TRUE") {
                    doAddForm(Loader.getManagerForm("RoutePlanner", new object[] { this, 70 }));
                } else {
                    doAddForm(Loader.getManagerForm("Route", new object[] { this, 70 }));
                }

                string sVehicleManager = Config.ParameterWithDefault("VEHMAN", "IDHVECR2");
                if (sVehicleManager == "IDHVECR2") {
                    doAddForm(Loader.getManagerForm("Vehicle2", new object[] { this, 72 }));
                } else if (sVehicleManager == "IDHVECR3") {
                    doAddForm(Loader.getManagerForm("Vehicle3", new object[] { this, 72 }));
                }

                doAddForm(Loader.getManagerForm("Disposal", new object[] { this, 73, "IDHDOM", "Disposal Manager.srf", "Disposal Order Manager" }));

                if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_FORMS1)) {
                    doAddForm(Loader.getManagerForm("ConsignmentWO", new object[] { this, 75 }));
                }

                doAddForm(Loader.getManagerForm("ConsignmentDO", new object[] { this, 76 }));

                if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_FORMS1)) {
                    doAddForm(Loader.getManagerForm("DeLink", new object[] { this, 78 }));
                }

                doAddForm(Loader.getManagerForm("Licence", new object[] { this, 80 }));
                doAddForm(Loader.getManagerForm("Updater", new object[] { this, 81 }));

                if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_FORMS1)) {
                    doAddForm(Loader.getManagerForm("DOUpdater", new object[] { this, 82 }));
                }

                if (Config.INSTANCE.getParameterAsBool("PBIENABL", true)) {
                    if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_FORMS1)) {
                        doAddForm(Loader.getManagerForm("PreBook", new object[] { this, 83 }));
                    }
                    doAddForm(Loader.getAdminForm("PBIDetails", new object[] { this, -1 }));
                }

                doAddForm(Loader.getSearchForm("Subby", new object[] { this, "IDHPS", 85 }));

                doAddForm(Loader.getManagerForm("APDocWizard", new object[] { this, 87 }));
                doAddForm(Loader.getManagerForm("ARDocWizard", new object[] { this, 88 }));

                Loader.doRegisterFR2Form("manager.LoadForm", new object[] { "IDHPS" });
            }

            //TFS Module 
            if (!goDB.Data().Extra.Equals("ECO")) {
                if (Config.ParameterWithDefault("TFSMOD", "") == "TFSMOD_TRUE") {
                    doAddForm(Loader.getAdminForm("TFS", new object[] { this, "IDHTFS", 1 }));
                    doAddForm(Loader.getAdminForm("CompAuthority", new object[] { this, "IDHTFS", 2 }));
                    doAddForm(Loader.getAdminForm("CADocuments", new object[] { this, "IDHTFS", 3 }));
                    doAddForm(Loader.getAdminForm("CAMatrix", new object[] { this, "IDHTFS", 4 }));
                    doAddForm(Loader.getAdminForm("DCodeMaster", new object[] { this, "IDHTFS", 5 }));
                    doAddForm(Loader.getAdminForm("HCodeMaster", new object[] { this, "IDHTFS", 6 }));
                    doAddForm(Loader.getAdminForm("RCodeMaster", new object[] { this, "IDHTFS", 7 }));
                    doAddForm(Loader.getAdminForm("YCodeMaster", new object[] { this, "IDHTFS", 8 }));
                    doAddForm(Loader.getAdminForm("BASEL", new object[] { this, "IDHTFS", 9 }));
                    doAddForm(Loader.getAdminForm("OECD", new object[] { this, "IDHTFS", 10 }));
                    doAddForm(Loader.getAdminForm("CarLic", new object[] { this, "IDHTFS", 11 }));
                    doAddForm(Loader.getAdminForm("ShipStat", new object[] { this, "IDHTFS", 12 }));
                    doAddForm(Loader.getAdminForm("TFSTyp", new object[] { this, "IDHTFS", 13 }));
                    doAddForm(Loader.getAdminForm("TFSStat", new object[] { this, "IDHTFS", 14 }));
                    doAddForm(Loader.getAdminForm("PackIndex", new object[] { this, "IDHTFS", 15 }));
                    doAddForm(Loader.getAdminForm("MovStat", new object[] { this, "IDHTFS", 16 }));

                    doAddForm(Loader.getFormInstance("ISB_TFS.dll", "WR1_TFS.idh.forms.TFS", "TFSNewNo", new object[] { this, "IDHTFS", 17 }));
                    doAddForm(Loader.getFormInstance("ISB_TFS.dll", "WR1_TFS.idh.forms.TFS", "TFSConsole", new object[] { this, "IDHTFS", 18 }));
                    doAddForm(Loader.getFormInstance("ISB_TFS.dll", "WR1_TFS.idh.forms.TFS", "TFSAnnex", new object[] { this, "IDHTFS", 19 }));

                    doAddForm(Loader.getManagerForm("TFSSummary", new object[] { this, 20 }));
                }
            }

            //REPORTING
            if (!goDB.Data().Extra.Equals("ECO")) {
                doAddForm(Loader.getReportForm("OnSite", new object[] { this, 10 }));
            }

            doAddForm(Loader.getAdminForm("FormSettings", new object[] { this, "IDHADM", 11 }));

            Loader.doRegisterFR3Form("DOTAXDETAIL", new object[] { });
            Loader.doRegisterFR2Form("BusinessPartnerAddressUpdater", new object[] { });
            Loader.doRegisterFR3Form("PostCode", new object[] { });
        }


        protected override void doCreateExtraMenu() {
            base.doCreateExtraMenu();
        }
    }
}
