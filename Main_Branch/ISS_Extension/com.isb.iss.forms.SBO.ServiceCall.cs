﻿using SAPbouiCOM;

namespace com.isb.iss.forms.SBO {
    public class ServiceCall : WR1_SBOForms.idh.forms.SBO.ServiceCall {
        public ServiceCall(IDHAddOns.idh.addon.Base oParent) : base(oParent) {
        }

        public override void doLoadForm(Form oForm, ref bool BubbleEvent) {
            base.doLoadForm(oForm, ref BubbleEvent);
            //Prices
            //"IDH_SUBCON"
            SAPbouiCOM.Item oItem = oForm.Items.Item("IDH_SUBCON");
            doAddButton(oForm, "IDH_PRICES", 0, oItem.Left + oItem.Width + 5, oItem.Top, oItem.Width, oItem.Height, getTranslatedWord("Prices"), "8");

        }
    }
}
